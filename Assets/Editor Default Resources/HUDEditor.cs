﻿using UnityEngine;
using System.Collections;

public class HUDEditor : MonoBehaviour 
{
    private AnimationControl anim;
    private Transform target;
	void Start () 
    {
        anim = transform.Find("player/Root/EntityRoot").GetComponentInChildren<AnimationControl>();
        anim.Init();
        target = transform.Find("player/Root/TopRoot");
	}

    void OnGUI()
    {
        if(GUI.Button(new Rect(0, 0, 100, 50), "Damage"))
        {
            anim.PlayHit();
            HUDManager.instance.SetContent(target, HUDUnit.HUDType.Damage, "100");
        }
        if (GUI.Button(new Rect(0, 100, 100, 50), "Treat"))
        {
            HUDManager.instance.SetContent(target, HUDUnit.HUDType.Treat, "100");
        }
    }

}
