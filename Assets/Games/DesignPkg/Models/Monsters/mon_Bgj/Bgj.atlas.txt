
skeleton.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
animation/001
  rotate: false
  xy: 2, 825
  size: 212, 113
  orig: 232, 134
  offset: 14, 13
  index: -1
animation/attack/attack.001
  rotate: false
  xy: 329, 669
  size: 118, 153
  orig: 1152, 648
  offset: 556, 269
  index: -1
animation/attack/attack.004
  rotate: true
  xy: 2, 327
  size: 164, 147
  orig: 1152, 648
  offset: 504, 258
  index: -1
animation/attack/attack.007
  rotate: false
  xy: 263, 516
  size: 147, 151
  orig: 1152, 648
  offset: 508, 247
  index: -1
animation/attack/attack.010
  rotate: true
  xy: 1585, 801
  size: 137, 159
  orig: 1152, 648
  offset: 522, 246
  index: -1
animation/attack/attack.013
  rotate: false
  xy: 412, 513
  size: 122, 154
  orig: 1152, 648
  offset: 580, 256
  index: -1
animation/attack/attack.016
  rotate: false
  xy: 153, 182
  size: 112, 158
  orig: 1152, 648
  offset: 600, 259
  index: -1
animation/attack/attack.019
  rotate: true
  xy: 892, 686
  size: 126, 159
  orig: 1152, 648
  offset: 610, 255
  index: -1
animation/attack/attack.021
  rotate: true
  xy: 1746, 786
  size: 152, 158
  orig: 1152, 648
  offset: 618, 251
  index: -1
animation/attack/attack.023
  rotate: true
  xy: 2, 163
  size: 162, 149
  orig: 1152, 648
  offset: 617, 255
  index: -1
animation/attack/attack.025
  rotate: false
  xy: 892, 543
  size: 137, 141
  orig: 1152, 648
  offset: 618, 260
  index: -1
animation/attack/attack.026
  rotate: false
  xy: 1053, 674
  size: 130, 138
  orig: 1152, 648
  offset: 620, 263
  index: -1
animation/attack/attack.027
  rotate: false
  xy: 540, 366
  size: 126, 145
  orig: 1152, 648
  offset: 561, 266
  index: -1
animation/attack/attack.029
  rotate: true
  xy: 2, 493
  size: 174, 143
  orig: 1152, 648
  offset: 462, 268
  index: -1
animation/attack/attack.031
  rotate: false
  xy: 2, 669
  size: 170, 154
  orig: 1152, 648
  offset: 452, 257
  index: -1
animation/attack/attack.033
  rotate: false
  xy: 2, 2
  size: 156, 159
  orig: 1152, 648
  offset: 474, 257
  index: -1
animation/attack/attack.035
  rotate: true
  xy: 922, 814
  size: 124, 167
  orig: 1152, 648
  offset: 535, 258
  index: -1
animation/dead/dead.001
  rotate: false
  xy: 668, 361
  size: 122, 154
  orig: 1152, 648
  offset: 556, 270
  index: -1
animation/dead/dead.004
  rotate: false
  xy: 268, 368
  size: 135, 146
  orig: 1152, 648
  offset: 573, 286
  index: -1
animation/dead/dead.007
  rotate: false
  xy: 1906, 787
  size: 140, 151
  orig: 1152, 648
  offset: 586, 300
  index: -1
animation/dead/dead.010
  rotate: true
  xy: 752, 824
  size: 114, 168
  orig: 1152, 648
  offset: 603, 303
  index: -1
animation/dead/dead.013
  rotate: true
  xy: 405, 369
  size: 142, 133
  orig: 1152, 648
  offset: 575, 310
  index: -1
animation/dead/dead.016
  rotate: false
  xy: 1486, 556
  size: 138, 124
  orig: 1152, 648
  offset: 554, 313
  index: -1
animation/dead/dead.019
  rotate: true
  xy: 734, 681
  size: 141, 156
  orig: 1152, 648
  offset: 539, 269
  index: -1
animation/dead/dead.022
  rotate: false
  xy: 1189, 557
  size: 142, 132
  orig: 1152, 648
  offset: 509, 221
  index: -1
animation/dead/dead.025
  rotate: false
  xy: 1091, 815
  size: 164, 123
  orig: 1152, 648
  offset: 504, 218
  index: -1
animation/dead/dead.028
  rotate: false
  xy: 401, 833
  size: 178, 105
  orig: 1152, 648
  offset: 501, 208
  index: -1
animation/idel/idel_-45/idel_-45.001
  rotate: false
  xy: 653, 517
  size: 110, 159
  orig: 1152, 648
  offset: 503, 270
  index: -1
animation/idel/idel_-45/idel_-45.004
  rotate: false
  xy: 151, 342
  size: 115, 161
  orig: 1152, 648
  offset: 496, 267
  index: -1
animation/idel/idel_-45/idel_-45.007
  rotate: false
  xy: 147, 505
  size: 114, 162
  orig: 1152, 648
  offset: 499, 264
  index: -1
animation/idel/idel_-45/idel_-45.010
  rotate: false
  xy: 160, 20
  size: 110, 160
  orig: 1152, 648
  offset: 506, 262
  index: -1
animation/idel/idel_-45/idel_-45.013
  rotate: false
  xy: 792, 362
  size: 109, 159
  orig: 1152, 648
  offset: 508, 260
  index: -1
animation/idel/idel_-45/idel_-45.016
  rotate: false
  xy: 536, 513
  size: 115, 157
  orig: 1152, 648
  offset: 504, 260
  index: -1
animation/idel/idel_-45/idel_-45.019
  rotate: true
  xy: 1185, 691
  size: 122, 155
  orig: 1152, 648
  offset: 497, 261
  index: -1
animation/idel/idel_-45/idel_-45.022
  rotate: false
  xy: 765, 523
  size: 125, 156
  orig: 1152, 648
  offset: 493, 262
  index: -1
animation/idel/idel_-45/idel_-45.025
  rotate: false
  xy: 903, 386
  size: 123, 155
  orig: 1152, 648
  offset: 493, 265
  index: -1
animation/idel/idel_-45/idel_-45.028
  rotate: true
  xy: 1031, 552
  size: 120, 156
  orig: 1152, 648
  offset: 495, 268
  index: -1
animation/idel/idel_-45/idel_-45.031
  rotate: true
  xy: 1502, 682
  size: 117, 156
  orig: 1152, 648
  offset: 497, 271
  index: -1
animation/idel/idel_-45/idel_-45.034
  rotate: true
  xy: 1342, 690
  size: 109, 158
  orig: 1152, 648
  offset: 504, 271
  index: -1
animation/prepare/prepare.001
  rotate: true
  xy: 1333, 567
  size: 121, 151
  orig: 1152, 648
  offset: 554, 269
  index: -1
animation/prepare/prepare.004
  rotate: false
  xy: 174, 671
  size: 153, 151
  orig: 1152, 648
  offset: 553, 269
  index: -1
animation/prepare/prepare.007
  rotate: false
  xy: 580, 678
  size: 152, 151
  orig: 1152, 648
  offset: 552, 271
  index: -1
animation/prepare/prepare.010
  rotate: true
  xy: 1423, 801
  size: 137, 160
  orig: 1152, 648
  offset: 551, 265
  index: -1
animation/prepare/prepare.013
  rotate: false
  xy: 449, 672
  size: 129, 159
  orig: 1152, 648
  offset: 550, 268
  index: -1
animation/prepare/prepare.016
  rotate: true
  xy: 1257, 815
  size: 123, 164
  orig: 1152, 648
  offset: 551, 262
  index: -1
animation/prepare/prepare.019
  rotate: true
  xy: 216, 824
  size: 114, 183
  orig: 1152, 648
  offset: 552, 241
  index: -1
animation/prepare/prepare.022
  rotate: true
  xy: 581, 831
  size: 107, 169
  orig: 1152, 648
  offset: 554, 253
  index: -1
