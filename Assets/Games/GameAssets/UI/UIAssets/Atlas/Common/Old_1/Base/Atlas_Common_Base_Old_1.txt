{"frames": {

"UI-S2-Common-duihuaqipao.png":
{
	"frame": {"x":93,"y":304,"w":59,"h":57},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":59,"h":57},
	"sourceSize": {"w":59,"h":57}
},
"UI-s2-Common-anniuweixuanzezhuangtai.png":
{
	"frame": {"x":906,"y":2,"w":115,"h":72},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":115,"h":72},
	"sourceSize": {"w":116,"h":72}
},
"UI-s2-Common-anniuxuanzezhuangtai.png":
{
	"frame": {"x":906,"y":76,"w":115,"h":72},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":115,"h":72},
	"sourceSize": {"w":116,"h":72}
},
"UI-s2-Common-diban.png":
{
	"frame": {"x":385,"y":300,"w":45,"h":26},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":45,"h":26},
	"sourceSize": {"w":45,"h":26}
},
"UI-s2-Common-gouxuan.png":
{
	"frame": {"x":2,"y":350,"w":56,"h":56},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":56,"h":56},
	"sourceSize": {"w":56,"h":56}
},
"UI-s2-Common-liaotianqipao.png":
{
	"frame": {"x":102,"y":181,"w":72,"h":52},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":72,"h":52},
	"sourceSize": {"w":72,"h":52}
},
"UI-s2-Common-weigouxuan.png":
{
	"frame": {"x":60,"y":363,"w":56,"h":56},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":56,"h":56},
	"sourceSize": {"w":56,"h":56}
},
"UI-s2-commom-duihua.png":
{
	"frame": {"x":358,"y":92,"w":29,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":29,"h":27},
	"sourceSize": {"w":29,"h":27}
},
"UI-s2-commom-exp.png":
{
	"frame": {"x":696,"y":216,"w":39,"h":28},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":39,"h":28},
	"sourceSize": {"w":39,"h":28}
},
"UI-s2-commom-guanbi.png":
{
	"frame": {"x":390,"y":60,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-commom-jishiben.png":
{
	"frame": {"x":669,"y":112,"w":25,"h":33},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":25,"h":33},
	"sourceSize": {"w":25,"h":33}
},
"UI-s2-commom-lingquanniu.png":
{
	"frame": {"x":2,"y":408,"w":54,"h":92},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":54,"h":92},
	"sourceSize": {"w":54,"h":92}
},
"UI-s2-commom-lingquwaikuangliang.png":
{
	"frame": {"x":2,"y":202,"w":72,"h":51},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":72,"h":51},
	"sourceSize": {"w":72,"h":51}
},
"UI-s2-commom-renzheng.png":
{
	"frame": {"x":648,"y":147,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-commom-taohaozhuren.png":
{
	"frame": {"x":76,"y":223,"w":24,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":22},
	"sourceSize": {"w":24,"h":22}
},
"UI-s2-commom-yuandi.png":
{
	"frame": {"x":2,"y":502,"w":51,"h":51},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":51,"h":51},
	"sourceSize": {"w":51,"h":51}
},
"UI-s2-commom-zhiyewenlu.png":
{
	"frame": {"x":696,"y":2,"w":208,"h":212},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":208,"h":212},
	"sourceSize": {"w":210,"h":213}
},
"UI-s2-commom-zuduirenwuwenzidi.png":
{
	"frame": {"x":669,"y":18,"w":25,"h":92},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":25,"h":92},
	"sourceSize": {"w":25,"h":92}
},
"UI-s2-common-anniufengexian.png":
{
	"frame": {"x":630,"y":203,"w":13,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":13,"h":5},
	"sourceSize": {"w":13,"h":5}
},
"UI-s2-common-baidikuang.png":
{
	"frame": {"x":439,"y":18,"w":125,"h":75},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":125,"h":75},
	"sourceSize": {"w":125,"h":75}
},
"UI-s2-common-bianjiicon.png":
{
	"frame": {"x":531,"y":166,"w":31,"h":33},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":31,"h":33},
	"sourceSize": {"w":31,"h":33}
},
"UI-s2-common-bianjiicon2.png":
{
	"frame": {"x":66,"y":282,"w":25,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":25,"h":27},
	"sourceSize": {"w":25,"h":27}
},
"UI-s2-common-daleixuanzedi.png":
{
	"frame": {"x":265,"y":322,"w":63,"h":66},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":66},
	"sourceSize": {"w":63,"h":66}
},
"UI-s2-common-daojishizhong.png":
{
	"frame": {"x":531,"y":130,"w":31,"h":34},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":31,"h":34},
	"sourceSize": {"w":31,"h":34}
},
"UI-s2-common-dayi.png":
{
	"frame": {"x":1011,"y":185,"w":8,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":8,"h":27},
	"sourceSize": {"w":8,"h":27}
},
"UI-s2-common-dayudengyu fuhao.png":
{
	"frame": {"x":78,"y":171,"w":22,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":22,"h":27},
	"sourceSize": {"w":22,"h":27}
},
"UI-s2-common-dengjidi.png":
{
	"frame": {"x":435,"y":225,"w":66,"h":24},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":66,"h":24},
	"sourceSize": {"w":66,"h":24}
},
"UI-s2-common-di.png":
{
	"frame": {"x":351,"y":151,"w":34,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":27},
	"sourceSize": {"w":34,"h":27}
},
"UI-s2-common-dikuang.png":
{
	"frame": {"x":202,"y":208,"w":68,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":68,"h":64},
	"sourceSize": {"w":68,"h":64}
},
"UI-s2-common-duizhangzi.png":
{
	"frame": {"x":58,"y":454,"w":53,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":53,"h":31},
	"sourceSize": {"w":53,"h":31}
},
"UI-s2-common-fa.png":
{
	"frame": {"x":348,"y":191,"w":37,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":37,"h":37},
	"sourceSize": {"w":37,"h":37}
},
"UI-s2-common-fanhui.png":
{
	"frame": {"x":503,"y":201,"w":60,"h":53},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":60,"h":53},
	"sourceSize": {"w":60,"h":53}
},
"UI-s2-common-fengexian.png":
{
	"frame": {"x":1019,"y":214,"w":3,"h":71},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":3,"h":71},
	"sourceSize": {"w":3,"h":71}
},
"UI-s2-common-fenggexian.png":
{
	"frame": {"x":2,"y":18,"w":435,"h":3},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":435,"h":3},
	"sourceSize": {"w":435,"h":3}
},
"UI-s2-common-fenggexian2.png":
{
	"frame": {"x":1011,"y":214,"w":6,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":6,"h":5},
	"sourceSize": {"w":6,"h":5}
},
"UI-s2-common-fuwuqizhuangtaihao.png":
{
	"frame": {"x":531,"y":95,"w":33,"h":33},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":33,"h":33},
	"sourceSize": {"w":33,"h":33}
},
"UI-s2-common-fuwuqizhuangtaimang.png":
{
	"frame": {"x":171,"y":257,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"UI-s2-common-gong.png":
{
	"frame": {"x":348,"y":230,"w":37,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":37,"h":37},
	"sourceSize": {"w":37,"h":37}
},
"UI-s2-common-guanbibaisecha.png":
{
	"frame": {"x":78,"y":124,"w":23,"h":23},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":23,"h":23},
	"sourceSize": {"w":23,"h":23}
},
"UI-s2-common-hechenganniu.png":
{
	"frame": {"x":2,"y":23,"w":99,"h":99},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":99,"h":99},
	"sourceSize": {"w":99,"h":99}
},
"UI-s2-common-heidijiantou.png":
{
	"frame": {"x":176,"y":200,"w":24,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":32},
	"sourceSize": {"w":24,"h":32}
},
"UI-s2-common-heisetuoyuandi.png":
{
	"frame": {"x":259,"y":390,"w":57,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":57,"h":35},
	"sourceSize": {"w":57,"h":35}
},
"UI-s2-common-hongsexiangxiajiantou.png":
{
	"frame": {"x":78,"y":200,"w":22,"h":21},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":22,"h":21},
	"sourceSize": {"w":22,"h":21}
},
"UI-s2-common-hongsexuetiao.png":
{
	"frame": {"x":180,"y":104,"w":20,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":20,"h":9},
	"sourceSize": {"w":20,"h":9}
},
"UI-s2-common-huangsexuetiao.png":
{
	"frame": {"x":180,"y":115,"w":20,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":20,"h":9},
	"sourceSize": {"w":20,"h":9}
},
"UI-s2-common-huisexiaobianqian.png":
{
	"frame": {"x":648,"y":195,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-common-jiadianhuangse.png":
{
	"frame": {"x":58,"y":487,"w":52,"h":34},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":52,"h":34},
	"sourceSize": {"w":52,"h":34}
},
"UI-s2-common-jiadianlanse.png":
{
	"frame": {"x":1011,"y":150,"w":9,"h":33},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":9,"h":33},
	"sourceSize": {"w":9,"h":33}
},
"UI-s2-common-jiagezhangdieshujudikuang.png":
{
	"frame": {"x":277,"y":148,"w":72,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":72,"h":35},
	"sourceSize": {"w":72,"h":35}
},
"UI-s2-common-jiahao.png":
{
	"frame": {"x":358,"y":121,"w":28,"h":28},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":28,"h":28},
	"sourceSize": {"w":28,"h":28}
},
"UI-s2-common-jiahao2.png":
{
	"frame": {"x":102,"y":235,"w":67,"h":67},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":67,"h":67},
	"sourceSize": {"w":67,"h":67}
},
"UI-s2-common-jiahao3.png":
{
	"frame": {"x":435,"y":251,"w":65,"h":66},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":65,"h":66},
	"sourceSize": {"w":65,"h":66}
},
"UI-s2-common-jianhaoicon.png":
{
	"frame": {"x":351,"y":180,"w":28,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":28,"h":9},
	"sourceSize": {"w":28,"h":9}
},
"UI-s2-common-jiantouxiangshang.png":
{
	"frame": {"x":171,"y":286,"w":27,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":25},
	"sourceSize": {"w":27,"h":25}
},
"UI-s2-common-jiantouxiangxia.png":
{
	"frame": {"x":66,"y":255,"w":27,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":25},
	"sourceSize": {"w":27,"h":25}
},
"UI-s2-common-jinbi.png":
{
	"frame": {"x":630,"y":243,"w":37,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":37,"h":37},
	"sourceSize": {"w":37,"h":37}
},
"UI-s2-common-jindu.png":
{
	"frame": {"x":503,"y":188,"w":15,"h":6},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":15,"h":6},
	"sourceSize": {"w":15,"h":6}
},
"UI-s2-common-jindudi.png":
{
	"frame": {"x":202,"y":23,"w":92,"h":17},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":92,"h":17},
	"sourceSize": {"w":92,"h":17}
},
"UI-s2-common-jindudi2.png":
{
	"frame": {"x":76,"y":247,"w":21,"h":6},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":6},
	"sourceSize": {"w":21,"h":6}
},
"UI-s2-common-juseanniu.png":
{
	"frame": {"x":103,"y":23,"w":97,"h":79},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":97,"h":79},
	"sourceSize": {"w":97,"h":79}
},
"UI-s2-common-lansebianjiao.png":
{
	"frame": {"x":389,"y":108,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-common-lansexuetiao.png":
{
	"frame": {"x":180,"y":126,"w":20,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":20,"h":9},
	"sourceSize": {"w":20,"h":9}
},
"UI-s2-common-leibieanniu.png":
{
	"frame": {"x":338,"y":317,"w":44,"h":88},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":44,"h":88},
	"sourceSize": {"w":44,"h":89}
},
"UI-s2-common-leibieanniudianjizhuangtai.png":
{
	"frame": {"x":154,"y":313,"w":44,"h":88},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":44,"h":88},
	"sourceSize": {"w":44,"h":89}
},
"UI-s2-common-leibiexuanzeicon.png":
{
	"frame": {"x":502,"y":256,"w":59,"h":59},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":59,"h":59},
	"sourceSize": {"w":59,"h":59}
},
"UI-s2-common-lixian.png":
{
	"frame": {"x":118,"y":403,"w":54,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":54,"h":31},
	"sourceSize": {"w":54,"h":31}
},
"UI-s2-common-lvsebiaoqian.png":
{
	"frame": {"x":387,"y":156,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-common-lvsexiangshangjiantou.png":
{
	"frame": {"x":176,"y":234,"w":22,"h":21},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":22,"h":21},
	"sourceSize": {"w":22,"h":21}
},
"UI-s2-common-lvsexuetiao.png":
{
	"frame": {"x":180,"y":137,"w":20,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":20,"h":9},
	"sourceSize": {"w":20,"h":9}
},
"UI-s2-common-menpaibiaoji1.png":
{
	"frame": {"x":114,"y":436,"w":53,"h":53},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":53,"h":53},
	"sourceSize": {"w":53,"h":53}
},
"UI-s2-common-mofatiao.png":
{
	"frame": {"x":296,"y":23,"w":92,"h":17},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":92,"h":17},
	"sourceSize": {"w":92,"h":17}
},
"UI-s2-common-new.png":
{
	"frame": {"x":387,"y":204,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-common-nuqitiao.png":
{
	"frame": {"x":202,"y":42,"w":92,"h":17},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":92,"h":17},
	"sourceSize": {"w":92,"h":17}
},
"UI-s2-common-qiehuanjiantou.png":
{
	"frame": {"x":277,"y":185,"w":69,"h":69},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":69,"h":69},
	"sourceSize": {"w":69,"h":69}
},
"UI-s2-common-renwutouxiangkuang.png":
{
	"frame": {"x":566,"y":121,"w":80,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":80},
	"sourceSize": {"w":80,"h":80}
},
"UI-s2-common-renzhengzi.png":
{
	"frame": {"x":358,"y":61,"w":29,"h":29},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":29,"h":29},
	"sourceSize": {"w":29,"h":29}
},
"UI-s2-common-sanjiaoxingjiantou.png":
{
	"frame": {"x":78,"y":149,"w":23,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":23,"h":20},
	"sourceSize": {"w":23,"h":20}
},
"UI-s2-common-shaizi.png":
{
	"frame": {"x":2,"y":124,"w":74,"h":76},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":74,"h":76},
	"sourceSize": {"w":74,"h":76}
},
"UI-s2-common-shen.png":
{
	"frame": {"x":180,"y":180,"w":19,"h":18},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":18},
	"sourceSize": {"w":19,"h":18}
},
"UI-s2-common-tian.png":
{
	"frame": {"x":648,"y":121,"w":19,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":20},
	"sourceSize": {"w":19,"h":20}
},
"UI-s2-common-tiankongkuang.png":
{
	"frame": {"x":435,"y":188,"w":66,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":66,"h":35},
	"sourceSize": {"w":66,"h":35}
},
"UI-s2-common-tixingicon.png":
{
	"frame": {"x":272,"y":256,"w":64,"h":64},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":64,"h":64},
	"sourceSize": {"w":66,"h":66}
},
"UI-s2-common-touxiangdikuang.png":
{
	"frame": {"x":906,"y":150,"w":103,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":103,"h":102},
	"sourceSize": {"w":103,"h":102}
},
"UI-s2-common-weixuanzeanniu.png":
{
	"frame": {"x":283,"y":61,"w":73,"h":85},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":73,"h":85},
	"sourceSize": {"w":73,"h":86}
},
"UI-s2-common-weixuanzezhuangtaianniuxiao.png":
{
	"frame": {"x":202,"y":61,"w":79,"h":58},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":79,"h":58},
	"sourceSize": {"w":79,"h":59}
},
"UI-s2-common-weixuanzhongzhanniu&wenzidi.png":
{
	"frame": {"x":200,"y":274,"w":63,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":85},
	"sourceSize": {"w":63,"h":85}
},
"UI-s2-common-wenzidi.png":
{
	"frame": {"x":390,"y":23,"w":47,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":47,"h":35},
	"sourceSize": {"w":47,"h":35}
},
"UI-s2-common-wenzidikuang.png":
{
	"frame": {"x":200,"y":361,"w":57,"h":54},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":57,"h":54},
	"sourceSize": {"w":57,"h":54}
},
"UI-s2-common-wenzizhuangshidi.png":
{
	"frame": {"x":2,"y":313,"w":57,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":57,"h":35},
	"sourceSize": {"w":57,"h":35}
},
"UI-s2-common-wupindikuang.png":
{
	"frame": {"x":438,"y":95,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"UI-s2-common-xuanzeanniu.png":
{
	"frame": {"x":202,"y":121,"w":73,"h":85},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":73,"h":85},
	"sourceSize": {"w":73,"h":86}
},
"UI-s2-common-xuanzezhuangtaikuang.png":
{
	"frame": {"x":566,"y":18,"w":101,"h":101},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":101,"h":101},
	"sourceSize": {"w":101,"h":101}
},
"UI-s2-common-xuetiao.png":
{
	"frame": {"x":296,"y":42,"w":92,"h":17},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":92,"h":17},
	"sourceSize": {"w":92,"h":17}
},
"UI-s2-common-xuetiaodi.png":
{
	"frame": {"x":180,"y":148,"w":20,"h":9},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":20,"h":9},
	"sourceSize": {"w":20,"h":9}
},
"UI-s2-common-yinbi.png":
{
	"frame": {"x":563,"y":268,"w":37,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":37,"h":37},
	"sourceSize": {"w":37,"h":37}
},
"UI-s2-common-yulananniudi.png":
{
	"frame": {"x":103,"y":104,"w":75,"h":75},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":75,"h":75},
	"sourceSize": {"w":75,"h":75}
},
"UI-s2-common-zhan.png":
{
	"frame": {"x":338,"y":269,"w":45,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":45,"h":46},
	"sourceSize": {"w":45,"h":46}
},
"UI-s2-common-zhanli.png":
{
	"frame": {"x":58,"y":421,"w":54,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":54,"h":31},
	"sourceSize": {"w":54,"h":31}
},
"UI-s2-common-zhezhao.png":
{
	"frame": {"x":2,"y":2,"w":692,"h":14},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":692,"h":14},
	"sourceSize": {"w":692,"h":14}
},
"UI-s2-common-zhuangbeixiangqingicon.png":
{
	"frame": {"x":565,"y":203,"w":63,"h":63},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":63},
	"sourceSize": {"w":63,"h":63}
},
"UI-s2-common-zisebianjiao.png":
{
	"frame": {"x":387,"y":252,"w":46,"h":46},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
	"sourceSize": {"w":46,"h":46}
},
"UI-s2-common-zuduirenwudi.png":
{
	"frame": {"x":180,"y":159,"w":19,"h":19},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":19},
	"sourceSize": {"w":19,"h":19}
},
"UI-s2-common-zuzhanzi.png":
{
	"frame": {"x":174,"y":417,"w":54,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":54,"h":31},
	"sourceSize": {"w":54,"h":31}
},
"UI-s2-ph-fanyeanniudi.png":
{
	"frame": {"x":2,"y":255,"w":62,"h":56},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":56},
	"sourceSize": {"w":62,"h":56}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "Atlas_Common_Base_Old_1.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:c706e5332e420c63e90dcb91d0e6170e:35eb93623bc45b3e39555d971996a957:ab3216775a4f74443c22c378cc1bcf13$"
}
}
