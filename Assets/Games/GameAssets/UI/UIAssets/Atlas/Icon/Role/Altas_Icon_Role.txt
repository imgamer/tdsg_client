{"frames": {

"Assistant_001.png":
{
	"frame": {"x":2,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_002.png":
{
	"frame": {"x":2,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_003.png":
{
	"frame": {"x":2,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_004.png":
{
	"frame": {"x":2,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_005.png":
{
	"frame": {"x":2,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_006.png":
{
	"frame": {"x":97,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_007.png":
{
	"frame": {"x":192,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_008.png":
{
	"frame": {"x":287,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Assistant_009.png":
{
	"frame": {"x":382,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_001.png":
{
	"frame": {"x":97,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_002.png":
{
	"frame": {"x":97,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_003.png":
{
	"frame": {"x":97,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_004.png":
{
	"frame": {"x":97,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_005.png":
{
	"frame": {"x":192,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Hero_006.png":
{
	"frame": {"x":287,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "Altas_Icon_Role.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:a1a9027cec9039bf5b1b4878aaf1b66c:adf3bc35a17b2c391c559262b0556ddc:14d7bcef0443bdcf9ba6a95f2f8b836d$"
}
}
