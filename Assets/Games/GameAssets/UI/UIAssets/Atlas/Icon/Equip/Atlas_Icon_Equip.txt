{"frames": {

"Equip_0101.png":
{
	"frame": {"x":2,"y":124,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0102.png":
{
	"frame": {"x":2,"y":220,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0103.png":
{
	"frame": {"x":2,"y":316,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0104.png":
{
	"frame": {"x":2,"y":412,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0105.png":
{
	"frame": {"x":2,"y":508,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0106.png":
{
	"frame": {"x":2,"y":604,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0107.png":
{
	"frame": {"x":2,"y":700,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0108.png":
{
	"frame": {"x":2,"y":796,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0201.png":
{
	"frame": {"x":2,"y":892,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0202.png":
{
	"frame": {"x":97,"y":124,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0203.png":
{
	"frame": {"x":124,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0204.png":
{
	"frame": {"x":97,"y":220,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0205.png":
{
	"frame": {"x":97,"y":316,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0206.png":
{
	"frame": {"x":97,"y":412,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0207.png":
{
	"frame": {"x":97,"y":508,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0208.png":
{
	"frame": {"x":97,"y":604,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0301.png":
{
	"frame": {"x":97,"y":700,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0302.png":
{
	"frame": {"x":97,"y":796,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0303.png":
{
	"frame": {"x":97,"y":892,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0304.png":
{
	"frame": {"x":219,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0305.png":
{
	"frame": {"x":314,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0306.png":
{
	"frame": {"x":409,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0307.png":
{
	"frame": {"x":504,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0308.png":
{
	"frame": {"x":599,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0401.png":
{
	"frame": {"x":694,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0402.png":
{
	"frame": {"x":2,"y":2,"w":120,"h":120},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":120},
	"sourceSize": {"w":120,"h":120}
},
"Equip_0403.png":
{
	"frame": {"x":789,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0404.png":
{
	"frame": {"x":884,"y":2,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0405.png":
{
	"frame": {"x":192,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0406.png":
{
	"frame": {"x":287,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0407.png":
{
	"frame": {"x":382,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0408.png":
{
	"frame": {"x":477,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0501.png":
{
	"frame": {"x":572,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0502.png":
{
	"frame": {"x":667,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0503.png":
{
	"frame": {"x":762,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0504.png":
{
	"frame": {"x":857,"y":98,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0505.png":
{
	"frame": {"x":192,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0506.png":
{
	"frame": {"x":192,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0507.png":
{
	"frame": {"x":192,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0508.png":
{
	"frame": {"x":192,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0601.png":
{
	"frame": {"x":192,"y":578,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0602.png":
{
	"frame": {"x":192,"y":674,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0603.png":
{
	"frame": {"x":192,"y":770,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0604.png":
{
	"frame": {"x":192,"y":866,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0605.png":
{
	"frame": {"x":287,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0606.png":
{
	"frame": {"x":382,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0607.png":
{
	"frame": {"x":477,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0608.png":
{
	"frame": {"x":572,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0701.png":
{
	"frame": {"x":667,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0702.png":
{
	"frame": {"x":762,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0703.png":
{
	"frame": {"x":857,"y":194,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0704.png":
{
	"frame": {"x":287,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0705.png":
{
	"frame": {"x":287,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0706.png":
{
	"frame": {"x":287,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0707.png":
{
	"frame": {"x":287,"y":578,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0708.png":
{
	"frame": {"x":287,"y":674,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0801.png":
{
	"frame": {"x":287,"y":770,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0802.png":
{
	"frame": {"x":287,"y":866,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0803.png":
{
	"frame": {"x":382,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0804.png":
{
	"frame": {"x":477,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0805.png":
{
	"frame": {"x":572,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0806.png":
{
	"frame": {"x":667,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0807.png":
{
	"frame": {"x":762,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0808.png":
{
	"frame": {"x":857,"y":290,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0901.png":
{
	"frame": {"x":382,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0902.png":
{
	"frame": {"x":382,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0903.png":
{
	"frame": {"x":382,"y":578,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0904.png":
{
	"frame": {"x":382,"y":674,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0905.png":
{
	"frame": {"x":382,"y":770,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0906.png":
{
	"frame": {"x":382,"y":866,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0907.png":
{
	"frame": {"x":477,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_0908.png":
{
	"frame": {"x":572,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1001.png":
{
	"frame": {"x":667,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1002.png":
{
	"frame": {"x":762,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1003.png":
{
	"frame": {"x":857,"y":386,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1004.png":
{
	"frame": {"x":477,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1005.png":
{
	"frame": {"x":477,"y":578,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1006.png":
{
	"frame": {"x":477,"y":674,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1007.png":
{
	"frame": {"x":477,"y":770,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1008.png":
{
	"frame": {"x":477,"y":866,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1101.png":
{
	"frame": {"x":572,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1102.png":
{
	"frame": {"x":667,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1103.png":
{
	"frame": {"x":762,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1104.png":
{
	"frame": {"x":857,"y":482,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1105.png":
{
	"frame": {"x":572,"y":578,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1106.png":
{
	"frame": {"x":572,"y":674,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1107.png":
{
	"frame": {"x":572,"y":770,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
},
"Equip_1108.png":
{
	"frame": {"x":572,"y":866,"w":93,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":93,"h":94},
	"sourceSize": {"w":93,"h":94}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "Atlas_Icon_Equip.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:e5808f3e7fde3a698f923d4324a9c0bf:93f8b9965eaf4a22811e858be72bcebd:d51d11a62b7ee8fd07e5ca41dd94cf19$"
}
}
