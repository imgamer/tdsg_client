﻿using UnityEngine;
using System.Collections;

public class TempController
{
    public SkeletonAnimation skeletonAnimation;
    private string _current;

    public TempController(GameObject go)
    {
        if (go == null)
        {
            return;
        }
        skeletonAnimation = go.GetComponent<SkeletonAnimation>();
        if (skeletonAnimation == null)
        {
            Printer.LogError("Animation Component not found");
        }
    }

    /// <summary>
    /// Play the animation.
    /// </summary>
    /// <param name="ani">动画名</param>
    /// <param name="loop">是否循环</param>
    public void PlayAnimation(string ani, bool loop)
    {
        if(ani == _current )
        {
            return;
        }
        skeletonAnimation.state.SetAnimation(0, ani, loop);
        _current = ani;
    }
}

