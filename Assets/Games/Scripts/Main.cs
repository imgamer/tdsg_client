﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 游戏入口
/// </summary>
public enum PublishMode
{
    Customer,    //用户模式
    Developer,   //开发者模式
}
public class Main : MonoBehaviour
{
    private static Main _instance;

    public PublishMode m_buildMode = PublishMode.Customer;

    public KBEngine.DEBUGLEVEL m_debuglevel = KBEngine.DEBUGLEVEL.ERROR;

	public static Main instance
    {
        get
        {
            return _instance;
        }
    }

    private void CheckSingleton()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy( this.gameObject );
            Printer.LogWarning("There is already an instance of Main created (" + _instance.name + "). Destroying new one.");
        }
    }

	void Awake()
	{
        CheckSingleton();
	}

	void Start()
	{
        Object.DontDestroyOnLoad(transform.gameObject);
        Printer.debugLevel = this.m_debuglevel;
		Printer.Log("Main:Start.");
        GameMain.instance.Start();
	}

	void OnDestroy()
	{
        Printer.Log("Main::OnDestroy");
		GameMain.instance.Destroy();
	}

}
