namespace KBEngine
{
  	using UnityEngine; 
	using System; 
	using System.Collections; 
	using System.Collections.Generic;
	
    public class Monster : KBEngine.GameObject  
    {
        //指示是否一创建entity就在进入场景
        protected bool enterSceneOnBorn_ = true;

        public override void onEnterWorld()
        {
            base.onEnterWorld();
            //初始化怪物的技能
            NpcMonsterData monster = NpcMonsterConfig.SharedInstance.GetNpcMonsterData(UType);
            if (monster != null)
            {
                foreach (int skillId in monster.skills)
                {
                    onAddSkill(skillId);
                }
               
            }

            if (enterSceneOnBorn_)
            {
                SceneManager.instance.OnEnterScene(this);
            }
        }

        public override void onLeaveWorld()
        {
            base.onLeaveWorld();
            SceneManager.instance.OnLeaveScene(this);
        }



    }

} 
