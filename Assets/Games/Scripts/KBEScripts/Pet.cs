namespace KBEngine
{
	using UnityEngine; 
	using System; 
	using System.Collections; 
	using System.Collections.Generic;
	
	public class Pet : KBEngine.GameObject  
	{
        public override void onEnterWorld()
        {
            base.onEnterWorld();
            cellCall("requestPull", new object[] { });
            SceneManager.instance.OnEnterScene(this);
        }

        public override void onLeaveWorld()
        {
            base.onLeaveWorld();
            SceneManager.instance.OnLeaveScene(this);
        }

        public override bool CanDie()
        {
            return false;
        }

	}
	
} 
