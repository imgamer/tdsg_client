using UnityEngine;

using System;
using System.Collections;

using LitJson;
using KBEngine;

public class clientapp : KBEMain 
{
	private String _loginIP = "172.16.2.234";
	private UInt16 _loginPort = 20013;

	private void LoadServerInfo()
	{
		Printer.Log("GameMgr :: LoadServerInfo");
		string pathUrl = string.Empty;
#if UNITY_EDITOR
		pathUrl = string.Format("file://{0}/StreamingAssets/ServerInfo", Application.dataPath);
		Printer.Log("This is Windows platform ");
#else
        if (Application.platform == RuntimePlatform.OSXEditor ||
            Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.WindowsEditor)
        {
		    pathUrl = string.Format("file://{0}/StreamingAssets/ServerInfo", Application.dataPath);
		    Printer.Log("This is Windows platform ");            
        }
        else
        {
#if UNITY_ANDROID
        pathUrl = string.Format("jar:file://{0}!/assets/ServerInfo", Application.dataPath);
		Printer.Log("This is Android platform");
#elif UNITY_IPHONE
		pathUrl = string.Format("file://{0}/ServerInfo", Application.streamingAssetsPath, urlName);
		Printer.Log("This is Apple platform");
#endif
        }
#endif
		string info = "";
		try
		{
			WWW mywww = new WWW(pathUrl);
			while (!mywww.isDone)
			{
				if (mywww.error != null) return;
			}
			info = mywww.text;
			mywww.Dispose();
			Printer.Log("Current Path is:" + pathUrl);
		}
		catch (Exception e)
		{
			Printer.LogError("GameMgr::LoadServerInfo:open Path: " + pathUrl + " error:" + e.ToString());
			return;
		}

		JsonData jdata = JsonMapper.ToObject(info);
		_loginIP = Convert.ToString(jdata["ip"]);
		_loginPort = (UInt16)jdata["port"];
	}

	public override void initKBEngine()
	{
		// 如果此处发生错误，请查看 Assets\Scripts\kbe_scripts\if_Entity_error_use______git_submodule_update_____kbengine_plugins_______open_this_file_and_I_will_tell_you.cs
		KBEngineArgs args = new KBEngineArgs();

		LoadServerInfo();
		args.ip = _loginIP;
		args.port = _loginPort;
		args.clientType = clientType;

		if (persistentDataPath == "Application.persistentDataPath")
		{
			args.persistentDataPath = Application.persistentDataPath;
			Printer.LogWarning("Application.persistentDataPath:" + Application.persistentDataPath);
		}
		else
			args.persistentDataPath = persistentDataPath;

		args.syncPlayer = syncPlayer;
		args.threadUpdateHZ = threadUpdateHZ;

		args.SEND_BUFFER_MAX = (UInt32)SEND_BUFFER_MAX;
		args.RECV_BUFFER_MAX = (UInt32)RECV_BUFFER_MAX;

		args.isMultiThreads = isMultiThreads;

		if (isMultiThreads)
			gameapp = new KBEngineAppThread(args);
		else
			gameapp = new KBEngineApp(args);
	}
}
