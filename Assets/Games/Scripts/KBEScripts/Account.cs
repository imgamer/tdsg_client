namespace KBEngine
{
    using UnityEngine;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using DBID_TYPE = System.UInt64;

    public class Account : KBEngine.GameObject
    {
        public DBID_TYPE LastSelCharacter
        {
            get { return (DBID_TYPE)getDefinedPropterty("lastSelCharacter"); }
        }

        public override void __init__()
        {
            LoginManager.instance.SetAccount(this);
            baseCall("reqAvatarList", new object[0]);
        }

        /// <summary>
        /// 请求角色列表回调
        /// </summary>
        /// <param name="infos">Infos.</param>
        public void onReqAvatarList(List<object> listinfos)
        {
            LoginManager.instance.InitRoles(listinfos);
        }

        public void onCreateAvatarResult(Byte retcode, object info)
        {
            LoginManager.instance.AddRole(retcode, info);
        }

        public void onRemoveAvatar(DBID_TYPE dbid)
        {
            LoginManager.instance.RemoveRole(dbid);
        }

        public void reqCreateAvatar(UInt32 feature, string name)
        {
            baseCall("reqCreateAvatar", new object[] { feature, name });
        }

        public void selectAvatarForGame(DBID_TYPE dbid)
        {
            baseCall("selectAvatarForGame", new object[] { dbid });
        }

        public void reqRemoveAvatar(DBID_TYPE dbid)
        {
            baseCall("reqRemoveAvatar", new object[] { dbid });
        }

        /// <summary>
        /// 服务器向客户端同步时间
        /// </summary>
        /// <param name="time">Time.</param>
        public void onSynchronizeServerTime(double time)
        {
            Printer.Log("Account::onSynchronizeServerTime:" + time.ToString());
            TimeUtils.SyncFromServer(time);
        }

        public void statusMessage(UInt16 statusID, string value)
        {
            Printer.LogError("Account::statusMessage: statusID = " + statusID + ", value" + value);
            TipManager.instance.ShowTextTip(statusID, value);
        }

    }
}
