namespace KBEngine
{
  	using UnityEngine; 
	using System; 
	using System.Collections; 
	using System.Collections.Generic;
	
    public class NPC : KBEngine.GameObject
    {
        //指示是否一创建entity就在进入场景
        protected bool enterSceneOnBorn_ = true;

        public override void onEnterWorld()
        {
            UType = (UInt32)getDefinedPropterty("utype");
            Name = (string)getDefinedPropterty("name");
            ModelID = (string)getDefinedPropterty("modelID");

            if (enterSceneOnBorn_)
            {
                SceneManager.instance.OnEnterScene(this);
            }
        }

        public override void onLeaveWorld()
        {
            base.onLeaveWorld();
            SceneManager.instance.OnLeaveScene(this);
        }
    }
    
} 
