namespace KBEngine
{
	using UnityEngine; 
	using System;
	using System.Reflection;
	using System.Collections;
	using System.Collections.Generic;
	using LitJson;

	using DBID = System.UInt64;
	using ITEM_ORDER = System.Int16;
	using EQUIPMENT_LOCATION = System.Byte;
	using CLIENT_ITEM = System.Collections.Generic.Dictionary<string, object>;

	//好友相关
	using CLIENT_FRIEND = System.Collections.Generic.Dictionary<string, object>;
	using CLIENT_REQUEST = System.Collections.Generic.Dictionary<string, object>;
	using AVATAR_STATUS_INFO = System.Collections.Generic.Dictionary<string, object>;
	using AVATAR_LEVEL_INFO = System.Collections.Generic.Dictionary<string, object>;
	//using ARRAY = System.Collections.Generic.List<object>;


	//建筑
	using BUILDING_ID_TYPE = System.Int32;
	using BUILDING_ORDER_TYPE = System.Int16;
	using BUILDING_LOCATION_TYPE = UnityEngine.Vector2;

    using EQUIPMENT_BAG_TYPE = System.Collections.Generic.Dictionary<EQUIPMENT_LOCATION, Item.ItemEquipment>;

	//战斗
	using ROUND_FIGHT_DATAS = System.Collections.Generic.List<object>;
	using OPERATION_FIGHT_DATA = System.Collections.Generic.Dictionary<string, object>;

    //商城出售道具
    using SHOP_SELL_ITEMS = System.Collections.Generic.List<object>;
    using SELL_ITEM = System.Collections.Generic.Dictionary<string, object>;

    //摆摊道具
    using TRADE_SELL_ITEM = System.Collections.Generic.Dictionary<string, object>;

    using ENTITY_UTYPE = System.UInt32;

    public class Avatar : KBEngine.GameObject
    {
        public DBID DatabaseID { get; private set; }    // dbid

        public Int32 EXP { get; private set; } //当前经验
        //仙玉或元宝
        public Int32 Fairyjade { get; private set; }
        //金币
        public Int32 Gold { get; private set; }
        //银币
        public Int32 Silver { get; private set; }

        public Int32 ArenaMedal { get; private set; } //竞技场勋章

        public Int32 PataCoin { get; private set; } //爬塔币

        public Int32 Chivalrous { get; private set; } //侠义值

        public UInt16 School { get; private set; } //门派 
        public UInt32 Score { get; private set; } //评分

        public bool AutoMode { get; private set; } //操作模式   
        public Byte TeamStatus { get; private set; } //组队状态
        public DBID FightingPetDBID { get; private set; } //出战宠物DBID

        public UInt16 FreePoint { get; private set; }   //剩余自由点数

        public Dictionary<string, UInt32> FreePointAssignment { get; private set; }   //自由点分配

        public string TongName { get; private set; }    //帮会名
        public byte TongDuty { get; private set; }    //帮会职务

        //助战
        public HeroModule heroModule { get; private set; }

        //邮件
        public MailModule mailModule { get; private set; }

        //聊天
        public ChatModule chatModule { get; private set; }

        //对话
        public TalkModule talkModule { get; private set; }

        //背包
        public Item.KitbagModule kitbagModule { get; private set; }

        //装备
        public EquipModule equipModule { get; private set; }

        //技能
        public SkillModule skillModule { get; private set; }

        //好友
        public FriendModule friendModule { get; private set; }

        //任务
        public TaskModule taskModule { get; private set; }

        //组队
        public TeamModule teamModule { get; private set; }
        
        //帮会
        public TongModule tongModule { get; private set; }

        //商城
        public ShopModule shopModule { get; private set; }

        //摆摊
        public PitchModule pitchModule { get; private set; }

        //宠物
        public PetModule petModule { get; private set; }

        //活动
        public ActivityModule activityModule { get; private set; }

        //奖励
        public RewardModule rewardModule { get; private set; }

        //奴隶
        public SlaveModule slaveModule { get; private set; }
  
        //排行榜
        public RankModule rankModule { get; private set; }

        //竞技场
        public ArenaModule arenaModule { get; private set; }

        //跨场景寻路
        public SpaceNavigator spaceNavigator { get; private set; }

        public TeamCopyModule teamCopyModule { get; private set; }
        public override void onEnterWorld()
        {
            base.onEnterWorld();
            Level = (UInt16)getDefinedPropterty("level");
            TeamStatus = Convert.ToByte(getDefinedPropterty("teamingStatus"));
			if (!isPlayer()) 
			{
				SceneManager.instance.OnEnterScene(this);
				return;
			}
			GameMain.instance.SetSceneAvatar(this);

            mailModule = new MailModule(this);
            chatModule = new ChatModule(this);
            talkModule = new TalkModule(this);
            friendModule = new FriendModule(this);
            kitbagModule = new Item.KitbagModule(this);
            equipModule = new EquipModule(this);
            skillModule = new SkillModule(this);
            taskModule = new TaskModule(this);
            teamModule = new TeamModule(this);
            tongModule = new TongModule(this);
            shopModule = new ShopModule(this);
            pitchModule = new PitchModule(this);
            petModule = new PetModule(this);
            activityModule = new ActivityModule(this);
            rewardModule = new RewardModule(this);
            slaveModule = new SlaveModule(this);
            rankModule = new RankModule(this);
            arenaModule = new ArenaModule(this);
            spaceNavigator = new SpaceNavigator(this);
            heroModule = new HeroModule(this);
            teamCopyModule = new TeamCopyModule(this);

            EXP = (Int32)getDefinedPropterty("AvatarEXP");
            DatabaseID = (DBID)(getDefinedPropterty("dbid"));
            School = Convert.ToUInt16(getDefinedPropterty("school"));
            Score = (UInt32)getDefinedPropterty("score");
            AutoMode = Convert.ToBoolean(getDefinedPropterty("autoMode"));
            FightingPetDBID = (DBID)getDefinedPropterty("fightingPetDBID");
            ArenaMedal = (Int32)getDefinedPropterty("arenaMedal");
            PataCoin = (Int32)getDefinedPropterty("pataCoin");
            Chivalrous = (Int32)getDefinedPropterty("chivalrous");
            FreePoint = (UInt16)getDefinedPropterty("freePoint");

            FreePointAssignment = new Dictionary<string, uint>();
            set_freePointAssignment(null);

            baseCall("onClientAvatarInitFinished", new object[] { });
            baseCall("requestWareHouse", new object[] { });
            cellCall("requestKitbag", new object[] { });
            cellCall ("requestPull", new object[]{});
            baseCall("requestShopSellItems", new object[] { 1 });
            baseCall("requestShopSellItems", new object[] { 2 });
            baseCall("requestShopSellItems", new object[] { 3 });
            cellCall("requestPublicSellItems", new object[] { });
            cellCall("requestTradeSellItems", new object[] { });
        }

        public override void onLeaveWorld()
        {
            base.onLeaveWorld();
            SceneManager.instance.OnLeaveScene(this);
        }

        public override void onEnterSpace()
        {
            base.onEnterSpace();
            SceneManager.instance.OnEnterScene(this);
        }
        
        public override void onLeaveSpace()
        {
            base.onLeaveSpace();
            SceneManager.instance.OnLeaveScene(this);
            SceneManager.instance.SetSceneUnavailable();
        }

        public override bool CanDie()
        {
            return false;
        }

        public void chat_receiveMessage(int channelID, string senderName, string message)
        {
            chatModule.AddChatData(channelID, senderName, message);
        }  
      
        public void receiveRankingDatas(UInt16 rankType, UInt16 beginIndex, List<object> rankDatas)
        {
            rankModule.ReceiveRankDatas(rankType, beginIndex, rankDatas);
        }

        /// <summary>
        /// 传送到指定地图的出生点
        /// </summary>
        /// <param name="spaceUType"></param>
        public void TeleportSpaceSpawnPoint(int spaceUType)
        {
            cellCall("teleportSpawnPoint", (UInt32)spaceUType);
        }

       #region AvatarAttribute
        public override void set_name(object old)
        {
            base.set_name(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_level(object old)
        {
            base.set_level(old);
            if (SceneManager.instance.IsSceneAvailable)
            {
                TipManager.instance.ShowTextTip(Define.COMMON_LEVEL_UP, Convert.ToString(Level));
            }           
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public virtual void set_AvatarEXP(object old)
        {
            object v = getDefinedPropterty("AvatarEXP");
            if (SceneManager.instance.IsSceneAvailable)
            {
                int differNum = (Int32)v - EXP;
                if (differNum > 0)
                {
                    string tip = string.Format("{0},{1},{2}", "经验", "增加", differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
            }
            EXP = (Int32)v;

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public virtual void set_school(object old)
        {
            object v = getDefinedPropterty("school");
            School = Convert.ToUInt16(v);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public virtual void set_score(object old)
        {
            object v = getDefinedPropterty("score");
            Score = (UInt32)v;

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_force(object old)
        {
            base.set_force(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_vitality(object old)
        {
            base.set_vitality(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_spirituality(object old)
        {
            base.set_spirituality(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_gengu(object old)
        {
            base.set_gengu(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_attackSpeed(object old)
        {
            base.set_attackSpeed(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_HP(object old)
        {
            base.set_HP(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_MP(object old)
        {
            base.set_MP(old);
            if (isPlayer())
            {
                EventManager.Invoke<Int32>(EventID.EVNT_PLAYER_ANGER_UPDATE, MP);
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_HP_Max(object old)
        {
            base.set_HP_Max(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_MP_Max(object old)
        {
            base.set_MP_Max(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_physicsPower(object old)
        {
            base.set_physicsPower(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_magicPower(object old)
        {
            base.set_magicPower(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_hit(object old)
        {
            base.set_hit(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_dodge(object old)
        {
            base.set_dodge(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_physicsDefense(object old)
        {
            base.set_physicsDefense(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_magicDefense(object old)
        {
            base.set_magicDefense(old);
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_crit(object old)
        {
            base.set_crit(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }
        public override void set_toughness(object old)
        {
            base.set_toughness(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_escapeRate(object old)
        {
            base.set_escapeRate(old);
        }

        public override void set_treatmentStrength(object old)
        {
            base.set_treatmentStrength(old);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        public override void set_barriersShootingSkill(object old)
        {
            base.set_barriersShootingSkill(old);
        }

       #endregion

        #region ArenaModule
        public void onReceiveCurrentRankingAndRival(UInt32 currentRanking, List<object> rivalDatas)
        {
            arenaModule.ReceiveCurrentRankingAndRival(currentRanking, rivalDatas);
        }

        public void receiveFightRecordDynamic(string challengerName, string rivalName, UInt32 challengerCurrentRanking)
        {
            arenaModule.ReceiveFightRecordDynamic(challengerName, rivalName, challengerCurrentRanking);
        }

        public void onFightResultRecords(List<object> fightResultRecords)
        {
            arenaModule.ReceiveFightResultRecords(fightResultRecords);
        }

        public void onRemainChallengeCount(UInt16 remainChallengeCount)
        {
            arenaModule.RemainChallengeCount(remainChallengeCount);
        }
        public void onBuyChallengeCountNeedIngot(Int32 needCostIngot)
        {
            arenaModule.BuyChallengeCountNeedIngot(needCostIngot);
        }

        public void onChallengeCDOverTime(Int32 cdTime)
        {
            arenaModule.ChallengeCDOverTime(cdTime);
        }

        public virtual void set_arenaMedal(object old)
        {
            object v = getDefinedPropterty("arenaMedal");
            ArenaMedal = (Int32)v;
            EventManager.Invoke<object>(EventID.EVNT_MEDAL_UPDATE, null);
        }

        public virtual void set_pataCoin(object old)
        {
            object v = getDefinedPropterty("pataCoin");
            PataCoin = (Int32)v;
        }

        public virtual void onCurrentMedalCanToReceive(UInt32 medals)
        {
            arenaModule.CurrentMedalCanToReceive(medals);
        }

        public virtual void onHandLosers(List<object> handLosers)
        {
            slaveModule.OnHandLosers(handLosers);
        }

        public virtual void receiveArenaShopSellItems(List<object> items)
        {
            arenaModule.ReceiveArenaShopSellItems(items);
        }

        #endregion

        #region Pata
        public virtual void onWhetherRelive(UInt16 statusID, string value)
        {
            TipManager.instance.ShowTwoButtonTip(statusID, value,
            () =>
            {
                if (Fairyjade < Convert.ToInt32(value))
                {
                    TipManager.instance.ShowTextTip(Convert.ToInt32(StatusID.ARENA_INGOT_NOT_ENOUGH));
                    return false;
                }   
                cellCall("confirmWhetherRelive", new object[]{ 1 });
                return true;
            },
            () =>
            {
                cellCall("confirmWhetherRelive", new object[]{ 0 });
                return true;
            });   
        }
        #endregion

        public void onFightResult(object win, List<object> paramList)
        {
            List<string> nparamList = new List<string>();
            foreach (object param in paramList)
            {
                nparamList.Add(Convert.ToString(param));
            }
            SceneManager.instance.CurrentScene.CurrentStage.OnFightResult(Convert.ToBoolean(win), nparamList);
        }
        
        public void stageStart(UInt16 stageindex)
        {
            EventManager.Invoke<UInt16>(EventID.EVNT_ROOM_STAGE_START, stageindex);
        }

        public void stageOver()
        {
            EventManager.Invoke<object>(EventID.EVNT_ROOM_STAGE_OVER, null);
        }

    #region SlaveModule   

        public virtual void onCaptureCDOverTime(Int32 cdTime)
        {
            slaveModule.OnCaptureCDOverTime(cdTime);
        }

        public virtual void onRemainCaptureCount(UInt16 remainCaptureCount)
        {
            slaveModule.OnRemainCaptureCount(remainCaptureCount);
        }

        public virtual void onSqueezeCDOverTime(Int32 cdTime)
        {
            slaveModule.OnSqueezeCDOverTime(cdTime);
        }

        public virtual void onRemainSqueezeCount(UInt16 remainSqueezeCount)
        {
            slaveModule.OnRemainSqueezeCount(remainSqueezeCount);
        }

        public virtual void onRemainPlacateCount(UInt16 remainPlacateCount)
        {
            slaveModule.OnRemainPlacateCount(remainPlacateCount);
        }

        public virtual void onIdentityAndRelationAvatar(sbyte avatarIdentity, List<object> slavesInfo, List<object> lordInfo, List<object> slavesToGetRidOfRemainTime)
        {
            slaveModule.OnIdentityAndRelationAvatar(avatarIdentity, slavesInfo, lordInfo, slavesToGetRidOfRemainTime);
        }

        public virtual void onLordLogRecords(List<object> lordLogRecords)
        {
            slaveModule.OnLordLogRecords(lordLogRecords);
        }

    #endregion

        public void set_autoMode(object old)
        {
            object v = getDefinedPropterty("autoMode");
            AutoMode = Convert.ToBoolean(v);
            if (isPlayer())
            {
                EventManager.Invoke<bool>(EventID.EVNT_PLAYER_MODE_UPDATE, AutoMode);
            }
        }

        protected override void SetFightState(sbyte v)
        {
            base.SetFightState(v);
            if (isPlayer())
            {
                EventManager.Invoke<sbyte>(EventID.EVNT_PLAYER_FIGHTSTATE_UPDATE, v);
            }
        }

        public void startFight()
        {
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_FIGHT_START, null);
            }
        }

        public void overFight()
        {
            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_FIGHT_END, null);
            }
        }

        public void skillShowResult(List<object> skillShowResultDataList)
        {
            SkillManager.instance.RecieveFightData(skillShowResultDataList);
        }

        public void skillShowStart(Int32 entityId)
        {
            EventManager.Invoke<Int32>(EventID.EVNT_ROUND_START, entityId);
            SkillManager.instance.OnFightStart();
        }

        public void skillShowOver(Int32 entityId)
        {
            EventManager.Invoke<Int32>(EventID.EVNT_ROUND_END, entityId);
        }

       #region AvatarPlusPoint
        public void set_freePoint(object old)
        {
            FreePoint = Convert.ToUInt16(getDefinedPropterty("freePoint"));
            Printer.Log("Set free point to {0}", FreePoint);
        }

       //当前等级最大可以使用自由点，以后改为配置读取
        public int MaxFreePoint
        {
            get { return 200; }
        }
        public void set_freePointAssignment(object old)
        {
            Printer.Log("Set freePointAssignment.");

            Dictionary<string, object> assignment = getDefinedPropterty("freePointAssignment") as Dictionary<string, object>;
            FreePointAssignment["force"] = Convert.ToUInt32(assignment["force"]);
            FreePointAssignment["vitality"] = Convert.ToUInt32(assignment["vitality"]);
            FreePointAssignment["spirituality"] = Convert.ToUInt32(assignment["spirituality"]);
            FreePointAssignment["gengu"] = Convert.ToUInt32(assignment["gengu"]);

            if (isPlayer())
            {
                EventManager.Invoke<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, null);
            }
        }

        //重置自由加点
        public void ReqResetFreePoint()
        {
            cellCall("resetFreePoint");
        }

        //同步自由点属性
        public void SyncFreePointAssignmentToServer(int force, int vitality, int spirituality, int gengu)
        {
            Dictionary<string, object> assignment = new Dictionary<string, object>();
            assignment["force"] = (UInt32)force;
            assignment["spirituality"] = (UInt32)spirituality;
            assignment["vitality"] = (UInt32)vitality;
            assignment["gengu"] = (UInt32)gengu;
            cellCall("assignFreePoint", assignment);
        }

        public UInt32 BaseForce
        {
            get { return Force - (UInt32)FreePointAssignment["force"]; }
        }

        public UInt32 BaseSpirituality
        {
            get { return Spirituality - (UInt32)FreePointAssignment["spirituality"]; }
        }

        public UInt32 BaseVitality
        {
            get { return Vitality - (UInt32)FreePointAssignment["vitality"]; }
        }

        public UInt32 BaseGengu
        {
            get { return Gengu - (UInt32)FreePointAssignment["gengu"]; }
        }
       #endregion



        #region Shop
        public void receiveSellItems(SByte sellType, SHOP_SELL_ITEMS sellItemDatas)
        {
            shopModule.ReceiveSellItems(sellType, sellItemDatas);
        }

        public void addPublicSellItem(TRADE_SELL_ITEM tradeSellItemData)
        {
            pitchModule.ReceivePublicSellItemData(tradeSellItemData);
        }

        public void addTradeSellItem(TRADE_SELL_ITEM tradeSellItemData)
        {
            pitchModule.ReceiveTradeSellItemData(tradeSellItemData);
        }

        public void removePublicSellItem(Int64 tradeUid)
        {
            pitchModule.RemovePublicSellItem(tradeUid);
        }

        public void removeTradeSellItem(Int64 tradeUid)
        {
            pitchModule.RemoveTradeSellItem(tradeUid);
        }
        #endregion

        public void judgeThunderResult(object success)
        {
            SceneManager.instance.CurrentScene.JudgeThunderResult(Convert.ToBoolean(success));
        }

        public void currentFightStageCount(int fightStageCount, string fightStageScript)
        {
            SceneManager.instance.CurrentScene.CurrentFightStageCount(fightStageCount, fightStageScript);
        }

        public void statusMessage(UInt16 statusID, string value)
        {
            TipManager.instance.ShowTextTip(statusID, value);
        }

        public void enterRoom(UInt16 roomType)
        {
            SceneManager.instance.EnterScene(roomType);
        }

        public void npcCreateOver()
        {
            EventManager.Invoke<object>(EventID.EVNT_ON_NPCCREATE_OVER, null);
        }

        public void updateSkillPiece(UInt16 skillPiece)
        {

        }

        public override void onUpdateVolatileData()
        {
            if (SceneEntityObj != null)
            {
                SceneEntityObj.Navigate.Start(BaseNavigate.NavigateType.Passive, Position);
            }
        }

        public override void set_position(object old)
        {
            base.set_position(old);
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SetPosition(Position);
            }
        }

        public override void set_direction(object old)
        {
            base.set_direction(old);
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SetDirection(this.direction);
            }
        }

        #region avatar equipment
        public void recvEquipmentBag(Dictionary<string, object> data)
        {
            equipModule.recvEquipmentBag(data);
            }

        public void putOnEquipment(ITEM_ORDER order)
        {
            equipModule.putOnEquipment(order);  
            }

        public void takeOffEquipment(EQUIPMENT_LOCATION location)
        {
            equipModule.takeOffEquipment(location);
            }

        public void updateEquipmentAttrInt32(EQUIPMENT_LOCATION location, Int32 attrIndex, Int32 value)
        {
            equipModule.updateEquipmentAttrInt32(location, attrIndex, value);
            }

        public void updateEquipmentAttrString(EQUIPMENT_LOCATION location, Int32 attrIndex, string value)
        {
            equipModule.updateEquipmentAttrString(location, attrIndex, value);
            }

        public void onItemAttrUpdated_dict(ITEM_ORDER order, Int32 itemAttrsIndex, string value)
        {
            equipModule.onItemAttrUpdatedDict(order, itemAttrsIndex, value);
        }

        public void updateEquipmentAttrDict(EQUIPMENT_LOCATION location, Int32 itemAttrsIndex, string value)
        {
            equipModule.updateEquipmentAttrDict(location, itemAttrsIndex, value);
        }

        #endregion

        #region MakePoint
        public void updateMakePoint(Int32 makePoint)
        {

        }
        public void AddMakePoint(int makePoint)
        {
            SetMakePoint(GetMakePoint() + makePoint);
        }

        public void SetMakePoint(int makePoint)
        {

        }

        public int GetMakePoint()
        {
            return 0;
        }
        #endregion

        #region hero operations
        public void recvRecruitHeroData(List<object> heroUType)
        {
            heroModule.RecvRecruitHeroData(heroUType);
        }

        public void recvHeroData(Dictionary<string, object> heroData)
        {
            heroModule.RecvHeroData(heroData);
        }

        public void updateHeroProperty(ENTITY_UTYPE utype, string attrName, string json)
        {
            heroModule.UpdateHeroPropertyJson(utype, attrName, json);
        }

        public void updateHeroPropertyInt32(ENTITY_UTYPE utype, string attrName, Int32 value)
        {
            heroModule.UpdateHeroPropertyInt32(utype, attrName, value);
        }

        public void updateHeroPropertyFloat(ENTITY_UTYPE utype, string attrName, float value)
        {
            heroModule.UpdateHeroPropertyFloat(utype, attrName, value);
        }

        public void updateHeroPropertyString(ENTITY_UTYPE utype, string attrName, string value)
        {
            heroModule.UpdateHeroPropertyString(utype, attrName, value);
        }

        public void updateHeroPropertyListInt(ENTITY_UTYPE utype, string attrName, UInt16 index, Int32 newValue)
        {
            heroModule.UpdateHeroPropertyListInt(utype, attrName, index, newValue);
        }
        #endregion
	
		#region pet operations
        /// <summary>
        /// 参战宠物改变通知
        /// </summary>
        /// <param name="old">Old.</param>
        public void set_fightingPetDBID(object old)
        {
            FightingPetDBID = (DBID)getDefinedPropterty("fightingPetDBID");
            petModule.SetFightingPetDBID(old);
        }
		public void recvPetData(Dictionary<string, object> petData)
		{
            petModule.RecvPetData(petData);
		}
		
		public void updatePetPropertyJson(DBID dbid,  string attrName, string json)
		{
            petModule.UpdatePetPropertyJson(dbid, attrName, json);
		}
		
		public void updatePetPropertyInt32(DBID dbid, string attrName, Int32 value)
		{
            petModule.UpdatePetPropertyInt32(dbid, attrName, value);
		}
		
		public void updatePetPropertyFloat(DBID dbid, string attrName, float value)
		{
            petModule.UpdatePetPropertyFloat(dbid, attrName, value);
		}
		
		public void updatePetPropertyString(DBID dbid, string attrName, string value)
		{
            petModule.UpdatePetPropertyString(dbid, attrName, value);
		}
		
		public void updatePetPropertyListInt(DBID dbid, string attrName, UInt16 index, Int32 newValue)
		{
            petModule.UpdatePetPropertyListInt(dbid, attrName, index, newValue);
		}

		public void onPetFreed(DBID dbid)
		{
			petModule.OnPetFreed(dbid);
		}

        public void onPetRejuvenated(DBID dbid, Dictionary<string, object> data)
        {
            petModule.OnPetRejuvenated(dbid, data);
        }

        public void onPetCombined(DBID dbid)
        {
            petModule.OnPetCombined(dbid);
        }
		#endregion

        #region kitbag
        public void onItemAttrUpdated_int(ITEM_ORDER order, Int32 attrIndex, Int32 value)
        {
            if (kitbagModule.UpdateItemAttribute(order, attrIndex, value))
            {
                Printer.Log(String.Format("Avatar::onItemAttrUpdated_int: {0}.", order));
        	}	
        }

        public void onItemAttrUpdated_str(ITEM_ORDER order, Int32 attrIndex, string value)
        {
	        
        }

        public void onAddItem(CLIENT_ITEM itemInfo)
        {
            Item.ItemBase item = Item.ItemFactory.CreateItem(itemInfo);

            if (item != null)
            {
                kitbagModule.AddItem(item);
            }
        }

        public void onRemoveItem(ITEM_ORDER order)
        {
            Printer.Log(String.Format("Avatar::onRemoveItem: {0}.", order));

            kitbagModule.RemoveItemByOrder(order);
        }

        public void onSwapItem(ITEM_ORDER orderA, ITEM_ORDER orderB)
        {

        }

        public void onAddItemToWarehouse(CLIENT_ITEM itemInfo)
        {
            Item.ItemBase item = Item.ItemFactory.CreateItem(itemInfo);
            kitbagModule.AddWarehouseItem(item);
        }

        public void onRemoveItemToWarehouse(ITEM_ORDER order)
        {
            kitbagModule.RemoveWarehouseItemByOrder(order);
        }

       
        #endregion

        #region Friend
        public void sendFriendsImformationToClient(CLIENT_FRIEND friendInfo)
        {
            friendModule.InitFriendInfoFromSever(friendInfo);
        }

        public void sendAllRequestToClient(CLIENT_REQUEST requestInfo)
        {
            friendModule.InitRequestInfoFromSever(requestInfo);
        }

        public void sendFriendRequestToClient(CLIENT_REQUEST newRequestInfo)
        {
            friendModule.AddNewRequest(newRequestInfo);
        }

        public void updateLevelToClient(AVATAR_LEVEL_INFO levelInfo)
        {
            friendModule.UpdateLevelInfo(levelInfo);
        }

        public void updateStatusToClient(AVATAR_STATUS_INFO statusInfo)
        {
            friendModule.UpdateStatusInfo(statusInfo);
        }

        public void updateNewFriendsToClient(CLIENT_FRIEND newFriendInfo)
        {
            friendModule.AddNewFriend(newFriendInfo);
        }

        public void updateDeleteFriendsToClient(DBID dbid)
        {
            friendModule.DeleteFriend(dbid);
        }

        public void sendRecommendFriendsImformationToClient(CLIENT_FRIEND recommendInfo)
        {
            friendModule.InitRecommendInfoFromSever(recommendInfo);
        }
        #endregion

        #region quest
        /// <summary>
        /// <Defined Method>
        /// 接取一个新任务
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void onAcceptQuest(uint id)
        {
            taskModule.AcceptQuest(id);
        }

        /// <summary>
        /// <Defined Method>
        /// 服务器发送一个任务实例过来，通常初始化时会调用
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void onAddQuest(Dictionary<string, object> questData)
        {
            taskModule.AddQuest(questData);
        }

        /// <summary>
        /// <Defined Method>
        /// 服务器发送已经做过的任务记录.
        /// </summary>
        /// <param name="questData">Quest data.</param>
        public void onRecvQuestLog(Dictionary<string, object> questLogData)
        {
            taskModule.RecvQuestLog(questLogData);
        }

        /// <summary>
        /// <Defined Method>
        /// 服务器通知放弃一个任务
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void onAbandonQuest(uint id)
        {
            taskModule.AbandonQuest(id);
        }

        /// <summary>
        /// <Defined Method>
        /// 完成任务时服务器通知客户端
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void onCommitQuest(uint id)
        {
            taskModule.CommitQuest(id);
        }

        /// <summary>
        /// <Defined Method>
        /// 服务器通知更新客户端任务目标
        /// </summary>
        /// <param name="questID">Quest ID.</param>
        /// <param name="taskID">Task ID.</param>
        /// <param name="progress">Task Progress.</param>
        public void onUpdateQuestTask(uint questID, ushort taskID, Int32 progress)
        {
            taskModule.UpdateQuestTask(questID, taskID, progress);
        }

        /// <summary>
        /// 移动到指定npc位置 并进行对话
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="UType"></param>
        public void onRunToNpcTalk(Vector2 dest, UInt32 UType)
        {
            taskModule.RunToNpcTalk(dest, UType);
        }

        /// <summary>
        /// 提示师门任务将获得较少奖励
        /// </summary>
        public void warnOfDecadentShimenReward()
        {
            taskModule.WarnOfDecadentShimenReward();
        }

        public void set_shimenRingIndex(object old)
        {
            taskModule.ShimenRingIndex(old);
        }

        public void set_shimenDailyCounter(object old)
        {
            taskModule.ShimenDailyCounter(old);
        }
        #endregion

        /// <summary>
        /// 提示猎魂任务将获得较少奖励
        /// </summary>
        public void warnOfDecadentLiehunReward()
        {
            taskModule.WarnOfDecadentLiehunReward();
        }

        #region buildings
        /// <summary>
        /// 增加一个新的建筑
        /// </summary>
        /// <param name="data">Data.</param>
        public void addBuilding(Dictionary<string, object> data)
        {

        }

        /// <summary>
        /// 移除建筑
        /// </summary>
        /// <param name="order">Order.</param>
        public void removeBuilding(BUILDING_ORDER_TYPE order)
        {

        }

        /// <summary>
        /// 更新建筑属性
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="attrName">Attr name.</param>
        /// <param name="value">Value.</param>
        public void updateBuildingInt32(BUILDING_ORDER_TYPE order, string attrName, Int32 value)
        {

        }

        /// <summary>
        /// 更新建筑属性
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="attrName">Attr name.</param>
        /// <param name="value">Value.</param>
        public void updateBuildingFloat(BUILDING_ORDER_TYPE order, string attrName, float value)
        {

        }

        /// <summary>
        /// 更新建筑属性
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="attrName">Attr name.</param>
        /// <param name="value">Value.</param>
        public void updateBuildingString(BUILDING_ORDER_TYPE order, string attrName, string value)
        {

        }
        #endregion

        #region 训练场
        /// <summary>
        /// 添加英雄到训练场
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="heroDbid">Hero dbid.</param>
        public void addTrainingHero(BUILDING_ORDER_TYPE order, DBID heroDbid)
        {

        }

        /// <summary>
        /// 从训练场移除一个英雄
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="heroDbid">Hero dbid.</param>
        public void removeTrainingHero(BUILDING_ORDER_TYPE order, DBID heroDbid)
        {

        }

        /// <summary>
        /// 英雄吸取经验后服务器回调
        /// </summary>
        /// <param name="order">Order.</param>
        /// <param name="heroDbid">Hero dbid.</param>
        public void onExtractHeroTrainingExp(BUILDING_ORDER_TYPE order, DBID heroDbid)
        {

        }
        #endregion

        #region VIP
        public void sendVipItemsToClient(Int32 itemID)
        {

        }

        public void sendVip(Int32 vipLevel, Int32 costMoney)
        {

        }

        #endregion

        #region Stamina
        public void updateStamina(Int32 stamina)
        {

        }
        public void StaminaUpdate(object arg)
        {
            cellCall("requestFlushStamina", new object[] { });
        }

        public void InitUpdStamina()
        {

        }

        public void StartUpdStamina()
        {

        }

        public void StopUpdAStamina()
        {

        }

        public void ContnuUpdStamina()
        {

        }

        public void UninitUpdStamina()
        {

        }

        public void AddStamina(int stamina)
        {

        }

        public void SetStamina(int stamina)
        {

        }

        public int GetStamina()
        {
            return 0;
        }
        #endregion

        #region Gold
        public virtual void updateGold(Int32 goldNum)
        {
            if(SceneManager.instance.IsSceneAvailable)
            {
                int differNum = goldNum - Gold;
                if (differNum < 0)
                {
                    string tip = string.Format("{0},{1},{2}", "金币", "减少", 0 - differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else if(differNum > 0)
                {
                    string tip = string.Format("{0},{1},{2}", "金币", "增加", differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else
                {

                }
            }
            Gold = goldNum;
            if(isPlayer())
            {
            EventManager.Invoke<Int32>(EventID.EVNT_GOLD_UPDATE, Gold);
        }
        }
        #endregion

        #region Silver
        public virtual void updateSilver(Int32 silverNum)
        {
             if(SceneManager.instance.IsSceneAvailable)
             {
                 int differNum = silverNum - Silver;
                 if (differNum < 0)
                 {
                     string tip = string.Format("{0},{1},{2}", "银币", "减少", 0 - differNum);
                     TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                 }
                 else if(differNum > 0)
                 {
                     string tip = string.Format("{0},{1},{2}", "银币", "增加", differNum);
                     TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                 }
                 else
                 {

                 }
             }
            
            Silver = silverNum;
            EventManager.Invoke<Int32>(EventID.EVNT_SILVER_UPDATE, Silver);           
        }
        #endregion

        #region Ingot
        public virtual void updateIngot(Int32 ingotNum)
        {
            if(SceneManager.instance.IsSceneAvailable)
            {
                int differNum = ingotNum - Fairyjade;
                if (differNum < 0)
                {
                    string tip = string.Format("{0},{1},{2}", "元宝", "减少", 0 - differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else if (differNum > 0)
                {
                    string tip = string.Format("{0},{1},{2}", "元宝", "增加", differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else
                {

                }
            }
            
            Fairyjade = ingotNum;
            EventManager.Invoke<Int32>(EventID.EVNT_INGOT_UPDATE, Fairyjade);
        }
        #endregion

		#region Mail
        public void onReceiveEmails(List<object> mailList)
        {
            mailModule.OnReceiveEmails(mailList);
        }

        public void onAddEmail(Int64 emailUID, string title, string content, string attachmentTypes, string attachmentIds, string attachmentCounts, Int32 sendTime, sbyte received)
        {
            mailModule.OnAddEmail(emailUID, title, content, attachmentTypes, attachmentIds, attachmentCounts, sendTime, received);
        }

        public void receiveEmailSuccess(Int64 emailUID)
        {
            mailModule.ReceiveEmailSuccess(emailUID);
        }

        public void onNotReceiveValidEmailCount(Int32 count)
        {
            mailModule.OnNotReceiveValidEmailCount(count);
        }
		#endregion
		
		#region talk
		public void onTalkToNPC(Int32 npcId, Int32 key, Int32 nextTalk, List<object> options)
		{
            talkModule.TalkToNPC(npcId, key, nextTalk, options);
		}
		
		public void onStopTalking()
		{
			Printer.Log("Aavtar::onStopTalking!");
		}
		#endregion
		
		#region team
        //所有avatar都会调用此方法
        public void set_teamingStatus(object old)
        {
            object v = getDefinedPropterty("teamingStatus");
            TeamStatus = Convert.ToByte(v);
            if (isPlayer()) teamModule.SetState(TeamStatus);
        }
		/// <summary>
		/// 收到组队邀请
		/// </summary>
		/// <param name="inviterName">Inviter name.</param>
		public void receiveJoinTeamInvite(string inviterName)
		{
            teamModule.ReceiveJoinTeamInvite(inviterName);
		}
		/// <summary>
		/// 收到入队申请
		/// </summary>
		/// <param name="requestorInfo">请求者的信息.</param>
		public void receiveJoinTeamRequest(Dictionary<string, object> requestorInfo)
		{
            teamModule.ReceiveJoinTeamRequest(requestorInfo);
		}
        /// <summary>
        /// 同意入队请求
        /// </summary>
        /// <param name="dbid"></param>
        public void acceptJoinTeamRequest(DBID dbid)
        {
            teamModule.AcceptJoinTeamRequest(dbid);
        }
		/// <summary>
		/// 加入队伍通知
		/// </summary>
		/// <param name="teamInfo">队伍信息.</param>
		public void onJoinTeam(Dictionary<string, object> teamInfo)
		{
            teamModule.JoinTeam(teamInfo);
		}
		
		/// <summary>
		/// 增加队友
		/// </summary>
		/// <param name="teammateInfo">队友的信息.</param>
		public void onAddTeammate(Dictionary<string, object> teammateInfo)
		{
            teamModule.AddTeammate(teammateInfo);
		}
		
		/// <summary>
		/// 自己离开队伍
		/// </summary>
		/// <param name="reason">离队原因.</param>
		public void onLeaveTeam(Byte reason)
		{
            teamModule.QuitTeam(reason);
		}
		
		/// <summary>
		/// 队友离开队伍
		/// </summary>
		/// <param name="reason">离队原因.</param>
		public void onTeammateLeaveTeam(DBID dbid, Byte reason)
		{
            teamModule.TeamTeammateQuit(dbid, reason);
		}
		
		/// <summary>
		/// 队长改变通知
		/// </summary>
		/// <param name="cpatainDbid">队长的dbid.</param>
		public void onCaptainChanged(DBID cpatainDbid)
		{
            teamModule.CaptainChanged(cpatainDbid);
		}

		/// <summary>
		/// 更新队友等级
		/// </summary>
		/// <param name="dbid">Dbid.</param>
		/// <param name="level">Level.</param>
		public void onUpdateTeammateLevel(DBID dbid, UInt16 level)
		{
            teamModule.UpdateTeammateLevel(dbid, level);
		}
		
		/// <summary>
		/// 更新队友状态
		/// </summary>
		/// <param name="dbid">Dbid.</param>
		/// <param name="level">Level.</param>
		public void onUpdateTeammateStatus(DBID dbid, Byte status)
		{
            teamModule.UpdateTeammateState(dbid, status);
		}
		#endregion

        #region tong
        //所有avatar都会调用此方法
        public void set_tongName(object old)
        {
            TongName = (string)getDefinedPropterty("tongName");
        }
        //所有avatar都会调用此方法
        public void set_tongDuty(object old)
        {
            TongDuty = (Byte)getDefinedPropterty("tongDuty");
        }
        /// <summary>
        /// 收到入会邀请
        /// </summary>
        /// <param name="inviterName">Inviter name.</param>
        public void receiveJoinTongInvite(string inviterName, string tongName)
        {
            tongModule.ReceiveJoinTongInvite(inviterName, tongName);
        }
        /// <summary>
        /// 收到入会申请
        /// </summary>
        /// <param name="requestorInfo">请求者的信息.</param>
        public void receiveJoinTongRequest(Dictionary<string, object> requestorInfo)
        {
            tongModule.ReceiveJoinTongRequest(requestorInfo);
        }
        /// <summary>
        /// 同意入会请求
        /// </summary>
        /// <param name="dbid"></param>
        public void acceptJoinTongRequest(DBID dbid)
        {
            tongModule.AcceptJoinTongRequest(dbid);
        }
        /// <summary>
        /// 加入帮会通知
        /// </summary>
        /// <param name="tongInfo">帮会信息.</param>
        public void onJoinTong(Dictionary<string, object> tongInfo)
        {
            tongModule.JoinTong(tongInfo);
        }

        /// <summary>
        /// 增加会员
        /// </summary>
        /// <param name="tongMemberInfo">会员的信息.</param>
        public void onAddTongMember(Dictionary<string, object> tongMemberInfo)
        {
            tongModule.AddTongMember(tongMemberInfo);
        }

        /// <summary>
        /// 自己离开帮会
        /// </summary>
        /// <param name="reason">离队原因.</param>
        public void onLeaveTong(Byte reason)
        {
            tongModule.QuitTong(reason);
        }

        /// <summary>
        /// 会员离开帮会
        /// </summary>
        /// <param name="reason">离队原因.</param>
        public void onTongMemberLeaveTong(DBID dbid, Byte reason)
        {
            tongModule.TongMemberQuit(dbid, reason);
        }

        /// <summary>
        /// 帮主改变通知
        /// </summary>
        /// <param name="cpatainDbid">帮主的dbid.</param>
        public void onTongCaptainChanged(DBID cpatainDbid)
        {
            tongModule.CaptainChanged(cpatainDbid);
        }

        /// <summary>
        /// 更新会员等级
        /// </summary>
        /// <param name="dbid">Dbid.</param>
        /// <param name="level">Level.</param>
        public void onUpdateTongMemberLevel(DBID dbid, UInt16 level)
        {
            tongModule.UpdateTongMemberLevel(dbid, level);
        }

        /// <summary>
        /// 更新会员状态
        /// </summary>
        /// <param name="dbid">Dbid.</param>
        /// <param name="level">Level.</param>
        public void onUpdateTongMemberStatus(DBID dbid, Byte status)
        {
            tongModule.UpdateTongMemberState(dbid, status);
        }
        
        /// <summary>
        /// 会员职务改变
        /// </summary>
        /// <param name="dbid"></param>
        /// <param name="duty"></param>
        public void onCommittedTongDuty(DBID operatorDbid, DBID targetDbid, Byte duty)
        {
            tongModule.OnCommittedTongDuty(operatorDbid, targetDbid, duty);
        }
        #endregion

        #region controlledBy
        public override void onControlled(bool isControlled_)
        {
            if (isPlayer())
            {
                if (isControlled_)
                {
                    Printer.Log("Controlled by other player.");
                }
                else
                {
                    Printer.Log("Controlled by myself.");
                }
            }
            else
            {
                if (isControlled_)
                {
                    Printer.Log("{0} is controlled by me.", Name);
                }
                else
                {
                    Printer.Log("{0} is lost of controll from me.", Name);
                }
            }
        }
        #endregion //controlledBy

        #region RewardModule
        public void updateSignState(int signed)
        {
            //0为false，1为true
            rewardModule.UpdateSignState(signed == 1);
    }
        public void updateSignTimes(uint signedDays)
        {
            rewardModule.UpdateSignTimes(signedDays);
        }
        public void updateRetroactiveTimes(uint unsignedDays)
        {
            rewardModule.UpdateRetroactiveTimes(unsignedDays);
        }
        #endregion

        #region Activity
        public void receiveActivityInfo(Dictionary<string, object> activityInfo)
        {
            activityModule.InitActivityInfo(activityInfo);
        }

        public void updateLivenessValue(UInt32 value)
        {
            activityModule.UpdateLivenessValue(value);
        }

        public void updateRewardItemState(List<object> rewardItems)
        {
            activityModule.UpdateRewardItemState(rewardItems);
        }
        #endregion

        #region MultipleFightCopy
        public void confirmJoinMultipleFightCopy(List<object> agreeDatas)
        {
            teamCopyModule.ConfirmJoinMultipleFightCopy(agreeDatas);
        }

        public virtual void set_chivalrous(object old)
        {
            object v = getDefinedPropterty("chivalrous");
            if (SceneManager.instance.IsSceneAvailable)
            {
                int differNum = (Int32)v - Chivalrous;
                if (differNum < 0)
                {
                    string tip = string.Format("{0},{1},{2}", "侠义值", "减少", Math.Abs(differNum));
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else if (differNum > 0)
                {
                    string tip = string.Format("{0},{1},{2}", "侠义值", "增加", differNum);
                    TipManager.instance.ShowTextTip(Define.COMMON_CURRENCY_TYPE_CHANGE, tip);
                }
                else
                {

                }
            }
            Chivalrous = (Int32)v;
        }

        public void confirmLeaveMultipleSpace(Int16 leaveType, Int32 statsChivalrous, Int32 statsGolds, Int32 statsSilvers, Int32 statsAvatarEXPs, Dictionary<string, object> statsItems)
        {
            teamCopyModule.ConfirmLeaveMultipleSpace(leaveType, statsChivalrous, statsGolds, statsSilvers, statsAvatarEXPs, statsItems);
        }


        #endregion
    }

}
