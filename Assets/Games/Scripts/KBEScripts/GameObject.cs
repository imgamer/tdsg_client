namespace KBEngine
{
	using UnityEngine; 
	using System; 
	using System.Collections; 
	using System.Collections.Generic;

	public abstract class GameObject : Entity
	{
        public SceneEntity SceneEntityObj { get; private set; }
        //基本属性
        public SByte State{ get; private set; }
        public SByte FightState{ get; private set; } //战斗中的状态
        public UInt32 UType { get; protected set; }
        public string Name { get; protected set; }
        public UInt16 Level { get; protected set; }


        public string ModelID { get; protected set; }

        //一级属性
        public UInt32 Force { get; private set; } //武力
        public UInt32 Spirituality { get; private set; } //灵性
		public UInt32 Vitality { get; private set; } //体质       
        public UInt32 Gengu { get; private set; } //根骨

        //二级属性
        public Int32 AttackSpeed { get; private set; } //攻击速度
        public UInt32 ActionValue { get; private set; } //行动值
        public Int32 HP { get; private set; } //生命值
        public Int32 HP_Max { get; private set; } //生命值上限
        public Int32 MP { get; private set; } //怒气值
        public Int32 MP_Max { get; private set; } //怒气值上限
        public Int32 PhysicsPower { get; private set; } //物理攻击力
        public Int32 MagicPower { get; private set; } //法术攻击力
        public Int32 Hit { get; private set; } //命中率
        public Int32 Dodge { get; private set; } //闪避（对抗命中）
        public Int32 PhysicsDefense { get; private set; } //物理防御
        public Int32 MagicDefense { get; private set; } //法术防御
        public Int32 Crit { get; private set; } //暴击
        public Int32 Toughness { get; private set; } //坚韧
        public Int32 EscapeRate { get; private set; } //逃跑成功率

        public Int32 TreatmentStrength { get; private set; } //治疗强度
        public Int32 BarriersShootingSkill { get; private set; } //障碍技能命中率

        public SkillBox skillBox;

        public int ClientHP { get; set; }

        public Vector3 Position
        {
            get 
            {
                return new Vector3( position.x, position.z, position.y );
            }

            set
            {
                position.x = value.x;
                position.y = value.z;
                position.z = value.y;
            }
        }


        private bool _inMoving = false;
        public bool InMoving
        {
            get { return _inMoving; } 
            set { _inMoving = value; } 
        }

        private int _skillToUse = -1;//当前使用的技能

        public override void onEnterWorld()
        {
            base.onEnterWorld();

            UType = (UInt32)getDefinedPropterty("utype");
            Name = (string)getDefinedPropterty("name");

            ModelID = (string)getDefinedPropterty("modelID");

            Force = (UInt32)getDefinedPropterty("force");
			Vitality = (UInt32)getDefinedPropterty("vitality");
            Spirituality = (UInt32)getDefinedPropterty("spirituality");
            Gengu = (UInt32)getDefinedPropterty("gengu");
            AttackSpeed = (Int32)getDefinedPropterty("attackSpeed");
            HP_Max = (Int32)getDefinedPropterty("HP_Max");
            //默认最多血量
            HP = HP_Max;
            MP = (Int32)getDefinedPropterty("MP");
            MP_Max = (Int32)getDefinedPropterty("MP_Max");
            PhysicsPower = (Int32)getDefinedPropterty("physicsPower");
            MagicPower = (Int32)getDefinedPropterty("magicPower");
            Hit = (Int32)getDefinedPropterty("hit");
            Dodge = (Int32)getDefinedPropterty("dodge");
            PhysicsDefense = (Int32)getDefinedPropterty("physicsDefense");
            MagicDefense = (Int32)getDefinedPropterty("magicDefense");
            Crit = (Int32)getDefinedPropterty("crit");
            Toughness = (Int32)getDefinedPropterty("toughness");
            EscapeRate = (Int32)getDefinedPropterty("escapeRate");
            TreatmentStrength = (Int32)getDefinedPropterty("treatmentStrength");
            BarriersShootingSkill = (Int32)getDefinedPropterty("barriersShootingSkill");

            //State = (SByte)getDefinedPropterty("state");
            skillBox = new SkillBox();
        }

        public void SetSceneEntity(SceneEntity sceneEntity)
        {
            SceneEntityObj = sceneEntity;
        }

        public virtual void set_force(object old)
        {
            object v = getDefinedPropterty("force");
            Dbg.DEBUG_MSG(className + "::set_force: " + old + " => " + v);
            Force = (UInt32)v;
        }
        public virtual void set_vitality(object old)
        {
            object v = getDefinedPropterty("vitality");
            Dbg.DEBUG_MSG(className + "::set_vitality: " + old + " => " + v);
            Vitality = (UInt32)v;
        }
        public virtual void set_spirituality(object old)
        {
            object v = getDefinedPropterty("spirituality");
            Dbg.DEBUG_MSG(className + "::set_spirituality: " + old + " => " + v);
            Spirituality = (UInt32)v;             
        }
        public virtual void set_gengu(object old)
        {
            object v = getDefinedPropterty("gengu");
            Dbg.DEBUG_MSG(className + "::set_gengu: " + old + " => " + v);
            Gengu = (UInt32)v;
        }
        public virtual void set_attackSpeed(object old)
        {
            object v = getDefinedPropterty("attackSpeed");
            Dbg.DEBUG_MSG(className + "::set_attackSpeed: " + old + " => " + v);
            AttackSpeed = (Int32)v;
        }


        //进入战斗初始化状态
        public void InitState()
        {
            ResetHP();
            SetActionValue(Define.DEFAULT_ACTION_VALUE);
            SetFightState(Define.ENTITY_STATE_FIGHT_FREE);
        }

        #region 行动值
        public void SetActionValue(uint value)
        {
            if (FightState == Define.ENTITY_STATE_FIGHT_DEAD || FightState == Define.ENTITY_STATE_FIGHT_WOUNDED) return;
            ActionValue = value;
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SyncActionValue();
            } 
        }
        #endregion

        #region 复活
        public void onFightRelive()
        {
            ResetHP();
            SetFightState(Define.ENTITY_STATE_FIGHT_FREE);
        }
        #endregion

        #region 战斗状态
        public void fightStateChanged(SByte state)
        {
            SetFightState(state);
        }
        protected virtual void SetFightState(SByte v)
        {
            FightState = v;
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SyncFightState();
            } 
        }
        #endregion

        #region 血量
        public void ChangeHP(int changeValue)
        {
            SetHP(HP + changeValue);
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SyncHPBar();
            }
        }

        private void ResetHP()
        {
            HP = HP_Max;
            ClientHP = HP_Max;
            if (SceneEntityObj != null)
            {
                SceneEntityObj.SyncHPBar();
            }
        }

        private void SetHP(int value)
        {
            HP = value;
            if (HP <= 0)
            {
                HP = 0;
                if (CanDie())
                {
                    SetFightState(Define.ENTITY_STATE_FIGHT_DEAD);
                }
                else
                {
                    SetFightState(Define.ENTITY_STATE_FIGHT_WOUNDED);
                }
            }
            else
            {
                if (HP > HP_Max)
                {
                    HP = HP_Max;
                }
                //从重伤状态恢复血量时
                if (FightState == Define.ENTITY_STATE_FIGHT_WOUNDED)
                {
                    SetFightState(Define.ENTITY_STATE_FIGHT_FREE);
                }
            }
        }
        #endregion

        public virtual bool CanDie()
        {
            return true;
        }

        public virtual void set_HP(object old)
        {
            object v = getDefinedPropterty("HP");
            Dbg.DEBUG_MSG(className + "::set_HP: " + old + " => " + v);
            HP = (Int32)v;
        }

        public virtual void set_HP_Max(object old)
        {
            object v = getDefinedPropterty("HP_Max");
            Dbg.DEBUG_MSG(className + "::set_HP_Max: " + old + " => " + v);
            HP_Max = (Int32)v;            
        }
        public virtual void set_MP(object old)
        {
            object v = getDefinedPropterty("MP");
            Dbg.DEBUG_MSG(className + "::set_MP: " + old + " => " + v);
            MP = (Int32)v;            
        }
        public virtual void set_MP_Max(object old)
        {
            object v = getDefinedPropterty("MP_Max");
            Dbg.DEBUG_MSG(className + "::set_MP_Max: " + old + " => " + v);
            MP_Max = (Int32)v;             
        }

        public virtual void set_physicsPower(object old)
        {
            object v = getDefinedPropterty("physicsPower");
            Dbg.DEBUG_MSG(className + "::set_physicsPower: " + old + " => " + v);
            PhysicsPower = (Int32)v;           
        }
        public virtual void set_magicPower(object old)
        {
            object v = getDefinedPropterty("magicPower");
            Dbg.DEBUG_MSG(className + "::set_magicPower: " + old + " => " + v);
            MagicPower = (Int32)v;
        }
        public virtual void set_hit(object old)
        {
            object v = getDefinedPropterty("hit");
            Dbg.DEBUG_MSG(className + "::set_hit: " + old + " => " + v);
            Hit = (Int32)v;             
        }
        public virtual void set_dodge(object old)
        {
            object v = getDefinedPropterty("dodge");
            Dbg.DEBUG_MSG(className + "::set_dodge: " + old + " => " + v);
            Dodge = (Int32)v;            
        }
        
        public virtual void set_physicsDefense(object old)
        {
            object v = getDefinedPropterty("physicsDefense");
            Dbg.DEBUG_MSG(className + "::set_physicsDefense: " + old + " => " + v);
            PhysicsDefense = (Int32)v;             
        }
        public virtual void set_magicDefense(object old)
        {
            object v = getDefinedPropterty("magicDefense");
            Dbg.DEBUG_MSG(className + "::set_magicDefense: " + old + " => " + v);
            MagicDefense = (Int32)v;           
        }
        public virtual void set_crit(object old)
        {
            object v = getDefinedPropterty("crit");
            Dbg.DEBUG_MSG(className + "::set_crit: " + old + " => " + v);
            Crit = (Int32)v;
        }
        public virtual void set_toughness(object old)
        {
            object v = getDefinedPropterty("toughness");
            Dbg.DEBUG_MSG(className + "::set_toughness: " + old + " => " + v);
            Toughness = (Int32)v;            
        }
        
        public virtual void set_escapeRate(object old)
        {
            object v = getDefinedPropterty("escapeRate");
            Dbg.DEBUG_MSG(className + "::set_escapeRate: " + old + " => " + v);
            EscapeRate = (Int32)v;             
        }

        public virtual void set_treatmentStrength(object old)
        {
            object v = getDefinedPropterty("treatmentStrength");
            Dbg.DEBUG_MSG(className + "::set_treatmentStrength: " + old + " => " + v);
            TreatmentStrength = (Int32)v;             
        }

        public virtual void set_barriersShootingSkill(object old)
        {
            object v = getDefinedPropterty("barriersShootingSkill");
            Dbg.DEBUG_MSG(className + "::set_barriersShootingSkill: " + old + " => " + v);
            BarriersShootingSkill = (Int32)v;            
        }

        public virtual void SendToLogicLayout(string methodName, object[] args)
        {
            Event.fireOut("procEntityInvocation", new object[] { this, methodName, args });
        }

		public virtual void set_level(object old)
		{
			object v = getDefinedPropterty("level");
			Dbg.DEBUG_MSG(className + "::set_level: " + old + " => " + v); 
            Level = (UInt16)v;             
		}
		
		public virtual void set_name(object old)
		{
			object v = getDefinedPropterty("name");
			Dbg.DEBUG_MSG(className + "::set_name: " + old + " => " + v);
            Name = (string)v;            
		}
		
		public virtual void set_state(object old)
		{
			object v = getDefinedPropterty("state");
			Dbg.DEBUG_MSG(className + "::set_state: " + old + " => " + v); 
			Event.fireOut("set_state", new object[]{this, v});
		}
		
        public virtual void set_inMoving(object old)
        {
            object v = getDefinedPropterty("inMoving");
            //Dbg.DEBUG_MSG(className + "::inMoving: " + old + " => " + v);
            InMoving = Convert.ToBoolean(v);
        }

        public virtual void setMovingState(bool val)
        {
            cellCall("changeMovingStatus", new object[] { val ? 1 : 0 });
        }

		public virtual void set_subState(object old)
		{
			Dbg.DEBUG_MSG(className + "::set_subState: " + getDefinedPropterty("subState")); 
		}
		
		public virtual void set_utype(object old)
		{
			Dbg.DEBUG_MSG(className + "::set_utype: " + getDefinedPropterty("utype")); 
		}
		
		public virtual void set_uid(object old)
		{
			Dbg.DEBUG_MSG(className + "::set_uid: " + getDefinedPropterty("uid")); 
		}
		
		public virtual void set_spaceUType(object old)
		{
			Dbg.DEBUG_MSG(className + "::set_spaceUType: " + getDefinedPropterty("spaceUType")); 
		}
		
		public virtual void set_moveSpeed(object old)
		{
			object v = getDefinedPropterty("moveSpeed");
			Dbg.DEBUG_MSG(className + "::set_moveSpeed: " + old + " => " + v); 
			Event.fireOut("set_moveSpeed", new object[]{this, v});
		}
		
		public virtual void set_modelScale(object old)
		{
			object v = getDefinedPropterty("modelScale");
			Dbg.DEBUG_MSG(className + "::set_modelScale: " + old + " => " + v); 
			Event.fireOut("set_modelScale", new object[]{this, v});
		}
		
		public virtual void set_modelID(object old)
		{
			object v = getDefinedPropterty("modelID");
			Dbg.DEBUG_MSG(className + "::set_modelID: " + old + " => " + v);
            ModelID = Convert.ToString(v);
		}
		
		public virtual void set_forbids(object old)
		{
			Dbg.DEBUG_MSG(className + "::set_forbids: " + getDefinedPropterty("forbids")); 
		}

		public virtual void recvDamage(Int32 attackerID, Int32 skillID, Int32 damageType, Int32 damage)
		{
			Dbg.DEBUG_MSG(className + "::recvDamage: attackerID=" + attackerID + ", skillID=" + skillID + ", damageType=" + damageType + ", damage=" + damage);
			
			Entity entity = KBEngineApp.app.findEntity(attackerID);

			Event.fireOut("recvDamage", new object[]{this, entity, skillID, damageType, damage});
		}

        public virtual void onAddSkill(Int32 skillID)
        {
            Dbg.DEBUG_MSG(className + "::onAddSkill(" + skillID + ")");
            skillBox.add(skillID);
        }

        public virtual void onRemoveSkill(Int32 skillID)
        {
            Dbg.DEBUG_MSG(className + "::onRemoveSkill(" + skillID + ")");
            skillBox.remove(skillID);
        }

		public virtual void addDBuff(Dictionary<string, object> buffData)
		{
			Int32 buffID = 0;
			Byte currTick = 0;
			Byte currRound = 0;
			Int32 casterID = 0;

			IDictionaryEnumerator buffiter = buffData.GetEnumerator();
			while (buffiter.MoveNext())
			{
				Dbg.DEBUG_MSG(className + "::addDBuff: buffData .key=" + buffiter.Key + " (Type: )" + buffiter.Key.GetType() + ", .value=" + buffiter.Value + " (Type: )" + buffiter.Value.GetType());

				if (((string)buffiter.Key).CompareTo("skillID") == 0)
				{
					buffID = (Int32)buffiter.Value;
				}
				else if (((string)buffiter.Key).CompareTo("currTick") == 0)
				{
					currTick = (Byte)buffiter.Value;
				}
				else if (((string)buffiter.Key).CompareTo("currRound") == 0)
				{
					currRound = (Byte)buffiter.Value;
				}
				else if (((string)buffiter.Key).CompareTo("casterID") == 0)
				{
					casterID = (Int32)buffiter.Value;
				}
			}

			Event.fireOut("onAddDBuff", new object[]{this, buffID, currTick, currRound, casterID});
		}

		


    }
} 
