namespace KBEngine
{
  	using UnityEngine; 
	using System; 
	using System.Collections; 
	using System.Collections.Generic;

    public class VisibleNPC : NPC, IVisibility
    {
        private ulong _timerID = TimeUtils.InvalidTimerID;
        private bool _prevVisible = false;
        private VisibleTypeBase _visibleType;

        public override void onEnterWorld()
        {
            SetVisibleTypeFromStream();
            enterSceneOnBorn_ = Visible();
            _prevVisible = Visible();

            base.onEnterWorld();

            _timerID = TimeUtils.AddLoopRealTimer(1f, OnUpdateVisibilityTimer);
            TimeUtils.StartTimer(_timerID);
        }

        public override void onLeaveWorld()
        {
            base.onLeaveWorld();
            TimeUtils.RemoveTimer(_timerID);
        }

        public VisibleTypeBase VisibleType
        {
            get { return _visibleType; }
        }

        void SetVisibleTypeFromStream()
        {
            Dictionary<string, object> visibleType = (Dictionary<string, object>)getDefinedPropterty("visibleType");
            _visibleType = VisibilityFactory.SharedInstance.CreateVisibleTypeFromStream(
                (byte)visibleType["type"], (string)visibleType["data"]);
        }

        void OnUpdateVisibilityTimer(int delta)
        {
            UpdateVisibility();
        }

        void UpdateVisibility()
        {
            bool visibleChanged = _prevVisible != Visible();

            if (visibleChanged)
            {
                if (!_prevVisible && SceneEntityObj == null)
                {
                    SceneManager.instance.OnEnterScene(this);
                }
                else if (_prevVisible && SceneEntityObj != null)
                {
                    SceneManager.instance.OnLeaveScene(this);
                }

                _prevVisible = Visible();
            }
        }

        public void set_visibleType(object old)
        {
            SetVisibleTypeFromStream();

            Printer.Log("npc visible type set to {0}", _visibleType.ToString());
        }

        public void SetVisibleType(byte type, object data)
        {
            _visibleType = VisibilityFactory.SharedInstance.CreateVisibleType(type, data);
        }

        public bool Visible()
        {
            return _visibleType.Check(GameMain.Player);
        }
    }
    
} 
