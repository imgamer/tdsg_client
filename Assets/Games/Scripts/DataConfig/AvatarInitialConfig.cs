﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class AvatarInitialData
{
    public int school;
    public int HP;
    public int HP_Max;
}


public class AvatarInitialDataReader
{
    private JsonData data;

    #region properties
    //门派
    public UInt16 School
    {
        get { return (UInt16)data["school"]; }
    }

    //武力
    public UInt32 Force
    {
        get { return (UInt32)data["force"]; }
    }

    //体质
    public UInt32 Vitality
    {
        get { return (UInt32)data["vitality"]; }
    }

    //灵性
    public UInt32 Spirituality
    {
        get { return (UInt32)data["spirituality"]; }
    }

    //根骨
    public UInt32 Gengu
    {
        get { return (UInt32)data["gengu"]; }
    }

    //初始技能
    public List<int> Skills
    {
        get {
            List<int> skills = new List<int>();
            for (int i = 0; i < data["skills"].Count; i++)
            {
                skills.Add((int)data["skills"][i]);
            }
            return skills; 
        }
    }

    //初始生命
    public Int32 HP
    {
        get { return (Int32)data["HP"]; }
    }

    //初始生命上限
    public Int32 HP_Max
    {
        get { return (Int32)data["HP_Max"]; }
    }

    //初始怒气
    public Int32 MP
    {
        get { return (Int32)data["MP"]; }
    }

    //初始怒气上限
    public Int32 MP_Max
    {
        get { return (Int32)data["MP_Max"]; }
    }

    //暴击值
    public Int32 Crit
    {
        get { return (Int32)data["crit"]; }
    }

    //暴击率
    public float CritRate
    {
        get { return (float)data["critRate"]; }
    }

    //命中值
    public Int32 Hit
    {
        get { return (Int32)data["hit"]; }
    }

    //命中率
    public float HitRate
    {
        get { return (float)data["hitRate"]; }
    }

    //闪避值
    public Int32 Dodge
    {
        get { return (Int32)data["dodge"]; }
    }

    //坚韧
    public Int32 Toughness
    {
        get { return (Int32)data["toughness"]; }
    }

    //物理攻击
    public Int32 PhysicsPower
    {
        get { return (Int32)data["physicsPower"]; }
    }

    //物理防御
    public Int32 PhysicsDefense
    {
        get { return (Int32)data["physicsDefense"]; }
    }

    //法术攻击
    public Int32 MagicPower
    {
        get { return (Int32)data["magicPower"]; }
    }

    //法术防御
    public Int32 MagicDefense
    {
        get { return (Int32)data["magicDefense"]; }
    }

    //攻击速度
    public Int32 AttackSpeed
    {
        get { return (Int32)data["attackSpeed"]; }
    }

    //治疗强度
    public Int32 TreatmentStrength
    {
        get { return (Int32)data["treatmentStrength"]; }
    }
    #endregion

    public AvatarInitialDataReader(JsonData data)
    {
        this.data = data;
    }

    public JsonData Query(string key)
    {
        if (data.Keys.Contains(key))
        {
            return data[key];
        }
        else
        {
            return null;
        }
    }
}

public class AvatarInitialConfig 
{
    private static AvatarInitialConfig _instance;
	private JsonData _datas;
	
	public static AvatarInitialConfig SharedInstance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new AvatarInitialConfig();
			}
			return _instance;
		}
	}

    private AvatarInitialConfig()
	{
		LoadAvatarPropertiesInitialConfig();
	}
	
	public void LoadAvatarPropertiesInitialConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("avatar_properties_initial");
        _datas = JsonMapper.ToObject(textdata.text);
	}
	
	public AvatarInitialDataReader GetAvatarInitialData(UInt16 school)
	{
        if (_datas.Keys.Contains(school.ToString()))
        {
            return new AvatarInitialDataReader(_datas[school.ToString()]);
        }
        else
        {
            return null;
        }
	}
}
