﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class SceneData
{
	public int roomType;
	public int utype;
	public string name;
	public string scriptType;
	public string entityType;
	public string description;
	public List<StageData> stages = new List<StageData>();
	public string sceneName;
    public string resPath;
	public string clientScript;
    public List<double> spawnPosition;
    public Vector3 spawnPosVector3;
}

public class StageData
{
	public string monsterDatasid;
	public string script;
	public string clientScript;
}

public class SceneConfig
{
	private static SceneConfig instance_;
	private Dictionary<int,SceneData> SceneConfigData_ = new Dictionary<int,SceneData>();

	public static SceneConfig SharedInstance
	{
		get
		{
			if (instance_ == null)
			{
				instance_ = new SceneConfig();
			}
			return instance_;
		}
	}

	private SceneConfig()
	{
		LoadSceneConfig();
	}

	public void LoadSceneConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_scenes");
        JsonData data = JsonMapper.ToObject(textdata.text);
        for (int i = 0; i < data.Count; i++)
        {
            SceneData scenedata = new SceneData();
            scenedata.roomType = (int)data[i]["roomType"];
            scenedata.description = (string)data[i]["description"];
            scenedata.entityType = (string)data[i]["entityType"];
            scenedata.scriptType = (string)data[i]["scriptType"];
            scenedata.utype = (int)data[i]["utype"];
            scenedata.name = (string)data[i]["name"];
            scenedata.sceneName = (string)data[i]["sceneName"];
            scenedata.resPath = (string)data[i]["resPath"];
            scenedata.clientScript = (string)data[i]["clientScript"];

            if (data[i].Keys.Contains("spawnPosition") && data[i]["spawnPosition"].Count > 0)
            { 
                scenedata.spawnPosition = JsonMapper.ToObject<List<double>>(data[i]["spawnPosition"].ToJson());
                if (scenedata.spawnPosition.Count >= 3)
                {
                    scenedata.spawnPosVector3 = new Vector3((float)scenedata.spawnPosition[0], (float)scenedata.spawnPosition[1], (float)scenedata.spawnPosition[2]);
                } 
            }

            JsonData stagesData = data[i]["stages"];
            if (stagesData.Count != 0)
            {
                scenedata.stages = JsonMapper.ToObject<List<StageData>>(stagesData.ToJson());
            }

            SceneConfigData_.Add(scenedata.roomType, scenedata);
        }
	}

	public SceneData GetSceneData(int roomType)
	{
		SceneData data;
		SceneConfigData_.TryGetValue(roomType, out data);
		return data;
	}

}
