﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public enum Function
{
    None = -1,

    Guide,      //指引
    Act,        //活动
    Hang,       //挂机
    Promote,    //提升
    Vigour,     //血气

    Map,        //地图
    Award,      //奖励
    Shop,       //商城
    Friend,     //好友
    Channel,    //频道
    WorldVoice, //世界语音
    FactionVoice,//帮派语音
    TeamVoice,  //队伍语音

    Pet,        //宠物
    Player,     //玩家
    Task,       //任务
    Team,       //队伍
    Bag,        //背包
    Assist,     //助战
    Skill,      //技能
    Rank,       //排行
    Faction,    //帮派
    System,     //系统
    Strengthen, //强化

    Max,
}

public class HomeFunctionsConfig 
{

    public struct HomeFunctionsData
    {
        public int m_id;
        public int m_priority;
    }

    private static HomeFunctionsConfig _instance;
    public static HomeFunctionsConfig SharedInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new HomeFunctionsConfig();
            }
            return _instance;
        }
    }

    private Dictionary<Function, HomeFunctionsData> _homeFunctionsData = new Dictionary<Function, HomeFunctionsData>();
    private HomeFunctionsConfig()
	{
        LoadHomeFunctionsJson();
	}

    private void LoadHomeFunctionsJson()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("home_functions");
        JsonData data = JsonMapper.ToObject(textdata.text);
        if (data.Count != (int)Function.Max)
        {
            Printer.LogError("主界面功能表的数量和定义的枚举数量不一致！");
            return;
        }
        for (int i = 0; i < data.Count; i++)
        {
            HomeFunctionsData item = new HomeFunctionsData();
            item.m_id = (int)data[i]["ID"];
            item.m_priority = (int)data[i]["Priority"];
            Function key = (Function)Enum.Parse(typeof(Function), (string)data[i]["Function"]);
            _homeFunctionsData.Add(key, item);
        }
	}

    public int GetID(Function function)
    {
        HomeFunctionsData item;
        if (_homeFunctionsData.TryGetValue(function, out item))
        {
            return item.m_id;
        }
        return -1;
    }

    public int GetPriority(Function function)
    {
        HomeFunctionsData item;
        if (_homeFunctionsData.TryGetValue(function, out item))
        {
            return item.m_priority;
        }
        return -1;
    }

}
