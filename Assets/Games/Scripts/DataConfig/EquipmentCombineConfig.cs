using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class EquipmentCombine
{
	public byte qualityID; //品质编号
	public string qualityName;   //装备品质
	public int illusionPercent;   //显示成功概率
	public int realPercent;  //实际成功概率
	public byte combineQualityID;  //合成获得装备品质编号
	public string combineQualityName;  //合成获得装备品质
}

public class EquipmentCombineConfig
{
	private static EquipmentCombineConfig _instance;
	private Dictionary<string, EquipmentCombine> _equipmentCombineConfigData = new Dictionary<string, EquipmentCombine>();
	
	public static EquipmentCombineConfig SharedInstance
	{
		get
		{
            if (_instance == null)
			{
                _instance = new EquipmentCombineConfig();
			}
            return _instance;
		}
	}
	
	private EquipmentCombineConfig()
	{
		LoadEquipmentCombineConfig();
	}
	
	public void LoadEquipmentCombineConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("equipment_combine");
        _equipmentCombineConfigData = JsonMapper.ToObject<Dictionary<string, EquipmentCombine>>(textdata.text);
	}
	
	public EquipmentCombine GetEquipmentCombineData(byte qualityID)
	{
		string index = Convert.ToString(qualityID);
		EquipmentCombine data;
        _equipmentCombineConfigData.TryGetValue(index, out data);
		return data;
	}
}
