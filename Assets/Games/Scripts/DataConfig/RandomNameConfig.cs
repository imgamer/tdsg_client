﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
/// <summary>
/// 随机名字配置
/// </summary>
public class RandomNameConfig : Singleton<RandomNameConfig>
{
    private Dictionary<string, List<string>> _dic = new Dictionary<string, List<string>>();
    protected override void OnInit()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("random_name");
        _dic = JsonMapper.ToObject<Dictionary<string, List<string>>>(textdata.text);
    }

    private string GetFamilyName()
    {
        string ret = string.Empty;
        List<string> names;
        if (_dic.TryGetValue("0", out names))
        {
            int i = UnityEngine.Random.Range(0, names.Count);
            ret = names[i];
        }
        return ret;
    }

    public string GetMaleName()
    {
        string ret = string.Empty;
        List<string> names;
        if(_dic.TryGetValue("1", out names))
        {
            int i = UnityEngine.Random.Range(0, names.Count);
            ret = names[i];
        }
        return string.Format("{0}{1}", GetFamilyName(), ret);
    }

    public string GetFemaleName()
    {
        string ret = string.Empty;
        List<string> names;
        if (_dic.TryGetValue("2", out names))
        {
            int i = UnityEngine.Random.Range(0, names.Count);
            ret = names[i];
        }
        return string.Format("{0}{1}", GetFamilyName(), ret);
    }
}
