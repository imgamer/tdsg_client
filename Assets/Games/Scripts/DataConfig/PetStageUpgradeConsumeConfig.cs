﻿using System;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class PetStageUpgradeConsumeData
{
    public int itemID;
    public Dictionary<string, int> amount;
}

public class PetStageUpgradeConsumeConfig : Singleton<PetStageUpgradeConsumeConfig>
{
    private Dictionary<string, PetStageUpgradeConsumeData> _datas;

    protected override void OnInit()
    {
        Load();
    }

    private void Load()
    {
        TextAsset asset = AssetsManager.instance.SyncSpawnConfig("pet_upgrade_stage_consume");
        _datas = JsonMapper.ToObject<Dictionary<string, PetStageUpgradeConsumeData>>(asset.text);
    }

    public PetStageUpgradeConsumeData GetUpgradeConsume(Byte type)
    {
        PetStageUpgradeConsumeData data;
        if (_datas.TryGetValue(type.ToString(), out data))
            return data;
        else
            return null;
    }
}

