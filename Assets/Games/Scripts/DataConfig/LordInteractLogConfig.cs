﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class LordInteractLogConfig
{
    private static LordInteractLogConfig _instance;
    private Dictionary<string, string> _lordInteractLogConfigData = new Dictionary<string, string>();
    
    public static LordInteractLogConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new LordInteractLogConfig();
            }
            return _instance;
        }
    }

    private LordInteractLogConfig()
    {
        LoadLordInteractLogConfig();
    }

    public void LoadLordInteractLogConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("lord_interact_log");
        _lordInteractLogConfigData = JsonMapper.ToObject<Dictionary<string, string>>(textdata.text);
    }

    public string GetLogData(Int32 id)
    {
        string data;
        _lordInteractLogConfigData.TryGetValue(Convert.ToString(id), out data);
        return data;
    }
}
