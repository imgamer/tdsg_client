﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class EquipmentAttributeConsumeData
{
    public int amount;
    public int itemID;
}

public class EquipmentWashsAttributeConsumeConfig
{
    private static EquipmentWashsAttributeConsumeConfig _instance;
    private Dictionary<string, EquipmentAttributeConsumeData> _equipmentAttributeConsumeConfigData = new Dictionary<string, EquipmentAttributeConsumeData>();
    
    public static EquipmentWashsAttributeConsumeConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new EquipmentWashsAttributeConsumeConfig();
            }
            return _instance;
        }
    }

    private EquipmentWashsAttributeConsumeConfig()
    {
        LoadEquipmentWashsAttributeConsumeConfig();
    }

    public void LoadEquipmentWashsAttributeConsumeConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("equipment_washs_attribute_consume");
        _equipmentAttributeConsumeConfigData = JsonMapper.ToObject<Dictionary<string, EquipmentAttributeConsumeData>>(textdata.text);
    }

    public EquipmentAttributeConsumeData GetAttributeConsumeData(int level)
    {
        string equipLevel = Convert.ToString(level);
        EquipmentAttributeConsumeData data;
        _equipmentAttributeConsumeConfigData.TryGetValue(equipLevel, out data);
        return data;
    }
}
