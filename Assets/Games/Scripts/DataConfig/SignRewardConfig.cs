﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class SignReward
{
    public int num;
    public ItemsData itemData;
    public SignReward(int num, ItemsData itemData)
    {
        this.num = num;
        this.itemData = itemData;
    }
}

/// <summary>
/// 签到奖励配置表
/// </summary>
public class SignRewardConfig : Singleton<SignRewardConfig>
{
    private Dictionary<int, SignReward> _dataDict = new Dictionary<int, SignReward>();
    protected override void OnInit()
    {
        LoadJson();
    }

    private void LoadJson()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_sign_reward");
        JsonData jsonData = JsonMapper.ToObject(textdata.text);
        for (int i = 0; i < jsonData.Count; i++)
        {
            int id = (int)(jsonData[i]["rewardItemID"]);
            ItemsData item = ItemsConfig.SharedInstance.GetItemData(id);
            if (item == null)
            {
                Printer.LogError("第{0}天奖励数据出错！", i + 1);
                continue;
            }
            int num = (int)(jsonData[i]["rewardItemNum"]);
            SignReward reward = new SignReward(num, item);
            _dataDict.Add(i, reward);
        }
    }

    public SignReward GetSignReward(int index)
    {
        SignReward reward;
        _dataDict.TryGetValue(index, out reward);
        return reward;
    }
}
