﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class EquipmentSpecialSkillConsumeData
{
    public int amount;
    public int itemID;
}

public class EquipmentRenewSpecialSkillConsumeConfig 
{
    private static EquipmentRenewSpecialSkillConsumeConfig _instance;
    private Dictionary<string, EquipmentSpecialSkillConsumeData> _equipmentSpecialSkillConsumeConfigData = new Dictionary<string, EquipmentSpecialSkillConsumeData>();
    
    public static EquipmentRenewSpecialSkillConsumeConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new EquipmentRenewSpecialSkillConsumeConfig();
            }
            return _instance;
        }
    }

    private EquipmentRenewSpecialSkillConsumeConfig()
    {
        LoadEquipmentRenewSpecialSkillConsumeConfig();
    }

    public void LoadEquipmentRenewSpecialSkillConsumeConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("equipment_renew_specialskill_consume");
        _equipmentSpecialSkillConsumeConfigData = JsonMapper.ToObject<Dictionary<string, EquipmentSpecialSkillConsumeData>>(textdata.text);
    }

    public EquipmentSpecialSkillConsumeData GetSpecialSkillConsumeData(int quality)
    {
        string equipQuality = Convert.ToString(quality);
        EquipmentSpecialSkillConsumeData data;
        _equipmentSpecialSkillConsumeConfigData.TryGetValue(equipQuality, out data);
        return data;
    }
}
