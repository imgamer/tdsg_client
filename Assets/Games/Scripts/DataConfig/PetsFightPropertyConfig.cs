﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class PetsFightProperty
{
    public int toughnessValue;
    public int sunderArmorValue;
    public int HP_Max;
    public int level;
    public int utype;
    public int attackValue;
    public int HP;
    public int criticalValue;
    public int parryValue;
    public int speedInitValue;
    public int effectResistValue;
    public int effectResistValueInit;
    public int criticalDamageValue;
    public int effectEnhanceValue;
    public int defenseValue;
    public int speed;
	public int magicAttackValue;
	public int magicDefenseValue;
	public int MP_Max;
}

public class PetsFightPropertyConfig
{
    private static PetsFightPropertyConfig _instance;
    private Dictionary<string, Dictionary<string, PetsFightProperty>> _petsFightPropertyConfigData = new Dictionary<string, Dictionary<string, PetsFightProperty>>();

    public static PetsFightPropertyConfig SharedInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PetsFightPropertyConfig();
            }
            return _instance;
        }
    }

    private PetsFightPropertyConfig()
    {
        LoadPetsFightPropertyConfig();
    }

    public void LoadPetsFightPropertyConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_pets_fight_property");
        _petsFightPropertyConfigData = JsonMapper.ToObject<Dictionary<string, Dictionary<string, PetsFightProperty>>>(textdata.text);
    }

    public PetsFightProperty GetPetsFightProperty(UInt32 utype, byte type)
    {
        string pid = Convert.ToString(utype);
        Dictionary<string, PetsFightProperty> dic;
        _petsFightPropertyConfigData.TryGetValue(pid, out dic);
        PetsFightProperty data;
        if (dic.Count == 0)
        {
            return null;
        }
        string t = Convert.ToString(type);
        dic.TryGetValue(t, out data);
        return data;
    }
}
