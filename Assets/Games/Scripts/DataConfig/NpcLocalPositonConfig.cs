﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class NpcLoaclPositonData
{
    public UInt32 id;
    public NpcLoaclPosVector2 localPosition;
}

public class NpcLoaclPosVector2
{
    public double x;
    public double y;

    public Vector3 ToVector3()
    {
        return new Vector3((float)x, (float)y, (float)y);
    }
}

public class NpcLocalPositonConfig 
{
    	private static NpcLocalPositonConfig _instance;

        private Dictionary<UInt16, Dictionary<string, NpcLoaclPositonData>>  _npcLocalPositon= new Dictionary<UInt16, Dictionary<string, NpcLoaclPositonData>>();
        public static NpcLocalPositonConfig SharedInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NpcLocalPositonConfig();
                }
                return _instance;
            }
        }

		private NpcLocalPositonConfig()
		{

		}

        public void LoadConfig(UInt16 sceneUType)
        {
            if (_npcLocalPositon.ContainsKey(sceneUType))
            {
                return;
            }

            string resPath = SceneConfig.SharedInstance.GetSceneData(sceneUType).resPath;
            TextAsset textdata = AssetsManager.instance.SyncSpawnConfig(resPath);
            Dictionary<string, NpcLoaclPositonData> posData = new Dictionary<string, NpcLoaclPositonData>();
            posData = JsonMapper.ToObject<Dictionary<string, NpcLoaclPositonData>>(textdata.text);
            _npcLocalPositon.Add(sceneUType, posData);
        }

        public NpcLoaclPosVector2 GetNpcLocalPositon(UInt16 sceneUType, UInt32 npcUType)
        {
            LoadConfig(sceneUType);

            Dictionary<string, NpcLoaclPositonData> data;
            _npcLocalPositon.TryGetValue(sceneUType, out data);
            if (data.Count == 0)
            {
                return null;
            }
            NpcLoaclPositonData nData;
            string nID = Convert.ToString(npcUType);
            data.TryGetValue(nID, out nData);
            if(nData == null)
            {
                return null;
            }
            return nData.localPosition;        
        }
}
