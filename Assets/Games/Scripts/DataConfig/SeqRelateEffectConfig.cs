﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class SeqRelateEffectData
{
    public string seqName;
    public List<string> effectNames;
}
public class SeqRelateEffectConfig : Singleton<SeqRelateEffectConfig>
{
    private Dictionary<string, SeqRelateEffectData> _dataDict = new Dictionary<string, SeqRelateEffectData>();
    protected override void OnInit()
    {
        LoadJson();
    }
    private void LoadJson()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("seq_relate_effect");
        _dataDict = JsonMapper.ToObject<Dictionary<string, SeqRelateEffectData>>(textdata.text);
    }

    private List<string> emptyList = new List<string>();
    public List<string> GetEffectNames(string seqName)
    {
        SeqRelateEffectData data;
        if (_dataDict.TryGetValue(seqName, out data))
        {
            return data.effectNames;
        }
        return emptyList;
    }
}
