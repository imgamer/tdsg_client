﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class EquipAdvanceData
{
    public List<int> material1 = new List<int>();
    public List<int> material2 = new List<int>();
    public List<int> material3 = new List<int>();
    public int consumesilver;
    public int nextquality;
}
public class EquipmentAdvanceConfig 
{
    private static EquipmentAdvanceConfig _instance;
    private Dictionary<string, Dictionary<string, EquipAdvanceData>> _equipmentAdvanceConfigData = new Dictionary<string, Dictionary<string, EquipAdvanceData>>();

    public static EquipmentAdvanceConfig SharedInstance
	{
		get
		{
            if (_instance == null)
			{
                _instance = new EquipmentAdvanceConfig();
			}
            return _instance;
		}
	}

    private EquipmentAdvanceConfig()
	{
        LoadEquipmentAdvanceConfig();
	}

    public void LoadEquipmentAdvanceConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("equipment_build");
        _equipmentAdvanceConfigData = JsonMapper.ToObject<Dictionary<string, Dictionary<string, EquipAdvanceData>>>(textdata.text);
	}

    public EquipAdvanceData GetEquipmentAdvanceData(int ID, int quality)
	{
        Dictionary<string, EquipAdvanceData> advanceData = new Dictionary<string,EquipAdvanceData>();
        _equipmentAdvanceConfigData.TryGetValue(Convert.ToString(ID), out advanceData);
        if(advanceData.Count == 0)
        {
            return null;
        }
        EquipAdvanceData data;
        advanceData.TryGetValue(Convert.ToString(quality), out data);
		return data;
	}
}
