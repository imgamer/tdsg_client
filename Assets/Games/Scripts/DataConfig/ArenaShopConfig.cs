﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using LitJson;

public class ArenaShopData
{
    public UInt16 id;
    public int itemId;
    public string itemName;
    public int itemPrice;
    public string description;
    public string icon;
}

public class ArenaShopConfig
{
     private static ArenaShopConfig _instance;
     private Dictionary<string, ArenaShopData> _arenaShopData = new Dictionary<string, ArenaShopData>();

    public static ArenaShopConfig SharedInstance
	{
		get
		{
            if (_instance == null)
			{
                _instance = new ArenaShopConfig();
			}
            return _instance;
		}
	}

    private ArenaShopConfig()
	{
        LoadArenaShopConfig();
	}

    public void LoadArenaShopConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_arena_shop");
        JsonReader reader = new JsonReader(textdata.text);
        reader.SkipNonMembers = true;
        _arenaShopData = JsonMapper.ToObject<Dictionary<string, ArenaShopData>>(reader);
	}

    public ArenaShopData GetArenaShopData(UInt16 id)
    {
        ArenaShopData data;
        _arenaShopData.TryGetValue(id.ToString(), out data);
        return data;
    }
}
