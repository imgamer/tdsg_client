﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;

public class SkillConfig : Singleton<SkillConfig>
{
    private Dictionary<int, JsonData> _dataDict = new Dictionary<int, JsonData>();
    protected override void OnInit()
    {
        LoadJson();
    }

    private void LoadJson()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_skills");
        JsonData skillJsonData = JsonMapper.ToObject(textdata.text);
        for (int i = 0; i < skillJsonData.Count; i++)
        {
            int id = (int)(skillJsonData[i]["id"]);
            _dataDict.Add(id, skillJsonData[i]);
        }
    }

    public JsonData GetJsonData(int skillID)
    {
        JsonData skillJsonData;
        if (_dataDict.TryGetValue(skillID, out skillJsonData))
        {
            return skillJsonData;
        }
        return null;
    }

    public string GetSeqName(int skillID)
    {
        JsonData skillJsonData;
        if (_dataDict.TryGetValue(skillID, out skillJsonData))
        {
            return (string)skillJsonData["seqName"];
        }
        return string.Empty;
    }
}
