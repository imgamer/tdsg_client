﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;

public class AttributeResData
{
	public int attack;
	public int water;
	public int wind;
	public int fire;
	public int ray;
	public int dark;
}

public class AttributeRestriction
{
	private static AttributeRestriction instance_;
	
	private Dictionary<int, AttributeResData> AttributeRestrictionData = new Dictionary<int, AttributeResData>();

	public static AttributeRestriction SharedInstance
	{ 
		get
		{
			if (instance_ == null)
			{
				instance_ = new AttributeRestriction();
			}
			return instance_;
		}
	}

	public AttributeRestriction()
	{
		LoadAttributeResJson();
	}

	public void LoadAttributeResJson()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("attribute_restriction");
        JsonData data = JsonMapper.ToObject(textdata.text);
        for (int i = 0; i < data.Count; i++)
        {
            AttributeResData attriData = new AttributeResData();
            attriData.attack = (int)data[i]["attack"];
            attriData.water = (int)data[i]["1"];
            attriData.fire = (int)data[i]["2"];
            attriData.wind = (int)data[i]["3"];
            attriData.ray = (int)data[i]["4"];
            attriData.dark = (int)data[i]["5"];
            AttributeRestrictionData.Add(attriData.attack, attriData);
        }
	}

	public AttributeResData GetAttributeResData(int attackAttribute )
	{
		AttributeResData data;
		AttributeRestrictionData.TryGetValue(attackAttribute,out data);
		return data;
	}

	public int GetAttributeResDamage(int attackAttribute ,int resAttribute)
	{
		AttributeResData data = GetAttributeResData(attackAttribute);
		if(resAttribute == 1)
			return data.water;
		else if(resAttribute == 2)
			return data.fire;
		else if(resAttribute == 3)
			return data.wind;
		else if(resAttribute == 4)
			return data.ray;
		else if(resAttribute == 5)
			return data.dark;
		else
			return 0;
	}
}
