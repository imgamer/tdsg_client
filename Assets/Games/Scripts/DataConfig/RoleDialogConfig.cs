﻿using UnityEngine;
using LitJson;
using System.Collections;
using System.Collections.Generic;

public class RoleDialogData
{
	public string type;
	public string icon;
	public string name;
	public string content;
}

public class RoleDialogConfig
{
	private static RoleDialogConfig instance_;
	private List<RoleDialogData> mRoleDialogDataList;
	private Dictionary<string, RoleDialogData> mRoleDialogDataDict;
	private Dictionary<string, Dictionary<string, RoleDialogData>> mRoleDialogConfigData;

	public static RoleDialogConfig SharedInstance
	{
		get
		{
			if (instance_ == null)
			{
				instance_ = new RoleDialogConfig();
			}
			return instance_;
		}
	}

	RoleDialogConfig()
	{
		mRoleDialogDataList = new List<RoleDialogData>();
		mRoleDialogDataDict = new Dictionary<string, RoleDialogData>();
		mRoleDialogConfigData = new Dictionary<string, Dictionary<string, RoleDialogData>>();
	}

	~RoleDialogConfig()
	{
		mRoleDialogDataList.Clear();
		mRoleDialogDataDict.Clear();
		mRoleDialogConfigData.Clear();
	}

	public List<RoleDialogData> GetRoleDialogDataList(uint dlgIndex)
	{
		LoadConfig(dlgIndex);
		return mRoleDialogDataList;
	}


	private void LoadConfig(uint dlgIndex)
	{
		mRoleDialogDataList.Clear();
		mRoleDialogDataDict.Clear();
		mRoleDialogConfigData.Clear();

        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_dialogs");
        mRoleDialogConfigData = JsonMapper.ToObject<Dictionary<string, Dictionary<string, RoleDialogData>>>(textdata.text);
        if (mRoleDialogConfigData.Count == 0)
        {
            Printer.LogWarning("RoleDialogConfig :: LoadConfig : (mRoleDialogConfigData.Count == 0)");
            return;
        }

        mRoleDialogConfigData.TryGetValue(dlgIndex.ToString(), out mRoleDialogDataDict);
        if (mRoleDialogDataDict.Count == 0)
        {
            Printer.LogWarning("RoleDialogConfig :: LoadConfig : (mRoleDialogDataDict.Count == 0)");
            return;
        }

        RoleDialogData roleDialogData = null;
        for (int i = 0; i < mRoleDialogDataDict.Count; ++i)
        {
            mRoleDialogDataDict.TryGetValue((i + 1).ToString(), out roleDialogData);
            if (roleDialogData == null)
            {
                Printer.LogWarning("RoleDialogConfig :: LoadConfig : (roleDialogData == null)");
                return;
            }
            //Common.DEBUG_MSG("RoleDialogConfig :: LoadConfig : type = " + roleDialogData.type + ", icon = " + roleDialogData.icon + ", name = " + roleDialogData.name + ", content = " + roleDialogData.content);
            mRoleDialogDataList.Add(roleDialogData);
            roleDialogData = null;
        }

        //清除缓存数据
        mRoleDialogDataDict.Clear();
        mRoleDialogConfigData.Clear();

	}
}
