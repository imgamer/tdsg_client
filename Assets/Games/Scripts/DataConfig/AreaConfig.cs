﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class AreaConfig
{
    private static AreaConfig _instance;
    private Dictionary<string, List<int>> _areaConfigData = new Dictionary<string, List<int>>();
    
    public static AreaConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new AreaConfig();
            }
            return _instance;
        }
    }
    
    private AreaConfig()
    {
        LoadAreaConfig();
    }
    
    public void LoadAreaConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("area");
        _areaConfigData = JsonMapper.ToObject<Dictionary<string, List<int>>>(textdata.text);
    }

    public List<int> GetAreaData(int id)
    {
        string mId = Convert.ToString(id);
        List<int> areaList;
        _areaConfigData.TryGetValue(mId,out areaList);
        return areaList;
    }
}
