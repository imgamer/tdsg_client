﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class GemCombinData
{
    public int nextgemlevel;
    public string name;
    public int nextgemid;
    public int id;
    public int level;
    public string nextgemname;
}
public class GemCombinConfig
{
    private static GemCombinConfig instance_;
    private Dictionary<string, GemCombinData> _gemCombinConfigData = new Dictionary<string, GemCombinData>();

    public static GemCombinConfig SharedInstance
    {
        get
        {
            if (instance_ == null)
            {
                instance_ = new GemCombinConfig();
            }
            return instance_;
        }
    }

    private GemCombinConfig()
    {
        LoadGemCombinConfig();
    }

    public void LoadGemCombinConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_gem_combine");
        _gemCombinConfigData = JsonMapper.ToObject<Dictionary<string, GemCombinData>>(textdata.text);
    }

    public GemCombinData GetGemCombinData(int id)
    {
        string index = Convert.ToString(id);
        GemCombinData data;
        _gemCombinConfigData.TryGetValue(index, out data);
        return data;
    }
}
