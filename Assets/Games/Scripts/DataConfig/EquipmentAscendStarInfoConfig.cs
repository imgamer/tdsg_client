﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class EquipmentAscendStarData
{
    public int star;
    public double markup;
    public int exp;
}

public class EquipmentAscendStarInfoConfig 
{

	private static EquipmentAscendStarInfoConfig _instance;
    private Dictionary<string, EquipmentAscendStarData> _equipAscendStarData = new Dictionary<string, EquipmentAscendStarData>();
	
	public static EquipmentAscendStarInfoConfig SharedInstance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new EquipmentAscendStarInfoConfig();
			}
			return _instance;
		}
	}

    private EquipmentAscendStarInfoConfig()
	{
        LoadEquipmentAscendStarInfoConfig();
	}

    public void LoadEquipmentAscendStarInfoConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("equipment_ascend_star_info");
        _equipAscendStarData = JsonMapper.ToObject<Dictionary<string, EquipmentAscendStarData>>(textdata.text);
	}

    public EquipmentAscendStarData GetAscendStarData(int star)
	{
        EquipmentAscendStarData ascendData;
        _equipAscendStarData.TryGetValue(Convert.ToString(star), out ascendData);
        return ascendData;
	}
}
