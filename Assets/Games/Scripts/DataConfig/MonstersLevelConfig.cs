﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class MonstersLevelData
{
	public int toughnessValue;
	public int sunderArmorValue;
	public int HP_Max;
	public int level;
	public int utype;
	public int attackValue;
	public int HP;
	public int criticalValue;
	public int parryValue;
	public int speedInitValue;
	public int effectResistValue;
	public int effectResistValueInit;
	public int criticalDamageValue;
	public int effectEnhanceValue;
	public int defenseValue;
	public int speed;
}

public class MonstersLevelConfig
{
	private static MonstersLevelConfig instance_;
	private Dictionary<string, Dictionary<string, MonstersLevelData>> MonstersLevelConfigData_ = new Dictionary<string, Dictionary<string, MonstersLevelData>>();
	
	public static MonstersLevelConfig SharedInstance
	{
		get
		{
			if(instance_ == null)
			{
				instance_ = new MonstersLevelConfig();
			}
			return instance_;
		}
	}
	
	public MonstersLevelConfig()
	{
		LoadMonstersLevelConfig();
	}
	
	public void LoadMonstersLevelConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("monster_level");
        MonstersLevelConfigData_ = JsonMapper.ToObject<Dictionary<string, Dictionary<string, MonstersLevelData>>>(textdata.text);
	}
	
	public MonstersLevelData GetMonstersLevelData(int utype,int level)
	{
		string mUtype = Convert.ToString(utype);
		string mLevel = Convert.ToString(level);
		Dictionary<string, MonstersLevelData> hlData;
		MonstersLevelConfigData_.TryGetValue(mUtype,out hlData);
		if(hlData == null)
			return null;
		MonstersLevelData data;
		hlData.TryGetValue(mLevel,out data);
		if(data == null)
			return null;
		else
			return data;
	}
}
