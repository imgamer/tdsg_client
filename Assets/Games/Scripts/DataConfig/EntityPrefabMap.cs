﻿using System.Collections.Generic;
using UnityEngine;
using System;
using LitJson;

public class BornPointData
{
	public string positionX {get;set;}
	public string positionY {get;set;}
	public string positionZ {get;set;}
	public string rotationY {get;set;}
}

public class CameraConfigData
{
	public string sceneName {get;set;}
	public Vector3 position {get;set;}
	public Vector3 rotation {get;set;}
}

public class EntityPrefabData
{
	public int utype {get;set;}
	public string prefabName {get;set;}
	public string wakeupName {get;set;}
}

public class EntityPrefabMap : MonoSingleton<EntityPrefabMap>
{
	private Dictionary<int, EntityPrefabData> entityPrefabDatas = new Dictionary<int, EntityPrefabData>();
	private Dictionary<string, CameraConfigData> cameraConfig = new Dictionary<string, CameraConfigData>();
	private Dictionary<string, Dictionary<string, BornPointData>> bornPointConfig = new Dictionary<string, Dictionary<string, BornPointData>>();

    protected override void OnInit()
    {
        TextAsset textdata1 = AssetsManager.instance.SyncSpawnConfig("entity_prefab_map");
        JsonData data1 = JsonMapper.ToObject(textdata1.text);
        for (int i = 0; i < data1.Count; i++)
        {
            EntityPrefabData entityData = new EntityPrefabData();
            entityData.prefabName = (string)data1[i]["0"];
            entityData.wakeupName = (string)data1[i]["1"];
            entityData.utype = (int)data1[i]["utype"];
            entityPrefabDatas.Add(entityData.utype, entityData);
        }

        TextAsset textdata2 = AssetsManager.instance.SyncSpawnConfig("camera");
        JsonData data2 = JsonMapper.ToObject(textdata2.text);
        for (int j = 0; j < data2.Count; j++)
        {
            CameraConfigData cameraData = new CameraConfigData();
            cameraData.sceneName = (string)data2[j]["sceneName"];
            double x = (double)data2[j]["position"]["positionX"];
            double y = (double)data2[j]["position"]["positionY"];
            double z = (double)data2[j]["position"]["positionZ"];
            cameraData.position = new Vector3((float)x, (float)y, (float)z);

            x = (double)data2[j]["rotation"]["rotationX"];
            y = (double)data2[j]["rotation"]["rotationY"];
            z = (double)data2[j]["rotation"]["rotationZ"];
            cameraData.rotation = new Vector3((float)x, (float)y, (float)z);

            cameraConfig.Add(cameraData.sceneName, cameraData);
        }


        TextAsset textdata3 = AssetsManager.instance.SyncSpawnConfig("born_point");
        bornPointConfig = JsonMapper.ToObject<Dictionary<string, Dictionary<string, BornPointData>>>(textdata3.text);
    }

    protected override void OnUnInit()
    {
        entityPrefabDatas.Clear();
        bornPointConfig.Clear();
        cameraConfig.Clear();
    }
	
	public string getAssetName(int id,bool wakeup)
	{
		EntityPrefabData value;
		if (!entityPrefabDatas.TryGetValue(id, out value))
		{
			return string.Empty;
		}
		
		if(wakeup)
			return value.wakeupName;
		else
			return value.prefabName;
	}
	
	public void SetPresetCamera(GameObject go, string scene)
	{
		if(go == null) return;
		if(!cameraConfig.ContainsKey(scene))
		{
			Printer.LogWarning("No camera config for " + scene + "found");
			return;
		}
		
		var cam = go.GetComponent<Camera>();
		if( cam == null)
		{
			Printer.LogWarning("No Camera component on object found");
			return;
		}
		var data = cameraConfig[scene];
		cam.transform.position = data.position;
		cam.transform.localRotation = Quaternion.Euler(data.rotation);
	}
	
	public void SetPresetTransfrom(GameObject obj, string scene, int index)
	{
		if(!bornPointConfig.ContainsKey(scene))
		{
			Printer.LogError("No born point config for " + scene + " found");
			return;
		}
		
		Dictionary<string, BornPointData> bornData;
		bornPointConfig.TryGetValue(scene,out bornData);
		
		string bornIndex = Convert.ToString(index);
		if(!bornData.ContainsKey(bornIndex))
			return;
		BornPointData bpData;
		bornData.TryGetValue(bornIndex,out bpData);
		
		float x = float.Parse(bpData.positionX);
		float y = float.Parse(bpData.positionY);
		float z = float.Parse(bpData.positionZ);
		obj.transform.position = new Vector3(x, y, z);
		obj.transform.localRotation = Quaternion.Euler(0, float.Parse(bpData.rotationY), 0);
		return;				
	}

    BornPointData GetPresetBornPoint(string scene, int index)
    {
        if (!bornPointConfig.ContainsKey(scene))
        {
            Printer.LogWarning("No born point config for " + scene + " found");
            return null;
        }

        Dictionary<string, BornPointData> bornData;
        bornPointConfig.TryGetValue(scene, out bornData);

        string bornIndex = Convert.ToString(index);
        if (!bornData.ContainsKey(bornIndex))
            return null;
        BornPointData bpData;
        bornData.TryGetValue(bornIndex, out bpData);
        return bpData;
    }

    public Vector3 GetPresetPosition(string scene, int index)
    {
        var data = GetPresetBornPoint(scene, index);
        if (data != null)
        {
            float x = float.Parse(data.positionX);
            float y = float.Parse(data.positionY);
            float z = float.Parse(data.positionZ);
            return new Vector3(x, y, z);
        }

        return Vector3.zero;
    }

    public Vector3 GetPresetRotation(string scene, int index)
    {
        var data = GetPresetBornPoint(scene, index);
        if (data != null)
        {
            var y = float.Parse(data.rotationY);
            return new Vector3(0, y, 0);
        }

        return Vector3.zero;
    }
}
