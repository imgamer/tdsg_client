﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class NpcMonsterData
{
	public string modelID;
	public int quality;
	public int level;
	public string name;
	public string entityType;
	public int moveSpeed;
	public int headID;
	public int career;
	public string scriptType;
	public List<int> skills = new List<int>();
	public string description;
	public int utype;
	public int attributeVal;
	public int potraitID;
	public int starLevel;
	public string type;
	public string path;
	public string clientScript;
    public int dropBox;
    public string icon;
}


public class NpcMonsterConfig 
{
	
	private static NpcMonsterConfig instance_;
	private Dictionary<string, NpcMonsterData> npcMonsterConfigData_ = new Dictionary<string, NpcMonsterData>();
		
	public static NpcMonsterConfig SharedInstance
	{
		get
		{
			if(instance_ == null)
			{
				instance_ = new NpcMonsterConfig();
			}
			return instance_;
		}
	}
		
	private NpcMonsterConfig()
	{
		LoadNpcMonsterConfig();
	}
		
	public void LoadNpcMonsterConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_npc_monster");
        npcMonsterConfigData_ = JsonMapper.ToObject<Dictionary<string, NpcMonsterData>>(textdata.text);
	}
		
	public NpcMonsterData GetNpcMonsterData(UInt32 utype)
	{
        string index = Convert.ToString(utype);
		NpcMonsterData data;
		npcMonsterConfigData_.TryGetValue(index,out data);
		return data;
	}
}
