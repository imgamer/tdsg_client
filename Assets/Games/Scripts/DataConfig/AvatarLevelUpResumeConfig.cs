﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class AvatarLevelUpResumeData
{
	public int EXP;
}

public class AvatarLevelUpResumeConfig 
{
	private static AvatarLevelUpResumeConfig _instance;
	private Dictionary<string, AvatarLevelUpResumeData> _avatarLevelUpResumeConfigData = new Dictionary<string, AvatarLevelUpResumeData>();
	
	public static AvatarLevelUpResumeConfig SharedInstance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new AvatarLevelUpResumeConfig();
			}
			return _instance;
		}
	}
	
	private AvatarLevelUpResumeConfig()
	{
		LoadAvatarLevelUpResumeConfig();
	}
	
	public void LoadAvatarLevelUpResumeConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("avatar_level_up_resume");
        _avatarLevelUpResumeConfigData = JsonMapper.ToObject<Dictionary<string, AvatarLevelUpResumeData>>(textdata.text);
	}
	
	public AvatarLevelUpResumeData GetAvatarLevelUpResumeData(int level)
	{
		string mLevel = Convert.ToString(level);
		AvatarLevelUpResumeData levelUpData;
		_avatarLevelUpResumeConfigData.TryGetValue(mLevel,out levelUpData);
		return levelUpData;
	}
}
