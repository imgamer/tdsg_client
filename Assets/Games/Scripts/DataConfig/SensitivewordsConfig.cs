﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;

public class SensitivewordsConfig : Singleton<SensitivewordsConfig>
{
    private List<string> _sensitiveWordList = new List<string>();
    public List<string> SensitiveWordList
    {
        get { return _sensitiveWordList; }
    }
    protected override void OnInit()
    {
        LoadSensitivewordsConfig();
    }
    private void LoadSensitivewordsConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_sensitive_words");
        _sensitiveWordList = JsonMapper.ToObject<List<string>>(textdata.text);
    }
}
