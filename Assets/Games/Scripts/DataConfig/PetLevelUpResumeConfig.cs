﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class PetLevelUpResume
{
    public int EXP;
}
public class PetLevelUpResumeConfig 
{
      private static PetLevelUpResumeConfig _instance;
      private Dictionary<string, PetLevelUpResume> _petLevelUpResumeConfigData = new Dictionary<string, PetLevelUpResume>();
    
    public static PetLevelUpResumeConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new PetLevelUpResumeConfig();
            }
            return _instance;
        }
    }

    private PetLevelUpResumeConfig()
    {
        LoadPetLevelUpResumeConfig();
    }

    public void LoadPetLevelUpResumeConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("pet_level_up_resume");
        _petLevelUpResumeConfigData = JsonMapper.ToObject<Dictionary<string, PetLevelUpResume>>(textdata.text);       
    }

    public PetLevelUpResume GetPetLevelUpResumeData(int level)
    {
        string pLevel = Convert.ToString(level);
        PetLevelUpResume data;
        _petLevelUpResumeConfigData.TryGetValue(pLevel, out data);
        return data;
    }
}
