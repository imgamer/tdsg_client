﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class PetSkillUpResume
{
    public int upgradeNeedStones;
}
public class PetSkillUpResumeConfig
{
    private static PetSkillUpResumeConfig _instance;
    private Dictionary<string, PetSkillUpResume> _petSkillUpResumeConfigData = new Dictionary<string, PetSkillUpResume>();

    public static PetSkillUpResumeConfig SharedInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PetSkillUpResumeConfig();
            }
            return _instance;
        }
    }

    private PetSkillUpResumeConfig()
    {
        LoadPetSkillUpResumeConfig();
    }

    public void LoadPetSkillUpResumeConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("pet_skill_up_resume");
        _petSkillUpResumeConfigData = JsonMapper.ToObject<Dictionary<string, PetSkillUpResume>>(textdata.text);
    }

    public PetSkillUpResume GetPetSkillUpResumeData(int level)
    {
        string pLevel = Convert.ToString(level);
        PetSkillUpResume data;
        _petSkillUpResumeConfigData.TryGetValue(pLevel, out data);
        return data;
    }
}
