﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class VipLevelData
{
    public int vipID;
    public int handofMidas;
    public int sweepTicket;
    public int buyPhysical;
    public string description;
    public int number;
    public int money;
    public string icon;
}

public class VipLevelConfig 
{
    private static VipLevelConfig instance_;
    private Dictionary<string, VipLevelData> vipLevelConfigData_ = new Dictionary<string, VipLevelData>();
    
    public static VipLevelConfig SharedInstance
    {
        get
        {
            if(instance_ == null)
            {
                instance_ = new VipLevelConfig();
            }
            return instance_;
        }
    }
    
    private VipLevelConfig()
    {
        LoadVipLevelConfig();
    }
    
    public void LoadVipLevelConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("vip_level");
        vipLevelConfigData_ = JsonMapper.ToObject<Dictionary<string, VipLevelData>>(textdata.text);
    }
    
    public VipLevelData GetVipLevelData(Int32 id)
    {
        string index = Convert.ToString(id);
        VipLevelData data;
        vipLevelConfigData_.TryGetValue(index,out data);
        return data;
    }

}
