﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class MonsterData
{
	public int level;
	public UInt32 utype;
}

public class MonsterDatasConfig
{
	private static MonsterDatasConfig _instance;
	private Dictionary<string, List<MonsterData>> _MonsterConfigData = new Dictionary<string, List<MonsterData>>();
    
	public static MonsterDatasConfig SharedInstance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new MonsterDatasConfig();
			}
			return _instance;
		}
	}
    
	private MonsterDatasConfig()
	{
		LoadMonsterDatasConfig();
	}
    
	public void LoadMonsterDatasConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("monster_datas");
        _MonsterConfigData = JsonMapper.ToObject<Dictionary<string, List<MonsterData>>>(textdata.text); 		
	}

	public List<MonsterData> GetMonsterData(int id)
	{
		string mId = Convert.ToString(id);
		List<MonsterData> monsterData;
		_MonsterConfigData.TryGetValue(mId, out monsterData);
		return monsterData;
	}
}
