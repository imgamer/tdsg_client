﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class QuestGroup
{
	public int questType;
	public int questGroup;

}

public class QuestsGroupConfig: Singleton<QuestsGroupConfig>
{
	private Dictionary<string, QuestGroup> _data = new Dictionary<string, QuestGroup>();
     protected override void OnInit()
    {
        LoadHerosConfig();
    }

	public void LoadHerosConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_quests_group");
        _data = JsonMapper.ToObject<Dictionary<string, QuestGroup>>(textdata.text);
	}

    public QuestGroup GetQuestGroup(int questType)
	{
        string index = Convert.ToString(questType);
		QuestGroup data;
		_data.TryGetValue(index,out data);
		return data;
	}

    public byte QuestTypeToGroup(int questType)
    {
        QuestGroup group = GetQuestGroup(questType);
        if (group == null)
        {
            return 0;
        }
        else
        {
            return (byte)group.questGroup;

        }
    }
}

