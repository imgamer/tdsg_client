﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class CharacterPlusPointData
{
    public uint force;
    public uint vitality;
    public uint spirituality;
    public uint gengu;
    public string description;
}
/// <summary>
/// 角色加点推荐配置表
/// </summary>
public class CharacterPlusPointHelpConfig : MonoSingleton<CharacterPlusPointHelpConfig>
{
    public int currentRoleRecommendNum { get; private set;}
    private Dictionary<string, Dictionary<string, CharacterPlusPointData>> _dicData = new Dictionary<string,Dictionary<string,CharacterPlusPointData>>();

    protected override void OnInit()
    {
        LoadCharacterPlusPointHelpConfig();
    }

    protected override void OnUnInit()
    {

    }

    private void LoadCharacterPlusPointHelpConfig()
    {
        TextAsset textData = AssetsManager.instance.SyncSpawnConfig("d_avatarpoint_recommend_assignment");
        JsonReader reader = new JsonReader(textData.text);
        _dicData = JsonMapper.ToObject<Dictionary<string, Dictionary<string, CharacterPlusPointData>>>(reader);
        
        Dictionary<string, CharacterPlusPointData> dic = new Dictionary<string, CharacterPlusPointData>();
        if (_dicData.TryGetValue(GameMain.Player.School.ToString(), out dic)) currentRoleRecommendNum = dic.Count;
    }

    /// <summary>
    /// 推荐方案条数
    /// 参数 recommendNum 从 '0' 开始 相当于配置表第'1'方案
    /// </summary>
    public CharacterPlusPointData GetCurrentRoleRecommendData(int recommendNum)
    {
        Dictionary<string, CharacterPlusPointData> dic = new Dictionary<string, CharacterPlusPointData>();
        if (_dicData.TryGetValue(GameMain.Player.School.ToString(), out dic))
        {
            CharacterPlusPointData currentRoledata;
            dic.TryGetValue((recommendNum + 1).ToString(), out currentRoledata);
            return currentRoledata;
        }
        return null;
    }
}
