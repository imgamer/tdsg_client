﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;
using ENTITY_UTYPE = System.UInt32;

public class HerosData
{
    public int utype;
    public UInt32 school;
    public string name;
    public string description;

    public string entityType;
    public string scriptType;
    public string clientScript;

    public string modelID;
    public string portraitID;
    public int takeLevel;

    public List<int> skills = new List<int>();
    public List<int> skillsFriendship = new List<int>();

    public int HP;
    public int HP_Max;

    public int MP;
    public int MP_Max;

    public int physicsDefense;
    public int physicsDefense_Max;

    public int physicsPower;
    public int physicsPower_Max;

    public int magicDefense;
    public int magicDefense_Max;

    public int magicPower;
    public int magicPower_Max;
    public int convertCost;
}

public class HerosConfig
{
	private static HerosConfig instance_;
	private Dictionary<string, HerosData> _herosConfigData = new Dictionary<string, HerosData>();
	
	public static HerosConfig SharedInstance
	{
		get
		{
			if(instance_ == null)
			{
				instance_ = new HerosConfig();
			}
			return instance_;
		}
	}
	
	private HerosConfig()
	{
		LoadHerosConfig();
	}

	public void LoadHerosConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_heros");
        JsonReader reader = new JsonReader(textdata.text);
        reader.SkipNonMembers = true;
        _herosConfigData = JsonMapper.ToObject<Dictionary<string, HerosData>>(reader);
	}

    public  List<HerosData> GetAllHerosData()
    {
        List < HerosData > list =  new List<HerosData>(_herosConfigData.Values);
        list.Sort((HerosData a, HerosData b) =>
        {
            return a.takeLevel.CompareTo(b.takeLevel);
        });
        return list;
    }

    public HerosData GetHerosData(ENTITY_UTYPE utype)
	{
        string index = Convert.ToString(utype);
		HerosData data;
		_herosConfigData.TryGetValue(index,out data);
		return data;
	}

    public T GetConfig<T>(ENTITY_UTYPE utype, string propertyName)
    {
        HerosData data = GetHerosData(utype);
        if (data == null) return default(T);

        System.Reflection.FieldInfo p = data.GetType().GetField(propertyName);
        if (p == null)
            return default(T);
        else
            return (T)p.GetValue(data);
    }
}
