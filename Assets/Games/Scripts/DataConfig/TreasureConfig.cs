﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class TreasureData
{
    public int boxID;
    public int boxType;
    public int itemPercent;
    public int item;
    public int itemCount;
}

public class TreasureConfig
{
    private static TreasureConfig _instance;
    private Dictionary<string, List<TreasureData>> _TreasureConfigData = new Dictionary<string, List<TreasureData>>();
    
    public static TreasureConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new TreasureConfig();
            }
            return _instance;
        }
    }
    
    private TreasureConfig()
    {
        LoadTreasureConfig();
    }
    
    public void LoadTreasureConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_treasure");
        _TreasureConfigData = JsonMapper.ToObject<Dictionary<string, List<TreasureData>>>(textdata.text);
    }

    public List<TreasureData> GetTreasureData(int drop)
    {
        string mDrop = Convert.ToString(drop);
        List<TreasureData> dropData;
        _TreasureConfigData.TryGetValue(mDrop,out dropData);
        return dropData;
    }
}
