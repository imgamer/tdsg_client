﻿using System;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
public class PetStageEnhanceData
{
    public double growthFactor;                     //资质加成
    public double growthFactorUpperLimit;           //资质上限加成
    public double hpFactorUpperLimit;               //生命成长上限加成
    public double magicPowerFactorUpperLimit;       //法攻成长上限加成
    public double magicDefenseFactorUpperLimit;     //法防成长上限加成
    public double physicsPowerFactorUpperLimit;     //物攻成长上限加成
    public double physicsDefenseFactorUpperLimit;   //物防成长上限加成
}

public class PetStageEnhanceConfig : Singleton<PetStageEnhanceConfig>
{
    private Dictionary<string, PetStageEnhanceData> _datas;

    protected override void OnInit()
    {
        Load();
    }

    private void Load()
    {
        TextAsset asset = AssetsManager.instance.SyncSpawnConfig("pet_stage_enhance");
        _datas = JsonMapper.ToObject<Dictionary<string, PetStageEnhanceData>>(asset.text);
    }

    public PetStageEnhanceData GetEnhanceByStage(Byte stage)
    {
        PetStageEnhanceData data;
        if (_datas.TryGetValue(stage.ToString(), out data))
            return data;
        else
            return null;
    }
}
