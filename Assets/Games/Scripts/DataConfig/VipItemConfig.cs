﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using LitJson;

public class VipItemData
{
    public string name;
    public int type;
    public int money;
    public int ingot;
    public int vipItemID;
    public string description;
    public string affect;
    public string icon;
    public int extraIngot;
}

public class VipItemConfig 
{
    private static VipItemConfig instance_;
    private Dictionary<string, VipItemData> vipItemConfigData_ = new Dictionary<string, VipItemData>();
    
    public static VipItemConfig SharedInstance
    {
        get
        {
            if(instance_ == null)
            {
                instance_ = new VipItemConfig();
            }
            return instance_;
        }
    }
    
    private VipItemConfig()
    {
        LoadVipItemConfig();
    }
    
    public void LoadVipItemConfig()
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("vip_item");
        vipItemConfigData_ = JsonMapper.ToObject<Dictionary<string, VipItemData>>(textdata.text);
    }
    
    public VipItemData GetVipItemData(Int32 id)
    {
        string index = Convert.ToString(id);
        VipItemData data;
        vipItemConfigData_.TryGetValue(index,out data);
        return data;
    }
}
