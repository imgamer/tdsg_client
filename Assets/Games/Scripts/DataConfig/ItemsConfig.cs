﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System;

public class ItemsData
{
	public int id; //编号
	public int amount;   //数量
	public string script;   //脚本
	public string description;  //描述
	public string name;  //名称
	public int childType;  //子类
	public int stackable;   //可叠加数
	public int quality;    //品质
	public string icon;  //图标
	public int type;  //大类
	public int price; //出售价格
	public int goldCost; //购买金币
	public int ingotCost; //购买元宝
	public string param1; //参数1
    public string param2; //参数2
    public string param3; //参数3
    public int level;
    public int durability;
    public int gemType;
}

public class ItemsConfig : Singleton<ItemsConfig>
{
	private Dictionary<string, ItemsData> itemsConfigData_ = new Dictionary<string, ItemsData>();

    protected override void OnInit()
    {
        LoadItemsConfig();
    }

	private void LoadItemsConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_items");
        JsonReader reader = new JsonReader(textdata.text);
        reader.SkipNonMembers = true;
        itemsConfigData_ = JsonMapper.ToObject<Dictionary<string, ItemsData>>(textdata.text);
	}

	public ItemsData GetItemData(Int64 id)
	{
		string index = Convert.ToString(id);
		ItemsData data;
		itemsConfigData_.TryGetValue(index,out data);
		return data;
	}

    public string GetItemName(ItemsData item)
    {
        if (item == null) return string.Empty;
        return string.Format(GetItemColor(item.quality), item.name);
    }

    private static string GetItemColor(int quality)
    {
        string color = string.Empty;
        switch (quality)
        {
            case ItemTypeDefine.ITEM_QUALITY_ONE:
                color = "[FFFFFF]{0}[-]";
                break;
            case ItemTypeDefine.ITEM_QUALITY_TWO:
                color = "[7CFC00]{0}[-]";
                break;
            case ItemTypeDefine.ITEM_QUALITY_THREE:
                color = "[0000FF]{0}[-]";
                break;
            case ItemTypeDefine.ITEM_QUALITY_FOUR:
                color = "[912CEE]{0}[-]";
                break;
            case ItemTypeDefine.ITEM_QUALITY_FIVE:
                color = "[EE7600]{0}[-]";
                break;
            default:
                break;
        }
        return color;
    }
}
