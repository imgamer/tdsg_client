﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class PetsFactors
{
    public List<double> growthFactor;
    public List<int> physicsPowerFactor;
    public List<int> physicsDefenseFactor;
    public List<int> magicPowerFactor;
    public List<int> magicDefenseFactor;
    public List<int> hpFactor;
 }

public class PetsFactorsConfig 
{
    private static PetsFactorsConfig _instance;
    private Dictionary<string, Dictionary<string, PetsFactors>> _petsFactorsConfigData = new Dictionary<string, Dictionary<string, PetsFactors>>();

    public static PetsFactorsConfig SharedInstance
	{
		get
		{
            if (_instance == null)
			{
                _instance = new PetsFactorsConfig();
			}
            return _instance;
		}
	}

    private PetsFactorsConfig()
	{
        LoadPetsFactorsConfigConfig();
	}

    public void LoadPetsFactorsConfigConfig()
	{
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_pets_factors");
        _petsFactorsConfigData = JsonMapper.ToObject<Dictionary<string, Dictionary<string, PetsFactors>>>(textdata.text);
	}

    public PetsFactors GetPetFactorsData(UInt32 utype, byte type)
	{
        string pid = Convert.ToString(utype);
        Dictionary<string, PetsFactors> dic;
        _petsFactorsConfigData.TryGetValue(pid, out dic);
        PetsFactors data;
		if(dic.Count == 0)
        {
            return null;
        }
        string t = Convert.ToString(type);
        dic.TryGetValue(t, out data);
        return data;
	}
}
