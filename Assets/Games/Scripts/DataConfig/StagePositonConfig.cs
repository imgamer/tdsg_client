﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class StagePositonData
{
    public HeroMonsterPositon Hero;
    public HeroMonsterPositon Monster;
}

public class HeroMonsterPositon
{
    public StageFightPosition StageFight;
    public StageFightPosition StageFightBoss;
}

public class StageFightPosition
{
    public List<int> fight_count_1 = new List<int>();
    public List<int> fight_count_2 = new List<int>();
    public List<int> fight_count_3 = new List<int>();
    public List<int> fight_count_4 = new List<int>();
    public List<int> fight_count_5 = new List<int>();
    public List<int> fight_count_6 = new List<int>();
}

public class StagePositonConfig
{
    private static StagePositonConfig _instance;
    private Dictionary<string, StagePositonData> _stagePositonConfigData = new Dictionary<string, StagePositonData>();
    
    public static StagePositonConfig SharedInstance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new StagePositonConfig();
            }
            return _instance;
        }
    }
    
    private StagePositonConfig()
    {

    }
    
    public void LoadStagePositonConfig(string sceneName)
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig(sceneName);
        _stagePositonConfigData = JsonMapper.ToObject<Dictionary<string, StagePositonData>>(textdata.text);
    }

    public List<int> GetStagePositonData(int stageNum, string type, string stageName, int fightCount)
    {
        string mStageNum = Convert.ToString(stageNum);
        StagePositonData pData;
        _stagePositonConfigData.TryGetValue(mStageNum,out pData);
        if(pData == null)
        {
            return null;
        }

        List<int> fightCountData = new List<int>();
        HeroMonsterPositon heroMonsterPos = new HeroMonsterPositon();
        StageFightPosition stageFightPos = new StageFightPosition();
        if(type == "Hero")
        {
            heroMonsterPos = pData.Hero;
        }
        else if(type == "Monster")
        {
            heroMonsterPos = pData.Monster;
        }

        if(stageName == "StageFight")
        {
            stageFightPos = heroMonsterPos.StageFight;
        }
        else if(stageName == "StageFightBoss")
        {
            stageFightPos = heroMonsterPos.StageFightBoss;
        }

        switch (fightCount)
        {
            case 1: 
                fightCountData = stageFightPos.fight_count_1;
                break;
            case 2:
                fightCountData = stageFightPos.fight_count_2;
                break;
            case 3:
                fightCountData = stageFightPos.fight_count_3;
                break;
            case 4:
                fightCountData = stageFightPos.fight_count_4;
                break;
            case 5:
                fightCountData = stageFightPos.fight_count_5;
                break;
            case 6:
                fightCountData = stageFightPos.fight_count_6;
                break;
            default:
                break;
        }
        return fightCountData;
    }
}
