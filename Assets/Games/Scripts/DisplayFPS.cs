﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 显示FPS
/// </summary>
public class DisplayFPS : MonoBehaviour
{
    /// <summary>
    /// 每次刷新计算的时间      帧/秒
    /// </summary>
    public float updateInterval = 0.5f;
    /// <summary>
    /// 最后间隔结束时间
    /// </summary>
    private double lastInterval;
    private int frames = 0;
    private float currFPS;


    void Start()
    {
        lastInterval = Time.unscaledTime;
        frames = 0;

        style = new GUIStyle();
        style.fontSize = 30;
        style.alignment = TextAnchor.UpperCenter;
    }

    void Update()
    {
        ++frames;
        float timeNow = Time.unscaledTime;
        if (timeNow > lastInterval + updateInterval)
        {
            currFPS = (float)(frames / (timeNow - lastInterval));
            frames = 0;
            lastInterval = timeNow;
        }
    }

    private GUIStyle style;
    private void OnGUI()
    {
        if (currFPS > 20f)
        {
            style.normal.textColor = Color.green;
        }
        else
        {
            style.normal.textColor = Color.red;
        }
        GUILayout.TextField("FPS:" + currFPS.ToString("f2"), style);
    }

}