﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 角色加点推荐项目
/// </summary>
public class CharacterPointRecommendItem : WinItem
{
    private UILabel _top;
    public string Top
    {
        set { _top.text = string.Format(" • {0}", value); }
    }
    private UILabel _description;
    public string Description
    {
        set { _description.text = value; }
    }
    private UILabel _recommendPoint;
    public string RecommendPoint
    {
        set { _recommendPoint.text = value; }
    }

    protected override void OnInit()
    {
        _top = transform.Find("Top").GetComponent<UILabel>();
        _description = transform.Find("Description").GetComponent<UILabel>();
        _recommendPoint = transform.Find("Recommend").GetComponent<UILabel>();
    }
}
