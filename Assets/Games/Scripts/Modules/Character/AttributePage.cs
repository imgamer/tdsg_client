﻿using UnityEngine;
using System.Collections;
using System;

public class AttributePage : WinPage
{
    private KBEngine.Avatar _avatar;
	protected override void OnInit()
	{
        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdateAttribute);
	}
	
	protected override void OnOpen(params object[] args)
	{
		
	}
	
	protected override void OnRefresh()
	{
        RefreshValue();
	}
	
	protected override void OnClose()
	{
		
	}
	
	protected override void OnUnInit()
	{
        EventManager.RemoveListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdateAttribute);
	}

    private UILabel _name;
    private UILabel _level;
    private UIProgressBar _hpBar;
    private UIProgressBar _mpBar;
    private UIProgressBar _dpBar;
    private UIProgressBar _expBar;
    private UILabel _hp;
    private UILabel _mp;
    private UILabel _dp;
    private UILabel _exp;

    private GameObject _baseBtn;

    private UILabel _phyAttack;
    private UILabel _phyDefend;
    private UILabel _mgcAttack;
    private UILabel _mgcDefend;
    private UILabel _treatmentStrength;
    private UILabel _speed;

    private UILabel _crit;
    private UILabel _tough;
    private UILabel _hit;
    private UILabel _dodge;

    private GameObject _characterRoot;
    private void InitPage()
    {
        _name = transform.Find("Text/Name").GetComponent<UILabel>();
        _level  = transform.Find("Text/Level").GetComponent<UILabel>();

        _hpBar = transform.Find("HPProBar").GetComponent<UIProgressBar>();
        _mpBar = transform.Find("MPProBar").GetComponent<UIProgressBar>();
        _dpBar = transform.Find("DPProBar").GetComponent<UIProgressBar>();
        _expBar = transform.Find("EXPProBar").GetComponent<UIProgressBar>();

        _hp = transform.Find("AttributeNum/HP").GetComponent<UILabel>();
        _mp = transform.Find("AttributeNum/MP").GetComponent<UILabel>();
        _dp = transform.Find("AttributeNum/DP").GetComponent<UILabel>();
        _exp = transform.Find("AttributeNum/EXP").GetComponent<UILabel>();

        GameObject baseObj = transform.Find("Base").gameObject;
        GameObject advanceObj = transform.Find("Advanced").gameObject;

        GameObject baseBtn = transform.Find("BaseBtn").gameObject;
        GameObject btnBg = transform.Find("BtnBg").gameObject;
        btnBg.transform.parent = baseBtn.transform;
        btnBg.transform.localPosition = Vector3.zero;
        baseObj.SetActive(true);
        advanceObj.SetActive(false);
        InputManager.instance.AddClickListener(baseBtn, (go) =>
        {
            btnBg.transform.parent = baseBtn.transform;
            btnBg.transform.localPosition = Vector3.zero;

            baseObj.SetActive(true);
            advanceObj.SetActive(false);
        });

        GameObject advancedBtn = transform.Find("AdvancedBtn").gameObject;
        InputManager.instance.AddClickListener(advancedBtn, (go) =>
        {
            btnBg.transform.parent = advancedBtn.transform;
            btnBg.transform.localPosition = Vector3.zero;

            baseObj.SetActive(false);
            advanceObj.SetActive(true);
        });

        _phyAttack = transform.Find("Base/PhyAttack").GetComponent<UILabel>();
        _phyDefend = transform.Find("Base/PhyDefend").GetComponent<UILabel>();        
        _mgcAttack = transform.Find("Base/MgicAttack").GetComponent<UILabel>();
        _mgcDefend = transform.Find("Base/MgicDefend").GetComponent<UILabel>();
        _treatmentStrength = transform.Find("Base/TreatmentStrength").GetComponent<UILabel>();
        _speed = transform.Find("Base/Speed").GetComponent<UILabel>();

        _crit = transform.Find("Advanced/Crit").GetComponent<UILabel>();
        _tough = transform.Find("Advanced/Tough").GetComponent<UILabel>();
        _hit = transform.Find("Advanced/Hit").GetComponent<UILabel>();
        _dodge = transform.Find("Advanced/Dodge").GetComponent<UILabel>();

        _characterRoot = transform.Find("Character").gameObject;
    }

    private void RefreshValue()
    {
        _name.text = _avatar.Name;
        _level.text = Convert.ToString(_avatar.Level);
        _hp.text = String.Format("{0}/{1}", _avatar.HP, _avatar.HP_Max);
        _mp.text = String.Format("{0}/{1}", _avatar.MP, _avatar.MP_Max);
        var percent = (float)_avatar.HP / (float)_avatar.HP_Max;
        _hpBar.value = Mathf.Max(0, percent);
        if(_avatar.MP_Max == 0)
        {
            _mpBar.value = Mathf.Max(0, 0);
        }
        else
        {
            percent = (float)_avatar.MP / (float)_avatar.MP_Max;
            _mpBar.value = Mathf.Max(0, percent);
        }
        
        //临时
        _dp.text = "0/0";
        _dpBar.value = Mathf.Max(0, 0);

        Int32 nextLevelExp = AvatarRule.UpgradeLevelNeedExp(_avatar.Level, (UInt16)(_avatar.Level + 1));

        _exp.text = String.Format("{0}/{1}", _avatar.EXP, nextLevelExp);
        percent = (float)_avatar.EXP / (float)nextLevelExp;
        _expBar.value = Mathf.Max(0, percent);

        _phyAttack.text = Convert.ToString(_avatar.PhysicsPower);
        _phyDefend.text = Convert.ToString(_avatar.PhysicsDefense);
        _mgcAttack.text = Convert.ToString(_avatar.MagicPower);
        _mgcDefend.text = Convert.ToString(_avatar.MagicDefense);
        _treatmentStrength.text = Convert.ToString(_avatar.TreatmentStrength);
        _speed.text = Convert.ToString(_avatar.AttackSpeed);

        _crit.text = Convert.ToString(_avatar.Crit);
        _tough.text = Convert.ToString(_avatar.Toughness);
        _hit.text = Convert.ToString(_avatar.Hit);
        _dodge.text = Convert.ToString(_avatar.Dodge);

        string modelName = _avatar.ModelID;
        SetEntity(modelName);
    }

    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        Transform child = _characterRoot.transform.Find(resName);
        if (child != null)
        {
            if (child.name.Equals(resName))
            {
                AnimationControl control = child.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
                return;
            } 
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                o.transform.parent = _characterRoot.transform;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private void UpdateAttribute(object o)
    {
        if (!Active) return;
        RefreshValue();
    }
}
