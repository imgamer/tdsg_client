﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterPunctuateItem : WinItem
{
    private int _totalSize;
    private float _step;
    private int _relLength;
    private int _totalValue;
    private int _originalValue;
    private int _fixedValue;
    private int _curValue;
    public int CurValue
    {
        get { return _curValue; }
        set
        {
            _curValue = value;
            _slider.value = 1;//修复不刷新问题
            _slider.value = (_curValue - _originalValue) / (float)(_relLength);
            _value.text = string.Format("{0}", _curValue);
            if (_curValue - _fixedValue > 0)
            {
                _change.text = string.Format("+{0}", _curValue - _fixedValue);
            }
            else
            {
                _change.text = "";
            }
        }
    }
    public int FixedValue
    {
        get { return _fixedValue; }
    }
    public int ChangeValue
    {
        get { return _curValue - _fixedValue; }
    }

    private UISprite _original;
    private UISprite _foreground;
    public void SetInit(int totalValue, int originalValue, int fixedValue)
    {
        _totalValue = totalValue;
        _originalValue = originalValue;
        _fixedValue = fixedValue;

        _relLength = _totalValue - _originalValue;
        if (_relLength <= 0) _relLength = 1;
        _step = 1.0f / _relLength;
        _range = _totalValue - _fixedValue;
        _min = _step * (_fixedValue - _originalValue);
        _max = 1;
        _original.width = (int)(_totalSize * _originalValue / (float)_totalValue);
        _foreground.width = _totalSize - _original.width;

        CurValue = fixedValue;
    }
    private UILabel _value;
    private UILabel _change;
    private UI.Slider _slider;
    private int _range;
    private float _min;
    private float _max;
    public void SetRange(int range)
    {
        _range = Mathf.Min(range, _totalValue - _fixedValue);
        float ret = _step * (range + _fixedValue - _originalValue);
        _max = Mathf.Min(ret, 1);
    }
    public GameObject BtnSub { get; private set; }
    public GameObject BtnAdd { get; private set; }

    public Action OnChange;

    private bool drag = false;
    protected override void OnInit()
    {
        Transform bar = transform.Find("Bar");
        _slider = bar.GetComponent<UI.Slider>();
        _slider.OnVerify = (cur, before) =>
        {
            float ret;
            if (cur >= before && cur >= _max)
            {
                if (drag)
                {
                    _curValue = _range + _fixedValue;
                }
                ret = _max;
            }
            else if (cur <= before && cur <= _min)
            {
                if (drag)
                {
                    _curValue = _fixedValue;
                }
                ret = _min;
            }
            else
            {
                if (drag)
                {
                    int value = (int)(cur * _relLength);
                    _curValue = value + _originalValue;
                    ret = (float)(value) / _relLength;
                }
                ret = cur;
            }
            _value.text = string.Format("{0}", _curValue);
            if (_curValue == _fixedValue)
            {
                _change.text = "";
            }
            else
            {
                _change.text = string.Format("+{0}", _curValue - _fixedValue);
            }
            if (!Mathf.Approximately(cur, before))
            {
                if (OnChange != null) OnChange();
            }
            return ret;
        };
        _original = bar.Find("Original").GetComponent<UISprite>();
        _foreground = bar.Find("Foreground").GetComponent<UISprite>();
        UISprite background = bar.Find("Background").GetComponent<UISprite>();
        _totalSize = background.width;
        _value = transform.Find("Value").GetComponent<UILabel>();
        _change = transform.Find("Change").GetComponent<UILabel>();
        _change.text = "";
        BtnSub = transform.Find("BtnSub").gameObject;
        BtnAdd = transform.Find("BtnAdd").gameObject;
        InputManager.instance.AddClickListener(BtnSub, (go) =>
        {
            if (_curValue <= _fixedValue) return;
            _curValue--;
            _slider.value = _step * (_curValue - _originalValue);
        });
        InputManager.instance.AddClickListener(BtnAdd, (go) =>
        {
            int max = Mathf.Min(_totalValue, _range + _fixedValue);
            if (_curValue >= max) return;
            _curValue++;
            _slider.value = _step * (_curValue - _originalValue);
        });
        InputManager.instance.AddPressListener(_slider.thumb.gameObject, (go, pressed) =>
        {
            drag = pressed;
        });
    }
}
