﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class UICharacterPlusPointHelp : UIWin
{
    private bool _inited = false;
    public const int attributeNum = 4;
    private const int _recommendNum = 3;
    private UILabel[] _assignmentPoint = new UILabel[attributeNum];
    private CharacterPointRecommendItem[] _recommendItems = new CharacterPointRecommendItem[_recommendNum];

    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform assignmentPoint = center.Find("Root/AssignmentPoint");
        Transform recmmendAssignment = center.Find("Root/RecommendList");
        for (int i = 0; i < attributeNum; i++)
        { 
            _assignmentPoint[i] = assignmentPoint.Find(i.ToString()).GetComponent<UILabel>();
            if (i < _recommendNum)
            {
                CharacterPointRecommendItem item = recmmendAssignment.Find(i.ToString()).GetComponent<CharacterPointRecommendItem>();
                item.Init(this);
                _recommendItems[i] = item;
            }
        }
    }

    protected override void OnOpen(params object[] args)
    {
        if (!_inited)
        {
            for (int i = 0; i < _recommendNum; i++)
            {
                if (i < CharacterPlusPointHelpConfig.instance.currentRoleRecommendNum)
                {
                    SetProgramTopAndDescription(i);
                }
                else
                {
                    _recommendItems[i].gameObject.SetActive(false);
                }
            }
            _inited = true;
        }

        for (int i = 0; i < CharacterPlusPointHelpConfig.instance.currentRoleRecommendNum; i++)
        { 
            SetRecommendPoint(i);
        } 
        SetAssignmentPoint();
    }

    protected override void OnRefresh()
    {
    }

    protected override void OnClose()
    {
    }

    protected override void OnUnInit()
    {
    }

    /// <summary>
    /// 设置已分配点数
    /// </summary>
    private void SetAssignmentPoint()
    { 
        _assignmentPoint[0].text = string.Format("已分配 {0}点", GameMain.Player.FreePointAssignment["force"]);
        _assignmentPoint[1].text = string.Format("已分配 {0}点", GameMain.Player.FreePointAssignment["vitality"]);
        _assignmentPoint[2].text = string.Format("已分配 {0}点", GameMain.Player.FreePointAssignment["spirituality"]);
        _assignmentPoint[3].text = string.Format("已分配 {0}点", GameMain.Player.FreePointAssignment["gengu"]);
    }

    /// <summary>
    /// 设置加点方案 Top and RecommendPoint 的 Lable.text
    /// </summary>
    private void SetProgramTopAndDescription(int recommendNum)
    {
        string topText = "";
        uint[] _arrData;
        GetCurrentRoleArray(recommendNum, out _arrData);
        if (_arrData[0] > 0) topText += _arrData[0].ToString() + "武力 ";
        if (_arrData[1] > 0) topText += _arrData[1].ToString() + "体质 ";
        if (_arrData[2] > 0) topText += _arrData[2].ToString() + "灵性 ";
        if (_arrData[3] > 0) topText += _arrData[3].ToString() + "根骨";
        _recommendItems[recommendNum].Top = topText;
        _recommendItems[recommendNum].Description =
            CharacterPlusPointHelpConfig.instance.GetCurrentRoleRecommendData(recommendNum).description;
    }

    /// <summary>
    /// 设置推荐分配 分配点数
    /// </summary>
    private void SetRecommendPoint(int recommendNum)
    {
        string recommendPointText ="";
        if (GameMain.Player.FreePoint >= attributeNum)
        {
            uint[] addPoint = new uint[attributeNum] { 0, 0, 0, 0 };
            GetFreePointToRecommendPoint(recommendNum, ref addPoint);
            if (addPoint[0] > 0) recommendPointText += "    武力：" + addPoint[0].ToString();
            if (addPoint[1] > 0) recommendPointText += "    体质：" + addPoint[1].ToString();
            if (addPoint[2] > 0) recommendPointText += "    灵性：" + addPoint[2].ToString();
            if (addPoint[3] > 0) recommendPointText += "    根骨：" + addPoint[3].ToString();
        }
        else
        {
            recommendPointText = "    剩余潜力不足";
        }
        _recommendItems[recommendNum].RecommendPoint = recommendPointText;
    }

    /// <summary>
    /// 推荐加点的剩余点分配
    /// </summary>
    public static void GetFreePointToRecommendPoint(int recommendNum, ref uint[] addPoint)
    {
        uint freePoint = (uint)GameMain.Player.FreePoint;
        uint[] recommedArr;
        GetCurrentRoleArray(recommendNum, out recommedArr);
        while (freePoint >= attributeNum)
        {
            for (int k = 0; k < attributeNum; k++)
            {
                if (recommedArr[k] > 0)
                {
                    addPoint[k] += recommedArr[k];
                    freePoint -= recommedArr[k];
                }
            }
        }
    }

    /// <summary>
    /// 获取当前角色推荐方案 uint 数组
    /// </summary>
    private static void GetCurrentRoleArray(int recommendNum, out uint[] recommendArray)
    {
        CharacterPlusPointData roleData =
            CharacterPlusPointHelpConfig.instance.GetCurrentRoleRecommendData(recommendNum);
        recommendArray = new uint[attributeNum] {
            roleData.force,
            roleData.vitality,
            roleData.spirituality,
            roleData.gengu
        };
    }

}
