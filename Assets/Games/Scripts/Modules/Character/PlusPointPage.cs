﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlusPointPage : WinPage
{
    KBEngine.Avatar _avatar;
    protected override void OnInit()
    {
        InitPage();
        InitProperty();
        InitPunctuate();

        _avatar = GameMain.Player;

        EventManager.AddListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdatePage);
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateAttribute();
        UpdateItems();
        GetRemain();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdatePage);
    }

    private void InitPage()
    {
        GameObject plusPointMenu = transform.Find("PlusPointMenu").gameObject;
        plusPointMenu.SetActive(false);
        GameObject plusPointBtn = transform.Find("PlusPointBtn").gameObject;
        InputManager.instance.AddClickListener(plusPointBtn, (go) =>
        {
            if (plusPointMenu.activeSelf)
            {
                plusPointMenu.SetActive(false);
            }
            else
            {
                plusPointMenu.SetActive(true);
            }
        });
    }


    private const int Num = 6;
    private UILabel[] _values = new UILabel[Num];
    private UILabel[] _adds = new UILabel[Num];

    private void InitProperty()
    {
        Transform grid = transform.Find("Property");
        for (int i = 0; i < Num; i++)
        {
            Transform item = grid.Find(i.ToString());
            _values[i] = item.Find("Value").GetComponent<UILabel>();
            _adds[i] = item.Find("Up/Add").GetComponent<UILabel>();
            _adds[i].transform.parent.gameObject.SetActive(false);
        }
    }

    private UILabel _remain;
    private const int Count = 5;
    private CharacterPunctuateItem[] _punctuateItems = new CharacterPunctuateItem[Count];
    private void InitPunctuate()
    {
        _remain = transform.Find("Remain/Value").GetComponent<UILabel>();
        GameObject btnRecommend = transform.Find("BtnRecommend").gameObject;
        InputManager.instance.AddClickListener(btnRecommend, (go) =>
        {
            RecommendPoint();
        });
        GameObject btnReset = transform.Find("BtnReset").gameObject;
        InputManager.instance.AddClickListener(btnReset, (go) =>
        {
            _avatar.ReqResetFreePoint();
        });
        GameObject btnSure = transform.Find("BtnSure").gameObject;
        InputManager.instance.AddClickListener(btnSure, (go) =>
        {
            _avatar.SyncFreePointAssignmentToServer(_punctuateItems[0].ChangeValue, _punctuateItems[1].ChangeValue,
                _punctuateItems[2].ChangeValue, _punctuateItems[3].ChangeValue);
        });
        GameObject btnHelp = transform.Find("BtnHelp").gameObject;
        InputManager.instance.AddClickListener(btnHelp, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UICharacterPlusPointHelp);
        });
        Transform grid = transform.Find("Punctuate");
        for (int i = 0; i < Count; i++)
        {
            CharacterPunctuateItem item = grid.Find(i.ToString()).GetComponent<CharacterPunctuateItem>();
            item.Init(ParentUI);
            _punctuateItems[i] = item;
        }
    }

    private void UpdateItems()
    {
        for (int i = 0; i < Count; i++)
        {
            int index = i;
            CharacterPunctuateItem item = _punctuateItems[i];
            int[] values = GetValues(i);
            item.SetInit(AvatarRule.GetMaxFreePoint(_avatar.Level) + values[0], values[0], values[1]);//free+base, base, total
            item.SetRange(_avatar.FreePoint);
            item.OnChange = () =>
            {
                int ret = GetRemain();
                OnUpdateItems(item, ret);
                OnUpdateAdds(index);
            };
        }
    }

    private int[] GetValues(int index)
    {
        int[] ret = new int[2];
        switch (index)
        {
            case 0:
                ret[0] = (int)_avatar.BaseForce;
                ret[1] = (int)_avatar.Force;
                break;
            case 1:
                ret[0] = (int)_avatar.BaseVitality;
                ret[1] = (int)_avatar.Vitality;
                break;               
            case 2:
                ret[0] = (int)_avatar.BaseSpirituality;
                ret[1] = (int)_avatar.Spirituality;
                break;
            case 3:
                ret[0] = (int)_avatar.BaseGengu;
                ret[1] = (int)_avatar.Gengu;
                break;
            //[@]身法已移除，后面来删掉
            case 4:
                ret[0] = 0;
                ret[1] = 0;
                break;
            default:
                break;
        }
        return ret;
    }
    private void OnUpdateItems(CharacterPunctuateItem curItem, int ret)
    {
        foreach (var item in _punctuateItems)
        {
            if (curItem == item) continue;
            item.SetRange(item.ChangeValue + ret);
        }
    }
    private void OnUpdateAdds(int index)
    {
        switch (index)
        {
            case 0:
                OnUpdateAdd(1);
                OnUpdateAdd(4);
                OnUpdateAdd(5);
                break;
            case 1:
                OnUpdateAdd(0);
                OnUpdateAdd(4);
                break;
            case 2:
                OnUpdateAdd(2);
                OnUpdateAdd(4);
                break;
            case 3:
                OnUpdateAdd(3);
                OnUpdateAdd(4);
                OnUpdateAdd(5);
                break;
            case 4:
                OnUpdateAdd(5);
                break;
            default:
                break;
        }
    }
    private void OnUpdateAdd(int index)
    {
        UILabel label = _adds[index];
        int oldValue = 0;
        int newValue = 0;
        switch (index)
        {
            case 0:
                oldValue = _avatar.HP_Max;
                newValue = AvatarRule.CalcHPMax(_avatar.School, Convert.ToUInt32 (_punctuateItems[1].CurValue));
                break;
            case 1:
                oldValue = _avatar.PhysicsPower;
                newValue = AvatarRule.CalcPhysicsPower(_avatar.School, Convert.ToUInt32(_punctuateItems[0].CurValue));
                break;
            case 2:
                oldValue = _avatar.MagicPower;
                newValue = AvatarRule.CalcMagicPower(_avatar.School, Convert.ToUInt32(_punctuateItems[2].CurValue));
                break;
            case 3:
                oldValue = _avatar.PhysicsDefense;
                newValue = AvatarRule.CalcPhysicsDefense(_avatar.School, Convert.ToUInt32(_punctuateItems[0].CurValue), Convert.ToUInt32(_punctuateItems[3].CurValue));
                break;
            case 4:
                oldValue = _avatar.MagicDefense;
                newValue = AvatarRule.CalcMagicDefense(_avatar.School, Convert.ToUInt32(_punctuateItems[1].CurValue), Convert.ToUInt32(_punctuateItems[2].CurValue), 
                                      Convert.ToUInt32(_punctuateItems[3].CurValue));
                break;
            case 5:
                oldValue = (int)_avatar.AttackSpeed;
                newValue = AvatarRule.CalcAttackSpeed(_avatar.School);
                break;
            default:
                break;
        }
        if (newValue > oldValue)
        {
            label.transform.parent.gameObject.SetActive(true);
            label.text = string.Format("+{0}", newValue - oldValue);
        }
        else
        {
            label.transform.parent.gameObject.SetActive(false);
        }
    }

    private int GetRemain()
    {
        int sum = 0;
        foreach (var item in _punctuateItems)
        {
            sum += item.ChangeValue;
        }
        int ret = _avatar.FreePoint - sum;
        _remain.text = string.Format("{0}", ret);
        return ret;
    }

    private void UpdateAttribute()
    {

        _values[0].text = string.Format("{0}", _avatar.HP_Max);
        _values[1].text = string.Format("{0}", _avatar.PhysicsPower);
        _values[2].text = string.Format("{0}", _avatar.MagicPower);
        _values[3].text = string.Format("{0}", _avatar.PhysicsDefense);
        _values[4].text = string.Format("{0}", _avatar.MagicDefense);
        _values[5].text = string.Format("{0}", _avatar.AttackSpeed);
        for (int i = 0; i < Count; i++)
        {
            OnUpdateAdd(i);
        }
    }

    private void UpdatePage(object o)
    {
        if (!Active) return;
        UpdateAttribute();
        UpdateItems();
        GetRemain();
    }

    private const int attributeNum = 4;
    private void RecommendPoint()
    {
        if (_avatar.FreePoint < attributeNum) return;
        
        uint[] addPoint = new uint[UICharacterPlusPointHelp.attributeNum] { 0, 0, 0, 0 };
        UICharacterPlusPointHelp.GetFreePointToRecommendPoint(0, ref addPoint);
        for (int i = 0; i < attributeNum; i++)
        {
            if (0 != _punctuateItems[i].ChangeValue)
            {
                _punctuateItems[i].CurValue -= _punctuateItems[i].ChangeValue;
            }
            _punctuateItems[i].CurValue += (int)addPoint[i];
        }
    }
}
