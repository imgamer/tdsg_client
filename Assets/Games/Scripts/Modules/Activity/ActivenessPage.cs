﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ActivenessPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private bool _inited = false;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_ACTIVENESS_REWARD_STATE_UPDATE, RefreshRewardItem);
        EventManager.AddListener<object>(EventID.EVNT_ACTIVENESS_UPDATE, RefreshActiveness);
    }

    protected override void OnOpen(params object[] args)
    {
        if(!_inited)
        {
            InitRewardItem();           
            _inited = true;
        }
       _avatar.activityModule.RequestLivenessValue();
       _avatar.activityModule.RequestRewardItemState();
    }

    protected override void OnRefresh()
    {
        
    }

    protected override void OnClose()
    {
        for(int i = 0; i < Num; i++)
        {
            _activenessRewardItems[i].CloseTimer();
        }
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ACTIVENESS_REWARD_STATE_UPDATE, RefreshRewardItem);
        EventManager.RemoveListener<object>(EventID.EVNT_ACTIVENESS_UPDATE, RefreshActiveness);
    }

    private UIProgressBar _proBar;

    private const int Num = 5;
    private ActivenessRewardItem[] _activenessRewardItems = new ActivenessRewardItem[Num];

    private void InitPage()
    {
        _proBar = transform.Find("ProBar").GetComponent<UIProgressBar>();
        Transform rewards = transform.Find("Rewards");
        for(int i = 0; i < Num; i++)
        {
            ActivenessRewardItem item = rewards.Find(i.ToString()).GetComponent<ActivenessRewardItem>();
            item.Init(ParentUI);
            _activenessRewardItems[i] = item;
            item.Index = Convert.ToByte(i);
            InputManager.instance.AddClickListener(item.gameObject, (go) =>
            {
                if(item.State == 0)
                {
                    _avatar.activityModule.RequestRewardItem(item.Index);
                }               
            });
        }
    }

    private void InitRewardItem()
    {
        List<ActivenessRewardData> data = _avatar.activityModule.RewardData;
        for(int i = 0; i < Num; i++)
        {
            _activenessRewardItems[i].ActivenessValue = string.Format("{0}活跃", data[i].livenessValue);
        }
    }

    private const int ACTIVENESS_MAX = 130;

    private void RefreshActiveness(object o)
    {
        if (!Active) return;
        UInt32 Activeness = _avatar.activityModule.Activeness;
        if (Activeness == 0)
        {
            _proBar.value = Mathf.Max(0, 0);
        }
        else
        {
            float percent = (float)Activeness / (float)ACTIVENESS_MAX;
            _proBar.value = Mathf.Max(0, percent);
        }
    }

    private void RefreshRewardItem(object o)
    {
        if (!Active) return;
        UInt32 Activeness = _avatar.activityModule.Activeness;
        List<ActivenessRewardData> data = _avatar.activityModule.RewardData;
        Dictionary<byte, Int32> rewardItemState = _avatar.activityModule.RewardItemState;
        for(int i = 0; i < Num; i++)
        {
            if(Activeness< data[i].livenessValue)
            {
                _activenessRewardItems[i].SetSate(3);
            }
            else
            {
                byte index = Convert.ToByte(i);
                if(!rewardItemState.ContainsKey(index))
                {
                    _activenessRewardItems[i].SetSate(1);
                }
                else
                {
                    //超过服务器时间设为可领取状态
                    int timeSpan = rewardItemState[index] - Convert.ToInt32(TimeUtils.Now());
                    if(timeSpan<= 0)
                    {
                        _activenessRewardItems[i].SetSate(0);
                    }
                    else
                    {
                        _activenessRewardItems[i].AddCountdownRealTimer(timeSpan);
                    }
                }
            }
        }
    }
}
