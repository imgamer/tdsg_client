﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using LitJson;

public  class ActivityData
{
    public string activitykey;
    public string name;
    public string description;
    public string startTime;
    public string icon;
    public SByte type;
    public Int32 validCount;
    public SByte playerCount;
    public Int32 levelLimit;
    public Int32 priority;
    public List<List<Int32>> rewardItem = new List<List<Int32>>();
    public Int32 rewardGold;
    public Int32 rewardEXP;
    public SByte activityState;
    public Int32 currentCount;

    public ActivityData(Dictionary<string, object> activityData)
    {
        activitykey = (string)activityData["activitykey"];
        name = (string)activityData["name"];
        description = (string)activityData["description"]; 
        startTime = (string)activityData["startTime"];
        icon = (string)activityData["icon"];
        type = (SByte)activityData["type"];
        validCount = (Int32)activityData["validCount"];
        playerCount = (SByte)activityData["playerCount"];
        levelLimit = (Int32)activityData["levelLimit"];
        priority = (Int32)activityData["priority"];
        List<object>  rewards = (List<object>)activityData["rewardItem"];
        foreach(object obj in rewards)
        {
            List<object> reward = (List<object>)obj;
            List<Int32> tempReward = new List<int>();
            foreach (object a in reward)
            {
                tempReward.Add(Convert.ToInt32(a));
            }
            rewardItem.Add(tempReward);
        }
       
        rewardGold = (Int32)activityData["rewardGold"];
        rewardEXP = (Int32)activityData["rewardEXP"];
        activityState = (SByte)activityData["activityState"];
        currentCount = (Int32)activityData["currentCount"];
    }
}

public class ActivityModule 
{
    private KBEngine.Avatar _avatar;
    private Dictionary<string, ActivityData> _activityData = new Dictionary<string, ActivityData>();

    

    public ActivityModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;

        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_liveness_reward");
        _activenessRewardData = JsonMapper.ToObject<Dictionary<string, ActivenessRewardData>>(textdata.text);
    }

    /// <summary>
    /// 请求活动数据
    /// </summary>
    public void RequestActivityInfos()
    {
        _activityData.Clear();
        _avatar.cellCall("requestActivityInfos", new object[] { });
    }

    public void InitActivityInfo(Dictionary<string, object> activityInfo)
    {
        ActivityData data = new ActivityData(activityInfo);
        if(_activityData.ContainsKey(data.activitykey))
        {
            return;
        }
        _activityData.Add(data.activitykey, data);

        EventManager.Invoke<object>(EventID.EVNT_ACTIVITY_EVERYDAY_UPDATE, null);
    }

    public List<ActivityData> GetActivityData()
    {
        List<ActivityData> sortData = new List<ActivityData>();
        List<ActivityData> tempData1 = new List<ActivityData>();
        List<ActivityData> tempData2 = new List<ActivityData>();
        List<ActivityData> tempData3 = new List<ActivityData>();
        //按状态及优先级排序
        foreach(ActivityData data in _activityData.Values)
        {
            if(data.activityState == 2)
            {
                tempData1.Add(data);
                SortListData(tempData1);
            }
            else if (data.activityState == 1)
            {
                tempData2.Add(data);
                SortListData(tempData2);
            }
            else if(data.activityState == 3)
            {
                tempData3.Add(data);
                SortListData(tempData3);
            }
        }

        sortData.AddRange(tempData1);
        sortData.AddRange(tempData2);
        sortData.AddRange(tempData3);

        return sortData;
    }

    private List<ActivityData> SortListData(List<ActivityData> data)
    {
        data.Sort(delegate(ActivityData x, ActivityData y)
        {
            return y.priority.CompareTo(x.priority);
        });

        return data;
    }

    public void JoinActivity(ActivityData data)
    {
        if (data.name == "领主与马奴")
        {
            UIManager.instance.OpenWindow(null, WinID.UISlave, 1);
        }
        else if (data.name == "竞技场")
        {
            if (GameMain.Player.TeamStatus == Define.TEAM_STATUS_NONE)
            {
                UIManager.instance.OpenWindow(null, WinID.UISlave, 0);
            }
            else
            {
                TipManager.instance.ShowTextTip(2508);
            }
        }
        else if (data.name == "爬塔")
        {
            GameMain.Player.cellCall("requestToAcceptPataQuest", new object[] { });
        }
        else if (data.name == "封神")
        {
            GameMain.Player.cellCall("requestTeleportToSealGodSpace", new object[] { });
        }
    }

    public UInt32 Activeness { private set; get; }

    public Dictionary<byte, Int32> _rewardItemState = new Dictionary<byte, Int32>();

    private Dictionary<string, ActivenessRewardData> _activenessRewardData = new Dictionary<string, ActivenessRewardData>();

    public List<ActivenessRewardData> RewardData
    {
        get
        {
            List<ActivenessRewardData> data = new List<ActivenessRewardData>(_activenessRewardData.Values);
            data.Sort(delegate(ActivenessRewardData a, ActivenessRewardData b)
            {
                return a.livenessValue.CompareTo(b.livenessValue);
            });
            return data;
        }
    }

    public Dictionary<byte, Int32> RewardItemState
    {
        get
        {
            return _rewardItemState;
        }
    }

    /// <summary>
    /// 更新当前活跃度
    /// </summary>
    /// <param name="value"></param>
    public void UpdateLivenessValue(UInt32 value)
    {
        Activeness = value;
        EventManager.Invoke<object>(EventID.EVNT_ACTIVENESS_UPDATE, null);
    }

   /// <summary>
   /// 更新奖励状态
   /// </summary>
   /// <param name="rewardItemSate"></param>
    public void UpdateRewardItemState(List<object> rewardItems)
    {
        _rewardItemState.Clear();
        foreach(object obj in rewardItems)
        {
            Dictionary<string, object> state = (Dictionary<string, object>)obj;
            byte itemIndex = (byte)state["itemIndex"];
            Int32 itemTime = (Int32)state["itemTime"];
            _rewardItemState.Add(itemIndex, itemTime);
        }
        EventManager.Invoke<object>(EventID.EVNT_ACTIVENESS_REWARD_STATE_UPDATE, null);
    }

    /// <summary>
    /// 领取宝箱奖励
    /// </summary>
    /// <param name="index"></param>
    public void RequestRewardItem(byte index)
    {       
        _avatar.cellCall("requestRewardItem", index);
    }

    /// <summary>
    /// 请求当前活跃度值
    /// </summary>
    public void RequestLivenessValue()
    {
        _avatar.cellCall("requestLivenessValue", new object[] { });
    }

   /// <summary>
   /// 查询宝箱状态
   /// </summary>
    public void RequestRewardItemState()
    {      
        _avatar.cellCall("requestRewardItemState", new object[] { });
    }
}

public class ActivenessRewardData
{
    public int livenessValue;
    public List<int> rewardItemID= new List<int>();
    public int rewardItemTime;
}