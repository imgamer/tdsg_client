﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivityItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _count;
    public string Count
    {
        set
        {
            _count.text = value;
        }
    }

    private UILabel _activeness;
    public string Activeness
    {
        set
        {
            _activeness.text = value;
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value.ToString();
        }
    }

    private GameObject _joinBtn;
    public GameObject JoinBtn
    {
        get
        {
            return _joinBtn;
        }
    }

    private GameObject _complete;

    private const int Num = 3;
    private GameObject[] _stateItem = new GameObject[Num];

    // 0：时间开启 1: 可以参加  2：完成
    public void SetItemState(int state)
    {
        for (int i = 0; i < Num; i++)
        {
            _stateItem[i].SetActive(i == state);
        }
    }
    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _count = transform.Find("Count/Value").GetComponent<UILabel>();
        _activeness = transform.Find("Activeness/Value").GetComponent<UILabel>();
        _joinBtn = transform.Find("1/JoinBtn").gameObject;
        for (int i = 0; i < Num; i++)
        {
            GameObject item = transform.Find(i.ToString()).gameObject;
            _stateItem[i] = item;
        }      
    }
}
