﻿using UnityEngine;
using System.Collections;
using System;

public class ActivenessRewardItem : WinItem 
{
    private byte _index;
    public byte Index
    {
        get { return _index; }
        set { _index = value; }
    }
    public int State { private set; get; }

    private UILabel _value;
    public string ActivenessValue
    {
        set
        {
            _value.text = value;
        }
    }

    private UILabel _time;
    
    private GameObject[]  stateItem = new GameObject[Num];
    private const int Num = 4;

    //0: 可以打开 1：已打开 2：倒计时 3：未开启
    public void SetSate(int state)
    {
        for(int i = 0; i< Num; i++)
        {
            stateItem[i].SetActive(i == state);
        }
        State = state;
    }

    private ulong _timerId;

    public void AddCountdownRealTimer(int timeSpan)
    {
        SetSate(2);
        if (_timerId != TimeUtils.InvalidTimerID)
        {
            TimeUtils.RestartTimer(_timerId);
        }
        else
        {
            _timerId = TimeUtils.AddCountdownRealTimerCallStr(timeSpan, (str) =>
            {
                _time.text = string.Format("{0}", str);
            }, () =>
            {
                SetSate(0);
                _timerId = TimeUtils.InvalidTimerID; 
            }, TimeUtils.TimeFormat3v);
            TimeUtils.StartTimer(_timerId);
        }
    }

    public void CloseTimer()
    {
        if(_timerId != TimeUtils.InvalidTimerID)
        {
            TimeUtils.RemoveTimer(_timerId);
            _timerId = TimeUtils.InvalidTimerID;
        }
    }

    protected override void OnInit()
    {
        for(int i =0; i < Num; i++)
        {
            GameObject item = transform.Find(i.ToString()).gameObject;
            stateItem[i] = item;
        }

        _value = transform.Find("Value").GetComponent<UILabel>();
        _time = transform.Find("2/Value").GetComponent<UILabel>();
    }
}
