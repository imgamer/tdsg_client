﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivityEverydayPage : WinPage
{
    private KBEngine.Avatar _avatar;
    private List<ActivityItem> _activityItems = new List<ActivityItem>();
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_ACTIVITY_EVERYDAY_UPDATE, UpdateActivityItem);
    }

    protected override void OnOpen(params object[] args)
    {
        _avatar.activityModule.RequestActivityInfos();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ACTIVITY_EVERYDAY_UPDATE, UpdateActivityItem);
    }

    private GameObject _tempItem;
    private UI.Grid _uiGrid;
    private GameObject _selectBg;
    private void InitPage()
    {
        _uiGrid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = transform.Find("Scroll/Grid/Item").gameObject;
        _selectBg = transform.Find("SelectBg").gameObject;
        _tempItem.SetActive(false);
        _selectBg.SetActive(false);
    }

    private void RefreshItem()
    {
        List<ActivityData> activityData = _avatar.activityModule.GetActivityData();
        int dataCount = activityData.Count;
        int itemCount = _activityItems.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_activityItems[i], activityData[i]);
                    _activityItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    _tempItem.SetActive(false);
                    ActivityItem activityItem = item.GetComponent<ActivityItem>();
                    activityItem.Init(ParentUI);                   
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _activityItems.Add(activityItem);
                    SetItemData(activityItem, activityData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_activityItems[i], activityData[i]);
                    _activityItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _activityItems[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(ActivityItem item, ActivityData data)
    {
        item.Name = data.name;
        item.SetItemState(data.activityState);
        InputManager.instance.AddClickListener(item.JoinBtn, (go) =>
        {
            _avatar.activityModule.JoinActivity(data);
         });
    }

    private void SetItemSelect(ActivityItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    public void UpdateActivityItem(object o)
    {
        if (!Active) return;
        RefreshItem();
    }
}
