﻿using UnityEngine;
using System.Collections;

public class UIActivity : UIWin
{
    protected override void OnInit()
    {
        InitPage();
        InitTab();        
    }

    protected override void OnOpen(params object[] args)
    {
        _activenessPage.Open(this);
        uiTab.SwitchTo(0);
    }
    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        _activenessPage.Close();
    }

    protected override void OnUnInit()
    {

    }

    private WinPage _activenessPage;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _activenessPage = center.Find("Activeness").GetComponent<WinPage>();
    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(this);
        };
    }

}
