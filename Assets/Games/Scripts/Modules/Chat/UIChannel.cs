﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 聊天频道界面
/// </summary>
public class UIChannel : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<string>(EventID.EVNT_CHAT_DATA_ADD, AddInput);
        EventManager.AddListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChannel);

        Transform left = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Left);
        Transform root = left.Find("Root");
        InitItem(root);
        InitInput(root);
        InitTip(root);
        InitSort(root);
    }

    protected override void OnOpen(params object[] args)
    {
        SwitchTo(curChannel);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<string>(EventID.EVNT_CHAT_DATA_ADD, AddInput);
        EventManager.RemoveListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChannel);
    }

    #region 初始化
    private string _playerName;
    private UIProgressBar _btnLock;
    private GameObject _messageRoot;
    private UILabel _messageText;
    private void InitItem(Transform root)
    {
        _playerName = GameMain.Player.Name;
        GameObject btnClose = root.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) => 
        {
            UIManager.instance.CloseWindow(winID);
        });
        _btnLock = root.Find("BtnLock").GetComponent<UIProgressBar>();
        _btnLock.value = 0;
        InputManager.instance.AddClickListener(_btnLock.gameObject, (go) =>
        {
            SetLockState(!_locked);
        });
        _messageRoot = root.Find("MessageTip").gameObject;
        _messageText = root.Find("MessageTip/Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_messageRoot, (go) =>
        {
            SetLockState(false);
        });

        Transform select = root.Find("BtnRoot/Select");
        Transform grid = root.Find("BtnRoot/Grid");
        for (int i = 0; i < 7; i++)
        {
            int channel = (int)(i + 1);
            GameObject item = grid.Find(i.ToString()).gameObject;
            InputManager.instance.AddClickListener(item, (go) => 
            {
                if (curChannel == channel) return;
                SwitchTo(channel);

                select.parent = item.transform;
                select.localPosition = Vector3.zero;
                select.localEulerAngles = Vector3.zero;
                select.localScale = Vector3.one;
            });
        }
    }
    private bool _locked = false;
    private void SetLockState(bool locked)
    {
        _locked = locked;
        if (locked)
        {
            _btnLock.value = 1;
        }
        else
        {
            _btnLock.value = 0;
            RefreshChannel();
        }
    }
    private GameObject inputArea;
    private UIInput _uiInput;
    private const int MaxNum = 50;
    private void InitInput(Transform root)
    {
        Transform inputRoot = root.Find("InputRoot");
        inputArea = inputRoot.gameObject;
        _uiInput = inputRoot.Find("Input").GetComponent<UIInput>();
        UILabel num = inputRoot.Find("Input/Num").GetComponent<UILabel>();
        num.text = string.Format("{0}", MaxNum);
        EventDelegate numOnChange = new EventDelegate(() =>
        {
            num.text = string.Format("{0}", MaxNum - _uiInput.value.Length);
        });
        _uiInput.onChange.Add(numOnChange);
        GameObject btnVoice = inputRoot.Find("BtnVoice").gameObject;
        InputManager.instance.AddClickListener(btnVoice, (go) =>
        {

        });
        GameObject btnFace = inputRoot.Find("BtnFace").gameObject;
        InputManager.instance.AddClickListener(btnFace, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIChatExpand);
        });
        GameObject btnSend = inputRoot.Find("BtnSend").gameObject;
        InputManager.instance.AddClickListener(btnSend, (go) =>
        {
            GameMain.Player.chatModule.SendChatData(curChannel, _uiInput.value);
            _uiInput.value = string.Empty;
        });
    }
    private GameObject tipArea;
    private UILabel tip;
    private void InitTip(Transform root)
    {
        tipArea = root.Find("TipsRoot").gameObject;
        tip = tipArea.transform.Find("Label").GetComponent<UILabel>();
    }
    private UI.Table uiTable;
    private ChannelItem templateLeft;
    private ChannelItem templateRight;
    private void InitSort(Transform root)
    {
        UIScrollView scrollView = root.Find("TextRoot/View").GetComponent<UIScrollView>();
        scrollView.onStoppedMoving = () => 
        {
            Vector4 clip = scrollView.panel.finalClipRegion;
            Bounds b = scrollView.bounds;
            float hy = (clip.w == 0f) ? Screen.height : clip.w * 0.5f;

            if (b.max.y <= clip.y + hy)
            {
                if (!_locked) return;
                SetLockState(false);
            }
        };
        GameObject box = root.Find("TextRoot/Box").gameObject;
        InputManager.instance.AddDragingListener(box, (go, delta) =>
        {
            if (delta.y < 0) return;
            if (_locked) return;
            if (!scrollView.shouldMoveVertically) return;
            SetLockState(true);
        });


        Transform sortRoot = root.Find("TextRoot/View/Sort");
        uiTable = sortRoot.GetComponent<UI.Table>();
        templateLeft = sortRoot.Find("Left").GetComponent<ChannelItem>();
        templateLeft.Init(this);
        templateLeft.gameObject.SetActive(false);

        templateRight = sortRoot.Find("Right").GetComponent<ChannelItem>();
        templateRight.Init(this);
        templateRight.gameObject.SetActive(false);
    }
    #endregion

    private int curChannel = Define.CHAT_CHANNEL_SYSTEM;
    private void SwitchTo(int channel)
    {
        curChannel = channel;
        if (curChannel == Define.CHAT_CHANNEL_SYSTEM)
        {
            inputArea.SetActive(false);
            tipArea.SetActive(true);
            tip.text = "此频道不能发言，请切换至其它频道发言";
        }
        else if (curChannel == Define.CHAT_CHANNEL_PARTY)
        {
            inputArea.SetActive(false);
            tipArea.SetActive(true);
            tip.text = "请在队伍界面中选择一键喊话";
        }
        else
        {
            inputArea.SetActive(true);
            tipArea.SetActive(false);
        }
        SetLockState(false);
    }

    private List<ChannelItem> leftItems = new List<ChannelItem>();
    private List<ChannelItem> rightItems = new List<ChannelItem>();
    private void RefreshChannel()
    {
        if (!CanRefresh()) return;

        HideChat();
        List<ChatModule.ChatData> chatDataList = GameMain.Player.chatModule.GetChatDataList(curChannel);
        int left = 0;
        int right = 0;
        for (int i = 0; i < chatDataList.Count; i++)
        {
            ChatModule.ChatData data = chatDataList[i];
            if (data.m_name.Equals(_playerName))
            {
                ShowChat(right, i, data, templateRight, ref rightItems);
                right++;
            }
            else
            {

                ChannelItem item = ShowChat(left, i, data, templateLeft, ref leftItems);
                InputManager.instance.AddClickListener(item.IconObject, (go) =>
                {
                    UIManager.instance.OpenWindow(null, WinID.UIChatMenu, data);
                });
                left++;
            }
        }
        uiTable.Reposition();
        uiTable.GoToStart();
    }

    private int noReadNum = 0;
    private bool CanRefresh()
    {
        if (_locked)
        {
            noReadNum++;
            _messageRoot.SetActive(true);
            _messageText.text = string.Format("未读消息{0}条", noReadNum);
            return false;
        }
        else
        {
            noReadNum = 0;
            _messageRoot.SetActive(false);
            return true;
        }
    }
    private void HideChat()
    {
        foreach (var item in leftItems)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in rightItems)
        {
            item.gameObject.SetActive(false);
        }
    }
    private ChannelItem ShowChat(int index, int i, ChatModule.ChatData data, ChannelItem template, ref List<ChannelItem> items)
    {
        ChannelItem item;
        if (index < items.Count)
        {
            item = items[index];
            item.name = i.ToString("D2");
            item.gameObject.SetActive(true);

            item.Name = data.m_name;
            item.Channel = GetChannelIcon(curChannel);
            item.InframeText = data.m_content;
        }
        else
        {
            item = Instantiate(template) as ChannelItem;
            item.gameObject.SetActive(true);
            item.name = i.ToString("D2");
            item.transform.parent = uiTable.transform;
            item.transform.localPosition = Vector3.zero;
            item.transform.localEulerAngles = Vector3.zero;
            item.transform.localScale = Vector3.one;
            item.Init(this);
            items.Add(item);

            item.Name = data.m_name;
            item.Channel = GetChannelIcon(curChannel);
            item.InframeText = data.m_content;
        }
        return item;
    }
    private string GetChannelIcon(int channel)
    {
        string channelIcon = string.Empty;
        switch (channel)
        {
            case Define.CHAT_CHANNEL_SYSTEM:
                channelIcon = "UI-s2-common-xitong";
                break;
            case Define.CHAT_CHANNEL_WORLD:
                channelIcon = "UI-s2-common-shijie";
                break;
            case Define.CHAT_CHANNEL_CURRENT:
                channelIcon = "UI-s2-common-dangqian";
                break;
            case Define.CHAT_CHANNEL_SCHOOL:
                channelIcon = "UI-s2-common-menpai";
                break;
            case Define.CHAT_CHANNEL_FACTION:
                channelIcon = "UI-s2-common-bangpaio";
                break;
            case Define.CHAT_CHANNEL_TEAM:
                channelIcon = "UI-s2-common-duiwuo";
                break;
            case Define.CHAT_CHANNEL_PARTY:
                channelIcon = "UI-s2-common-duzuio";
                break;
            default:
                break;
        }
        return channelIcon;
    }

    private void AddInput(string str)
    {
        if (!Active) return;
        _uiInput.value += str;
    }

    private void UpdateChannel(int channel)
    {
        if (curChannel != channel) return;
        if (!Active) return;
        RefreshChannel();
    }

}
