﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 聊天界面
/// </summary>
public class UIChat : UIWin 
{
    protected override void OnInit()
    {
        EventManager.AddListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChat);
        InitItem();
        InitSort();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        UpdateChat();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChat);
    }

    private Animation _menuAnimtion;
    private GameObject _btnShow;
    private void InitItem()
    {
        Transform bottomLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomLeft);
        GameObject btnSet = bottomLeft.Find("Set").gameObject;
        InputManager.instance.AddClickListener(btnSet, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIChatSettings);
        });

        Transform top = bottomLeft.Find("Top");
        _menuAnimtion = top.GetComponent<Animation>();
        _btnShow = top.Find("BtnShow").gameObject;
        InputManager.instance.AddClickListener(_btnShow, (go) =>
        {
            PlayMenuAnim();
        });

        Transform btnRoot = top.Find("BtnRoot");
        GameObject btnChannel = btnRoot.Find(Function.Channel.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnChannel);
        InputManager.instance.AddClickListener(btnChannel, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIChannel);
        });
        GameObject btnWorldVoice = btnRoot.Find(Function.WorldVoice.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnWorldVoice);
        InputManager.instance.AddClickListener(btnWorldVoice, (go) =>
        {

        });
        GameObject btnFactionVoice = btnRoot.Find(Function.FactionVoice.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnFactionVoice);
        InputManager.instance.AddClickListener(btnFactionVoice, (go) =>
        {

        });
        GameObject btnTeamVoice = btnRoot.Find(Function.TeamVoice.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnTeamVoice);
        InputManager.instance.AddClickListener(btnTeamVoice, (go) =>
        {

        });
    }
    
    private const int MaxNum = 10;
    private UI.Table uiTable;
    private List<ChatItem> chatItems = new List<ChatItem>();
    private void InitSort()
    {
        Transform bottomLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomLeft);
        Transform sortRoot = bottomLeft.Find("Top/ChatList/View/Sort");
        uiTable = sortRoot.GetComponent<UI.Table>();
        UITools.CopyFixNumChild(sortRoot, MaxNum);
        for (int i = 0; i < MaxNum; i++)
        {
            Transform item = sortRoot.GetChild(i);
            item.name = i.ToString("D2");
            ChatItem temp = item.GetComponent<ChatItem>();
            temp.Init(this);
            chatItems.Add(temp);
        }
    }

    private void UpdateChat()
    {
        List<ChatModule.ChatData> chatDataList = GameMain.Player.chatModule.GetChatDataList();
        int itemCount = chatItems.Count;
        int dataCount = chatDataList.Count;
        for (int i = 0; i < itemCount; i++)
        {
            ChatItem item = chatItems[i];
            if(i < dataCount)
            {
                ChatModule.ChatData data = chatDataList[i];
                item.gameObject.SetActive(true);
                item.Channel = GetChannelIcon(data.m_channel);
                item.InframeText = string.Format("[{0}]:{1}", data.m_name, data.m_content);
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
        uiTable.Reposition();
    }
    private string GetChannelIcon(int channel)
    {
        string channelIcon = string.Empty;
        switch (channel)
        {
            case Define.CHAT_CHANNEL_SYSTEM:
                channelIcon = "UI-s2-common-xitong";
                break;
            case Define.CHAT_CHANNEL_WORLD:
                channelIcon = "UI-s2-common-shijie";
                break;
            case Define.CHAT_CHANNEL_CURRENT:
                channelIcon = "UI-s2-common-dangqian";
                break;
            case Define.CHAT_CHANNEL_SCHOOL:
                channelIcon = "UI-s2-common-menpai";
                break;
            case Define.CHAT_CHANNEL_FACTION:
                channelIcon = "UI-s2-common-bangpaio";
                break;
            case Define.CHAT_CHANNEL_TEAM:
                channelIcon = "UI-s2-common-duiwuo";
                break;
            case Define.CHAT_CHANNEL_PARTY:
                channelIcon = "UI-s2-common-duzuio";
                break;
            default:
                break;
        }
        return channelIcon;
    }

    private void UpdateChat(int channel)
    {
        if (!Active) return;
        if (!GameMain.Player.chatModule.GetChannelState(channel)) return;
        UpdateChat();
    }


    #region 菜单动画
    private void ResetMenuAnim()
    {
        _forward = true;
    }
    private bool _forward = true;
    private void PlayMenuAnim()
    {
        uiTable.GoToStart();
        _btnShow.transform.localEulerAngles = _forward ? new Vector3(0, 0, 180) : Vector3.zero;
        AnimationState animtionState = _menuAnimtion[_menuAnimtion.clip.name];
        UITools.PlayUIAnim(_menuAnimtion, animtionState, _forward);
        _forward = !_forward;
    }
    #endregion
}
