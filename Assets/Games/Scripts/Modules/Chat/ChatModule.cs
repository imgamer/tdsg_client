﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 聊天系统管理类
/// </summary>
public class ChatModule
{
    private KBEngine.Avatar _avatar;
    private SensitivewordFilter _sensitivewordFilter;
    public ChatModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
        _sensitivewordFilter = SensitivewordFilterFactory.GetSensitiveFilter(Define.SENSITIVE_FILTER_TYPE_CHAT);
    }
    public struct ChatData
    {
        public int m_channel;     //频道
        public uint m_profession; //职业
        public string m_name;     //名字
        public string m_content;  //内容
    }
    private List<ChatData> _chatList = new List<ChatData>();
    private Dictionary<int, List<ChatData>> _chatDataDic = new Dictionary<int, List<ChatData>>();
    //需要改成配置表形式
    private readonly List<string> faceList = new List<string>{"100", "101", "102", "103", "104", "105", "106", "107", "108", "109", 
                                                    "110", "111", "112", "113", "114", "115", "116", "117", "118", "119"};
    /// <summary>
    /// 判断是否为表情符
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public bool ExpressionVerify(string eName)
    {
        return faceList.Contains(eName);
    }
    public List<ChatData> GetChatDataList()
    {
        return _chatList;
    }
    private List<ChatData> emptyList = new List<ChatData>(0);
    public List<ChatData> GetChatDataList(int channel)
    {
        List<ChatData> list;
        if (_chatDataDic.TryGetValue(channel, out list))
        {
            return list;
        }
        else
        {
            return emptyList;
        }
    }
    private const int MaxNum = 10;
    private const int MaxCount = 10;
    private void AddChatData(int channel, ChatData data)
    {
        List<ChatData> list;
        if (!_chatDataDic.TryGetValue(channel, out list))
        {
            list = new List<ChatData>();
        }
        list.Insert(0, data);
        if (list.Count > MaxNum)
        {
            list.RemoveAt(list.Count - 1);
        }
        _chatDataDic[channel] = list;

        _chatList.Insert(0, data);
        if (_chatList.Count > MaxCount)
        {
            _chatList.RemoveAt(_chatList.Count - 1);
        }
        EventManager.Invoke<int>(EventID.EVNT_CHAT_DATA_UPDATE, channel);
    }

    public void AddChatData(int channel, string name, string content)
    {
        ChatData data = new ChatData { m_channel = channel, m_profession = 1, m_name = name, m_content = content };
        AddChatData(channel, data);
    }

    public void SendChatData(int channel, string content, string receiverName = "")
    {
        if (string.IsNullOrEmpty(content)) return;
        if (IsGMInstruction(content)) return;

        string str = _sensitivewordFilter.ReplaceSensitiveWord(content);
        _avatar.baseCall("chat_sendMessage", new object[] { channel, receiverName, str });
    }
    /// <summary>
    /// GM指令
    /// </summary>
    private bool IsGMInstruction(string content)
    {
        // 以':'开头，每个数据使用' '单空格隔开
        if (' ' == content[0]) return false;
        string key = content.Split(' ')[0];
        if (null == key || ':' != key[0]) return false;

        //int id = 0;
        //UIWin roleMenu = UIManager.instance.GetCacheUI(WinID.UIRoleMenu);
        //if (UIManager.instance.IsActiveUI(WinID.UIRoleMenu) &&
        //    roleMenu.gameObject.activeSelf)
        //{
        //    id = roleMenu.GetComponent<UIRoleMenu>().GetRoleId();
        //}
        //else
        //{
        //    id = GameMain.Player.id;
        //}
        key = key.Substring(1);
        string value = content.Substring(content.IndexOf(' ') + 1);
        GameMain.Player.cellCall("executeGMInstruction", new object[] { GameMain.Player.id, key, value });

        return true;
    }

    #region 聊天频道屏蔽状态设定
    private Dictionary<int, bool> _shieldChannelDict = new Dictionary<int, bool>();
    public bool GetChannelState(int channel)
    {
        bool state;
        if(_shieldChannelDict.TryGetValue(channel, out state))
        {
            return state;
        }
        else
        {
            return true;
        }
    }
    public void SetChannelState(int channel, bool state)
    {
        _shieldChannelDict[channel] = state;
    }
    #endregion

}
