﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 点击聊天头像显示
/// </summary>
public class UIChatMenu : UIWin
{
    protected override void OnInit()
    {
        InitItem();
    }
    private ChatModule.ChatData _data;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _data = (ChatModule.ChatData)args[0];
        _name.text = _data.m_name;
        if (false)
        {
            btnTeamName.text = "申请入队";
        }
        else
        {
            btnTeamName.text = "邀请入队";
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private UILabel _name;
    private UILabel btnTeamName;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform icon = center.Find("Menu/Icon");
        _name = icon.Find("Name").GetComponent<UILabel>();

        Transform grid = center.Find("Menu/UIGrid");
        GameObject btnTeam = grid.Find("2").gameObject;
        btnTeamName = btnTeam.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnTeam, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
//             if (true)
//             {
//                 GameMain.Player.team.RequestJoinTeamNear(_data.entityID, _data.name);
//             }
//             else
//             {
//                 GameMain.Player.team.InviteTeammate(_data.entityID, _data.name);
//             }
        });
    }
}
