﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 聊天界面的拓展界面
/// </summary>
public class UIChatExpand : UIWin
{
    protected override void OnInit()
    {
        InitItem();
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }


    private void InitItem()
    {
        Transform bottomLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomLeft);
        GameObject btnClose = bottomLeft.Find("Root/BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }
    private WinPage _curContent;
    private UI.Tab _uiTab;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            _curContent.Open(this);
        };
    }
}
