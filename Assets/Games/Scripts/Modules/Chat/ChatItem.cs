﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 主界面聊天信息Item
/// </summary>
public class ChatItem : WinItem
{
    private UISprite channel;
    public string Channel
    {
        set { channel.spriteName = value; }
    }
    private UI.Label customText;
    public string InframeText
    {
        set 
        {
            customText.text = value;
        }
    }
    
    protected override void OnInit()
    {
        channel = transform.Find("Channel").GetComponent<UISprite>();
        customText = transform.Find("CustomText").GetComponent<UI.Label>();
        customText.onExpressionVerify = (eName) => 
        {
            return GameMain.Player.chatModule.ExpressionVerify(eName);
        };
    }
}
