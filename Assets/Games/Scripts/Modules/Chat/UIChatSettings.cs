﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 聊天频道设定界面
/// </summary>
public class UIChatSettings : UIWin
{
    protected override void OnInit()
    {
        InitItem();
        InitChannel();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateState();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject btnSure = center.Find("BtnSure").gameObject;
        InputManager.instance.AddClickListener(btnSure, (go) =>
        {
            SetState();
            UIManager.instance.CloseWindow(winID);
        });
    }
    private const int Num = 5;
    private UIToggle[] _items = new UIToggle[Num];
    private void InitChannel()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform channel = center.Find("Channel");
        for (int i = 0; i < Num; i++)
        {
            UIToggle item = channel.Find(i.ToString()).GetComponent<UIToggle>();
            _items[i] = item;
        }
    }

    private void SetState()
    {
        for (int i = 0; i < Num; i++)
        {
            UIToggle item = _items[i];
            int cur = GetChannel(i);
            GameMain.Player.chatModule.SetChannelState(cur, item.value);
        }
    }

    private void UpdateState()
    {
        for (int i = 0; i < Num; i++)
        {
            UIToggle item = _items[i];
            int cur = GetChannel(i);
            item.Set(GameMain.Player.chatModule.GetChannelState(cur));
        }
    }
    private int GetChannel(int index)
    {
        switch (index)
        {
            case 0:
                return Define.CHAT_CHANNEL_WORLD;
                break;
            case 1:
                return Define.CHAT_CHANNEL_FACTION;
                break;
            case 2:
                return Define.CHAT_CHANNEL_SCHOOL;
                break;
            case 3:
                return Define.CHAT_CHANNEL_CURRENT;
                break;
            case 4:
                return Define.CHAT_CHANNEL_TEAM;
                break;
            default:
                return Define.CHAT_CHANNEL_WORLD;
                break;
        }
    }

}
