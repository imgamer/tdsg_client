﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 频道界面Item
/// </summary>
public class ChannelItem : WinItem
{
    private UISprite icon;
    public string Icon
    {
        set { icon.spriteName = value; }
    }
    private GameObject _iconObject;
    public GameObject IconObject
    {
        get { return _iconObject; }
    }
    private UISprite channel;
    public string Channel
    {
        set { channel.spriteName = value; }
    }
    private UILabel name;
    public string Name
    {
        set { name.text = value; }
    }
    private UISprite bg;
    private UI.Label customText;
    public string InframeText
    {
        set
        {
            if (customText.text == value) return;
            customText.text = value;
            float width = customText.printedSize.x;
            float hight = customText.printedSize.y;
            bg.width = (int)width + customText.fontSize;
            bg.height = (int)(hight + 0.5f * customText.fontSize);
        }
    }
    protected override void OnInit()
    {
        _iconObject = transform.Find("Icon").gameObject;
        icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        name = transform.Find("Name").GetComponent<UILabel>();
        channel = transform.Find("Channel").GetComponent<UISprite>();
        customText = transform.Find("CustomText").GetComponent<UI.Label>();
        customText.onExpressionVerify = (eName) =>
        {
            return GameMain.Player.chatModule.ExpressionVerify(eName);
        };
        bg = transform.Find("BG").GetComponent<UISprite>();
    }
}
