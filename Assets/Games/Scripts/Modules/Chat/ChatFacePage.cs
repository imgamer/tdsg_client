﻿using UnityEngine;
using System.Collections;

public class ChatFacePage : WinPage
{
    protected override void OnInit()
    {
        InitFace();
    }

    protected override void OnOpen(params object[] args)
    {
        _uiGrid.GoToItem(0, 20);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UI.Grid _uiGrid;
    private void InitFace()
    {
        Transform grid = transform.Find("View/UIGrid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        for (int i = 100; i < 120; i++)
        {
            string name = i.ToString();
            GameObject item = grid.Find(name).gameObject;
            InputManager.instance.AddClickListener(item, (go) => 
            {
                EventManager.Invoke<string>(EventID.EVNT_CHAT_DATA_ADD, string.Format("#e{0}", name));
            });
        }
        _uiGrid.Reposition();
    }
}
