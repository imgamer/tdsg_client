﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 宠物图鉴界面
/// </summary>
public class PetGuidePage : WinPage
{
    protected override void OnInit()
    {
        InitTab();
        InitDetailPage();
        InitGetPage();
        InitPetShow();
        InitPetList();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Close();
        UIPet.ExhibitPage.Close();
        SwitchPetList(0);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    #region 初始化
    private UI.Tab _uiTab;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            if(index == 0)
            {
                UpdateDetailPage();
            }
        };
    }

    private UIProgressBar[] _bars = new UIProgressBar[5];
    private UILabel _desc;
    private void InitDetailPage()
    {
        Transform grid = transform.Find("DetailPage/Grid");
        for (int i = 0; i < _bars.Length; i++)
        {
            Transform child = grid.Find(i.ToString());
            _bars[i] = child.GetComponent<UIProgressBar>();
        }
        _desc = transform.Find("DetailPage/Desc").GetComponent<UILabel>();
    }
    private void InitGetPage()
    {
        GameObject btnGet = transform.Find("GetPage/BtnGet").gameObject;
        InputManager.instance.AddClickListener(btnGet, (go) => 
        {

        });
        GameObject btnBuy = transform.Find("GetPage/BtnBuy").gameObject;
        InputManager.instance.AddClickListener(btnBuy, (go) =>
        {

        });
    }
    private byte GetType(int index)
    {
        byte type = Define.PET_TYPE_FRIENDLY;
        switch (index)
        {
            case 0:
                type = Define.PET_TYPE_FRIENDLY;
                break;
            case 1:
                type = Define.PET_TYPE_SUPER;
                break;
            case 2:
                type = Define.PET_TYPE_WILD;
                break;
        }
        return type;
    }


    private Transform _modelRoot;
    private UILabel _level;
    private UILabel _name;
    private Transform _skillSelect;
    private const int SkillNum = 5;
    private UI.Grid _skillUIGrid;
    private PetSkillItem[] _skillItems = new PetSkillItem[SkillNum];
    private void InitPetShow()
    {
        Transform trans = transform.Find("PetShow");
        _modelRoot = trans.Find("ModelRoot");
        _level = trans.Find("Level/Value").GetComponent<UILabel>();
        _name = trans.Find("Name/Text").GetComponent<UILabel>();

        _skillSelect = trans.Find("Skills/Select");
        Transform grid = trans.Find("Skills/UIGrid");
        _skillUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, SkillNum);
        for (int i = 0; i < SkillNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(ParentUI);
            _skillItems[i] = item;
        }
        _skillUIGrid.Reposition();
    }


    private Transform _petSelect;
    private const int PetNum = 24;
    private UI.Grid _petUIGrid;
    private List<PetGuideItem> _petItems = new List<PetGuideItem>();
    private Transform _menuSelect;
    private GameObject _selectMenu;
    private GameObject[] _btnMenus = new GameObject[7];
    private UILabel _curSelectName;
    private void InitPetList()
    {
        GameObject btnSelect = transform.Find("SelectList/BtnSelect").gameObject;
        _curSelectName = btnSelect.transform.Find("Text").GetComponent<UILabel>();
        Transform mark = btnSelect.transform.Find("Mark");
        InputManager.instance.AddClickListener(btnSelect, (go) =>
        {
            bool preOpen = _selectMenu.activeSelf;
            if (preOpen)
            {
                mark.localEulerAngles = Vector3.zero;
                _selectMenu.SetActive(false);
            }
            else
            {
                mark.localEulerAngles = new Vector3(0, 0, 180);
                _selectMenu.SetActive(true);
            }
        });
        _selectMenu = transform.Find("SelectList/Menu").gameObject;
        _menuSelect = _selectMenu.transform.Find("Select");
        Transform btnGrid = _selectMenu.transform.Find("UIGrid");
        for (int i = 0; i < _btnMenus.Length; i++)
        {
            int index = i;
            GameObject btn = btnGrid.Find(i.ToString()).gameObject;
            _btnMenus[index] = btn;
            InputManager.instance.AddClickListener(btn, (go) =>
            {
                mark.localEulerAngles = Vector3.zero;
                SwitchPetList(index);
            });
        }

        _petSelect = transform.Find("PetList/Select");
        Transform grid = transform.Find("PetList/UIGrid");
        _petUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, PetNum);
        for (int i = 0; i < PetNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetGuideItem item = child.GetComponent<PetGuideItem>();
            item.Init(ParentUI);
            _petItems.Add(item);
        }
        _petUIGrid.Reposition();
    }
    #endregion

    #region 更新
    private void UpdateDetailPage()
    {

    }
    private void UpdateSkillItems(PetsData pet)
    {
        _skillSelect.gameObject.SetActive(false);
        if (pet == null)
        {
            for (int i = 0; i < SkillNum; i++)
            {
                PetSkillItem item = _skillItems[i];
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        else
        {
            for (int i = 0; i < SkillNum; i++)
            {
                PetSkillItem item = _skillItems[i];
                if (i < pet.skills.Count)
                {
                    Skill.SkillBase data = SkillManager.instance.GetSkill(pet.skills[i]);
                    item.Icon = data.icon;
                    item.Mark = false;
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SelectItem(_skillSelect, go.transform);
                    });
                }
                else
                {
                    item.Icon = string.Empty;
                    item.Mark = false;
                    InputManager.instance.RemoveClickListener(item.gameObject);
                }
            }
        }
    }
    private void SelectItem(Transform select, Transform item)
    {
        select.parent = item;
        select.localPosition = Vector3.zero;
        select.localScale = Vector3.one;
        select.gameObject.SetActive(true);
    }

    private void SwitchPetList(int index)
    {
        _selectMenu.SetActive(false);
        Transform item = _btnMenus[index].transform;
        SelectItem(_menuSelect, _btnMenus[index].transform);
        _curSelectName.text = item.Find("Text").GetComponent<UILabel>().text;
        List<PetsData> list = new List<PetsData>();
        switch (index)
        {
            case 0:
                list = PetsInitialConfig.SharedInstance.GetAllPetList();
                break;
            case 1:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(0, 30);
                break;
            case 2:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(31, 60);
                break;
            case 3:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(61, 70);
                break;
            case 4:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(71, 80);
                break;
            case 5:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(81, 90);
                break;
            case 6:
                list = PetsInitialConfig.SharedInstance.GetPetListByLevelLimit(91, 100);
                break;
            default:
                break;
        }
        UpdatePetItems(list);
    }

    private void UpdatePetItems(List<PetsData> list)
    {
        int num = list.Count;
        if (num <= 0)
        {
            for (int i = 0; i < PetNum; i++)
            {
                PetGuideItem item = _petItems[i];
                item.gameObject.SetActive(false);
            }
        }
        else
        {
            SwitchPet(_petSelect, _petItems[0].transform, list[0]);
            for (int i = 0; i < PetNum; i++)
            {
                PetGuideItem item = _petItems[i];
                if (i < num)
                {
                    PetsData data = list[i];
                    item.gameObject.SetActive(true);
                    //item.Icon = "UI-s2-Role-renwu1";
                    item.Level = data.level;
                    item.State = data.takeLevel > GameMain.Player.Level;
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchPet(_petSelect, item.transform, data);
                    });
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }
    private PetsData _curPetsData;
    private void SwitchPet(Transform select, Transform item, PetsData data)
    {
        _curPetsData = data;
        UpdateSkillItems(_curPetsData);
        SetEntity(_curPetsData);
        _uiTab.SwitchTo(0);
        SelectItem(select, item);
    }

    #region 设置宠物模型
    private void SetEntity(PetsData petData)
    {
        string resName = petData.modelID;
        if (string.IsNullOrEmpty(resName)) return;
        _level.text = string.Format("{0}", petData.takeLevel);
        _name.text = petData.name;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                ClearModel();
                o.transform.parent = _modelRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
    private void ClearModel()
    {
        for (int i = 0; i < _modelRoot.childCount; i++)
        {
            Transform child = _modelRoot.GetChild(i);
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
    }
    #endregion
    #endregion

}
