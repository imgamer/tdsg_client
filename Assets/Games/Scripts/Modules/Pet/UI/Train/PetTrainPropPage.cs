﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
using Item;
using System;
/// <summary>
/// 宠物培养物品选择界面
/// </summary>
public class PetTrainPropPage : WinPage
{
    public Action<int> selectCB;
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private PetTrainPropItem _template;
    private Transform _select;
    private List<PetTrainPropItem> _items = new List<PetTrainPropItem>();
    private Grid _uiGrid;
    private void InitItem()
    {
        _select = transform.Find("Root/ScrollView/Select");
        Transform grid = transform.Find("Root/ScrollView/Grid");
        _uiGrid = grid.GetComponent<Grid>();
        _template = grid.Find("Item").GetComponent<PetTrainPropItem>();
        _template.gameObject.SetActive(false);
    }

    private void UpdateItems()
    {
        SelectItem(null);
        List<int> itemsData = Define.PET_CACTOR_TO_CONSUME;
        int dataCount = itemsData.Count;
        int itemCount= _items.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount - 1; i++)
            {
                if (i < itemCount)
                {
                    _items[i].gameObject.SetActive(true);
                    SetItemData(_items[i], itemsData[i+1]);
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    PetTrainPropItem item = Instantiate(_template) as PetTrainPropItem;
                    item.Init(ParentUI);
                    _template.gameObject.SetActive(false);
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    SetItemData(_items[i], itemsData[i +1]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_items[i], itemsData[i + 1]);
                }
                else
                {
                    _items[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(PetTrainPropItem item, int itemID)
    {
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(itemID);
        if (itemData == null) return;
        item.Name = itemData.name;
        item.Icon = itemData.icon;
        item.Desc = itemData.description;

        List<ItemBase> bagItem = GameMain.Player.kitbagModule.GetItemsByID(itemID);
        if (bagItem.Count == 0)
        {
            item.Count = "0";
        }
        else
        {
            item.Count = bagItem[0].Amount.ToString();
        }
        
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SelectItem(item.transform);
            if (selectCB != null) selectCB(itemID);
            Close();
        });
    }

    private void SelectItem(Transform item)
    {
        if (item == null)
        {
            _select.gameObject.SetActive(false);
            return;
        }
        _select.gameObject.SetActive(true);
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
    }
}
