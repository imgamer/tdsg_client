﻿using UnityEngine;
using System.Collections;
using Item;
using System.Collections.Generic;
/// <summary>
/// 宠物进阶界面
/// </summary>
public class UIPetEvolution : UIWin
{
    private const int Num = 6;
    private UILabel[] _values = new UILabel[Num];
    private UILabel[] _nexts = new UILabel[Num];
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        InitItem();
        InitIcon();
    }

    private PetInfo _petData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _petData = (PetInfo)args[0];
        if (_petData == null) return;
        UpdateAptitude(_petData);
        UpdateIcon(_petData);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
    }

    private GameObject _btnUse;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        _btnUse = center.Find("BtnUse").gameObject;
        InputManager.instance.AddClickListener(_btnUse, (go) =>
        {
            if (_petData == null) return;
            GameMain.Player.petModule.RequestToUpgradeStage(_petData.dbid);
        });
        Transform grid = center.Find("Grid");
        for (int i = 0; i < Num; i++)
        {
            Transform item = grid.Find(i.ToString());
            _values[i] = item.Find("Value").GetComponent<UILabel>();
            _nexts[i] = item.Find("Next/Value").GetComponent<UILabel>();
        }
    }

    private UISprite _leftIcon;
    private UISprite _leftStageIcon;
    private UILabel _leftStageValue;

    private UISprite _rightIcon;
    private UISprite _rightStageIcon;
    private UILabel _rightStageValue;

    private GameObject _item;
    private UISprite _itemIcon;
    private UILabel _itemName;
    private UILabel _itemNum;
    private void InitIcon()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform leftTrans = center.Find("Left");
        _leftIcon = leftTrans.Find("Icon").GetComponent<UISprite>();
        _leftStageIcon = leftTrans.Find("Stage/Icon").GetComponent<UISprite>();
        _leftStageValue = leftTrans.Find("Stage/Value").GetComponent<UILabel>();

        Transform rightTrans = center.Find("Right");
        _rightIcon = rightTrans.Find("Icon").GetComponent<UISprite>();
        _rightStageIcon = rightTrans.Find("Stage/Icon").GetComponent<UISprite>();
        _rightStageValue = rightTrans.Find("Stage/Value").GetComponent<UILabel>();

        Transform itemTrans = center.Find("Item");
        _item = itemTrans.gameObject;
        _itemIcon = itemTrans.Find("Icon").GetComponent<UISprite>();
        _itemName = itemTrans.Find("Name").GetComponent<UILabel>();
        _itemNum = itemTrans.Find("Num").GetComponent<UILabel>();
    }

    private void UpdateAptitude(PetInfo pet)
    {
        PetsFactors petsFactors = PetsFactorsConfig.SharedInstance.GetPetFactorsData(pet.utype, pet.type);
        if (petsFactors == null) return;
        PetStageEnhanceData curEnhance = PetStageEnhanceConfig.SharedInstance.GetEnhanceByStage((byte)(pet.stage));
        if (curEnhance == null) return;

        int physicsPowerFactor = petsFactors.physicsPowerFactor[1];
        int magicPowerFactor = petsFactors.magicPowerFactor[1];
        int physicsDefenseFactor = petsFactors.physicsDefenseFactor[1];
        int magicDefenseFactor = petsFactors.magicDefenseFactor[1];
        int hpFactor = petsFactors.hpFactor[1];

        _values[0].text = string.Format("{0}", GetUpperLimit(physicsPowerFactor, curEnhance.physicsPowerFactorUpperLimit));
        _values[1].text = string.Format("{0}", GetUpperLimit(magicPowerFactor, curEnhance.magicPowerFactorUpperLimit));
        _values[2].text = string.Format("{0}", GetUpperLimit(physicsDefenseFactor, curEnhance.physicsDefenseFactorUpperLimit));
        _values[3].text = string.Format("{0}", GetUpperLimit(magicDefenseFactor, curEnhance.magicDefenseFactorUpperLimit));
        _values[4].text = string.Format("{0}", GetUpperLimit(hpFactor, curEnhance.hpFactorUpperLimit));
        _values[5].text = string.Format("{0}", 0);

        PetStageEnhanceData nextEnhance = PetStageEnhanceConfig.SharedInstance.GetEnhanceByStage((byte)(pet.stage + 1));
        if (nextEnhance == null) return;
        _nexts[0].text = string.Format("{0}", GetUpperLimit(physicsPowerFactor, nextEnhance.physicsPowerFactorUpperLimit));
        _nexts[1].text = string.Format("{0}", GetUpperLimit(magicPowerFactor, nextEnhance.magicPowerFactorUpperLimit));
        _nexts[2].text = string.Format("{0}", GetUpperLimit(physicsDefenseFactor, nextEnhance.physicsDefenseFactorUpperLimit));
        _nexts[3].text = string.Format("{0}", GetUpperLimit(magicDefenseFactor, nextEnhance.magicDefenseFactorUpperLimit));
        _nexts[4].text = string.Format("{0}", GetUpperLimit(hpFactor, nextEnhance.hpFactorUpperLimit));
        _nexts[5].text = string.Format("{0}", 0);
    }

    private int GetUpperLimit(int value, double factor)
    {
        return (int)(value * (1 + factor));
    }

    private void UpdateIcon(PetInfo pet)
    {
        //_leftIcon.spriteName = "";
        _leftStageIcon.spriteName = PetRule.GetStageFrameName(pet.type, pet.stage);
        _leftStageValue.text = string.Format("{0}", pet.stage);

        //_rightIcon.spriteName = "";
        _rightStageIcon.spriteName = PetRule.GetStageFrameName(pet.type, (byte)(pet.stage + 1));
        _rightStageValue.text = string.Format("{0}", pet.stage + 1);

        PetStageUpgradeConsumeData consumeData = PetStageUpgradeConsumeConfig.SharedInstance.GetUpgradeConsume(pet.type);
        if (consumeData == null) return;
        int needCount = 0;
        int bagCount = 0;
        consumeData.amount.TryGetValue(pet.stage.ToString(), out needCount);
        List<ItemBase> bagItem = GameMain.Player.kitbagModule.GetItemsByID(consumeData.itemID);
        if (bagItem.Count == 0)
        {
            bagCount = 0;
            UITools.SetButtonState(_btnUse, false);
        }
        else
        {
            UITools.SetButtonState(_btnUse, true);
            bagCount = bagItem[0].Amount;
        }

        ItemsData consumeItemData = ItemsConfig.SharedInstance.GetItemData(consumeData.itemID);
        if (consumeItemData == null) return;
        _itemIcon.spriteName = consumeItemData.icon;
        _itemName.text = consumeItemData.name;
        _itemNum.text = string.Format("{0}/{1}", needCount, bagCount);
        ItemDetailManager.instance.AddListenerItemDetail(_item, AnchorType.Center, SourcePage.Other, consumeItemData);
    }

    private void UpdatePet(PetInfo pet)
    {
        if (!Active) return;
        _petData = pet;
        if (_petData == null) return;
        UpdateAptitude(_petData);
        UpdateIcon(_petData);
    }
}
