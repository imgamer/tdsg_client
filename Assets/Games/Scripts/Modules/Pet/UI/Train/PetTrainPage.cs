﻿using UnityEngine;
using System.Collections;
using Item;
using System.Collections.Generic;
/// <summary>
/// 宠物培养界面
/// </summary>
public class PetTrainPage : WinPage
{
    private int _trainItemID;
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateAptitude);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateAptitude);
        InitItem();
        InitAptitude();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Open(ParentUI);
        UIPet.ExhibitPage.Open(ParentUI);
        ResetItem();
        UpdateAptitude();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateAptitude);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateAptitude);
    }

    private UISprite _icon;
    private UISprite _add;
    private UILabel _name;
    private UILabel _count;
    private GameObject _btnUse;
    private PetTrainPropPage _propPage;
    private void InitItem()
    {
        Transform detail = transform.Find("Detail");
        Transform item = detail.Find("Item");
        _propPage = transform.Find("PropPage").GetComponent<PetTrainPropPage>();
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _propPage.Open(ParentUI);
        });
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _add = item.Find("Add").GetComponent<UISprite>();
        _name = item.Find("Name").GetComponent<UILabel>();
        _count = item.Find("Count").GetComponent<UILabel>();
        _btnUse = detail.Find("BtnUse").gameObject;
        InputManager.instance.AddClickListener(_btnUse, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null) return;
            int index = Define.PET_CACTOR_TO_CONSUME.IndexOf(_trainItemID);
            GameMain.Player.petModule.RequestToEnhanceFactor(pet.dbid, index, 1);
        });
        GameObject btnEvolve = detail.Find("BtnEvolve").gameObject;
        InputManager.instance.AddClickListener(btnEvolve, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null || pet.stage >= 5) return;
            UIManager.instance.OpenWindow(null, WinID.UIPetEvolution, pet);
        });
        _propPage.selectCB = (id) =>
        {
            _trainItemID = id;
            UpdateConsumeItem(id);
        };
    }

    private void ResetItem()
    {
        _icon.enabled = false;
        _count.enabled = false;
        _name.text = "请放入强化丹";
        _add.enabled = true;
        _trainItemID = 0;
        UITools.SetButtonState(_btnUse, false);
        _propPage.Close();
    }

    public class Aptitude
    {
        public UIProgressBar bar;
        public UILabel value;
        public UILabel add;
    }
    private const int Num = 5;
    private Aptitude[] _aptitudes = new Aptitude[Num];
    private UILabel _aptitudeValue;
    private void InitAptitude()
    {
        Transform grid = transform.Find("Detail/Grid");
        for (int i = 0; i < Num; i++)
        {
            Aptitude data = new Aptitude();
            Transform item = grid.Find(i.ToString());
            data.bar = item.GetComponent<UIProgressBar>();
            data.value = item.Find("Value").GetComponent<UILabel>();
            data.add = item.Find("Add").GetComponent<UILabel>();
            _aptitudes[i] = data;
        }
        _aptitudeValue = transform.Find("Detail/Aptitude/Value").GetComponent<UILabel>();
    }

    private void UpdateAptitude()
    {
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;
        PetsFactors petsFactors = PetsFactorsConfig.SharedInstance.GetPetFactorsData(pet.utype, pet.type);
        if (petsFactors == null) return;
        PetStageEnhanceData curEnhanceData = PetStageEnhanceConfig.SharedInstance.GetEnhanceByStage(pet.stage);
        if (curEnhanceData == null) return;

        _aptitudeValue.text = string.Format("{0:N3}", pet.growthFactor);
        SetAptitude(0, pet.physicsPowerFactor, GetUpperLimit(petsFactors.physicsPowerFactor[1], curEnhanceData.physicsPowerFactorUpperLimit));
        SetAptitude(1, pet.magicPowerFactor, GetUpperLimit(petsFactors.magicPowerFactor[1], curEnhanceData.magicPowerFactorUpperLimit));
        SetAptitude(2, pet.physicsDefenseFactor, GetUpperLimit(petsFactors.physicsDefenseFactor[1], curEnhanceData.physicsDefenseFactorUpperLimit));
        SetAptitude(3, pet.magicDefenseFactor, GetUpperLimit(petsFactors.magicDefenseFactor[1], curEnhanceData.magicDefenseFactorUpperLimit));
        SetAptitude(4, pet.hpFactor, GetUpperLimit(petsFactors.hpFactor[1], curEnhanceData.hpFactorUpperLimit));
    }

    private float GetUpperLimit(int value, double factor)
    {
        return (float)(value * (1 + factor));
    }

    private void SetAptitude(int index, int value, float max)
    {
        Aptitude item = _aptitudes[index];
        item.bar.value = value / (float)max;
        item.value.text = string.Format("{0}", value);
        item.add.text = string.Empty;
    }

    private void UpdateAptitude(PetInfo o)
    {
        if (!Active) return;
        UpdateAptitude();
        if(_trainItemID != 0)
        {
            UpdateConsumeItem(_trainItemID);
        }
    }

    public void UpdateConsumeItem(int itemID)
    {
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(itemID);
        if (itemData == null) return;
        _name.text = itemData.name;
        _icon.spriteName = itemData.icon;
        _icon.enabled = true;
        _count.enabled = true;
        _add.enabled = false;
        List<ItemBase> bagItem = GameMain.Player.kitbagModule.GetItemsByID(itemID);
        if (bagItem.Count == 0)
        {
            UITools.SetButtonState(_btnUse, false);
            _count.text = "0";
        }
        else
        {
            UITools.SetButtonState(_btnUse, true);
            _count.text = bagItem[0].Amount.ToString();
        }
    }
}
