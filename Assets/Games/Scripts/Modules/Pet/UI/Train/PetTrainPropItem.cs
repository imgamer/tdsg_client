﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物道具Item
/// </summary>
public class PetTrainPropItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private UILabel _desc;
    public string Desc
    {
        set
        {
            _desc.text = value;
        }
    }

    private UILabel _count;
    public string Count
    {
        set
        {
            _count.text = value;
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _desc = transform.Find("Desc").GetComponent<UILabel>();
        _count = transform.Find("Count").GetComponent<UILabel>();
    }
}
