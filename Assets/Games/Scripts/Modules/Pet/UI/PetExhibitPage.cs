﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
/// <summary>
/// 宠物模型展示
/// </summary>
public class PetExhibitPage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateEntity);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        SetEntity(pet);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateEntity);
    }

    private UILabel _type;
    private UISprite _stageIcon;
    private UILabel _stageValue;
    private UILabel _power;
    private Transform _modelRoot;

    private void InitItem()
    {
        Transform trans = transform.Find("Pet");
        _modelRoot = trans.Find("ModelRoot");
        _power = trans.Find("Power/Value").GetComponent<UILabel>();
        _type = trans.Find("Type").GetComponent<UILabel>();
        _stageIcon = trans.Find("Stage/Icon").GetComponent<UISprite>();
        _stageValue = trans.Find("Stage/Value").GetComponent<UILabel>();
    }

    private void SetEntity(PetInfo pet)
    {
        if (pet == null) return;
        PetsData petData = PetsInitialConfig.SharedInstance.GetPetsData(pet.utype);
        if (petData == null) return;

        _stageIcon.spriteName = PetRule.GetStageIconName(pet.stage);
        _stageValue.text = string.Format("{0}", pet.stage);
        _power.text = string.Format("{0}", 0);
        _type.text = PetRule.GetTypeDesc(pet.type);

        string resName = petData.modelID;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                ClearModel();
                o.transform.parent = _modelRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
    private void UpdateEntity(PetInfo pet)
    {
        if (!Active) return;
        SetEntity(pet);
    }
    private void ClearModel()
    {
        for (int i = 0; i < _modelRoot.childCount; i++)
        {
            Transform child = _modelRoot.GetChild(i);
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
    }

}
