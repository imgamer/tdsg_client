﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物属性——基础属性界面
/// </summary>
public class PetAttributePage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdateButton);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdatePet);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        InitItem();
        InitDetail();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Open(ParentUI);
        UIPet.ExhibitPage.Open(ParentUI);
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;
        UpdateButton(pet);
        UpdateAttribute(pet);
        SetState(_curState);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdateButton);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdatePet);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
    }

    private UILabel _name;
    private UILabel _level;
    private UILabel _expValue;
    private UIProgressBar _expBar;

    private UILabel _btnBattleName;
    private GameObject _btnBattle;
    private void InitItem()
    {
        _name = transform.Find("Name/Text").GetComponent<UILabel>();
        GameObject btnEdit = transform.Find("Name/BtnEdit").gameObject;
        InputManager.instance.AddClickListener(btnEdit, (go) => 
        {

        });
        Transform expTrans = transform.Find("Exp");
        _level = expTrans.Find("Level").GetComponent<UILabel>();
        _expValue = expTrans.Find("Value").GetComponent<UILabel>();
        _expBar = expTrans.GetComponent<UIProgressBar>();
        GameObject btnAdd = expTrans.Find("BtnAdd").gameObject;
        InputManager.instance.AddClickListener(btnAdd, (go) =>
        {
            
        });

        WinPage aptitudePage = transform.parent.Find("Aptitude").GetComponent<WinPage>();
        GameObject btnAptitude = transform.Find("BtnAptitude").gameObject;
        InputManager.instance.AddClickListener(btnAptitude, (go) =>
        {
            aptitudePage.Open(ParentUI);
            this.Close();
        });
        GameObject btnPunctuate = transform.Find("BtnPunctuate").gameObject;
        InputManager.instance.AddClickListener(btnPunctuate, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIPetPunctuate);
        });

        GameObject btnFree = transform.Find("BtnFree").gameObject;
        InputManager.instance.AddClickListener(btnFree, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null) return;
            if (pet.dbid == GameMain.Player.FightingPetDBID)
            {
                TipManager.instance.ShowTextTip(Define.PET_FIGHT_NOT_FREE);
                return;
            }
            
            TipManager.instance.ShowTwoButtonTip(Define.PET_FREE, () =>
            {
                GameMain.Player.petModule.RequestFree(pet.dbid);
                return true;
            }, null);     
        });

        _btnBattle = transform.Find("BtnBattle").gameObject;
        _btnBattleName = _btnBattle.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_btnBattle, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (!PetRule.CanOutFight(pet)) return;
            GameMain.Player.petModule.RequestBattle(pet.dbid);
        });
    }

    private UILabel[] _baseAttributes = new UILabel[4];
    private UILabel[] _allAttributes = new UILabel[10];
    private GameObject[] _states = new GameObject[2];
    private void InitDetail()
    {
        Transform trans0 = transform.Find("Detail/State/0");
        _states[0] = trans0.gameObject;
        GameObject btnMore = trans0.Find("BtnMore").gameObject;
        InputManager.instance.AddClickListener(btnMore, (go) =>
        {
            SetState(1);
        });
        Transform baseGrid = trans0.Find("Grid");
        for (int i = 0; i < _baseAttributes.Length; i++)
        {
            Transform child = baseGrid.Find(i.ToString());
            _baseAttributes[i] = child.Find("Value").GetComponent<UILabel>();
        }

        Transform trans1 = transform.Find("Detail/State/1");
        _states[1] = trans1.gameObject;
        GameObject btnBack = trans1.Find("BtnBack").gameObject;
        InputManager.instance.AddClickListener(btnBack, (go) =>
        {
            SetState(0);
        });
        Transform allGrid = trans1.Find("Grid");
        for (int i = 0; i < _allAttributes.Length; i++)
        {
            Transform child = allGrid.Find(i.ToString());
            _allAttributes[i] = child.Find("Value").GetComponent<UILabel>();
        }
    }

    private int _curState = 0;
    private void SetState(int index)
    {
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;

        _curState = index;
        for (int i = 0; i < _states.Length; i++)
        {
            _states[i].SetActive(i == _curState);
        }

        switch (_curState)
        {
            case 0:
                UpdateBase(pet);
                break;
            case 1:
                UpdateAll(pet);
                break;
            default:
                break;
        }
    }
    private void UpdateBase(PetInfo pet)
    {
        _baseAttributes[0].text = string.Format("{0}", pet.Hp);
        _baseAttributes[1].text = string.Format("{0}", pet.PhyAttack);
        _baseAttributes[2].text = string.Format("{0}", pet.PhyDefense);
        _baseAttributes[3].text = string.Format("{0}", pet.daoheng);
    }
    private void UpdateAll(PetInfo pet)
    {
        PetsData petConfig = PetsInitialConfig.SharedInstance.GetPetsData(pet.utype);
        if (petConfig == null) return;
        _allAttributes[0].text = string.Format("{0}", pet.Hp);
        _allAttributes[1].text = string.Format("{0}", pet.Speed);
        _allAttributes[2].text = string.Format("{0}", pet.PhyAttack);
        _allAttributes[3].text = string.Format("{0}", petConfig.crit);

        _allAttributes[4].text = string.Format("{0}", pet.MagAttack);
        _allAttributes[5].text = string.Format("{0}", petConfig.toughness);
        _allAttributes[6].text = string.Format("{0}", pet.PhyDefense);
        _allAttributes[7].text = string.Format("{0}", 0);

        _allAttributes[8].text = string.Format("{0}", pet.MagDefense);
        _allAttributes[9].text = string.Format("{0}", 0);
    }

    private void UpdateAttribute(PetInfo pet)
    {
        if (pet == null) return;
        _name.text = pet.Name;
        _level.text = string.Format("Lv.{0}", pet.level);
        int _expMax = PetRule.UpgradeLevelNeedExp(pet.level, (ushort)(pet.level + 1));
        _expValue.text = string.Format("{0}/{1}", pet.EXP, _expMax);
        _expBar.value = (float)pet.EXP / _expMax;
    }

    private void UpdatePet(PetInfo data)
    {
        if (!Active) return;
        UpdateButton(data);
        UpdateAttribute(data);
        SetState(_curState);
    }
    private void UpdateButton(object o)
    {
        if (!Active) return;
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        UpdateButton(pet);
    }
    private void UpdateButton(PetInfo pet)
    {
        if (pet == null) return;
        UITools.SetButtonState(_btnBattle, PetRule.CanOutFight(pet));
        if (pet.dbid == GameMain.Player.FightingPetDBID)
        {
            _btnBattleName.text = "休    息";
        }
        else
        {
            _btnBattleName.text = "出    战";
        }
    }
}
