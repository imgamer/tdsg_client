﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物属性界面
/// </summary>
public class PetPropertyPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        _attributePage.Open(ParentUI);
        _aptitudePage.Close();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private WinPage _attributePage;
    private WinPage _aptitudePage;
    private void InitItem()
    {
        _attributePage = transform.Find("Attribute").GetComponent<WinPage>();
        _aptitudePage = transform.Find("Aptitude").GetComponent<WinPage>();
    }

}
