﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
/// <summary>
/// 宠物属性——资质技能界面
/// </summary>
public class PetAptitudePage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdatePet);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        InitItem();
        InitSkill();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Open(ParentUI);
        UIPet.ExhibitPage.Close();
        UpdateAptitude();
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdatePet);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
    }

    private GameObject _item;
    private UILabel _itemName;
    private UILabel _itemNum;
    private UISprite _itemIcon;
    private UISprite _aptitudeIcon;
    private GameObject _btnUse;
    private UIProgressBar[] _bars = new UIProgressBar[5];
    private UILabel[] _aptitudes = new UILabel[6];
    private void InitItem()
    {
        Transform detail = transform.Find("Detail");
        _aptitudeIcon = detail.Find("Aptitude/Icon").GetComponent<UISprite>();
        Transform grid = detail.Find("Grid");
        for (int i = 0; i < _aptitudes.Length; i++)
        {
            Transform item = grid.Find(i.ToString());
            if (i < _bars.Length)
            {
                _bars[i] = item.GetComponent<UIProgressBar>();
            }
            _aptitudes[i] = item.Find("Value").GetComponent<UILabel>();
        }

        Transform itemTrans = detail.Find("Item");
        _item = itemTrans.gameObject;
        _itemIcon = itemTrans.Find("Icon").GetComponent<UISprite>();
        _itemName = itemTrans.Find("Name").GetComponent<UILabel>();
        _itemNum = itemTrans.Find("Num").GetComponent<UILabel>();

        WinPage attributePage = transform.parent.Find("Attribute").GetComponent<WinPage>();
        GameObject btnBack = detail.Find("BtnBack").gameObject;
        InputManager.instance.AddClickListener(btnBack, (go) => 
        {
            attributePage.Open(ParentUI);
            this.Close();
        });
        _btnUse = detail.Find("BtnUse").gameObject;
        InputManager.instance.AddClickListener(_btnUse, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null) return;
            if (!CanRejuvenate(pet)) return;
            GameMain.Player.petModule.RequestToRejuvenate(pet.dbid);
        });
    }
    private bool CanRejuvenate(PetInfo pet)
    {
        if(!PetRule.CanRejuvenate(pet.type, TipManager.instance.ShowTextTip))
        {
            return false;
        }
        return true;
    }
    private Transform _select;
    private const int Num = 10;
    private UI.Grid _uiGrid;
    private PetSkillItem[] _items = new PetSkillItem[Num];
    private void InitSkill()
    {
        Transform scrollView = transform.Find("SkillList/ScrollView");
        _select = scrollView.Find("Select");
        Transform grid = scrollView.Find("Grid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            _items[i] = item;
        }
        _uiGrid.Reposition();
    }
    private void UpdateItems()
    {
        _select.gameObject.SetActive(false);
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;
        for (int i = 0; i < Num; i++)
        {
            PetSkillItem item = _items[i];
            item.Init(ParentUI);
            if(i < pet.skills.Count)
            {
                Skill.SkillBase data = SkillManager.instance.GetSkill(pet.skills[i]);
                item.Icon = data.icon;
                item.Mark = pet.bindedSkills.Contains(data.id);
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    SelectItem(go.transform);
                });
            }
            else
            {
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        int consumeItemID = PetRule.GetWashsAptitudeConsumeID(pet.level);
        List<ItemBase> bagItem = GameMain.Player.kitbagModule.GetItemsByID(consumeItemID);
        if (bagItem.Count == 0)
        {
            UITools.SetButtonState(_btnUse, false);
        }
        else
        {
            UITools.SetButtonState(_btnUse, true);
        }

        ItemsData consumeItemData = ItemsConfig.SharedInstance.GetItemData(consumeItemID);
        if (consumeItemData == null) return;
        _itemIcon.spriteName = consumeItemData.icon;
        _itemName.text = consumeItemData.name;
        _itemNum.text = "x 1";
        ItemDetailManager.instance.AddListenerItemDetail(_item, AnchorType.Center, SourcePage.Other, consumeItemData);
    }
    private void SelectItem(Transform item)
    {
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
        _select.gameObject.SetActive(true);
    }
    private void UpdateAptitude()
    {
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;
        PetsFactors petsFactors = PetsFactorsConfig.SharedInstance.GetPetFactorsData(pet.utype, pet.type);
        if (petsFactors == null) return;
        PetStageEnhanceData curEnhance = PetStageEnhanceConfig.SharedInstance.GetEnhanceByStage(pet.stage);
        if (curEnhance == null) return;

        _bars[0].value = pet.physicsPowerFactor / GetUpperLimit(petsFactors.physicsPowerFactor[1], curEnhance.physicsPowerFactorUpperLimit);
        _aptitudes[0].text = string.Format("{0}", pet.physicsPowerFactor);

        _bars[1].value = pet.magicPowerFactor / GetUpperLimit(petsFactors.magicPowerFactor[1], curEnhance.magicPowerFactorUpperLimit);
        _aptitudes[1].text = string.Format("{0}", pet.magicPowerFactor);

        _bars[2].value = pet.physicsDefenseFactor / GetUpperLimit(petsFactors.physicsDefenseFactor[1], curEnhance.physicsDefenseFactorUpperLimit);
        _aptitudes[2].text = string.Format("{0}", pet.physicsDefenseFactor);

        _bars[3].value = pet.magicDefenseFactor / GetUpperLimit(petsFactors.magicDefenseFactor[1], curEnhance.magicDefenseFactorUpperLimit);
        _aptitudes[3].text = string.Format("{0}", pet.magicDefenseFactor);

        _bars[4].value = pet.hpFactor / GetUpperLimit(petsFactors.hpFactor[1], curEnhance.hpFactorUpperLimit);
        _aptitudes[4].text = string.Format("{0}", pet.hpFactor);

        _aptitudes[5].text = string.Format("{0:N3}", pet.growthFactor);
    }

    private float GetUpperLimit(int value, double factor)
    {
        return (float)(value * (1 + factor));
    }

    private void UpdatePet(PetInfo pet)
    {
        if (!Active) return;
        UpdateAptitude();
        UpdateItems();
    }
}
