﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 宠物加点界面
/// </summary>
public class UIPetPunctuate : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        InitItem();
        InitProperty();
        InitPunctuate();
    }

    private PetInfo pet;
    protected override void OnOpen(params object[] args)
    {
        pet = GameMain.Player.petModule.GetSelectedPet();
        UpdateAttribute();
        UpdateItems();
        GetRemain();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
    }
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }
    private const int Num = 6;
    private UILabel[] _values = new UILabel[Num];
    private UILabel[] _adds = new UILabel[Num];
    private UILabel _name;
    private void InitProperty()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _name = center.Find("Property/Name/Text").GetComponent<UILabel>();
        Transform grid = center.Find("Property/UIGrid");
        for (int i = 0; i < Num; i++)
        {
            Transform item = grid.Find(i.ToString());
            _values[i] = item.Find("Value").GetComponent<UILabel>();
            _adds[i] = item.Find("Up/Add").GetComponent<UILabel>();
            _adds[i].transform.parent.gameObject.SetActive(false);
        }
    }
    private UILabel _remain;
    private const int Count = 4;
    private PetPunctuateItem[] _punctuateItems = new PetPunctuateItem[Count];
    private void InitPunctuate()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform punctuate = center.Find("Punctuate");
        _remain = punctuate.Find("Remain/Value").GetComponent<UILabel>();
        GameObject btnHelp = punctuate.Find("BtnHelp").gameObject;
        InputManager.instance.AddClickListener(btnHelp, (go) => 
        {

        });
        GameObject btnReset = punctuate.Find("BtnReset").gameObject;
        InputManager.instance.AddClickListener(btnReset, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null) return;
            GameMain.Player.petModule.ReqResetPetFreePoint(pet.dbid);
        });
        GameObject btnSure = punctuate.Find("BtnSure").gameObject;
        InputManager.instance.AddClickListener(btnSure, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (pet == null) return;
            GameMain.Player.petModule.SyncPetFreePointAssignmentToServer(pet.dbid,
                _punctuateItems[0].ChangeValue, _punctuateItems[1].ChangeValue, _punctuateItems[2].ChangeValue, _punctuateItems[3].ChangeValue);
        });
        Transform grid = punctuate.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            PetPunctuateItem item = grid.Find(i.ToString()).GetComponent<PetPunctuateItem>();
            item.Init(this);
            _punctuateItems[i] = item;
        }
    }
    private void UpdateItems()
    {
        if (pet == null) return;
        for (int i = 0; i < Count; i++)
        {
            int index = i;
            PetPunctuateItem item = _punctuateItems[i];
            int[] values = GetValues(pet, i);
            item.SetInit(PetRule.GetMaxFreePoint(pet.level) + values[0], values[0], values[1]);
            item.SetRange(pet.freePoint);
            item.OnChange = () => 
            {
                int ret = GetRemain();
                OnUpdateItems(item, ret);
                OnUpdateAdds(index);
            };
        }
    }
    private int[] GetValues(PetInfo pet, int index)
    {
        int[] ret = new int[2];
        switch (index)
        {
            case 0:
                ret[0] = (int)pet.BaseForce;
                ret[1] = (int)pet.force;
                break;
            case 1:
                ret[0] = (int)pet.BaseSpirituality;
                ret[1] = (int)pet.spirituality;
                break;
            case 2:
                ret[0] = (int)pet.BaseVitality;
                ret[1] = (int)pet.vitality;
                break;
            case 3:
                ret[0] = (int)pet.BaseGengu;
                ret[1] = (int)pet.gengu;
                break;
            //[@]身法已移除，后面来删掉
            case 4:
                ret[0] = 0;
                ret[1] = 0;
                break;
            default:
                break;
        }
        return ret;
    }
    private void OnUpdateItems(PetPunctuateItem curItem, int ret)
    {
        foreach (var item in _punctuateItems)
        {
            if (curItem == item) continue;
            item.SetRange(item.ChangeValue + ret);
        }
    }
    private void OnUpdateAdds(int index)
    {
        switch (index)
        {
            case 0:
                OnUpdateAdd(1);
                OnUpdateAdd(3);
                break;
            case 1:
                OnUpdateAdd(2);
                OnUpdateAdd(4);
                break;
            case 2:
                OnUpdateAdd(0);
                OnUpdateAdd(4);
                break;
            case 3:
                OnUpdateAdd(3);
                OnUpdateAdd(4);
                break;
            case 4:
                OnUpdateAdd(5);
                break;
            default:
                break;
        }
    }
    private void OnUpdateAdd(int index)
    {
        if (pet == null) return;
        UILabel label = _adds[index];
        int oldValue = 0;
        int newValue = 0;
        switch (index)
        {
            case 0:
                oldValue = pet.Hp;
                newValue = PetRule.CalcHpMax(pet.level, _punctuateItems[2].CurValue, pet.hpFactor, pet.growthFactor);
                break;
            case 1:
                oldValue = pet.PhyAttack;
                newValue = PetRule.CalcPhysicsPower(pet.level, _punctuateItems[0].CurValue, pet.physicsPowerFactor, pet.growthFactor);
                break;
            case 2:
                oldValue = pet.MagAttack;
                newValue = PetRule.CalcMagicPower(pet.level, _punctuateItems[1].CurValue, pet.magicPowerFactor, pet.growthFactor);
                break;
            case 3:
                oldValue = pet.PhyDefense;
                newValue = PetRule.CalcPhysicsDefense(pet.level, _punctuateItems[0].CurValue, _punctuateItems[3].CurValue, pet.physicsDefenseFactor, pet.growthFactor);
                break;
            case 4:
                oldValue = pet.MagDefense;
                newValue = PetRule.CalcMagicDefense(pet.level, _punctuateItems[2].CurValue, _punctuateItems[1].CurValue, _punctuateItems[3].CurValue, pet.magicDefenseFactor, pet.growthFactor);
                break;
            case 5:
                oldValue = pet.Speed;
                newValue = PetRule.CalcAttackSpeed(pet.utype, pet.type);
                break;
            default:
                break;
        }
        if (newValue > oldValue)
        {
            label.transform.parent.gameObject.SetActive(true);
            label.text = string.Format("+{0}", newValue - oldValue);
        }
        else
        {
            label.transform.parent.gameObject.SetActive(false);
        }
    }

    private int GetRemain()
    {
        if (pet == null) return 0;
        int sum = 0;
        foreach (var item in _punctuateItems)
        {
            sum += item.ChangeValue;
        }
        int ret = pet.freePoint - sum;
        _remain.text = string.Format("{0}", ret);
        return ret;
    }

    private void UpdateAttribute()
    {
        if (pet == null) return;
        _name.text = pet.Name;

        _values[0].text = string.Format("{0}", pet.Hp);
        _values[1].text = string.Format("{0}", pet.PhyAttack);
        _values[2].text = string.Format("{0}", pet.MagAttack);
        _values[3].text = string.Format("{0}", pet.PhyDefense);
        _values[4].text = string.Format("{0}", pet.MagDefense);
        _values[5].text = string.Format("{0}", pet.Speed);
        for (int i = 0; i < Count; i++)
        {
            OnUpdateAdd(i);
        }
    }

    private void UpdatePet(PetInfo o)
    {
        if (!Active) return;
        if (o == null) return;
        UpdateAttribute();
        UpdateItems();
        GetRemain();
    }

}
