﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物栏位Item
/// </summary>
public class PetFenceItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }
    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value;
        }
    }
    private UISprite _mark;
    public bool Mark
    {
        set
        {
            _mark.enabled = value;
        }
    }

    private UILabel _addText;
    public string AddText
    {
        set
        {
            _addText.text = value;
        }
    }
    private UILabel _lockText;
    public string LockText
    {
        set
        {
            _lockText.text = value;
        }
    }

    private const int Num = 3;
    private GameObject[] _states = new GameObject[Num];
    public void SetState(int index)
    {
        for (int i = 0; i < Num; i++)
        {
            _states[i].SetActive(i == index);
        }
    }

    protected override void OnInit()
    {
        Transform state0 = transform.Find("State/0");
        _states[0] = state0.gameObject;
        _name = state0.Find("Name").GetComponent<UILabel>();
        _icon = state0.Find("Icon").GetComponent<UISprite>();
        _level = state0.Find("Level").GetComponent<UILabel>();
        _mark = state0.Find("Mark").GetComponent<UISprite>();
        _mark.enabled = false;

        Transform state1 = transform.Find("State/1");
        _states[1] = state1.gameObject;
        _addText = state1.Find("Text").GetComponent<UILabel>();

        Transform state2 = transform.Find("State/2");
        _states[2] = state2.gameObject;
        _lockText = state2.Find("Text").GetComponent<UILabel>();
    }
}
