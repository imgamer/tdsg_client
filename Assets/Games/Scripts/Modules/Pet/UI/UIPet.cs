﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物基础界面
/// </summary>
public class UIPet : UIWin
{
    protected override void OnInit()
    {
        InitItem();
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        if(args != null && args.Length == 1)
        {
            int index = (int)args[0];
            _uiTab.SwitchTo(index);
        }
        else
        {
            _uiTab.SwitchTo(0);
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    public static WinPage FencePage { get; private set; }
    public static WinPage ExhibitPage { get; private set; }
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }

    private WinPage _curContent;
    private UI.Tab _uiTab;
    private void InitTab()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        ExhibitPage = center.Find("Exhibit").GetComponent<WinPage>();
        FencePage = center.Find("Fence").GetComponent<WinPage>();
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            _curContent.Open(this);
        };
    }
}
