﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
/// <summary>
/// 宠物栏位显示
/// </summary>
public class PetFencePage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdateItems);
        EventManager.AddListener<object>(EventID.EVNT_PET_NUM_UPDATE, UpdateItems);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateItems);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdateItems);
        EventManager.RemoveListener<object>(EventID.EVNT_PET_NUM_UPDATE, UpdateItems);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateItems);
    }

    private const int Num = 8;
    private Grid _uiGrid;
    private Transform _select;
    private PetFenceItem[] items = new PetFenceItem[Num];
    private void InitItem()
    {
        Transform trans = transform.Find("Pet");
        _select = transform.Find("ScrollView/Select");
        Transform grid = transform.Find("ScrollView/Grid");
        _uiGrid = grid.GetComponent<Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString("D2");
            PetFenceItem item = child.GetComponent<PetFenceItem>();
            item.Init(ParentUI);
            items[i] = item;
        }
        _uiGrid.Reposition();
    }
    private void UpdateItems()
    {
        List<PetInfo> list = GameMain.Player.petModule.GetSortPets();
        int num = list.Count;
        int maxNum = PetRule.GetMaxPetNumByLevel(GameMain.Player.Level);
        for (int i = 0; i < Num; i++)
        {
            PetFenceItem item = items[i];
            if (i < num)
            {
                PetInfo data = list[i];
                item.SetState(0);
                item.Icon = "Pet_002";
                item.Name = data.Name;
                PetsData petData = PetsInitialConfig.SharedInstance.GetPetsData(data.utype);
                if (petData.takeLevel > GameMain.Player.Level)
                {
                    item.Level = string.Format("{0}级出战", petData.takeLevel);
                }
                else
                {
                    item.Level = string.Format("{0}级", data.level);
                }
                item.Mark = GameMain.Player.FightingPetDBID == data.dbid;
                if (i == GameMain.Player.petModule.GetSelectedIndex())
                {
                    SelectItem(item.transform, data);
                    _uiGrid.GoToItem(i, Num);
                }

                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    SelectItem(go.transform, data);
                });
            }
            else if(i < maxNum)
            {
                item.SetState(1);
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    UIManager.instance.OpenWindow(null, WinID.UIPet, 3);
                });
            }
            else
            {
                item.SetState(2);
                //临时
                item.LockText = string.Format("{0}级解锁", 50 + (i - maxNum) * 10);
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
    }
    private void SelectItem(Transform item, PetInfo data)
    {
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
        GameMain.Player.petModule.SelectPet(data);
    }

    private void UpdateItems(object o)
    {
        if (!Active) return;
        UpdateItems();
    }

    private void UpdateItems(PetInfo o)
    {
        if (!Active) return;
        UpdateItems();
    }
}
