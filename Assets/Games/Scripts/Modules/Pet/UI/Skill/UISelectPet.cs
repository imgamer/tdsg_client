﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 选择宠物界面
/// </summary>
public class UISelectPet : UIWin
{
    public enum Type
    {
        Left,
        Right,
    }
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_COMBINEPET_UPDATE, SelectDone);
        InitItem();
        InitDescriptions();
        InitSkill();
        InitPets();
    }

    protected override void OnOpen(params object[] args)
    {
        if(args == null || args.Length != 2) return;
        Type type = (Type)(args[0]);
        _oldPetData = (PetInfo)(args[1]);
        UpdateAll(type);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_COMBINEPET_UPDATE, SelectDone);
    }

    #region 初始化
    private PetInfo _oldPetData = null;
    private PetInfo _curPetData = null;
    private Transform _root;
    private Vector2 _rootInitPos;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _root = center.Find("Root");
        _rootInitPos = _root.localPosition;
        GameObject btnSelect = _root.Find("BtnSelect").gameObject;
        InputManager.instance.AddClickListener(btnSelect, (go) =>
        {
            GameMain.Player.petModule.SetCombinePet(_oldPetData, _curPetData);
        });
        GameObject btnClose = _root.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }

    private const int Count = 6;
    private UIProgressBar[] _bars = new UIProgressBar[Count];
    private UILabel[] _descriptions = new UILabel[Count];
    private UILabel _name;
    private UILabel _level;
    private UILabel _aptitude;
    private void InitDescriptions()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform trans = center.Find("Root/Description");
        _aptitude = trans.Find("Aptitude/Value").GetComponent<UILabel>();
        _name = trans.Find("Name").GetComponent<UILabel>();
        _level = trans.Find("Level").GetComponent<UILabel>();
        Transform grid = trans.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            Transform item = grid.Find(i.ToString());
            _bars[i] = item.Find("Bar").GetComponent<UIProgressBar>();
            _descriptions[i] = item.Find("Bar/Value").GetComponent<UILabel>();
        }
    }
    private Transform _skillSelect;
    private const int SkillNum = 8;
    private UI.Grid _skillUIGrid;
    private PetSkillItem[] _skillItems = new PetSkillItem[SkillNum];
    private void InitSkill()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform trans = center.Find("Root/Skills");
        _skillSelect = trans.Find("Select");
        Transform grid = trans.Find("UIGrid");
        _skillUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, SkillNum);
        for (int i = 0; i < SkillNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(this);
            _skillItems[i] = item;
        }
        _skillUIGrid.Reposition();
    }
    private Transform _petSelect;
    private const int PetNum = 8;
    private UI.Grid _petUIGrid;
    private PetItem[] _petItems = new PetItem[PetNum];
    private void InitPets()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform trans = center.Find("Root/Pets");
        _petSelect = trans.Find("Select");
        Transform grid = trans.Find("UIGrid");
        _petUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, PetNum);
        for (int i = 0; i < PetNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetItem item = child.GetComponent<PetItem>();
            item.Init(this);
            _petItems[i] = item;
        }
        _petUIGrid.Reposition();
    }
    #endregion

    #region 更新
    private void UpdateAll(Type type)
    {
        switch (type)
        {
            case Type.Left:
                _root.localPosition = new Vector2(_rootInitPos.x - 250, _rootInitPos.y);
                break;
            case Type.Right:
                _root.localPosition = new Vector2(_rootInitPos.x + 210, _rootInitPos.y);
                break;
            default:
                break;
        }
        UpdateSkillItems(null);
        UpdateDescription(null);
        UpdatePetItems();
    }
    private void UpdateDescription(PetInfo pet)
    {
        if (pet == null)
        {
            _name.text = "请选择宠物";
            _level.text = "";
            _aptitude.text = "0";
            for (int i = 0; i < Count; i++)
            {
                _descriptions[i].text = "";
                _bars[i].value = 0;
            }
        }
        else
        {
            _name.text = pet.Name;
            _level.text = string.Format("{0}级", pet.level);
            PetsFactors petsFactors = PetsFactorsConfig.SharedInstance.GetPetFactorsData(pet.utype, pet.type);
            if (petsFactors == null) return;
            PetStageEnhanceData curEnhance = PetStageEnhanceConfig.SharedInstance.GetEnhanceByStage(pet.stage);
            if (curEnhance == null) return;

            _descriptions[0].text = string.Format("{0}", pet.physicsPowerFactor);
            _bars[0].value = pet.physicsPowerFactor / GetUpperLimit(petsFactors.physicsPowerFactor[1], curEnhance.physicsPowerFactorUpperLimit);

            _descriptions[1].text = string.Format("{0}", pet.physicsDefenseFactor);
            _bars[1].value = pet.physicsDefenseFactor / GetUpperLimit(petsFactors.physicsDefenseFactor[1], curEnhance.physicsDefenseFactorUpperLimit);

            _descriptions[2].text = string.Format("{0}", pet.magicPowerFactor);
            _bars[2].value = pet.magicPowerFactor / GetUpperLimit(petsFactors.magicPowerFactor[1], curEnhance.magicPowerFactorUpperLimit);

            _descriptions[3].text = string.Format("{0}", pet.magicDefenseFactor);
            _bars[3].value = pet.magicDefenseFactor / GetUpperLimit(petsFactors.magicDefenseFactor[1], curEnhance.magicDefenseFactorUpperLimit);

            _descriptions[4].text = string.Format("{0}", pet.hpFactor);
            _bars[4].value = pet.hpFactor / GetUpperLimit(petsFactors.hpFactor[1], curEnhance.hpFactorUpperLimit);

            _descriptions[5].text = string.Format("{0}", "null");
            _bars[5].value = 0;

            _aptitude.text = string.Format("{0:N3}", pet.growthFactor);
        }
    }

    private float GetUpperLimit(int value, double factor)
    {
        return (float)(value * (1 + factor));
    }

    private void UpdateSkillItems(PetInfo pet)
    {
        _skillSelect.gameObject.SetActive(false);
        if (pet == null)
        {
            for (int i = 0; i < SkillNum; i++)
            {
                PetSkillItem item = _skillItems[i];
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        else
        {
            for (int i = 0; i < SkillNum; i++)
            {
                PetSkillItem item = _skillItems[i];
                if (i < pet.skills.Count)
                {
                    Skill.SkillBase data = SkillManager.instance.GetSkill(pet.skills[i]);
                    item.Icon = data.icon;
                    item.Mark = pet.bindedSkills.Contains(data.id);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SelectItem(_skillSelect, go.transform);
                    });
                }
                else
                {
                    item.Icon = string.Empty;
                    item.Mark = false;
                    InputManager.instance.RemoveClickListener(item.gameObject);
                }
            }
        }
        _skillUIGrid.GoToItem(0, SkillNum);
    }
    private void SelectItem(Transform select, Transform item)
    {
        select.parent = item;
        select.localPosition = Vector3.zero;
        select.localScale = Vector3.one;
        _skillSelect.gameObject.SetActive(true);
    }
    private void UpdatePetItems()
    {
        _petSelect.gameObject.SetActive(false);
        List<PetInfo> list = GameMain.Player.petModule.GetCombinePets();
        int num = list.Count;
        for (int i = 0; i < PetNum; i++)
        {
            PetItem item = _petItems[i];
            if (i < num)
            {
                PetInfo data = list[i];
                item.Icon = "Pet_002";
                item.Level = data.level;
                item.State = data.dbid == GameMain.Player.FightingPetDBID;
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    _petSelect.gameObject.SetActive(true);
                    SelectItem(_petSelect, go.transform);
                    _curPetData = data;
                    UpdateSkillItems(data);
                    UpdateDescription(data);
                });
            }
            else
            {
                item.Icon = "";
                item.Level = 0;
                item.State = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        _petUIGrid.GoToItem(0, PetNum);
    }
    #endregion


    private void SelectDone(PetInfo o)
    {
        if (!Active) return;
        _oldPetData = null;
        _curPetData = null;
        UIManager.instance.CloseWindow(winID);
    }
}
