﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 合宠预览界面
/// </summary>
public class UICombinePet : UIWin
{

    protected override void OnInit()
    {
        InitItem();
        InitDescriptions();
        InitSkill();
    }

    protected override void OnOpen(params object[] args)
    {
        if(args == null || args.Length != 2) return;
        PetInfo leftPet = (PetInfo)args[0];
        PetInfo rightPet = (PetInfo)args[1];
        UpdateAll(leftPet, rightPet);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    #region 初始化
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnCombine = center.Find("BtnCombine").gameObject;
        InputManager.instance.AddClickListener(btnCombine, (go) =>
        {
            GameMain.Player.petModule.PetCombine();
        });
        GameObject btnCancel = center.Find("BtnCancel").gameObject;
        InputManager.instance.AddClickListener(btnCancel, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }
    private const int Count = 7;
    private UILabel[] _descriptions = new UILabel[Count];
    private UILabel _level;
    private UISprite[] _icons = new UISprite[2];
    private UILabel[] _names = new UILabel[2];
    private void InitDescriptions()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform icon = center.Find("Icon");
        _level = icon.Find("Level/Value").GetComponent<UILabel>();
        _icons[0] = icon.Find("Left/Icon").GetComponent<UISprite>();
        _names[0] = icon.Find("Left/Name").GetComponent<UILabel>();

        _icons[1] = icon.Find("Right/Icon").GetComponent<UISprite>();
        _names[1] = icon.Find("Right/Name").GetComponent<UILabel>();

        Transform description = center.Find("Description");
        Transform grid = description.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            Transform item = grid.Find(i.ToString());
            _descriptions[i] = item.Find("Value").GetComponent<UILabel>();
        }
    }
    private Transform _select;
    private const int Num = 8;
    private UI.Grid _uIGrid;
    private PetSkillItem[] _skillItems = new PetSkillItem[Num];
    private void InitSkill()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _select = center.Find("ScrollView/Select");
        Transform grid = center.Find("ScrollView/UIGrid");
        _uIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(this);
            _skillItems[i] = item;
        }
        _uIGrid.Reposition();
    }
    #endregion

    #region 更新
    private void UpdateAll(PetInfo leftPet, PetInfo rightPet)
    {
        UpdateItems(_skillItems, leftPet, rightPet);
        UpdateAptitude(leftPet, rightPet);
    }
    private void UpdateItems(PetSkillItem[] items, PetInfo leftPet, PetInfo rightPet)
    {
        _select.gameObject.SetActive(false);
        List<int> skills = new List<int>();
        skills.AddRange(leftPet.skills);
        foreach (var id in rightPet.skills)
        {
            if (skills.Contains(id)) continue;
            skills.Add(id);
        }
        for (int i = 0; i < items.Length; i++)
        {
            PetSkillItem item = items[i];
            if (i < skills.Count)
            {
                Skill.SkillBase data = SkillManager.instance.GetSkill(skills[i]);
                item.Icon = data.icon;
                item.Mark = false;
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    SelectItem(_select, go.transform);
                });
            }
            else
            {
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
    }
    private void SelectItem(Transform select, Transform item)
    {
        select.parent = item;
        select.localPosition = Vector3.zero;
        select.localScale = Vector3.one;
        select.gameObject.SetActive(true);
    }
    private void UpdateAptitude(PetInfo leftPet, PetInfo rightPet)
    {
        int[] physicsPowerFactor = GetRange(leftPet.physicsPowerFactor, rightPet.physicsPowerFactor);
        _descriptions[0].text = string.Format("{0}-{1}", physicsPowerFactor[0], physicsPowerFactor[1]);

        int[] physicsDefenseFactor = GetRange(leftPet.physicsDefenseFactor, rightPet.physicsDefenseFactor);
        _descriptions[1].text = string.Format("{0}-{1}", physicsDefenseFactor[0], physicsDefenseFactor[1]);

        int[] magicPowerFactor = GetRange(leftPet.magicPowerFactor, rightPet.magicPowerFactor);
        _descriptions[2].text = string.Format("{0}-{1}", magicPowerFactor[0], magicPowerFactor[1]);

        int[] magicDefenseFactor = GetRange(leftPet.magicDefenseFactor, rightPet.magicDefenseFactor);
        _descriptions[3].text = string.Format("{0}-{1}", magicDefenseFactor[0], magicDefenseFactor[1]);

        int[] hpFactor = GetRange(leftPet.hpFactor, rightPet.hpFactor);
        _descriptions[4].text = string.Format("{0}-{1}", hpFactor[0], hpFactor[1]);

        _descriptions[5].text = string.Format("{0}-{1}", "null", "null");

        float[] growthFactor = GetRange(leftPet.growthFactor, rightPet.growthFactor);
        _descriptions[6].text = string.Format("{0:N3}-{1:N3}", growthFactor[0], growthFactor[1]);

        if(leftPet.level == rightPet.level)
        {
            _level.text = string.Format("{0}", leftPet.level);
        }
        else
        {
            int min = Mathf.Min(leftPet.level, rightPet.level);
            int max = Mathf.Max(leftPet.level, rightPet.level);
            _level.text = string.Format("{0}-{1}", min, max);
        }
        //_icons[0].spriteName = "";
        _names[0].text = leftPet.Name;
        //_icons[1].spriteName = "";
        _names[1].text = rightPet.Name;
    }
    private int[] GetRange(int a, int b)
    {
        int[] range = new int[2];
        float average = (a + b) * 0.5f;
        range[0] = (int)(average * 0.8f);
        range[1] = (int)(average * 1.2f);
        return range;
    }

    private float[] GetRange(float a, float b)
    {
        float[] range = new float[2];
        float average = (a + b) * 0.5f;
        range[0] = average * 0.8f;
        range[1] = average * 1.2f;
        return range;
    }
    #endregion

}
