﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物合宠、学习界面
/// </summary>
public class PetSkillPage : WinPage
{
    protected override void OnInit()
    {
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private UI.Tab _uiTab;
    private WinPage _curContent;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            _curContent.Open(ParentUI);
        };
    }
}
