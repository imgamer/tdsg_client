﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
/// <summary>
/// 宠物技能学习界面
/// </summary>
public class PetLearnPage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateSkill);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateSkill);
        InitItem();
        InitSkill();
        InitDisplay();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Close();
        UIPet.ExhibitPage.Open(ParentUI);
        ResetItem();
        _fencePage.Open(ParentUI);
        UpdateSkill();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, UpdateSkill);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdateSkill);
    }

    #region 宠物栏位和技能学习
    private UISprite _icon;
    private UISprite _add;
    private WinPage _fencePage;
    private void InitItem()
    {
        _fencePage = transform.Find("Fence").GetComponent<WinPage>();
        Transform item = transform.Find("Item");
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SetState(2);
        });
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _add = item.Find("Add").GetComponent<UISprite>();
        GameObject btnUse = transform.Find("BtnUse").gameObject;
        InputManager.instance.AddClickListener(btnUse, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (!CanLearn(pet, 1)) return;
            GameMain.Player.petModule.RequestToLearnSkill(pet.dbid, 1);
        });
    }
    private bool CanLearn(PetInfo pet, int skillBookID)
    {
        if (pet == null) return false;
        return true;
    }

    private void ResetItem()
    {
        _icon.enabled = false;
    }
    #endregion

    #region 宠物技能
    private Transform _select;
    private const int Num = 8;
    private Grid _uiGrid;
    private PetSkillItem[] _items = new PetSkillItem[Num];
    private void InitSkill()
    {
        _select = transform.Find("SkillList/Select");
        Transform grid = transform.Find("SkillList/UIGrid");
        _uiGrid = grid.GetComponent<Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            _items[i] = item;
        }
        _uiGrid.Reposition();
    }
    private void UpdateSkill()
    {
        SetState(0);
        _select.gameObject.SetActive(false);
        PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
        if (pet == null) return;
        for (int i = 0; i < Num; i++)
        {
            PetSkillItem item = _items[i];
            item.Init(ParentUI);
            if (i < pet.skills.Count)
            {
                Skill.SkillBase data = SkillManager.instance.GetSkill(pet.skills[i]);
                item.Icon = data.icon;
                item.Mark = pet.bindedSkills.Contains(data.id);
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    SelectItem(go.transform, data);
                });
            }
            else
            {
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
    }
    private Skill.SkillBase _curSkill = null;
    private void SelectItem(Transform item, Skill.SkillBase data)
    {
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
        _select.gameObject.SetActive(true);

        _curSkill = data;
        SetState(1);
    }

    private void UpdateSkill(PetInfo o)
    {
        if (!Active) return;
        UpdateSkill();
    }
    #endregion

    private const int StateNum = 3;
    private GameObject[] _states = new GameObject[StateNum];

    public class SkillDetail
    {
        public UISprite icon;
        public UILabel name;
        public UILabel type;
        public UILabel desc;
        public GameObject mark;
    }
    private SkillDetail _skillDetail;
    private WinPage _cheatsPage;
    private void InitDisplay()
    {
        Transform root = transform.Find("Display");
        for (int i = 0; i < StateNum; i++)
        {
            Transform item = root.Find(i.ToString());
            _states[i] = item.gameObject;
        }
        _skillDetail = new SkillDetail();
        Transform trans = root.Find("1/Item");
        _skillDetail.icon = trans.Find("Icon").GetComponent<UISprite>();
        _skillDetail.name = trans.Find("Name").GetComponent<UILabel>();
        _skillDetail.type = trans.Find("Type").GetComponent<UILabel>();
        _skillDetail.desc = trans.Find("Desc").GetComponent<UILabel>();
        _skillDetail.mark = trans.Find("Mark").gameObject;
        _skillDetail.mark.SetActive(false);
        GameObject btnForget = root.Find("1/BtnForget").gameObject;
        InputManager.instance.AddClickListener(btnForget, (go) => 
        {

        });
        GameObject btnLock = root.Find("1/BtnLock").gameObject;
        InputManager.instance.AddClickListener(btnLock, (go) =>
        {
            PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
            if (!CanLockSkill(pet, _curSkill)) return;
            GameMain.Player.petModule.RequestToBindSkill(pet.dbid, _curSkill.id);
        });
        _cheatsPage = root.Find("2").GetComponent<WinPage>();
    }

    //是否能锁定技能
    private bool CanLockSkill(PetInfo pet, Skill.SkillBase skill)
    {
        if (pet == null || skill == null) return false;
        if (!PetRule.CanApproveSkill(pet, skill.id, TipManager.instance.ShowTextTip))
        {
            return false;
        }
        //消耗相关
        return true;
    }

    private void SetState(int index)
    {
        for (int i = 0; i < StateNum; i++)
        {
            _states[i].SetActive(i == index);
        }
        switch (index)
        {
            case 0:
                _curSkill = null;
                break;
            case 1:
                PetInfo pet = GameMain.Player.petModule.GetSelectedPet();
                if (pet == null) return;
                _skillDetail.icon.spriteName = _curSkill.icon;
                _skillDetail.name.text = _curSkill.name;
                _skillDetail.mark.SetActive(pet.bindedSkills.Contains(_curSkill.id));
                //_skillDetail.type.text = "";
                _skillDetail.desc.text = _curSkill.description;
                break;
            case 2:
                _cheatsPage.Open(ParentUI);
                break;
            default:
                break;
        }
    }
}
