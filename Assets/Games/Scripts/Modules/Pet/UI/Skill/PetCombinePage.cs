﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using DBID_TYPE = System.UInt64;
/// <summary>
/// 宠物练宠——合宠界面
/// </summary>
public class PetCombinePage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_COMBINEPET_UPDATE, SelectDone);
        EventManager.AddListener<DBID_TYPE>(EventID.EVNT_PET_COMBINEPET_SUCCESS, CombineSuccess);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UIPet.FencePage.Close();
        UIPet.ExhibitPage.Close();
        GameMain.Player.petModule.ResetCombinePets();
        UpdateLeft(null);
        UpdateRight(null);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_COMBINEPET_UPDATE, SelectDone);
        EventManager.RemoveListener<DBID_TYPE>(EventID.EVNT_PET_COMBINEPET_SUCCESS, CombineSuccess);
    }

    #region 初始化
    private bool _isLeft = true;
    private void InitItem()
    {
        InitLeft();
        InitRight();
        GameObject btnCombine = transform.Find("BtnCombine").gameObject;
        InputManager.instance.AddClickListener(btnCombine, (go) => 
        {
            GameMain.Player.petModule.PetCombine();
        });
        GameObject btnPreview = transform.Find("BtnPreview").gameObject;
        InputManager.instance.AddClickListener(btnPreview, (go) =>
        {
            if (_leftPetData == null || _rightPetData == null)
            {
                return;
            }
            UIManager.instance.OpenWindow(null, WinID.UICombinePet, _leftPetData, _rightPetData);
        });
        GameObject btnInfo = transform.Find("BtnInfo").gameObject;
        InputManager.instance.AddClickListener(btnInfo, (go) =>
        {

        });
    }
    public class PetIcon
    {
        public GameObject addIcon;
        public GameObject iconRoot;
        public UISprite icon;
        public UILabel name;
        public UILabel level;
    }
    private PetIcon InitPetIcon(Transform petItem)
    {
        PetIcon petIcon = new PetIcon();
        petIcon.addIcon = petItem.Find("Add").gameObject;
        petIcon.iconRoot = petItem.Find("Icon").gameObject;
        petIcon.icon = petItem.Find("Icon/Icon").GetComponent<UISprite>();
        petIcon.name = petItem.Find("Icon/Name").GetComponent<UILabel>();
        petIcon.level = petItem.Find("Icon/Level").GetComponent<UILabel>();
        return petIcon;
    }
    private PetInfo _leftPetData = null;
    private PetIcon _leftIcon;
    private const int Count = 7;
    private UILabel[] _leftDescriptions = new UILabel[Count];
    private void InitLeft()
    {
        Transform petItem = transform.Find("Left/Item");
        _leftIcon = InitPetIcon(petItem);
        InputManager.instance.AddClickListener(petItem.gameObject, (go) => 
        {
            _isLeft = true;
            UIManager.instance.OpenWindow(null, WinID.UISelectPet, UISelectPet.Type.Left, _leftPetData);
        });

        Transform description = transform.Find("Left/Description");
        Transform grid = description.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            Transform item = grid.Find(i.ToString());
            _leftDescriptions[i] = item.Find("Value").GetComponent<UILabel>();
        }
        InitLeftSkill();
    }
    private Transform _leftSelect;
    private const int Num = 8;
    private UI.Grid _leftUIGrid;
    private PetSkillItem[] _leftItems = new PetSkillItem[Num];
    private void InitLeftSkill()
    {
        _leftSelect = transform.Find("Left/ScrollView/Select");
        Transform grid = transform.Find("Left/ScrollView/UIGrid");
        _leftUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(ParentUI);
            _leftItems[i] = item;
        }
        _leftUIGrid.Reposition();
    }
    private PetInfo _rightPetData = null;
    private PetIcon _rightIcon;
    private UILabel[] _rightDescriptions = new UILabel[Count];
    private void InitRight()
    {
        Transform petItem = transform.Find("Right/Item");
        _rightIcon = InitPetIcon(petItem);
        InputManager.instance.AddClickListener(petItem.gameObject, (go) =>
        {
            _isLeft = false;
            UIManager.instance.OpenWindow(null, WinID.UISelectPet, UISelectPet.Type.Right, _rightPetData);
        });
        Transform description = transform.Find("Right/Description");
        Transform grid = description.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            Transform item = grid.Find(i.ToString());
            _rightDescriptions[i] = item.Find("Value").GetComponent<UILabel>();
        }
        InitRightSkill();
    }
    private Transform _rightSelect;
    private UI.Grid _rightUIGrid;
    private PetSkillItem[] _rightItems = new PetSkillItem[Num];
    private void InitRightSkill()
    {
        _rightSelect = transform.Find("Right/ScrollView/Select");
        Transform grid = transform.Find("Right/ScrollView/UIGrid");
        _rightUIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(ParentUI);
            _rightItems[i] = item;
        }
        _rightUIGrid.Reposition();
    }
    #endregion

    #region 更新
    private void UpdateLeft(PetInfo petData)
    {
        _leftPetData = petData;
        UpdateItems(_leftItems, _leftPetData, _leftIcon, _leftSelect);
        UpdateAptitude(_leftDescriptions, _leftPetData);
    }

    private void UpdateRight(PetInfo petData)
    {
        _rightPetData = petData;
        UpdateItems(_rightItems, _rightPetData, _rightIcon, _rightSelect);
        UpdateAptitude(_rightDescriptions, _rightPetData);
    }

    private void UpdateItems(PetSkillItem[] items, PetInfo pet, PetIcon petIcon, Transform select)
    {
        select.gameObject.SetActive(false);
        if (pet == null)
        {
            petIcon.addIcon.SetActive(true);
            petIcon.iconRoot.SetActive(false);

            for (int i = 0; i < items.Length; i++)
            {
                PetSkillItem item = items[i];
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        else
        {
            petIcon.addIcon.SetActive(false);
            petIcon.iconRoot.SetActive(true);
            //petIcon.icon.spriteName = "";
            petIcon.name.text = pet.Name;
            petIcon.level.text = string.Format("{0}", pet.level);

            for (int i = 0; i < items.Length; i++)
            {
                PetSkillItem item = items[i];
                item.Init(ParentUI);
                if (i < pet.skills.Count)
                {
                    Skill.SkillBase data = SkillManager.instance.GetSkill(pet.skills[i]);
                    item.Icon = data.icon;
                    item.Mark = pet.bindedSkills.Contains(data.id);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SelectItem(select, go.transform);
                    });
                }
                else
                {
                    item.Icon = string.Empty;
                    item.Mark = false;
                    InputManager.instance.RemoveClickListener(item.gameObject);
                }
            }
        }
    }
    private void SelectItem(Transform select, Transform item)
    {
        select.parent = item;
        select.localPosition = Vector3.zero;
        select.localScale = Vector3.one;
        select.gameObject.SetActive(true);
    }
    private void UpdateAptitude(UILabel[] descriptions, PetInfo pet)
    {
        if (pet == null)
        {
            foreach (var item in descriptions)
            {
                item.text = "0";
            }
        }
        else
        {
            descriptions[0].text = string.Format("{0}", pet.physicsPowerFactor);
            descriptions[1].text = string.Format("{0}", pet.physicsDefenseFactor);
            descriptions[2].text = string.Format("{0}", pet.magicPowerFactor);
            descriptions[3].text = string.Format("{0}", pet.magicDefenseFactor);
            descriptions[4].text = string.Format("{0}", pet.hpFactor);
            descriptions[5].text = string.Format("{0}", 0);
            descriptions[6].text = string.Format("{0:N3}", pet.growthFactor);
        }
    }
    #endregion

    private void SelectDone(PetInfo petData)
    {
        if (!Active) return;
        if (_isLeft)
        {
            UpdateLeft(petData);
        }
        else
        {
            UpdateRight(petData);
        }
    }

    private void CombineSuccess(DBID_TYPE dbid)
    {
        if (!Active) return;
        GameMain.Player.petModule.ResetCombinePets();
        UpdateLeft(null);
        UpdateRight(null);
        UIManager.instance.OpenWindow(null, WinID.UICombinePetResult, dbid);
    }
}
