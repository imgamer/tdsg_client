﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
/// <summary>
/// 宠物技能秘籍选择界面
/// </summary>
public class PetCheatsPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private PetPropItem _template;
    private Transform _select;
    private List<PetPropItem> _items = new List<PetPropItem>();
    private Grid _uiGrid;
    private void InitItem()
    {
        _select = transform.Find("ScrollView/Select");
        Transform grid = transform.Find("ScrollView/Grid");
        _uiGrid = grid.GetComponent<Grid>();
        _template = grid.Find("Item").GetComponent<PetPropItem>();
        _template.gameObject.SetActive(false);
    }

    private void UpdateItems()
    {
        SelectItem(null);
    }

    private void SelectItem(Transform item)
    {
        if (item == null)
        {
            _select.gameObject.SetActive(false);
            return;
        }
        _select.gameObject.SetActive(true);
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
    }
}
