﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物图标Item
/// </summary>
public class PetItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                _icon.spriteName = value;
            }
        }
    }

    private UISprite _state;
    public bool State
    {
        set
        {
            _state.enabled = value;
        }
    }
    private UILabel _level;
    public int Level
    {
        set
        {
            if (value <= 0)
            {
                _level.text = "";
            }
            else
            {
                _level.text = string.Format("Lv.{0}", value);
            }
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _state = transform.Find("State").GetComponent<UISprite>();
        _state.enabled = false;
        _level = transform.Find("Level").GetComponent<UILabel>();
    }
}
