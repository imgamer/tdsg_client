﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物技能Item
/// </summary>
public class PetSkillItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                _icon.spriteName = value;
            }
        }
    }
    private GameObject _mark;
    public bool Mark
    {
        set 
        {
            _mark.SetActive(value);
        }
    }
    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _mark = transform.Find("Mark").gameObject;
    }
}
