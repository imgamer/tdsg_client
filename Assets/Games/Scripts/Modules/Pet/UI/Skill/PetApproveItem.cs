﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 宠物技能认证Item
/// </summary>
public class PetApproveItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                _icon.spriteName = value;
            }
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private UILabel _type;
    public string Type
    {
        set
        {
            _type.text = value;
        }
    }
    protected override void OnInit()
    {
        _icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _type = transform.Find("Type").GetComponent<UILabel>();
    }
}
