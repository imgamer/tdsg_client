﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using DBID_TYPE = System.UInt64;
/// <summary>
/// 合宠预览界面
/// </summary>
public class UICombinePetResult : UIWin
{

    protected override void OnInit()
    {
        InitPage();
        InitDescriptions();
        InitSkill();
    }

    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        DBID_TYPE dbid = (DBID_TYPE)args[0];
        PetInfo pet = GameMain.Player.petModule.GetPetInfoByDBID(dbid);
        if (pet == null) return;
        SetEntity(pet);
        UpdateItems(_skillItems, pet);
        UpdateAptitude(pet);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    #region 初始化
    private Transform _entityRoot;
    private UILabel _name;
    private UILabel _level;
    private UILabel _score;

    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject btnClose2 = center.Find("BtnClose2").gameObject;
        InputManager.instance.AddClickListener(btnClose2, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        Transform info = center.Find("Info");
        _name = info.Find("Name/Text").GetComponent<UILabel>();
        _level = info.Find("Level").GetComponent<UILabel>();
        _score = info.Find("Score/Value").GetComponent<UILabel>();
        _entityRoot = center.Find("Info/EntityRoot");
    }

    private const int Count = 7;
    private UILabel[] _descriptions = new UILabel[Count];
    private void InitDescriptions()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform description = center.Find("Description");
        Transform grid = description.Find("UIGrid");
        for (int i = 0; i < Count; i++)
        {
            Transform item = grid.Find(i.ToString());
            _descriptions[i] = item.Find("Value").GetComponent<UILabel>();
        }
    }
    private Transform _select;
    private const int Num = 8;
    private UI.Grid _uIGrid;
    private PetSkillItem[] _skillItems = new PetSkillItem[Num];
    private void InitSkill()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _select = center.Find("ScrollView/Select");
        Transform grid = center.Find("ScrollView/UIGrid");
        _uIGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            PetSkillItem item = child.GetComponent<PetSkillItem>();
            item.Init(this);
            _skillItems[i] = item;
        }
        _uIGrid.Reposition();
    }
    #endregion

    #region 更新
    private void UpdateItems(PetSkillItem[] items, PetInfo pet)
    {
        _select.gameObject.SetActive(false);
        _name.text = pet.name;
        _level.text = string.Format("Lv.{0}", pet.level);
        _score.text = pet.score.ToString();

        List<int> skills = pet.skills;
        for (int i = 0; i < items.Length; i++)
        {
            PetSkillItem item = items[i];
            if (i < skills.Count)
            {
                Skill.SkillBase data = SkillManager.instance.GetSkill(skills[i]);
                item.Icon = data.icon;
                item.Mark = false;
                InputManager.instance.AddClickListener(item.gameObject, (go) =>
                {
                    SelectItem(_select, go.transform);
                });
            }
            else
            {
                item.Icon = string.Empty;
                item.Mark = false;
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
    }

    private void SelectItem(Transform select, Transform item)
    {
        select.parent = item;
        select.localPosition = Vector3.zero;
        select.localScale = Vector3.one;
        select.gameObject.SetActive(true);
    }

    private void UpdateAptitude(PetInfo pet)
    {
        _descriptions[0].text = pet.physicsPowerFactor.ToString();

        _descriptions[1].text = pet.physicsDefenseFactor.ToString();

        _descriptions[2].text = pet.magicPowerFactor.ToString();

        _descriptions[3].text = pet.magicDefenseFactor.ToString();

        _descriptions[4].text = pet.hpFactor.ToString();

        _descriptions[5].text = "0";

        _descriptions[6].text = string.Format("{0:N3}", pet.growthFactor);
    }

    private void SetEntity(PetInfo pet)
    {
        PetsData petData = PetsInitialConfig.SharedInstance.GetPetsData(pet.utype);
        if (petData == null) return;

        string resName = petData.modelID;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                ClearModel();
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = SortingOrder + 1;
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private void ClearModel()
    {
        for (int i = 0; i < _entityRoot.childCount; i++)
        {
            Transform child = _entityRoot.GetChild(i);
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
    }
    #endregion

}
