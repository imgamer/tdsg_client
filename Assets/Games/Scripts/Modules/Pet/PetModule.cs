﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DBID_TYPE = System.UInt64;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;
/// <summary>
/// 宠物控制类
/// </summary>
public class PetModule
{
    private KBEngine.Avatar _avatar;
    public PetModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    #region 宠物相关操作
    private List<PetInfo> _pets = new List<PetInfo>();
    //获取所有宠物
    public List<PetInfo> GetAllPets()
    {
        return _pets;
    }
    public List<PetInfo> GetSortPets()
    {
        _pets.Sort((PetInfo a, PetInfo b) => 
        {
            return b.dbid.CompareTo(a.dbid);
        });
        return _pets;
    }

    public List<PetInfo> GetPetsOfUType(uint utype)
    {
        return _pets.FindAll((p) =>
            {
                return p.utype == utype;
            });
    }

    public PetInfo GetPetInfoByDBID(DBID_TYPE dbid)
    {
        PetInfo ret = _pets.Find((pet) => 
        {
            return pet.dbid == dbid;
        });
        return ret;
    }
    //获取出战宠物
    public PetInfo GetBattlePet()
    {
        return GetPetInfoByDBID(_avatar.FightingPetDBID);
    }
    //当前选择的宠物DBID
    private DBID_TYPE _selectedDBID = 0;
    //获取当前选择的宠物
    public PetInfo GetSelectedPet()
    {
        if (_pets.Count <= 0) return null;
        if (_selectedDBID == 0) return _pets[0];
        return GetPetInfoByDBID(_selectedDBID);
    }
    public int GetSelectedIndex()
    {
        PetInfo pet = GetPetInfoByDBID(_avatar.FightingPetDBID);
        if (pet == null) return 0;
        return _pets.IndexOf(pet);
    }
    //选择宠物
    public void SelectPet(PetInfo data)
    {
        if (data.dbid != _selectedDBID)
        {
            _selectedDBID = data.dbid;
            EventManager.Invoke<PetInfo>(EventID.EVNT_PET_SELECT_UPDATE, data);
        }
    }
    #endregion

    #region 合宠本地操作
    //当前合宠对象的DBID
    private List<object> _tempPets = new List<object>(2);
    //获取所有可以合宠的宠物
    public List<PetInfo> GetCombinePets()
    {
        List<PetInfo> list = new List<PetInfo>();
        foreach (var item in _pets)
        {
            if (_tempPets.Contains(item.dbid)) continue;
            if (item.type == Define.PET_TYPE_SUPER || item.type == Define.PET_TYPE_MYTHICAL) continue;
            list.Add(item);
        }
        return list;
    }

    public void SetCombinePet(PetInfo oldPet, PetInfo newPet)
    {
        if (newPet == null) return;
        if (newPet.dbid == _avatar.FightingPetDBID) 
        {
            TipManager.instance.ShowTextTip(Define.PET_FIGHT_NOT_COMBINE);
            return;
        }
        if (oldPet != null) _tempPets.Remove(oldPet.dbid);
        _tempPets.Add(newPet.dbid);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_COMBINEPET_UPDATE, newPet);
    }
    public void ResetCombinePets()
    {
        _tempPets.Clear();
    }

    public int CountPetOfUType(uint utype)
    {
        int amount = 0;
        _pets.ForEach((p) =>
            {
                if (p.utype == utype)
                    amount++;
            });
        return amount;
    }
    #endregion

    #region 宠物网络请求
    //宠物放生
    public void RequestFree(DBID_TYPE dbid)
    {
        if (_pets.Count <= 1)
        {
            //提示不能放生唯一宠物
            return;
        }
		_avatar.cellCall("freePet", dbid);
    }
    //宠物出战
    public void RequestBattle(DBID_TYPE dbid)
    {
        if (dbid == _avatar.FightingPetDBID)
        {
            _avatar.baseCall("setFightingPet", new object[] { 0 });
        }
        else
        {
            _avatar.baseCall("setFightingPet", new object[] { dbid });
        }
    }

    //重置宠物属性加点
    public void ReqResetPetFreePoint(DBID_TYPE dbid)
    {
        _avatar.baseCall("resetPetFreePoint", dbid);
    }

    //同步宠物加点属性
    public void SyncPetFreePointAssignmentToServer(DBID_TYPE dbid, int force, int spirituality, int vitality, int gengu)
    {
        JSON_DICT_TYPE assignment = new JSON_DICT_TYPE();
        assignment["force"] = (UInt32)force;
        assignment["spirituality"] = (UInt32)spirituality;
        assignment["vitality"] = (UInt32)vitality;
        assignment["gengu"] = (UInt32)gengu;
        _avatar.baseCall("assignPetFreePoint", dbid, assignment);
    }

	/// <summary>
	/// 宠物资质培养
	/// </summary>
	/// <param name="dbid">Dbid.</param>
	/// <param name="factorIndex">资质索引.</param>
	/// <param name="enhanceAmount">强化次数.</param>
	public void RequestToEnhanceFactor(DBID_TYPE dbid, int factorIndex, int enhanceAmount)
	{
		//资质索引应该这样生成：
		//int index = Define.PET_FACTOR_COLLECTION.IndexOf("growthFactor");
		_avatar.baseCall("enhancePetFactor", dbid, (Byte)factorIndex, (UInt16)enhanceAmount);
	}

    /// <summary>
    /// 请求学习技能
    /// </summary>
    /// <param name="dbid">宠物DBID</param>
    /// <param name="skillBookID">技能书ID</param>
    public void RequestToLearnSkill(DBID_TYPE dbid, int skillBookID)
    {
        //为了测试方便，服务器临时定义了技能书ID和技能ID的对应关系：
        // 技能书ID: 技能ID
        // 1: 1002001, 金顶绵掌
        // 2: 1004001, 清心普善咒
        // 3: 2005001, 点苍派技能5
        _avatar.baseCall("learnPetSkill", dbid, ITEM_ID.Convert(skillBookID));
    }

    /// <summary>
    /// 请求认证技能
    /// </summary>
    /// <param name="dbid"></param>
    /// <param name="skillID"></param>
    public void RequestToBindSkill(DBID_TYPE dbid, int skillID)
    {
        _avatar.baseCall("bindPetSkill", dbid, skillID);
    }

    /// <summary>
    /// 请求升阶
    /// </summary>
    /// <param name="dbid"></param>
    public void RequestToUpgradeStage(DBID_TYPE dbid)
    {
        _avatar.baseCall("petUpgradeStage", dbid);
    }

    /// <summary>
    /// 请求还童
    /// </summary>
    /// <param name="dbid"></param>
    public void RequestToRejuvenate(DBID_TYPE dbid)
    {
        _avatar.baseCall("petRejuvenate", dbid);
    }

    /// <summary>
    /// 请求合宠
    /// </summary>
    public void PetCombine()
    {
        if(_tempPets.Count != 2)
        {
            return;
        }
        _avatar.baseCall("petCombine", _tempPets);
    }

    #endregion

    #region 宠物更新
    public void RecvPetData(Dictionary<string, object> petData)
    {
        var pet = new PetInfo(petData);
        _pets.Add(pet);
        EventManager.Invoke<object>(EventID.EVNT_PET_NUM_UPDATE, null);
    }

    public void SetFightingPetDBID(object old)
    {
        EventManager.Invoke<object>(EventID.EVNT_PET_BATTLE_STATE, null);
    }

    public void OnPetFreed(DBID_TYPE dbid)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet != null)
        {
            _pets.Remove(pet);
            EventManager.Invoke<object>(EventID.EVNT_PET_NUM_UPDATE, null);
        }
        else
        {
            Printer.LogError("Can't find pet by dbid " + dbid.ToString());
        }
    }

	public void UpdatePetPropertyJson(DBID_TYPE dbid, string attrName, string json)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
		if (pet == null) return;
        pet.updateFromJson(attrName, json);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void UpdatePetPropertyInt32(DBID_TYPE dbid, string attrName, Int32 value)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet == null) return;
        pet.update(attrName, value);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void UpdatePetPropertyFloat(DBID_TYPE dbid, string attrName, float value)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet == null) return;
        pet.update(attrName, value);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void UpdatePetPropertyString(DBID_TYPE dbid, string attrName, string value)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet == null) return;
        pet.update(attrName, value);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void UpdatePetPropertyListInt(DBID_TYPE dbid, string attrName, UInt16 index, Int32 newValue)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet == null) return;
        pet.updateListInt(attrName, index, newValue);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void OnPetRejuvenated(DBID_TYPE dbid, Dictionary<string, object> data)
    {
        PetInfo pet = GetPetInfoByDBID(dbid);
        if (pet == null) return;
        pet.OnRejuvenate(data);
        EventManager.Invoke<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, pet);
    }

    public void OnPetCombined(DBID_TYPE dbid)
    {
        EventManager.Invoke<DBID_TYPE>(EventID.EVNT_PET_COMBINEPET_SUCCESS, dbid);
    }
    #endregion

}
