//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Reflection;
using System.Collections.Generic;
using LitJson;

using EQUIPMENT_LOCATION_TYPE = System.Byte;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;
using EQUIPMENT_BAG_TYPE = System.Collections.Generic.Dictionary<EQUIPMENT_LOCATION, Item.ItemEquipment>;

public class PetInfo
{	
	public UInt64 dbid;
	public UInt32 utype;
    public string name;
	public byte type;				//类型：野生，宝宝，变异，神兽
	public UInt16 level;			//等级
	public Int32 EXP;				//当前经验值
	public UInt16 daoheng;			//道行
	public byte stage;				//阶次
	public UInt16 freePoint;		//自由点数
	public JSON_DICT_TYPE freePointAssignment;	//自由点分配
    public UInt32 score;	//评分

	public float growthFactor;		//成长资质
	public Int32 physicsPowerFactor;//物理攻击资质
	public Int32 physicsDefenseFactor;//物理防御资质
	public Int32 magicPowerFactor;	//法术攻击资质
    public Int32 magicDefenseFactor;  //法术防御资质
	public Int32 hpFactor;//生命资质
	
	public UInt32 force;			//武力
	public UInt32 vitality;			//体质
	public UInt32 spirituality;  	//灵性
	public UInt32 gengu;			//根骨

	public List<Int32> skills = new List<Int32>();
    public List<Int32> bindedSkills = new List<Int32>(); //认证技能

    public Int32 talent;            //天赋
	
	#region private fileds
	private EQUIPMENT_BAG_TYPE _equipments;
	#endregion

	public EQUIPMENT_BAG_TYPE Equpments
	{
		get{ return _equipments;}
	}
	
	public PetInfo(JSON_DICT_TYPE petData)
	{
		dbid = (UInt64)petData ["dbid"];
		utype = (UInt32)petData ["utype"];
        name = (string)petData["name"];
		type = (byte)petData ["type"];
		level = (UInt16)petData ["level"];
		EXP = (Int32)petData ["EXP"];
		daoheng = (UInt16)petData ["daoheng"];
		stage = (byte)petData ["stage"];
		freePoint = (UInt16)petData ["freePoint"];
		freePointAssignment = (JSON_DICT_TYPE)petData ["freePointAssignment"];
        score = (UInt32)petData["score"];

        growthFactor = (float)petData["growthFactor"];
		physicsPowerFactor = (Int32)petData ["physicsPowerFactor"];
		physicsDefenseFactor = (Int32)petData ["physicsDefenseFactor"];
		magicPowerFactor = (Int32)petData ["magicPowerFactor"];
		magicDefenseFactor = (Int32)petData ["magicDefenseFactor"];
		hpFactor = (Int32)petData ["hpFactor"];

		force = (UInt32)petData ["force"];
		vitality = (UInt32)petData ["vitality"];
		spirituality = (UInt32)petData ["spirituality"];
		gengu = (UInt32)petData ["gengu"];

        talent = (Int32)petData["talent"];

		foreach (object skillID in (List<object>)petData ["skills"])
		{
			skills.Add(Convert.ToInt32(skillID));
		}

        foreach (object skillID in (List<object>)petData["bindedSkills"])
		{
            bindedSkills.Add(Convert.ToInt32(skillID));
		}
	}

    /// <summary>
    /// 还童更新
    /// </summary>
    /// <param name="data"></param>
    public void OnRejuvenate(Dictionary<string, object> petData)
    {
        level = (UInt16)1;
        EXP = (Int32)0;
        freePoint = (UInt16)0;
        type = (byte)petData["type"];
        score = (UInt32)petData["score"];

        freePointAssignment["force"] = (UInt32)0;
        freePointAssignment["vitality"] = (UInt32)0;
        freePointAssignment["spirituality"] = (UInt32)0;
        freePointAssignment["gengu"] = (UInt32)0;

        growthFactor = (float)petData["growthFactor"];
        physicsPowerFactor = (Int32)petData["physicsPowerFactor"];
        physicsDefenseFactor = (Int32)petData["physicsDefenseFactor"];
        magicPowerFactor = (Int32)petData["magicPowerFactor"];
        magicDefenseFactor = (Int32)petData["magicDefenseFactor"];
        hpFactor = (Int32)petData["hpFactor"];

        force = (UInt32)petData["force"];
        vitality = (UInt32)petData["vitality"];
        spirituality = (UInt32)petData["spirituality"];
        gengu = (UInt32)petData["gengu"];

        skills.Clear();
        bindedSkills.Clear();

        foreach (object skillID in (List<object>)petData["skills"])
        {
            skills.Add(Convert.ToInt32(skillID));
        }
    }

	public bool update(string attrName, object value)
	{
		FieldInfo field = this.GetType().GetField(attrName, Define.PUBLIC_AND_NONPUBLIC);
		if (field != null)
		{
			//保留旧值
			object oldValue = GameUtils.DeepCopy(field.GetValue(this));
			//检查看看是否有自定义赋值方法，优先使用自定义赋值
			MethodInfo setter = this.GetType().GetMethod("set_" + attrName, Define.PUBLIC_AND_NONPUBLIC);
			if(setter != null)
			{
				setter.Invoke(this, new object[]{value});
			}
			else
			{
				field.SetValue(this, Convert.ChangeType(value, oldValue.GetType()));
			}
			
			//调用自定义值改变通知
			MethodInfo notifier = this.GetType().GetMethod("onSet_" + attrName, Define.PUBLIC_AND_NONPUBLIC);
			if (notifier != null)
			{
				notifier.Invoke(this, new object[]{oldValue});
			}
			
			return true;
		}
		else
		{
			Printer.LogError(String.Format("PetInfo::update: property {0} dosen't exist.", attrName));
			return false;
		}
	}

    public bool updateFromJson(string attrName, string json)
    {
        //本来想做一个json的通用更新，但是因为这里不知道服务器定义的
        //数据类型，所以做不到。所以需要用json更新的属性，都要定义一个
        //更新方法（即set_attrName方法），在方法里再把json转为正确的格式。
        return update(attrName, json);
    }

	public bool updateListInt(string attrName, UInt16 index, Int32 newValue)
	{
		FieldInfo field = this.GetType().GetField(attrName);
		if (field != null)
		{
			List<Int32> currentValue = (List<Int32>)field.GetValue(this);
			currentValue [index] = newValue;
			field.SetValue(this, currentValue);
			return true;
		}
		else
		{
			Printer.LogError(String.Format("PetInfo::updateListInt: property {0} dosen't exist.", attrName));
			return false;
		}
	}

	#region property setters
	public void set_freePointAssignment(string json)
	{
		JSON_DICT_TYPE dict = JsonMapper.ToObject<JSON_DICT_TYPE>(json);
        UInt32 oldForce = (UInt32)freePointAssignment["force"];
        UInt32 oldVitality = (UInt32)freePointAssignment["vitality"];
        UInt32 oldSpirituality = (UInt32)freePointAssignment["spirituality"];
        UInt32 oldGengu = (UInt32)freePointAssignment["gengu"];

        UInt32 newForce = Convert.ToUInt32(dict["force"]);
        UInt32 newVitality = Convert.ToUInt32(dict["vitality"]);
        UInt32 newSpirituality = Convert.ToUInt32(dict["spirituality"]);
        UInt32 newGengu = Convert.ToUInt32(dict["gengu"]);

        freePointAssignment["force"] = newForce;
        freePointAssignment["vitality"] = newVitality;
        freePointAssignment["spirituality"] = newSpirituality;
        freePointAssignment["gengu"] = newGengu;

        force += newForce - oldForce;
        vitality += newVitality - oldVitality;
        spirituality += newSpirituality - oldSpirituality;
        gengu += newGengu - oldGengu;
	}

    public void set_skills(string json)
    {
        skills = JsonMapper.ToObject<List<Int32>>(json);
    }

    public void set_bindedSkills(string json)
    {
        bindedSkills = JsonMapper.ToObject<List<Int32>>(json);
    }

	public void set_freePoint(Int32 value)
	{
		freePoint = (UInt16)value;
	}
	#endregion
	
	#region property updated notifiers
	public void onSet_level(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} level changed from {1} to {2}", dbid, oldValue, level));
	}

	public void onSet_stage(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} stage changed from {1} to {2}", dbid, oldValue, stage));
	}

    public void onSet_growthFactor(float oldValue)
	{
		Printer.Log(String.Format("Pet {0} growthFactor changed from {1} to {2}", dbid, oldValue, growthFactor));
	}
	
	public void onSet_physicsPowerFactor(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} physicsPowerFactor changed from {1} to {2}", dbid, oldValue, physicsPowerFactor));
	}
	
	public void onSet_physicsDefenseFactor(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} physicsDefenseFactor changed from {1} to {2}", dbid, oldValue, physicsDefenseFactor));
	}
	
	public void onSet_magicPowerFactor(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} magicPowerFactor changed from {1} to {2}", dbid, oldValue, magicPowerFactor));
	}
	
	public void onSet_magicDefenseFactor(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} magicDefenseFactor changed from {1} to {2}", dbid, oldValue, magicDefenseFactor));
	}
	
	public void onSet_hpFactor(Int32 oldValue)
	{
		Printer.Log(String.Format("Pet {0} hpFactor changed from {1} to {2}", dbid, oldValue, hpFactor));
	}

    public void onSet_skills(List<Int32> oldValue)
    {
        Printer.Log(String.Format("Pet {0} skills changed from {1} to {2}", dbid, oldValue.ToString(), skills.ToString()));
    }

    public void onSet_bindedSkills(List<Int32> oldValue)
    {
        Printer.Log(String.Format("Pet {0} bindedSkills changed from {1} to {2}", dbid, oldValue.ToString(), bindedSkills.ToString()));
    }
	#endregion

    #region 宠物基础属性显示
    public string Name
    {
        get { return name; }
    }
    #endregion

    #region 五个基础点
    public UInt32 BaseForce
    {
        get { return force - (UInt32)freePointAssignment["force"]; }
    }

    public UInt32 BaseSpirituality
    {
        get { return spirituality - (UInt32)freePointAssignment["spirituality"]; }
    }

    public UInt32 BaseVitality
    {
        get { return vitality - (UInt32)freePointAssignment["vitality"]; }
    }

    public UInt32 BaseGengu
    {
        get { return gengu - (UInt32)freePointAssignment["gengu"]; }
    }

    #endregion

    #region 宠物属性计算
    public int Hp
    {
        get { return PetRule.CalcHpMax(level, (int)vitality, hpFactor, growthFactor); }
    }
    public int PhyAttack
    {
        get { return PetRule.CalcPhysicsPower(level, (int)force, physicsPowerFactor, growthFactor); }
    }
    public int MagAttack
    {
        get { return PetRule.CalcMagicPower(level, (int)spirituality, magicPowerFactor, growthFactor); }
    }
    public int PhyDefense
    {
        get { return PetRule.CalcPhysicsDefense(level, (int)force, (int)gengu, physicsDefenseFactor, growthFactor); }
    }
    public int MagDefense
    {
        get { return PetRule.CalcMagicDefense(level, (int)vitality, (int)spirituality, (int)gengu, magicDefenseFactor, growthFactor); }
    }
    public int Speed
    {
        get { return PetRule.CalcAttackSpeed(utype, type); }
    }
    #endregion
}

