﻿using UnityEngine;
using System.Collections;

public class UIFactionCreate : UIWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UIInput _input;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _input = center.Find("Input").GetComponent<UIInput>();
        GameObject cancelBtn = center.Find("CanelBtn").gameObject;
        InputManager.instance.AddClickListener(cancelBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject createBtn = center.Find("CreateBtn").gameObject;
        InputManager.instance.AddClickListener(createBtn, (go) =>
        {
            
        });
    }
}
