﻿using UnityEngine;
using System.Collections;

public class FactionInfomationInfoPage : WinPage
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _name;
    private UILabel _master;
    private UILabel _level;
    private UILabel _id;
    private UILabel _rank;
    private UILabel _count;

    private UILabel _noticeText;
    private UIInput _noticeInput;
    private void InitPage()
    {
        Transform info = transform.Find("Info");
        _name = info.Find("Name/Value").GetComponent<UILabel>();
        _master = info.Find("Master/Value").GetComponent<UILabel>();
        _level = info.Find("Level/Value").GetComponent<UILabel>();
        _id = info.Find("ID/Value").GetComponent<UILabel>();
        _rank = info.Find("Rank/Value").GetComponent<UILabel>();
        _count = info.Find("Count/Value").GetComponent<UILabel>();

        Transform notice = transform.Find("Notice");
        _noticeText = notice.Find("NoticeText").GetComponent<UILabel>();
        _noticeInput = notice.Find("NoticeInput").GetComponent<UIInput>();
        _noticeInput.gameObject.SetActive(false);

        GameObject editBtn = notice.Find("EditBtn").gameObject;
        InputManager.instance.AddClickListener(editBtn, (go) =>
        {
            _noticeText.enabled = false;
            _noticeInput.enabled = true;
        });
    }
}
