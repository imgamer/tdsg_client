﻿using UnityEngine;
using System.Collections;

public class FactionInfomationPage : WinPage
{
    private WinPage _onlinePage;
    protected override void OnInit()
    {
        InitTab();

        _onlinePage = transform.Find("OnlineMember").GetComponent<WinPage>();
        GameObject donateBtn = transform.Find("DonateBtn").gameObject;
        InputManager.instance.AddClickListener(donateBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIFactionDonate);
        });

        GameObject gotoBtn = transform.Find("GotoBtn").gameObject;
        InputManager.instance.AddClickListener(gotoBtn, (go) =>
        {

        });
    }

    protected override void OnOpen(params object[] args)
    {
        uiTab.SwitchTo(0);
        _onlinePage.Open(ParentUI);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();  
            curContent.Open(ParentUI);
        };
    }
}
