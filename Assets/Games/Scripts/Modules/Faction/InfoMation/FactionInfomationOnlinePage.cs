﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FactionInfomationOnlinePage : WinPage
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _onlineCount;

    private UI.Grid _grid;
    private FactionOnlineMemberItem _tempItem;
    private GameObject _select;
    private void InitPage()
    {
        _onlineCount = transform.Find("OnlineCount/Value").GetComponent<UILabel>();
        _select = transform.Find("Select").gameObject;
        _select.SetActive(false);
        _grid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = transform.Find("Scroll/Grid/Item").GetComponent<FactionOnlineMemberItem>();
        _tempItem.gameObject.SetActive(false);
    }

    private List<FactionOnlineMemberItem> _items = new List<FactionOnlineMemberItem>();
    private void RefreshItem()
    {
         SelectItem(null);
        List<string> itemsData = new List<string>();
        int dataCount = itemsData.Count;
        int itemCount= _items.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    _items[i].gameObject.SetActive(true);
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _tempItem.gameObject.SetActive(true);
                    FactionOnlineMemberItem item = Instantiate(_tempItem) as FactionOnlineMemberItem;
                    item.Init(ParentUI);
                    _tempItem.gameObject.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    SetItemData(item, itemsData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _items[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
    }

    private void SetItemData(FactionOnlineMemberItem item, string data)
    {

    }

    private void SelectItem(FactionOnlineMemberItem item)
    {
        if (item == null)
        {
            _select.SetActive(false);
            return;
        }
        _select.SetActive(true);
        _select.transform.parent = item.transform;
        _select.transform.localPosition = Vector3.zero;
        _select.transform.localScale = Vector3.one;
    }
}
