﻿using UnityEngine;
using System.Collections;

public class FactionInfoamtionUpdatePage : WinPage
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {
       
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _fund;
    private UILabel _updateFund;
    private UILabel _functionText;
    private void InitPage()
    {
        _fund = transform.Find("Fund/Value").GetComponent<UILabel>();
        _updateFund = transform.Find("UpdateFund/Value").GetComponent<UILabel>();
        _functionText = transform.Find("UpdateFunction/FunctionText").GetComponent<UILabel>();
        GameObject updateBtn = transform.Find("UpdateBtn").gameObject;
        InputManager.instance.AddClickListener(updateBtn, (go) =>
        {

        });
    }
}
