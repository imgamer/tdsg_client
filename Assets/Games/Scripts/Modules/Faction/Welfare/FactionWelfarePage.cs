﻿using UnityEngine;
using System.Collections;

public class FactionWelfarePage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _weekWelfare;
    private void InitItem()
    {
        GameObject skillBtn = transform.Find("Grid/0/OpenBtn").gameObject;
        InputManager.instance.AddClickListener(skillBtn, (go) =>
        {

        });

        GameObject trainBtn = transform.Find("Grid/1/OpenBtn").gameObject;
        InputManager.instance.AddClickListener(trainBtn, (go) =>
        {

        });

        GameObject shopBtn = transform.Find("Grid/2/OpenBtn").gameObject;
        InputManager.instance.AddClickListener(shopBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIFactionShop);
        });

        _weekWelfare = transform.Find("Grid/3/Coin/Value").GetComponent<UILabel>();
        GameObject getBtn = transform.Find("Grid/3/GetBtn").gameObject;
        InputManager.instance.AddClickListener(skillBtn, (go) =>
        {

        });
    }
}
