﻿using UnityEngine;
using System.Collections;

public class UIFactionShop : UIWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UI.Grid _grid;
    private GameObject _tempItem;

    private UILabel _name;
    private UILabel _desc;
    private UILabel _coinCount;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        _grid = center.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;

        _name = center.Find("Prop/Name").GetComponent<UILabel>();
        _desc = center.Find("Prop/Desc").GetComponent<UILabel>();

        _coinCount = center.Find("Have/Value").GetComponent<UILabel>();

        GameObject refushBtn = center.Find("Refresh/RefreshBtn").gameObject;
        InputManager.instance.AddClickListener(refushBtn, (go) =>
        {

        });

        GameObject buyBtn = center.Find("BuyBtn").gameObject;
        InputManager.instance.AddClickListener(buyBtn, (go) =>
        {

        });
    }
}
