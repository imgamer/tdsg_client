﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FactionMemberListPage : WinPage
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UI.Grid _grid;
    private FactionAllMemberItem _tempItem;
    private GameObject _select;
    private void InitPage()
    {
        _select = transform.Find("Select").gameObject;
        _select.SetActive(false);
        _grid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = transform.Find("Scroll/Grid/Item").GetComponent<FactionAllMemberItem>();
        _tempItem.gameObject.SetActive(false);

        GameObject exitBtn = transform.Find("ExitBtn").gameObject;
        InputManager.instance.AddClickListener(exitBtn, (go) =>
        {

        });

        GameObject massTextBtn = transform.Find("MassTextBtn").gameObject;
        InputManager.instance.AddClickListener(massTextBtn, (go) =>
        {

        });

        GameObject recruitBtn = transform.Find("RecruitBtn").gameObject;
        InputManager.instance.AddClickListener(recruitBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIFactionRecruit);
        });
    }

    private List<FactionAllMemberItem> _items = new List<FactionAllMemberItem>();
    private void RefreshItem()
    {
        SelectItem(null);
        List<string> itemsData = new List<string>();
        int dataCount = itemsData.Count;
        int itemCount = _items.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    _items[i].gameObject.SetActive(true);
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _tempItem.gameObject.SetActive(true);
                    FactionAllMemberItem item = Instantiate(_tempItem) as FactionAllMemberItem;
                    item.Init(ParentUI);
                    _tempItem.gameObject.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    SetItemData(_items[i], itemsData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _items[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
    }

    private void SetItemData(FactionAllMemberItem item, string data)
    {

    }

    private void SelectItem(FactionAllMemberItem item)
    {
        if (item == null)
        {
            _select.SetActive(false);
            return;
        }
        _select.SetActive(true);
        _select.transform.parent = item.transform;
        _select.transform.localPosition = Vector3.zero;
        _select.transform.localScale = Vector3.one;
    }
}
