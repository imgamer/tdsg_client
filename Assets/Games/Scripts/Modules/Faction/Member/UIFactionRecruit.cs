﻿using UnityEngine;
using System.Collections;

public class UIFactionRecruit : UIWin 
{
    private UIInput _input;
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _input = center.Find("Input").GetComponent<UIInput>();

        GameObject cancelBtn = center.Find("CancelBtn").gameObject;
        InputManager.instance.AddClickListener(cancelBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject sendBtn = center.Find("SendBtn").gameObject;
        InputManager.instance.AddClickListener(sendBtn, (go) =>
        {
            
        });
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
