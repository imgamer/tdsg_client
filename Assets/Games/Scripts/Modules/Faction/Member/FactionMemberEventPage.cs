﻿using UnityEngine;
using System.Collections;

public class FactionMemberEventPage : WinPage
{
    private UITextList _eventText;
    protected override void OnInit()
    {
        _eventText = transform.Find("Scroll/Label").GetComponent<UITextList>();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
