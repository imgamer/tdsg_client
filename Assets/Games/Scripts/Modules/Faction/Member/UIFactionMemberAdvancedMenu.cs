﻿using UnityEngine;
using System.Collections;

public class UIFactionMemberAdvancedMenu : UIWin
{
    protected override void OnInit()
    {
        InitMenu();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _name;
    private UILabel _level;
    private UILabel _id;
    private UILabel _school;

    private GameObject _postMenu;
    private void InitMenu()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform rolemenu = center.Find("RoleMenu");
        _name = rolemenu.Find("Name").GetComponent<UILabel>();
        _level = rolemenu.Find("Icon/Level").GetComponent<UILabel>();
        _id = rolemenu.Find("ID").GetComponent<UILabel>();
        _school = rolemenu.Find("ID").GetComponent<UILabel>();

        Transform postMenu = center.Find("PostMenu");
        _postMenu = postMenu.gameObject;
        _postMenu.SetActive(false);
    }
}
