﻿using UnityEngine;
using System.Collections;

public class UIFactionMemberMenu : UIWin
{
    protected override void OnInit()
    {
        InitMenu();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void InitMenu()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform uigrid = center.Find("Root/UIGrid");
        GameObject chatBtn = uigrid.Find("0").gameObject;
        InputManager.instance.AddClickListener(chatBtn, (go) =>
        {

        });

        GameObject addBtn = uigrid.Find("1").gameObject;
        InputManager.instance.AddClickListener(addBtn, (go) =>
        {

        });

        GameObject inviteBtn = uigrid.Find("2").gameObject;
        InputManager.instance.AddClickListener(inviteBtn, (go) =>
        {

        });
    }
}
