﻿using UnityEngine;
using System.Collections;

public class FactionMemberPage : WinPage
{
    protected override void OnInit()
    {
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        uiTab.SwitchTo(0);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(ParentUI);
        };
    }
}
