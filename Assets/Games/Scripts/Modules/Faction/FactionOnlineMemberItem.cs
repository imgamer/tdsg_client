﻿using UnityEngine;
using System.Collections;

public class FactionOnlineMemberItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value;
        }
    }

    private UILabel _school;
    public string School
    {
        set
        {
            _school.text = value;
        }
    }

    private UILabel _post;
    public string Post
    {
        set
        {
            _post.text = value;
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _school = transform.Find("School").GetComponent<UILabel>();
        _post = transform.Find("Post").GetComponent<UILabel>();
    }	
}
