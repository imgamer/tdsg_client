﻿using UnityEngine;
using System.Collections;

public class UIFactionDonate : UIWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _count;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _count = center.Find("DonateCount/Value").GetComponent<UILabel>();
        GameObject helpBtn = center.Find("HelpBtn").gameObject;
        InputManager.instance.AddClickListener(helpBtn, (go) =>
        {
            
        });

        Transform item = center.Find("Item");
        GameObject normalBtn = item.Find("0/DonateBtn").gameObject;
        InputManager.instance.AddClickListener(normalBtn, (go) =>
        {

        });

        GameObject advancedBtn = item.Find("1/DonateBtn").gameObject;
        InputManager.instance.AddClickListener(advancedBtn, (go) =>
        {

        });

        GameObject supremacyBtn = item.Find("2/DonateBtn").gameObject;
        InputManager.instance.AddClickListener(supremacyBtn, (go) =>
        {

        });
    }
}
