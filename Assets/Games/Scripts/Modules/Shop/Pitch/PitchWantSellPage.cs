﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Item;

public class PitchWantSellPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private List<PitchPropItem> _pitchSellItem = new List<PitchPropItem>();
    private List<KitbagItem> _pitchPropItem = new List<KitbagItem>();
    private PitchData _selectPitchData;
    protected override void OnInit()
    {
        InitPage();

        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemovePutItem);
        EventManager.AddListener<object>(EventID.EVNT_PITCH_TRADE_UPDATE, UpdatePitchTradeSellItem);
        EventManager.AddListener<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, UpdatePitchPublicSellItem);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _pitchItemBg.SetActive(false);

        RefushPitchSellItem();
        RefushPropItem();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemovePutItem);
        EventManager.RemoveListener<object>(EventID.EVNT_PITCH_TRADE_UPDATE, UpdatePitchTradeSellItem);
        EventManager.RemoveListener<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, UpdatePitchPublicSellItem);
    }

    private UILabel _ptichIndex;
    private GameObject _btnBg;
    private void InitPage()
    {
        _ptichIndex = transform.Find("MyPitch").GetComponent<UILabel>();
        _btnBg = transform.Find("BtnBg").gameObject;
        GameObject petBtn = transform.Find("PetBtn").gameObject;
        InputManager.instance.AddClickListener(petBtn, (go) =>
        {
            _btnBg.transform.parent = petBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            _petScroll.SetActive(true);
            _propScroll.SetActive(false);
        });
        GameObject propBtn = transform.Find("PropBtn").gameObject;
        InputManager.instance.AddClickListener(propBtn, (go) =>
        {
            _btnBg.transform.parent = propBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            _petScroll.SetActive(false);
            _propScroll.SetActive(true);
        });
        GameObject recordBtn = transform.Find("RecordBtn").gameObject;
        InputManager.instance.AddClickListener(recordBtn, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UITradingRecord);
        });

        InitScroll();
    }

    private GameObject _tempPitchItem;
    private Transform _pitchGrid;
    private UI.Grid _pitchUiGrid;
    private GameObject _pitchItemBg;

    private GameObject _propScroll;
    private GameObject _tempPropItem;
    private Transform _propGrid;
    private UI.Grid _propUiGrid;

    private GameObject _petScroll;
    private GameObject _petItem;
    private Transform _petGrid;
    private UI.Grid _petUiGrid;
    private void InitScroll()
    {
        _pitchGrid = transform.Find("PitchScroll/Grid");
        _pitchUiGrid = _pitchGrid.GetComponent<UI.Grid>();
        _tempPitchItem = transform.Find("PitchScroll/Grid/Item").gameObject;
        _tempPitchItem.SetActive(false);
        _pitchItemBg = transform.Find("PitchItemBg").gameObject;
        _pitchItemBg.SetActive(false);

        _propScroll = transform.Find("PropScroll").gameObject;
        _propGrid = transform.Find("PropScroll/Grid");
        _propUiGrid = _propGrid.GetComponent<UI.Grid>();
        _tempPropItem = transform.Find("PropScroll/Grid/Item").gameObject;
        _tempPropItem.SetActive(false);
        //_propScroll.SetActive(false);

        _petScroll = transform.Find("PetScroll").gameObject;
        _petGrid = transform.Find("PetScroll/Grid");
        _petUiGrid = _petGrid.GetComponent<UI.Grid>();
        _petItem = transform.Find("PetScroll/Grid/Item").gameObject;
        _petItem.SetActive(false);
        _petScroll.SetActive(false);
    }

    private void RefushPitchSellItem()
    {
        List<PitchData> mySellData = new List<PitchData>();
        List<PitchData> publicData = _avatar.pitchModule.GetPublicSellData();
        foreach(PitchData data in publicData)
        {
            if(data.avatarDbid == _avatar.DatabaseID)
            {
                mySellData.Add(data);
            }
        }

        List<PitchData> tradeData = _avatar.pitchModule.GetTradeSellData();
        foreach (PitchData data in tradeData)
        {
            if (data.avatarDbid == _avatar.DatabaseID)
            {
                mySellData.Add(data);
            }
        }

        _ptichIndex.text = String.Format("{0}/8", mySellData.Count);

        int dataCount = mySellData.Count;
        int itemCount = _pitchSellItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetPitchSellItem(_pitchSellItem[i], mySellData[i]);
                    _pitchSellItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempPitchItem.SetActive(true);
                    Transform item = UITools.AddChild(_pitchGrid, _tempPitchItem);
                    _tempPitchItem.SetActive(false);
                    item.name = i.ToString();
                    PitchPropItem sellItem = item.GetComponent<PitchPropItem>();
                    sellItem.Init(ParentUI);
                    _pitchSellItem.Add(sellItem);
                    SetPitchSellItem(_pitchSellItem[i], mySellData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetPitchSellItem(_pitchSellItem[i], mySellData[i]);
                }
                else
                {
                    _pitchSellItem[i].gameObject.SetActive(false);
                }
            }
        }
        _pitchUiGrid.Reposition();
    }

    private void SetPitchSellItem(PitchPropItem item, PitchData data)
    {
        int goodsId = (int)(data.tradeId / 1000);
        int level = (int)(data.tradeId % 1000);
        item.Level = Convert.ToString(level);
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(goodsId);
        if (itemData == null)
        {
            Printer.LogWarning("Items dont't have  ItemID:" + goodsId);
        }
        ParentUI.SetDynamicSpriteName(item.Icon, itemData.icon);
        item.Name = itemData.name;
        if (data.treasure == 1)
        {
            item.SignObj.SetActive(true);
        }
        else
        {
            item.SignObj.SetActive(false);
        }
        item.Price = Convert.ToString(data.tradePrice);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _pitchItemBg.transform.parent = item.transform;
            _pitchItemBg.transform.localPosition = Vector3.zero;
            _pitchItemBg.SetActive(true);
            _selectPitchData = data;
        });
        item.TimeObj.SetActive(false);
    }

    private void RefushPropItem()
    {
        List<ItemBase> sellData = new List<ItemBase>();
        List<ItemBase> itemData = _avatar.kitbagModule.Items;
        List<CanPitchSellData> canSellData = _avatar.pitchModule.GetCanSellData();
        foreach (ItemBase data in itemData)
        {
            if (data.Type != 0)
            {
                foreach (CanPitchSellData pdata in canSellData)
                {
                    if (((int)(pdata.id / 1000) )== data.ID)
                    {
                        sellData.Add(data);
                    }
                }
            }
        }

        int dataCount = sellData.Count;
        int itemCount = _pitchPropItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetPropItem(_pitchPropItem[i], sellData[i]);
                    _pitchPropItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempPropItem.SetActive(true);
                    Transform item = UITools.AddChild(_propGrid, _tempPropItem);
                    _tempPropItem.SetActive(false);
                    item.name = i.ToString();
                    KitbagItem propItem = item.GetComponent<KitbagItem>();
                    propItem.Init(ParentUI);
                    _pitchPropItem.Add(propItem);
                    SetPropItem(_pitchPropItem[i], sellData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetPropItem(_pitchPropItem[i], sellData[i]);
                }
                else
                {
                    _pitchPropItem[i].gameObject.SetActive(false);
                }
            }
        }
        _propUiGrid.Reposition();
    }

    private void SetPropItem(KitbagItem item, ItemBase data)
    {
        ParentUI.SetDynamicSpriteName(item.GoodsSprite, data.Icon);
        item.GoodsObj.SetActive(true);
        if (data.Amount > 1)
        {
            item.Count = Convert.ToString(data.Amount);
            item.CountObj.SetActive(true);
        }
        else
        {
            item.CountObj.SetActive(false);
        }
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UIPropOperate, data);
        });
    }

    private void RemovePutItem(ITEM_ORDER order)
    {
        if (!Active) return;
        RefushPropItem();       
    }

    private void UpdatePitchTradeSellItem(object value)
    {
        if (!Active) return;
        RefushPitchSellItem();
    }

    private void UpdatePitchPublicSellItem(object value)
    {
        if (!Active) return;
        RefushPitchSellItem();
    }
}
