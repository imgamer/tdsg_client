﻿using UnityEngine;
using System.Collections;

public class PitchCategoryItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value.ToString();
        }
    }

    public GameObject IconObj
    {
        get
        {
            return _icon.gameObject;
        }
    }

    
    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
    }
}
