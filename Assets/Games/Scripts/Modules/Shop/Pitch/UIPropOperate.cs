﻿using UnityEngine;
using System.Collections;
using Item;
using System;
public class UIPropOperate : UIWin
{
    private KBEngine.Avatar _avatar;

    private ItemBase _itemData;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        InitInterface();
    }

    protected override void OnOpen(params object[] args)
    {
        _itemData = (ItemBase)args[0];
    }

    protected override void OnRefresh()
    {
        _name.text = _itemData.Name;
        SetDynamicSpriteName(_icon, _itemData.Icon);
        //_price.text = Convert.ToString(_itemData.Price);
        _price.text = "1000";
        _putPrice.text = "100";
    }

    protected override void OnClose()
    {
        _itemData = null;
    }

    protected override void OnUnInit()
    {

    }

    private UILabel _name;
    private UISprite _icon;
    private UILabel _price;
    private UILabel _putPrice;
    private void InitInterface()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _name = center.Find("Name").GetComponent<UILabel>();
        _icon = center.Find("Icon").GetComponent<UISprite>();

        _price = center.Find("OutTime/Price/Price").GetComponent<UILabel>();
        _putPrice = center.Find("OutTime/Put/PutPrice").GetComponent<UILabel>();

        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject cancelBtn = center.Find("CancelBtn").gameObject;
        InputManager.instance.AddClickListener(cancelBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject putBtn = center.Find("OutTime/PutBtn").gameObject;
        InputManager.instance.AddClickListener(putBtn, (go) =>
        {
            Int32 totalPrice = Convert.ToInt32(_price.text)+ Convert.ToInt32(_putPrice.text);
            _avatar.pitchModule.SellTradeItem((Int16)_itemData.Order, totalPrice);
            UIManager.instance.CloseWindow(winID);
        });
    }
}
