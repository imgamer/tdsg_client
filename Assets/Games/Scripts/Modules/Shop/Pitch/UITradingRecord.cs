﻿using UnityEngine;
using System.Collections;

public class UITradingRecord : UIWin
{
    protected override void OnInit()
    {
        InitInterface();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }


    private GameObject _sellItem;
    private Transform _sellGrid;
    private UI.Grid _sellUiGrid;

    private GameObject _buyItem;
    private Transform _buyGrid;
    private UI.Grid _buyUiGrid;
    private void InitInterface()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);

        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject clearBtn = center.Find("ClearBtn").gameObject;
        InputManager.instance.AddClickListener(clearBtn, (go) =>
        {

        });

        _sellGrid = center.Find("SellScroll/Grid");
        _sellUiGrid = _sellGrid.GetComponent<UI.Grid>();
        _sellItem = center.Find("SellScroll/Grid/Item").gameObject;
       // _sellItem.SetActive(false);

        _buyGrid = center.Find("BuyScroll/Grid");
        _buyUiGrid = _buyGrid.GetComponent<UI.Grid>();
        _buyItem = center.Find("BuyScroll/Grid/Item").gameObject;
        //_buyItem.SetActive(false);
    }
}
