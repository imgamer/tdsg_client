﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PitchNoticePage : WinPage
{
    private bool beInited = false;

    private KBEngine.Avatar _avatar;
    private List<PitchUIData> _uiData = new List<PitchUIData>();  //UI数据
    private List<PitchCategoryItem> _classifyItem = new List<PitchCategoryItem>(); //一级分类
    private List<PitchCategoryItem> _categoryItem = new List<PitchCategoryItem>();  //二级分类
    private List<PitchCategoryItem> _levelItem = new List<PitchCategoryItem>(); //等级分类

    private List<PitchData> _publicData = new List<PitchData>(); //摆摊出售数据

    //当前选择的分类ID
    private Int32 firstCategory = 0;
    private Int32 secondCategory = 0;
    private Int32 thirdCategory = 0;

    //当前选择分类的数据
    private List<PitchData> _currentCategoryData = new List<PitchData>();

    //数据可存储总页数
    private int totalIndexPage = 0;
    //当前页面索引页
    private int indexPage = 1;
    //当前排序状态 默认0 从低到高
    private int sortState = 0;

    //当前选择准备购买或关注的数据
    private PitchData _selectPitchData;
    protected override void OnInit()
    {
        InitPage();

        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        EventManager.AddListener<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, UpdatePitchPublicGoods);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        if (!beInited)
        {
            _uiData = _avatar.pitchModule.GetPitchUIData();
            RefushClassifyItem();
            _pageIndex.text = "1/1";
            beInited = true;
        }

        _goodsItemBg.SetActive(false);
        _selectPitchData = null;
        sortState = 0;
        UpdatePitchPublicGoods(null);

        _gold.text = Convert.ToString(_avatar.Gold);        
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, UpdatePitchPublicGoods);
    }

    private UILabel _gold;
    private UILabel _pageIndex;

    private UILabel _menuTitle;
    private GameObject _levelBtn;
    private GameObject _priceBtn;
    private GameObject _priceBtnArows;
    private GameObject _levelMenu;
    private GameObject _priceMenu;

    private void InitPage()
    {
        _gold = transform.Find("GoldNum").GetComponent<UILabel>();
        _pageIndex = transform.Find("PageIndex").GetComponent<UILabel>();

        GameObject searchBtn = transform.Find("SearchBtn").gameObject;
        InputManager.instance.AddClickListener(searchBtn, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UIPropSearch);
        });
        GameObject upBtn = transform.Find("UpBtn").gameObject;
        InputManager.instance.AddClickListener(upBtn, (go) =>
        {
            if(_currentCategoryData.Count == 0)
            {
                return;
            }
            indexPage++;
            if (indexPage > totalIndexPage)
            {
                indexPage = totalIndexPage;
                return;
            }
            RefushPithGoodsItem(indexPage);
            _pageIndex.text = String.Format("{0}/{1}", indexPage, totalIndexPage);
        });
        GameObject downBtn = transform.Find("DownBtn").gameObject;
        InputManager.instance.AddClickListener(downBtn, (go) =>
        {
            if (_currentCategoryData.Count == 0)
            {
                return;
            }
            if (indexPage == 1)
            {
                return;
            }
            indexPage--;
            RefushPithGoodsItem(indexPage);
            _pageIndex.text = String.Format("{0}/{1}", indexPage, totalIndexPage);
        });
        GameObject attentionBtn = transform.Find("AttentionBtn").gameObject;
        InputManager.instance.AddClickListener(attentionBtn, (go) =>
        {

        });
        GameObject addGoldBtn = transform.Find("AddGoldBtn").gameObject;
        InputManager.instance.AddClickListener(addGoldBtn, (go) =>
        {

        });
        _priceMenu = transform.Find("PriceMenu").gameObject;
        _priceMenu.SetActive(false);
        _priceBtn = transform.Find("PriceBtn").gameObject;
        _priceBtnArows = transform.Find("PriceBtn/bg").gameObject;
        InputManager.instance.AddClickListener(_priceBtn, (go) =>
        {
            _levelMenu.SetActive(false);
            if (_priceMenu.activeSelf)
            {
                _priceMenu.SetActive(false);
            }
            else
            {
                _priceMenu.SetActive(true);
            }
        });

        //从高到底
        GameObject priceHighBtn = transform.Find("PriceMenu/PriceHighBtn").gameObject;
        InputManager.instance.AddClickListener(priceHighBtn, (go) =>
        {
            sortState = 1;
            _priceMenu.SetActive(false);
            _priceBtnArows.transform.localEulerAngles = new Vector3(0, 0, 180f);
            _currentCategoryData = _avatar.pitchModule.SortPitchData(_currentCategoryData, sortState);
            if (_currentCategoryData.Count == 0)
            {
                return;
            }
            RefushPithGoodsItem(1); 
        });

        //从低到高
        GameObject priceLowBtn = transform.Find("PriceMenu/PriceLowBtn").gameObject;
        InputManager.instance.AddClickListener(priceLowBtn, (go) =>
        {
            sortState = 0;
            _priceBtnArows.transform.localEulerAngles = Vector3.zero;
            _priceMenu.SetActive(false);
            _currentCategoryData = _avatar.pitchModule.SortPitchData(_currentCategoryData, sortState);
            if (_currentCategoryData.Count == 0)
            {
                return;
            }
            RefushPithGoodsItem(1);
        });

        _menuTitle = transform.Find("LevelBtn/Label").GetComponent<UILabel>();
        _levelMenu = transform.Find("LevelMenu").gameObject;
        _levelMenu.SetActive(false);
        _levelBtn = transform.Find("LevelBtn").gameObject;
        _levelBtn.SetActive(false);
        InputManager.instance.AddClickListener(_levelBtn, (go) =>
        {
            _priceMenu.SetActive(false);
            if (_levelMenu.activeSelf)
            {
                _levelMenu.SetActive(false);
            }
            else
            {
                _levelMenu.SetActive(true);
            }
        });

        InitScroll();
    }

    private GameObject _tempClassifyItem;
    private GameObject _classifyItemBg;
    private Transform _classifyGrid;
    private UI.Grid _classifyUiGrid;

    private GameObject _categorySrcoll;
    private GameObject _tempCategoryItem;
    //private GameObject _categoryItemBg;
    private Transform _categoryGrid;
    private UI.Grid _categoryUiGrid;

    private GameObject _tempLevelMenuItem;
    private Transform _levelMenuGrid;
    private UI.Grid _levelMenuUiGrid;

    private GameObject _goodsSrcoll;
    //private GameObject _tempGoodsItem;
    private GameObject _goodsItemBg;
    private UI.Grid _goodsUiGrid;

    private const int _Num = 8;
    private PitchPropItem[] _goodItem = new PitchPropItem[_Num]; //物品
    private void InitScroll()
    {
        _classifyItemBg = transform.Find("ClassifyItemBg").gameObject;
        _classifyItemBg.SetActive(false);

        _classifyGrid = transform.Find("ClassifyScroll/Grid");
        _classifyUiGrid = _classifyGrid.GetComponent<UI.Grid>();
        _tempClassifyItem = transform.Find("ClassifyScroll/Grid/Item").gameObject;
        _tempClassifyItem.SetActive(false);

        _categorySrcoll = transform.Find("CategoryScroll").gameObject;
        _categoryGrid = transform.Find("CategoryScroll/Grid");
        _categoryUiGrid = _categoryGrid.GetComponent<UI.Grid>();
        _tempCategoryItem = transform.Find("CategoryScroll/Grid/Item").gameObject;
        _tempCategoryItem.SetActive(false);

        _levelMenuGrid = transform.Find("LevelMenu/Scroll/Grid");
        _levelMenuUiGrid = _levelMenuGrid.GetComponent<UI.Grid>();
        _tempLevelMenuItem = transform.Find("LevelMenu/Scroll/Grid/Item").gameObject;
        _tempLevelMenuItem.SetActive(false);

        _goodsItemBg = transform.Find("GoodsItemBg").gameObject;
        _goodsItemBg.SetActive(false);
        _goodsSrcoll = transform.Find("GoodsScroll").gameObject;
        Transform goodsGrid = transform.Find("GoodsScroll/Grid");
        _goodsUiGrid = goodsGrid.GetComponent<UI.Grid>();

        UITools.CopyFixNumChild(goodsGrid, _Num);
        for (int i = 0; i < _Num; i++)
        {
            Transform goods = goodsGrid.GetChild(i);
            goods.name = i.ToString();

            PitchPropItem goodsItem = goods.GetComponent<PitchPropItem>();
            goodsItem.Init(ParentUI);
            _goodItem[i] = goodsItem;
        }
        _goodsUiGrid.Reposition();
        _goodsSrcoll.SetActive(false);
    }

    private void RefushClassifyItem()
    {
        int uiDataCount = _uiData.Count;
        int itemCount = _classifyItem.Count;
        if (uiDataCount >= itemCount)
        {
            for (int i = 0; i < uiDataCount; i++)
            {
                if (i < itemCount)
                {
                    SetClassifyItemData(_classifyItem[i], _uiData[i]);
                    _classifyItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempClassifyItem.SetActive(true);
                    Transform item = UITools.AddChild(_classifyGrid, _tempClassifyItem);
                    _tempClassifyItem.SetActive(false);
                    item.name = i.ToString();
                    PitchCategoryItem classifyItem = item.GetComponent<PitchCategoryItem>();
                    classifyItem.Init(ParentUI);
                    _classifyItem.Add(classifyItem);
                    SetClassifyItemData(_classifyItem[i], _uiData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < uiDataCount)
                {
                    SetClassifyItemData(_classifyItem[i], _uiData[i]);
                }
                else
                {
                    _classifyItem[i].gameObject.SetActive(false);
                }
            }
        }
        _classifyUiGrid.Reposition();
    }

    private void SetClassifyItemData(PitchCategoryItem classifyItem, PitchUIData uiData)
    {
        classifyItem.Name = uiData.categoryName;
        classifyItem.IconObj.SetActive(false);
        InputManager.instance.AddClickListener(classifyItem.gameObject, (go) =>
        {
            _classifyItemBg.transform.parent = classifyItem.transform;
            _classifyItemBg.transform.localPosition = Vector3.zero;
            _classifyItemBg.SetActive(true);

            _categorySrcoll.SetActive(true);
            _goodsSrcoll.SetActive(false);
            _levelBtn.SetActive(false);
            _levelMenu.SetActive(false);
            _priceMenu.SetActive(false);
            indexPage = 1;
            _currentCategoryData.Clear();

            _selectPitchData = null;
            firstCategory = uiData.category;
            List<CategoryItem> categoryData = new List<CategoryItem>(uiData.subCategory.Values);
            RefushCategoryItem(categoryData);
        });
    }

    private void RefushCategoryItem(List<CategoryItem> categoryData)
    {
        int uiDataCount = categoryData.Count;
        int itemCount = _categoryItem.Count;
        if (uiDataCount >= itemCount)
        {
            for (int i = 0; i < uiDataCount; i++)
            {
                if (i < itemCount)
                {
                    SetCategoryItemData(_categoryItem[i], categoryData[i]);
                    _categoryItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempCategoryItem.SetActive(true);
                    Transform item = UITools.AddChild(_categoryGrid, _tempCategoryItem);
                    _tempCategoryItem.SetActive(false);
                    item.name = i.ToString();
                    PitchCategoryItem categoryItem = item.GetComponent<PitchCategoryItem>();
                    categoryItem.Init(ParentUI);
                    _categoryItem.Add(categoryItem);
                    SetCategoryItemData(_categoryItem[i], categoryData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < uiDataCount)
                {
                    SetCategoryItemData(_categoryItem[i], categoryData[i]);
                }
                else
                {
                    _categoryItem[i].gameObject.SetActive(false);
                }
            }
        }
        _categoryUiGrid.Reposition();
    }

    private void SetCategoryItemData(PitchCategoryItem item, CategoryItem itemData)
    {
        item.Name = itemData.categoryName;
        item.Icon = itemData.categoryIcon;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _categorySrcoll.SetActive(false);
            _goodsSrcoll.SetActive(true);
            _goodsItemBg.SetActive(false);
            //加载等级菜单
            List<CategoryLevel> categoryData = new List<CategoryLevel>(itemData.subCategory.Values);
            if (categoryData.Count > 0)
            {
                _levelBtn.SetActive(true);
                _levelMenu.SetActive(true);             
                RefushLevelMenuItem(categoryData);
                _levelMenu.SetActive(false);    
            }
            //加载具体数据
            secondCategory = itemData.category;

            _menuTitle.text = categoryData[0].categoryName;
            thirdCategory = thirdCategory = categoryData[0].category; ;
            _currentCategoryData = FindCategoryData(firstCategory, secondCategory, thirdCategory);

            SetPageIndex();
            //默认从低到高排序
            _currentCategoryData = _avatar.pitchModule.SortPitchData(_currentCategoryData, sortState);
            RefushPithGoodsItem(1);      
        });
    }

    private void RefushLevelMenuItem(List<CategoryLevel> categoryData)
    {
        int uiDataCount = categoryData.Count;
        int itemCount = _levelItem.Count;
        if (uiDataCount >= itemCount)
        {
            for (int i = 0; i < uiDataCount; i++)
            {
                if (i < itemCount)
                {
                    SetLevelMenuItemData(_levelItem[i], categoryData[i]);
                    _levelItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempLevelMenuItem.SetActive(true);
                    Transform item = UITools.AddChild(_levelMenuGrid, _tempLevelMenuItem);
                    _tempLevelMenuItem.SetActive(false);
                    item.name = i.ToString();
                    PitchCategoryItem levelItem = item.GetComponent<PitchCategoryItem>();
                    levelItem.Init(ParentUI);
                    _levelItem.Add(levelItem);
                    SetLevelMenuItemData(_levelItem[i], categoryData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < uiDataCount)
                {
                    SetLevelMenuItemData(_levelItem[i], categoryData[i]);
                }
                else
                {
                    _levelItem[i].gameObject.SetActive(false);
                }
            }
        }
        _levelMenuUiGrid.Reposition();
    }

    private void SetLevelMenuItemData(PitchCategoryItem item, CategoryLevel data)
    {
        item.Name = data.categoryName;
        item.IconObj.SetActive(false);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _levelMenu.SetActive(false);
            _menuTitle.text = data.categoryName;
            thirdCategory = data.category;
            _currentCategoryData = FindCategoryData(firstCategory, secondCategory, thirdCategory);
            if (_currentCategoryData.Count == 0)
            {
                _pageIndex.text = "1/1";
            }
            else
            {
                totalIndexPage = (_currentCategoryData.Count + _Num - 1) / _Num;
                _pageIndex.text = String.Format("1/{0}", totalIndexPage);
            }
            RefushPithGoodsItem(1);

            _selectPitchData = null;
            _goodsItemBg.SetActive(false);
        });
    }

    private List<PitchData> FindCategoryData(Int32 first, Int32 second, Int32 third)
    {
        List<PitchData> categoryData = new List<PitchData>();
        foreach (PitchData data in _publicData)
        {
            if (data.firstCategory == first)
            {
                if (data.secondCategory == second)
                {
                    if (third == 0)
                    {
                        categoryData.Add(data);
                    }
                    else
                    {
                        if (data.thirdCategory == third)
                        {
                            categoryData.Add(data);
                        }
                    }
                }
            }
        }
        return categoryData;
    }

    private void RefushPithGoodsItem(int index)
    {
        int j = 0;
        if (_currentCategoryData.Count >= _Num * index)
        {
            //数据从0开始算，UI从1开始算，对应Lebel的描述
            index--;
            for (int i = _Num * index; i < _currentCategoryData.Count; i++)
            {
                if (i < _Num * (index + 1))
                {
                    SetPitchGoodsItemData(_goodItem[j], _currentCategoryData[i]);
                    _goodItem[j].gameObject.SetActive(true);
                    j++;
                }
            }
        }
        else
        {
            index--;
            for (int i = _Num * index; i < _Num * (index + 1); i++)
            {
                if (i < _currentCategoryData.Count)
                {
                    SetPitchGoodsItemData(_goodItem[j], _currentCategoryData[i]);
                    _goodItem[j].gameObject.SetActive(true);
                }
                else
                {
                    _goodItem[j].gameObject.SetActive(false);
                }
                j++;
            }
        }
    }

    private void SetPitchGoodsItemData(PitchPropItem item, PitchData data)
    {
        int goodsId = (int)(data.tradeId / 1000);
        int level = (int)(data.tradeId % 1000);
        item.Level = Convert.ToString(level);
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(goodsId);
        if (itemData == null)
        {
            Printer.LogWarning("Items dont't have  ItemID:" + goodsId);
        }
        ParentUI.SetDynamicSpriteName(item.Icon, itemData.icon);
        item.Name = itemData.name;
        if (data.treasure == 1)
        {
            item.SignObj.SetActive(true);
        }
        else
        {
            item.SignObj.SetActive(false);
        }
        item.Price = Convert.ToString(data.tradePrice);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _goodsItemBg.transform.parent = item.transform;
            _goodsItemBg.transform.localPosition = Vector3.zero;
            _goodsItemBg.SetActive(true);
            _selectPitchData = data;
        });
        DateTime now = DateTime.Now;
        DateTime end = new DateTime(1970, 1, 1).AddSeconds(data.publicValidTime);
        TimeSpan span = end - now;
        item.Time = string.Format("{0}:{1}", span.Hours, span.Minutes);
    }

    private void SetPageIndex()
    {
        if (_currentCategoryData.Count == 0)
        {
            _pageIndex.text = "1/1";
        }
        else
        {
            totalIndexPage = (_currentCategoryData.Count + _Num - 1) / _Num;
            _pageIndex.text = String.Format("1/{0}", totalIndexPage);
        }
    }

    private void UpdatePitchPublicGoods(object value)
    {
        _publicData = _avatar.pitchModule.GetPublicSellData();

        if (!Active) return;
        _currentCategoryData = FindCategoryData(firstCategory, secondCategory, thirdCategory);

        SetPageIndex();

        _currentCategoryData = _avatar.pitchModule.SortPitchData(_currentCategoryData, sortState);
        RefushPithGoodsItem(1);

        indexPage = 1;
    }
}
