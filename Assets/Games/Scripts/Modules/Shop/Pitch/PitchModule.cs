﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

using TRADE_SELL_ITEM = System.Collections.Generic.Dictionary<string, object>;

public class PitchUIData
{
    public int category;
    public string categoryName;
    public string categoryIcon;
    public int categoryPriority;
    public int isPublicOption;
    public Dictionary<string, CategoryItem> subCategory = new Dictionary<string,CategoryItem>();
}

public class CategoryItem
{
    public int category;
    public string categoryName;
    public string categoryIcon;
    public int categoryPriority;
    public Dictionary<string, CategoryLevel> subCategory = new Dictionary<string, CategoryLevel>();
}

public class CategoryLevel
{
    public int category;
    public string categoryName;
    public string categoryIcon;
    public int categoryPriority;
    public string subCategory;
}

//可以摆摊出售的物品数据
public class CanPitchSellData
{
    public Int64 id;
    public int firstCategory;
    public int secondCategory;
    public int thirdCategory;
    public int treasure;
}

// 摆摊物品数据
public class PitchData
{
    public Int64 tradeUid;
    public UInt64 avatarDbid;
    public Int32 firstCategory;
    public Int32 secondCategory;
    public Int32 thirdCategory;
    public SByte treasure;
    public Int64 tradeId;
    public Int32 tradePrice;
    public Int64 publicValidTime;
    public UInt64 buyerDbid;
    public SByte tradeState;

    public PitchData(Dictionary<string, object> itemData)
    {
        tradeUid = (Int64)itemData["tradeUid"];
        avatarDbid = (UInt64)itemData["avatarDbid"];
        firstCategory = (Int32)itemData["firstCategory"];
        secondCategory = (Int32)itemData["secondCategory"];
        thirdCategory = (Int32)itemData["thirdCategory"];
        treasure = (SByte)itemData["treasure"];
        tradeId = (Int64)itemData["tradeId"];
        tradePrice = (Int32)itemData["tradePrice"];
        publicValidTime = (Int64)itemData["publicValidTime"];
        buyerDbid = (UInt64)itemData["buyerDbid"];
        tradeState = (SByte)itemData["tradeState"];
    }
}

public class PitchModule
{
    private KBEngine.Avatar _avatar;
    private Dictionary<string, PitchUIData> _pitchUIData = new Dictionary<string, PitchUIData>();  //UI数据

    private Dictionary<string, CanPitchSellData> _canSellData = new Dictionary<string, CanPitchSellData>(); //可以摆摊出售的数据

    private Dictionary<Int64, PitchData> _publicSellData = new Dictionary<Int64, PitchData>();  //公示物品数据
    private Dictionary<Int64, PitchData> _tradeSellData = new Dictionary<Int64, PitchData>();   //购买数据
    
    public PitchModule(KBEngine.Avatar avatar)
    {
        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_trade_category");
        _pitchUIData = JsonMapper.ToObject<Dictionary<string, PitchUIData>>(textdata.text);

        textdata = AssetsManager.instance.SyncSpawnConfig("d_trade_datas");
        _canSellData = JsonMapper.ToObject<Dictionary<string, CanPitchSellData>>(textdata.text);

        _avatar = avatar;
    }

    public List<PitchUIData> GetPitchUIData()
    {
        return new List<PitchUIData>(_pitchUIData.Values);
    }

    public List<PitchData> GetTradeSellData()
    {
        return new List<PitchData>(_tradeSellData.Values);
    }

    public List<PitchData> GetPublicSellData()
    {
        return new List<PitchData>(_publicSellData.Values);
    }

    public List<CanPitchSellData> GetCanSellData()
    {
        return new List<CanPitchSellData>(_canSellData.Values);
    }

    /// <summary>
    /// 接收公示物品数据
    /// </summary>
    public void ReceivePublicSellItemData(TRADE_SELL_ITEM itemData)
    {
        PitchData data = new PitchData(itemData);
        _publicSellData[data.tradeUid] = data;

        EventManager.Invoke<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, null);
    }

    /// <summary>
    /// 接受出售数据
    /// </summary>
    public void ReceiveTradeSellItemData(TRADE_SELL_ITEM itemData)
    {
        PitchData data = new PitchData(itemData);
        _tradeSellData[data.tradeUid] = data;

        EventManager.Invoke<object>(EventID.EVNT_PITCH_TRADE_UPDATE, null);
    }

    /// <summary>
    /// 删除公示物品
    /// </summary>
    /// <param name="tradeUid"></param>
    public void RemovePublicSellItem(Int64 tradeUid)
    {
        if(_publicSellData.ContainsKey(tradeUid))
        {
            _publicSellData.Remove(tradeUid);
        }
        else
        {
            Printer.Log("PitchModule::RemovePublicSellItem Romve TradeUid: " + tradeUid + "Don't have");
        }

        EventManager.Invoke<object>(EventID.EVNT_PITCH_PUBLIC_UPDATE, null);
    }

    /// <summary>
    /// 删除出售物品
    /// </summary>
    /// <param name="tradeUid"></param>
    public void RemoveTradeSellItem(Int64 tradeUid)
    {
        if(_tradeSellData.ContainsKey(tradeUid))
        {
            _tradeSellData.Remove(tradeUid);
        }
        else
        {
            Printer.Log("PitchModule::RemoveTradeSellItem Romve TradeUid: " + tradeUid + "Don't have");
        }

        EventManager.Invoke<object>(EventID.EVNT_PITCH_TRADE_UPDATE, null);
    }


    /// <summary>
    /// 购买摆摊里的物品
    /// </summary>
    /// <param name="tradeUid"></param>
    public void BuyTradeSellItem(Int64 tradeUid)
    {
        _avatar.cellCall("buyTradeItem", new object[] {tradeUid});
    }

    /// <summary>
    /// 上架摆摊
    /// </summary>
    /// <param name="orderId"></param>
    /// <param name="price"></param>
     public void SellTradeItem(Int16 orderId, Int32 price)
    {
        _avatar.cellCall("sellTradeItem", new object[] {orderId, price});
    }

    /// <summary>
    /// 对摆摊数据的价格进行排序
    /// </summary>
    /// <param name="publicData"></param>
     /// <param name="state">0：从低到高，1：从高到低</param>
    /// <returns></returns>
    public List<PitchData> SortPitchData(List<PitchData> publicData, int state)
     {
         if (publicData == null)
         {
             return null;
         }
         publicData.Sort(delegate(PitchData x, PitchData y)
         {
             if(state == 0)
             {
                 return x.tradePrice.CompareTo(y.tradePrice);
             }
             else
             {
                 return y.tradePrice.CompareTo(x.tradePrice);
             }
         });
         return publicData;
     }
}
