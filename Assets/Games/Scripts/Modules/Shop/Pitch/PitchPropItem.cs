﻿using UnityEngine;
using System.Collections;

public class PitchPropItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value.ToString();
        }
    }

    private UILabel _price;
    public string Price
    {
        set
        {
            _price.text = value.ToString();
        }
    }

    private UISprite _coin;
    public string Coin
    {
        set
        {
            _coin.spriteName = value.ToString();
        }
    }

    private UISprite _sign;

    public GameObject SignObj
    {
        get
        {
            return _sign.gameObject;
        }
    }
    public string Sign
    {
        set
        {
            _sign.spriteName = value.ToString();
        }
    }

//     private UISprite _signBg;
//     public string SignBg
//     {
//         set
//         {
//             _sign.spriteName = value.ToString();
//         }
//     }

    private UILabel _time;
    public string Time
    {
        set
        {
            _time.text = value.ToString();
        }
    }

    public GameObject TimeObj
    {
        get
        {
            return _time.gameObject;
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _coin = transform.Find("Coin").GetComponent<UISprite>();
        _price = transform.Find("Price").GetComponent<UILabel>();
        _sign = transform.Find("Sign").GetComponent<UISprite>();
        _time = transform.Find("Time").GetComponent<UILabel>();
       // _signBg = transform.Find("Sign/SignBg").GetComponent<UISprite>();
    }
}