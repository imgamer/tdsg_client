﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

//商城出售道具
using SHOP_SELL_ITEMS = System.Collections.Generic.List<object>;
using SELL_ITEM = System.Collections.Generic.Dictionary<string, object>;


//商城出售道具数据
public class ShopSellItemData
{
    public Int32 id { get; private set; }
    public SByte type { get; private set; }
    public string startTime { get; private set; }
    public string overTime { get; private set; }
    public Int32 itemId { get; private set; }
    public Int32 priority { get; private set; }
    public string description { get; private set; }
    public SByte costType { get; private set; }
    public Int32 costCount { get; private set; }
    public float discount { get; private set; }
    public SByte purchasingLimit { get; private set; }
    public Int32 countLimit { get; private set; }
    public void SetId(Int32 value)
    {
        id = value;
    }
    public void SetType(SByte value)
    {
        type = value;
    }
    public void SetStartTime(string value)
    {
        startTime = value;
    }
    public void SetOverTime(string value)
    {
        overTime = value;
    }
    public void SetItemId(Int32 value)
    {
        itemId = value;
    }
    public void SetPriority(Int32 value)
    {
        priority = value;
    }
    public void SetDescription(string value)
    {
        description = value;
    }
    public void SetCostType(SByte value)
    {
        costType = value;
    }
    public void SetCostCount(Int32 value)
    {
        costCount = value;
    }
    public void SetDiscount(float value)
    {
        discount = value;
    }
    public void SetPurchasingLimit(SByte value)
    {
        purchasingLimit = value;
    }
    public void SetCountLimit(Int32 value)
    {
        countLimit = value;
    }
}
public class ShopModule 
{
    private KBEngine.Avatar _avatar;
    private Dictionary<SByte, List<ShopSellItemData>> _shopSellItemDatas = new Dictionary<SByte, List<ShopSellItemData>>();

    public ShopModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    public List<ShopSellItemData>  GetShopSellItemData(SByte type)
    {
        List<ShopSellItemData> itemData = new List<ShopSellItemData>();
        _shopSellItemDatas.TryGetValue(type, out itemData);
        return itemData;
    }

    public void ReceiveSellItems(SByte sellType, SHOP_SELL_ITEMS sellItemDatas)
    {
        if (_shopSellItemDatas.ContainsKey(sellType))
        {
            _shopSellItemDatas.Remove(sellType);
        }

        ShopSellItemData shopSellItemData = null;
        List<ShopSellItemData> tempShopDatas = new List<ShopSellItemData>();
        foreach (object sellItemData in sellItemDatas)
        {
            shopSellItemData = new ShopSellItemData();
            foreach (string key in ((SELL_ITEM)sellItemData).Keys)
            {
                object value = ((SELL_ITEM)sellItemData)[key];

                MethodInfo setMethod = shopSellItemData.GetType().GetMethod("Set" + key,
                                                                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.IgnoreCase,
                                                                null,
                                                                new Type[] { value.GetType() },
                                                                null);
                if (setMethod != null)
                {
                    setMethod.Invoke(shopSellItemData, new object[] { value });
                }
                else
                {
                    Printer.LogError("Can't find property setter Set" + key);
                }
            }
            tempShopDatas.Add(shopSellItemData);
        }
        _shopSellItemDatas[sellType] = tempShopDatas;
    }

    /// <summary>
    /// 发动购买请求
    /// </summary>
    /// <param name="id">物品ID</param>
    /// <param name="count">物品数量</param>
    public void BuyShopSellItem(Int32 id, Int32 count)
    {
        _avatar.cellCall("buyShopSellItem", new object[] { id, count });
    }
}
