﻿using UnityEngine;
using System.Collections;
using System;

public class ShopItem : WinItem
{
    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }

    private UILabel _name;
    public String Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }
    private UISprite _coin;
    public String Coin
    {
        set
        {
            _coin.spriteName = value.ToString();
        }
    }

    private UILabel _price;
    public String Price
    {
        set
        {
            _price.text = value.ToString();
        }
    }

    private UILabel _discount;
    public UILabel Discount
    {
        get
        {
            return _discount;
        }
    }

    private GameObject _discountBg;
    public GameObject DiscountBg
    {
        get
        {
            return _discountBg;
        }
    }
    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _coin = transform.Find("Coin").GetComponent<UISprite>();
        _price = transform.Find("Price").GetComponent<UILabel>();
        _discount = transform.Find("Discount").GetComponent<UILabel>();
        _discountBg = transform.Find("DiscountBG").gameObject;
        _discount.gameObject.SetActive(false);
        _discountBg.gameObject.SetActive(false);
    }
}
