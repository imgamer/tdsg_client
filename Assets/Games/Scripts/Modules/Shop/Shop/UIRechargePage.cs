﻿using UnityEngine;
using System.Collections;

public class UIRechargePage : UIWin
{
    private KBEngine.Avatar _avatar;

    protected override void OnInit()
    {

        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }


    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
