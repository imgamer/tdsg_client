﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class UIShopPage : UIWin 
{
    private KBEngine.Avatar _avatar;

    private List<ShopItem> _shopItem = new List<ShopItem>();
    private const SByte FRESH = 1;
    private const SByte INTENSIFY = 2;
    private const SByte QUOTA = 3;

    private ShopSellItemData _buyData; //当前购买物品的数据
    private Int32 _buyCount;  //购买数量
    private bool _canBuy = false;  //是否可以购买

    private Action onConfirm;
    private Action onCancel;
    protected override void OnInit()
    {
        InitPage();

        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        EventManager.AddListener<Int32>(EventID.EVNT_GOLD_UPDATE, UpdateGold);
        EventManager.AddListener<Int32>(EventID.EVNT_INGOT_UPDATE, UpdateIngot);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _buyData = null;
        //界面默认显示金币数量
        _have.text = Convert.ToString(_avatar.Gold);
        _cost.text = "0";
        _input.value  = "0";
        _costCoin.spriteName = "UI-s2-common-jinbi";
        _haveCoin.spriteName = "UI-s2-common-jinbi";
        _itemBackGround.SetActive(false);
        _disCountTip.SetActive(false);
        _time.gameObject.SetActive(false);
        _canBuy = false;

        _btnBackGround.transform.parent = freshBtn.transform;
        _btnBackGround.transform.localPosition = Vector3.zero;
        _btnBackGround.SetActive(true);
        RefreshItem(FRESH);
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<Int32>(EventID.EVNT_GOLD_UPDATE, UpdateGold);
        EventManager.RemoveListener<Int32>(EventID.EVNT_INGOT_UPDATE, UpdateIngot);
    }

    private UI.Grid _uiGrid;
    private Transform _grid;
    private GameObject _tempItem;

    private UILabel _name;
    private UILabel _desc;
    private UILabel _cost;
    private UILabel _have;
    private UILabel _time;
    private UISprite _costCoin;
    private UISprite _haveCoin;
    private GameObject _itemBackGround;
    private GameObject _btnBackGround;
    private UIInput _input;
    private GameObject _disCountTip;

    GameObject freshBtn;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _grid = center.Find("Scroll/Grid");
        _uiGrid = _grid.GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        _name = center.Find("Name").GetComponent<UILabel>();
        _desc = center.Find("Desc").GetComponent<UILabel>();
        _cost = center.Find("Buy/Cost/Cost").GetComponent<UILabel>();
        _have = center.Find("Buy/Have/Have").GetComponent<UILabel>();
        _time = center.Find("Time").GetComponent<UILabel>();
        _time.gameObject.SetActive(false);
        _costCoin = center.Find("Buy/Cost/Coin").GetComponent<UISprite>();
        _haveCoin = center.Find("Buy/Have/Coin").GetComponent<UISprite>();
        _itemBackGround = center.Find("ItemBackGround").gameObject;
        _itemBackGround.SetActive(false);
        _btnBackGround = center.Find("BtnBackGround").gameObject;
        _btnBackGround.SetActive(false);
        _disCountTip = center.Find("DisCountTip").gameObject;
        _disCountTip.SetActive(false);

        _input = center.Find("Buy/Count/Input").GetComponent<UIInput>();
        EventDelegate.Add(_input.onChange, OnInputChange);

        GameObject countSubBtn = center.Find("Buy/Count/SubBtn").gameObject;
        InputManager.instance.AddClickListener(countSubBtn, (go) =>
        {
            int count = Convert.ToInt32(_input.value );
            if(count <= 1)
            {
                return;
            }
            count--;
            _input.value  = Convert.ToString(count);
            _buyCount = count;
            RefreshBuyLabel();
        });

        GameObject countAddBtn = center.Find("Buy/Count/AddBtn").gameObject;
        InputManager.instance.AddClickListener(countAddBtn, (go) =>
        {
            int count = Convert.ToInt32(_input.value );
            if(count == 99)
            {
                return;
            }
            count++;
            _input.value  = Convert.ToString(count);
            _buyCount = count;
            RefreshBuyLabel();
        });

        GameObject buyBtn = center.Find("BuyBtn").gameObject;
        InputManager.instance.AddClickListener(buyBtn, (go) =>
        {         
            if(!_canBuy)
            {
                if (_buyData == null)
                {
                    return;
                }

                if(_buyData.costType  == 1)
                {
                    TipManager.instance.ShowTwoButtonTip(10102, "仙玉", /*() => { }*/null, null);
                }
                else
                {
                    TipManager.instance.ShowTwoButtonTip(10102, "金币", null, null);
                }
            }
            else
            {
                _avatar.shopModule.BuyShopSellItem(_buyData.id, _buyCount);
                RefreshBuyLabel();
            }           
        });

        freshBtn = center.Find("FreshBtn").gameObject;
        InputManager.instance.AddClickListener(freshBtn, (go) =>
        {
            _btnBackGround.transform.parent = freshBtn.transform;
            _btnBackGround.transform.localPosition = Vector3.zero;
            RefreshItem(FRESH);
            _itemBackGround.SetActive(false);
            _time.gameObject.SetActive(false);
            _buyData = null;
            _canBuy = false;
        });

        GameObject intensifyBtn = center.Find("IntensifyBtn").gameObject;
        InputManager.instance.AddClickListener(intensifyBtn, (go) =>
        {
            _btnBackGround.transform.parent = intensifyBtn.transform;
            _btnBackGround.transform.localPosition = Vector3.zero;
            RefreshItem(INTENSIFY);
            _itemBackGround.SetActive(false);
            _time.gameObject.SetActive(false);
            _buyData = null;
            _canBuy = false;
        });

        GameObject quotaBtn = center.Find("QuotaBtn").gameObject;
        InputManager.instance.AddClickListener(quotaBtn, (go) =>
        {
            _btnBackGround.transform.parent = quotaBtn.transform;
            _btnBackGround.transform.localPosition = Vector3.zero;
            RefreshItem(QUOTA);
            _itemBackGround.SetActive(false);
            _buyData = null;
            _canBuy = false;
        });
    }

    private void RefreshItem(SByte type)
    {
        List<ShopSellItemData> itemData = _avatar.shopModule.GetShopSellItemData(type);

        int dataCount = itemData.Count;
        if (type == QUOTA)
        {
            SetQuotaTime(); 
        }
        int itemCount =_shopItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    //设数据
                    SetItemData(_shopItem[i], itemData[i]);
                    _shopItem[i].gameObject.SetActive(true);
                }
                else
                {
                    //创建Item设数据
                    _tempItem.SetActive(true);
                    Transform item = UITools.AddChild(_grid, _tempItem);
                    _tempItem.SetActive(false);
                    item.name = i.ToString();
                    ShopItem shopItem = item.GetComponent<ShopItem>();
                    shopItem.Init(this);
                    _shopItem.Add(shopItem);
                    SetItemData(shopItem, itemData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    //设数据
                    SetItemData(_shopItem[i], itemData[i]);
                }
                else
                {
                    //隐藏多余的
                    _shopItem[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(ShopItem item, ShopSellItemData data)
    {
        ItemsData iData = ItemsConfig.SharedInstance.GetItemData(data.itemId);
        if(iData == null)
        {
            Printer.LogWarning("ItemsConfig  item ID:" + data.itemId + "not found");
            return;
        }
        this.SetDynamicSpriteName(item.Icon, iData.icon);
        item.Name = iData.name;
        if(data.costType ==1) //仙玉
        {
            item.Coin = "UI-s2-common-huobi2";
        }
        else
        {
            item.Coin = "UI-s2-common-jinbi";
        }
        int price = (int)(data.discount / 10 * data.costCount);
        item.Price = Convert.ToString(price);
        if(data.discount != 10)
        {
            item.DiscountBg.SetActive(true);
            item.Discount.text = Convert.ToString(data.discount);
            item.Discount.gameObject.SetActive(true);
            _disCountTip.SetActive(true);
        }
        else
        {
            item.DiscountBg.SetActive(false);
            item.Discount.gameObject.SetActive(false);
            _disCountTip.SetActive(false);
        }

        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _itemBackGround.transform.parent = item.transform;
            _itemBackGround.transform.localPosition = Vector3.zero;
            _itemBackGround.SetActive(true);

            _cost.color = Color.white;

            _name.text = iData.name;
            _desc.text = data.description;

            //默认购买一个
            _buyCount = 1;
            _buyData = data;
            _canBuy = true;
            
            _input.value  = Convert.ToString(_buyCount);
            _cost.text = Convert.ToString(price);

            if (data.costType == 1) //仙玉
            {
                _costCoin.spriteName = "UI-s2-common-huobi2";
                _haveCoin.spriteName = "UI-s2-common-huobi2";
                _have.text = Convert.ToString(_avatar.Fairyjade);
                if(price > _avatar.Fairyjade)
                {
                    _cost.color = Color.red;
                    _canBuy = false;
                }
            }
            else
            {
                _costCoin.spriteName = "UI-s2-common-jinbi";
                _haveCoin.spriteName = "UI-s2-common-jinbi";
                _have.text = Convert.ToString(_avatar.Gold);
                if (price > _avatar.Gold)
                {
                    _cost.color = Color.red;
                    _canBuy = false;
                }
            }
        });
    }
   
    private void OnInputChange()
    {
        if (_buyData == null)
        {
            return;
        }
        if(Convert.ToInt32(_input.value) > 99)
        {
            TipManager.instance.ShowTextTip(10103);
            _input.value = "99";
        }

        if (String.IsNullOrEmpty(_input.value) || Convert.ToInt32(_input.value) == 0)
        {
            _input.value = "1";
        }

        _buyCount = Convert.ToInt32(_input.value);
        RefreshBuyLabel();
    }

    private void RefreshBuyLabel()
    {
        if(_buyData == null)
        {
            return;
        }
        int cost = (int)(_buyData.discount / 10 * _buyData.costCount * _buyCount);
        _cost.text = Convert.ToString(cost);
        if (cost > Convert.ToInt32(_have.text))
        {
            _cost.color = Color.red;
            _canBuy = false;
        }
        else
        {
            _cost.color = Color.white;
            _canBuy = true;
        }
    }

    private void SetQuotaTime()
    {
        _time.gameObject.SetActive(true);
        DateTime  dt = DateTime.Now;
        int i = dt.DayOfWeek - DayOfWeek.Monday;
        if (i != 0)
        {
            i = 7 - i; 
        }
        DateTime next = dt.AddDays(i);
        next = DateTime.Parse(next.ToString("yyyy-MM-dd"));
        TimeSpan ts = next - dt;
        _time.text = string.Format("{0}天{1}小时", ts.Days, ts.Hours);
    }

    #region Message
    private void UpdateGold(Int32 gold)
    {
        if (!Active) return;
        _have.text = Convert.ToString(gold);
    }

    private void UpdateIngot(Int32 fairyjade)
    {
        if (!Active) return;
        _have.text = Convert.ToString(fairyjade);
    }
    #endregion
}
