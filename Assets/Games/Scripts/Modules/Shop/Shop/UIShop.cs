﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIShop : UIWin
{
    private GameObject _btnBg;
    private GameObject commerceBtn;
    private GameObject pitchBtn;
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        _btnBg = center.Find("BtnBG").gameObject;
        _btnBg.SetActive(false);
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(WinID.UIShopPage);
            UIManager.instance.CloseWindow(WinID.UIPitchPage);
            UIManager.instance.CloseWindow(WinID.UIRechargePage);
            UIManager.instance.CloseWindow(WinID.UICommercePage);
            UIManager.instance.CloseWindow(winID);
        });

        commerceBtn = center.Find("CommerceBtn").gameObject;
        InputManager.instance.AddClickListener(commerceBtn, (go) =>
        {
            _btnBg.transform.parent = commerceBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            UIManager.instance.CloseWindow(WinID.UIShopPage);
            UIManager.instance.CloseWindow(WinID.UIPitchPage);
            UIManager.instance.CloseWindow(WinID.UIRechargePage);

            UIManager.instance.OpenWindow(this, WinID.UICommercePage);  
        });
        pitchBtn = center.Find("PitchBtn").gameObject;
        InputManager.instance.AddClickListener(pitchBtn, (go) =>
        {
            _btnBg.transform.parent = pitchBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            UIManager.instance.CloseWindow(WinID.UIShopPage);
            UIManager.instance.CloseWindow(WinID.UIRechargePage);
            UIManager.instance.CloseWindow(WinID.UICommercePage);

            UIManager.instance.OpenWindow(this, WinID.UIPitchPage);
        });
        GameObject shopBtn = center.Find("ShopBtn").gameObject;
        InputManager.instance.AddClickListener(shopBtn, (go) =>
        {
            _btnBg.transform.parent = shopBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            UIManager.instance.CloseWindow(WinID.UIPitchPage);
            UIManager.instance.CloseWindow(WinID.UIRechargePage);
            UIManager.instance.CloseWindow(WinID.UICommercePage);

            UIManager.instance.OpenWindow(this, WinID.UIShopPage);
        });
        GameObject rechargeBtn = center.Find("RechargeBtn").gameObject;
        InputManager.instance.AddClickListener(rechargeBtn, (go) =>
        {
            _btnBg.transform.parent = rechargeBtn.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            UIManager.instance.CloseWindow(WinID.UIShopPage);
            UIManager.instance.CloseWindow(WinID.UIPitchPage);
            UIManager.instance.CloseWindow(WinID.UICommercePage);

            UIManager.instance.OpenWindow(this, WinID.UIRechargePage);
        });
    }

    protected override void OnOpen(params object[] args)
    {
        _btnBg.SetActive(true);
        _btnBg.transform.parent = commerceBtn.transform;
        _btnBg.transform.localPosition = Vector3.zero;
        UIManager.instance.OpenWindow(this, WinID.UICommercePage);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
