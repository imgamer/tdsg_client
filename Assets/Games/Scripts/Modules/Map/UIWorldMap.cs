﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 世界地图界面
/// </summary>
using System.Collections.Generic;


public class UIWorldMap : UIWin
{


    protected override void OnInit()
    {
        InitPage();
     }
    protected override void OnOpen(params object[] args)
    {
        
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject findBtn = center.Find("FindBtn").gameObject;
        InputManager.instance.AddClickListener(findBtn, (go) =>
        {
            
        });

        GameObject detailBtn = center.Find("DetailBtn").gameObject;
        InputManager.instance.AddClickListener(detailBtn, (go) =>
        {
            UIManager.instance.OpenWindow(this, WinID.UIWorldDetailMap);
            UIManager.instance.CloseWindow(winID);
        });

        Transform scene = center.Find("Scene");
        GameObject diancangpai = scene.Find("DianCangPai").gameObject;
        InputManager.instance.AddClickListener(diancangpai, (go) =>
        {
            EnterScene(SceneDefine.SCENE_DIANCANGPAI);
        });

        GameObject tianxinge = scene.Find("TianXinGe").gameObject;
        InputManager.instance.AddClickListener(tianxinge, (go) =>
        {
            EnterScene(SceneDefine.SCENE_TIANXINGE);
        });

        GameObject taoyuanzhen = scene.Find("TaoYuanZhen").gameObject;
        InputManager.instance.AddClickListener(taoyuanzhen, (go) =>
        {
            EnterScene(SceneDefine.SCENE_TAOYUANZHEN);
        });

        GameObject jiuhuashan = scene.Find("JiuHuaShan").gameObject;
        InputManager.instance.AddClickListener(jiuhuashan, (go) =>
        {
            EnterScene(SceneDefine.SCENE_JIUHUASHAN);
        });

        GameObject sanxiandao = scene.Find("SanXianDao").gameObject;
        InputManager.instance.AddClickListener(sanxiandao, (go) =>
        {
            EnterScene(SceneDefine.SCENE_SANXIANDAO);
        });

        GameObject fengshentai = scene.Find("ZiDianMen").gameObject;
        InputManager.instance.AddClickListener(fengshentai, (go) =>
        {
            EnterScene(SceneDefine.SCENE_FENGSHENTAI);
        });

        GameObject guaji = scene.Find("QiShan").gameObject;
        InputManager.instance.AddClickListener(guaji, (go) =>
        {
            EnterScene(SceneDefine.SCENE_SANXIANDAO_OLD);
        });
    }

    private void EnterScene(int sceneID)
    {
        GameMain.Player.cellCall("smallMapTeleportSpace", new object[] { sceneID });
    }
}
