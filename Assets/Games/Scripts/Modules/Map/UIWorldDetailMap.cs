﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UIWorldDetailMap : UIWin
{

    private List<WorldNpcItem> _npcItems = new List<WorldNpcItem>();
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {     
        RefreshNPCList();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private GameObject _npcList;

    private UI.Grid _uiGrid;
    private GameObject _tempItem;

 	private GameObject _miniMap;
 	private GameObject _miniMapOrigin;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _npcList = center.Find("NPCList").gameObject;
        _uiGrid = center.Find("NPCList/Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("NPCList/Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);
        _npcList.SetActive(false);

        GameObject npcBtn = center.Find("NpcBtn").gameObject;
        InputManager.instance.AddClickListener(npcBtn, (go) =>
        {
            if(_npcList.activeSelf)
            {
                _npcList.SetActive(false);
            }
            else
            {
                _npcList.SetActive(true);
            }
        });

        GameObject returnBtn = center.Find("ReturnBtn").gameObject;
        InputManager.instance.AddClickListener(returnBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            UIManager.instance.BackToWindow(this);
        });

        _miniMap = center.Find("MiniMap").gameObject;
        _miniMapOrigin = center.Find("MiniMap/Origin").gameObject;
        var box = _miniMap.GetComponent<BoxCollider>();
        InputManager.instance.AddClickListener(_miniMap, (go) =>
        {
            var pos = UICamera.currentCamera.ScreenToWorldPoint(Input.mousePosition);
            var offset = _miniMapOrigin.transform.position;
            pos -= offset;

            var px = pos.x / (box.bounds.size.x);
            var py = pos.y / (box.bounds.size.y);

            var se = GameMain.Player.SceneEntityObj;

            if (MapDetail.instance != null && se != null)
            {
                se.Navigate.Start(BaseNavigate.NavigateType.Initiative, new Vector2(px * MapDetail.instance.MapWidth, py * MapDetail.instance.MapHeight), 0f);
            }
        });
    }

    private void RefreshNPCList()
    {
        _npcList.SetActive(true);
        List<SceneEntity> npcData = SceneManager.instance.CurrentScene.GetEntitiesByType(EntityType.NPCEntity);
        int dataCount = npcData.Count;
        int itemCount = _npcItems.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_npcItems[i], npcData[i]);
                    _npcItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    _tempItem.SetActive(false);
                    WorldNpcItem npcItem = item.GetComponent<WorldNpcItem>();
                    npcItem.Init(this);
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _npcItems.Add(npcItem);
                    SetItemData(npcItem, npcData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_npcItems[i], npcData[i]);
                }
                else
                {
                    _npcItems[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
        _npcList.SetActive(false);
    }

    private void SetItemData(WorldNpcItem item, SceneEntity data)
    {
        item.Name = data.KBEntity.Name;

        NpcLoaclPosVector2 posVector2 = NpcLocalPositonConfig.SharedInstance.GetNpcLocalPositon(SceneManager.instance.CurrentSceneType, data.KBEntity.UType);
        if (posVector2 == null)
        {
            return;
        }
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            GameMain.Player.spaceNavigator.Navigate(SceneManager.instance.CurrentSceneType, posVector2.ToVector3(), true);
        });
    }
}
