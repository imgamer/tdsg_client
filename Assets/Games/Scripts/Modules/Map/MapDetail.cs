﻿using UnityEngine;
using System.Collections;

public class MapDetail : MonoBehaviour 
{
    public static MapDetail instance { get; private set; }
    public float LeftX { get; private set; }
    public float RightX { get; private set; }
    public float TopY { get; private set; }
    public float BottomY { get; private set; }
	public float MapWidth;
	public float MapHeight;

    public Transform SpawnPoint;
    public Transform[] TransportPoints;

	void Awake () 
    {
        instance = this;
        LeftX = -MapWidth * 0.5f;
        TopY = MapHeight * 0.5f;
        RightX = MapWidth + LeftX;
        BottomY = TopY - MapHeight;
	}

}
