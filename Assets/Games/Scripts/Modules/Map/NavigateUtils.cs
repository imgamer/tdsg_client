﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 寻路导航插件封装
/// </summary>
public class NavigateUtils 
{
    public static bool Valid
    {
        get { return NavMesh2D.GetNavMeshObject() != null; }
    }
    /// <summary>
    /// 获取路径点
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <param name="restrictDist"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetPath(Vector2 startPosition, Vector2 endPosition, float restrictDist, out float length)
    {
        List<Vector2> path = NavMesh2D.GetPath(startPosition, endPosition);
        int count = path.Count;
        List<Vector3> ret = new List<Vector3>(count);
        length = -restrictDist;
        for (int i = 0; i < count; i++)
        {
            Vector2 temp = path[i];
            if (i < count - 1)
            {
                length += Vector2.Distance(path[i + 1], temp);
            }
            ret.Add(new Vector3(temp.x, temp.y, temp.y));
        }
        DisposePath(ref ret, restrictDist);
        return ret;
    }
    /// <summary>
    /// 获取路径点
    /// </summary>
    /// <param name="positions"></param>
    /// <param name="restrictDist"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetPath(List<Vector2> positions, float restrictDist, out float length)
    {
        if (positions.Count < 2)
        {
            length = 0;
            return new List<Vector3>();
        }
        List<Vector2> path = new List<Vector2>();
        for (int i = 1; i < positions.Count; i++)
        {
            Vector2 startPosition = positions[i - 1];
            Vector2 endPosition = positions[i];
            path.AddRange(NavMesh2D.GetPath(startPosition, endPosition));
        }
        int count = path.Count;
        List<Vector3> ret = new List<Vector3>(count);
        length = -restrictDist;
        for (int i = 0; i < count; i++)
        {
            Vector2 temp = path[i];
            if (i < count - 1)
            {
                length += Vector2.Distance(path[i + 1], temp);
            }
            ret.Add(new Vector3(temp.x, temp.y, temp.y));
        }
        DisposePath(ref ret, restrictDist);
        return ret;
    }
    /// <summary>
    /// 获取平滑路径点
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <param name="restrictDist"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetSmoothedPath(Vector2 startPosition, Vector2 endPosition, float restrictDist, out float length)
    {
        List<Vector2> path = NavMesh2D.GetSmoothedPath(startPosition, endPosition);
        int count = path.Count;
        List<Vector3> ret = new List<Vector3>(count);
        length = -restrictDist;
        for (int i = 0; i < count; i++)
        {
            Vector2 temp = path[i];
            if (i < count - 1)
            {
                length += Vector2.Distance(path[i + 1], temp);
            }
            ret.Add(new Vector3(temp.x, temp.y, temp.y));
        }
        DisposePath(ref ret, restrictDist);
        return ret;
    }
    /// <summary>
    /// 获取平滑路径点
    /// </summary>
    /// <param name="positions"></param>
    /// <param name="restrictDist"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetSmoothedPath(List<Vector2> positions, float restrictDist, out float length)
    {
        if (positions.Count < 2)
        {
            length = 0;
            return new List<Vector3>();
        }
        List<Vector2> path = new List<Vector2>();
        for (int i = 1; i < positions.Count; i++)
        {
            Vector2 startPosition = positions[i - 1];
            Vector2 endPosition = positions[i];
            path.AddRange(NavMesh2D.GetSmoothedPath(startPosition, endPosition));
        }
        int count = path.Count;
        List<Vector3> ret = new List<Vector3>(count);
        length = -restrictDist;
        for (int i = 0; i < count; i++)
        {
            Vector2 temp = path[i];
            if (i < count - 1)
            {
                length += Vector2.Distance(path[i + 1], temp);
            }
            ret.Add(new Vector3(temp.x, temp.y, temp.y));
        }
        DisposePath(ref ret, restrictDist);
        return ret;
    }
    /// <summary>
    /// 根据距离获取新的路径点
    /// </summary>
    /// <param name="path"></param>
    /// <param name="restrictDist">寻路到距离目标点的距离</param>
    /// <returns></returns>
    private static void DisposePath(ref List<Vector3> path, float restrictDist)
    {
        if (Mathf.Approximately(restrictDist, 0.0f)) return;
        while (path.Count >= 2)
        {
            Vector3 pointA = path[path.Count - 1];
            Vector3 pointB = path[path.Count - 2];
            float dis = Vector2.Distance(pointA, pointB);
            if (dis < restrictDist)
            {
                path.RemoveAt(path.Count - 1);
                restrictDist -= dis;
            }
            else
            {
                path[path.Count - 1] = Vector3.Lerp(pointA, pointB, restrictDist / dis);
                break;
            }
        }
    }
    /// <summary>
    /// 随机获取除起点外的终点位置路径
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetRandomPath(Vector2 startPosition, out float length)
    {
        return GetPath(startPosition, GetRandomPosition(startPosition), 0, out length);
    }
    /// <summary>
    /// 随机获取除起点外的终点位置路径
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static List<Vector3> GetRandomSmoothedPath(Vector2 startPosition, out float length)
    {
        return GetSmoothedPath(startPosition, GetRandomPosition(startPosition), 0, out length);
    }
    /// <summary>
    /// 随机获取除起点外的终点位置
    /// </summary>
    /// <param name="startPosition"></param>
    /// <returns></returns>
    private static Vector2 GetRandomPosition(Vector2 startPosition)
    {
        int count = NavMesh2D.GetNavMeshObject().NavMesh2DNodes.Length;
        int startIndex = NavMesh2D.GetNavMeshObject().ClosestNodeIndexTo(startPosition);
        int endIndex = Random.Range(0, count);
        while (startIndex == endIndex)
        {
            endIndex = Random.Range(0, count);
        }
        return NavMesh2D.GetNavMeshObject().GetNode(endIndex).position;
    }
}
