﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// KBEApp模块
/// </summary>
public class KBEApp : MonoBehaviour
{
    public static KBEApp instance { get; private set; }

    public KBEngine.KBEngineApp gameapp = null;

    // 在unity3d界面中可见选项
    public bool isMultiThreads = false;
    public string ip = "172.16.2.234";
    public int port = 20013;
    public KBEngine.KBEngineApp.CLIENT_TYPE clientType = KBEngine.KBEngineApp.CLIENT_TYPE.CLIENT_TYPE_MINI;
    public string persistentDataPath = "Application.persistentDataPath";
    public bool syncPlayer = true;
    public int threadUpdateHZ = 10;
    public int SEND_BUFFER_MAX = (int)KBEngine.NetworkInterface.TCP_PACKET_MAX;
    public int RECV_BUFFER_MAX = (int)KBEngine.NetworkInterface.TCP_PACKET_MAX;

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Init();
    }

    private bool inited = false;
    private void Init()
    {
        if (inited) return;
        inited = true;
        ip = string.Empty;
        InstallEvents();
        InitKBEngine();
    }
    private void InstallEvents()
    {

    }

    private void InitKBEngine()
    {
        InitDbg();
        if (isMultiThreads)
            gameapp = new KBEngine.KBEngineAppThread(GetArgs());
        else
            gameapp = new KBEngine.KBEngineApp(GetArgs());
    }

    private void InitDbg()
    {
        KBEngine.Dbg.debugLevel = Main.instance.m_debuglevel;
    }

    void FixedUpdate()
    {
        KBEUpdate();
    }

    private void KBEUpdate()
    {
        if (!inited) return;
        // 单线程模式必须自己调用
        if (!isMultiThreads) gameapp.process();
        KBEngine.Event.processOutEvents();
    }

    void OnDestroy()
    {
        inited = false;
        KBEngine.KBEngineApp.app.destroy();
    }

    public void SelectIP(string loginIp)
    {
        if (string.IsNullOrEmpty(loginIp)) return;
        if (ip.Equals(loginIp)) return;
        ip = loginIp;
        gameapp.reset();
        gameapp.initialize(GetArgs());
    }

    private KBEngine.KBEngineArgs GetArgs()
    {
        // 如果此处发生错误，请查看 Assets\Scripts\kbe_scripts\if_Entity_error_use______git_submodule_update_____kbengine_plugins_______open_this_file_and_I_will_tell_you.cs
        KBEngine.KBEngineArgs args = new KBEngine.KBEngineArgs();
        args.ip = ip;
        args.port = port;
        args.clientType = clientType;

        if (persistentDataPath == "Application.persistentDataPath")
        {
            args.persistentDataPath = Application.persistentDataPath;
            Printer.LogWarning("Application.persistentDataPath:" + Application.persistentDataPath);
        }
        else
        args.persistentDataPath = persistentDataPath;

        args.syncPlayer = syncPlayer;
        args.threadUpdateHZ = threadUpdateHZ;

        args.SEND_BUFFER_MAX = (UInt32)SEND_BUFFER_MAX;
        args.RECV_BUFFER_MAX = (UInt32)RECV_BUFFER_MAX;

        args.isMultiThreads = isMultiThreads;
        args.isOnInitCallPropertysSetMethods = false;

        return args;
    }
    public void Login(string username, string password)
    {
        Printer.Log("connect to server...(连接到服务端...)");
		KBEngine.Event.fireIn("login", username, password, System.Text.Encoding.UTF8.GetBytes("tdsg"));
    }

}
