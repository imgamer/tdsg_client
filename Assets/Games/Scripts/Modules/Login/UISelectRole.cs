﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 角色选择界面
/// </summary>
public class UISelectRole : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_ROLE_NUM_UPDATE, RoleUpdate);
        InitItem();
        InitSelect();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateCurRole();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        LoginManager.instance.HideRole();
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ROLE_NUM_UPDATE, RoleUpdate);
    }

    private GameObject _roleName;
    private UILabel _level;
    private UILabel _name;

    private UISprite _school;
    private UILabel _text;
    private UILabel _desc;
    private GameObject _btnEnter;
    private UILabel _btnEnterName;
    private GameObject _randomName;
    private UIInput _inputName;
    private void InitItem()
    {
        Transform topLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopLeft);
        GameObject btnBack = topLeft.Find("BtnBack").gameObject;
        InputManager.instance.AddClickListener(btnBack, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            UIManager.instance.BackToWindow(this);
        });

        Transform right = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Right);
        _roleName = right.Find("RoleName").gameObject;
        _level = _roleName.transform.Find("Level").GetComponent<UILabel>();
        _name = _roleName.transform.Find("Name").GetComponent<UILabel>();

        Transform schoolRoot = right.Find("School");
        _school = schoolRoot.Find("Icon").GetComponent<UISprite>();
        _text = schoolRoot.Find("Text").GetComponent<UILabel>();
        _desc = schoolRoot.Find("Desc").GetComponent<UILabel>();

        _randomName = right.Find("RandomName").gameObject;
        _inputName = _randomName.transform.Find("InputName").GetComponent<UIInput>();
        GameObject btnRandom = _randomName.transform.Find("BtnRandom").gameObject;
        InputManager.instance.AddClickListener(btnRandom, (go) =>
        {
            _inputName.value = LoginManager.instance.GetRandomName(_curType);
        });
        _btnEnter = right.Find("BtnEnter").gameObject;
        _btnEnterName = _btnEnter.transform.Find("Label").GetComponent<UILabel>();
    }
    private void InitSelect()
    {
        Transform left = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Left);
        GameObject btnLeft = left.Find("BtnLeft").gameObject;
        InputManager.instance.AddClickListener(btnLeft, (go) =>
        {
            SelectRole(-1);
        });
        Transform right = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Right);
        GameObject btnRight = right.Find("BtnRight").gameObject;
        InputManager.instance.AddClickListener(btnRight, (go) =>
        {
            SelectRole(+1);
        });
    }

    private void UpdateCurRole()
    {
        SelectRole(0);
    }

    private uint _curType;
    private void SelectRole(int changeIndex)
    {
        LoginManager.instance.ShowRole(changeIndex, (type, role) => 
        {
            _curType = type;
            if (role != null)
            {
                _roleName.SetActive(true);
                _name.text = role.m_name;
                _level.text = string.Format("Lv.{0}", role.m_level);
                _randomName.SetActive(false);
                _btnEnterName.text = "进入游戏";
                InputManager.instance.AddClickListener(_btnEnter, (go) =>
                {
                    LoginManager.instance.ReqSelectAvatarForGame(role.m_dbid);
                });
            }
            else
            {
                _roleName.SetActive(false);
                _randomName.SetActive(true);
                _btnEnterName.text = "创建角色";
                _inputName.value = LoginManager.instance.GetRandomName(_curType);
                InputManager.instance.AddClickListener(_btnEnter, (go) =>
                {
                    LoginManager.instance.ReqCreateAvatar(_curType, _inputName.value);
                });
            }
            switch (_curType)
            {
                case Define.ROLE_TYPE_DIANCANG:
                    _text.text = "点苍派";
                    break;
                case Define.ROLE_TYPE_JIUHUASHAN:
                    _text.text = "九华山";
                    break;
                case Define.ROLE_TYPE_XUANQINGGONG:
                    _text.text = "玄清宫";
                    break;
                case Define.ROLE_TYPE_XIAOYAOGONG:
                    _text.text = "逍遥宫";
                    break;
                case Define.ROLE_TYPE_ZIDIANMEN:
                    _text.text = "紫电门";
                    break;
                case Define.ROLE_TYPE_TIANXINGE:
                    _text.text = "天心阁";
                    break;
                default:
                    break;
            }
        });
    }

    private void RoleUpdate(object o)
    {
        if (!Active) return;
        UpdateCurRole();
    }
}
