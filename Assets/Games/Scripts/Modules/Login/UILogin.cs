﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// 登录界面
/// </summary>
public class UILogin : UIWin
{
    private string userName;
    private string password;
    private GameObject btnLogin;
    private GameObject btnRegister;

    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_ON_LOGIN_SUCCESS, LoginSuccess);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ON_LOGIN_SUCCESS, LoginSuccess);
    }

    #region 初始化
    private UIInput userNameInput;
    private UIInput passwordInput;
    private const string UserName = "UserName";
    private const string Password = "Password";
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform inputRoot = center.Find("Input");

        userNameInput = inputRoot.Find("UserName").GetComponent<UIInput>();
        passwordInput = inputRoot.Find("Password").GetComponent<UIInput>();

        EventDelegate userNameOnChange = new EventDelegate(() =>
        {
            userName = userNameInput.value;
        });
        userNameInput.onChange.Add(userNameOnChange);

        EventDelegate passwardOnChange = new EventDelegate(() =>
        {
            password = passwordInput.value;
        });
        passwordInput.onChange.Add(passwardOnChange);

        btnLogin = center.Find("BtnLogin").gameObject;
        InputManager.instance.AddClickListener(btnLogin, (go) =>
        {
            LoginManager.instance.Login(userName, password, (ok) =>
            {
                if (ok)
                {
                    UIManager.instance.CloseWindow(winID);
                    UIManager.instance.OpenWindow(null, WinID.UIServer);
                }
                else
                {
                    Printer.LogError("登录失败");
                }
            });
        });
        GetUserInfo();
    }
    #endregion

    #region 事件
    private void LoginSuccess(object o)
    {
        SaveUserInfo();
    }
    #endregion

    #region 账号和密码记录功能
    private void GetUserInfo()
    {
        string name = PlayerPrefs.GetString(UserName, "");
        if (string.IsNullOrEmpty(name)) return;
        userNameInput.value = name;
        string key = PlayerPrefs.GetString(Password, "");
        if (string.IsNullOrEmpty(key)) return;
        passwordInput.value = key;
    }
    private void SaveUserInfo()
    {
        PlayerPrefs.SetString(UserName, userName);
        PlayerPrefs.SetString(Password, password);
    }
    #endregion
}