﻿using UnityEngine;

/// <summary>
/// 登录跳转界面
/// </summary>
public class UIBridge : UIWin
{
    private UIProgressBar _progress;
    private UILabel _text;
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_ROOM_ENTER, UpdateBar);
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _progress = center.Find("Progress").GetComponent<UIProgressBar>();
        _text = center.Find("Text").GetComponent<UILabel>();
        _progress.value = 0;
        _continue = false;
    }

    protected override void OnOpen(params object[] args)
    {
        InvokeRepeating("Loading", 1f, 0.04f);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        CancelInvoke("Loading");
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ROOM_ENTER, UpdateBar);
    }

    private bool _continue = false;
    private void Loading()
    {
        if (_progress.value >= 0.95f)
        {
            if (!_continue) return;
        }
        if (_progress.value >= 1f)
        {
            _continue = false;
            CancelInvoke("Loading");
            UIManager.instance.DestroyWindow(winID);
            return;
        }
        _progress.value += UnityEngine.Random.Range(0.01f, 0.02f);
    }

    private void UpdateBar(object size)
    {
        _continue = true;
    }
}
