﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 客户端更新提示界面
/// </summary>
public class UIDownloadInfo : UIWin
{
    private UILabel _text;
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _text = center.Find("Text").GetComponent<UILabel>();
        GameObject btnLeft = center.Find("BtnLeft").gameObject;
        InputManager.instance.AddClickListener(btnLeft, (go) =>
        {
            UIManager.instance.CloseWindow(winID);

        });
        GameObject btnRight = center.Find("BtnRight").gameObject;
        InputManager.instance.AddClickListener(btnRight, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            Application.Quit();
        });
    }

    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _text.text = string.Format(_text.text, args[0]);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
