﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 推荐服务器列表
/// </summary>
public class ServerRecommendPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItem();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private List<ServerItem> _items = new List<ServerItem>();
    private UI.Grid _uiGrid;
    private ServerItem _template;
    private void InitItem()
    {
        Transform grid = transform.Find("ScrollView/UIGrid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        _template = grid.Find("Item").GetComponent<ServerItem>();
        _template.gameObject.SetActive(false);
    }

    private void UpdateItem()
    {
        List<LoginManager.ServerData> list = LoginManager.instance.GetServerDatas();
        int dataNum = list.Count;
        int itemNum = _items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                LoginManager.ServerData data = list[i];
                if (i < itemNum)
                {
                    ServerItem item = _items[i];
                    SetServerItem(data, item, i);
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    ServerItem item = Instantiate(_template) as ServerItem;
                    _template.gameObject.SetActive(false);
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(ParentUI);
                    _items.Add(item);
                    SetServerItem(data, item, i);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                ServerItem item = _items[i];
                if (i < dataNum)
                {
                    LoginManager.ServerData data = list[i];
                    SetServerItem(data, item, i);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }
    private void SetServerItem(LoginManager.ServerData data, ServerItem item, int index)
    {
        item.gameObject.SetActive(true);
        item.Mark = data.m_new;
        item.Name = data.m_name;
        if (index == LoginManager.instance.GetCurServerIndex())
        {
            item.BG = "UI-s2-common-xuanzeanniu";
        }
        else
        {
            item.BG = "UI-s2-common-weixuanzeanniu";
        }
        switch (data.m_state)
        {
            case Define.SERVER_STATE_FREE:
                item.State = "UI-s2-common-fuwuqizhuangtaihao";
                break;
            case Define.SERVER_STATE_BUSY:
                item.State = "UI-s2-common-fuwuqizhuangtaimang";
                break;
            case Define.SERVER_STATE_MAINTAIN:
                break;
            default:
                break;
        }
        InputManager.instance.AddClickListener(item.gameObject, (go) => 
        {
            SelectItem(index);
        });
    }
    private void SelectItem(int index)
    {
        LoginManager.instance.SetTempServerIndex(index);
        for (int i = 0; i < _items.Count; i++)
        {
            ServerItem item = _items[i];
            if (i == index)
            {
                item.BG = "UI-s2-common-xuanzeanniu";
            }
            else
            {
                item.BG = "UI-s2-common-weixuanzeanniu";
            }
        }
    }
}
