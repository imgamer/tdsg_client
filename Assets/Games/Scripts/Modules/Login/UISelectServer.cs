﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 服务器选择列表
/// </summary>
public class UISelectServer : UIWin
{
    protected override void OnInit()
    {
        InitItem();
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject btnSure = center.Find("BtnSure").gameObject;
        InputManager.instance.AddClickListener(btnSure, (go) =>
        {
            LoginManager.instance.SetCurServerIndex();
            UIManager.instance.CloseWindow(WinID.UISelectServer);
            UIManager.instance.BackToWindow(this);
        });
    }
    private UI.Tab _uiTab;
    private WinPage _curContent;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            _curContent.Open(this);
        };
    }
}
