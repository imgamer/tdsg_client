﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerThreeAreaPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private List<ServerItem> _items = new List<ServerItem>();
    private UI.Grid _uiGrid;
    private ServerItem _template;
    private void InitItem()
    {
        Transform grid = transform.Find("ScrollView/UIGrid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        _template = grid.Find("Item").GetComponent<ServerItem>();
        _template.gameObject.SetActive(false);
    }
}
