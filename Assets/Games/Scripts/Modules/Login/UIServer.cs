﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 服务器显示界面
/// </summary>
public class UIServer : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_ON_LOGIN_SUCCESS, LoginSuccess);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        UpdateData();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ON_LOGIN_SUCCESS, LoginSuccess);
    }
    private UILabel _ip;
    private UISprite _state;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject ip = center.Find("IP").gameObject;
        _ip = ip.transform.Find("Label").GetComponent<UILabel>();
        _state = ip.transform.Find("State").GetComponent<UISprite>();
        InputManager.instance.AddClickListener(ip, (go) =>
        {
            UIManager.instance.OpenWindow(this, WinID.UISelectServer);
        });
        GameObject btnLogin = center.Find("BtnLogin").gameObject;
        InputManager.instance.AddClickListener(btnLogin, (go) =>
        {
            LoginManager.instance.Enter();
        });

        Transform edit = center.Find("Editor");
#if UNITY_EDITOR
        edit.gameObject.SetActive(true);
#else
        switch (Main.instance.m_buildMode)
        {
            case PublishMode.Customer:
                edit.gameObject.SetActive(false);
                break;
            case PublishMode.Developer:
                edit.gameObject.SetActive(true);
                break;
        }
#endif
        UIInput addServerIPInput = edit.Find("AddServerIP").GetComponent<UIInput>();
        GameObject btnAdd = edit.Find("BtnAdd").gameObject;
        InputManager.instance.AddClickListener(btnAdd, (go) =>
        {
            LoginManager.instance.AddServerIP(addServerIPInput.value);
        });
    }

    private void UpdateData()
    {
        LoginManager.ServerData data = LoginManager.instance.GetCurServer();
        _ip.text = data.m_name;
        switch (data.m_state)
        {
            case Define.SERVER_STATE_FREE:
                _state.spriteName = "UI-s2-common-fuwuqizhuangtaihao";
                break;
            case Define.SERVER_STATE_BUSY:
                _state.spriteName = "UI-s2-common-fuwuqizhuangtaimang";
                break;
            case Define.SERVER_STATE_MAINTAIN:
                break;
            default:
                break;
        }
        LoginManager.instance.SelectIP(data.m_ip);
    }

    private void LoginSuccess(object o)
    {
        if (!Active) return;
        UIManager.instance.CloseWindow(winID);
        UIManager.instance.OpenWindow(this, WinID.UISelectRole);
    }
}
