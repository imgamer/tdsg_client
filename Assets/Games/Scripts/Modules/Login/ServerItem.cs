﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 服务器Ip选择Item
/// </summary>
public class ServerItem : WinItem
{
    private UISprite _bg;
    public string BG
    {
        set
        {
            _bg.spriteName = value;
        }
    }
    private UISprite _state;
    public string State
    {
        set
        {
            _state.spriteName = value;
        }
    }

    private UISprite _mark;
    public bool Mark
    {
        set
        {
            _mark.enabled = value;
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private GameObject _levelRoot;
    private UILabel _level;
    public int Level
    {
        set
        {
            _level.text = string.Format("{0}级", value);
        }
    }

    protected override void OnInit()
    {
        _bg = transform.Find("BG").GetComponent<UISprite>();
        _state = transform.Find("State").GetComponent<UISprite>();
        _mark = transform.Find("Mark").GetComponent<UISprite>();
        _mark.enabled = false;
        _name = transform.Find("Text").GetComponent<UILabel>();
        _levelRoot = transform.Find("Level").gameObject;
        _levelRoot.SetActive(false);
        _level = _levelRoot.transform.Find("Text").GetComponent<UILabel>();
    }
}
