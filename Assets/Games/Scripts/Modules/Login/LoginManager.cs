﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DBID_TYPE = System.UInt64;
/// <summary>
/// 登录流程管理
/// </summary>
public class LoginManager : MonoSingleton<LoginManager>
{
    //账户数据
    private KBEngine.Account _account;
    public void SetAccount(KBEngine.Account account)
    {
        _account = account;
    }

    #region 服务器列表数据
    public struct ServerData
    {
        public bool m_new;
        public Byte m_state;
        public string m_name;
        public string m_ip;
    }
    private List<ServerData> serverDatas = new List<ServerData>();
    //Test
    private void InitServer()
    {
        ServerData data1 = new ServerData() { m_new = true, m_state = Define.SERVER_STATE_FREE, m_name = "魔道云", m_ip = "120.76.137.227" };
        ServerData data2 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试230", m_ip = "172.16.2.230" };
        ServerData data3 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试173", m_ip = "172.16.4.173" };
        ServerData data4 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试174", m_ip = "172.16.4.174" };
        ServerData data5 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试118", m_ip = "172.16.4.118" };
        ServerData data6 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试144", m_ip = "172.16.4.144" };
        ServerData data7 = new ServerData() { m_new = false, m_state = Define.SERVER_STATE_BUSY, m_name = "测试116", m_ip = "172.16.4.116" };
#if UNITY_EDITOR
        serverDatas.Add(data2);
        serverDatas.Add(data1);
        serverDatas.Add(data3);
        serverDatas.Add(data4);
        serverDatas.Add(data5);
        serverDatas.Add(data6);
        serverDatas.Add(data7);
#else
        serverDatas.Add(data1);
        serverDatas.Add(data2);
        serverDatas.Add(data3);
        serverDatas.Add(data4);
        serverDatas.Add(data5);
        serverDatas.Add(data6);
        serverDatas.Add(data7);
#endif
        GetServerIP();
    }
    public List<ServerData> GetServerDatas()
    {
        return serverDatas;
    }
    private int _curServerIndex = 0;
    private int _tempServerIndex = 0;
    public void SetTempServerIndex(int index)
    {
        _tempServerIndex = index;
    }
    public void SetCurServerIndex()
    {
        _curServerIndex = _tempServerIndex;
    }
    public int GetCurServerIndex()
    {
        return _curServerIndex;
    }
    public ServerData GetCurServer()
    {
        return serverDatas[_curServerIndex];
    }
    private const string ServerKey = "ServerKey";
    private void GetServerIP()
    {
        string serverIP = PlayerPrefs.GetString(ServerKey, "");
        if (string.IsNullOrEmpty(serverIP)) return;
        ServerData data = new ServerData() { m_new = true, m_state = Define.SERVER_STATE_FREE, m_name = serverIP, m_ip = serverIP };
        serverDatas.Add(data);
    }
    public void AddServerIP(string serverIP)
    {
        if (string.IsNullOrEmpty(serverIP)) return;

        string last = PlayerPrefs.GetString(ServerKey, "");
        if (!string.IsNullOrEmpty(last))
        {
            serverDatas.RemoveAt(serverDatas.Count - 1);
        }
        PlayerPrefs.SetString(ServerKey, serverIP);
        GetServerIP();
    }
    #endregion

    protected override void OnInit()
    {
        _logined = false;
        _userName = string.Empty;
        _password = string.Empty;
        InitServer();
    }

    protected override void OnUnInit()
    {

    }

    #region 角色切换相关

    #region 场景位置和角色门派类型的索引和互转
    public struct TypeInfo
    {
        public UInt32 type;
        public string resName;
        public TypeInfo(UInt32 type, string resName)
        {
            this.type = type;
            this.resName = resName;
        }
    }
    private readonly Dictionary<int, TypeInfo> _indexToType = new Dictionary<int, TypeInfo> 
    {
        { 0, new TypeInfo(Define.ROLE_TYPE_DIANCANG, "diancangpai_show") },
        { 1, new TypeInfo(Define.ROLE_TYPE_JIUHUASHAN, "jiuhua_show") },
        { 2, new TypeInfo(Define.ROLE_TYPE_XUANQINGGONG, "xuanqing_show") },
        { 3, new TypeInfo(Define.ROLE_TYPE_TIANXINGE, "xuanqing_show") },
        { 4, new TypeInfo(Define.ROLE_TYPE_ZIDIANMEN, "xuanqing_show") },
        //{ 5, new TypeInfo(Define.ROLE_TYPE_XIAOYAOGONG, "xuanqing_show") },
    };
    private TypeInfo GetTypeByIndex(int index)
    {
        TypeInfo type;
        _indexToType.TryGetValue(index, out type);
        return type;
    }
    private int GetIndexByType(UInt32 type)
    {
        foreach (var item in _indexToType)
        {
            if (item.Value.type == type)
            {
                return item.Key;
            }
        }
        return 0;
    }
    #endregion

    public void PreLoadRole(Action<bool> cb)
    {
        List<BaseSpawnData> datas = new List<BaseSpawnData>();
        foreach (var item in _indexToType)
        {
            string resName = item.Value.resName;
            BaseSpawnData data = new PreloadData(resName, resName, PoolName.Scene, PoolType.Once);
            datas.Add(data);
        }
        AssetsManager.instance.AsyncPreload(datas, cb);
    }

    public class RoleData
    {
        public DBID_TYPE m_dbid;
        public UInt32 m_type;
        public string m_name;
        public UInt16 m_level;
        public RoleData(DBID_TYPE dbid, UInt32 type, string name, UInt16 level)
        {
            m_dbid = dbid;
            m_type = type;
            m_name = name;
            m_level = level;
        }
    }
    private List<RoleData> _ownRoles = new List<RoleData>();
    private int _curIndex = 0;
    private bool _logined = false;
    public void InitRoles(List<object> listinfos)
    {
        _ownRoles.Clear();
        for (int i = 0; i < listinfos.Count; i++)
        {
            Dictionary<string, object> role = (Dictionary<string, object>)listinfos[i];
            DBID_TYPE dbid = (DBID_TYPE)role["dbid"];
            UInt32 type = (UInt32)role["feature"];
            string name = (string)role["name"];
            UInt16 level = (UInt16)role["level"];
            RoleData data = new RoleData(dbid, type, name, level);
            _ownRoles.Add(data);
            if (_account.LastSelCharacter == dbid) _curIndex = GetIndexByType(type);
        }
        _logined = true;
        EventManager.Invoke<object>(EventID.EVNT_ON_LOGIN_SUCCESS, null);
    }
    public void AddRole(Byte retcode, object info)
    {
        if (retcode == 0)
        {
            Dictionary<string, object> role = (Dictionary<string, object>)info;
            DBID_TYPE dbid = (DBID_TYPE)role["dbid"];
            UInt32 type = (UInt32)role["feature"];
            string name = (string)role["name"];
            UInt16 level = (UInt16)role["level"];
            RoleData data = new RoleData(dbid, type, name, level);
            _ownRoles.Add(data);
            EventManager.Invoke<object>(EventID.EVNT_ROLE_NUM_UPDATE, null);
        }
        else
        {

        }
    }
    public void RemoveRole(DBID_TYPE dbid)
    {
        foreach (var item in _ownRoles)
        {
            if(item.m_dbid == dbid)
            {
                _ownRoles.Remove(item);
                EventManager.Invoke<object>(EventID.EVNT_ROLE_NUM_UPDATE, null);
                break;
            }
        }
    }
    private RoleData ContainsRole(UInt32 type)
    {
        foreach (var item in _ownRoles)
        {
            if (item.m_type == type)
            {
                return item;
            }
        }
        return null;
    }
    
    private GameObject _curRole;
    public void ShowRole(int changeIndex, Action<UInt32, RoleData> cb)
    {
        HideRole();
        _curIndex += changeIndex;
        if (_curIndex < 0) _curIndex = _indexToType.Count - 1;
        if (_curIndex >= _indexToType.Count) _curIndex = 0;

        TypeInfo typeInfo = GetTypeByIndex(_curIndex);
        string resName = typeInfo.resName;
        if (string.IsNullOrEmpty(resName)) return;

        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Scene, PoolType.Once, (o) =>
        {
            if (o)
            {
                _curRole = o;
                o.transform.parent = null;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        if (cb != null) cb(typeInfo.type, ContainsRole(typeInfo.type));
    }

    public void HideRole()
    {
        AssetsManager.instance.Despawn(_curRole);
    }
    #endregion

    #region 登录创角
    private string _userName = string.Empty;
    private string _password = string.Empty;
    public void Login(string userName, string password, Action<bool> cb)
    {
        if (_userName.Equals(userName)) return;
        _userName = userName;
        _password = password;
        if (cb != null) cb(true);
    }
    
    public void SelectIP(string ip)
    {
        KBEApp.instance.SelectIP(ip);
    }

    public void Enter()
    {
        if (_logined)
        {
            EventManager.Invoke<object>(EventID.EVNT_ON_LOGIN_SUCCESS, null);
            return;
        }
        KBEApp.instance.Login(_userName, _password);
    }

    public void ReqCreateAvatar(UInt32 feature, string name)
    {
        _account.reqCreateAvatar(feature, name);
    }

    public void ReqSelectAvatarForGame(DBID_TYPE dbid)
    {
        _account.selectAvatarForGame(dbid);
    }

    public void ReqRemoveAvatar(DBID_TYPE dbid)
    {
        _account.reqRemoveAvatar(dbid);
    }

    public string GetRandomName(UInt32 type)
    {
        switch (type)
        {
            case Define.ROLE_TYPE_DIANCANG:
                return RandomNameConfig.SharedInstance.GetMaleName();
            case Define.ROLE_TYPE_JIUHUASHAN:
                return RandomNameConfig.SharedInstance.GetMaleName();
            case Define.ROLE_TYPE_XUANQINGGONG:
                return RandomNameConfig.SharedInstance.GetFemaleName();
            case Define.ROLE_TYPE_XIAOYAOGONG:
                return RandomNameConfig.SharedInstance.GetMaleName();
            case Define.ROLE_TYPE_ZIDIANMEN:
                return RandomNameConfig.SharedInstance.GetFemaleName();
            case Define.ROLE_TYPE_TIANXINGE:
                return RandomNameConfig.SharedInstance.GetFemaleName();
        }
        return string.Empty;
    }
    #endregion

}
