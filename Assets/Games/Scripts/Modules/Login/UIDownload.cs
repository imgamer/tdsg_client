﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 资源下载进度界面
/// </summary>
public class UIDownload : UIWin
{
    private UIProgressBar _progress;
    private UILabel _text;
    private int _downloadSize;
    private float _size;
    protected override void OnInit()
    {
        EventManager.AddListener<int>(EventID.EVNT_UPDATE_DOWNLOAD, UpdateBar);
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _progress = center.Find("Progress").GetComponent<UIProgressBar>();
        _text = center.Find("Text").GetComponent<UILabel>();
    }

    protected override void OnOpen(params object[] args)
    {
        _progress.value = 0;
        if (args == null || args.Length != 1) return;
        _downloadSize = (int)args[0];
        _size = (float)_downloadSize / 1024;
        _text.text = string.Format("{0}KB / {1}KB", 0, _size);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<int>(EventID.EVNT_UPDATE_DOWNLOAD, UpdateBar);
    }

    private void UpdateBar(int size)
    {
        if (!Active) return;
        if (_progress.value >= 1)
        {
            return;
        }
        _text.text = string.Format("{0}KB / {1}KB", (float)size / 1024, _size);
        _progress.value += 1 - (float)size / _downloadSize;
    }
}
