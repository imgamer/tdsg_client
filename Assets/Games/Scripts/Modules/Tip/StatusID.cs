﻿using System;

//ID 用法 by wangshufeng:
//	1. ID 为 UINT16
//	2. ID 不能重复。
//	3. ID 的百位数以上标示信息提示的类别，每增加一个类别，数值就增 1。
//	4. ID 的个位和十位给予信息提示类别内用（最多可以设置 100 个信息 ID）。
//	5. 类别内的信息 ID 要加上类别相关的前缀，以免信息 ID 重复定义。
//	6. 每个类别之间用两个“//--------”分割线隔开，分割线之间写上类别注释。
//	7. 类别内尽量不要超出 100 个信息 ID 从而使得 ID 号要往类别上增。
//		（不强制，但遇到 ID 过多的尽量尝试拆分为多个类别，确实不能拆分时也不反对往类别上增一）
//	8. 每个类别后加一空行。
//	9. 类别内的 ID 可以自行分子类，子分类之间用空格分开，并可以在子类别前加上注释，
//		但别用“//--------”分割，
public enum StatusID : ushort
{
	// ----------------------------------------
	// 公有消息，编号范围：  0 - 99
	// ----------------------------------------
	// COMMON_ILLEGAL_USERS = 0 // 非法使用者
	// COMMON_ENTITY_NOT_EXIST = 1 // 实体不存在
	
	
	// ----------------------------------------
	// 道具消息 ,编号范围：100 - 199
	// ----------------------------------------
	ITEM_CAN_NOT_FIND_THE_EMPTY_POSITION = 100, // 找不到空位置
	ITEM_NOT_EXIST = 101, // 道具不存在
	ITEM_NOT_CONSISTENT = 102, //道具不一致
	ITEM_NOT_SUPERPOSITION = 103, //道具不可叠加
	ITEM_CAN_BE_STACKED_SHORTAGE = 104, //可叠加数量不足
	ITEM_CAN_NOT_FIND_IN_THE_KITBAG = 105, //在背包中找不到该道具
	ITEM_AMOUNT_MORE_THAN_STACKABLE = 106, //该道具的数量超过该道具的最大叠加量
	
	// ----------------------------------------
	// 背包消息 ,编号范围：200 - 299
	// ----------------------------------------
	KITBAG_ADD_STACK_ITEM_SUCCESS = 200, //背包道具叠加成功
	KITBAG_NULL_NOT_FIT_EQUIP = 201, //物品为空，不能装备
	KITBAG_NOT_EQUIP_NOT_FIT_EQUIP = 202, //物品类型不是装备，不能装备
	KITBAG_NOT_CAN_EQUIPPED_NOT_FIT_EQUIP = 203, //装备的位置不在该物品可装备位置上， 不能装备
	KITBAG_ADD_ITEM_SUCCESS = 204, //背包添加道具成功
	KITBAG_ADD_ITEM_FAILURE = 205, //背包添加道具失败
	KITBAG_DELETE_ITEM_SUCCESS = 206, //背包删除道具成功
	KITBAG_DELETE_ITEM_FAILURE = 207, //背包删除道具失败
	
	// ----------------------------------------
	// 技能消息,编号范围： 300 - 399
	// ----------------------------------------
	SKILL_OK = 300, //技能可以使用
	SKILL_MP_NOT_ENOUGH = 301, // 实体的MP小于技能所需的MP
	SKILL_NOT_EXIST = 302, //技能不存在
	SKILL_CANNOT_USE = 303, // 技能不能使用
    SKILL_SPACE_INCONFORMITY = 310, //不能在当前地图使用！
    SKILL_TOO_FAR_TO_CAST = 311, //距离目标太远！
    SKILL_TOO_FAR_TO_DEST_POINT = 312, //未到达目标位置！

    SKILL_BINDED_REPEAT = 2321,//该技能已认证过，不需要再认证。
    SKILL_BINDED_INVALID_FOR_MYTHICAL = 2322,//神兽不能认证技能。
    SKILL_BINDED_INVALID_FOR_PRIMARY_SKILL = 2323,//自带技能不能认证。

    PET_REJUVEATE_INVALID_TYPE = 2341,//神兽或者变异宝宝不能还童。

	// ----------------------------------------
	// 组队消息,编号范围： 2000 - 2100
    // ----------------------------------------
    TEAM_CAPTAIN_CHANGED = 2020,
    TEAM_RECEIVE_JOIN_TEAM_INVITE = 2004,
    TEAM_RECEIVE_JOIN_TEAM_REQUEST = 2023,

    // ----------------------------------------
    // 帮会消息,编号范围： 4600 - 4699
    // ----------------------------------------
    TONG_CAPTAIN_CHANGED = 4615,
    TONG_RECEIVE_JOIN_TONG_INVITE = 4624,
    TONG_RECEIVE_JOIN_TONG_REQUEST = 4625,
    TONG_EVENT_COMMIT_DUTY = 4650,

	// ----------------------------------------------
	// StatusID的最大值
	// ----------------------------------------------
	STATUSID_MAX = 65535,

	FIGHT_CAN_NOT_NEXT_ROOM = 802, //当前地区没有下一关了，请返回主城

    ARENA_INGOT_NOT_ENOUGH = 2503 //元宝不够,此操作不能进行


}

