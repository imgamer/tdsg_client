﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 一个按钮对话框提示
/// </summary>
public class OneButtonTip : BaseTip
{
    public Func<bool> CallBack;//返回True表示关闭，false表示不关闭
    private UILabel _text;
    private UILabel _btnName;

    public override TipType Type { get { return TipType.OneButtonTip; } }

    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _text = center.Find("Text").GetComponent<UILabel>();
        GameObject btnCenter = center.Find("BtnCenter").gameObject;
        _btnName = btnCenter.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnCenter, (go) => 
        {
            if (CallBack != null)
            {
                if (!CallBack()) return;
            }
            TipManager.instance.HideTip(this);
        });
    }

    protected override void OnShow(params object[] args)
    {
        if (args == null || args.Length != 2) return;
        _text.text = (string)args[0];
        _btnName.text = (string)args[1];
    }

    protected override void OnHide()
    {
        CallBack = null;
    }

    protected override void OnUnInit()
    {
        CallBack = null;
    }
}
