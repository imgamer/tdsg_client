﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 两个按钮对话框提示
/// </summary>
public class TwoButtonTip : BaseTip
{
    public Func<bool> LeftCallBack;//返回True表示关闭，false表示不关闭
    public Func<bool> RightCallBack;//返回True表示关闭，false表示不关闭
    private UILabel _text;
    private UILabel _btnLeftName;
    private UILabel _btnRightName;

    public override TipType Type { get { return TipType.TwoButtonTip; } }

    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _text = center.Find("Text").GetComponent<UILabel>();
        GameObject btnLeft = center.Find("BtnLeft").gameObject;
        _btnLeftName = btnLeft.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnLeft, (go) =>
        {
            if (LeftCallBack != null)
            {
                if (!LeftCallBack()) return;
            }
            TipManager.instance.HideTip(this);
        });
        GameObject btnRight = center.Find("BtnRight").gameObject;
        _btnRightName = btnRight.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnRight, (go) =>
        {
            if (RightCallBack != null)
            {
                if (!RightCallBack()) return;
            }
            TipManager.instance.HideTip(this);
        });
    }

    protected override void OnShow(params object[] args)
    {
        if (args == null || args.Length != 3) return;
        _text.text = (string)args[0];
        _btnLeftName.text = (string)args[1];
        _btnRightName.text = (string)args[2];
    }

    protected override void OnHide()
    {
        LeftCallBack = null;
        RightCallBack = null;
    }

    protected override void OnUnInit()
    {
        LeftCallBack = null;
        RightCallBack = null;
    }
}
