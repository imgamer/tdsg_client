﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 游戏提示基类
/// </summary>
public enum TipType
{
    TextTip,
    OneButtonTip,
    TwoButtonTip,
}

public abstract class BaseTip : MonoBehaviour 
{
    public abstract TipType Type { get; }
    private Dictionary<UIPanel, int> childPanels = new Dictionary<UIPanel, int>();
    private bool _inited = false;
    protected abstract void OnInit();

    protected abstract void OnShow(params object[] args);

    protected abstract void OnHide();

    protected abstract void OnUnInit();

    public void Show(params object[] args)
    {
        if (!_inited)
        {
            OnInit();
            UIPanel[] panels = transform.GetComponentsInChildren<UIPanel>(true);
            foreach (UIPanel p in panels)
            {
                childPanels[p] = p.depth;
            }
            _inited = true;
        }
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        OnShow(args);
    }

    public void Hide()
    {
        OnHide();
        if (gameObject.activeSelf) gameObject.SetActive(false);
    }

    private const int DepthOffset = 1000;
    public void SetDepth(int depth)
    {
        int d = depth + DepthOffset;
        transform.localPosition = new Vector3(0, 0, -d);
        foreach (UIPanel p in childPanels.Keys)
        {
            p.depth = childPanels[p] + d;
            p.sortingOrder = p.depth;
        }
    }

    void OnDestroy()
    {
        OnUnInit();
    }

}
