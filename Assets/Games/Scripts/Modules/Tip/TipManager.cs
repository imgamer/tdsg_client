﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

/// <summary>
/// 游戏提示管理类
/// </summary>
public class TipManager : MonoBehaviour
{
    public static TipManager instance { get; private set; }
    void Awake()
    {
        instance = this;
    }

    private Dictionary<TipType, LinkedList<BaseTip>> _availableTips = new Dictionary<TipType, LinkedList<BaseTip>>();
    private LinkedList<BaseTip> _activeTips = new LinkedList<BaseTip>();
    private const int _offsetDepth = 50;

    #region 转换提示内容
    private Dictionary<string, string> _tipsJson;
    private string GetText(int tipID)
    {
        if (_tipsJson == null )
        {
            TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("status_id");
            _tipsJson = JsonMapper.ToObject<Dictionary<string, string>>(textdata.text);
        }
        string message;
        if(!_tipsJson.TryGetValue(tipID.ToString(), out message))
        {
            Printer.LogError("找不到提示ID：{0}", tipID);
        }
        return message;
    }
    private const char separaror = ',';
    private string ConvertText(int tipID, string str)
    {
        string[] args = str.Split(separaror);
        string text = GetText(tipID);
        if (string.IsNullOrEmpty(text)) return string.Empty;
        return string.Format(text, args);
    }
    private string GetServerErrDescr(UInt16 id)
    {
        KBEngine.KBEngineApp.ServerErr err;
        KBEngine.KBEngineApp.serverErrs.TryGetValue(id, out err);
        return err.descr;
    }
    #endregion

    #region 加载设置预设
    private T LoadTip<T>(TipType type) where T : BaseTip
    {
        T tip = GetAvailableTip<T>(type);
        try
        {
            if (tip == null)
            {
                UnityEngine.Object prefab = Resources.Load(string.Format("Tip/{0}", type));
                if (prefab == null)
                {
                    Printer.LogWarning(string.Format("UI/Tip/{0} is not exist!", type));
                    return null;
                }
                else
                {
                    GameObject go = (GameObject)Instantiate(prefab);
                    go.name = prefab.name;
                    tip = go.GetComponent<T>();
                    if (!tip)
                    {
                        Printer.LogError(string.Format("Tip:{0} has not BaseTip component!", type));
                        return null;
                    }
                    Transform trans = go.transform;
                    trans.parent = transform;
                    trans.localScale = Vector3.one;
                    trans.localPosition = Vector3.one;
                    go.SetActive(false);
                }
            }
            return tip;
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            return null;
        }
    }
    public T GetActiveTip<T>(TipType type) where T : BaseTip
    {
        foreach (var item in _activeTips)
        {
            if(item.Type == type)
            {
                return (T)item;
            }
        }
        return null;
    }
    private T GetAvailableTip<T>(TipType type) where T : BaseTip
    {
        LinkedList<BaseTip> list;
        if (_availableTips.TryGetValue(type, out list))
        {
            if (list.Count > 0)
            {
                BaseTip ret = list.Last.Value;
                list.RemoveLast();
                return (T)ret;
            }
        }
        return null;
    }
    private void AddAvailableTip(BaseTip tip)
    {
        if (tip == null) return;
        LinkedList<BaseTip> list;
        if (!_availableTips.TryGetValue(tip.Type, out list))
        {
            list = new LinkedList<BaseTip>();
        }
        list.AddLast(tip);
        _availableTips[tip.Type] = list;
        tip.Hide();
    }
    private void SetTipsDepth()
    {
        int i = 0;
        foreach (BaseTip tip in _activeTips)
        {
            if (tip != null)
            {
                tip.SetDepth((i + 1) * _offsetDepth);
                i++;
            }
        }
    }
    #endregion

    #region 显示服务器错误信息提示
    public void ShowServerErrTip(UInt16 id)
    {
        TextTip tip = GetActiveTip<TextTip>(TipType.TextTip);
        if (tip == null)
        {
            tip = LoadTip<TextTip>(TipType.TextTip);
        }
        else
        {
            if (_activeTips.Contains(tip))
            {
                _activeTips.Remove(tip);
            }
        }
        _activeTips.AddLast(tip);
        tip.Show(GetServerErrDescr(id));
        SetTipsDepth();
    }
    #endregion

    #region 显示纯文字信息提示
    public void ShowTextTip(int tipID)
    {
        ShowTextTip(GetText(tipID));
    }
    public void ShowTextTip(int tipID, string str)
    {
        ShowTextTip(ConvertText(tipID, str));
    }
    public void ShowTextTip(string text)
    {
        TextTip tip = GetActiveTip<TextTip>(TipType.TextTip);
        if (tip == null)
        {
            tip = LoadTip<TextTip>(TipType.TextTip);
        }
        else
        {
            if (_activeTips.Contains(tip))
            {
                _activeTips.Remove(tip);
            }
        }
        _activeTips.AddLast(tip);
        tip.Show(text);
        SetTipsDepth();
    }
    #endregion

    #region 显示一个按钮的信息提示
    public void ShowOneButtonTip(int tipID, Func<bool> cb, string btnCenterName = "确定")
    {
        OneButtonTip tip = LoadTip<OneButtonTip>(TipType.OneButtonTip);
        if (_activeTips.Contains(tip))
        {
            _activeTips.Remove(tip);
        }
        _activeTips.AddLast(tip);
        tip.CallBack = cb;
        tip.Show(GetText(tipID), btnCenterName);
        SetTipsDepth();
    }
    public void ShowOneButtonTip(int tipID, string str, Func<bool> cb, string btnCenterName = "确定")
    {
        OneButtonTip tip = LoadTip<OneButtonTip>(TipType.OneButtonTip);
        if (_activeTips.Contains(tip))
        {
            _activeTips.Remove(tip);
        }
        _activeTips.AddLast(tip);
        tip.CallBack = cb;
        tip.Show(ConvertText(tipID, str), btnCenterName);
        SetTipsDepth();
    }
    #endregion

    #region 显示两个按钮的信息提示
    public void ShowTwoButtonTip(int tipID, Func<bool> leftCB, Func<bool> rightCB, string btnLeftName = "确定", string btnRightName = "取消")
    {
        TwoButtonTip tip = LoadTip<TwoButtonTip>(TipType.TwoButtonTip);
        if (_activeTips.Contains(tip))
        {
            _activeTips.Remove(tip);
        }
        _activeTips.AddLast(tip);
        tip.LeftCallBack = leftCB;
        tip.RightCallBack = rightCB;
        tip.Show(GetText(tipID), btnLeftName, btnRightName);
        SetTipsDepth();
    }
    public void ShowTwoButtonTip(int tipID, string str, Func<bool> leftCB, Func<bool> rightCB, string btnLeftName = "确定", string btnRightName = "取消")
    {
        TwoButtonTip tip = LoadTip<TwoButtonTip>(TipType.TwoButtonTip);
        if (_activeTips.Contains(tip))
        {
            _activeTips.Remove(tip);
        }
        _activeTips.AddLast(tip);
        tip.LeftCallBack = leftCB;
        tip.RightCallBack = rightCB;
        tip.Show(ConvertText(tipID, str), btnLeftName, btnRightName);
        SetTipsDepth();
    }
    #endregion

    #region 掩藏提示
    public void HideAll()
    {
        BaseTip[] tips = new BaseTip[_activeTips.Count];
        _activeTips.CopyTo(tips, 0);
        for (int i = 0; i < tips.Length; i++)
        {
            HideTip(tips[i]);
        }
    }
    public void HideTip(BaseTip tip)
    {
        if (tip == null) return;
        _activeTips.Remove(tip);
        AddAvailableTip(tip);
    }
    #endregion

}
