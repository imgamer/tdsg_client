﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 文本提示
/// </summary>
public class TextTip : BaseTip
{
    public override TipType Type { get { return TipType.TextTip; } }

    protected class Entry
    {
        public float time;
        public float stay = 0f;
        public float offset = 0f;
        public TextItem item;

        public float movementStart { get { return time + stay; } }
    }
    private int Comparison(Entry a, Entry b)
    {
        if (a.movementStart < b.movementStart) return -1;
        if (a.movementStart > b.movementStart) return 1;
        return 0;
    }
    /// <summary>
    /// 是否忽略TimeScale的影响
    /// </summary>
    public bool m_ignoreTimeScale = false;
    private float _Time
    {
        get { return m_ignoreTimeScale ? Time.unscaledTime : Time.time; }
    }

    //移动曲线
    public AnimationCurve m_offsetCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(3f, 100f) });
    //缩放曲线
    public AnimationCurve m_scaleCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.25f, 1f) });

    #region 创建或者销毁
    private List<Entry> _mList = new List<Entry>();
    private List<Entry> _mUnused = new List<Entry>();
    private int _counter = 0;
    private Entry Create()
    {
        // See if an unused entry can be reused
        if (_mUnused.Count > 0)
        {
            Entry ent = _mUnused[_mUnused.Count - 1];
            _mUnused.RemoveAt(_mUnused.Count - 1);
            ent.time = _Time;
            ent.item.Show();
            ent.offset = 0f;
            _mList.Add(ent);
            return ent;
        }

        // New entry
        Entry ne = new Entry();
        ne.time = _Time;
        ne.item = Instantiate(_template) as TextItem;
        ne.item.name = _counter.ToString();
        ne.item.transform.parent = _template.transform.parent;
        ne.item.Init();
        ne.item.Hide();
        _mList.Add(ne);
        ++_counter;
        return ne;
    }

    private void Delete(Entry ent)
    {
        _mList.Remove(ent);
        _mUnused.Add(ent);
        ent.item.Hide();
    }
    #endregion

    void Update()
    {
        if (_mList.Count <= 0)
        {
            TipManager.instance.HideTip(this);
            return;
        }
        float time = _Time;

        float offset = 0f;
        for (int i = _mList.Count; i > 0; )
        {
            Entry ent = _mList[--i];
            Transform entTrans = ent.item.transform;
            float currentTime = time - ent.movementStart;
            ent.offset = m_offsetCurve.Evaluate(currentTime);

            // Make the label scale in
            float s = m_scaleCurve.Evaluate(time - ent.time);
            if (s < 0.001f) s = 0.001f;
            entTrans.localScale = new Vector3(s, s, s);

            // Delete the entry when needed
            if (currentTime > _totalEnd) Delete(ent);
            else ent.item.Show();

            offset = Mathf.Max(offset, ent.offset);
            entTrans.localPosition = new Vector3(0f, offset, 0f);
            offset += Mathf.Round(entTrans.localScale.y * ent.item.GetHight());
        }
    }

    private TextItem _template;
    private float _totalEnd;
    protected override void OnInit()
    {
        _template = transform.Find("Root/Item").GetComponent<TextItem>();
        _template.Init();
        _template.Hide();

        Keyframe[] offsets = m_offsetCurve.keys;
        Keyframe[] scales = m_scaleCurve.keys;

        float offsetEnd = offsets[offsets.Length - 1].time;
        float scalesEnd = scales[scales.Length - 1].time;
        _totalEnd = Mathf.Max(scalesEnd, offsetEnd);
    }

    protected override void OnShow(params object[] args)
    {
        Entry ne = Create();
        ne.stay = 0.5f;
        ne.item.SetText((string)args[0]);

        // Sort the list
        _mList.Sort(Comparison);
    }

    protected override void OnHide()
    {
        for (int i = _mList.Count; i > 0; )
        {
            Entry ent = _mList[--i];
            if (ent.item != null) ent.item.Hide();
            else _mList.RemoveAt(i);
        }
    }

    protected override void OnUnInit()
    {

    }

}
