﻿using UnityEngine;
using System.Collections;

public class TextItem : MonoBehaviour 
{
    private UILabel _text;
    private UISprite _bg;
    public void Init()
    {
        _text = transform.Find("Text").GetComponent<UILabel>();
        _bg = transform.Find("BG").GetComponent<UISprite>();
    }
	
    public float GetHight()
    {
        return _bg.height;
    }

    public void SetText(string str)
    {
        _text.text = str;
        Vector2 size = _text.printedSize;
        _bg.width = (int)size.x + 30;
        _bg.height = (int)size.y + 20;
    }

	public void Show () 
    {
        if (gameObject.activeSelf) return;
        gameObject.SetActive(true);
	}

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
