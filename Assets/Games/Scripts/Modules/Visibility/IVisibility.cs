﻿using System;
using System.Collections.Generic;

public interface IVisibility
{
    VisibleTypeBase VisibleType { get; }

    void set_visibleType(object old);

    void SetVisibleType(byte type, object data);

    bool Visible();
}

