﻿using System;
using System.Collections.Generic;

public class VisibilityFactory : Singleton<VisibilityFactory>
{
    private Dictionary<byte, Type> _type_map;

    protected override void OnInit()
    {
        _type_map = new Dictionary<byte, Type>();
        _type_map[VisibilityDefine.VISIBLE_TYPE_ALL] = typeof(VisibleTypeAll);
        _type_map[VisibilityDefine.VISIBLE_TYPE_SINGLE] = typeof(VisibleTypeSingle);
        _type_map[VisibilityDefine.VISIBLE_TYPE_MULTITUDE] = typeof(VisibleTypeMultPlayers);
        _type_map[VisibilityDefine.VISIBLE_TYPE_TEAM] = typeof(VisibleTypeTeam);
        _type_map[VisibilityDefine.VISIBLE_TYPE_HAVEQUEST] = typeof(VisibleTypeHaveQuest);
    }

    public VisibleTypeBase CreateVisibleType(byte type, object data)
    {
        VisibleTypeBase vinst = Activator.CreateInstance(_type_map[type]) as VisibleTypeBase;
        vinst.Init(data);
        return vinst;
    }

    public VisibleTypeBase CreateVisibleTypeFromStream(byte type, string stream)
    {
        VisibleTypeBase vinst = Activator.CreateInstance(_type_map[type]) as VisibleTypeBase;
        vinst.InitFromServer(stream);
        return vinst;
    }
}
