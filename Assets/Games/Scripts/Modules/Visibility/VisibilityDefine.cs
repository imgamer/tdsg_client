﻿using System;
using System.Collections.Generic;

public class VisibilityDefine
{
    //可见类型定义
    public const byte VISIBLE_TYPE_ALL          = 0;    // 所有玩家可见
    public const byte VISIBLE_TYPE_SINGLE		= 1;	// 指定玩家可见
    public const byte VISIBLE_TYPE_MULTITUDE	= 2;	// 指定多个玩家可见
    public const byte VISIBLE_TYPE_TEAM			= 3;	// 指定队伍可见
    public const byte VISIBLE_TYPE_HAVEQUEST	= 4;	// 有指定任务可见

}
