﻿using System;
using System.Collections.Generic;
using LitJson;

public abstract class VisibleTypeBase
{
    public abstract void Init(object data);

    public abstract void InitFromServer(string json);

    public abstract bool Check(KBEngine.Avatar target);
}

public class VisibleTypeAll : VisibleTypeBase
{
    public override void Init(object data) {}

    public override void InitFromServer(string json) {}

    public override bool Check(KBEngine.Avatar target)
    {
        return true;
    }
}

public class VisibleTypeSingle : VisibleTypeBase
{
    private int _data;

    public override void Init(object data) 
    {
        _data = (int)data;
    }

    public override void InitFromServer(string json)
    {
        //LitJson的JsonMapper有以下问题：
        // int i = JsonMapper.ToObject<int>("0")无法正常解释出 i=0，
        //原因是字符串"0"是以\0结束的，但是\0字符被LitJson认为是无效字符，
        //因而返回一个错误。
        //
        //为了让客户端可以正确将数据解释出来，服务器端会把数据放入列表，
        //再发送给客户端。
        _data = JsonMapper.ToObject<List<int>>(json)[0];
    }

    public override bool Check(KBEngine.Avatar target)
    {
        return _data == target.id;
    }
}

public class VisibleTypeMultPlayers : VisibleTypeBase
{
    private int[] _data;

    public override void Init(object data)
    {
        _data = (int[])data;
    }

    public override void InitFromServer(string json)
    {
        _data = JsonMapper.ToObject<List<int[]>>(json)[0];
    }

    public override bool Check(KBEngine.Avatar target)
    {
        foreach (int i in _data)
        {
            if (i == target.id)
                return true;
        }
        return false;
    }
}

public class VisibleTypeTeam : VisibleTypeBase
{
    private int _data;

    public override void Init(object data)
    {
        _data = (int)data;
    }

    public override void InitFromServer(string json)
    {
        _data = JsonMapper.ToObject<List<int>>(json)[0];
    }

    public override bool Check(KBEngine.Avatar target)
    {
        return target.teamModule.IsInTeam();
    }
}

public class VisibleTypeHaveQuest : VisibleTypeBase
{
    private uint[] _data;

    public override void Init(object data)
    {
        _data = (uint[])data;
    }

    public override void InitFromServer(string json)
    {
        _data = JsonMapper.ToObject<List<uint[]>>(json)[0];
    }

    public override bool Check(KBEngine.Avatar target)
    {
        foreach (var qid in _data)
        {
            if (target.taskModule.HaveQuest(qid))
            {
                return true;
            }
        }   
        return false;
    }
}

