﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 跨地图寻路方案
/// </summary>
public class SpaceNavigator
{
    public enum NavResult
    {
        REACH,
        BROKEN,
    };

    private ulong _timerID = TimeUtils.InvalidTimerID;
    private bool _autoTeleport;     //如果不是相同地图，是否自动传送

    private int _destSpace;
    private Vector3 _destPosition;
    private Action _onComplete;

    private bool _isRuning;
    private WeakReference _target;

    public bool IsRuning
    {
        get { return _isRuning; }
    }

    public int DestSpace
    {
        get { return _destSpace; }
    }

    public Vector3 DestPosition
    {
        get { return _destPosition; }
    }

    public KBEngine.Avatar Target
    {
        get 
        {
            if (_target == null)
                return null;
            else
                return _target.Target as KBEngine.Avatar; 
        }
    }

    public SpaceNavigator(KBEngine.Avatar target)
    {
        _isRuning = false;
        _target = new WeakReference(target);

        _destSpace = 0;
        _destPosition = new Vector3();
        _onComplete = null;

        EventManager.AddListener<Scene>(EventID.EVNT_SCENE_READY, OnSceneReady);
    }

    /// <summary>
    /// 新场景准备好后回掉
    /// </summary>
    /// <param name="scene"></param>
    void OnSceneReady(Scene scene)
    {
        Continue(true, 1f);
    }

    public bool Navigate(int destSpace, Vector3 destPosition, bool autoTeleport, Action onComplete = null)
    {
        Stop();

        _autoTeleport = autoTeleport;

        _destSpace = destSpace;
        _destPosition = destPosition;
        _onComplete = onComplete;

        Continue(autoTeleport);
        return true;
    }

    public void Stop()
    {
        Target.SceneEntityObj.Navigate.Stop();
        Reset();
    }

    public void Pause()
    {
        Target.SceneEntityObj.Navigate.Stop();
        _isRuning = false;
    }

    private void Reset()
    {
        _isRuning = false;
        _destSpace = 0;
        _onComplete = null;
    }

    /// <summary>
    /// 继续之前的寻路（可以设置一个延后时间）
    /// </summary>
    /// <param name="delay"></param>
    public void Continue(bool autoTeleport, float delay = 0f)
    {
        if(_isRuning || _destSpace == 0)
            return;

        if (0.001f < delay - 0f || delay - 0f < -0.001f)
        {
            if(_timerID != TimeUtils.InvalidTimerID)
            {
                TimeUtils.RemoveTimer(_timerID);
            }

            _timerID = TimeUtils.AddRealTimer(delay, null, () =>
                //onComplete
                {
                    TimeUtils.RemoveTimer(_timerID);
                    _timerID = TimeUtils.InvalidTimerID;
                    Continue(autoTeleport, 0f);
                });
            TimeUtils.StartTimer(_timerID);
            return;
        }

        if (SceneManager.instance.CurrentScene.UType != _destSpace)
        {
            if (autoTeleport)
            {
                Target.TeleportSpaceSpawnPoint(_destSpace);
            }
            return;
        }

        _isRuning = true;
        Target.SceneEntityObj.Navigate.Start(BaseNavigate.NavigateType.Initiative, _destPosition, 1f, 
        () =>
        //onComplete
        {
            Action onComplete = _onComplete;
            Reset();
            if (onComplete != null) onComplete();
        }, () =>
        //onKill
        {
            Reset();
        });
    }
}
