﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Item;
using System.Linq;

public class KitbagBagPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private bool _inited = false;   //判断界面是否第一次打开
    private int _lastBagIndex = 0;  //物品位置索引
    private SortedList<Int32, KitbagItem> _emptyBagItem = new SortedList<Int32, KitbagItem>(); //背包中间空位置Item
    private Dictionary<ITEM_ORDER, KitbagItem> _bagOrderItem = new Dictionary<ITEM_ORDER, KitbagItem>(); //物品索引
    
    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        Initpage();

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddItem);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveItem);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItemAttribute); 
    }

    protected override void OnOpen(params object[] args)
    {
        if (!_inited)
        {
            //第一次打开自动整理背包
            ArrangeBagItem();
            _inited = true;
        }
        _selectBg.SetActive(false);
        _uiGrid.GoToItem(0, _Num);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddItem);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveItem);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItemAttribute);
    }

    private const int _Num = 60;
    private UI.Grid _uiGrid;
    private KitbagItem[] _goodItems = new KitbagItem[_Num]; //物品

    private GameObject _selectBg;
    private void Initpage()
    {
        _selectBg = transform.Find("SelectBg").gameObject;

        Transform gird = transform.Find("Scroll/Grid");
        _uiGrid = gird.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(gird, _Num);

        for (int i = 0; i < _Num; i++)
        {
            Transform goods = gird.GetChild(i);
            goods.name = i.ToString();

            KitbagItem goodsItem = goods.GetComponent<KitbagItem>();
            goodsItem.Init(ParentUI);
            goodsItem.Index = i;
            _goodItems[i] = goodsItem;
        }
        _uiGrid.Reposition();

        GameObject arrangeBtn = transform.Find("ArrangeBtn").gameObject;
        InputManager.instance.AddClickListener(arrangeBtn, (go) =>
        {
            ArrangeBagItem();
        });
    }

    private void SelectItem(KitbagItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void RefreshItem(List<ItemBase> Items)
    {
        foreach (ItemBase baseData in Items)
        {
            AddBagItem(baseData);
        }
    }

    private void AddBagItem( ItemBase data)
    {
        if (_emptyBagItem.Count > 0)
        {
            KitbagItem emptyItem = _emptyBagItem.Values.First();
            SetItemData(emptyItem, data);
            _bagOrderItem.Add(data.Order, emptyItem);
            _emptyBagItem.RemoveAt(0);
        }
        else
        {
            SetItemData(_goodItems[_lastBagIndex], data);
            if (_bagOrderItem.ContainsKey(data.Order))
            {
                Printer.LogError("KitbageBagPage:: _bagOrderItem " + data.Order.ToString() + " Have  Same Key");
            }
            _bagOrderItem.Add(data.Order, _goodItems[_lastBagIndex]);
            _lastBagIndex++;
        }
    }

    private void SetItemData(KitbagItem item, ItemBase itemData)
    {
        ParentUI.SetDynamicSpriteName(item.GoodsSprite, itemData.Icon);
        item.GoodsObj.SetActive(true);
        if (itemData.Amount > 1)
        {
            item.Count = Convert.ToString(itemData.Amount);
            item.CountObj.SetActive(true);
        }
        else
        {
            item.CountObj.SetActive(false);
        }
        ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Left, SourcePage.Goods, itemData, (go) =>
            {
                SelectItem(item);
            });
    }

    private void RemoveBagItem(KitbagItem item)
    {
        item.CountObj.SetActive(false);
        item.GoodsObj.SetActive(false);
        ItemDetailManager.instance.RemoveListenerItemDetail(item.gameObject);
    }

    private void ArrangeBagItem()
    {
        _lastBagIndex = 0;
        _emptyBagItem.Clear();
        foreach(KitbagItem item in _bagOrderItem.Values)
        {
            RemoveBagItem(item);
        }
        _bagOrderItem.Clear();
        List<ItemBase> Items = _avatar.kitbagModule.BagItems();
        Items.Sort(delegate(ItemBase a, ItemBase b)
        {
            if (a.ChildType == b.ChildType)
            {
                return ((Int32)a.ID).CompareTo((Int32)b.ID);
            }
            else
            {
                return a.ChildType.CompareTo(b.ChildType);
            }
        });
        RefreshItem(Items);
    }

    private void AddItem(ITEM_ORDER order)
    {
        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if (data == null)
        {
            Printer.LogError(string.Format("KitbagBagPage::AddGoods ORDER: {0} not find"), order);
            return;
        }
        if (data.ChildType == ItemTypeDefine.ITEM_QUEST)
        {
            return;
        }
        AddBagItem(data);
    }

    private void RemoveItem(ITEM_ORDER order)
    {
        KitbagItem item;
        _bagOrderItem.TryGetValue(order, out item);
        if (item == null) return;
        RemoveBagItem(item);
        _emptyBagItem.Add(item.Index, item);
        _bagOrderItem.Remove(order);
    }

    private void UpdateItemAttribute(ITEM_ORDER order)
    {
        if (_avatar.equipModule.Equipped(order)) //背包更新物品的时候，该物品可能已经装备
        {
            return;
        }

        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if (data == null)
        {
            Printer.LogError(string.Format("KitbagBagPage::UpdateGoodsAttribute ORDER: {0} not find"), order);
            return;
        }
        if (data.ChildType == ItemTypeDefine.ITEM_QUEST)
        {
            return;
        }
        KitbagItem item;
        _bagOrderItem.TryGetValue(order, out item);
        SetItemData(item, data);
    }
}
