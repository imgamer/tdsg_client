﻿using UnityEngine;
using System.Collections;
using System;

public class KitbagItem : WinItem
{
    private Int32  _index;
    public  Int32  Index
    {
        set
        {
            _index = value;
        }
        get
        {
            return _index;
        }
    }

    private UISprite _quality;
    public string Quality
    {
        set
        {
            _quality.spriteName = value.ToString();
        }
    }

    private UISprite _goods;
    public string Goods
    {
        set
        {
            _goods.spriteName = value.ToString();
        }
    }

    public UISprite GoodsSprite
    {
        get
        {
            return _goods;
        }
    }

    public GameObject GoodsObj
    {
        get
        {
            return _goods.gameObject;
        }
    }

    private UILabel _count;
    public string  Count
    {
        set
        {
            _count.text = value.ToString();
        }
    }
    public GameObject CountObj
    {
        get 
        {
            return _count.gameObject;
        }
    }

    protected override void OnInit()
    {
        _quality = transform.Find("Quality").GetComponent<UISprite>();
        _goods = transform.Find("Goods").GetComponent<UISprite>();
        _goods.gameObject.SetActive(false);
        _count = transform.Find("Count").GetComponent<UILabel>();
        _count.gameObject.SetActive(false);
    }
}
