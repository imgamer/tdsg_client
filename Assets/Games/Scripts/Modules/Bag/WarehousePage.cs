﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;
using System.Linq;

public class WarehousePage : WinPage
{
    bool _inited = false;   //判断界面是否第一次打开
    private KBEngine.Avatar _avatar;

    private int _lastBagIndex = 0;  //背包最后位置索引
    private SortedList<Int32, KitbagItem> _emptyBagItem = new SortedList<Int32, KitbagItem>(); //背包中间空位置Item
    private Dictionary<ITEM_ORDER, KitbagItem> _bagOrderItem = new Dictionary<ITEM_ORDER, KitbagItem>(); //物品索引数据

    private int _lastWarehouseIndex = 0;
    private SortedList<Int32, KitbagItem> _emptyWhItem = new SortedList<Int32, KitbagItem>(); //仓库中间空位置Item
    private Dictionary<ITEM_ORDER, KitbagItem> _whOrderItem = new Dictionary<ITEM_ORDER, KitbagItem>(); //仓库索引数据

    private int _currentWhIndex = 0;  //当前所在仓库

    //当前仓库总数量(临时)
    private int _warehouseCount = 2;

	protected override void OnInit()
	{
        InitPage();
        InitKitbagItem();
        InitWarehouseItem();

        _avatar = GameMain.Player;

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddBagGoods);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveBagGoods);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_ADD_ITEM, AddWarehouseGoods);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_REMOVE_ITEM, RemoveWarehouseGoods);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateBagItemAttribute); 
	}
	
	protected override void OnOpen(params object[] args)
	{
        if (_meau.activeSelf)
        {
            _meau.SetActive(false);
        }
	}
	
	protected override void OnRefresh()
	{
        if(!_inited)
        {
            _currentWhIndex = 1;
            _avatar.kitbagModule.CurrentWarehouseIndex = _currentWhIndex;
            ArrangeBagItem();
            ArrangeWarehouseItem();           
            _inited = true;
        }
        _kitbagSelectBg.SetActive(false);
        _warehouseSelectBg.SetActive(false);
	}
	
	protected override void OnClose()
	{
		
	}
	
	protected override void OnUnInit()
	{
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddBagGoods);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveBagGoods);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_ADD_ITEM, AddWarehouseGoods);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_REMOVE_ITEM, RemoveWarehouseGoods);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateBagItemAttribute); 
	}

    private GameObject _meau;
    private UILabel _whBtnLabel;
    private GameObject _btnBg;
    private UILabel _warehouseLabelIndex;

    private GameObject _kitbagSelectBg;
    private GameObject _warehouseSelectBg;
    public void InitPage()
    {
        _kitbagSelectBg = transform.Find("KitbagSelectBg").gameObject;
        _warehouseSelectBg = transform.Find("WarehouseSelectBg").gameObject;

        _meau = transform.Find("Menu").gameObject;
        _meau.SetActive(false);
        GameObject warehouseBtn = transform.Find("WarehouseBtn").gameObject;
        //GameObject arrows = transform.Find("WarehouseBtn/Arrows").gameObject;
        _whBtnLabel = transform.Find("WarehouseBtn/Label").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(warehouseBtn, (go) =>
        {
            if (_meau.activeSelf)
            {
                _meau.SetActive(false);
                //arrows.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
            else
            {
                _meau.SetActive(true);
               // arrows.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
            }
        });
        GameObject arrangeWarehouseBtn = transform.Find("ArrangeWarehouseBtn").gameObject;
        InputManager.instance.AddClickListener(arrangeWarehouseBtn, (go) =>
        {
            ArrangeWarehouseItem();
        });
        GameObject arrageKitbagBtn = transform.Find("ArrageKitbagBtn").gameObject;
        InputManager.instance.AddClickListener(arrageKitbagBtn, (go) =>
        {
            ArrangeBagItem();
        });

        _warehouseLabelIndex = transform.Find("WarehouseIndex").GetComponent<UILabel>();
        GameObject upBtn = transform.Find("UpBtn").gameObject;

        InputManager.instance.AddClickListener(upBtn, (go) =>
        {
            if (_currentWhIndex == _warehouseCount)
            {
                return;
            }
            _currentWhIndex++;
            ChangeWarehouse(_currentWhIndex);
            _warehouseLabelIndex.text = String.Format("{0}/{1}", _currentWhIndex, _warehouseCount);
        });

        GameObject DownBtn = transform.Find("DownBtn").gameObject;
        InputManager.instance.AddClickListener(DownBtn, (go) =>
        {
            if (_currentWhIndex == 1)
            {
                return;
            }
            _currentWhIndex--;
            ChangeWarehouse(_currentWhIndex);
            _warehouseLabelIndex.text = String.Format("{0}/{1}", _currentWhIndex, _warehouseCount);
        });

        _btnBg = transform.Find("Menu/Buttons/BtnBg").gameObject;
        //切换仓库
        GameObject freeBtn1 = transform.Find("Menu/Buttons/FreeBtn1").gameObject;
        _btnBg.transform.parent = freeBtn1.transform;
        _btnBg.transform.localPosition = Vector3.zero;

        InputManager.instance.AddClickListener(freeBtn1, (go) =>
        {
            if (_currentWhIndex == 1)
            {
                return;
            }
            _btnBg.transform.parent = freeBtn1.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            //arrows.transform.localRotation = Quaternion.Euler(Vector3.zero);

            ChangeWarehouse(1);
            _meau.SetActive(false);
        });

        GameObject freeBtn2 = transform.Find("Menu/Buttons/FreeBtn2").gameObject;
        InputManager.instance.AddClickListener(freeBtn2, (go) =>
        {
            if (_currentWhIndex == 2)
            {
                return;
            }
            _btnBg.transform.parent = freeBtn2.transform;
            _btnBg.transform.localPosition = Vector3.zero;
            //arrows.transform.localRotation = Quaternion.Euler(Vector3.zero);

            ChangeWarehouse(2);
            _meau.SetActive(false);
        });

        GameObject addBtn = transform.Find("Menu/Buttons/AddBtn").gameObject;
        InputManager.instance.AddClickListener(addBtn, (go) =>
        {

        });
    }

    private void SelecKitbagItem(KitbagItem item)
    {
        _kitbagSelectBg.transform.parent = item.transform;
        _kitbagSelectBg.transform.localPosition = Vector3.zero;
        _kitbagSelectBg.SetActive(true);
    }

    private void SelecWarehouseItem(KitbagItem item)
    {
        _warehouseSelectBg.transform.parent = item.transform;
        _warehouseSelectBg.transform.localPosition = Vector3.zero;
        _warehouseSelectBg.SetActive(true);
    }

    private void ChangeWarehouse(int index)
    {
        _whBtnLabel.text = "免费仓库" + index;

        _currentWhIndex = index;
        _warehouseLabelIndex.text = String.Format("{0}/{1}", _currentWhIndex, _warehouseCount);
        _avatar.kitbagModule.CurrentWarehouseIndex = index;
        ArrangeWarehouseItem();
    }

    private const int _Num = 60;
    private UI.Grid _goodUIGrid;
    private KitbagItem[] _bagItems = new KitbagItem[_Num]; //包裹
    private void InitKitbagItem()
    {
        Transform goodsGrid = transform.Find("Kitbag/Scroll/Grid");
        _goodUIGrid = goodsGrid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(goodsGrid, _Num);
        for (int i = 0; i < _Num; i++)
        {
            Transform goods = goodsGrid.GetChild(i);
            goods.name = i.ToString();
            KitbagItem goodsItem = goods.GetComponent<KitbagItem>();
            goodsItem.Init(ParentUI);
            goodsItem.Index = i;
            _bagItems[i] = goodsItem;
        }
        _goodUIGrid.Reposition();
    }

    private UI.Grid _whUIGrid;
    private KitbagItem[] _whItems = new KitbagItem[_Num]; //仓库
    private void InitWarehouseItem()
    {
        Transform whGrid = transform.Find("Warehouse/Scroll/Grid");
        _whUIGrid = whGrid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(whGrid, _Num);
        for (int i = 0; i < _Num; i++)
        {
            Transform goods = whGrid.GetChild(i);
            goods.name = i.ToString();
            KitbagItem goodsItem = goods.GetComponent<KitbagItem>();
            goodsItem.Init(ParentUI);
            goodsItem.Index = i;
            _whItems[i] = goodsItem;
        }
        _whUIGrid.Reposition();
    }

    #region Bag
    private void RefreshBagItem(List<ItemBase> bagData)
    {
        foreach(ItemBase baseData in bagData)
        {
            AddBagItem(baseData);
        }
    }

    private void AddBagItem(ItemBase data)
    {
        if (_emptyBagItem.Count > 0)
        {
            KitbagItem emptyItem = _emptyBagItem.Values.First();
            SetBagItemData(emptyItem, data);
            _bagOrderItem.Add(data.Order, emptyItem);
            _emptyBagItem.RemoveAt(0);
        }
        else
        {
            SetBagItemData(_bagItems[_lastBagIndex], data);
            if (_bagOrderItem.ContainsKey(data.Order))
            {
                Printer.LogError("WarehousePage:: _bagOrderItem " + data.Order.ToString() + " Have  Same Key");
            }
            _bagOrderItem.Add(data.Order, _bagItems[_lastBagIndex]);
            _lastBagIndex++;
        }      
    }

    private void RemoveGoodsItem(KitbagItem item)
    {
        item.CountObj.SetActive(false);
        item.GoodsObj.SetActive(false);
        ItemDetailManager.instance.RemoveListenerItemDetail(item.gameObject);
       // item.Quality = "UI-s2-common-wupindikuang";
    }

    private void SetBagItemData(KitbagItem item, ItemBase itemData)
    {
        SetCommonData(item, itemData);
        ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Left, SourcePage.kitbag, itemData, (go) =>
            {
                SelecKitbagItem(item);
            });
    }
    #endregion

    private void SetCommonData(KitbagItem item, ItemBase itemData)
    {
        ParentUI.SetDynamicSpriteName(item.GoodsSprite, itemData.Icon);
        item.GoodsObj.SetActive(true);
        if (itemData.Amount > 1)
        {
            item.Count = Convert.ToString(itemData.Amount);
            item.CountObj.SetActive(true);
        }
    }

    private void ArrangeBagItem()
    {
        _lastBagIndex = 0;
        _emptyBagItem.Clear();
        foreach (KitbagItem item in _bagOrderItem.Values)
        {
            RemoveGoodsItem(item);
        }
        _bagOrderItem.Clear();
        List<ItemBase> Items = _avatar.kitbagModule.BagItems();
        Items.Sort(delegate(ItemBase a, ItemBase b)
        {
            if (a.ChildType == b.ChildType)
            {
                return ((Int32)a.ID).CompareTo((Int32)b.ID);
            }
            else
            {
                return a.ChildType.CompareTo(b.ChildType);
            }
        });
        RefreshBagItem(Items);
    }

    #region Warehouse
    private void RefushWarehouseItem(List<ItemBase> whData)
    {
        _whOrderItem.Clear();
        for (int i = 0; i < whData.Count; i++)
        {
            AddWarehouseItem(whData[i]);
        }
    }

    private void AddWarehouseItem(ItemBase data)
    {
        if (_emptyWhItem.Count > 0)
        {
            KitbagItem emptyItem = _emptyWhItem.Values.First();
            SetWarehouseItemData(emptyItem, data);
            _whOrderItem.Add(data.Order, emptyItem);
            _emptyWhItem.RemoveAt(0); //删除索引第一个数据
        }
        else
        {
            SetWarehouseItemData(_whItems[_lastWarehouseIndex], data);
            if (_whOrderItem.ContainsKey(data.Order))
            {
                Printer.LogError("WarehousePage:: _whOrderItem " + data.Order.ToString() + " Have  Same Key");
            }
            _whOrderItem.Add(data.Order, _whItems[_lastWarehouseIndex]);
            _lastWarehouseIndex++;           
        }
    }

    private void  SetWarehouseItemData(KitbagItem item, ItemBase data)
    {
        SetCommonData(item, data);
        ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Right, SourcePage.Warehouse, data, (go) =>
            {
                SelecWarehouseItem(item);
            });
    }

    private void ArrangeWarehouseItem()
    {
        _lastWarehouseIndex = 0;
        _emptyWhItem.Clear();
        foreach(KitbagItem item in _whOrderItem.Values)
        {
            RemoveGoodsItem(item);
        }
        _whOrderItem.Clear();
        List<ItemBase> Items = _avatar.kitbagModule.GetWarehouseData(_currentWhIndex);
        Items.Sort(delegate(ItemBase a, ItemBase b)
        {
            if (a.ChildType == b.ChildType)
            {
                return ((Int32)a.ID).CompareTo((Int32)b.ID);
            }
            else
            {
                return a.ChildType.CompareTo(b.ChildType);
            }
        });
        RefushWarehouseItem(Items);
    }
    #endregion

    #region Message
    private void AddBagGoods(ITEM_ORDER order)
    {
        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if(data == null)
        {
            Printer.LogError(string.Format("WareHousePgae::AddBagGoods ORDER: {0} not find"), order);
            return;
        }
        AddBagItem(data);
    }

    private void RemoveBagGoods(ITEM_ORDER order)
    {
        KitbagItem item;
        _bagOrderItem.TryGetValue(order, out item);
        RemoveGoodsItem(item);
        Int32 index = (Int32)order;
        _emptyBagItem.Add(item.Index, item);
        _bagOrderItem.Remove(order);
    }

    private void UpdateBagItemAttribute(ITEM_ORDER order)
    {
        if (_avatar.equipModule.Equipped(order)) //背包更新物品的时候，该物品可能已经装备
        {
            return;
        }

        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if (data == null)
        {
            Printer.LogError(string.Format("KitbagBagPage::UpdateGoodsAttribute ORDER: {0} not find"), order);
            return;
        }
        if (data.ChildType == ItemTypeDefine.ITEM_QUEST)
        {
            return;
        }
        
        KitbagItem item;
        _bagOrderItem.TryGetValue(order, out item);
        SetBagItemData(item, data);
    }

    private void AddWarehouseGoods(ITEM_ORDER order)
    {
        Printer.Log("WHOUSE add order is" +          (Int32)order);
        ItemBase data = _avatar.kitbagModule.GetWarehouseItemByOrder(order);
        if(data == null)
        {
            Printer.LogError(string.Format("WareHousePgae::AddWarehouseGoods ORDER: {0} not find"), order);
            return;
        }
        AddWarehouseItem(data);
    }

    private void RemoveWarehouseGoods(ITEM_ORDER order)
    {
        KitbagItem item;
        _whOrderItem.TryGetValue(order, out item);
        RemoveGoodsItem(item);
        _emptyWhItem.Add(item.Index, item);
        _whOrderItem.Remove(order);
    }
    #endregion
}
