﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;
using System.Linq;

public class KitbagPage : WinPage
{
	protected override void OnInit()
	{
        InitPage();
        InitEquipment();
        InitTab();
	}
	
	protected override void OnOpen(params object[] args)
	{
        _equipPage.Open(ParentUI);
        _uiTab.SwitchTo(0);
	}

    protected override void OnRefresh()
    {
        string modelName = GameMain.Player.ModelID;
        SetEntity(modelName);

    }
	protected override void OnClose()
	{
		
	}
	
	protected override void OnUnInit()
	{

	}

    private Transform _characterRoot;
    private WinPage _equipPage;
    private void InitEquipment()
    {
        _equipPage = transform.Find("Equipment").GetComponent<WinPage>(); 
        _characterRoot = transform.Find("Character"); 
    }

    private WinPage _curContent;
    private UI.Tab _uiTab;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            _curContent.Open(ParentUI);
        };
    }

    private UILabel _coin;
    private UILabel _bindCoin;
    private UILabel _ingot;
    private UILabel _bindIngot;
    private void InitPage()
    {
        _coin = transform.Find("Text/Coin").GetComponent<UILabel>();
        _bindCoin = transform.Find("Text/BindCoin").GetComponent<UILabel>();
        _ingot = transform.Find("Text/Ingot").GetComponent<UILabel>();
        _bindIngot = transform.Find("Text/BindIngot").GetComponent<UILabel>();

        GameObject shopBtn = transform.Find("ShopBtn").gameObject;
        InputManager.instance.AddClickListener(shopBtn, (go) =>
        {

        });        
    }

    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        Transform child = _characterRoot.transform.Find(resName);
        if (child != null)
        {
            if (child.name.Equals(resName))
            {
                AnimationControl control = child.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
                return;
            } 
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                o.transform.parent = _characterRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
}
