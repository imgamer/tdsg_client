﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 主角穿戴装备Item
/// </summary>
public class KitbagEquipItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                _icon.spriteName = value;
            }
        }
    }

    private UISprite _quality;
    public string Quality
    {
        set
        {
            _quality.spriteName = value;
        }
    }
    private UILabel _level;
    public int Level
    {
        set
        {
            if (value <= 0)
            {
                _level.text = "";
            }
            else
            {
                _level.text = string.Format("Lv.{0}", value);
            }
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _icon.enabled = false;
        _quality = transform.Find("Quality").GetComponent<UISprite>();
        _level = transform.Find("Level").GetComponent<UILabel>();
    }
}
