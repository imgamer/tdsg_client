﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Item;
using System.Linq;
public class KitbagTaskPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private bool _inited = false;   //判断界面是否第一次打开
    private int _lastTaskIndex = 0;
    private SortedList<Int32, KitbagItem> _emptyTaskItem = new SortedList<Int32, KitbagItem>(); //背包中间空位置Item
    private Dictionary<ITEM_ORDER, KitbagItem> _taskOrderItem = new Dictionary<ITEM_ORDER, KitbagItem>(); //物品索引
    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        Initpage();

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddItem);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveItem);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItemAttribute); 
    }

    protected override void OnOpen(params object[] args)
    {
        if (!_inited)
        {
            ArrangeTaskItem();
            _inited = true;
        }
        _selectBg.SetActive(false);
        _uiGrid.GoToItem(0, _Num);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, AddItem);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, RemoveItem);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItemAttribute);
    }

    private const int _Num = 60;
    private UI.Grid _uiGrid;
    private KitbagItem[] _taskItems = new KitbagItem[_Num]; //任务物品

    private GameObject _selectBg;
    private void Initpage()
    {
        _selectBg = transform.Find("SelectBg").gameObject;

        Transform grid = transform.Find("Scroll/Grid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, _Num);

        for (int i = 0; i < _Num; i++)
        {
            Transform task = grid.GetChild(i);
            task.name = i.ToString();

            KitbagItem taskItem = task.GetComponent<KitbagItem>();
            taskItem.Init(ParentUI);
            _taskItems[i] = taskItem;
        }
        _uiGrid.Reposition();

        GameObject arrangeBtn = transform.Find("ArrangeBtn").gameObject;
        InputManager.instance.AddClickListener(arrangeBtn, (go) =>
        {
            ArrangeTaskItem();
        });
    }

    private void SelectItem(KitbagItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void RefreshItem(List<ItemBase> Items)
    {
        foreach (ItemBase baseData in Items)
        {
            AddTaskItem(baseData);
        }
    }

    private void AddTaskItem(ItemBase data)
    {
        if (_emptyTaskItem.Count > 0)
        {
            KitbagItem emptyItem = _emptyTaskItem.Values.First();
            SetItemData(emptyItem, data);
            _taskOrderItem.Add(data.Order, emptyItem);
            _emptyTaskItem.RemoveAt(0);
        }
        else
        {
            SetItemData(_taskItems[_lastTaskIndex], data);
            if (_taskOrderItem.ContainsKey(data.Order))
            {
                Printer.LogError("TaskBagPage:: _taskOrderItem " + data.Order.ToString() + " Have  Same Key");
            }
            _taskOrderItem.Add(data.Order, _taskItems[_lastTaskIndex]);
            _lastTaskIndex++;
        }
    }

    private void SetItemData(KitbagItem item, ItemBase itemData)
    {
        //跨图集
        ParentUI.SetDynamicSpriteName(item.GoodsSprite, itemData.Icon);
        item.GoodsObj.SetActive(true);
        if (itemData.Amount > 1)
        {
            item.Count = Convert.ToString(itemData.Amount);
            item.CountObj.SetActive(true);
        }
        else
        {
            item.CountObj.SetActive(false);
        }
        ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Left, SourcePage.Goods, itemData, (go) =>
            {
                SelectItem(item);
            });
    }

    private void RemoveTaskItem(KitbagItem item)
    {
        item.CountObj.SetActive(false);
        item.GoodsObj.SetActive(false);
        ItemDetailManager.instance.RemoveListenerItemDetail(item.gameObject);
    }

    private void ArrangeTaskItem()
    {
        _lastTaskIndex = 0;
        _emptyTaskItem.Clear();
        foreach(KitbagItem item in _taskOrderItem.Values)
        {
            RemoveTaskItem(item);
        }
        _taskOrderItem.Clear();
        List<ItemBase> Items = _avatar.kitbagModule.ChildTypeItems(ItemTypeDefine.ITEM_QUEST);
        Items.Sort(delegate(ItemBase a, ItemBase b)
        {
            return ((Int32)a.ID).CompareTo((Int32)b.ID);
        });
        RefreshItem(Items);
    }

    private void AddItem(ITEM_ORDER order)
    {
        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if (data == null)
        {
            Printer.LogError(string.Format("KitbagTaskPage::AddGoods ORDER: {0} not find"), order);
            return;
        }
        if (data.ChildType != ItemTypeDefine.ITEM_QUEST)
        {
            return;
        }
        AddTaskItem(data);
    }

    private void RemoveItem(ITEM_ORDER order)
    {
        KitbagItem item;
        _taskOrderItem.TryGetValue(order, out item);
        if (item == null) return;
        RemoveTaskItem(item);
        _emptyTaskItem.Add(item.Index, item);
        _taskOrderItem.Remove(order);
    }

    private void UpdateItemAttribute(ITEM_ORDER order)
    {
        ItemBase data = _avatar.kitbagModule.GetItemByOrder(order);
        if (data == null)
        {
            Printer.LogError(string.Format("KitbagTaskPage::UpdateGoodsAttribute ORDER: {0} not find"), order);
            return;
        }
        if (data.ChildType != ItemTypeDefine.ITEM_QUEST)
        {
            return;
        }
        KitbagItem item;
        _taskOrderItem.TryGetValue(order, out item);
        SetItemData(item, data);
    }
}
