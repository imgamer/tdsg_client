﻿using UnityEngine;
using System.Collections;
using EQUIPMENT_BAG_TYPE = System.Collections.Generic.Dictionary<EQUIPMENT_LOCATION, Item.ItemEquipment>;
/// <summary>
/// 主角穿戴装备Page
/// </summary>
public class KitbagEquipPage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_PUTON, UpdateItem);
        EventManager.AddListener<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_TAKEOFF, UpdateItem);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItem();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_PUTON, UpdateItem);
        EventManager.RemoveListener<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_TAKEOFF, UpdateItem);
    }

    private const int Num = 8;
    private KitbagEquipItem[] _items = new KitbagEquipItem[Num];
    private void InitItem()
    {
        Transform grid = transform.Find("UIGrid");
        for (int i = 0; i < Num; i++)
        {
            KitbagEquipItem item = grid.Find(i.ToString()).GetComponent<KitbagEquipItem>();
            _items[i] = item;
        }
    }

    private void UpdateItem()
    {
        EQUIPMENT_BAG_TYPE equips = GameMain.Player.equipModule.GetEquipments();
        for (int i = 0; i < Num; i++)
        {
            KitbagEquipItem item = _items[i];
            item.Init(ParentUI);
            EQUIPMENT_LOCATION location = GetLocation(i);
            Item.ItemEquipment equip;
            if(equips.TryGetValue(location, out equip))
            {
                item.Icon = equip.Icon;
                ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Right, SourcePage.Avatar, equip);
            }
            else
            {
                item.Icon = string.Empty;
                ItemDetailManager.instance.RemoveListenerItemDetail(item.gameObject);
            }
        }
    }
    private EQUIPMENT_LOCATION GetLocation(int index)
    {
        switch (index)
        {
            case 0:
                return Define.EB_HELMET;
            case 1:
                return Define.EB_NECKLACE;
            case 2:
                return Define.EB_ARMOR;
            case 3:
                return Define.EB_BOOT;
            case 4:
                return Define.EB_WEAPON;
            case 5:
                return Define.EB_RING;
            case 6:
                return Define.EB_ERROR_LOCATION;
            case 7:
                return Define.EB_ERROR_LOCATION;
            default:
                return Define.EB_ERROR_LOCATION;
        }
    }

    private void UpdateItem(EQUIPMENT_LOCATION location)
    {
        if (!Active) return;
        UpdateItem();
    }
}
