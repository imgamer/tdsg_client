﻿using DG.Tweening;
using UnityEngine;
using System.Collections.Generic;
using System;

static class SceneEntityEffect
{
    //实体表现次数记录
    private static Dictionary<int, int> DamageMap = new Dictionary<int, int>();
    public static void ShowDamage(this SceneEntity se, List<int> damageDesc)
    {
        var type = damageDesc[0];
        var damage = damageDesc[1];
        se.ChangeHP(-damage, type);
    }

    public static void CalculateDamage(this SceneEntity se, List<int> damageDesc)
    {
        int id = se.GetInstanceID();
        int num;
        if (DamageMap.TryGetValue(id, out num))
        {
            if (num > 0)
            {
                num--;
                if (num == 0)
                {
                    DamageMap.Remove(id);
                }
                else
                {
                    DamageMap[id] = num;
                }
                return;
            }
        }
        //实体表现次数小于数据次数，矫正
        Printer.LogError("Seq表现出错，数据矫正！");
        var type = damageDesc[0];
        var damage = damageDesc[1];
        se.ChangeHP(-damage, type);
    }

    public static void OnDamageTriggerShow(this SceneEntity se)
    {
        SkillManager.instance.PlayShow(Define.SKILL_RESULT_TARGET_DAMAGE, null);
    }

    public static void ShowTrigger(this SceneEntity se, FightShowData data, int type, Action cb)
    {
        int id = se.GetInstanceID();
        int num;
        if (!DamageMap.TryGetValue(id, out num))
        {
            num = 0;
            DamageMap[id] = num;
        }
        //实体表现次数
        num++;
        DamageMap[id] = num;

        switch (type)
        {
            case Define.SKILL_DAMAGE_TYPE_CRIT:
                SeqManager.instance.PlaySeq("seq_crit", se.KBEntity.id, data,
                    CameraManager.instance.CurCamera.transform, (s) =>
                    {
                        if (s) AssetsManager.instance.Despawn(s.gameObject);
                        if (cb != null) cb();
                    });
                break;
            case Define.SKILL_DAMAGE_TYPE_DODGE:
                SeqManager.instance.PlaySeq("seq_dodge", se.KBEntity.id, data,
                    CameraManager.instance.CurCamera.transform, (s) =>
                    {
                        if (s) AssetsManager.instance.Despawn(s.gameObject);
                        if (cb != null) cb();
                    });
                break;
            case Define.SKILL_DAMAGE_TYPE_ORDINARY:
                SeqManager.instance.PlaySeq("seq_damage", se.KBEntity.id, data,
                    CameraManager.instance.CurCamera.transform, (s) =>
                    {
                        if (s) AssetsManager.instance.Despawn(s.gameObject);
                        if (cb != null) cb();
                    });
                break;
            default:
                Printer.LogError("Unkown Type.");
                if (cb != null) cb();
                break;
        }
    }
}