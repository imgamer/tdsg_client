﻿using UnityEngine;
using System.Collections;
using Skill;
using DG.Tweening;
/// <summary>
/// 其他玩家模型封装
/// </summary>
public class RoleEntity : SceneEntity
{
    public override EntityType Type
    {
        get { return EntityType.RoleEntity; }
    }

    protected override void OnEnterWorld()
    {
        base.OnEnterWorld();
        Navigate = new RoleNavigate(this);
    }

    protected override void OnLeaveWorld()
    {
        base.OnLeaveWorld();
        Navigate.Stop();
    }

    public override void SyncFightState()
    {
        base.SyncFightState();
        switch (KBEntity.FightState)
        {
            case Define.ENTITY_STATE_FIGHT_FREE:
                SetAlpha(1);
                AnimControl.SetPrepare();
                break;
            case Define.ENTITY_STATE_FIGHT_READYING:
                break;
            case Define.ENTITY_STATE_FIGHT_READYED:
                break;
            case Define.ENTITY_STATE_FIGHT_CAST:
                break;
            case Define.ENTITY_STATE_FIGHT_DEAD:
                AnimControl.PlayDeath();
                DOTween.To(x => AnimControl.SkeletonAtr.Skeleton.A = x, 1.0f, 0.0f, 2.0f).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    SkillManager.instance.PlayShow(Define.SKILL_RESULT_TARGET_DEAD, null);
                });
                break;
            case Define.ENTITY_STATE_FIGHT_WOUNDED:
                SetAlpha(0.5f);
                SkillManager.instance.PlayShow(Define.SKILL_RESULT_TARGET_DEAD, null);
                break;
            default:
                break;
        }
    }
}
