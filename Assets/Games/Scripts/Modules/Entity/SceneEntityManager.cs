﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 场景实体管理类
/// </summary>
public class SceneEntityManager : MonoSingleton<SceneEntityManager>
{

    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {

    }

    public void Create(Transform parent, KBEngine.GameObject entity, PoolName poolName, PoolType poolType, Action<SceneEntity> cb)
    {
        if (parent == null || entity == null)
        {
            if (cb != null) cb(null);
            return;
        }
        OnCreate(parent, entity, poolName, poolType, cb);
    }
    private void OnCreate(Transform parent, KBEngine.GameObject entity, PoolName poolName, PoolType poolType, Action<SceneEntity> cb)
    {
        GameObject shell = null;
        GameObject model = null;
        string sceneEntityName = GetSceneEntityName(entity);
        BaseSpawnData data0 = new InstantiateData(sceneEntityName, sceneEntityName, PoolName.Permanent, PoolType.Once, (o) =>
        {
            shell = o;
        });
        string modelName = entity.ModelID;
        BaseSpawnData data1 = new InstantiateData(modelName, modelName, poolName, poolType, (o) =>
        {
            model = o;
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data0, data1 }, (ok) => 
        {
            if(ok)
            {
                if (parent == null || entity == null || shell == null || model == null)
                {
                    AssetsManager.instance.Despawn(shell);
                    AssetsManager.instance.Despawn(model);
                    if (cb != null) cb(null);
                    return;
                }
                shell.transform.parent = parent;
                SceneEntity sceneEntity = shell.GetComponent<SceneEntity>();
                sceneEntity.Init();
                sceneEntity.SetEntity(entity, model.transform);
                if (cb != null) cb(sceneEntity);
            }
            else
            {
                if (cb != null) cb(null);
            }
        });
    }
    private string GetSceneEntityName(KBEngine.GameObject entity)
    {
        if (entity.isPlayer())
        {
            return "player";
        }
        else if (entity.GetType() == typeof(KBEngine.Avatar))
        {
            return "role";
        }
        else if (entity.GetType() == typeof(KBEngine.Hero))
        {
            return "hero";
        }
        else if (entity.GetType() == typeof(KBEngine.Monster))
        {
            return "monster";
        }
        else if (entity.GetType() == typeof(KBEngine.NPC))
        {
            return "npc";
        }
        else if (entity.GetType() == typeof(KBEngine.VisibleMonster))
        {
            return "monster";
        }
        else if (entity.GetType() == typeof(KBEngine.VisibleNPC))
        {
            return "npc";
        }
        else if (entity.GetType() == typeof(KBEngine.Pet))
        {
            return "pet";
        }
        else if (entity.GetType() == typeof(KBEngine.OfflineObject))
        {
            return "offline";
        }
        return string.Empty;
    }

}
