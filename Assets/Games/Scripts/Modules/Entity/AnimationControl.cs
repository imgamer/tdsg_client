﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AnimationControl : MonoBehaviour
{
    private Animator animator;
    public SkeletonAnimator SkeletonAtr { get; private set; }
    public Action<Status> OnStatus; 
    public static readonly List<int> VALID_ANGLES = new List<int>() { -180, -135, -90, -45, 0, 45, 90, 135, 180 };
    public static readonly Dictionary<int, int> ROTATE_MAP = new Dictionary<int, int>() { { 0, 180 }, { -180, 180 }, { 45, 135 }, { -45, -135 } };
    
    public void Init()
    {
        animator = gameObject.GetComponent<Animator>();
        SkeletonAtr = gameObject.GetComponent<SkeletonAnimator>();
        if (animator == null)
        {
            Printer.LogError("Animator Component not found");
        }
    }

    private void SetStatus(Status s)
    {
        if (OnStatus != null) OnStatus(s);
    }

	// codes from http://answers.unity3d.com/questions/519818/check-available-variables-of-mecanim-animator.html
	private bool HasParameterOfType (string name, AnimatorControllerParameterType type) 
	{
		var parameters = animator.parameters;
        for (int i = 0; i < parameters.Length; i++)
        {
            var param = parameters[i];
            if (param.type == type && param.name == name)
            {
                return true;
            }
        }
		return false;
	}

    private void SetRotation(int dir)
    {
        var needFlip = dir > -90 && dir < 90;
        transform.localScale = new Vector3(needFlip ? -1 : 1, 1, 1);
        if(ROTATE_MAP.ContainsKey(dir))
        {
            dir = ROTATE_MAP[dir];
        }
        animator.SetInteger("rundirection", dir);
    }

    public void SetDirection(Vector3 direction)
    {
        int dir = ClampAngle(direction.y);
        SetRotation(dir);
        if (!animator.GetBool("run"))
        {
            animator.SetTrigger("idle");
            SetStatus(Status.Idle);
        } 
    }

    public void SetIdle(int dir)
    {
        SetRotation(dir);
        animator.SetTrigger("idle");
        SetStatus(Status.Idle);
    }

    public void BeginRun()
    {
        if(!animator.GetBool("run"))
        {
            animator.SetBool("run", true);
            SetStatus(Status.Run);
        }
    }
    public void BeginRun(Vector3 destination)
    {
        int dir = CalculateDirection(transform.position, destination);
        SetRotation(dir);
        animator.SetBool("run", true);
        SetStatus(Status.Run);
    }

    public void EndRun()
    {
        animator.SetBool("run", false);
        SetStatus(Status.Idle);
    }

	public void LookAt(Vector3 destination)
	{
        int dir = CalculateDirection(transform.position, destination);
        SetIdle(dir);
	}

    #region 攻击
    private Action onAttackCall;
    public void PlayAttack(string skillName, Action cb)
    {
        onAttackCall = cb;
        animator.SetTrigger(skillName);
        SetStatus(Status.Attack);
    }
    private void AttackCallBack()
    {
        if (onAttackCall != null) onAttackCall();
    }
    #endregion

    public void PlayHit()
    {
        animator.SetTrigger("hit");
        SetStatus(Status.Hit);
    }

    public void PlayDeath()
    {
        animator.SetTrigger("death");
        SetStatus(Status.Death);
    }

    public void SetPrepare()
    {
        animator.SetTrigger("prepare");
        SetStatus(Status.Prepare);
    }

    public bool GetDirection(out Vector3 direction)
    {
        //NPC，怪物等Animator没有rundirection这个属性
        if (!HasParameterOfType("rundirection", AnimatorControllerParameterType.Int))
        {
            direction = new Vector3(0, transform.localScale.x > 0 ? 180 : 0, 0);
            return true;
        }
        var dir = animator.GetInteger("rundirection");
        //镜像情况下
        if (transform.localScale.x < 0)
        {
            foreach (var item in ROTATE_MAP)
            {
                if (item.Value == dir)
                {
                    dir = item.Key;
                    direction = new Vector3(0, dir, 0);
                    return true;
                }  
            }
            direction = Vector3.zero;
            return false;
        }
        direction = new Vector3(0, dir, 0);
        return true;
    }
	// Directions used by animator are shown below:
	//   (135) (90)(45)
	//        \ | /
	//         \|/    
	// (180)----*---- (0)
	//         /|\
	//        / | \ 
	//  (-135)(-90)(-45)
	private int CalculateDirection(Vector3 start, Vector3 dest)
	{
		if(Vector2.Distance(start, dest) < Mathf.Epsilon)
			return 0;
		
		Vector2 dir = dest - start;
		//弧度
		float radian = Mathf.Atan2(dir.y, dir.x);
		//角度
		float angle = radian * Mathf.Rad2Deg;

        return ClampAngle(angle);
	}

    private int ClampAngle(float angle)
    {
        const float range = 22.5f;
        int ret = 0;

        if(VALID_ANGLES.Contains((int)angle))
        {
            ret = (int)angle;
            return ret;
        }

        for (int i = 0; i < VALID_ANGLES.Count; i++ )
        {
            var desiredAngle = VALID_ANGLES[i];
            if (angle > desiredAngle - range && angle < desiredAngle + range)
            {
                ret = desiredAngle;
                break;
            }
        }
        return ret;
    }

}

