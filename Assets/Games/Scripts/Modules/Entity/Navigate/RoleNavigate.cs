﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
/// <summary>
/// 其他角色寻路导航类
/// </summary>
public class RoleNavigate : BaseNavigate
{
    public RoleNavigate(SceneEntity owner) : base(owner) { }

    protected override void OnStart(NavigateType type, Vector2 dest, float dist)
    {
        switch (type)
        {
            case NavigateType.Initiative:
                InitiativeNavigate();
                break;
            case NavigateType.Passive:
                _posQueue.Enqueue(Owner.KBEntity.Position);
                if (Navigating) return;
                PassiveNavigate();
                break;
            default:
                break;
        }
    }

    private void InitiativeNavigate()
    {
        Printer.LogError("其他角色无法主动寻路!");
    }

    private Queue<Vector3> _posQueue = new Queue<Vector3>();
    private void PassiveNavigate()
    {
        Vector3 targetPos = _posQueue.Dequeue();
        float duration = Vector2.Distance(Owner.GetPosition(), targetPos) / Owner.MoveSpeed;
        Owner.SetDirection(Owner.KBEntity.direction);
        pathTweener = TweenTools.DOLocalMove(Owner.transform, targetPos, duration, null, () =>
        {
            if (_posQueue.Count > 0)
            {
                targetPos = _posQueue.Dequeue();
                duration = Vector2.Distance(Owner.GetPosition(), targetPos) / Owner.MoveSpeed;
                Owner.SetDirection(Owner.KBEntity.direction);
                pathTweener.ChangeEndValue(targetPos, duration, true);
                pathTweener.Play();
            }
            else
            {
                TweenTools.DOKill(ref pathTweener);
                Owner.AnimControl.EndRun();
            }
        });
        pathTweener.OnStart(() =>
        {
            Owner.AnimControl.BeginRun();
        });
    }


}
