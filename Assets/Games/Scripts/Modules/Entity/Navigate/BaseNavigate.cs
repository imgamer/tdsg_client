﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using System.Collections.Generic;
/// <summary>
/// 寻路导航基类
/// </summary>
public abstract class BaseNavigate 
{
    public enum NavigateType
    {
        Initiative, //主动
        Passive,    //被动
    }
    public SceneEntity Owner { get; private set; }
    protected Action onComplete;
    protected Action onInterrupt;
    protected Vector2 destination;  //寻路目标点
    protected float distance;       //与目标点间隔

    protected Tweener pathTweener;
    protected List<Vector3> pathList = new List<Vector3>();//寻路路径点
    protected float length;       //寻路路程

    public BaseNavigate(SceneEntity owner)
    {
        Owner = owner;
        destination = Owner.GetPosition();
    }

    public bool Valid
    {
        get { return NavigateUtils.Valid; }
    }

    public bool Navigating
    {
        get { return pathTweener != null; }
    }

    public Vector2 GetDestination()
    {
        return destination;
    }

    public float GetDistance()
    {
        return distance;
    }

    /// <summary>
    /// 主动被动寻路
    /// </summary>
    /// <param name="type"></param>
    /// <param name="dest"></param>
    /// <param name="dist"></param>
    /// <param name="onComplete"></param>
    /// <param name="onInterrupt"></param>
    public void Start(NavigateType type, Vector2 dest, float dist = 0, Action onComplete = null, Action onInterrupt = null)
    {
        if (destination == dest && Navigating) return;
        if (dist > 0 && Vector2.Distance(Owner.GetPosition(), dest) <= dist)
        {
            this.onComplete = onComplete;
            this.onInterrupt = onInterrupt;
            Complete();
            return;
        }

        Interrupt();
        destination = dest;
        distance = dist;
        this.onComplete = onComplete;
        this.onInterrupt = onInterrupt;
        OnStart(type, destination, distance);
    }
    protected abstract void OnStart(NavigateType type, Vector2 dest, float dist);

    /// <summary>
    /// 组队跟随
    /// </summary>
    /// <param name="positions"></param>
    /// <param name="dist"></param>
    /// <param name="onComplete"></param>
    /// <param name="onInterrupt"></param>
    public void Follow(List<Vector2> positions, float dist, Action onComplete = null, Action onInterrupt = null)
    {
        int num = positions.Count;
        if (num < 2) return;
        Vector2 dest = positions[num - 1];
        if (destination == dest && Navigating) return;

        Interrupt();
        destination = dest;
        distance = dist;
        this.onComplete = onComplete;
        this.onInterrupt = onInterrupt;

        pathList = NavigateUtils.GetSmoothedPath(positions, distance, out length);
        int count = pathList.Count;
        if (count >= 2)
        {
            float duration = this.length / Owner.MoveSpeed;

            //此处DoPath有Bug，onWayPointChanged回调的Index有时并非从0开始。
            Owner.AnimControl.BeginRun(pathList[1]);
            Owner.SendDirection();
            pathTweener = TweenTools.DOPath(Owner.transform, pathList.ToArray(), duration, (index) =>
            { // OnWaypointChanged
                if (index > 0 && index + 1 < count)
                {
                    Owner.AnimControl.BeginRun(pathList[index + 1]);
                    Owner.SendDirection();
                }
            }, () =>
            { // OnUpdte
                Owner.SendPosition();
            }, () =>
            { // OnComplete
                Complete();
            });
        }
        else
        {
            Stop();
        }
    }

    /// <summary>
    /// 巡逻
    /// </summary>
    /// <param name="onInterrupt"></param>
    public virtual void Patrol(Action onInterrupt = null)
    {

    }

    protected void Interrupt()
    {
        if (Navigating)
        {
            //打断寻路
            if (onInterrupt != null)
            {
                Action cb = onInterrupt;
                onInterrupt = null;
                cb();
            }
        }
    }

    public void Stop()
    {
        if (Navigating)
        {
            Owner.AnimControl.EndRun();
            pathList.Clear();
            TweenTools.DOKill(ref pathTweener);
            //打断寻路
            if (onInterrupt != null)
            {
                Action cb = onInterrupt;
                onInterrupt = null;
                cb();
            }
        }
    }
    protected void Complete()
    {
        if (Navigating)
        {
            Owner.AnimControl.EndRun();
            pathList.Clear();
            TweenTools.DOKill(ref pathTweener);
        }
        //完成寻路
        if (onComplete != null)
        {
            Action cb = onComplete;
            onComplete = null;
            cb();
        }
    }

}
