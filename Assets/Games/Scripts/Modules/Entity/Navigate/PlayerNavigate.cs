﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;
/// <summary>
/// 玩家寻路导航类
/// </summary>
public class PlayerNavigate : BaseNavigate
{
    public PlayerNavigate(SceneEntity owner) : base(owner) { }

    protected override void OnStart(NavigateType type, Vector2 dest, float dist)
    {
        switch (type)
        {
            case NavigateType.Initiative:
                if (!GameMain.Player.teamModule.IsFree()) return;
                pathList = NavigateUtils.GetSmoothedPath(Owner.GetPosition(), dest, dist, out length);
                InitiativeNavigate();
                break;
            case NavigateType.Passive:
                _posQueue.Enqueue(Owner.KBEntity.Position);
                if (Navigating) return;
                PassiveNavigate();
                break;
            default:
                break;
        }
    }
    private void InitiativeNavigate()
    {
        Transform trans = Owner.transform;
        int count = pathList.Count;
        if (count >= 2)
        {
            float duration = length / Owner.MoveSpeed;

            //此处DoPath有Bug，onWayPointChanged回调的Index有时并非从0开始。
            Owner.AnimControl.BeginRun(pathList[1]);
            Owner.SendDirection();
            pathTweener = TweenTools.DOPath(trans, pathList.ToArray(), duration, (index) =>
            { // OnWaypointChanged
                if (index > 0 && index + 1 < count)
                {
                    Owner.AnimControl.BeginRun(pathList[index + 1]);
                    Owner.SendDirection();
                }
            }, () =>
            { // OnUpdte
                CameraManager.instance.FollowTarget(trans.position);
                Owner.SendPosition();
            }, () =>
            { // OnComplete
                CameraManager.instance.AlignTarget(trans.position, 0.5f);
                Complete();
            });
        }
        else
        {
            CameraManager.instance.AlignTarget(trans.position, 0.5f);
            Stop();
        }
    }

    private Queue<Vector3> _posQueue = new Queue<Vector3>();
    private void PassiveNavigate()
    {
        Transform trans = Owner.transform;
        Vector3 targetPos = _posQueue.Dequeue();
        float duration = Vector2.Distance(Owner.GetPosition(), targetPos) / Owner.MoveSpeed;
        Owner.SetDirection(Owner.KBEntity.direction);
        pathTweener = TweenTools.DOLocalMove(trans, targetPos, duration, () => 
        {
            CameraManager.instance.FollowTarget(trans.position);
        }, () =>
        {
            if (_posQueue.Count > 0)
            {
                targetPos = _posQueue.Dequeue();
                duration = Vector2.Distance(Owner.GetPosition(), targetPos) / Owner.MoveSpeed;
                Owner.SetDirection(Owner.KBEntity.direction);
                pathTweener.ChangeEndValue(targetPos, duration, true);
                pathTweener.Play();
            }
            else
            {
                CameraManager.instance.AlignTarget(trans.position, 0.5f);
                TweenTools.DOKill(ref pathTweener);
                Owner.AnimControl.EndRun();
            }
        });
        pathTweener.OnStart(() =>
        {
            Owner.AnimControl.BeginRun();
        });
    }

    private Action cb = null;
    public override void Patrol(Action onInterrupt = null)
    {
        base.Patrol();
        Interrupt();
        distance = 0;
        this.onComplete = null;
        this.onInterrupt = onInterrupt;

        Transform trans = Owner.transform;
        cb = () => 
        {
            pathList = NavigateUtils.GetRandomSmoothedPath(Owner.GetPosition(), out length);
            int count = pathList.Count;
            if (count != 0)
            {
                destination = pathList[count - 1];
                float duration = length / Owner.MoveSpeed;
                // Do path finding
                pathTweener = TweenTools.DOPath(trans, pathList.ToArray(), duration, (index) =>
                { // OnWaypointChanged
                    if (index < count - 1)
                    {
                        Owner.AnimControl.BeginRun(pathList[index + 1]);
                        Owner.SendDirection();
                    }
                }, () =>
                { // OnUpdte
                    CameraManager.instance.FollowTarget(trans.position);
                    Owner.SendPosition();
                }, () =>
                { // OnComplete
                    cb();
                });
            }
            else
            {
                //不可移动的区域
                CameraManager.instance.AlignTarget(trans.position, 0.5f);
                Stop();
            }
        };
        cb();
    }
}
