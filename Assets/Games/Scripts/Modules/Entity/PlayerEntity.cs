﻿using UnityEngine;
using System;
using System.Collections;
using Skill;
using DG.Tweening;
/// <summary>
/// 玩家自己模型封装
/// </summary>
public class PlayerEntity : SceneEntity
{
    public override EntityType Type
    {
        get { return EntityType.PlayerEntity; }
    }

    protected override void OnEnterWorld()
    {
        base.OnEnterWorld();
        Navigate = new PlayerNavigate(this);
    }

    protected override void OnLeaveWorld()
    {
        (KBEntity as KBEngine.Avatar).spaceNavigator.Pause();
        Navigate.Stop();
    }

    public override void SetPosition(Vector3 pos)
    {
        base.SetPosition(pos);
        CameraManager.instance.AlignTarget(pos, 0);
    }

    protected override void OnStatus(Status s)
    {
        base.OnStatus(s);
        switch (s)
        {
            case Status.Idle:
                SyncMovingState(false);
                break;
            case Status.Run:
                SyncMovingState(true);
                break;
            case Status.Prepare:
                break;
            case Status.Attack:
                break;
            case Status.Hit:
                break;
            case Status.Death:
                break;
            default:
                break;
        }
        EventManager.Invoke<Status>(EventID.EVNT_PLAYERENTITY_STATUS, Curstatus);
    }

    public override void SyncFightState()
    {
        base.SyncFightState();
        switch (KBEntity.FightState)
        {
            case Define.ENTITY_STATE_FIGHT_FREE:
                SetAlpha(1);
                AnimControl.SetPrepare();
                break;
            case Define.ENTITY_STATE_FIGHT_READYING:
                break;
            case Define.ENTITY_STATE_FIGHT_READYED:
                break;
            case Define.ENTITY_STATE_FIGHT_CAST:
                break;
            case Define.ENTITY_STATE_FIGHT_DEAD:
                AnimControl.PlayDeath();
                DOTween.To(x => AnimControl.SkeletonAtr.Skeleton.A = x, 1.0f, 0.0f, 2.0f).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    SkillManager.instance.PlayShow(Define.SKILL_RESULT_TARGET_DEAD, null);
                });
                break;
            case Define.ENTITY_STATE_FIGHT_WOUNDED:
                SetAlpha(0.5f);
                SkillManager.instance.PlayShow(Define.SKILL_RESULT_TARGET_DEAD, null);
                break;
            default:
                break;
        }
    }

    #region 服务端状态
    private void SyncMovingState(bool isMoving)
    {
        if (KBEntity != null) KBEntity.setMovingState(isMoving);
    }
    #endregion

    /// <summary>
    /// 传送到指定地图的出生点
    /// </summary>
    /// <param name="spaceUType"></param>
    public void TeleportSpaceSpawnPoint(int spaceUType)
    {
        KBEntity.cellCall("teleportSpawnPoint", (UInt32)spaceUType);
    }
}
