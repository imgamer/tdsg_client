﻿using UnityEngine;
using System.Collections;
using System;

public enum EntityType
{
    PlayerEntity,
    RoleEntity,
    HeroEntity,
    PetEntity,
    MonsterEntity,
    NPCEntity,
    BossEntity,
    OfflineEntity,
}

public enum Status
{
    //普通场景
    Idle,    //默认状态
    Run,     //寻路状态

    //战斗场景
    Prepare, //准备状态
    Attack,  //攻击状态
    Hit,     //受击状态
    Death,   //死亡状态
}

/// <summary>
/// 场景实体封装
/// </summary>
public abstract class SceneEntity : MonoBehaviour
{
    public abstract EntityType Type { get; }
    public Status Curstatus { get; private set; }
    public int SortingOrder { get; private set; }
    //数据实体
    private KBEngine.GameObject _kbeEntity;
    public KBEngine.GameObject KBEntity
    {
        get { return _kbeEntity; }
    }
    //资源实体
    private GameObject _entity;
    public GameObject Entity { set { _entity = value; } }
    private AnimationControl animControl;
    public AnimationControl AnimControl { get { return animControl; } }
    public BaseNavigate Navigate { get; protected set; }

    private Transform _entityRoot;
    public Transform EntityRoot { get { return _entityRoot; } }
    private Transform _collider;
    public GameObject Collider { get { return _collider.gameObject; } }
    private Transform _topRoot;
    public Transform TopRoot { get { return _topRoot; } }
    private Transform _centerRoot;
    public Transform CenterRoot { get { return _centerRoot; } }
    private Transform _bottomRoot;
    public Transform BottomRoot { get { return _bottomRoot; } }

    public readonly float MoveSpeed = 2.0f;

    public bool IsDefaultDirection { get { return _kbeEntity.direction.y == 180;} }

    #region 是否有效
    public bool Valid 
    {
        get { return KBEntity != null; }
    }
    #endregion

    #region 初始化
    private bool _inited = false;
    public void Init()
    {
        if (_inited) return;
        _inited = true;
        Transform root = transform.Find("Root");
        _entityRoot = root.Find("EntityRoot");
        _topRoot = root.Find("TopRoot");
        _centerRoot = root.Find("CenterRoot");
        _bottomRoot = root.Find("BottomRoot");
        _collider = transform.Find("Collider");
        OnInit();
    }
    protected virtual void OnInit() { }
    #endregion

    #region 重置实体
    public void ResetEntity()
    {
        if (_kbeEntity != null) _kbeEntity.SetSceneEntity(null);
        _kbeEntity = null;
    }
    #endregion

    #region 设置实体
    public void SetEntity(KBEngine.GameObject kbeEntity, Transform entity)
    {
        if (kbeEntity == null || entity == null)
        {
            Printer.LogError("参数为空！");
            return;
        }

		ResetEntity();
		_kbeEntity = kbeEntity;
		_kbeEntity.SetSceneEntity(this);
        _entity = entity.gameObject;

        
        animControl = _entity.GetComponent<AnimationControl>();
        animControl.Init();
        animControl.OnStatus = SetStatus;
        SetStatus(Status.Idle);

        _entity.transform.parent = _entityRoot;
        _entity.transform.localPosition = Vector3.zero;
        _entity.transform.localEulerAngles = Vector3.zero;
        _entity.transform.localScale = Vector3.one;
        SetAlpha(1.0f);
        SetLayer(GameUtils.LayerEntity);
        SetSortingOrder(Const.EntitySortingOrder);
        SetPosition(_kbeEntity.Position);
        SetDirection(_kbeEntity.direction);
        OnSetEntity();
    }
    protected virtual void OnSetEntity() { }
    #endregion

    #region 设置当前状态(注意:区分玩家，角色，英雄，宠物，怪物，NPC)
    public void SetStatus(Status s)
    {
        if (Curstatus == s) return;
        Curstatus = s;
        OnStatus(s);
    }
    protected virtual void OnStatus(Status s) { }
    #endregion

    #region 服务端的状态
    public bool IsMovingState
    {
        get { return KBEntity.InMoving; }
    }
    #endregion

    #region 发送位置方向到服务器
    public void SendPosition()
    {
        _kbeEntity.Position = GetPosition();
    }
    public void SendDirection()
    {
        _kbeEntity.direction = GetDirection();
    }
    #endregion

    #region 血量状态相关
    public void ChangeHP(int changeValue, int type)
    {
        switch (type)
        {
            case Define.SKILL_DAMAGE_TYPE_CRIT:
                KBEntity.ChangeHP(changeValue);
                HUDManager.instance.SetContent(TopRoot, HUDUnit.HUDType.Crit, Mathf.Abs(changeValue).ToString());
                break;
            case Define.SKILL_DAMAGE_TYPE_DODGE:
                HUDManager.instance.SetContent(TopRoot, HUDUnit.HUDType.Dodge, "闪避");
                break;
            case Define.SKILL_DAMAGE_TYPE_ORDINARY:
                KBEntity.ChangeHP(changeValue);
                var numberType = changeValue > 0 ? HUDUnit.HUDType.Treat : HUDUnit.HUDType.Damage;
                HUDManager.instance.SetContent(TopRoot, numberType, Mathf.Abs(changeValue).ToString());
                break;
            default:
                Printer.LogError("Unkown Type.");
                break;
        }
    }

    public void SyncActionValue()
    {
        uint value = KBEntity.ActionValue;
        HUDManager.instance.SetActionValue(TopRoot, (float)value / Define.MAX_ACTION_VALUE);
    }
    public void SyncHPBar()
    {
        int value = KBEntity.HP;
        int max = KBEntity.HP_Max;
        HUDManager.instance.SetHP(TopRoot, (float)value / max);
    }
    public virtual void SyncFightState() { }
    #endregion

    #region 设置位置方向
    public virtual void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public virtual void SetDirection(Vector3 dir)
    {
        animControl.SetDirection(dir);
    }

    public Vector3 GetDirection()
    {
        Vector3 direction;
        if (animControl.GetDirection(out direction))
        {
            return direction;
        }
        Printer.LogError("找不到对应的方向");
        return KBEntity.direction; 
    }

    public void RotateTo(SceneEntity sceneEntity)
    {
        float dirY = 180;
        if (sceneEntity.transform.position.x > this.transform.position.x)
        {
            dirY = 0;
        }
        SetDirection(new Vector3(0, dirY, 0));
    }
    #endregion

    #region 设置透明度
    private float _alpha = 1;
    public void SetAlpha(float alpha)
    {
        _alpha = alpha;
        if (animControl == null) return;
        animControl.SkeletonAtr.Skeleton.A = _alpha;
    }
    #endregion

    #region 设置层
    public void SetLayer(LayerMask layer)
    {
        Transform[] children = _entity.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in children)
        {
            child.gameObject.layer = layer;
        }
    }
    #endregion

    #region 设置渲染Order
    public void SetSortingOrder(int order)
    {
        SortingOrder = order;
        Renderer r = _entity.GetComponent<Renderer>();
        r.sortingOrder = SortingOrder;
    }
    #endregion

    #region 进入离开世界
    public void EnterWorld()
    {
        OnEnterWorld();
    }

    protected virtual void OnEnterWorld() { }

    public void LeaveWorld()
    {
        OnLeaveWorld();
        ResetEntity();
        Despawn();
    }

	public void Despawn()
	{
        SetAlpha(1.0f);
        SetLayer(GameUtils.LayerEntity);
        InputManager.instance.RemoveTapListener(Collider);
		AssetsManager.instance.Despawn(gameObject);
	}

    protected virtual void OnLeaveWorld() { }
    #endregion

}
