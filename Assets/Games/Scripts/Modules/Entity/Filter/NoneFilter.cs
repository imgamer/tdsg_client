﻿using UnityEngine;
using System.Collections;

namespace KBEngine
{
	/// <summary>
	/// 什么都不做的filter
	/// </summary>
	public class NoneFilter : Filter
	{
		public override void OnUpdateVolatileData(Vector3 position, Vector3 direction)
		{ 
			// do nothing
		}

		public override void setPosition(Vector3 position)
        {
            // do nothing
        }

		public override void setDirection(Vector3 direction)
        {
            // do nothing
        }
		
	}
}
