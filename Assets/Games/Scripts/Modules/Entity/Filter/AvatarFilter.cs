﻿using UnityEngine;
using System.Collections;

namespace KBEngine
{
    /// <summary>
    /// like as bigworld client AvatarFilter
    /// 一个听从服务器指挥，在一定的时间忠实的插值移动到服务器指定的位置，常用于服务器更新其它玩家、怪物的坐标及朝向
    /// </summary>
    public class AvatarFilter : Filter
    {
        Transform m_transform;
        SmoothFilter filter;
        Vector3 vLastPos = Vector3.zero;
        Vector3 m_velocity = Vector3.zero;
        CapsuleCollider m_capCollide = null;
        float offsetValue = 0.15f; //用于客户端计算高度的时候为避免脚丫陷地而人为的提高一定的高度

        void Awake()
        {
            m_transform = this.transform;
            filter = new SmoothFilter();
            m_capCollide = GetComponent<CapsuleCollider>();
			//initFilter();
		}

		void OnEnable()
		{
			initFilter();
		}

        void OnDisable()
        {
            SpeedChangedNotify(0);
        }

		void initFilter()
		{
			filter.Init(this);
			vLastPos = m_transform.position;
			float[] localAuxVolatile = { m_transform.forward.x, m_transform.forward.y, m_transform.forward.z };
			filter.input(Time.time - 0.1, 0, 0, m_transform.position, Vector3.zero, localAuxVolatile);
		}

        void Update()
        {
            filter.output(Time.time);

            Vector3 vVelocity = Vector3.zero;
            Vector3 entityVelocity = m_velocity;
            if (Vector3.Magnitude(entityVelocity) > 0.0f)
            {
                vVelocity = m_velocity;
                vVelocity.Normalize();
                Vector3 delta = m_transform.position - vLastPos;
                float speed = Vector3.Magnitude(delta);
                vVelocity *= speed * (1.0f / Time.deltaTime);
            }
            else
            {
                vVelocity = (m_transform.position - vLastPos) * (1.0f / Time.deltaTime);
            }

            Vector3 vHorizontalVelocity = vVelocity;
            vHorizontalVelocity.y = 0.0f;

            float newSpeed = Vector3.Magnitude(vHorizontalVelocity);
            if (newSpeed > 0.1f)
            {
                    SpeedChangedNotify(newSpeed);
            }
            else
            {
                    SpeedChangedNotify(0);
            }

            vLastPos = m_transform.position;

            if (m_capCollide != null)
            {
                RaycastHit hit;
                Vector3 p2 = m_transform.position + Vector3.up * m_capCollide.height * 0.5f;
                if (Physics.Raycast(p2, -m_transform.up, out hit, m_capCollide.height, LayerMask.NameToLayer("Entity")))
                {
                    Vector3 pos = m_transform.position;
                    pos.y = hit.point.y + offsetValue;
                    m_transform.position = pos;
                }
            }
        }


        public override void OnUpdateVolatileData(Vector3 position, Vector3 direction)
        {
            if (m_capCollide != null)
            {
                RaycastHit hit;
                Vector3 p2 = transform.position + Vector3.up * m_capCollide.height * 0.5f;
                if (Physics.Raycast(p2, -m_transform.up, out hit, m_capCollide.height, LayerMask.NameToLayer("Entity")))
                {
                    position.y = hit.point.y + offsetValue;
                }
            }

            direction.Set(0, direction.z, 0);  // 我们只想同步Y轴（朝向）
            Quaternion roation = Quaternion.Euler(direction);
            Vector3 forword = roation * Vector3.forward;

            int sceneID = 0;
            int vehicleID = 0;
            Vector3 err = Vector3.zero;
            float[] localAuxVolatile = { forword.x, forword.y, forword.z };
            filter.input(Time.time, sceneID, vehicleID, position, err, localAuxVolatile);
        }


        public void Pos(Vector3 localPos, Vector3 auxVolatile, Vector3 velocity)
        {
            m_transform.position = localPos;
            m_transform.forward = auxVolatile;
            m_velocity = velocity;
        }


        public override void setPosition(Vector3 position)
        {
			m_transform.position = position;
			initFilter();
        }


        public override void setDirection(Vector3 direction)
        {
			float y = direction.y;
			direction.y = direction.z;
			direction.z = y;
			m_transform.eulerAngles = direction;
			
			initFilter();
        }
    }


    public class SmoothFilter
    {
        public struct StoredInput
        {
            public double time_;
            public int spaceID_;
            public int vehicleID_;
            public Vector3 position_;
            public Vector3 positionError_;
            public Vector3 direction_;
        };

        public struct Waypoint
        {
            public double time_;
            public int spaceID_;
            public int vehicleID_;
            public Vector3 position_;
            public Vector3 direction_;

            public StoredInput storedInput_;

            public void changeCoordinateSystem(int spaceID, int vehicleID)
            {
                if (spaceID_ == spaceID && vehicleID_ == vehicleID)
                    return;

                spaceID_ = spaceID;
                vehicleID_ = vehicleID;
            }
        };

        StoredInput[] storedInputs_ = new StoredInput[8];
        uint currentInputIndex_;

        Waypoint nextWaypoint_;
        Waypoint previousWaypoint_;

        float latency_ = 0;
        float idealLatency_ = 0;
        double timeOfLastOutput_ = 0;
        bool gotNewInput_ = false;
        bool reset_ = true;
        AvatarFilter avatar_;

        static float s_latencyVelocity_ = 1.0f;
        static float s_latencyMinimum_ = 0.1f;
        static float s_latencyFrames_ = 0.0f;
        static float s_latencyCurvePower_ = 2.0f;

        StoredInput getStoredInput(uint index)
        {
            return storedInputs_[(currentInputIndex_ + index) % 8];
        }


        public void Init(AvatarFilter avatar)
        {
			avatar_ = avatar;
            timeOfLastOutput_ = 0;
            this.resetStoredInputs(-2000, 0, 0, new Vector3(0, 0, 0), new Vector3(0, 0, 0), null);
			reset_ = true;
        }


        public void input(double time, int spaceID, int vehicleID, Vector3 position, Vector3 positionError, float[] auxFiltered)
        {
            if (reset_)
            {
                this.resetStoredInputs(time, spaceID, vehicleID, position, positionError, auxFiltered);
                reset_ = false;
            }
            else
            {
                if (time >= this.getStoredInput(0).time_)
                {
                    currentInputIndex_ = (currentInputIndex_ + 7) % 8;

                    storedInputs_[(currentInputIndex_) % 8].time_ = time;
                    storedInputs_[(currentInputIndex_) % 8].spaceID_ = spaceID;
                    storedInputs_[(currentInputIndex_) % 8].vehicleID_ = vehicleID;
                    storedInputs_[(currentInputIndex_) % 8].position_ = position;
                    storedInputs_[(currentInputIndex_) % 8].positionError_ = positionError;

                    if (auxFiltered != null)
                    {
                        storedInputs_[(currentInputIndex_) % 8].direction_.x = auxFiltered[0];
                        storedInputs_[(currentInputIndex_) % 8].direction_.z = auxFiltered[2];
                    }
                    else
                    {
                        storedInputs_[(currentInputIndex_) % 8].direction_.x = 0.0f;
                        storedInputs_[(currentInputIndex_) % 8].direction_.z = 0.0f;
                    }

                    storedInputs_[(currentInputIndex_) % 8].direction_.y = 0.0f;

                    gotNewInput_ = true;
                }
            }
        }


        public void output(double time)
        {
            // adjust ideal latency if we got something new
            if (gotNewInput_)
            {
                gotNewInput_ = false;

                double newestTime = this.getStoredInput(0).time_;
                double olderTime = this.getStoredInput(7).time_;

                s_latencyFrames_ = Mathf.Clamp(s_latencyFrames_, 0, 7);

                double ratio = (7 - s_latencyFrames_) / 7;

                idealLatency_ = (float)(time - Mathf.Lerp((float)olderTime, (float)newestTime, (float)ratio));

                idealLatency_ = Mathf.Max(idealLatency_, s_latencyMinimum_);
            }

            // move latency towards the ideal...
            float dTime = (float)(time - timeOfLastOutput_);
            if (idealLatency_ > latency_)
            {
                latency_ += (s_latencyVelocity_ * dTime) * Mathf.Min(1.0f, Mathf.Pow(Mathf.Abs(idealLatency_ - latency_), s_latencyCurvePower_));
                latency_ = Mathf.Min(latency_, idealLatency_);
            }
            else
            {
                latency_ -= (s_latencyVelocity_ * dTime) * Mathf.Min(1.0f, Mathf.Pow(Mathf.Abs(idealLatency_ - latency_), s_latencyCurvePower_));
                latency_ = Mathf.Max(latency_, idealLatency_);
            }

            // record this so we can move latency at a velocity independent
            //  of the number of times we're called.
            timeOfLastOutput_ = time;

            // find the position at 'time - latency'
            double outputTime = time - latency_;

            int resultSpaceID = 0;
            int resultVehicleID = 0;
            Vector3 resultPosition = Vector3.zero;
            Vector3 resultVelocity = Vector3.zero;
            Vector3 resultDirection = Vector3.zero;

            this.extract(outputTime, ref resultSpaceID, ref resultVehicleID, ref resultPosition, ref resultVelocity, ref resultDirection);

            avatar_.Pos(resultPosition, resultDirection, resultVelocity);
        }


        void resetStoredInputs(double time, int spaceID, int vehicleID, Vector3 position, Vector3 positionError, float[] auxFiltered)
        {
            const float NONZERO_TIME_DIFFERENCE = 0.01f;

            currentInputIndex_ = 0;
            gotNewInput_ = true;

            for (uint i = 0; i < 8; i++)
            {
                // set times of older inputs as to avoid zero time differences
                storedInputs_[(currentInputIndex_ + i) % 8].time_ = time - (i * NONZERO_TIME_DIFFERENCE);

                storedInputs_[(currentInputIndex_ + i) % 8].spaceID_ = spaceID;
                storedInputs_[(currentInputIndex_ + i) % 8].vehicleID_ = vehicleID;
                storedInputs_[(currentInputIndex_ + i) % 8].position_ = position;
                storedInputs_[(currentInputIndex_ + i) % 8].positionError_ = positionError;

                //TODO
                if (auxFiltered != null)
                {
                    storedInputs_[(currentInputIndex_ + i) % 8].direction_.x = auxFiltered[0];
                    storedInputs_[(currentInputIndex_ + i) % 8].direction_.z = auxFiltered[2];
                }
                else
                {
                    storedInputs_[(currentInputIndex_ + i) % 8].direction_.x = 0.0f;
                    storedInputs_[(currentInputIndex_ + i) % 8].direction_.z = 0.0f;
                }
                storedInputs_[(currentInputIndex_ + i) % 8].direction_.y = 0.0f;
            }

            this.latency_ = s_latencyFrames_ * NONZERO_TIME_DIFFERENCE;

            nextWaypoint_.time_ = time - NONZERO_TIME_DIFFERENCE;
            nextWaypoint_.spaceID_ = spaceID;
            nextWaypoint_.vehicleID_ = vehicleID;
            nextWaypoint_.position_ = position;

            //TODO
            if (auxFiltered != null)
            {
                nextWaypoint_.direction_.x = auxFiltered[0];
                nextWaypoint_.direction_.z = auxFiltered[2];
            }
            else
            {
                nextWaypoint_.direction_.x = 0.0f;
                nextWaypoint_.direction_.z = 0.0f;
            }
            nextWaypoint_.direction_.y = 0.0f;

            previousWaypoint_ = nextWaypoint_;
            previousWaypoint_.time_ -= NONZERO_TIME_DIFFERENCE;
        }


        void extract(double time, ref int outputSpaceID,
                                   ref int outputVehicleID,
                                   ref Vector3 outputPosition,
                                   ref Vector3 outputVelocity,
                                   ref Vector3 outputDirection)
        {
            if (time > nextWaypoint_.time_)
            {
                this.chooseNextWaypoint(time);
            }

            float proportionateDifferenceInTime = (float)((time - previousWaypoint_.time_) /
                                                    (nextWaypoint_.time_ - previousWaypoint_.time_));

            outputSpaceID = nextWaypoint_.spaceID_;
            outputVehicleID = nextWaypoint_.vehicleID_;

            outputPosition = Vector3.Lerp(previousWaypoint_.position_, nextWaypoint_.position_, proportionateDifferenceInTime);

            outputVelocity = (nextWaypoint_.position_ - previousWaypoint_.position_) /
                                    (float)(nextWaypoint_.time_ - previousWaypoint_.time_);

            float x;
            float z;
            x = Mathf.Lerp(previousWaypoint_.direction_.x, nextWaypoint_.direction_.x, proportionateDifferenceInTime);
            z = Mathf.Lerp(previousWaypoint_.direction_.z, nextWaypoint_.direction_.z, proportionateDifferenceInTime);

            outputDirection = new Vector3(x, 0, z);

            outputDirection.Normalize();
        }


        void chooseNextWaypoint(double time)
        {
            Waypoint newWaypoint = new Waypoint();

            if (this.getStoredInput(0).time_ > time)
            {
                for (uint i = 7; i >= 0; i--)
                {
                    if (this.getStoredInput(i).time_ > time)
                    {
                        StoredInput lookAheadInput = this.getStoredInput(0);
                        StoredInput nextInput = this.getStoredInput(i);

                        newWaypoint.time_ = nextInput.time_;
                        newWaypoint.spaceID_ = nextInput.spaceID_;
                        newWaypoint.vehicleID_ = nextInput.vehicleID_;
                        newWaypoint.direction_ = nextInput.direction_;

                        newWaypoint.storedInput_ = nextInput;

                        previousWaypoint_.changeCoordinateSystem(newWaypoint.spaceID_,
                                                                newWaypoint.vehicleID_);

                        nextWaypoint_.changeCoordinateSystem(newWaypoint.spaceID_,
                                                            newWaypoint.vehicleID_);

                        float lookAheadRelativeDifferenceInTime = (float)((lookAheadInput.time_ - previousWaypoint_.time_) /
                                                                         (nextWaypoint_.time_ - previousWaypoint_.time_));

                        Vector3 lookAheadPosition;

                        lookAheadPosition = Vector3.Lerp(previousWaypoint_.position_, nextWaypoint_.position_, lookAheadRelativeDifferenceInTime);

                        Vector3Clamp(lookAheadInput.position_ - lookAheadInput.positionError_, lookAheadInput.position_ + lookAheadInput.positionError_, ref lookAheadPosition);

                        // Handel overlapping error rectangles
                        {
                            Bounds newWaypointBB = new Bounds();
                            newWaypointBB.min = newWaypoint.storedInput_.position_ - newWaypoint.storedInput_.positionError_;
                            newWaypointBB.max = newWaypoint.storedInput_.position_ + newWaypoint.storedInput_.positionError_;

                            Bounds currentWaypointBB = new Bounds();
                            currentWaypointBB.min = nextWaypoint_.storedInput_.position_ - nextWaypoint_.storedInput_.positionError_;
                            currentWaypointBB.max = nextWaypoint_.storedInput_.position_ + nextWaypoint_.storedInput_.positionError_;

                            if (newWaypoint.spaceID_ == nextWaypoint_.storedInput_.spaceID_ &&
                                newWaypoint.vehicleID_ == nextWaypoint_.storedInput_.vehicleID_ &&
                                !almostEqual(newWaypoint.storedInput_.positionError_, nextWaypoint_.storedInput_.positionError_) &&
                                BoundingBoxIntersects(newWaypointBB, currentWaypointBB))
                            {
                                // Remain still if the previous move was only to adjust
                                // for changes in position error (ie overlapping error regions).
                                newWaypoint.position_ = nextWaypoint_.position_;
                            }
                            else
                            {
                                float proportionateDifferenceInTime = (float)((nextInput.time_ - nextWaypoint_.time_) /
                                    (lookAheadInput.time_ - nextWaypoint_.time_));

                                newWaypoint.position_ = Vector3.Lerp(nextWaypoint_.position_, lookAheadPosition, proportionateDifferenceInTime);
                            }
                        }
                        {
                            float proportionateDifferenceInTime = (float)((nextInput.time_ - nextWaypoint_.time_) /
                                (lookAheadInput.time_ - nextWaypoint_.time_));

                            newWaypoint.position_ = Vector3.Lerp(nextWaypoint_.position_, lookAheadPosition, proportionateDifferenceInTime);
                        }

                        // Constrain waypoint position to its input error rectangle
                        {
                            Bounds nextInputBB = new Bounds();
                            nextInputBB.min = nextInput.position_ - nextInput.positionError_;
                            nextInputBB.max = nextInput.position_ + nextInput.positionError_;

                            if (!intersects(newWaypoint.position_, nextInputBB))
                            {
                                Vector3 clampedPosition = newWaypoint.position_;

                                Vector3Clamp(nextInput.position_ - nextInput.positionError_, nextInput.position_ + nextInput.positionError_, ref clampedPosition);

                                Vector3 lookAheadVector = newWaypoint.position_ - nextWaypoint_.position_;
                                Vector3 clampedVector = clampedPosition - nextWaypoint_.position_;

                                if (Vector3.Magnitude(lookAheadVector) > 0.0f)
                                {
                                    newWaypoint.position_ = nextWaypoint_.position_ + Vector3.Project(clampedVector, lookAheadVector);
                                }
                                else
                                {
                                    newWaypoint.position_ = nextWaypoint_.position_;
                                }

                                Vector3Clamp(nextInput.position_ - nextInput.positionError_, nextInput.position_ + nextInput.positionError_, ref newWaypoint.position_);

                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                // In the event there is no more input data, stand still for one frame.
                newWaypoint = nextWaypoint_;
                newWaypoint.time_ = time;
            }

            previousWaypoint_ = nextWaypoint_;
            nextWaypoint_ = newWaypoint;
        }


        public bool getLastInput(ref double time, ref int spaceID, ref int vehicleID, ref Vector3 pos, ref Vector3 posError, ref float[] auxFiltered)
        {
            if (!reset_)
            {
                StoredInput storedInput = this.getStoredInput(0);
                time = storedInput.time_;
                spaceID = storedInput.spaceID_;
                vehicleID = storedInput.vehicleID_;
                pos = storedInput.position_;
                if (auxFiltered != null)
                {
                    auxFiltered[0] = storedInput.direction_.x;
                    auxFiltered[2] = storedInput.direction_.z;
                }
                return true;
            }
            return false;
        }


        void Vector3Clamp(Vector3 lower, Vector3 upper, ref Vector3 resultVector)
        {
            resultVector.x = Mathf.Clamp(resultVector.x, lower.x, upper.x);
            resultVector.y = Mathf.Clamp(resultVector.y, lower.y, upper.y);
            resultVector.z = Mathf.Clamp(resultVector.z, lower.z, upper.z);
        }


        bool almostEqual(Vector3 v1, Vector3 v2, float epsilon = 0.0004f)
        {
            return almostEqual(v1.x, v2.x, epsilon) &&
                almostEqual(v1.y, v2.y, epsilon) &&
                almostEqual(v1.z, v2.z, epsilon);
        }


        bool almostEqual(double d1, double d2, double epsilon = 0.0004)
        {
            return Mathf.Abs((float)(d1 - d2)) < epsilon;
        }


        bool intervalsIntersect(float min1, float max1, float min2, float max2)
        {
            return min2 <= max1 && min1 <= max2;
        }


        bool BoundingBoxIntersects(Bounds boxA, Bounds boxB)
        {
            Vector3 min1 = boxA.min;
            Vector3 max1 = boxA.max;

            Vector3 min2 = boxB.min;
            Vector3 max2 = boxB.max;

            return (
                intervalsIntersect(min1.x, max1.x, min2.x, max2.x) &&
                intervalsIntersect(min1.y, max1.y, min2.y, max2.y) &&
                intervalsIntersect(min1.z, max1.z, min2.z, max2.z));
        }


        bool intersects(Vector3 v, Bounds nextInputBB)
        {
            return (v[0] >= nextInputBB.min[0]) && (v[1] >= nextInputBB.min[1]) && (v[2] >= nextInputBB.min[2]) &&
                (v[0] < nextInputBB.max[0]) && (v[1] < nextInputBB.max[1]) && (v[2] < nextInputBB.max[2]);
        }
    }

}
