﻿using UnityEngine;
using System.Collections;

namespace KBEngine
{
	/// <summary>
	/// like as bigworld client DumbFilter
	/// 一个直接改变对象位置的过滤器，可用于传送等非移动引起的位置位置改变
	/// </summary>
	public class DumbFilter : Filter
	{
		Transform m_transform;

		void Awake()
		{
			m_transform = this.transform;
		}

		public override void OnUpdateVolatileData(Vector3 position, Vector3 direction)
		{
			m_transform.position = position;

			direction.Set(0, direction.z, 0);  // 我们只想同步Y轴（朝向）
            m_transform.eulerAngles = direction;
		}

        public override void setPosition(Vector3 position)
        {
            m_transform.position = position;
        }

		public override void setDirection(Vector3 direction)
        {
            float y = direction.y;
            direction.y = direction.z;
            direction.z = y;
            m_transform.eulerAngles = direction;
        }
	}
}
