﻿using UnityEngine;
using System.Collections;

namespace KBEngine
{
	/// <summary>
	/// like as bigworld client Filter
	/// 位置、朝向过滤器基类
	/// </summary>
	public abstract class Filter : MonoBehaviour
	{
		public virtual void OnUpdateVolatileData(Vector3 position, Vector3 direction)
		{ 
			// do nothing
		}

        public virtual void setPosition(Vector3 position)
        {
            // do nothing
        }

        public virtual void setDirection(Vector3 direction)
        {
            // do nothing
        }
		
		public void SpeedChangedNotify(float speed)
        {
            SendMessage("OnFilterSpeedChanged", speed, SendMessageOptions.DontRequireReceiver);
        }
	}
}
