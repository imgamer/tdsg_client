﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 场景实体被遮挡半透明组件
/// </summary>
public class EntityTrigger : MonoBehaviour 
{
    private SceneEntity _sceneEntity;
    void Awake()
    {
        _sceneEntity = transform.parent.GetComponent<SceneEntity>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(GameUtils.TagShadowArea))
        {
            _sceneEntity.SetAlpha(0.5f);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(GameUtils.TagShadowArea))
        {
            _sceneEntity.SetAlpha(1.0f);
        }
    }
}
