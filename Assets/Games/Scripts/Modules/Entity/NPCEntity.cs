﻿using UnityEngine;
using System.Collections;
/// <summary>
/// NPC模型封装
/// </summary>
public class NPCEntity : SceneEntity 
{
    public override EntityType Type
    {
        get { return EntityType.NPCEntity; }
    }

}
