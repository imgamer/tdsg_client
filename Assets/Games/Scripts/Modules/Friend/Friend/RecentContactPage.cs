﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecentContactPage : WinPage
{
    private List<FriendItem> _friendItems = new List<FriendItem>();

    protected override void OnInit()
    {

    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }



    private GameObject _tempItem;
    private UI.Grid _iUIGrid;
    private void IntiPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _iUIGrid = center.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);
    }

    private void InitItem()
    {
        List<string> friendData = new List<string>();
        int dataNum = friendData.Count;
        int itemNum = _friendItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    FriendItem item = _friendItems[i];
                    item.gameObject.SetActive(true);
                    SetItemData( item, friendData[i]);
                }
                else
                {
                    _tempItem.gameObject.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    FriendItem friendItem = item.GetComponent<FriendItem>();
                    friendItem.Init(ParentUI);
                    _tempItem.gameObject.SetActive(false);
                    item.transform.parent = _iUIGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _friendItems.Add(friendItem);
                    SetItemData(friendItem, friendData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                FriendItem item = _friendItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetItemData(item, friendData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _iUIGrid.Reposition();
    }

    private void SetItemData(FriendItem item, string data)
    {

    }
}
