﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;

//friend types define（定义好友相关的数据类型）
using DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;

public class FriendInfoData
{
	public UInt64 m_dbid;
	public string m_name;
	public string m_status;
	public Int32 m_level;
	public string m_lastLoginTime;
	
	public FriendInfoData(Dictionary<string, object> friendInfo)
	{
		m_dbid = (UInt64)friendInfo["dbid"];
		m_name = (string)friendInfo["name"];
		m_status = (string)friendInfo["status"];
		m_level = (Int32)friendInfo["level"];
		m_lastLoginTime = (string)friendInfo["lastLoginTime"];
	}
}

public class RequestInfoData
{
	public UInt64 m_dbid;
	public string m_name;
	public Int32 m_level;
	
	public RequestInfoData(Dictionary<string, object> requestInfo)
	{
		m_dbid = (UInt64)requestInfo["requestDBID"];
		m_name = (string)requestInfo["requestName"];
		m_level = (Int32)requestInfo["requestLevel"];
	}
}

public class FriendModule
{
	private KBEngine.Avatar _avatar;

	private Dictionary<DBID, FriendInfoData> _friendData = new Dictionary<DBID, FriendInfoData>(); 
	private Dictionary<DBID, RequestInfoData> _requestData = new Dictionary<DBID, RequestInfoData>();

    public FriendModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }
	public int Count
	{
		get{ return _friendData.Count;}
	}
	
	public List<FriendInfoData> FriendData
	{
		get{ return new List<FriendInfoData>(_friendData.Values);}
	}
	
	public List<RequestInfoData> RequestData
	{
		get{ return new List<RequestInfoData>(_requestData.Values);}
	}

	/// <summary>
	/// 初始化服务器发来的好友数据
	/// </summary>
	/// <returns>The friend info from sever.</returns>
	/// <param name="friendInfo">Friend info.</param>
	public void InitFriendInfoFromSever(CLIENT_FRIEND friendInfo)
	{
		var friendDict = (DICT_TYPE)friendInfo;
		FriendInfoData infoData = new FriendInfoData(friendDict);
		if (HasFriendData(infoData.m_dbid))
		{
			return;
		}
		_friendData.Add(infoData.m_dbid, infoData);
	}

	/// <summary>
	/// 初始化服务器发来的好友申请数据
	/// </summary>
	/// <returns>The request info from sever.</returns>
	/// <param name="requestInfo">Request info.</param>
	public void InitRequestInfoFromSever(CLIENT_REQUEST requestInfo)
	{
		var requestDict = (DICT_TYPE)requestInfo;
		RequestInfoData infoData = new RequestInfoData(requestDict);
		if (HasRequestData(infoData.m_dbid))
		{
			return;
		}
		_requestData.Add(infoData.m_dbid, infoData);
	}


	/// <summary>
	/// 接受推荐数据
	/// </summary>
	/// <param name="recommendInfo">Recommend info.</param>
	public void InitRecommendInfoFromSever(CLIENT_FRIEND recommendInfo)
	{
		var friendDict = (DICT_TYPE)recommendInfo;
		FriendInfoData infoData = new FriendInfoData(friendDict);

        EventManager.Invoke<FriendInfoData>(EventID.EVNT_FRIEND_RECOMMEND_UPDATE, infoData);
	}

	public void AddNewFriend(CLIENT_FRIEND newFriendInfo)
	{
		InitFriendInfoFromSever(newFriendInfo);
		
		EventManager.Invoke<object>(EventID.EVNT_FRIEND_LIST_UPDATE, null);
	}

	/// <summary>
	/// 新的好友申请
	/// </summary>
	/// <param name="requestInfo">Request info.</param>
	public void AddNewRequest(CLIENT_REQUEST requestInfo)
	{
		InitRequestInfoFromSever(requestInfo);

        EventManager.Invoke<object>(EventID.EVNT_FRIEND_REQUEST_UPDATE, null);
	}

	/// <summary>
	/// 等级更新
	/// </summary>
	/// <param name="levelInfo">Level info.</param>
	public void UpdateLevelInfo(AVATAR_LEVEL_INFO levelInfo)
	{
		var levelDict = (DICT_TYPE)levelInfo;
		UInt64 dbid = (UInt64)levelDict["dbid"];
		Int32 level = (Int32)levelDict["level"];
		foreach (UInt64 id in _friendData.Keys)
		{
			if (id == dbid)
			{   
				_friendData[id].m_level = level;
			}           
		}

        EventManager.Invoke<object>(EventID.EVNT_FRIEND_LIST_UPDATE, null);
	}

	/// <summary>
	/// 状态更新
	/// </summary>
	/// <param name="statusInfo">Status info.</param>
	public void UpdateStatusInfo(AVATAR_STATUS_INFO statusInfo)
	{
		var statusDict = (DICT_TYPE)statusInfo;
		UInt64 dbid = (UInt64)statusDict["dbid"];
		string status = (string)statusDict["status"];
		foreach (UInt64 id in _friendData.Keys)
		{
			if (id == dbid)
			{
				_friendData [id].m_status = status;
			}
		}

        EventManager.Invoke<object>(EventID.EVNT_FRIEND_LIST_UPDATE, null);
	}
	
	public void DeleteFriend(DBID dbid)
	{
		_friendData.Remove(dbid);

        EventManager.Invoke<object>(EventID.EVNT_FRIEND_LIST_UPDATE, null);
	}

	/// <summary>
	/// 向服务器发送删除好友请求
	/// </summary>
	/// <param name="dbid">Dbid.</param>
	public void SendDeleteFriendForServer(UInt64 dbid)
	{
		_avatar.cellCall("deleteFriendFromClient", new object[]{dbid});
		_friendData.Remove(dbid);
	}

	/// <summary>
	/// 向服务器发送是否同意好友申请请求
	/// </summary>
	public void SendAnswerForServer(UInt64 dbid, string name, string value)
	{
		_avatar.cellCall("receiveAnswerFromClient", new object[]{dbid, name, value});
		RemoveRequestItemByDBID(dbid);
	}


	/// <summary>
	/// 向服务器发送好友申请
	/// </summary>
	/// <param name="name">Name.</param>
	public void SendRequestForServer(string value)
	{
		_avatar.cellCall("requestFriend", new object[]{value});
	}

	/// <summary>
	///请求推荐数据 
	/// </summary>
	public void SendRecommendForServer()
	{
		_avatar.cellCall("recommendFriend", new object[]{});
	}	
		
	//检查是否存在该好友
	public bool HasFriendData(DBID id)
	{
		foreach (FriendInfoData data in _friendData.Values)
		{
			if (data.m_dbid == id)
			{
				return true;
			}
		}			
		return false;
	}
	
	public bool HasRequestData(DBID id)
	{
		foreach (RequestInfoData data in _requestData.Values)
		{
			if (data.m_dbid == id)
			{
				return true;
			}
		}           
		return false;
	}

	//获取dbid对应的好友数据
	public FriendInfoData GetFriendItemByDBID(DBID dbid)
	{
		FriendInfoData item;
		_friendData.TryGetValue(dbid, out item);
		return item;
	}
	
	public void RemoveRequestItemByDBID(DBID dbid)
	{
		if(HasRequestData(dbid))
		{
			_requestData.Remove(dbid);
		}
	}
}
