﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIFriend : UIWin
{
    private KBEngine.Avatar _avatar;

    private GameObject _chat;
    protected override void OnInit()
    {
        InitTab();

        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject addBtn = center.Find("BtnAdd").gameObject;
        InputManager.instance.AddClickListener(addBtn, (go) =>
        {
         
        });
        GameObject locationBtn = center.Find("BtnLocation").gameObject;
        InputManager.instance.AddClickListener(locationBtn, (go) =>
        {

        });
        GameObject homeBtn = center.Find("BtnHome").gameObject;
        InputManager.instance.AddClickListener(homeBtn, (go) =>
        {

        });

        _chat = center.Find("Chat").gameObject;
        _chat.SetActive(false);

        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }
    }

    protected override void OnOpen(params object[] args)
    {
        uiTab.SwitchTo(2);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(this);
        };
    }
}
