﻿using UnityEngine;
using System.Collections;

public class FriendItem : WinItem
{
    private UISprite _icon;

    private GameObject _sysObj;
    private GameObject _friendObj;

    private UILabel _name;
    private UILabel _signature;
    private UISprite _school;
    private UILabel _level;
    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _sysObj = transform.Find("Sys").gameObject;
        _friendObj = transform.Find("Friend").gameObject;
        _school = transform.Find("Friend/School").GetComponent<UISprite>();
        _name = transform.Find("Friend/Name").GetComponent<UILabel>();
        _signature = transform.Find("Friend/Signature").GetComponent<UILabel>();
        _level = transform.Find("Friend/Level").GetComponent<UILabel>();
    }
}
