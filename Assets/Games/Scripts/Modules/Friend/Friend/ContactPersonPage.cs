﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ContactPersonPage : WinPage
{
    private List<FriendItem> _friendItems = new List<FriendItem>();
    private List<FriendItem> _blacklistItems = new List<FriendItem>();

    protected override void OnInit()
    {

    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UI.Table _iUITable;

    private GameObject _friendScroll;
    private GameObject _blacklistScroll;

    private UI.Grid _friendGrid;
    private UI.Grid _blacklistGrid;

    private GameObject _tempFriendItem;
    private GameObject _tempBlackListItem;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _iUITable = center.Find("UITable").GetComponent<UI.Table>();

        GameObject friendMenu = center.Find("UI.Table/Friend").gameObject;
        InputManager.instance.AddClickListener(friendMenu.gameObject, (go) =>
        {
           
        });

        GameObject blacklistMenu = center.Find("UI.Table/BlackList").gameObject;
        InputManager.instance.AddClickListener(blacklistMenu.gameObject, (go) =>
        {
            
        });

        _friendGrid = center.Find("Friend/Scroll/UIGrid").GetComponent<UI.Grid>();
        _blacklistGrid = center.Find("BlackList/Scroll/UIGrid").GetComponent<UI.Grid>();
        _tempFriendItem = center.Find("Friend/Scroll/UIGrid/Item").gameObject;
        _tempFriendItem.SetActive(false);
        _tempBlackListItem = center.Find("BlackList/Scroll/UIGrid/Item").gameObject;
        _tempBlackListItem.SetActive(false);
    }

    private void InitFriendItem()
    {
         List<string> friendData = new List<string>();
        int dataNum = friendData.Count;
        int itemNum = _friendItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    FriendItem item = _friendItems[i];
                    item.gameObject.SetActive(true);
                    SetItemData( item, friendData[i]);
                }
                else
                {
                    _tempFriendItem.gameObject.SetActive(true);
                    GameObject item = Instantiate(_tempFriendItem) as GameObject;
                    FriendItem friendItem = item.GetComponent<FriendItem>();
                    friendItem.Init(ParentUI);
                    _tempFriendItem.gameObject.SetActive(false);
                    item.transform.parent = _friendGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _friendItems.Add(friendItem);
                    SetItemData(friendItem, friendData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                FriendItem item = _friendItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetItemData(item, friendData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _friendGrid.Reposition();
    }

    private void InitBlackListItem()
    {
        List<string> blacklistData = new List<string>();
        int dataNum = blacklistData.Count;
        int itemNum = _blacklistItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    FriendItem item = _blacklistItems[i];
                    item.gameObject.SetActive(true);
                    SetItemData(item, blacklistData[i]);
                }
                else
                {
                    _tempBlackListItem.gameObject.SetActive(true);
                    GameObject item = Instantiate(_tempBlackListItem) as GameObject;
                    FriendItem friendItem = item.GetComponent<FriendItem>();
                    friendItem.Init(ParentUI);
                    _tempBlackListItem.gameObject.SetActive(false);
                    item.transform.parent = _blacklistGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _blacklistItems.Add(friendItem);
                    SetItemData(friendItem, blacklistData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                FriendItem item = _blacklistItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetItemData(item, blacklistData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _blacklistGrid.Reposition();
    }

    private void SetItemData(FriendItem item, string data)
    {

    }

}
