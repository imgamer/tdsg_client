﻿using UnityEngine;
using System.Collections;
using System;

public class MailAttachmentItem : WinItem
{
    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }

    private UILabel _count;
    public String Count
    {
        set
        {
            _count.text = value.ToString();
        }
    }

    public GameObject CountObj
    {
        get
        {
            return _count.gameObject;
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _count = transform.Find("Count").GetComponent<UILabel>();
    }
}
