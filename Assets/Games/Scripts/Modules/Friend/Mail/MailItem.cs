﻿using UnityEngine;
using System.Collections;
using System;

public class MailItem : WinItem
{
    private Int64 _emailUID;
    public Int64 EmailUID
    {
        set { _emailUID = value; }
        get { return _emailUID; }
    }

    private UILabel _title;
    public string Title
    {
        set
        {
            _title.text = value.ToString();
        }
    }

    private UISprite _icon;
    public String Icon
    {
        set
        {
            _icon.spriteName = value.ToString();
        }
    }

    private UILabel _time;
    public String SendTime
    {
        set
        {
            _time.text = value.ToString();
        }
    }

    protected override void OnInit()
    {
        _title = transform.Find("Title").GetComponent<UILabel>();
        _time = transform.Find("Time").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
    }
}
