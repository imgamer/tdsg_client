﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public struct MailAttachment
{
    public byte attachmentType;  //附件类型，道具，金币，元宝，经验等				
    public Int32 attachmentId; //附件编号			
    public Int32 attachmentCount; //附件数量
}

public class MailData
{
    public Int64 emailUID; //邮件唯一id
    public string title;   //标题
    public string content;	//内容			 
    public Int32 sendTime; //时间戳
    public sbyte received; //是否阅读 0:未读取
    public List<MailAttachment> attachments = new List<MailAttachment>(); //附件数据
}

public class MailModule
{
    private KBEngine.Avatar _avatar;
    private Dictionary<Int64, MailData> _mails = new Dictionary<Int64, MailData>();

    private bool _called = false; //是否请求过邮件列表

    public Int32 NotReadMailCount { get; private set; }
    public MailModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;       
    }

    public List<MailData> Mails
    {
        get
        {
            List<MailData> data = new List<MailData>(_mails.Values);
            return SortEmailData(data);
        }      
    }

    public MailData GetMailDataByUID(Int64 UID)
    {
        MailData data;
        _mails.TryGetValue(UID, out data);
        return data;
    }

    /// <summary>
    /// 请求邮件数据
    /// </summary>
    public void QueryEmail()
    {
        if(!_called)
        {
            _avatar.baseCall("queryEmail");
            _called = true;
        }
        else
        {
            EventManager.Invoke<object>(EventID.EVNT_EMAIL_UPDATE, null);
        }
    }

    /// <summary>
    /// 读取邮件
    /// </summary>
    public void ReceiveEmail(Int64 emailUID)
    {
        _avatar.baseCall("receiveEmail", emailUID);
    }

    /// <summary>
    /// 请求未读取的邮件数量
    /// </summary>
    public void queryNotReceiveValidEmailCount()
    {
        _avatar.baseCall("queryNotReceiveValidEmailCount");
    }

    /// <summary>
    /// 接收所有邮件
    /// </summary>
    /// <param name="mailList"></param>
    public void OnReceiveEmails(List<object> mailList)
    {
        foreach(object obj in mailList)
        {
            Dictionary<string, object> mail = (Dictionary<string, object>)obj;
            Int64 mailUID = (Int64)mail["emailUID"];
            string title = (string)mail["title"];
            string content = (string)mail["content"];
            string attachmentTypes = (string)mail["attachmentTypes"];
            string attachmentIds = (string)mail["attachmentIds"];
            string attachmentCounts = (string)mail["attachmentCounts"];
            Int32 sendTime = (Int32)mail["sendTime"];
            sbyte received = (sbyte)mail["received"];           
            if(!_mails.ContainsKey(mailUID))
            {
                AddMailData(mailUID, title, content, attachmentTypes, attachmentIds, attachmentCounts, sendTime, received);
            }           
        }
        EventManager.Invoke<object>(EventID.EVNT_EMAIL_UPDATE, null);
    }

    /// <summary>
    /// 接收新邮件
    /// </summary>
    public void OnAddEmail(Int64 emailUID, string title, string content, string attachmentTypes, string attachmentIds, string attachmentCounts, Int32 sendTime, sbyte received)
    {
        if (_mails.ContainsKey(emailUID))
        {
            return;
        }   
        AddMailData(emailUID, title, content, attachmentTypes, attachmentIds, attachmentCounts, sendTime, received);
        EventManager.Invoke<object>(EventID.EVNT_EMAIL_UPDATE, null);
    }

    /// <summary>
    /// 邮件附件收取成功
    /// </summary>
    /// <param name="emailUID"></param>
    public void ReceiveEmailSuccess(Int64 emailUID)
    {
        _mails[emailUID].received = Define.EMAIL_HAS_RECEIVE;
        EventManager.Invoke<object>(EventID.EVNT_EMAIL_UPDATE, null);
    }

    /// <summary>
    /// 接收未读取的邮件数量
    /// </summary>
    /// <param name="count"></param>
    public void OnNotReceiveValidEmailCount(Int32 count)
    {
        NotReadMailCount = count;
    }

    private void AddMailData(Int64 emailUID, string title, string content, string attachmentTypes, string attachmentIds, string attachmentCounts, Int32 sendTime, sbyte received)
    {
        MailData data = new MailData();
        data.emailUID = emailUID;
        data.title = title;
        data.content = content;
        data.sendTime = sendTime;
        data.received = received;
        if (!string.IsNullOrEmpty(attachmentTypes))
        {
            string[] types = attachmentTypes.Split(';');
            string[] Ids = attachmentIds.Split(';');
            string[] counts = attachmentCounts.Split(';');
            for (int i = 0; i < types.Length; i++)
            {
                MailAttachment attachment = new MailAttachment();
                attachment.attachmentType = Convert.ToByte(types[i]);
                attachment.attachmentId = Convert.ToInt32(Ids[i]);
                attachment.attachmentCount = Convert.ToInt32(counts[i]);
                data.attachments.Add(attachment);
            }
        }
        _mails.Add(emailUID, data);    
    }

    /// <summary>
    /// 数据按时间排序
    /// </summary>
    /// <param name="dic"></param>
    /// <returns></returns>
    private List<MailData> SortEmailData(List<MailData> data)
    {
        data.Sort(delegate(MailData x, MailData y)
        {
            return y.sendTime.CompareTo(x.sendTime);
        });
        return data;
    }
}
