﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MailPage : WinPage 
{
    private KBEngine.Avatar _avatar;

    private List<MailItem> _mailItems = new List<MailItem>();

    private List<MailAttachmentItem> _mailAttachmentItems = new List<MailAttachmentItem>(); //附件Item

    private Int64 _mailUID = 0; //当前正在阅读的邮件ID

    protected override void OnInit()
    {
        InitPage();

        _avatar = GameMain.Player;

        EventManager.AddListener<object>(EventID.EVNT_EMAIL_UPDATE, UpdateMailItem);
    }

    protected override void OnOpen(params object[] args)
    {
        _mailUID = 0;
        _avatar.mailModule.QueryEmail();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_EMAIL_UPDATE, UpdateMailItem);
    }

    
    private GameObject _tempMailItem;
    private UI.Grid _mailUiGrid;
    private GameObject _readBg;

    private GameObject _tempAttachmentItem;
    private UI.Grid _attachmentUiGrid;

    private GameObject _contentObj;
    private UILabel _content;
    private GameObject _attachmentObj;
    private GameObject _empty;
    private GameObject _takeBtn;
    private UILabel _takeBtnLabel;

    private void InitPage()
    {
        _empty = transform.Find("Empty").gameObject;

        _readBg = transform.Find("ReadBackGround").gameObject;
        _readBg.SetActive(false);

        _mailUiGrid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempMailItem = transform.Find("Scroll/Grid/Item").gameObject;
        _tempMailItem.SetActive(false);

        Transform content = transform.Find("Content");
        _contentObj = content.gameObject;
        _contentObj.SetActive(false);
        _content = content.Find("Content").GetComponent<UILabel>();

        _attachmentObj = content.Find("Attachment").gameObject;

        _attachmentUiGrid = content.Find("Attachment/Scroll/Grid").GetComponent<UI.Grid>();
        _tempAttachmentItem = content.Find("Attachment/Scroll/Grid/Item").gameObject;
        _tempAttachmentItem.SetActive(false);


        _takeBtn = content.Find("Attachment/BtnTake").gameObject;
        _takeBtnLabel = content.Find("Attachment/BtnTake/Label").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_takeBtn, (go) =>
        {
            _avatar.mailModule.ReceiveEmail(_mailUID);
        });
    }

    private void RefreshItem()
    {
        List<MailData> mailData = _avatar.mailModule.Mails;
        int emailCount = mailData.Count;
        if(emailCount == 0)
        {
            _empty.SetActive(true);
            _contentObj.SetActive(false);
        }
        else
        {
            _empty.SetActive(false);
            _contentObj.SetActive(true);
        }
        int itemCount = _mailItems.Count;
        if (emailCount >= itemCount)
        {
            for (int i = 0; i < emailCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_mailItems[i], mailData[i]);
                    _mailItems[i].gameObject.SetActive(true);
                }
                else
                {
                    //创建Item设数据
                    _tempMailItem.SetActive(true);
                    GameObject item = Instantiate(_tempMailItem) as GameObject;
                    MailItem mailItem = item.GetComponent<MailItem>();
                    mailItem.Init(ParentUI);
                    _tempMailItem.SetActive(false);
                    item.transform.parent = _mailUiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _mailItems.Add(mailItem);
                    SetItemData(mailItem, mailData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < emailCount)
                {
                    //设数据
                    SetItemData(_mailItems[i], mailData[i]);
                    _mailItems[i].gameObject.SetActive(true);
                }
                else
                {
                    //隐藏多余的
                    _mailItems[i].gameObject.SetActive(false);
                }
            }
        }
        _mailUiGrid.Reposition();
    }

    private void SetItemData(MailItem item, MailData data)
    {
        item.EmailUID = data.emailUID;
        item.Title = data.title;
        item.SendTime = TimeUtils.GetDateTime(Convert.ToDouble(data.sendTime), TimeUtils.DateFormat1);

        if (data.attachments.Count > 0)
        {
            if (data.received == Define.EMAIL_NOT_RECEIVE)
            {
                //附件未读取
                item.Icon = "[11-15]UI-s2-yj-baoxiangguanbi";           
            }
            else
            {
                item.Icon = "[11-15]UI-s2-yj-baoxiangkaiqi"; 
            }
        }
        else
        {
            if (data.received == Define.EMAIL_NOT_RECEIVE)
            {
                //邮件已阅读
                item.Icon = "[11-15]UI-s2-yj-youjian";
            }
            else
            {
                item.Icon = "[11-15]UI-s2-yj-youjiankaiqi";
            }
        }

        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {            
            ReadMailContent(data);
            SelectMailItem(item);
            if (data.attachments.Count == 0)
            {
                _avatar.mailModule.ReceiveEmail(_mailUID);
            }            
        });
    }

    private void ReadMailContent(MailData data)
    {
        _mailUID = data.emailUID;
        _contentObj.SetActive(true);
        _content.text = data.content;
     
        if (data.attachments.Count > 0)
         {
             _attachmentObj.SetActive(true);
             SetAttachmentData(data.attachments);
             if (data.received == Define.EMAIL_NOT_RECEIVE)
             {
                 UITools.SetButtonState(_takeBtn, true);
                 _takeBtnLabel.text = "领  取";
             }
             else
            {
                UITools.SetButtonState(_takeBtn, false);
                 _takeBtnLabel.text = "已领取";
            }
         }
         else
         {
             _attachmentObj.SetActive(false);
         }
    }

    private void SelectMailItem(MailItem item)
    {
        _readBg.transform.parent = item.gameObject.transform;
        _readBg.transform.localPosition = Vector3.zero;
        _readBg.SetActive(true);
    }

    private void SetAttachmentData(List<MailAttachment> atch)
    {
        int itemCount = _mailAttachmentItems.Count;
        int atchCount = atch.Count;
        if (atchCount >= itemCount)
        {
            for (int i = 0; i < atchCount; i++)
            {
                if (i < itemCount)
                {
                    //设数据
                    SetMailAtchItemData(_mailAttachmentItems[i], atch[i]);
                    _mailAttachmentItems[i].gameObject.SetActive(true);
                }
                else
                {
                    //创建Item设数据
                    _tempAttachmentItem.SetActive(true);
                    GameObject item = Instantiate(_tempAttachmentItem) as GameObject;
                    MailAttachmentItem atchItem = item.GetComponent<MailAttachmentItem>();
                    atchItem.Init(ParentUI);
                    _tempAttachmentItem.SetActive(false);
                    item.transform.parent = _attachmentUiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _mailAttachmentItems.Add(atchItem);
                    SetMailAtchItemData(atchItem, atch[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < atchCount)
                {
                    //设数据
                    SetMailAtchItemData(_mailAttachmentItems[i], atch[i]);
                    _mailAttachmentItems[i].gameObject.SetActive(true);
                }
                else
                {
                    //隐藏多余的
                    _mailAttachmentItems[i].gameObject.SetActive(false);
                }
            }
        }
        _attachmentUiGrid.Reposition();
    }

    private void SetMailAtchItemData(MailAttachmentItem item, MailAttachment data)
    {
        switch(data.attachmentType)
        {
            case Define.REWARD_TYPE_ITEM:
                ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(data.attachmentId);
                if (itemData == null) return;
                ParentUI.SetDynamicSpriteName(item.Icon, itemData.icon);
                break;
            default:
                ParentUI.SetDynamicSpriteName(item.Icon, "Consumables_0102");
                break;
        }      
        if(data.attachmentCount != 1)
        {
            item.CountObj.SetActive(true);
            item.Count = Convert.ToString(data.attachmentCount);
        }
        else
        {
            item.CountObj.SetActive(false);
        }
    }

    private void OpenDefaultItem()
    {
        List<MailData> mailData = _avatar.mailModule.Mails;
        if (mailData.Count != 0)
        {
            ReadMailContent(mailData[0]);
            SelectMailItem(_mailItems[0]);
        }       
    }

    private void UpdateMailItem(object o)
    {
        if (!Active) return;
        RefreshItem();  

        if (_mailUID == 0)
        {
            OpenDefaultItem();
            return;
        }

        foreach(MailItem item in _mailItems)
        {
            if (item.EmailUID == _mailUID && item.gameObject.activeSelf)
            {
                SelectMailItem(item);
            }
        }
        MailData data = _avatar.mailModule.GetMailDataByUID(_mailUID);
        ReadMailContent(data);
    }
}
