﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using WellFired;

public class SeqManager : MonoBehaviour
{
    public static SeqManager instance { get; private set; }
    void Awake()
    {
        instance = this;
    }    

    private void GetSeq(string seqName, Action<Seq> callBack)
    {
        BaseSpawnData data = new InstantiateData(seqName, seqName, PoolName.Seq, PoolType.Once, (o) =>
        {
            Seq seq = null;
            if (o)
            {
                o.transform.parent = null;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                seq = o.GetComponent<Seq>();
            }
            if (callBack != null) callBack(seq);
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
    
    /// <summary>
    /// 根据战斗数据播放Seq（注意：播放结束，回调由外部决定是否删除：例如Buff类，播放完并不删除，Buff结束时再删除）
    /// </summary>
    /// <param name="seqName"></param>
    /// <param name="data"></param>
    /// <param name="cam"></param>
    /// <param name="onFinish"></param>
    public void PlaySeq(string seqName, int ownerId, FightShowData showData, Transform cam, USSequencer.PlaybackDelegate onFinish)
    {
        if (onFinish == null)
        {
            Printer.LogError("onFinish不能为空！必须控制播放结束是否删除！");
            return;
        }
        if (!showData.IsValid())
        {
            onFinish(null);
            return;
        }

        GetSeq(seqName, seq => 
        {
            if (seq)
            {
                try
                {
                    seq.Init(ownerId, showData, cam);
                    seq.PlaybackFinished += onFinish;
                    seq.Play();
                }
                catch (Exception e)
                {
                    Printer.LogException(e);
                    onFinish(null);
                }
            } 
            else
            {
                onFinish(null);
            }
        });
    }
    
}
