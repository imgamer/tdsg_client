﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Seq : WellFired.USSequencer
{
    public Transform Owner { get; private set; }
    public Vector3 OwnerOriginPosition { get; private set; }
    public Vector3 OriginOwnerRotation { get; private set; }
    public Transform[] AffectedTargets { get; private set; }
    public Transform AffectedCamera { get; private set; }
    public Vector3 CameraOriginPosition { get; private set; }
    public FightShowData Data { get; private set; }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="ownerId">ownerId是Seq的拥有者，即作用对象，可以是非FightData内的userId</param>
    /// <param name="data"></param>
    /// <param name="cam"></param>
    public void Init(int ownerId, FightShowData data, Transform cam)
    {
        Data = data;
        AffectedCamera = cam;
        CameraOriginPosition = cam.position;

        var scene = SceneManager.instance.CurrentScene;
        var se = scene.GetEntityByID(ownerId);
        Owner = se.transform;
        OwnerOriginPosition = Owner.position;
        OriginOwnerRotation = se.GetDirection();

        var targetList = Data.GetTargetsId();
        AffectedTargets = new Transform[targetList.Count];
        for (int i = 0; i < targetList.Count; i++)
        {
            var targetId = targetList[i];
            SceneEntity target = scene.GetEntityByID(targetId);
            if (target == null)
            {
                Printer.LogError("targetID not find: " + targetId);
                continue;
            }
            AffectedTargets[i] = target.transform;
        }

        SeqContainer[] containers = transform.GetComponentsInChildren<SeqContainer>(true);
        for (int i = 0; i < containers.Length; i++)
        {
            containers[i].SetAffectedObject();
        }
    }

    public new void Play()
    {
        if (Owner == null) return;
        PlaybackFinished += (seq) =>
        {
            Stop();
        };
        base.Play();
    }


    public new void Stop()
    {
        PlaybackFinished = null;
        base.Stop();
    }
}