﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace WellFired
{
    [USequencerEvent("Customer/Camera Effect/Fade To")]
    [USequencerFriendlyName("Camera Fade To")]
    public class USCameraFadeToEvent : USBaseEvent
    {
        public Color color;
        public override void FireEvent()
        {
            base.FireEvent();
            if (Duration < 0) Duration = 1.0f;
            EnterHighlightMode(color, Duration);
        }
        
        void EnterHighlightMode(Color fadeColor, float duration)
        {
            var seq = this.Sequence as Seq;
            Transform[] array = new Transform[seq.AffectedTargets.Length + 1];
            array.SetValue(seq.Owner, 0);
            seq.AffectedTargets.CopyTo(array, 1);
            GameUtils.ChangeLayer(array, GameUtils.LayerHighlight);

            List<SceneEntity> list = SceneManager.instance.CurrentScene.GetAll();
            for (int i = 0; i < list.Count; i++)
            {
                SceneEntity entity = list[i];
                HUDManager.instance.ShowTopHUD(entity.TopRoot, false);
            }
            
            CameraManager.instance.HightLight(fadeColor, duration, false, 1 << GameUtils.LayerHighlight | 1 << GameUtils.LayerSkillEffect);
            UIManager.instance.HideAll();
        }
    }
}
