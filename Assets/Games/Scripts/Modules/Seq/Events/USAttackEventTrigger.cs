﻿using UnityEngine;
using System.Collections;
using WellFired;
using DG.Tweening;
using System.Collections.Generic;

[USequencerEvent("Customer/Attack Event Trigger")]
[USequencerFriendlyName("Attack Event Trigger")]
public class USAttackEventTrigger : USBaseEvent
{
    public override void FireEvent ()
    {
        base.FireEvent();
        var seq = Sequence as Seq;

        //作用所有目标
        for (int i = 0; i < seq.AffectedTargets.Length; i++)
        {
            var trans = seq.AffectedTargets[i];
            if (trans == null) continue;
            var se = trans.GetComponent<SceneEntity>();

            int id = se.KBEntity.id;
            int count = seq.Data.TargetEffectsCount[id];
            //剩余的受击表现次数小于等于0，不表现
            if (count <= 0) continue;

            List<List<int>> damageList = seq.Data.GetTargetEffect(id);
            if (damageList != null && damageList.Count > 0)
            {
                var curDamageList = damageList[0];
                int type = curDamageList[0];
                //每一个对象进行受击表现
                se.ShowTrigger(seq.Data, type, null);
                count--;
                seq.Data.TargetEffectsCount[id] = count;
            }
        }
    }

}
