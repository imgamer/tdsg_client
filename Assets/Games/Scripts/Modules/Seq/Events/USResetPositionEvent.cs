﻿using UnityEngine;
using System.Collections;
namespace WellFired
{
    [USequencerEvent("Customer/Reset Position")]
    [USequencerFriendlyName("Reset Position")]
    public class USResetPositionEvent : USBaseEvent
    {
        #region implemented abstract members of USEventBase
        public override void FireEvent()
        {
            var seq = this.Sequence as Seq;
            var se = AffectedObject.GetComponent<SceneEntity>();
            se.SetPosition(seq.OwnerOriginPosition);
            se.SetDirection(seq.OriginOwnerRotation);
            se.AnimControl.SetPrepare();
        }
        #endregion
    }
}
