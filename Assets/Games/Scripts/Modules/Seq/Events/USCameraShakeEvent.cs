﻿using UnityEngine;
using System.Collections;
using WellFired;
using DG.Tweening;

namespace WellFired
{
    [USequencerEvent("Customer/Camera Effect/Shake")]
    [USequencerFriendlyName("Camera shake")]
    public class USCameraShakeEvent : USBaseEvent
    {
        public float strength = 3;
        public int vibrato = 10;
        public float randomness = 90;

        #region implemented abstract members of USEventBase
        public override void FireEvent()
        {
            if (Duration < 0) Duration = 1.0f;
            var cam = AffectedObject.transform;
            cam.DOShakePosition(Duration, strength, vibrato, randomness);
        }
        #endregion

    }
}
