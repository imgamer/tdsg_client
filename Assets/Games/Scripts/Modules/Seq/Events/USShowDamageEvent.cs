﻿using UnityEngine;
using System.Collections;
using WellFired;
using System.Collections.Generic;

[USequencerEvent("Customer/Show Damage")]
[USequencerFriendlyName("Show Damage")]
public class USShowDamageEvent : USBaseEvent
{
    
    public override void FireEvent ()
    {
        base.FireEvent ();
        var se = AffectedObject.GetComponent<SceneEntity>();
        if (se == null) return;

        var seq = Sequence as Seq;
        List<List<int>> damageList = seq.Data.GetTargetEffect(se.KBEntity.id);
        if (damageList != null && damageList.Count > 0)
        {
            se.ShowDamage(damageList[0]);
            //触发表现飘血，闪避，暴击等表现，移除伤害列表
            damageList.RemoveAt(0);
            se.OnDamageTriggerShow();
        }
    }
}
