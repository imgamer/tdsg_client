﻿using UnityEngine;
using System.Collections;

namespace WellFired
{
    [USequencerEvent("Customer/Camera Effect/Scale")]
    [USequencerFriendlyName("Camera Scale")]
    public class USCameraScaleEvent : USBaseEvent
    {
        public AnimationCurve curve = new AnimationCurve(new Keyframe(0.0f, 3.75f), new Keyframe(1.0f, 3.75f));
        private Camera[] _cams;
        #region implemented abstract members of USEventBase    
        public override void FireEvent ()
        {
            Duration = curve.keys[curve.length - 1].time;
            var cam = AffectedObject.transform;
            _cams = cam.GetComponentsInChildren<Camera>(true);
        }
    
        public override void ProcessEvent (float runningTime)
        {
            for (int i = 0; i < _cams.Length; i++)
            {
                _cams[i].orthographicSize = curve.Evaluate(runningTime);
            }
        }
        #endregion

    }
}