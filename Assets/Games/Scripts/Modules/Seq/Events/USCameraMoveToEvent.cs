﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace WellFired
{
    [USequencerEvent("Customer/Camera Effect/Focus To")]
    [USequencerFriendlyName("Camera Move To")]
    public class USCameraMoveToEvent : USBaseEvent
    {

        public Destination target = Destination.Player;
        public float offsetX;
        public float offsetY;
        #region implemented abstract members of USEventBase
        public override void FireEvent()
        {
            var cam = AffectedObject.transform;
            if (cam == null)
                return;
            var dest = GetDestination(target);
            dest.x += offsetX;
            dest.y += offsetY;
            cam.DOMove(dest, Duration).SetEase(Ease.Linear);
        }
        #endregion

    }
}
