﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;
namespace WellFired
{
[USequencerEvent("Customer/Spawn Effect")]
[USequencerFriendlyName("Spawn Effect")]
public class USSpawnEffectEvent : USBaseEvent
{
    public enum SpawnPosition
    {
        AffectedObject,
        EnemyCenter,
        EnemyObject
    }

    public bool loop = false;
    public SpawnPosition spawnPosition;
    public float offsetX;
    public float offsetY;
    public string prefabName;
    private List<BaseEffect> _effects = new List<BaseEffect>();

    #region implemented abstract members of USEventBase
    public override void FireEvent ()
    {
        if(Duration < 0) Duration = 1.0f;
        Dictionary<Transform, Vector3> dict = GetSpawnPosition(spawnPosition);
        foreach (var item in dict)
        {
            if (item.Key == null) continue;
            var se = item.Key.GetComponent<SceneEntity>();
            if (se == null) continue;
            Vector3 spawnposition = item.Value + new Vector3(offsetX, offsetY, 0);
            EffectManager.instance.SpawnEffect(prefabName, null, loop, Duration, spawnposition, !se.IsDefaultDirection, (e) =>
            {
                _effects.Add(e);
            });
        }
    }

    void OnDestroy()
    {
        foreach( BaseEffect e in _effects )
        {
            EffectManager.instance.DespawnEffect(e);
        }
    }

    Dictionary<Transform, Vector3> GetSpawnPosition(SpawnPosition pos)
    {
        Dictionary<Transform, Vector3> dict = new Dictionary<Transform, Vector3>();
        Seq seq = Sequence as Seq;
        switch (pos) 
        {
            case SpawnPosition.AffectedObject:
                dict[AffectedObject.transform] = AffectedObject.transform.position;
                return dict;
            case SpawnPosition.EnemyCenter:
                dict[AffectedObject.transform] = GetDestination(Destination.EnemyCenter);
                return dict;
            case SpawnPosition.EnemyObject:
                for (int i = 0; i < seq.AffectedTargets.Length; i++ )
                {
                    Transform trans = seq.AffectedTargets[i];
                    if (trans == null) continue;
                    dict[trans] = trans.position;
                }
                return dict;
            default:
                throw new System.ArgumentOutOfRangeException ();
        }
        
    }
    #endregion
    
}
}
