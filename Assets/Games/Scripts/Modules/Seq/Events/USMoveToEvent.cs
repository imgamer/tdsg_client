using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace WellFired
{
    [USequencerFriendlyName("USMoveToEvent")]
    [USequencerEvent("Customer/USMoveToEvent")]
    public class USMoveToEvent : USBaseEvent
    {
        //目前只有remain > 0时才会去做偏移这种情况， remain = 0目前都是移动回来，暂不做偏移。后期应该将移动过去和回来拆分两个事件可能比较合理。
        public float remain = 0.0f;
        public Destination target;

        //移动过程中防止与其他模型重叠
        private static readonly float DeltaZ = -0.01f;
        public override void FireEvent()
        {
            var se = AffectedObject.GetComponent<SceneEntity>();
            var animation = se.AnimControl;
            var dest = GetDestination(target);
            if (!Mathf.Approximately(remain, 0.0f))
            {
                dest += (se.GetPosition() - dest).normalized * remain;
                //需要设置z轴偏移
                dest.z = dest.y + DeltaZ;
            }

            AffectedObject.transform.DOMove(dest, Duration).SetEase(Ease.Linear).OnStart(() =>
            {
                animation.BeginRun(dest); 
            })
            .OnComplete(() =>
            {
                animation.EndRun();
            });
        }
    }
}