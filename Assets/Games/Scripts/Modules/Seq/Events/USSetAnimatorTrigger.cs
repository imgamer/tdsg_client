using UnityEngine;
using System.Collections;

namespace WellFired
{
	/// <summary>
	/// A custom event that will set a boolean value on a Mecanim Animator.
	/// </summary>
	[USequencerFriendlyName("Set Mecanim Trigger")]
	[USequencerEvent("Animation (Mecanim)/Animator/Set Value/Trigger")]
	class USSetAnimatorTrigger : USBaseEvent 
	{
		public string valueName  = "";
		
		public override void FireEvent()
		{
            var se = AffectedObject.GetComponent<SceneEntity>();
            se.AnimControl.PlayAttack(valueName, null);
		}
		
	}
}