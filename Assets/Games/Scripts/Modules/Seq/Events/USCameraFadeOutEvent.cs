﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace WellFired
{
    [USequencerEvent("Customer/Camera Effect/Fade Out")]
    [USequencerFriendlyName("Camera Fade Out")]
    public class USCameraFadeOutEvent : USBaseEvent
    {
        Color defaultColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        public override void FireEvent()
        {
            base.FireEvent();
            if (Duration < 0) Duration = 1.0f;
            ExitHighlightMode(defaultColor, Duration);
        }
        
        void ExitHighlightMode(Color color, float duration)
        {
            var seq = this.Sequence as Seq;
            Transform[] array = new Transform[seq.AffectedTargets.Length + 1];
            array.SetValue(seq.Owner, 0);
            seq.AffectedTargets.CopyTo(array, 1);
            GameUtils.ChangeLayer(array, GameUtils.LayerEntity);

            List<SceneEntity> list = SceneManager.instance.CurrentScene.GetAll();
            for (int i = 0; i < list.Count; i++)
            {
                SceneEntity entity = list[i];
                HUDManager.instance.ShowTopHUD(entity.TopRoot, true);
            }

            CameraManager.instance.HightLight(color, duration, true, 1 << GameUtils.LayerHighlight | 1 << GameUtils.LayerSkillEffect);
            UIManager.instance.ShowAll();
        }
        
    }
}