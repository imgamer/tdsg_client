﻿using UnityEngine;
using System.Collections;


public enum Destination 
{
    Player,
    PlayerOrigin,
    Enemy,
    CameraOrigin,
    EnemyCenter
}

public class USBaseEvent : WellFired.USEventBase
{
    //默认生命周期
    public USBaseEvent()
    {
        Duration = 0.4f;
    }

    public override void FireEvent()
    {

    }

    public override void ProcessEvent(float deltaTime)
    {

    }
    
    
    public Vector3 GetDestination(Destination target)
    {
        var seq = this.Sequence as Seq;
        switch (target) {
            case Destination.Player:
                return seq.Owner.position;
            case Destination.PlayerOrigin:
                return seq.OwnerOriginPosition;
            case Destination.Enemy:
                return seq.AffectedTargets[0].position;
            case Destination.EnemyCenter:
            {
                var targets = seq.AffectedTargets;
                var ret = targets[0].position;
                for(int i = 1; i < targets.Length; i++)
                {
                    ret += targets[i].position;
                }
                return ret / targets.Length;
            }
            case Destination.CameraOrigin:
                return seq.CameraOriginPosition;
            default:
                throw new System.ArgumentOutOfRangeException ();
        }
    }
}
