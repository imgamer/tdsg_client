﻿using UnityEngine;
using System.Collections;

public class SeqContainer : WellFired.USTimelineContainer
{
    public enum Type
    {
        None,
        Camera,
        User,
    }
    public Type type = Type.None;
    public void SetAffectedObject()
    {         
        var seq = this.Sequence as Seq;
        switch (type)
        {
            case Type.None:
                break;
            case Type.Camera:
                AffectedObject = seq.AffectedCamera;
                break;
            case Type.User:
                AffectedObject = seq.Owner;
                break;
            default:
                break;
        }
    }
}
