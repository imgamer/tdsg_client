﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using WellFired;
using LitJson;
using System;
/// <summary>
/// Seq编辑工具
/// </summary>
public class SeqUtils : EditorWindow
{
    [MenuItem("Seq/模板创建", false)]
    static public void OpenTool()
    {
        EditorWindow.GetWindow<SeqUtils>(false, "创建Seq模板", true).Show();
    }

    private string seqName;
    void OnGUI()
    {
        if (Application.isPlaying)
        {
            EditorGUILayout.HelpBox("运行状态不能创建Seq", MessageType.Info);
            return;
        }

        seqName = EditorGUILayout.TextField("输入名称:", seqName);
        if (GUILayout.Button("确定创建", GUILayout.Width(200)))
        {
            if (string.IsNullOrEmpty(seqName))
            {
                this.ShowNotification(new GUIContent("Seq名字不能为空！"));
                return;
            }
            if (!seqName.Equals(seqName.ToLower()))
            {
                this.ShowNotification(new GUIContent("Seq名字必须全部小写！"));
                return;
            }
            Create();
            this.Close();
        }
        if (GUILayout.Button("放弃创建", GUILayout.Width(200)))
        {
            this.Close();
        }
    }
    void OnHierarchyChange()
    {
        this.RemoveNotification();
    }
    private void Create()
    {
        GameObject go = new GameObject(seqName);
        go.transform.localPosition = Vector3.zero;
        go.transform.localEulerAngles = Vector3.zero;
        go.transform.localScale = Vector3.one;
        go.AddComponent<Seq>();
        Selection.activeGameObject = go;
    }

    private static readonly string SeqEffectJsonPath = string.Format("Assets/Games/GameAssets/UnfixedAssets/Resources/Configs/{0}", "seq_relate_effect.json");
    [MenuItem("Seq/导出Seq对应的特效配置文件", false)]
    public static void ToEffectConfig()
    {
        JsonWriter writer = new JsonWriter();
        writer.PrettyPrint = true;

        writer.WriteObjectStart();
        List<GameObject> list = GetSeqList();
        foreach (var item in list)
        {
            WriteSeqRelateEffect(writer, item);
        }
        writer.WriteObjectEnd();

        //写入文件
        FileStream file = new FileStream(SeqEffectJsonPath, FileMode.Create);
        StreamWriter fileWriter = new StreamWriter(file, System.Text.Encoding.UTF8);
        fileWriter.Write(writer.ToString());
        fileWriter.Flush();
        fileWriter.Close();
        file.Close();
        AssetDatabase.Refresh();
    }


    private static void WriteSeqRelateEffect(JsonWriter writer, GameObject go)
    {
        writer.WritePropertyName(go.name);
        writer.WriteObjectStart();
        writer.WritePropertyName("seqName");
        writer.Write(go.name);

        writer.WritePropertyName("effectNames");
        writer.WriteArrayStart();
        USSpawnEffectEvent[] effects = go.GetComponentsInChildren<USSpawnEffectEvent>(true);
        if(effects.Length > 0)
        {
            List<string> effectNames = new List<string>();
            foreach (var item in effects)
            {
                string effectName = item.prefabName;
                if (string.IsNullOrEmpty(effectName)) continue;
                if (effectNames.Contains(effectName)) continue;
                effectNames.Add(effectName);
                writer.Write(item.prefabName);
            }
        }
        writer.WriteArrayEnd();
        writer.WriteObjectEnd();
    }

    [MenuItem("Seq/导出Seq配置文件", false)]
    public static void ToSeqConfig()
    {
        JsonWriter writer = new JsonWriter();
        writer.PrettyPrint = true;

        writer.WriteObjectStart();
        List<GameObject> list = GetSeqList();
        foreach (var item in list)
        {
            WriteSeqMessage(writer, item);
        }
        writer.WriteObjectEnd();

        //写入文件
        var path = EditorUtility.SaveFilePanel("输出场景点配置", "", "seq_data" + ".json", "json");
        FileStream file = new FileStream(path, FileMode.Create);
        StreamWriter fileWriter = new StreamWriter(file, System.Text.Encoding.UTF8);
        fileWriter.Write(writer.ToString());
        fileWriter.Flush();
        fileWriter.Close();
        file.Close();
        AssetDatabase.Refresh();
    }


    private const float offsetTime = 0.1f;
    private static void WriteSeqMessage(JsonWriter writer, GameObject go)
    {
        writer.WritePropertyName(go.name);
        writer.WriteObjectStart();

        writer.WritePropertyName("seqName");
        writer.Write(go.name);

        Seq seq = go.GetComponent<Seq>();
        writer.WritePropertyName("showTime");
        writer.Write(Math.Round(seq.Duration + offsetTime, 2));

        SeqContainer[] containers = go.GetComponentsInChildren<SeqContainer>(true);
        int[] array = new int[3];
        int maxAttackNum = 0;
        for (int i = 0; i < containers.Length; i++)
        {
            SeqContainer container = containers[i];
            USAttackEventTrigger[] evts = container.GetComponentsInChildren<USAttackEventTrigger>(true);
            if (maxAttackNum < evts.Length) maxAttackNum = evts.Length;
            switch (container.type)
            {
                case SeqContainer.Type.None:
                    array[0]++;
                    break;
                case SeqContainer.Type.Camera:
                    array[1]++;
                    break;
                case SeqContainer.Type.User:
                    array[2]++;
                    break;
                default:
                    break;
            }
        }
        writer.WritePropertyName("undefinedNum(Error)");
        writer.Write(array[0]);
        writer.WritePropertyName("cameraNum");
        writer.Write(array[1]);
        writer.WritePropertyName("userNum");
        writer.Write(array[2]);
        writer.WritePropertyName("maxAttackNum");
        writer.Write(maxAttackNum);

        writer.WriteObjectEnd();
    }

    private static readonly string SeqPath = string.Format("Assets/Games/GameAssets/UnfixedAssets/Resources/{0}", "Seq");
    private static List<GameObject> GetSeqList()
    {
        List<GameObject> list = new List<GameObject>();
        List<string> paths = GetPaths(SeqPath);
        if (paths.Count <= 0)
        {
            EditorUtility.DisplayDialog("警告", "没有Seq对象", "OK");
            return list;
        }
        foreach (var p in paths)
        {
            GameObject o = AssetDatabase.LoadAssetAtPath<GameObject>(p);
            if (o)
            {
                Seq seq = o.GetComponent<Seq>();
                if (seq != null) list.Add(seq.gameObject);
            }
        }
        return list;
    }

    private static List<string> GetPaths(string path)
    {
        List<string> ret = new List<string>();

        if (string.IsNullOrEmpty(path)) return ret;
        string[] paths = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
        foreach (var p in paths)
        {
            string str = p.Replace('\\', '/');
            ret.Add(str);
        }
        return ret;
    }
}