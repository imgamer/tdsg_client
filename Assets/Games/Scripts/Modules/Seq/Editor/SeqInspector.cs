﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using WellFired;

[CanEditMultipleObjects()]
[CustomEditor(typeof(Seq), true)]
public class SeqInspector : Editor
{
    private Seq _seq;
    private GameObject _target = null;
    public override void OnInspectorGUI()
    {
        base.DrawDefaultInspector();
        _seq = target as Seq;
        if (_seq == null) return;
        if (!_seq.name.Equals(_seq.name.ToLower()))
        {
            EditorGUILayout.HelpBox("命名不能包含大写字母", MessageType.Error);
            return;
        }
        ShowCreateGUI();
        ShowTimelinesGUI();
    }

    #region 显示创建相关信息
    private void ShowCreateGUI()
    {
        if (_seq.gameObject.activeInHierarchy == false) return;
        _target = (GameObject)EditorGUILayout.ObjectField("Target", _target, typeof(GameObject), true);
        if (_target == null)
        {
            EditorGUILayout.HelpBox("Target对象为空，无法创建TimeLiness!", MessageType.Warning);
        }
        if (GUILayout.Button("创建TimeLiness For Target"))
        {
            Create();
        }
        if (GUILayout.Button("预   览"))
        {
            USLicenseAgreementWindow.OpenWindow();
        }
        if (GUILayout.Button("保   存"))
        {
            Save();
        }
    }

    private void Create()
    {
        if (_target == null) return;
        GameObject go = new GameObject(string.Format("Timelines for {0}", _target.name));
        SeqContainer container = go.AddComponent<SeqContainer>();
        go.transform.parent = _seq.transform;
        go.transform.localPosition = Vector3.zero;
        go.transform.localEulerAngles = Vector3.zero;
        go.transform.localScale = Vector3.one;
        container.Index = 0;
        container.AffectedObject = _target.transform;

        GameObject timeLine = new GameObject("Event Timeline");
        timeLine.AddComponent<USTimelineEvent>();
        timeLine.transform.parent = go.transform;
        timeLine.transform.localPosition = Vector3.zero;
        timeLine.transform.localEulerAngles = Vector3.zero;
        timeLine.transform.localScale = Vector3.one;
    }
    private const int offset = 1;
    private void Save()
    {
        USBaseEvent[] events = _seq.GetComponentsInChildren<USBaseEvent>(true);
        float offsetTime = 1000000;
        foreach (var item in events)
        {
            if (item.FireTime < offsetTime) offsetTime = item.FireTime;
        }
        float length = 0;
        foreach (var item in events)
        {
            item.FireTime -= offsetTime;
            if (item.FireTime > length) length = item.FireTime;
        }
        _seq.Duration = length + offset;
        SavePrefab(_seq);
    }
    private void SavePrefab(Seq usSeq)
    {
        string path = string.Format("Assets/Games/GameAssets/UnfixedAssets/Resources/Seq/{0}.prefab", usSeq.name);
        PrefabUtility.CreatePrefab(path, usSeq.gameObject, ReplacePrefabOptions.ConnectToPrefab);
        EditorUtility.SetDirty(target);
        AssetDatabase.SaveAssets();
    }
    #endregion

    #region 显示Timelines相关信息
    private void ShowTimelinesGUI()
    {
        EditorGUILayout.HelpBox("Timelines相关信息", MessageType.Warning);
        SeqContainer[] containers = _seq.GetComponentsInChildren<SeqContainer>(true);
        Dictionary<SeqContainer.Type, List<SeqContainer>> dict = new Dictionary<SeqContainer.Type, List<SeqContainer>>();

        for (int i = 0; i < containers.Length; i++)
        {
            SeqContainer container = containers[i];
            List<SeqContainer> list;
            if(!dict.TryGetValue(container.type, out list))
            {
                list = new List<SeqContainer>();
                dict[container.type] = list;
            }
            list.Add(container);
        }

        ShowContainers(dict, SeqContainer.Type.User, (count) =>
        {
            if (count == 0)
            {
                GUI.color = Color.red;
                GUILayout.Label("没有使用者!");
            }
            else if (count == 1)
            {
                GUI.color = Color.white;
                GUILayout.Label("使用者:");
            }
            else
            {
                GUI.color = Color.red;
                GUILayout.Label("只能存在一个使用者!");
            }
            return count > 0;
        }, (container) => 
        {
            USAttackEventTrigger[] events = container.GetComponentsInChildren<USAttackEventTrigger>(true);
            GUILayout.Label(string.Format("伤害/加血表现次数：{0}", events.Length));
            GUILayout.Space(5);
        });

        GUILayout.Space(10);
        ShowContainers(dict, SeqContainer.Type.Camera, (count) =>
        {
            if (count == 0)
            {
                GUI.color = Color.yellow;
                GUILayout.Label("没有相机!");
            }
            else
            {
                GUI.color = Color.white;
                GUILayout.Label("相机:");
            }
            return count > 0;
        });

        GUILayout.Space(10);
        ShowContainers(dict, SeqContainer.Type.None, (count) =>
        {
            if (count > 0)
            {
                GUI.color = Color.red;
                GUILayout.Label("存在未设置类型:");
            }
            return count > 0;
        });
    }
    private void ShowContainers(Dictionary<SeqContainer.Type, List<SeqContainer>> dict, SeqContainer.Type type, Predicate<int> doShow, Action<SeqContainer> showSingle = null)
    {
        List<SeqContainer> list;
        if (dict.TryGetValue(type, out list))
        {
            list.Sort((a, b) =>
            {
                return a.Index.CompareTo(b.Index);
            });
            if (doShow != null)
            {
                if (!doShow(list.Count)) return;
            }
            int preIndex = -1;
            for (int i = 0; i < list.Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(20);
                GUILayout.BeginVertical();
                SeqContainer container = list[i];
                bool vaild = VaildContainer(container, preIndex, list.Count);
                ShowSingleContainer(container, vaild);
                if (showSingle != null) showSingle(container);
                preIndex = container.Index;
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
        }
        else
        {
            if (doShow != null) doShow(0);
        }
    }
    private bool VaildContainer(SeqContainer container, int preIndex, int count)
    {
        if (container.type == SeqContainer.Type.None) return false;

        int index = container.Index;
        //Index必须间隔为1
        if (index - preIndex != 1) return false;

        if (index < 0) return false;
        if (index >= count) return false;
        return true;
    }
    private void ShowSingleContainer(SeqContainer container, bool vaild)
    {
        if (vaild)
        {
            GUI.color = Color.white;
            EditorGUILayout.ObjectField(string.Format("Index : {0}", container.Index), container, typeof(SeqContainer), false);
        }
        else
        {
            GUI.color = Color.red;
            EditorGUILayout.ObjectField(string.Format("(Error) Index : {0}", container.Index), container, typeof(SeqContainer), false);
        }

        Color preColor = GUI.color;
        GUI.color = Color.red;
        USBaseEvent[] events = container.GetComponentsInChildren<USBaseEvent>(true);
        for (int i = 0; i < events.Length; i++)
        {
            USBaseEvent evt = events[i];
            if (evt.Duration > 0) continue;
            GUILayout.BeginHorizontal();
            GUILayout.Space(20);
            EditorGUILayout.ObjectField(string.Format("Event"), evt, typeof(USBaseEvent), false);
            GUILayout.EndHorizontal();
        }
        GUI.color = preColor;
    }
    #endregion
}