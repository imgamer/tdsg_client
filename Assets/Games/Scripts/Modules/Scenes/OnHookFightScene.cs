using System;
public class OnHookFightScene : FightScene
{
    public override void CurrentFightStageCount(int fightStageCount, string fightStageScript) 
    {
        for (int i = 0; i < fightStageCount; i++)
        {
            AddStage(fightStageScript);
        }
        AddStage("StageEnd");
    }

    private void AddStage(string stageScript)
    {
        Stage stageinst = (Stage)Activator.CreateInstance(Type.GetType(stageScript));
        stageinst.Init();
        _stages.Add(stageinst);
    }

    protected override void OnInit()
    {
        AddStage("StageInit");
    }

    protected override void OnPerform()
	{
        AudioManager.instance.PlayBGAudio("battle_bg", 1, true);
        CameraManager.instance.AlignFadeTo(() =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIPVE);
        });
	}

}
