﻿using UnityEngine;
using System.Collections;

public class LoginScene : Scene 
{
    //解决此时没有Avatar，导致场景不能设置为不可用状态
    protected override bool IsSceneAvailable
    {
        get { return false; }
    }

    protected override int HUDSortingOrder { get { return Const.BottomHUDSortingOrder; } }
    protected override void OnReady()
    {
        CameraManager.instance.AlignTo(() =>
        {
            UIManager.instance.DestroyWindow(WinID.UIConnect);
            UIManager.instance.OpenWindow(null, WinID.UILogin);
        });
    }

    protected override void OnPreLoad()
    {
        LoadingManager.instance.AddLoadQueue(LoginManager.instance.PreLoadRole);
        LoadingManager.instance.OpenUILoading();
    }

    protected override void OnLeave()
    {
        UIManager.instance.DestroyAllExcept(WinID.UILoading);
    }

}
