﻿//------------------------------------------------------------------------------
// written by wangshufeng
// 多人副本场景数据管理
//------------------------------------------------------------------------------
using System;
using UnityEngine;

public class MultCopyScene: CityScene
{
    protected override bool IsSceneAvailable
    {
        get { return true; }
    }

    protected override int HUDSortingOrder { get { return Const.BottomHUDSortingOrder; } }

    protected override void OnReady()
    {
        CameraManager.instance.AlignTo(() =>
        {
            HomeManager.instance.OpenHome();
            UIManager.instance.OpenWindow(null, WinID.UITeamCopy);
            HomeManager.instance.TryOpenUI();
        });
        CameraManager.instance.AlignTarget(GameMain.Player.Position, 0);
    }

    protected override void OnPreLoad()
    {
        base.OnPreLoad();
    }

    protected override void OnLeave()
    {
        UIManager.instance.CloseWindow(WinID.UITeamCopy);
    }

    protected override void OnEntityEnter(SceneEntity sceneEntity)
    {
        base.OnEntityEnter(sceneEntity);
    }
}


