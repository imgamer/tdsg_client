﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using Skill;

public class FightScene : Scene
{
    public override bool IsFightScene()
    {
        return true;
    }

    protected override void OnEntityEnter(SceneEntity sceneEntity) 
    {
        base.OnEntityEnter(sceneEntity);
        sceneEntity.KBEntity.InitState();
        InputManager.instance.AddTapListener(sceneEntity.Collider, (gestrue) =>
        {
            PVEManager.instance.SetCurSelectTarget(GameMain.Player, sceneEntity);
        });
        EventManager.Invoke<object>(EventID.EVNT_FIGHT_ENTITY_NUM_UPDATE, null);
    }

    protected override void OnEntityLeave(SceneEntity sceneEntity)
    {
        base.OnEntityLeave(sceneEntity);
        EventManager.Invoke<object>(EventID.EVNT_FIGHT_ENTITY_NUM_UPDATE, null);
    }

    protected override bool IsSceneAvailable
    {
        get { return true; }
    }

    protected override int HUDSortingOrder { get { return Const.TopHUDSortingOrder; } }
    protected override void OnReady()
    {
        AddListener();
        OnPerform();
    }

    protected virtual void OnPerform() 
    {
        AudioManager.instance.PlayBGAudio("battle_bg", 1, true);
        CameraManager.instance.AlignTo(() =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIPVE);
        });
    }

    protected override void OnPreLoad()
    {
        
    }

    protected override void OnLeave()
	{
        AudioManager.instance.StopBGAudio();
        AssetsManager.instance.ClearPool(PoolName.Effect);
        AssetsManager.instance.ClearPool(PoolName.Seq);
        SkillManager.instance.OnLeaveSpace();
        
        RemoveListener();
        EndActionValue();
	}

    private const float Period = 0.05f;
    private ulong _timeId = TimeUtils.InvalidTimerID;
    private void AddListener()
    {
        EventManager.AddListener<object>(EventID.EVNT_FIGHT_START, ResetActionValue);
        EventManager.AddListener<object>(EventID.EVNT_FIGHT_END, PauseActionValue);
        EventManager.AddListener<Int32>(EventID.EVNT_ROUND_START, PauseActionValue);
        EventManager.AddListener<Int32>(EventID.EVNT_ROUND_END, ContinueActionValue);
    }
    private void RemoveListener()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_FIGHT_START, ResetActionValue);
        EventManager.RemoveListener<object>(EventID.EVNT_FIGHT_END, PauseActionValue);
        EventManager.RemoveListener<Int32>(EventID.EVNT_ROUND_START, PauseActionValue);
        EventManager.RemoveListener<Int32>(EventID.EVNT_ROUND_END, ContinueActionValue);
    }
    private void ResetActionValue(object o)
    {
        //重置实体的行动值
        List<SceneEntity> sceneEntitys = GetAll();
        for (int i = 0; i < sceneEntitys.Count; i++)
        {
            SceneEntity sceneEntity = sceneEntitys[i];
            if (sceneEntity == null) continue;
            sceneEntity.KBEntity.SetActionValue(Define.DEFAULT_ACTION_VALUE);
        }
        if (_timeId == TimeUtils.InvalidTimerID)
        {
            _timeId = TimeUtils.AddLoopRealTimer(Period, (tick) =>
            {
                //更新行动值
                for (int i = 0; i < sceneEntitys.Count; i++)
                {
                    SceneEntity sceneEntity = sceneEntitys[i];
                    if (sceneEntity == null) continue;
                    KBEngine.GameObject entity = sceneEntity.KBEntity;
                    uint value = entity.ActionValue + (uint)Mathf.FloorToInt(entity.AttackSpeed * Period);
                    if(value >= Define.MAX_ACTION_VALUE)
                    {
                        //行动值已满，暂停定时器，等待服务器通知
                        value = Define.MAX_ACTION_VALUE;
                        TimeUtils.PauseTimer(_timeId);
                    }
                    entity.SetActionValue(value);
                }
            });
            TimeUtils.StartTimer(_timeId);
        }
        else
        {
            TimeUtils.ContinueTimer(_timeId);
        }
    }
    private void ContinueActionValue(Int32 entityId)
    {
        //扣除实体行动值
        TimeUtils.ContinueTimer(_timeId);
    }
    private void PauseActionValue(Int32 entityId)
    {
        //加满实体行动值
        KBEngine.GameObject entity = GetKBEntityByID(entityId);
        if (entity != null)
        {
            entity.SetActionValue(Define.MAX_ACTION_VALUE);
        }
        TimeUtils.PauseTimer(_timeId);
    }
    private void PauseActionValue(object o)
    {
        TimeUtils.PauseTimer(_timeId);
    }
    private void EndActionValue()
    {
        TimeUtils.RemoveTimer(_timeId);
    }
}
