﻿//------------------------------------------------------------------------------
//written by wangshufeng
//	场景管理基类：
//		1. 维护场景数据；
//		2. 进出场景的策略；
//		3. 场景玩法规则。
//		4. TODO:还要考虑纯客户端场景和服务端场景的区别
//------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class Scene
{
	private SceneData _sceneData;
    protected List<Stage> _stages = new List<Stage>();
    private int _curStageIndex = 0;
    private Transform _entityRoot;
    private Dictionary<Int32, KBEngine.GameObject> _entityIDDict = new Dictionary<int, KBEngine.GameObject>();
    private Dictionary<EntityType, List<SceneEntity>> _entityTypeDict = new Dictionary<EntityType, List<SceneEntity>>();

    #region properties
    /// <summary>
    /// 场景ID
    /// </summary>
    public int UType
    {
        get { return _sceneData.utype; }
    }
    #endregion

    private void AddEntity(SceneEntity sceneEntity)
    {
        if (sceneEntity == null) return;
        List<SceneEntity> list;
        if (!_entityTypeDict.TryGetValue(sceneEntity.Type, out list))
        {
            list = new List<SceneEntity>();
        }
        if (list.Contains(sceneEntity)) return;
        list.Add(sceneEntity);
        _entityTypeDict[sceneEntity.Type] = list;
        _entityIDDict[sceneEntity.KBEntity.id] = sceneEntity.KBEntity;
    }
    private void RemoveEntity(SceneEntity sceneEntity)
    {
        if (sceneEntity == null) return;
        List<SceneEntity> list;
        EntityType type = sceneEntity.Type;
        if (_entityTypeDict.TryGetValue(type, out list))
        {
            list.Remove(sceneEntity);
            _entityTypeDict[type] = list;
        }
        _entityIDDict.Remove(sceneEntity.KBEntity.id);
    }
    public KBEngine.GameObject GetKBEntityByID(Int32 id)
    {
        KBEngine.GameObject entity;
        if (_entityIDDict.TryGetValue(id, out entity))
        {
            return entity;
        }
        return null;
    }
    public List<KBEngine.GameObject> GetAllKBEntity()
    {
        return new List<KBEngine.GameObject>(_entityIDDict.Values);
    }
    public SceneEntity GetEntityByID(Int32 id)
    {
        KBEngine.GameObject entity;
        if (_entityIDDict.TryGetValue(id, out entity))
        {
            if (entity == null) return null;
            return entity.SceneEntityObj;
        }
        return null;
    }
    public SceneEntity GetEntityByUType(UInt32 UType)
    {
        foreach (var entity in _entityIDDict.Values)
        {
            if (entity != null && entity.UType == UType)
            {
                if (entity == null) return null;
                return entity.SceneEntityObj;
            }
        }
        return null;
    }

    private List<SceneEntity> _emptylist = new List<SceneEntity>();
    public List<SceneEntity> GetEntitiesByType(EntityType type)
    {
        List<SceneEntity> list;
        if (_entityTypeDict.TryGetValue(type, out list))
        {
            return list;
        }
        return _emptylist;
    }
    public List<SceneEntity> GetAll()
    {
        List<SceneEntity> list = new List<SceneEntity>();
        foreach (var item in _entityIDDict.Values)
        {
            if (item == null) continue;
            list.Add(item.SceneEntityObj);
        }
        return list;
    }
    public List<SceneEntity> GetOur()
    {
        List<SceneEntity> list = new List<SceneEntity>();
        list.AddRange(GetEntitiesByType(EntityType.PlayerEntity));
        list.AddRange(GetEntitiesByType(EntityType.RoleEntity));
        list.AddRange(GetEntitiesByType(EntityType.HeroEntity));
        list.AddRange(GetEntitiesByType(EntityType.PetEntity));
        return list;
    }
    public List<SceneEntity> GetEnemy()
    {
        List<SceneEntity> list = new List<SceneEntity>();
        list.AddRange(GetEntitiesByType(EntityType.MonsterEntity));
        list.AddRange(GetEntitiesByType(EntityType.BossEntity));
        list.AddRange(GetEntitiesByType(EntityType.OfflineEntity));
        return list;
    }

    public virtual bool IsFightScene() 
    {
        return false;
    }

    public virtual void JudgeThunderResult(bool success) { }

    public virtual void CurrentFightStageCount(int fightStageCount, string fightStageScript) { }



    public Stage CurrentStage
	{
        get { return GetStage(_curStageIndex); }
	}

	public void Init(SceneData sceneData)
	{
        if (sceneData == null)
        {
            Printer.LogError("场景数据为空！");
            return;
        }
		_sceneData = sceneData;
        OnInit();
	}
    protected virtual void OnInit()
    {
        foreach (StageData stagedata in _sceneData.stages)
        {
            String stagescript = stagedata.clientScript;
            Type stageclass = Type.GetType(stagescript);
            Stage stageinst = (Stage)Activator.CreateInstance(stageclass);
            stageinst.Init();
            _stages.Add(stageinst);
        }
    }

    public void ReqModeSwitch()
    {
        GameMain.Player.cellCall("switchMode", new object[] { });
    }

    public void ReqReturnCityRoom()
    {
        GameMain.Player.cellCall("returnCityRoom", new object[] { });
    }

    #region 进入离开场景
    protected abstract int HUDSortingOrder { get; }
    private void CreatEntityRoot()
    {
        if (_entityRoot == null)
        {
            GameObject go = new GameObject("EntityRoot");
            UIPanel panel = go.AddComponent<UIPanel>();
            panel.sortingOrder = HUDSortingOrder;
            go.layer = GameUtils.LayerHUD;
            _entityRoot = go.transform;
            _entityRoot.localPosition = Vector3.zero;
        }
    }
    private void LoadScene(Action<bool> cb)
    {
        string sceneName = _sceneData.sceneName;
        LevelData data = new LevelData(sceneName, sceneName);
        AssetsManager.instance.AsyncSpawnScene(data, (ok) =>
        {
            if (ok)
            {
                CreatEntityRoot();
            }
            else
            {
                Printer.LogError(string.Format("加载:{0} 场景失败", sceneName));
            }
            if (cb != null) cb(ok);
        });
    }
    private void LoadEntity(Action<bool> cb)
    {
        List<KBEngine.GameObject> entities = SceneManager.instance.GetPendingEntities();
        if (entities.Count <= 0)
        {
            if (cb != null) cb(true);
            return;
        }
        List<BaseSpawnData> preloadList = new List<BaseSpawnData>();
        for (int i = 0; i < entities.Count; i++)
        {
            KBEngine.GameObject entity = entities[i];
            if (entity == null) continue;
            PreloadData data = new PreloadData(entity.ModelID, entity.ModelID, PoolName.Model, PoolType.Loop);
            preloadList.Add(data);
        }
        AssetsManager.instance.AsyncPreload(preloadList, cb);
    }

    protected abstract bool IsSceneAvailable { get; }

    /// <summary>
    /// 场景进入
    /// </summary>
    /// <param name="cb"></param>
    public void Enter(Action<bool> cb)
	{
        LoadingManager.instance.AddLoadQueue(LoadScene);
        LoadingManager.instance.AddLoadQueue(LoadEntity);
        OnPreLoad();
        LoadingManager.instance.StartLoading(() => 
        {
            List<KBEngine.GameObject> entities = SceneManager.instance.GetPendingEntities();
            foreach (var entity in entities)
            {
                EntityEnter(entity);
            }
            CameraManager.instance.Show(true);
            OnReady();
            if (cb != null) cb(IsSceneAvailable);
        });
	}
    /// <summary>
    /// 场景完全准备完毕（场景加载，实体加载等都完成）
    /// </summary>
    protected abstract void OnReady();
    /// <summary>
    /// 场景预加载
    /// </summary>
    protected abstract void OnPreLoad();
    /// <summary>
    /// 场景离开
    /// </summary>
    public void Leave()
    {
        UIManager.instance.CloseAll();
        CameraManager.instance.Show(false);
        AssetsManager.instance.ClearPool(PoolName.Scene);
        List<KBEngine.GameObject> list = GetAllKBEntity();
        for (int i = 0; i < list.Count; i++)
        {
            EntityLeave(list[i]);
        }
        OnLeave();
    }
    /// <summary>
    /// 场景离开
    /// </summary>
    protected abstract void OnLeave();
    #endregion

    #region 场景实体操作
    public void EntityEnter(KBEngine.GameObject entity)
    {
        SceneEntityManager.instance.Create(_entityRoot, entity, PoolName.Model, PoolType.Loop, (sceneEntity) =>
        {
            if (sceneEntity)
            {
                sceneEntity.EnterWorld();
                AddEntity(sceneEntity);
                OnEntityEnter(sceneEntity);
            }
        });
    }
    protected virtual void OnEntityEnter(SceneEntity sceneEntity) { }

    public void EntityLeave(KBEngine.GameObject entity)
    {
        SceneEntity sceneEntity = entity.SceneEntityObj;
        if (sceneEntity == null) return;
        RemoveEntity(sceneEntity);
        OnEntityLeave(sceneEntity);
        sceneEntity.LeaveWorld();
    }
    protected virtual void OnEntityLeave(SceneEntity sceneEntity) { }
    #endregion

    #region 场景阶段
    private Stage GetStage(int index)
    {
        if (index < _stages.Count)
        {
            _curStageIndex = index;
            return _stages[_curStageIndex];
        }
        else
        {
            return null;
        }
    }

    public void StageStart(int index)
    {
        Stage stage = GetStage(index);
        if (stage != null)
        {
            stage.Start();
        }
    }

    public void StageEnd()
    {
        Stage stage = CurrentStage;
        if (stage != null)
        {
            stage.End();
        }
    }
    #endregion
}
