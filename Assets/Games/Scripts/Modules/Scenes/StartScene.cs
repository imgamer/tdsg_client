﻿using UnityEngine;
using System.Collections;

public class StartScene : Scene 
{
    //解决此时没有Avatar，导致场景不能设置为不可用状态
    protected override bool IsSceneAvailable
    {
        get { return false; }
    }

    protected override int HUDSortingOrder { get { return Const.BottomHUDSortingOrder; } }
    protected override void OnReady()
    {

    }

    protected override void OnPreLoad()
    {

    }

    protected override void OnLeave()
    {

    }
}
