﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Tools
{
    /// <summary>
    /// 场景编辑器
    /// </summary>
    public class SceneUtils : EditorWindow
    {
        [MenuItem("GameTools/场景编辑器", false)]
        static public void OpenTool()
        {
            EditorWindow.GetWindow<SceneUtils>(false, "场景编辑器", true).Show();
        }

        /// <summary>
        /// 临时存储int[]
        /// </summary>
        private int[] intArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        private static readonly Dictionary<string, int> TypeDict = new Dictionary<string, int>() 
        {
            {"普通场景", 0},
            {"战斗场景", 1},
        };
        private static readonly string[] TypeString = new List<string>(TypeDict.Keys).ToArray();
        private int type;
        private string sceneName;
        private float w;
        private float h;
        private const float width = 10.24f;
        private const float height = 10.24f;
        private const string ScenePath = "Assets/Games/GameAssets/UnfixedAssets/Scenes";
        void OnGUI()
        {
            if (Application.isPlaying)
            {
                EditorGUILayout.HelpBox("运行状态不能进行场景编辑", MessageType.Info);
                return;
            }
            EditorGUILayout.HelpBox("请选中需要的场景图片资源文件夹", MessageType.Info);

            type = EditorGUILayout.IntPopup("场景类型", type, TypeString, intArray);
            sceneName = EditorGUILayout.TextField("输入场景名称:", sceneName);
            w = EditorGUILayout.FloatField("地图宽:", w);
            h = EditorGUILayout.FloatField("地图高:", h);
            if (GUILayout.Button("生成场景"))
            {
                if (!VerifyName(sceneName)) return;
                if (!VerifyMapSize(w, h)) return;

                EditorApplication.NewEmptyScene();
                GameObject root = new GameObject("Root");
                SetGameObject(root, null);
                
                CreateEffect(root);
                GameObject mapRoot = CreateMap(root);
                //普通场景
                if (type == 0)
                {
                    //相机可以跟随玩家
                    CreateCamera(CameraState.Follow);
                    CreateCollider(mapRoot);
                    CreatePoint(root);
                }
                else
                {
                    //相机不能跟随玩家
                    CreateCamera(CameraState.Static);
                }
                EditorApplication.SaveScene(string.Format("{0}/{1}.unity", ScenePath, sceneName), false);
                EditorUtility.DisplayDialog("恭喜", "完成场景编辑", "OK");
            }
        }

        private bool VerifyName(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                this.ShowNotification(new GUIContent("场景名称不能为空！"));
                return false;
            }
            if (!str.Equals(str.ToLower()))
            {
                this.ShowNotification(new GUIContent("场景名称必须全部小写！"));
                return false;
            }
            return true;
        }
        private Dictionary<string, Sprite> textureDict = new Dictionary<string, Sprite>();
        private bool VerifyMapSize(float w, float h)
        {
            int r = Mathf.CeilToInt(w / width);
            int c = Mathf.CeilToInt(h / height);
            if (r < 1 || c < 1)
            {
                this.ShowNotification(new GUIContent("场景地图大小不符合！"));
                return false;
            }
            UnityEngine.Object[] textures = GetSelectedTextures();
            if (textures.Length != r * c)
            {
                this.ShowNotification(new GUIContent("场景地图大小和地图图片数量不符合！"));
                return false;
            }
            textureDict.Clear();
            Regex regex = new Regex("\\d{1}_\\d{1}$");
            for (int i = 0; i < textures.Length; i++)
            {
                Match mch = regex.Match(textures[i].name);
                if (!mch.Success)
                {
                    this.ShowNotification(new GUIContent("场景地图命名不规范：必须是“数字_数字”结尾！"));
                    return false;
                }
                string path = AssetDatabase.GetAssetPath(textures[i]);
                Sprite sp = AssetDatabase.LoadAssetAtPath<Sprite>(path);
                if (sp == null)
                {
                    this.ShowNotification(new GUIContent("读取场景地图失败！"));
                    return false;
                }
                if (!Mathf.Approximately(sp.rect.width, width * sp.pixelsPerUnit) || !Mathf.Approximately(sp.rect.height, height * sp.pixelsPerUnit))
                {
                    this.ShowNotification(new GUIContent(string.Format("场景地图图片大小不符合{0}*{1}", width, height)));
                    return false;
                }
                textureDict[mch.Value] = sp;
            }
            return true;
        }
        private GameObject CreateMap(GameObject root)
        {
            GameObject mapRoot = new GameObject("MapRoot");
            SetGameObject(mapRoot, root.transform, 1000);
            MapDetail mapDetail = mapRoot.AddComponent<MapDetail>();
            mapDetail.MapWidth = w;
            mapDetail.MapHeight = h;

            GameObject textureRoot = new GameObject("TextureRoot");
            SetGameObject(textureRoot, mapRoot.transform);
            CreateTexture(textureRoot.transform);
            return mapRoot;
        }
        private void CreateCollider(GameObject root)
        {
            GameObject colliderRoot = new GameObject("ColliderRoot");
            colliderRoot.layer = LayerMask.NameToLayer("NavMesh_Collider");
            SetGameObject(colliderRoot, root.transform);
            GameObject shadowRoot = new GameObject("ShadowRoot");
            SetGameObject(shadowRoot, root.transform, 100);
        }
        private void CreateTexture(Transform parent)
        {
            UnityEngine.Object[] textures = GetSelectedTextures();
            Vector2 offset = new Vector2(w / 2, h / 2);
            int r = Mathf.CeilToInt(w / width);
            int c = Mathf.CeilToInt(h / height);
            BoxCollider2D box = parent.gameObject.AddComponent<BoxCollider2D>();
            box.size = new Vector2(w, h);

            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    float x = i * width + width / 2;
                    float y = h - (j * height + height / 2);
                    CreateSprite(parent, string.Format("{0}_{1}", i, j), new Vector2(x, y) - offset);
                }
            }
        }
        private void CreateSprite(Transform parent, string name, Vector2 pos)
        {
            GameObject item = new GameObject(name);
            Transform trans = item.transform;
            trans.parent = parent;
            trans.localPosition = pos;
            trans.localEulerAngles = Vector3.zero;
            trans.localScale = Vector3.one;

            SpriteRenderer renderer = item.AddComponent<SpriteRenderer>();
            renderer.sprite = textureDict[name];
            renderer.sortingOrder = -1000;
        }
        private void CreateEffect(GameObject root)
        {
            GameObject effectRoot = new GameObject("EffectRoot");
            effectRoot.layer = LayerMask.NameToLayer("SceneEffect");
            SetGameObject(effectRoot, root.transform, 600);
        }
        private void CreatePoint(GameObject root)
        {
            GameObject pointRoot = new GameObject("PointRoot");
            pointRoot.tag = "EditorOnly";
            pointRoot.layer = LayerMask.NameToLayer("Entity");
            SetGameObject(pointRoot, root.transform);
            pointRoot.SetActive(false);

            GameObject oneRoot = new GameObject("0");
            oneRoot.layer = pointRoot.layer;
            SetGameObject(oneRoot, pointRoot.transform);

            GameObject commonRoot = new GameObject("Common");
            commonRoot.AddComponent<PointCreater>();
            commonRoot.layer = pointRoot.layer;
            SetGameObject(commonRoot, oneRoot.transform);

            GameObject serverRoot = new GameObject("ServerOnly");
            serverRoot.AddComponent<PointCreater>();
            serverRoot.layer = pointRoot.layer;
            SetGameObject(serverRoot, oneRoot.transform);

            GameObject clientRoot = new GameObject("ClientOnly");
            clientRoot.AddComponent<PointCreater>();
            clientRoot.layer = pointRoot.layer;
            SetGameObject(clientRoot, oneRoot.transform);
        }
        private void CreateCamera(CameraState state)
        {
            const string path = "Assets/Games/Scripts/Common/Camera/Prefabs/EditorCamera.prefab";
            EditorCamera cam = AssetDatabase.LoadAssetAtPath<EditorCamera>(path);
            if(cam == null)
            {
                this.ShowNotification(new GUIContent("读取场景相机失败！"));
                return;
            }
            EditorCamera editorCam = Instantiate<EditorCamera>(cam);
            editorCam.state = state;
            editorCam.name = cam.name;
            editorCam.transform.localPosition = new Vector3(0, 0, -50);
            editorCam.transform.localEulerAngles = Vector3.zero;
            editorCam.transform.localScale = Vector3.one;
        }
        private void SetGameObject(GameObject go, Transform parent, float z = 0)
        {
            Transform trans = go.transform;
            trans.parent = parent;
            trans.localPosition = new Vector3(0, 0, z);
            trans.localEulerAngles = Vector3.zero;
            trans.localScale = Vector3.one;
        }

        private UnityEngine.Object[] GetSelectedTextures()
        {
            return Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
        }
    }
}
