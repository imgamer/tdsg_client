﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEditor;
using LitJson;

namespace Tools
{
    /// <summary>
    /// 场景点配置器
    /// </summary>
    public class PointUtils
    {
        [MenuItem("GameTools/导出场景点配置表")]
        public static void Export()
        {
            GameObject root = GameObject.Find("Root");
            if (root == null)
            {
                EditorUtility.DisplayDialog("警告", "没有找到Root节点", "OK");
                return;
            }
            Transform pointRoot = root.transform.Find("PointRoot");
            if (pointRoot == null)
            {
                EditorUtility.DisplayDialog("警告", "没有找到PointRoot节点", "OK");
                return;
            }
            pointRoot.gameObject.SetActive(false);
            if (pointRoot.childCount <= 0)
            {
                EditorUtility.DisplayDialog("警告", "PointRoot节点不存在编辑的点对象节点", "OK");
                return;
            }

            string sceneName = Path.GetFileNameWithoutExtension(EditorApplication.currentScene);
            for (int i = 0; i < pointRoot.childCount; i++)
            {
                Transform child = pointRoot.GetChild(i);

                Transform commonRoot = child.Find("Common");
                Transform[] commonTrans = GetTransformsInChild(commonRoot);

                Transform clientRoot = child.Find("ClientOnly");
                Transform[] clientTrans = GetTransformsInChild(clientRoot);

                Transform serverRoot = child.Find("ServerOnly");
                Transform[] serverTrans = GetTransformsInChild(serverRoot);

                if(commonTrans.Length + serverTrans.Length + clientTrans.Length <= 0)
                {
                    EditorUtility.DisplayDialog("警告", string.Format("节点：{0} 没有编辑节点对象", child.name), "OK");
                    continue;
                }

                string fileName = string.Format("{0}_point_{1}", sceneName, child.name);
                ExportToJson(fileName, commonTrans, serverTrans, clientTrans);
                ExportToPython(fileName, commonTrans, serverTrans, clientTrans);
            }
            EditorUtility.DisplayDialog("提示", "操作完成", "OK");
        }

        private static Transform[] GetTransformsInChild(Transform trans)
        {
            if (trans == null || trans.childCount <= 0) return new Transform[0];
            Transform[] transforms = new Transform[trans.childCount];
            for (int i = 0; i < transforms.Length; i++)
            {
                transforms[i] = trans.GetChild(i);
            }
            return transforms;
        }

        private static void ExportToJson(string fileName, Transform[] common, Transform[] server, Transform[] client)
        {
            var path = EditorUtility.SaveFilePanel("输出场景点配置", "", fileName + ".json", "json");
            if (string.IsNullOrEmpty(path)) return;

            JsonWriter writer = new JsonWriter();
            writer.PrettyPrint = true;

            writer.WriteObjectStart();
            foreach (var obj in common)
            {
                WriteJsonUnit(writer, obj);
            }

            foreach (var obj in client)
            {
                WriteJsonUnit(writer, obj);
            }
            writer.WriteObjectEnd();

            //写入文件
            var file = new FileStream(path, FileMode.Create);
            var fileWriter = new StreamWriter(file, System.Text.Encoding.UTF8);
            fileWriter.Write(writer.ToString());
            fileWriter.Flush();
            fileWriter.Close();
            file.Close();
        }

        private static void WriteJsonUnit(JsonWriter writer, Transform trans)
        {
            PointModifier modifier = trans.GetComponent<PointModifier>();
            writer.WritePropertyName(modifier.id);
            writer.WriteObjectStart();

            writer.WritePropertyName("id");
            writer.Write(Convert.ToInt32(modifier.id));

            writer.WritePropertyName("localPosition");
            writer.WriteObjectStart();
            writer.WritePropertyName("x");
            writer.Write(Output(trans.position.x));
            writer.WritePropertyName("y");
            writer.Write(Output(trans.position.y));
            writer.WriteObjectEnd();

            writer.WriteObjectEnd();
        }

        private static void ExportToPython(string fileName, Transform[] common, Transform[] server, Transform[] client)
        {
            var path = EditorUtility.SaveFilePanel("输出场景点配置", "", fileName + ".py", "py");
            if (string.IsNullOrEmpty(path)) return;

            StringWriter writer = new StringWriter();
            writer.WriteLine("#-*- coding: utf-8 -*-");
            writer.WriteLine("datas = {");

            foreach (var trans in common)
            {
                PointModifier modifier = trans.GetComponent<PointModifier>();
                writer.WriteLine(String.Format("    {0} : {{\"position\":({1:F4},{2:F4},{3:F4}), \"direction\":({4:F4},{5:F4},{6:F4}), \"scale\":{7:F4}}},",
                   modifier.id,
                   trans.position.x, trans.position.y, trans.position.y,
                   trans.eulerAngles.x * Math. PI / 180f, trans.eulerAngles.y * Math.PI / 180f, trans.eulerAngles.z * Math.PI / 180f,
                   trans.localScale));
            }

            foreach (var trans in server)
            {
                PointModifier modifier = trans.GetComponent<PointModifier>();
                writer.WriteLine(String.Format("    {0} : {{\"position\":({1:F4},{2:F4},{3:F4}), \"direction\":({4:F4},{5:F4},{6:F4}), \"scale\":{7:F4}}},",
                   modifier.id,
                   trans.position.x, trans.position.y, trans.position.y,
                   trans.eulerAngles.x * Math.PI / 180f, trans.eulerAngles.y * Math.PI / 180f, trans.eulerAngles.z * Math.PI / 180f,
                   trans.localScale));
            }

            foreach (var trans in client)
            {
                PointModifier modifier = trans.GetComponent<PointModifier>();
                writer.WriteLine(String.Format("    {0} : {{\"position\":({1:F4},{2:F4},{3:F4}), \"direction\":({4:F4},{5:F4},{6:F4}), \"scale\":{7:F4}, \"clientOnly\": True}},",
                   modifier.id,
                   trans.position.x, trans.position.y, trans.position.y,
                   trans.eulerAngles.x * Math.PI / 180f, trans.eulerAngles.y * Math.PI / 180f, trans.eulerAngles.z * Math.PI / 180f,
                   trans.localScale));
            }

            writer.WriteLine("}");

            //写入文件
            var file = new FileStream(path, FileMode.Create);
            var fileWriter = new StreamWriter(file, System.Text.Encoding.UTF8);
            fileWriter.Write(writer.ToString());
            fileWriter.Flush();
            fileWriter.Close();
            file.Close();
        }

        private static double Output(float value)
        {
            return Math.Round(value, 4);
        }
    }
}
