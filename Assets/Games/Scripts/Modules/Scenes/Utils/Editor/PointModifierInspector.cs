﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace Tools
{
    /// <summary>
    /// 场景点修改器
    /// </summary>
    [CustomEditor(typeof(PointModifier))]
    public class PointModifierInspector : Editor
    {
        private PointModifier modifier;
        private string id = string.Empty;

        /// <summary>
        /// 临时存储int[]
        /// </summary>
        private int[] intArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        private int typeInt = 0;
        private static readonly Dictionary<string, PointModifier.Type> TypeDict = new Dictionary<string, PointModifier.Type>() 
        {
            {"NPC", PointModifier.Type.NPC},
            {"采集点", PointModifier.Type.Collect},
            {"宝藏点", PointModifier.Type.Treasure},
        };
        private static readonly string[] TypeString = new List<string>(TypeDict.Keys).ToArray();

        private int rotationInt = 0;
        private static readonly Dictionary<string, int> RotationDict = new Dictionary<string, int>() 
        {
            {"左", 0},
            {"右", 180},
        };
        private static readonly string[] RotationString = new List<string>(RotationDict.Keys).ToArray();
        private const char Char = ':';
        public override void OnInspectorGUI()
        {
            modifier = target as PointModifier;
            string name = modifier.name;
            string[] array = name.Split(Char);
            if (array.Length != 3)
            {
                EditorGUILayout.HelpBox("编辑对象名称不规范！", MessageType.Warning);
                return;
            }
            id = array[2];
            GUILayout.BeginHorizontal();
            GUILayout.Label("输入名称/ID：");
            id = GUILayout.TextField(id);
            modifier.id = id;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (string.IsNullOrEmpty(id))
            {
                EditorGUILayout.HelpBox("请输入名字/ID，不能为空!", MessageType.Warning);
                return;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            for (int i = 0; i < TypeString.Length; i++)
            {
                if (TypeString[i] == array[0])
                {
                    typeInt = i;
                    break;
                }
            }
            typeInt = EditorGUILayout.IntPopup("类型", typeInt, TypeString, intArray);
            string tp = TypeString[typeInt];
            modifier.type = TypeDict[tp];
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            for (int i = 0; i < RotationString.Length; i++)
            {
                if (RotationString[i] == array[1])
                {
                    rotationInt = i;
                    break;
                }
            }
            rotationInt = EditorGUILayout.IntPopup("朝向", rotationInt, RotationString, intArray);

            modifier.name = string.Format("{0}:{1}:{2}", TypeString[typeInt], RotationString[rotationInt], id);
            string rat = RotationString[rotationInt];
            modifier.transform.localEulerAngles = new Vector3(0, RotationDict[rat], 0);
            GUILayout.EndHorizontal();
            GUILayout.Space(30);
        }
    }
}
