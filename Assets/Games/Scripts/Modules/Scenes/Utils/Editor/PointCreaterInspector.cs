﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace Tools
{
    /// <summary>
    /// 场景点创建器
    /// </summary>
    [CustomEditor(typeof(PointCreater))]
    public class PointCreaterInspector : Editor
    {
        private PointCreater creater;
        private string id = string.Empty;

        /// <summary>
        /// 临时存储int[]
        /// </summary>
        private int[] intArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        private int typeInt = 0;
        private static readonly Dictionary<string, PointModifier.Type> TypeDict = new Dictionary<string, PointModifier.Type>() 
        {
            {"NPC", PointModifier.Type.NPC},
            {"采集点", PointModifier.Type.Collect},
            {"宝藏点", PointModifier.Type.Treasure},
        };
        private static readonly string[] TypeString = new List<string>(TypeDict.Keys).ToArray();

        private int rotationInt = 0;
        private static readonly Dictionary<string, int> RotationDict = new Dictionary<string, int>() 
        {
            {"左", 0},
            {"右", 180},
        };
        private static readonly string[] RotationString = new List<string>(RotationDict.Keys).ToArray();
        public override void OnInspectorGUI()
        {
            creater = target as PointCreater;
            GUILayout.BeginHorizontal();
            GUILayout.Label("输入名称/ID：");
            id = GUILayout.TextField(id);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (string.IsNullOrEmpty(id))
            {
                EditorGUILayout.HelpBox("请输入名字/ID，不能为空!", MessageType.Warning);
                return;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            typeInt = EditorGUILayout.IntPopup("类型", typeInt, TypeString, intArray);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            rotationInt = EditorGUILayout.IntPopup("朝向", rotationInt, RotationString, intArray);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("创建"))
            {
                Create();
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(30);
        }

        private void Create()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.name = string.Format("{0}:{1}:{2}", TypeString[typeInt], RotationString[rotationInt], id);
            go.layer = LayerMask.NameToLayer("Entity");
            Transform trans = go.transform;
            trans.parent = creater.transform;
            trans.localPosition = Vector3.zero;
            string rat = RotationString[rotationInt];
            trans.localEulerAngles = new Vector3(0, RotationDict[rat], 0);
            trans.localScale = Vector3.one;

            PointModifier modifier = go.AddComponent<PointModifier>();
            string tp = TypeString[typeInt];
            modifier.id = id;
            modifier.type = TypeDict[tp];
        }
    }
}
