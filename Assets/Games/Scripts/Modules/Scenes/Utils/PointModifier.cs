﻿using UnityEngine;

namespace Tools
{
    public class PointModifier : MonoBehaviour
    {
        public enum Type
        {
            NPC,
            Collect,
            Treasure,
        }
        public string id = string.Empty;
        public Type type = Type.NPC;
    }
}
