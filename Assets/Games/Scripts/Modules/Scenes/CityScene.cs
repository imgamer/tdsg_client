﻿//------------------------------------------------------------------------------
// written by wangshufeng
// 城建场景数据管理
//------------------------------------------------------------------------------
using System;
using UnityEngine;

public class CityScene: Scene
{
    protected override bool IsSceneAvailable
    {
        get { return true; }
    }

    protected override int HUDSortingOrder { get { return Const.BottomHUDSortingOrder; } }

    protected override void OnReady()
    {
        CameraManager.instance.AlignTo(() => 
        {
            HomeManager.instance.OpenHome();
            HomeManager.instance.TryOpenUI();
        });
        CameraManager.instance.AlignTarget(GameMain.Player.Position, 0);
    }

    protected override void OnPreLoad()
    {
        LoadingManager.instance.AddLoadQueue(EffectManager.instance.PreLoadPlayerEffects);
        LoadingManager.instance.OpenUILoading();
    }

    protected override void OnLeave()
    {
        InputManager.instance.RemovePickListener(gridRoot);
    }

    private GameObject gridRoot = null;
    protected override void OnEntityEnter(SceneEntity sceneEntity)
    {
        base.OnEntityEnter(sceneEntity);
        HUDManager.instance.SetTitle(sceneEntity.BottomRoot, sceneEntity.KBEntity.Name);
        switch (sceneEntity.Type)
        {
            case EntityType.PlayerEntity:
                if (MapDetail.instance) gridRoot = MapDetail.instance.transform.Find("TextureRoot").gameObject;
                InputManager.instance.AddPickListener(gridRoot, (gesture) =>
                {
                    if (gesture.Selection != null)
                    {
                        if (gesture.Selection.transform == gridRoot.transform)
                        {
                            if (sceneEntity == null) return;
                            Vector2 origin = CameraManager.instance.CurCamera.ScreenToWorldPoint(gesture.Position);
                            RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero);
                            sceneEntity.Navigate.Start(BaseNavigate.NavigateType.Initiative, hit.point, 0f);
                        }
                    }
                });
                break;
            case EntityType.RoleEntity:
                InputManager.instance.AddTapListener(sceneEntity.Collider, (gestrue) =>
                {
                    if (sceneEntity == null) return;
                    UIManager.instance.OpenWindow(null, WinID.UIRoleMenu, sceneEntity.KBEntity);
                });
                break;
            case EntityType.NPCEntity:
                InputManager.instance.AddTapListener(sceneEntity.Collider, (gestrue) =>
                {
                    if (sceneEntity == null) return;
                    SceneEntity player = GameMain.Player.SceneEntityObj;
                    player.Navigate.Start(BaseNavigate.NavigateType.Initiative, sceneEntity.KBEntity.Position, 1f,
                    () =>
                    {
                        GameMain.Player.talkModule.TalkTo(sceneEntity);
                    });
                });
                break;
            default:
                break;
        }
	}
}


