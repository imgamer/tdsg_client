﻿//------------------------------------------------------------------------------
// written by wangshufeng
// 城建场景数据管理
//------------------------------------------------------------------------------
using System;
using UnityEngine;
using DG.Tweening;

public class OnHookScene: Scene
{

    //踩雷遇怪判定时间
    public float ThunderFightJudgeTime { get; private set; }
    private ulong _thunderJuderTimerId = 0;
    private bool _waitJudging = false; //是否在等待服务器判定遇怪中

    protected override bool IsSceneAvailable
    {
        get { return true; }
    }

    protected override int HUDSortingOrder { get { return Const.BottomHUDSortingOrder; } }
    protected override void OnInit()
    {
        base.OnInit();
        EventManager.AddListener<Status>(EventID.EVNT_PLAYERENTITY_STATUS, PlayerStatus);
        ThunderFightJudgeTime = Const.THUNDER_FIGHT_JUDGE_TIME;
        InitThunderFightCountdown();
    }

    protected override void OnReady()
    {
        CameraManager.instance.AlignTo(() => 
        {
            HomeManager.instance.OpenHome();
            HomeManager.instance.TryOpenUI();
        });
        CameraManager.instance.AlignTarget(GameMain.Player.Position, 0);
    }

    protected override void OnPreLoad()
    {
        LoadingManager.instance.OpenUILoading();
    }

    protected override void OnLeave()
    {
        UninitThunderFightCountdown();
        EventManager.RemoveListener<Status>(EventID.EVNT_PLAYERENTITY_STATUS, PlayerStatus);
        InputManager.instance.RemovePickListener(gridRoot);
    }

    private GameObject gridRoot = null;
    protected override void OnEntityEnter(SceneEntity sceneEntity)
    {
        base.OnEntityEnter(sceneEntity);
        HUDManager.instance.SetTitle(sceneEntity.BottomRoot, sceneEntity.KBEntity.Name);
        switch (sceneEntity.Type)
        {
            case EntityType.PlayerEntity:
                if (MapDetail.instance) gridRoot = MapDetail.instance.transform.Find("TextureRoot").gameObject;
                InputManager.instance.AddPickListener(gridRoot, (gesture) =>
                {
                    if (gesture.Selection != null)
                    {
                        if (gesture.Selection.transform == gridRoot.transform)
                        {
                            if (sceneEntity == null) return;
                            Vector2 origin = CameraManager.instance.CurCamera.ScreenToWorldPoint(gesture.Position);
                            RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero);
                            sceneEntity.Navigate.Start(BaseNavigate.NavigateType.Initiative, hit.point, 0f);
                        }
                    }
                });
                break;
            case EntityType.RoleEntity:
                InputManager.instance.AddTapListener(sceneEntity.Collider, (gestrue) =>
                {
                    if (sceneEntity == null) return;
                    UIManager.instance.OpenWindow(null, WinID.UIRoleMenu, sceneEntity.KBEntity);
                });
                break;
            case EntityType.NPCEntity:
                InputManager.instance.AddTapListener(sceneEntity.Collider, (gestrue) =>
                {
                    if (sceneEntity == null) return;
                    SceneEntity player = GameMain.Player.SceneEntityObj;
                    player.Navigate.Start(BaseNavigate.NavigateType.Initiative, sceneEntity.KBEntity.Position, 1f,
                    () =>
                    {
                        GameMain.Player.talkModule.TalkTo(sceneEntity);
                    });
                });
                break;
            default:
                break;
        }
	}

    private void PlayerStatus(Status s)
    {
        if (_waitJudging)
        {
            return;
        }
        if (s == Status.Idle)
        {
            PauseThunderFightCountdown();
        }
        else
        {
            RunThunderFightCountdown();
        }
    }
    public override void JudgeThunderResult(bool success)
    {
        if( !success )
        {
            _waitJudging = false;
            ThunderFightJudgeTime = Const.THUNDER_FIGHT_JUDGE_TIME;
        }
//         else
//         {
//             CameraManager.instance.CurCamera.DOShakePosition(2f, 0.8f, 35, 90);
//         }
    }

    private void InitThunderFightCountdown()
    {
        _thunderJuderTimerId = TimeUtils.AddLoopRealTimer(Const.THUNDER_FIGHT_JUDGE_KEEP_MOVE_TIME, ThunderFightCountdown);
    }

    private void ThunderFightCountdown(int tick)
    {
        ThunderFightJudgeTime = ThunderFightJudgeTime - Const.THUNDER_FIGHT_JUDGE_KEEP_MOVE_TIME;
        if (ThunderFightJudgeTime <= 0.0f)
        {
            GameMain.Player.cellCall("judgeThunderFight", new object[] { });
            _waitJudging = true;
            PauseThunderFightCountdown();
        }

    }

    private void PauseThunderFightCountdown()
    {
        TimeUtils.PauseTimer(_thunderJuderTimerId);
    }

    private void RunThunderFightCountdown()
    {
        TimeUtils.RestartTimer(_thunderJuderTimerId);
    }

    private void UninitThunderFightCountdown()
    {
        TimeUtils.RemoveTimer(_thunderJuderTimerId);
    }


}


