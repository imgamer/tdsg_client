﻿using System;
using System.Collections;
using System.Collections.Generic;

public class ArenaStageEnd: Stage
{
    public override void OnFightResult(bool win, List<string> paramList)
    {
        // 竞技场战斗结算界面处理
        if(win)
        {
            UIManager.instance.OpenWindow(null, WinID.UIArenaWin, paramList);
        }
        else
        {
            UIManager.instance.OpenWindow(null, WinID.UIArenaLose);
        }
    }
    
}