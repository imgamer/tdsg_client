﻿//------------------------------------------------------------------------------
//written by wangshufeng
//	场景阶段封装：
//		1. 进出每一阶段的表现；
//		2. 阶段控制规则。
//------------------------------------------------------------------------------

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Stage 
{
	public Stage()
	{}

    public void Init()
    {

    }
	/// <summary>
	/// 关卡开始
	/// </summary>
	public virtual void Start()
	{
    }

	/// <summary>
    /// 关卡结束
	/// </summary>
	public virtual void End()
	{
    }

    /// <summary>
    /// 战斗结果通知
    /// </summary>
    public virtual void OnFightResult(bool win, List<string> paramList)
    {
    }

}