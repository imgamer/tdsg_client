using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 场景管理类
/// </summary>
public class SceneManager : MonoSingleton<SceneManager> 
{
    protected override void OnInit()
    {
        EventManager.AddListener<UInt16>(EventID.EVNT_ROOM_STAGE_START, StageStart);
        EventManager.AddListener<object>(EventID.EVNT_ROOM_STAGE_OVER, MoveNextStage);
    }

    protected override void OnUnInit()
    {
       IsSceneAvailable = false;
       EventManager.RemoveListener<UInt16>(EventID.EVNT_ROOM_STAGE_START, StageStart);
       EventManager.RemoveListener<object>(EventID.EVNT_ROOM_STAGE_OVER, MoveNextStage);
    }

    //场景是否已可用
    public bool IsSceneAvailable { get; private set; }
    public UInt16 CurrentSceneType { get; private set; }
    public string CurrentSceneDesc { get; private set; }
    private Scene _currentScene = new StartScene();
    public Scene CurrentScene
    {
        get { return _currentScene; }
        private set { _currentScene = value; }
    }
    private Dictionary<Int32, KBEngine.GameObject> _pendingEntities = new Dictionary<Int32, KBEngine.GameObject>();
    public List<KBEngine.GameObject> GetPendingEntities()
    {
        return new List<KBEngine.GameObject>(_pendingEntities.Values);
    }

    /// <summary>
    /// 主要是为了解决目前服务端Entity和Scene进入消息反过来的问题
    /// </summary>
    public void SetSceneUnavailable()
    {
        IsSceneAvailable = false;
    }
    /// <summary>
    /// 进入场景
    /// </summary>
    /// <param name="type"></param>
    public void EnterScene(UInt16 type)
    {
        SceneData scenedata = SceneConfig.SharedInstance.GetSceneData(type);
        Type sceneClass = Type.GetType(scenedata.clientScript);
        Scene sceneinstance = (Scene)Activator.CreateInstance(sceneClass);
        if (sceneinstance == null)
        {
            Printer.LogError("找不到对应的场景脚本:{0}", scenedata.clientScript);
            return;
        }
        LeaveCurScene();
        CurrentSceneType = type;
        CurrentSceneDesc = scenedata.description;
        sceneinstance.Init(scenedata);
        CurrentScene = sceneinstance;
        CurrentScene.Enter((available) => 
        {
            IsSceneAvailable = available;
            _pendingEntities.Clear();
            EventManager.Invoke<Scene>(EventID.EVNT_SCENE_READY, CurrentScene);
        });
    }
    /// <summary>
    /// 离开场景
    /// </summary>
    private void LeaveCurScene()
    {
        if (CurrentScene != null)
        {
            CurrentScene.Leave();
            CurrentScene = null;
        }
        IsSceneAvailable = false;
    }

    public void OnEnterScene(KBEngine.GameObject entity)
    {
        if (IsSceneAvailable == false)
        {
            _pendingEntities[entity.id] = entity;
            return;
        }
        CurrentScene.EntityEnter(entity);
    }

    public void OnLeaveScene(KBEngine.GameObject entity)
    {
        if (IsSceneAvailable == false)
        {
            _pendingEntities.Remove(entity.id);
            return;
        }
        CurrentScene.EntityLeave(entity);
    }

    private void StageStart(UInt16 index)
    {
        CurrentScene.StageStart(index);
    }

    private void MoveNextStage(object o)
    {
        CurrentScene.StageEnd();
    }

}
