﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// HUD单元
/// </summary>
public class HUDUnit : MonoBehaviour 
{
    public enum HUDType
    {
        Skill,  //技能
        Treat,  //治疗
        Damage, //伤害
        Crit,   //暴击 
        Dodge,  //闪避
    }

    private List<BaseHUD> _updateHudList = new List<BaseHUD>();
    public void Add(BaseHUD hud)
    {
        if (!_updateHudList.Contains(hud))
        {
            _updateHudList.Add(hud);
        }
    }

    void Update()
    {
        for (int i = _updateHudList.Count - 1; i >= 0; i--)
        {
            BaseHUD hud = _updateHudList[i];
            if (!hud.Refresh())
            {
                _updateHudList.Remove(hud);
            }
        }
    }

    public void Despawn()
    {
        _updateHudList.Clear();
        AssetsManager.instance.Despawn(gameObject);
    }

}
