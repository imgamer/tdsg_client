﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUDContent的Item
/// </summary>
public abstract class HUDContentItem : MonoBehaviour
{
    public int size = 30;
    public void Init()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;
        OnInit();
    }
    protected abstract void OnInit();

    public void Show()
    {
        if (gameObject.activeSelf) return;
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public abstract void SetAlpha(float alpha);

    public virtual void SetText(string text) { }

    public virtual void SetSprite(string spriteName) { }
}
