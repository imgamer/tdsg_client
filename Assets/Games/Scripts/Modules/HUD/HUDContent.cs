﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// HUD内容表现
/// </summary>
public class HUDContent : BaseHUD 
{
    protected class Entry
    {
        public float time;			
        public float stay = 0f;		
        public float offset = 0f;
        public HUDContentItem item;		

        public float movementStart { get { return time + stay; } }
    }
    private int Comparison(Entry a, Entry b)
    {
        if (a.movementStart < b.movementStart) return -1;
        if (a.movementStart > b.movementStart) return 1;
        return 0;
    }
    /// <summary>
    /// 是否忽略TimeScale的影响
    /// </summary>
    public bool m_ignoreTimeScale = false;
    private float _Time
    {
        get { return m_ignoreTimeScale ? Time.unscaledTime : Time.time; }
    }

    //移动曲线
    public AnimationCurve m_offsetCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(3f, 40f) });
    //渐变曲线
    public AnimationCurve m_alphaCurve = new AnimationCurve(new Keyframe[] { new Keyframe(1f, 1f), new Keyframe(3f, 0f) });
    //缩放曲线
    public AnimationCurve m_scaleCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.25f, 1f) });

    #region 创建或者销毁
    private List<Entry> _mList = new List<Entry>();
    private List<Entry> _mUnused = new List<Entry>();
    private int _counter = 0;
    private Entry Create()
    {
        // See if an unused entry can be reused
        if (_mUnused.Count > 0)
        {
            Entry ent = _mUnused[_mUnused.Count - 1];
            _mUnused.RemoveAt(_mUnused.Count - 1);
            ent.time = _Time;
            ent.item.Show();
            ent.offset = 0f;
            _mList.Add(ent);
            return ent;
        }

        // New entry
        Entry ne = new Entry();
        ne.time = _Time;
        ne.item = Instantiate(_template) as HUDContentItem;
        ne.item.name = _counter.ToString();
        ne.item.transform.parent = _template.transform.parent;
        ne.item.Init();
        ne.item.Hide();
        _mList.Add(ne);
        ++_counter;
        return ne;
    }

    private void Delete(Entry ent)
    {
        _mList.Remove(ent);
        _mUnused.Add(ent);
        ent.item.Hide();
    }
    #endregion

    private HUDContentItem _template;
    private float _totalEnd;
    protected override void OnInit()
    {
        _template = transform.Find("Root/Item").GetComponent<HUDContentItem>();
        _template.Init();
        _template.Hide();

        Keyframe[] offsets = m_offsetCurve.keys;
        Keyframe[] alphas = m_alphaCurve.keys;
        Keyframe[] scales = m_scaleCurve.keys;

        float offsetEnd = offsets[offsets.Length - 1].time;
        float alphaEnd = alphas[alphas.Length - 1].time;
        float scalesEnd = scales[scales.Length - 1].time;
        _totalEnd = Mathf.Max(scalesEnd, Mathf.Max(offsetEnd, alphaEnd));
    }

    public override void Hide()
    {
        base.Hide();
        for (int i = _mList.Count; i > 0; )
        {
            Entry ent = _mList[--i];
            if (ent.item != null) ent.item.Hide();
            else _mList.RemoveAt(i);
        }
    }

    public override bool Refresh()
    {
        if (_mList.Count <= 0)
        {
            return false;
        }
        float time = _Time;

        float offset = 0f;
        // Adjust alpha and delete old entries
        for (int i = _mList.Count; i > 0; )
        {
            Entry ent = _mList[--i];
            Transform entTrans = ent.item.transform;

            float currentTime = time - ent.movementStart;
            ent.offset = m_offsetCurve.Evaluate(currentTime);
            ent.item.SetAlpha(m_alphaCurve.Evaluate(currentTime));

            // Make the label scale in
            float s = m_scaleCurve.Evaluate(time - ent.time);
            if (s < 0.001f) s = 0.001f;
            entTrans.localScale = new Vector3(s, s, s);

            // Delete the entry when needed
            if (currentTime > _totalEnd) Delete(ent);
            else ent.item.Show();

            offset = Mathf.Max(offset, ent.offset);
            entTrans.localPosition = new Vector3(0f, offset, 0f);
            offset += Mathf.Round(entTrans.localScale.y * ent.item.size);
        }
        return true;
    }

    #region 显示文字或者图片
    public void AddText(string text, float stayDuration)
    {
        if (!enabled) return;
        if (string.IsNullOrEmpty(text)) return;

        // Create a new entry
        Entry ne = Create();
        ne.stay = stayDuration;
        ne.item.SetText(text);

        // Sort the list
        _mList.Sort(Comparison);
    }

    public void AddSprite(string spriteName, float stayDuration)
    {
        if (!enabled) return;
        if (string.IsNullOrEmpty(spriteName)) return;

        // Create a new entry
        Entry ne = Create();
        ne.stay = stayDuration;
        ne.item.SetSprite(spriteName);

        // Sort the list
        _mList.Sort(Comparison);
    }
    #endregion

}
