﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD标题文字表示
/// </summary>
public class HUDTitle : BaseHUD
{
    private UILabel desc1;
    private UILabel desc2;
    protected override void OnInit()
    {
        desc1 = transform.Find("Root/Desc1").GetComponent<UILabel>();
        desc2 = transform.Find("Root/Desc2").GetComponent<UILabel>();
    }

    public void SetValue(string text1, string text2 = "")
    {
        desc1.text = text1;
        desc2.text = text2;
    }
}
