﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD技能名字表现
/// </summary>
public class HUDSkillItem : HUDContentItem
{
    private UILabel label;
    protected override void OnInit()
    {
        label = transform.Find("Text").GetComponent<UILabel>();
    }

    public override void SetAlpha(float alpha)
    {
        label.alpha = alpha;
    }

    public override void SetText(string text)
    {
        label.text = text;
    }
}
