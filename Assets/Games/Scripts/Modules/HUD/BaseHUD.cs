﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD基类
/// </summary>
public abstract class BaseHUD : MonoBehaviour 
{
    private bool _inited = false;
    public void Init()
    {
        if (_inited) return;
        _inited = true;
        OnInit();
    }
    protected abstract void OnInit();

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public virtual bool Refresh()
    {
        return false;
    }
    
}
