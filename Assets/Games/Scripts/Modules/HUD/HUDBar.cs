﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD血条表现
/// </summary>
public class HUDBar : BaseHUD
{
    private UIProgressBar bar;
    protected override void OnInit()
    {
        bar = transform.Find("HP").GetComponent<UIProgressBar>();
    }
    
    public void SetValue(float value)
    {
        bar.value = value;
    }
}
