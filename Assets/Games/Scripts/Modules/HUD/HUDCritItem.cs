﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD暴击表现
/// </summary>
public class HUDCritItem : HUDContentItem
{
    private UILabel label;
    private UISprite[] sprites = new UISprite[4];
    private UIGrid uiGrid;
    protected override void OnInit()
    {
        label = transform.Find("Text").GetComponent<UILabel>();
        Transform grid = transform.Find("Grid");
        uiGrid = grid.GetComponent<UIGrid>();
        for (int i = 0; i < sprites.Length; i++)
        {
            Transform item = grid.Find(i.ToString());
            sprites[i] = item.GetComponent<UISprite>();
        }
    }

    public override void SetAlpha(float alpha)
    {
        label.alpha = alpha;
        foreach (var sprite in sprites)
        {
            sprite.alpha = alpha;
        }
    }

    public override void SetText(string text)
    {
        int num = text.Length;
        for (int i = 0; i < sprites.Length; i++)
        {
            UISprite sprite = sprites[i];
            if (i < num)
            {
                sprite.gameObject.SetActive(true);
                sprite.spriteName = string.Format("UI-s2-zd-hongse{0}", text[i]);
            }
            else
            {
                sprite.gameObject.SetActive(false);
            }
        }
        uiGrid.Reposition();
    }
}
