﻿using UnityEngine;
using System.Collections;
/// <summary>
/// HUD行动值表现
/// </summary>
public class HUDActionValue : BaseHUD
{
    private const int Num = 5;
    private UISprite[] _actions = new UISprite[Num];
    private const string ValidState = "UI-s2-zd-xingdongzhiliang";
    private const string InvalidState = "UI-s2-zd-xingdongzhidi";
    protected override void OnInit()
    {
        Transform root = transform.Find("Action");
        for (int i = 0; i < Num; i++)
        {
            UISprite item = root.Find(i.ToString()).GetComponent<UISprite>();
            _actions[i] = item;
        }
    }

    private int curValue = -1;
    public void SetValue(float value)
    {
        int index = Mathf.FloorToInt( value * Num);
        if (curValue == index) return;
        curValue = index;
        for (int i = 0; i < Num; i++)
        {
            if (i < curValue)
            {
                _actions[i].spriteName = ValidState;
            }
            else
            {
                _actions[i].spriteName = InvalidState;
            }
        }
    }

}
