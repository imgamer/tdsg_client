﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// HUD管理类
/// </summary>
public class HUDManager : MonoSingleton<HUDManager>
{

    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {

    }

    public void SetHP(Transform target, float value)
    {
        const string resName = "hp";

        HUDBar hud = null;
        Transform trans = target.Find(resName);
        if (trans)
        {
            hud = trans.GetComponent<HUDBar>();
            if (hud)
            {
                hud.SetValue(value);
            }
        }
        else
        {
            BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
            {
                if (o)
                {
                    if (target == null)
                    {
                        AssetsManager.instance.Despawn(o);
                        return;
                    }
                    o.transform.parent = target;
                    o.transform.localPosition = Vector3.zero;
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    hud = o.GetComponent<HUDBar>();
                    hud.Init();
                    hud.SetValue(value);
                }
            });
            AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        }
    }

    public void SetActionValue(Transform target, float value)
    {
        const string resName = "action";

        HUDActionValue hud = null;
        Transform trans = target.Find(resName);
        if (trans)
        {
            hud = trans.GetComponent<HUDActionValue>();
            if (hud)
            {
                hud.SetValue(value);
            }
        }
        else
        {
            BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
            {
                if (o)
                {
                    if (target == null)
                    {
                        AssetsManager.instance.Despawn(o);
                        return;
                    }
                    o.transform.parent = target;
                    o.transform.localPosition = Vector3.zero;
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    hud = o.GetComponent<HUDActionValue>();
                    hud.Init();
                    hud.SetValue(value);
                }
            });
            AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        }
    }

    public void ShowTopHUD(Transform target, bool show)
    {
        string[] resNames = new string[] { "hp", "action" };
        for (int i = 0; i < resNames.Length; i++)
        {
            Transform trans = target.Find(resNames[i]);
            if (trans)
            {
                BaseHUD hud = trans.GetComponent<BaseHUD>();
                if (hud)
                {
                    if (show)
                    {
                        hud.Show();
                    }
                    else
                    {
                        hud.Hide();
                    }
                }
            }
        }
    }
    public void SetTitle(Transform target, string text1, string text2 = "")
    {
        const string resName = "title";

        HUDTitle hud = null;
        Transform trans = target.Find(resName);
        if (trans)
        {
            hud = trans.GetComponent<HUDTitle>();
            if (hud)
            {
                hud.SetValue(text1, text2);
            }
        }
        else
        {
            BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
            {
                if (o)
                {
                    if (target == null)
                    {
                        AssetsManager.instance.Despawn(o);
                        return;
                    }
                    o.transform.parent = target;
                    o.transform.localPosition = Vector3.zero;
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    hud = o.GetComponent<HUDTitle>();
                    hud.Init();
                    hud.SetValue(text1, text2);
                }
            });
            AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        }
    }
    public void SetState(Transform target, bool show)
    {
        const string resName = "state";

        HUDState hud = null;
        Transform trans = target.Find(resName);
        if (trans)
        {
            hud = trans.GetComponent<HUDState>();
            if (hud)
            {
                if (show) hud.Show();
                else hud.Hide();
            }
        }
        else
        {
            BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
            {
                if (o)
                {
                    if (target == null)
                    {
                        AssetsManager.instance.Despawn(o);
                        return;
                    }
                    o.transform.parent = target;
                    o.transform.localPosition = Vector3.zero;
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    hud = o.GetComponent<HUDState>();
                    hud.Init();
                    if (show) hud.Show();
                    else hud.Hide();
                }
            });
            AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        }
    }
    public void SetContent(Transform target, HUDUnit.HUDType type, string text)
    {
        if (target == null) return;
        GetUnit(target, (unit) =>
        {
            Transform root = unit.transform;
            string resName = type.ToString().ToLower();

            HUDContent content = null;
            Transform trans = root.Find(resName);
            if (trans)
            {
                content = trans.GetComponent<HUDContent>();
                if (content)
                {
                    content.AddText(text, 0);
                    unit.Add(content);
                }
            }
            else
            {
                BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
                {
                    if (o)
                    {
                        if (root == null)
                        {
                            AssetsManager.instance.Despawn(o);
                            return;
                        }
                        o.transform.parent = root;
                        o.transform.localPosition = Vector3.zero;
                        o.transform.localEulerAngles = Vector3.zero;
                        o.transform.localScale = Vector3.one;

                        content = o.GetComponent<HUDContent>();
                        content.Init();
                        content.AddText(text, 0);
                        unit.Add(content);
                    }
                });
                AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
            }
        });
    }
    private void GetUnit(Transform target, Action<HUDUnit> cb)
    {
        HUDUnit unit = null;
        const string resName = "hud_unit";
        Transform trans = target.Find(resName);
        if (trans)
        {
            unit = trans.GetComponent<HUDUnit>();
            if (cb != null) cb(unit);
        }
        else
        {
            BaseSpawnData data = new InstantiateData(resName, resName, PoolName.HUD, PoolType.Loop, (o) =>
            {
                if (o)
                {
                    if (target == null)
                    {
                        AssetsManager.instance.Despawn(o);
                        return;
                    }
                    o.transform.parent = target;
                    o.transform.localPosition = Vector3.zero;
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    unit = o.GetComponent<HUDUnit>();
                    if (cb != null) cb(unit);
                }
            });
            AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
        }
    }

    public void RemoveHUD(Transform target)
    {

    }

}
