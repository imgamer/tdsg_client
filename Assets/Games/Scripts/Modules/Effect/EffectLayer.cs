﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 特效分层组件
/// </summary>
[DisallowMultipleComponent]
public class EffectLayer : MonoBehaviour 
{
    [SerializeField]
    private int sortingOrder = 1;//不允许为0，因为假定作用对象的SortingOrder = 0
    public int SortingOrder 
    {
        get { return sortingOrder; }
        private set 
        {
            if (value == 0 || Mathf.Abs(value) > 10) return;
            sortingOrder = value; 
        } 
    }
    private bool inited = false;
    private List<Renderer> renderers = new List<Renderer>();
    private void Init()
    {
        if (inited) return;
        inited = true;
        GetGetRenderers(transform);
    }

    private void GetGetRenderers(Transform root)
    {
        if (root == null) return;
        renderers.Clear();
        Renderer[] rs = root.GetComponents<Renderer>();
        if (rs.Length > 0) renderers.AddRange(rs);
        GetChildRenderers(root);
    }
    private void GetChildRenderers(Transform root)
    {
        for (int i = 0; i < root.childCount; i++)
        {
            Transform child = root.GetChild(i);
            if (child.GetComponent<EffectLayer>()) continue;
            Renderer[] rs = child.GetComponents<Renderer>();
            if (rs.Length > 0) renderers.AddRange(rs);
            GetChildRenderers(child);
        }
    }

#if UNITY_EDITOR
    public void SetSortingOrder(int value)
    {
        SortingOrder = value;
        SetSortingOffset(0);
    }
#endif

    public void SetSortingOffset(int offset)
    {
        Init();
        for (int i = 0; i < renderers.Count; i++)
        {
            Renderer r = renderers[i];
            r.sortingOrder = SortingOrder + offset;
        }
    }

}
