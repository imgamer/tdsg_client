﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 特效预览脚本，方便美术预览翻转和复原特效
/// </summary>
public class EffectDisplay : MonoBehaviour 
{
    void Awake()
    {
#if UNITY_EDITOR
        
#else
        Destroy(this);
#endif
    }

    private Transform[] _particleSystems = null;
    private Vector3[] _particleLocalEulerAngles = null;
    public void RecordParticleSystem()
    {
        ParticleSystem[] particleSystems = transform.GetComponentsInChildren<ParticleSystem>(true);
        int count = particleSystems.Length;
        _particleSystems = new Transform[count];
        _particleLocalEulerAngles = new Vector3[count];
        for (int i = 0; i < count; i++)
        {
            Transform trans = particleSystems[i].transform;
            _particleSystems[i] = trans;
            _particleLocalEulerAngles[i] = trans.localEulerAngles;
        }
    }

    public bool Flip { get; private set; }
    public void SetParticleSystem(bool flip)
    {
        Flip = flip;
        transform.localScale = Flip ? new Vector3(-1, 1, 1) : Vector3.one;
        if (_particleSystems == null || _particleLocalEulerAngles == null) return;
        Vector3 temp = new Vector3(0, 180, 0);
        for (int i = 0; i < _particleSystems.Length; i++)
        {
            Transform trans = _particleSystems[i];
            float absX = Mathf.Abs(trans.localScale.x);
            if (Flip)
            {
                trans.localScale = new Vector3(-absX, trans.localScale.y, trans.localScale.z);
                trans.localEulerAngles = _particleLocalEulerAngles[i] + temp;
            }
            else
            {
                trans.localScale = new Vector3(absX, trans.localScale.y, trans.localScale.z);
                trans.localEulerAngles = _particleLocalEulerAngles[i];
            }
        }
    }
}
