﻿using UnityEngine;
using System.Collections;
using UnityEditor;
/// <summary>
/// 特效预览脚本编辑器
/// </summary>
[CanEditMultipleObjects]
[CustomEditor(typeof(EffectDisplay), true)]
public class EffectDisplayInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("粒子翻转预览，注意复原状态后编辑", MessageType.Info);
        GUILayout.BeginHorizontal();
        EffectDisplay disp = target as EffectDisplay;
        if (disp.Flip)
        {
            if (GUILayout.Button("复   原"))
            {
                disp.SetParticleSystem(false);
            }
        }
        else
        {
            if (GUILayout.Button("翻   转"))
            {
                disp.RecordParticleSystem();
                disp.SetParticleSystem(true);
            }
        }
        GUILayout.EndHorizontal();

        ParticleSystem[] particleSystems = disp.GetComponentsInChildren<ParticleSystem>(true);
        for (int i = 0; i < particleSystems.Length; i++)
        {
            ParticleSystem p = particleSystems[i];
            GUI.color = p.gameObject.activeInHierarchy ? Color.white : Color.gray;
            GUILayout.BeginHorizontal();
            EditorGUILayout.ObjectField(string.Format("ParticleSystem_{0}", i), p, typeof(ParticleSystem), false);
            GUILayout.EndHorizontal();
        }
    }
}
