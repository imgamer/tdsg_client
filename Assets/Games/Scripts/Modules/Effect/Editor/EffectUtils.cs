﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;
/// <summary>
/// 特效批处理工具
/// </summary>
public class EffectUtils
{
    [MenuItem("Effect/批处理特效")]
    private static void Deal()
    {
        if (Application.isPlaying) return;
        if (!EditorUtility.DisplayDialog("批处理特效", "是否开始批处理", "Yes", "No"))
        {
            return;
        }
        DistributeAssets();
        BatchAll();
        EditorUtility.DisplayDialog("恭喜", "完成批处理", "OK");
    }

    #region 美术工程特效转移到工程特效目录下
    private static readonly Dictionary<string, int> BatchEffectDict = new Dictionary<string, int>() 
    {
        {"UIEffects", LayerMask.NameToLayer("UIEffect")},
        {"SkillEffects", LayerMask.NameToLayer("SkillEffect")},
        //{"Temp", LayerMask.NameToLayer("SkillEffect")},
        {"SceneEffects", LayerMask.NameToLayer("SceneEffect")},
    };
    private static readonly Dictionary<string, Type> BatchEffectScripts = new Dictionary<string, Type>() 
    {
        {"UIEffects", typeof(UIEffect)},
        {"SkillEffects", typeof(SkillEffect)},
        //{"Temp", typeof(SkillEffect)},
        {"SceneEffects", typeof(SceneEffect)},
    };
    private static void DistributeAssets()
    {
        foreach (var item in BatchEffectDict)
        {
            string relativePath = item.Key;
            DirectoryInfo dirInfo = new DirectoryInfo(GetDesignPath(relativePath));
            if (dirInfo.GetFiles().Length + dirInfo.GetDirectories().Length > 0)
            {
                ClearProgramAssets(relativePath);
                MoveDesignAssets(relativePath);
            }
        }
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }
    private static void ClearProgramAssets(string relativePath)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(GetProgramPath(relativePath));
        FileInfo[] files = dirInfo.GetFiles();
        for (int i = 0; i < files.Length; i++)
        {
            files[i].Delete();
        }
    }
    private static void MoveDesignAssets(string relativePath)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(GetDesignPath(relativePath));
        FileInfo[] files = dirInfo.GetFiles();
        for (int i = 0; i < files.Length; i++)
        {
            FileInfo file = files[i];
            file.MoveTo(Path.Combine(GetProgramPath(relativePath), file.Name));
        }
    }
    private const string DesignPath = "Assets/IGSoft_Resources/Projects/2 Project";
    private static string GetDesignPath(string relativePath)
    {
        return string.Format("{0}/{1}", DesignPath, relativePath);
    }
    private const string ProgramPath = "Assets/Games/GameAssets/UnfixedAssets/Resources";
    private static string GetProgramPath(string relativePath)
    {
        return string.Format("{0}/{1}", ProgramPath, relativePath);
    }
    #endregion

    #region 批处理特效
    private static void BatchAll()
    {
        foreach (var item in BatchEffectDict)
        {
            Batch(item.Key, item.Value);
        }
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }
    private static void Batch(string relativePath, int layer)
    {
        try
        {
            string path = GetProgramPath(relativePath);
            List<string> paths = GetPaths(path);
            foreach (var p in paths)
            {
                GameObject o = AssetDatabase.LoadAssetAtPath<GameObject>(p);
                if (o)
                {
                    BaseEffect effect = o.GetComponent<BaseEffect>();
                    if (effect != null)
                    {
                        SetPosition(o.transform, o.transform.GetChild(0));
                        SetLayer(o, layer);
                        EditorUtility.SetDirty(o);
                        continue;
                    }

                    GameObject parent = new GameObject(o.name);
                    parent.transform.localPosition = Vector3.zero;
                    parent.transform.localEulerAngles = Vector3.zero;
                    parent.transform.localScale = Vector3.one;
                    effect = parent.AddComponent(BatchEffectScripts[relativePath]) as BaseEffect;

                    GameObject child = GameObject.Instantiate<GameObject>(o);
                    child.name = o.name;
                    SetPosition(parent.transform, child.transform);
                    SetLayer(parent, layer);
                    string newPath = string.Format("{0}/{1}.prefab", path, parent.name);
                    PrefabUtility.CreatePrefab(newPath, parent, ReplacePrefabOptions.ConnectToPrefab);
                    GameObject.DestroyImmediate(parent);
                }
            }
        }
        catch (System.Exception ex)
        {
            EditorUtility.DisplayDialog("错误", string.Format("批处理出错：{0}", ex), "OK");
        }
    }

    private static void SetLayer(GameObject o, int layer)
    {
        Transform[] children = o.GetComponentsInChildren<Transform>(true);
        foreach (var item in children)
        {
            item.gameObject.layer = layer;
        }
    }

    private static void SetPosition(Transform parent, Transform child)
    {
        child.parent = parent;
        child.localPosition = Vector3.zero;
        child.localEulerAngles = new Vector3(-20, 0, 0);
        child.localScale = Vector3.one;
    }

    private static List<string> GetPaths(string path)
    {
        List<string> ret = new List<string>();

        if (string.IsNullOrEmpty(path)) return ret;
        string[] paths = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
        foreach (var p in paths)
        {
            string str = p.Replace('\\', '/');
            ret.Add(str);
        }
        return ret;
    }
    #endregion
}
