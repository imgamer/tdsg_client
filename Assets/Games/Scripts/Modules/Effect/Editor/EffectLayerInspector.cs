﻿using UnityEngine;
using System.Collections;
using UnityEditor;
/// <summary>
/// 特效分层组件编辑器
/// </summary>
[CustomEditor(typeof(EffectLayer))]
public class EffectLayerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("不建议内嵌使用", MessageType.Warning);
        EditorGUILayout.HelpBox("默认假定作用目标的sortingOrder = 0, 故不能设置为0，且绝对值不能大于10", MessageType.Info);
        EffectLayer effectLayer = target as EffectLayer;
        GUILayout.BeginHorizontal();
        int sortingOrder = EditorGUILayout.IntField("SortingOrder", effectLayer.SortingOrder, GUILayout.Width(200));
        GUILayout.EndHorizontal();
        if (sortingOrder != effectLayer.SortingOrder)
        {
            effectLayer.SetSortingOrder(sortingOrder);
            EditorUtility.SetDirty(effectLayer);
        }
    }
}