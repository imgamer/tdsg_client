﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 特效基类
/// </summary>
public abstract class BaseEffect : MonoBehaviour
{
    void Awake()
    {
        Init();
    }

    private GameObject[] _children = null;
    private void Init()
    {
        int count = transform.childCount;
        if (count <= 0) return;
        _children = new GameObject[count];
        for (int i = 0; i < _children.Length; i++)
        {
            _children[i] = transform.GetChild(i).gameObject;
        }
        InitParticleSystem();
        //InitEffectLayer();暂未调用，需要特效分层显示时，开启
        OnInit();
    }

    private EffectLayer[] _layers = null;
    private void InitEffectLayer()
    {
        _layers = transform.GetComponentsInChildren<EffectLayer>(true);
    }

    private Transform[] _particleSystems = null;
    private Vector3[] _particleLocalEulerAngles = null;
    private void InitParticleSystem()
    {
        ParticleSystem[] particleSystems = transform.GetComponentsInChildren<ParticleSystem>(true);
        int count = particleSystems.Length;
        _particleSystems = new Transform[count];
        _particleLocalEulerAngles = new Vector3[count];
        for (int i = 0; i < count; i++)
        {
            Transform trans = particleSystems[i].transform;
            _particleSystems[i] = trans;
            _particleLocalEulerAngles[i] = trans.localEulerAngles;
        }
    }

    protected virtual void OnInit()
    {
        Hide();
    }

    protected void SetActive(bool active)
    {
        if (_children == null) return;
        for (int i = 0; i < _children.Length; i++)
        {
            _children[i].SetActive(active);
        }
    }

    /// <summary>
    /// 播放特效
    /// </summary>
    /// <param name="flip">是否镜像翻转</param>
    /// <param name="targetOrder">作用对象的SortingOrder</param>
    public void Play(bool flip, int targetOrder = 0)
    {
        SetFlip(flip);
        //SetSortingOrder(targetOrder);暂未调用，需要特效分层显示时，开启
        SetActive(true);
    }

    public void Hide()
    {
        SetActive(false);
    }

    private void SetSortingOrder(int sortingOrder)
    {
        if (_layers == null || _layers.Length <= 0) return;
        for (int i = 0; i < _layers.Length; i++)
        {
            _layers[i].SetSortingOffset(sortingOrder);
        }
    }

    private void SetFlip(bool flip)
    {
        transform.localScale = flip ? new Vector3(-1, 1, 1) : Vector3.one;
        if (_particleSystems == null || _particleLocalEulerAngles == null) return;
        Vector3 temp = new Vector3(0, 180, 0);
        for (int i = 0; i < _particleSystems.Length; i++)
        {
            Transform trans = _particleSystems[i];
            float absX = Mathf.Abs(trans.localScale.x);
            if (flip)
            {
                trans.localScale = new Vector3(-absX, trans.localScale.y, trans.localScale.z);
                trans.localEulerAngles = _particleLocalEulerAngles[i] + temp;
            }
            else
            {
                trans.localScale = new Vector3(absX, trans.localScale.y, trans.localScale.z);
                trans.localEulerAngles = _particleLocalEulerAngles[i];
            }
        }
    }
}
