﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 特效加载管理类
/// </summary>
public class EffectManager : MonoSingleton<EffectManager>
{
    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {

    }

    /// <summary>
    /// 获取特效实例
    /// </summary>
    /// <param name="effectName"></param>
    /// <param name="root"></param>
    /// <param name="cb"></param>
    public void GetEffect(string effectName, Transform root, Action<BaseEffect> cb)
    {
        if (string.IsNullOrEmpty(effectName))
        {
            if (cb != null) cb(null);
            return;
        }
        
        bool contains = AssetsManager.instance.Contains(effectName, effectName, PoolName.Permanent, PoolType.Once);
        PoolName poolName = contains ? PoolName.Permanent : PoolName.Effect;
        BaseSpawnData data = new InstantiateData(effectName, effectName, poolName, PoolType.Once, (go) =>
        {
            if (go)
            {
                go.transform.parent = root;
                BaseEffect effect = go.GetComponent<BaseEffect>();
                if (effect == null)
                {
                    Printer.LogError(string.Format("特效：{0} 缺少脚本Effect！", effectName));
                    AssetsManager.instance.Despawn(go);
                    if (cb != null) cb(null);
                }
                else
                {
                    if (cb != null) cb(effect);
                }
            }
            else
            {
                if (cb != null) cb(null);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    /// <summary>
    /// 播放UI特效
    /// </summary>
    /// <param name="effectName"></param>
    /// <param name="root"></param>
    /// <param name="position"></param>
    /// <param name="sortingOrder"></param>
    public void SpawnUIEffect(string effectName, Transform root, Vector3 localPosition)
    {
        GetEffect(effectName, root, (effect) =>
        {
            if (effect)
            {
                Transform trans = effect.transform;
                trans.localPosition = localPosition;
                trans.localEulerAngles = Vector3.zero;
                effect.Play(false);
            }
        });
    }
    /// <summary>
    /// 播放技能特效
    /// </summary>
    /// <param name="effectName"></param>
    /// <param name="root"></param>
    /// <param name="loop"></param>
    /// <param name="existTime"></param>
    /// <param name="position"></param>
    /// <param name="flip"></param>
    /// <param name="cb"></param>
    public void SpawnEffect(string effectName, Transform root, bool loop, float existTime, Vector3 position, bool flip, Action<BaseEffect> cb = null)
    {
        GetEffect(effectName, root, (effect) =>
        {
            if (effect)
            {
                Transform trans = effect.transform;
                trans.position = position;
                effect.Play(flip);
                if (!loop)
                {
                    DespawnEffect(effect, existTime);
                }
            }
            if (cb != null) cb(effect);
        });
    }
    /// <summary>
    /// 销毁特效
    /// </summary>
    /// <param name="effect"></param>
    public void DespawnEffect(BaseEffect effect, float existTime = 0)
    {
        if (effect == null) return;
        AssetsManager.instance.Despawn(effect.gameObject, existTime);
    }

    /// <summary>
    /// 预加载主角技能特效
    /// </summary>
    /// <param name="cb"></param>
    public void PreLoadPlayerEffects(Action<bool> cb)
    {
        List<int> skillIDs = GameMain.Player.skillBox.skills;
        List<string> effectNames = new List<string>();

        List<BaseSpawnData> preloadList = new List<BaseSpawnData>();
        for (int i = 0; i < skillIDs.Count; i++)
        {
            int skillID = skillIDs[i];
            string seqName = SkillConfig.SharedInstance.GetSeqName(skillID);
            if (string.IsNullOrEmpty(seqName)) continue;
            List<string> names = SeqRelateEffectConfig.SharedInstance.GetEffectNames(seqName);
            for (int j = 0; j < names.Count; j++)
            {
                string item = names[j];
                if (effectNames.Contains(item)) continue;
                effectNames.Add(item);

                PreloadData data = new PreloadData(item, item, PoolName.Permanent, PoolType.Once);
                preloadList.Add(data);
            }
        }
        AssetsManager.instance.AsyncPreload(preloadList, cb);
    }

}
