﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 队伍目标设置界面
/// </summary>
public class UITeamTarget : UIWin
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        SetState(false);
        _targetLimitPage.Open(this);
    }

    protected override void OnRefresh()
    {
        RefreshMenu();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    #region 初始化
    private UI.Table _uiTable;
    private TeamTargetMenuItem _template;

    private TeamTargetLimitPage _targetLimitPage;
    private GameObject _btnCheck;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _targetLimitPage = center.Find("TargetLimit").GetComponent<TeamTargetLimitPage>();
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        _btnCheck = center.Find("BtnCheck").gameObject;
        InputManager.instance.AddClickListener(_btnCheck, (go) =>
        {

        });

        Transform table = center.Find("Menu/UITable");
        _uiTable = table.GetComponent<UI.Table>();
        _template = _uiTable.transform.Find("Item").GetComponent<TeamTargetMenuItem>();
        _template.Init(this);
        _template.gameObject.SetActive(false);
    }
    #endregion

    #region 刷新菜单
    private List<TeamTargetMenuItem> _targetMenuItems = new List<TeamTargetMenuItem>();
    private void RefreshMenu()
    {
        int dataNum = 5;
        int itemNum = _targetMenuItems.Count;

        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {

                if (i < itemNum)
                {
                    TeamTargetMenuItem item = _targetMenuItems[i];
                    item.gameObject.SetActive(true);
                    item.ID = i;
                    item.DataNum = UnityEngine.Random.Range(0, 2);
                    item.UpdateState();
                    SetData(item);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    TeamTargetMenuItem item = Instantiate(_template) as TeamTargetMenuItem;
                    _template.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiTable.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(this);
                    _targetMenuItems.Add(item);

                    item.ID = i;
                    item.DataNum = UnityEngine.Random.Range(0, 2);
                    item.UpdateState();
                    SetData(item);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                TeamTargetMenuItem item = _targetMenuItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    item.ID = i;
                    item.UpdateState();
                    SetData(item);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiTable.Reposition();
    }
    private void SetData(TeamTargetMenuItem menuItem)
    {
        UIGrid uiGrid = menuItem.Grid;
        GameObject template = menuItem.Template;
        List<GameObject> items = menuItem.Items;
        int itemNum = items.Count;
        if (menuItem.DataNum > itemNum)
        {
            for (int i = 0; i < menuItem.DataNum; i++)
            {

                if (i < itemNum)
                {
                    GameObject item = items[i];
                    item.SetActive(true);
                    InputManager.instance.AddClickListener(item, (go) =>
                    {
                        Select(menuItem);
                    });
                    //TODO
                }
                else
                {
                    template.SetActive(true);
                    GameObject item = Instantiate(template) as GameObject;
                    template.SetActive(false);
                    item.transform.parent = uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    items.Add(item);
                    InputManager.instance.AddClickListener(item, (go) =>
                    {
                        Select(menuItem);
                    });
                    //TODO
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                GameObject item = items[i];
                if (i < menuItem.DataNum)
                {

                    item.SetActive(true);
                    InputManager.instance.AddClickListener(item, (go) =>
                    {
                        Select(menuItem);
                    });
                    //TODO
                }
                else
                {
                    item.SetActive(false);
                }
            }
        }
        NGUITools.SetActive(uiGrid.gameObject, menuItem.OpenState);
        if (menuItem.OpenState) uiGrid.Reposition();
    }
    #endregion

    #region 选择状态相关
    private void SetState(bool openState)
    {
        foreach (var item in _targetMenuItems)
        {
            item.SetState(openState);
        }
        _uiTable.Reposition();
    }
    private void SwitchState(TeamTargetMenuItem menuItem)
    {
        if (menuItem.DataNum == 0)
        {
            GameMain.Player.teamModule.SetCurTarget(menuItem.ID);
            foreach (var item in _targetMenuItems)
            {
                if (item == menuItem)
                {
                    item.SetState(true);
                    _targetLimitPage.Open(this);
                }
                else
                {
                    item.UpdateState();
                    item.SetState(false);
                }
            }
        }
        else
        {
            foreach (var item in _targetMenuItems)
            {
                if (item == menuItem)
                {
                    item.SetState(!item.OpenState);
                    _targetLimitPage.Open(this);
                }
                else
                {
                    item.SetState(false);
                }
            }
        }
        _uiTable.Reposition();
    }
    private void Select(TeamTargetMenuItem menuItem)
    {
        GameMain.Player.teamModule.SetCurTarget(menuItem.ID);
        foreach (var item in _targetMenuItems)
        {
            item.UpdateState();
            item.SetState(false);
            _targetLimitPage.Open(this);
        }
        _uiTable.Reposition();
    }
    private void DefaultSelect()
    {

    }
    #endregion
}
