﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 组队申请列表
/// </summary>
public class TeamApplicationItem : WinItem
{
    private Transform _entityRoot;
    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        Transform child = _entityRoot.Find(resName);
        if (child != null)
        {
            if (child.name.Equals(resName))
            {
                AnimationControl control = child.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
                return;
            } 
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 3;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
    private UILabel _name;
    public string Name
    {
        set { _name.text = value; }
    }
    private UISprite _schoolIcon;
    public string SchoolIcon
    {
        set { _schoolIcon.spriteName = value; }
    }
    private UISprite _typeIcon;
    public string TypeIcon
    {
        set { _typeIcon.spriteName = value; }
    }
    private UILabel _level;
    public string Level
    {
        set { _level.text = value; }
    }
    private GameObject _btnAccept;
    public GameObject BtnAccept
    {
        get { return _btnAccept; }
    }
    protected override void OnInit()
    {
        _entityRoot = transform.Find("EntityRoot");
        _name = transform.Find("Name/Text").GetComponent<UILabel>();
        _schoolIcon = transform.Find("School/Icon").GetComponent<UISprite>();
        _typeIcon = transform.Find("Type/Icon").GetComponent<UISprite>();
        _level = transform.Find("Level/Text").GetComponent<UILabel>();
        _btnAccept = transform.Find("BtnAccept").gameObject;
    }
}
