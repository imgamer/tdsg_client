﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 组队界面
/// </summary>
public class UITeam : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<TeamModule.TeamState>(EventID.EVNT_TEAM_STATE_UPDATE, ChangeState);
        InitItem();
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {
        SwitchTo(GameMain.Player.teamModule.State);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<TeamModule.TeamState>(EventID.EVNT_TEAM_STATE_UPDATE, ChangeState);
    }
    private GameObject _tabs;
    private UISprite _tabFormationBG;
    private UISprite _tabApplicationBG;
    private const string normalState = "UI-s2-common-weixuanzeanniu";
    private const string pressState = "UI-s2-common-xuanzeaniu";
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) => 
        {
            UIManager.instance.CloseWindow(winID);
        });
        _tabs = center.Find("Tabs").gameObject;
        GameObject tabFormation = _tabs.transform.Find("Formation").gameObject;
        _tabFormationBG = tabFormation.transform.Find("Icon").GetComponent<UISprite>();
        GameObject tabApplication = _tabs.transform.Find("Application").gameObject;
        _tabApplicationBG = tabApplication.transform.Find("Icon").GetComponent<UISprite>();

        InputManager.instance.AddClickListener(tabFormation, (go) =>
        {
            SwitchTab(TabType.Formation);
        });
        
        InputManager.instance.AddClickListener(tabApplication, (go) =>
        {
            SwitchTab(TabType.Application);
        });
    }
    public enum TabType
    {
        Formation,
        Application,
    }
    private void SwitchTab(TabType type)
    {
        switch (type)
        {
            case TabType.Formation:
                _tabFormationBG.spriteName = pressState;
                _tabApplicationBG.spriteName = normalState;
                _formationPage.Open(this);
                if (GameMain.Player.teamModule.State == TeamModule.TeamState.Captain)
                {
                    _captainPage.Open(this);
                    _memberPage.Close();
                }
                else if (GameMain.Player.teamModule.State == TeamModule.TeamState.Member)
                {
                    _captainPage.Close();
                    _memberPage.Open(this);
                }
                _applicationPage.Close();
                break;
            case TabType.Application:
                _tabFormationBG.spriteName = normalState;
                _tabApplicationBG.spriteName = pressState;
                _formationPage.Close();
                _captainPage.Close();
                _memberPage.Close();
                _applicationPage.Open(this);
                break;
        }
    }
    private TeamFormationPage _formationPage;
    private TeamMainPage _teamMainPage;
    private TeamCaptainPage _captainPage;
    private TeamMemberPage _memberPage;
    private TeamApplicationPage _applicationPage;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _formationPage = center.Find("Formation").GetComponent<TeamFormationPage>();
        _teamMainPage = center.Find("TeamMain").GetComponent<TeamMainPage>();
        _captainPage = center.Find("Captain").GetComponent<TeamCaptainPage>();
        _memberPage = center.Find("Member").GetComponent<TeamMemberPage>();
        _applicationPage = center.Find("Application").GetComponent<TeamApplicationPage>();
    }
    private void SwitchTo(TeamModule.TeamState type)
    {
        switch (type)
        {
            case TeamModule.TeamState.Default:
                _tabs.SetActive(false);
                _formationPage.Open(this);
                _teamMainPage.Open(this);
                _captainPage.Close();
                _memberPage.Close();
                _applicationPage.Close();
                break;
            case TeamModule.TeamState.Captain:
                _tabs.SetActive(true);
                _teamMainPage.Close();
                SwitchTab(TabType.Formation);
                break;
            case TeamModule.TeamState.Member:
                _tabs.SetActive(true);
                _teamMainPage.Close();
                SwitchTab(TabType.Formation);
                break;
        }
    }

    private void ChangeState(TeamModule.TeamState state)
    {
        if (!Active) return;
        SwitchTo(state);
    }
}
