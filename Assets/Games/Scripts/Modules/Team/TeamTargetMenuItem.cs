﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 队伍目标菜单Item
/// </summary>
public class TeamTargetMenuItem : WinItem
{
    public int ID = 0;

    private UILabel name1;
    private UILabel name2;
    private UILabel desc;
    public void UpdateState()
    {
        if(ID == GameMain.Player.teamModule.CurID)
        {
            check.enabled = true;
            if (dataNum > 0)
            {
                name1.text = "";
                name2.text = "副本任务";
                desc.text = "混世魔王-普通";
            }
            else
            {
                name1.text = "单个";
                name2.text = "";
                desc.text = "";
            }
        }
        else
        {
            check.enabled = false;
            name1.text = dataNum > 0 ? "副本任务" : "单个";
            name2.text = "";
            desc.text = "";
        }
    }
    private UISprite check;
    private UISprite mark;
    private bool openState = false;
    public bool OpenState
    {
        get { return openState; }
    }
    public void SetState(bool state)
    {
        if (openState == state) return;
        openState = state;
        if (dataNum > 0)
        {
            uiGrid.gameObject.SetActive(openState);
            if (openState)
            {
                mark.transform.localEulerAngles = Vector3.zero;
                uiGrid.Reposition();
            }
            else
            {
                mark.transform.localEulerAngles = new Vector3(0, 0, -180);
            }
        }
        if(ID == GameMain.Player.teamModule.CurID)
        {
            check.enabled = true;
        }
        else
        {
            check.enabled = false;
        }
    }
    private UIGrid uiGrid;
    public UIGrid Grid
    {
        get { return uiGrid; }
    }
    private GameObject template;
    public GameObject Template
    {
        get { return template; }
    }
    private List<GameObject> items = new List<GameObject>();
    public List<GameObject> Items
    {
        get { return items; }
    }
    private int dataNum;
    public int DataNum
    {
        get { return dataNum; }
        set 
        {
            dataNum = value;
            if (dataNum <= 0)
            {
                mark.enabled = false;
            }
            else
            {
                mark.enabled = true;
            }
        }
    }
    

    protected override void OnInit()
    {
        name1 = transform.Find("Desc/Text1/Name").GetComponent<UILabel>();
        name2 = transform.Find("Desc/Text2/Name").GetComponent<UILabel>();
        desc = transform.Find("Desc/Text2/Desc").GetComponent<UILabel>();
        name1.text = "";
        name2.text = "";
        desc.text = "";

        mark = transform.Find("Mark").GetComponent<UISprite>();
        check = transform.Find("Check").GetComponent<UISprite>();
        uiGrid = transform.Find("UIGrid").GetComponent<UIGrid>();
        template = uiGrid.transform.Find("Item").gameObject;
        template.SetActive(false);
    }
}
