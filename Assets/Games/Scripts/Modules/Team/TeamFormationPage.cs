﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 组队队伍页面
/// </summary>
public class TeamFormationPage : WinPage
{
    private TeamFormationItem[] _items = new TeamFormationItem[3];
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, RefreshFormation);
        Transform grid = transform.Find("UIGrid");
        for (int i = 0; i < _items.Length; i++)
        {
            TeamFormationItem item = grid.Find(i.ToString()).GetComponent<TeamFormationItem>();
            item.Init(ParentUI);
            _items[i] = item;
        }
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateFormation();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, RefreshFormation);
    }
    private void UpdateFormation()
    {
        List<TeamModule.TeamMemberData> datas = GameMain.Player.teamModule.TeamMembers;
        for (int i = 0; i < _items.Length; i++)
        {
            TeamFormationItem item = _items[i];
            if (i < datas.Count)
            {
                TeamModule.TeamMemberData data = datas[i];
                item.gameObject.SetActive(true);
                item.SetEntity("diancangpai");
                item.Name = data.name;
                item.State = GetStateIconName(data.status);
                item.Level = string.Format("Lv.{0}", data.level);
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
    }
    private const string captain = "UI-s2-common-duizhangzi";
    private const string member = "UI-s2-common-zuzhanzi";
    private const string leave = "UI-s2-common-zhanli";
    private const string offline = "UI-s2-common-lixian";
    private string GetStateIconName(Byte state)
    {
        switch (state)
        {
            case Define.TEAM_MEMBER_PENDING:
                return "";
                break;
            case Define.TEAM_MEMBER_OFFLINE:
                return offline;
                break;
            case Define.TEAM_MEMBER_SEPARATING:
                return leave;
                break;
            case Define.TEAM_MEMBER_FOLLOWING:
                return "";
                break;
            case Define.TEAM_MEMBER_LEADING:
                return captain;
                break;
        }
        return "";
    }
    private void RefreshFormation(object o)
    {
        if (!Active) return;
        UpdateFormation();
    }
}
