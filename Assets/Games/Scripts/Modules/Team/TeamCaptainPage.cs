﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 队长所在组队界面（组队状态）
/// </summary>
public class TeamCaptainPage : WinPage
{
    private UILabel _targetDesc;
    private GameObject _menu;
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_TEAM_TARGET_UPDATE, UpdateTarget);
        GameObject btnQuit = transform.Find("BtnQuit").gameObject;
        InputManager.instance.AddClickListener(btnQuit, (go) =>
        {
            GameMain.Player.teamModule.RequestQuitTeam();
        });
        GameObject target = transform.Find("Target").gameObject;
        _targetDesc = target.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(target, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UITeamTarget);
        });
        GameObject btnAnnounce = transform.Find("BtnAnnounce").gameObject;
        InputManager.instance.AddClickListener(btnAnnounce, (go) =>
        {
            GameMain.Player.chatModule.SendChatData(Define.CHAT_CHANNEL_PARTY, _targetDesc.text);
        });

        _menu = transform.Find("Menu").gameObject;
        Transform grid = transform.Find("Menu/UIGrid");
        GameObject btnFriend = grid.Find("0").gameObject;
        InputManager.instance.AddClickListener(btnFriend, (go) =>
        {
            _menu.SetActive(false);
        });
        GameObject btnFaction = grid.Find("1").gameObject;
        InputManager.instance.AddClickListener(btnFaction, (go) =>
        {
            _menu.SetActive(false);
        });
        GameObject btnInvite = transform.Find("BtnInvite").gameObject;
        InputManager.instance.AddClickListener(btnInvite, (go) =>
        {
            _menu.SetActive(!_menu.activeSelf);
        });
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateTarget();
        _menu.SetActive(false);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_TEAM_TARGET_UPDATE, UpdateTarget);
    }

    private void UpdateTarget(object o = null)
    {
        if (!Active) return;
        Vector2 limit = GameMain.Player.teamModule.TargetLimit;
        _targetDesc.text = string.Format("目标：{0}({1}级-{2}级)", "混世魔王", limit.x, limit.y);
    }
}

