﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 申请列表界面（组队状态）
/// </summary>
public class TeamApplicationPage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_TEAM_APPLICANT_UPDATE, UpdateApplyMembers);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_TEAM_APPLICANT_UPDATE, UpdateApplyMembers);
    }
    private GameObject _default;
    private UI.Grid _uiGrid;
    private TeamApplicationItem _template;
    private List<TeamApplicationItem> _items = new List<TeamApplicationItem>();
    private void InitItem()
    {
        GameObject btnQuit = transform.Find("BtnQuit").gameObject;
        InputManager.instance.AddClickListener(btnQuit, (go) =>
        {
            GameMain.Player.teamModule.RequestQuitTeam();
        });
        GameObject btnClear = transform.Find("BtnClear").gameObject;
        InputManager.instance.AddClickListener(btnClear, (go) =>
        {
            GameMain.Player.teamModule.ClearTeamRequest();
        });
        _default = transform.Find("Default").gameObject;
        Transform grid = transform.Find("ScrollView/UIGrid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        _template = grid.Find("Item").GetComponent<TeamApplicationItem>();
        _template.gameObject.SetActive(false);
    }

    private void UpdateItems()
    {
        List<TeamModule.TeamMemberData> applyMembers = GameMain.Player.teamModule.ApplyMembers;
        int dataNum = applyMembers.Count;
        _default.SetActive(dataNum == 0);
        int itemNum = _items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                TeamModule.TeamMemberData data = applyMembers[i];
                if (i < itemNum)
                {
                    TeamApplicationItem item = _items[i];
                    item.gameObject.SetActive(true);
                    item.Name = data.name;
                    item.Level = string.Format("Lv.{0}", data.level);
                    item.SetEntity("diancangpai");
                    InputManager.instance.AddClickListener(item.BtnAccept, (go) => 
                    {
                        GameMain.Player.teamModule.AcceptJoinTeamRequest(data.dbId);
                    });
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    TeamApplicationItem item = Instantiate(_template) as TeamApplicationItem;
                    _template.gameObject.SetActive(false);
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(ParentUI);
                    _items.Add(item);
                    item.Name = data.name;
                    item.Level = string.Format("Lv.{0}", data.level);
                    item.SetEntity("diancangpai");
                    InputManager.instance.AddClickListener(item.BtnAccept, (go) =>
                    {
                        GameMain.Player.teamModule.AcceptJoinTeamRequest(data.dbId);
                    });
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                TeamApplicationItem item = _items[i];
                if (i < dataNum)
                {
                    TeamModule.TeamMemberData data = applyMembers[i];
                    item.gameObject.SetActive(true);
                    item.Name = data.name;
                    item.Level = string.Format("Lv.{0}", data.level);
                    item.SetEntity("diancangpai");
                    InputManager.instance.AddClickListener(item.BtnAccept, (go) =>
                    {
                        GameMain.Player.teamModule.AcceptJoinTeamRequest(data.dbId);
                    });
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }
    private void UpdateApplyMembers(object o)
    {
        if (!Active) return;
        UpdateItems();
    }
}
