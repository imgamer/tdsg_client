﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DBID_TYPE = System.UInt64;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;
/// <summary>
/// 组队数据类
/// </summary>
public class TeamModule
{
    public class TeamMemberData
    {
        public DBID_TYPE dbId;
        public Int32 id;
        public string name;
        public UInt16 level;
        public UInt16 feature; //特征值，包含性别、职业等属性
        public Byte status;    //状态，暂离，跟随，领导，离线

        private KBEngine.GameObject entity;
        public void SetEntity(Int32 id)
        {
            this.id = id;
            entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
        }
        public KBEngine.GameObject GetEntity()
        {
            if (entity == null || entity.SceneEntityObj == null)
            {
                entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
            }
            return entity;
        }
    }

    public KBEngine.GameObject getCaptainEntity()
    {
        foreach(TeamMemberData member in _teamMembers)
        {
            if (member.status == Define.TEAM_MEMBER_LEADING)
            {
                return member.GetEntity();
            }
        }
        return null;
    }

    public enum TeamState
    {
        Default,  //默认状态，没有组队
        Matching, //自动匹配状态
        Captain,  //（队长）组队状态
        Member,   //（队员）组队状态
    }
    private KBEngine.Avatar _avatar;
    //当前队伍列表
    private List<TeamMemberData> _teamMembers = new List<TeamMemberData>();
    public List<TeamMemberData> TeamMembers
    {
        get { return _teamMembers; }
    }
    //当前请求列表
    private List<TeamMemberData> _applyMembers = new List<TeamMemberData>();
    public List<TeamMemberData> ApplyMembers
    {
        get { return _applyMembers; }
    }
    public TeamModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
        DefaultMember();
    }
    private TeamState _state = TeamState.Default;
    public TeamState State
    {
        get { return _state; }
    }

    public DBID_TYPE CaptainDbid { get; private set; }

    public void SetState(Byte state)
    {
        SceneEntity sceneEntity = _avatar.SceneEntityObj;
        switch (state)
        {
            case Define.TEAM_STATUS_NONE:
                _state = TeamState.Default;
                _curID = 0;
                targetLimit = new Vector2(1, 100);
                //取消检测跟随
                CloseTeamNav();
                break;
            case Define.TEAM_STATUS_CAPTAIN:
                _state = TeamState.Captain;
                //启动检测跟随
                OpenTeamNav();
                break;
            case Define.TEAM_STATUS_MEMBER:
                _state = TeamState.Member;
                //取消检测跟随
                CloseTeamNav();
                break;
            default:
                //取消检测跟随
                CloseTeamNav();
                break;
        }
        EventManager.Invoke<TeamState>(EventID.EVNT_TEAM_STATE_UPDATE, _state);
    }

    private void DefaultMember()
    {
        _teamMembers.Clear();
        TeamMemberData data = new TeamMemberData();
        data.dbId = _avatar.DatabaseID;
        data.id = _avatar.id;
        data.name = _avatar.Name;
        data.level = _avatar.Level;
        data.feature = _avatar.School;         //特征值，包含性别、职业等属性
        data.status = Define.TEAM_MEMBER_LEADING;	  //状态，领导
        data.SetEntity(data.id);
        _teamMembers.Add(data);
    }

    #region 加入队伍
    //创建队伍
    public void RequestCreateTeam()
    {
        _avatar.cellCall("createTeam");
    }

    public void JoinTeam(JSON_DICT_TYPE teamInfo)
    {
        CaptainDbid = (DBID_TYPE)teamInfo["captainDbid"];

        _teamMembers.Clear();
        List<object> members = (List<object>)teamInfo["members"];
        for (int i = 0; i < members.Count; i++)
        {
            JSON_DICT_TYPE member = (JSON_DICT_TYPE)members[i];
            TeamMemberData data = new TeamMemberData();
            data.dbId = (DBID_TYPE)member["dbid"];
            data.id = (Int32)member["id"];
            data.name = (string)member["name"];
            data.level = (UInt16)member["level"];
            data.feature = (UInt16)member["feature"]; //特征值，包含性别、职业等属性
            data.status = (Byte)member["status"];	  //状态，暂离，跟随，领导，离线
            data.SetEntity(data.id);

            if (data.dbId == CaptainDbid)
            {
                _teamMembers.Insert(0, data);
            }
            else
            {
                _teamMembers.Add(data);
            }
        }
        EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
    }
    #endregion

    #region 暂离队伍/回归队伍
    /// <summary>
    /// 暂离队伍操作
    /// </summary>
    public void SeparateFromTeam()
    {
        if (!IsInTeam())
        {
            Printer.LogError("Not in team.");
        }
        else if (IsCaptain())
        {
            Printer.LogError("Captain can't separate from team.");
        }
        else if (AvatarTeamInfo.status == Define.TEAM_MEMBER_SEPARATING)
        {
            Printer.LogError("Current is separated.");
        }
        else
        {
            _avatar.baseCall("separateFromTeam");
        }
    }

    /// <summary>
    /// 回归队伍操作
    /// </summary>
    public void BackForTeam()
    {
        if (!IsInTeam())
        {
            Printer.LogError("Not in team.");
        }
        else if (IsCaptain())
        {
            Printer.LogError("Captain can't operate this.");
        }
        else if (AvatarTeamInfo.status != Define.TEAM_MEMBER_SEPARATING)
        {
            Printer.LogError("Current is not separated.");
        }
        else
        {
            _avatar.baseCall("backForTeam");
        }
    }
    #endregion

    #region 退出队伍
    //退出队伍
    public void RequestQuitTeam()
    {
        _avatar.cellCall("leaveTeam", new object[] { Define.LEAVE_TEAM_INITIATIVE });
    }
    public void QuitTeam(Byte reason)
    {
        DefaultMember();
        switch (reason)
        {
            case Define.LEAVE_TEAM_KICKOUT:
                break;
            case Define.LEAVE_TEAM_INITIATIVE:
                break;
            case Define.LEAVE_TEAM_DISBAND:
                break;
            default:
                break;
        }
        EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
    }
    #endregion

    #region 邀请入队
    //通过id和名字邀请一个附近的玩家组队
    public void InviteTeammate(Int32 entityId, string entityName)
    {
        _avatar.cellCall("inviteJoinTeamNear", new object[] { entityId, entityName });
    }
    //通过名字邀请一个玩家组队
    public void InviteTeammate(string entityName)
    {
        _avatar.cellCall("inviteJoinTeamRemote", new object[] { entityName });
    }
    //回复是否同意入队邀请
    private void ReplyJoinTeamInvite(bool agree)
    {
        int arg = agree ? 1 : 0;
        _avatar.baseCall("replyJoinTeamInvite", new object[] { arg });
    }
    //接收到的入队邀请
    public void ReceiveJoinTeamInvite(string inviterName)
    {
        TipManager.instance.ShowTwoButtonTip((int)StatusID.TEAM_RECEIVE_JOIN_TEAM_INVITE, inviterName, () => 
        {
            ReplyJoinTeamInvite(true);
            return true;
        }, 
        () => 
        {
            ReplyJoinTeamInvite(false);
            return true;
        });
    }
    #endregion

    #region 申请/接收加入玩家的队伍
    //申请加入玩家的队伍，当客户端能看得到玩家时调用
    public void RequestJoinTeamNear(Int32 entityId, string entityName)
    {
        _avatar.cellCall("requestJoinTeamNear", new object[] { entityId, entityName });
    }
    //申请加入玩家的队伍，当玩家不在视野内时调用
    public void RequestJoinTeamRemote(string entityName)
    {
        _avatar.cellCall("requestJoinTeamRemote", new object[] { entityName });
    }
    //接收到入队申请成员
    public void ReceiveJoinTeamRequest(JSON_DICT_TYPE requestorInfo)
    {
        TeamMemberData data = new TeamMemberData();
        data.dbId = (DBID_TYPE)requestorInfo["dbid"];
        data.id = (Int32)requestorInfo["id"];
        data.name = (string)requestorInfo["name"];
        data.level = (UInt16)requestorInfo["level"];
        data.feature = (UInt16)requestorInfo["feature"]; //特征值，包含性别、职业等属性
        data.status = (Byte)requestorInfo["status"];		//状态，暂离，跟随，领导，离线
        //data.sceneEntity = SceneManager.instance.CurrentScene.GetEntityByID(data.id);

        _applyMembers.Add(data);

		EventManager.Invoke<object>(EventID.EVNT_TEAM_APPLICANT_UPDATE, null);
		_avatar.statusMessage((UInt16)StatusID.TEAM_RECEIVE_JOIN_TEAM_REQUEST, data.name);
    }
    //同意玩家加入队伍申请
    public void AcceptJoinTeamRequest(DBID_TYPE dbid)
    {
        _avatar.cellCall("acceptJoinTeamRequest", new object[] { dbid });
        for (int i = 0; i < _applyMembers.Count; i++)
        {
            TeamMemberData item = _applyMembers[i];
            if(item.dbId == dbid)
            {
                _applyMembers.Remove(item);
                EventManager.Invoke<object>(EventID.EVNT_TEAM_APPLICANT_UPDATE, null);
                break;
            }
        }
    }
    //清除队伍申请列表
    public void ClearTeamRequest()
    {
        _applyMembers.Clear();
        EventManager.Invoke<object>(EventID.EVNT_TEAM_APPLICANT_UPDATE, null);
    }
    #endregion

    #region 队伍成员增加/离开
    public void AddTeammate(JSON_DICT_TYPE teammateInfo)
    {
        TeamMemberData data = new TeamMemberData();
        data.dbId = (DBID_TYPE)teammateInfo["dbid"];
        data.id = (Int32)teammateInfo["id"];
        data.name = (string)teammateInfo["name"];
        data.level = (UInt16)teammateInfo["level"];
        data.feature = (UInt16)teammateInfo["feature"]; //特征值，包含性别、职业等属性
        data.status = (Byte)teammateInfo["status"];		//状态，暂离，跟随，领导，离线
        data.SetEntity(data.id);
        _teamMembers.Add(data);

        EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
    }
    public void TeamTeammateQuit(DBID_TYPE dbId, Byte reason)
    {
        for (int i = 0; i < _teamMembers.Count; i++)
        {
            TeamMemberData data = _teamMembers[i];
            if (data.dbId == dbId)
            {
                _teamMembers.Remove(data);
                break;
            }
        }
        switch (reason)
        {
            case Define.LEAVE_TEAM_KICKOUT:
                break;
            case Define.LEAVE_TEAM_INITIATIVE:
                break;
            case Define.LEAVE_TEAM_DISBAND:
                break;
            default:
                break;
        }
        EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
    }
    #endregion

    //根据队伍ID申请加入队伍，当客户端可以得到队伍ID是可以用这个接口，例如在匹配系统中
    public void RequestJoinTeam(Int32 teamId)
    {
        _avatar.cellCall("requestJoinTeamId", new object[] { teamId });
    }

    //升为队长
    public void PromotedCaptain(DBID_TYPE dbid)
    {
        _avatar.cellCall("transferCaptainTo", dbid);
    }

    public void CaptainChanged(DBID_TYPE cpatainDbid)
    {
        TeamMemberData oldCpatain = null;
        TeamMemberData newCpatain = null;
        for (int i = 0; i < _teamMembers.Count; i++)
        {
            TeamMemberData item = _teamMembers[i];
            if (item.dbId == CaptainDbid)
            {
                oldCpatain = item;
            }
            else if (item.dbId == cpatainDbid)
            {
                newCpatain = item;
            }
        }

        //设置新队长
        CaptainDbid = cpatainDbid;

        //暂时处理方式，又客户端自行改变旧队长的状态，若此时旧队长是其他状态，使用服务器状态
        newCpatain.status = Define.TEAM_MEMBER_LEADING;
        if (oldCpatain != null)
        {
            oldCpatain.status = Define.TEAM_MEMBER_FOLLOWING;
        }

        _teamMembers.Sort(TeammateComparer);

        if (newCpatain.dbId == _avatar.DatabaseID)
        {
            TipManager.instance.ShowTextTip((int)StatusID.TEAM_CAPTAIN_CHANGED, "你");
        }
        else
        {
            TipManager.instance.ShowTextTip((int)StatusID.TEAM_CAPTAIN_CHANGED, newCpatain.name);
        }      
        EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
    }
    //请离队伍
    public void KickTeam(DBID_TYPE teammateDbid)
    {
        _avatar.cellCall("kickoutTeammate", new object[] { teammateDbid });
    }

    #region 队伍成员数据更新
    //更新队员等级
    public void UpdateTeammateLevel(DBID_TYPE dbId, UInt16 level)
    {
        for (int i = 0; i < _teamMembers.Count; i++)
        {
            TeamMemberData data = _teamMembers[i];
            if (data.dbId == dbId)
            {
                data.level = level;
                _teamMembers[i] = data;
                EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
                break;
            }
        }
    }

    private int TeammateComparer(TeamMemberData x, TeamMemberData y)
    {
        return y.status.CompareTo(x.status);
    }

    //更新队员状态
    public void UpdateTeammateState(DBID_TYPE dbId, Byte status)
    {
        for (int i = 0; i < _teamMembers.Count; i++)
        {
            TeamMemberData data = _teamMembers[i];
            if (data.dbId == dbId)
            {
                data.status = status;
                _teamMembers[i] = data;
                _teamMembers.Sort(TeammateComparer);
                EventManager.Invoke<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, null);
                break;
            }
        }
    }
    #endregion

    private Vector2 targetLimit = new Vector2(1, 100);
    public UnityEngine.Vector2 TargetLimit
    {
        get { return targetLimit; }
    }
    public void SetTargetLevel(int start, int end)
    {
        targetLimit = new Vector2(start, end);
        EventManager.Invoke<object>(EventID.EVNT_TEAM_TARGET_UPDATE, null);
    }
    private int _curID = 0;
    public int CurID
    {
        get { return _curID; }
    }
    public void SetCurTarget(int id)
    {
        _curID = id;
    }

    #region 队伍跟随
    private ulong _timerId = TimeUtils.InvalidTimerID;
    private void OpenTeamNav()
    {
        if (_timerId != TimeUtils.InvalidTimerID) return;
        _timerId = TimeUtils.AddLoopRealTimer(0.2f, FollowCaptain);
        TimeUtils.StartTimer(_timerId);
    }

    private void CloseTeamNav()
    {
        TimeUtils.RemoveTimer(_timerId);
        _timerId = TimeUtils.InvalidTimerID;
    }
    /// <summary>
    /// 组队跟随处理
    /// </summary>
    /// <param name="tick"> </param>
    private void FollowCaptain(int tick)
    {
        if (!IsCaptain())
        {
            CloseTeamNav();
            return;
        }
        SceneEntity captain = _avatar.SceneEntityObj;
        // 获得队长身份时客户端sceneEntity有可能还没创建完毕
        if (captain == null || !captain.Navigate.Valid) return;
        Vector2 captainTargetPos = captain.Navigate.GetDestination();
        for (int i = 1; i < TeamMembers.Count; i++)
        {
            TeamMemberData member = TeamMembers[i];
            if (member.status != Define.TEAM_MEMBER_FOLLOWING) continue;
            KBEngine.GameObject memberEntity = member.GetEntity();
            if (memberEntity == null || memberEntity.SceneEntityObj == null) continue; 
            if (!memberEntity.isControlled) continue;
            if (memberEntity.SceneEntityObj.Navigate.GetDestination() == captainTargetPos) continue;
            List<Vector2> positions;
            if (!SetPosition(i, memberEntity.SceneEntityObj, captainTargetPos, out positions)) continue;

            memberEntity.SceneEntityObj.Navigate.Follow(positions, captain.Navigate.GetDistance() + i);
        }
    }
    private bool SetPosition(int index, SceneEntity memberEntity, Vector2 captainTargetPos, out List<Vector2> positions)
    {
        positions = new List<Vector2>(4);
        Vector2 memberPos = memberEntity.GetPosition();
        positions.Add(memberPos);
        for (int i = index - 1; i >= 0; i--)
        {
            TeamMemberData m = TeamMembers[i];
            if (m.status == Define.TEAM_MEMBER_FOLLOWING || m.status == Define.TEAM_MEMBER_LEADING)
            {
                KBEngine.GameObject targetEntity = m.GetEntity();
                if (targetEntity == null || targetEntity.SceneEntityObj == null) continue;
                Vector2 targetPos = targetEntity.SceneEntityObj.GetPosition();
                if (Vector2.Distance(memberPos, targetPos) < 0.2f) return false;
                positions.Add(targetPos);
            }
        }
        positions.Add(captainTargetPos);
        return true;
    }
    #endregion

    /// <summary>
    /// 根据DBID获得成员数据
    /// </summary>
    /// <param name="dbid"></param>
    /// <returns></returns>
    public TeamMemberData GetMemberByDBID(DBID_TYPE dbid)
    {
        return _teamMembers.Find(x => x.dbId == dbid);
    }

    public bool IsCaptain()
    {
        return State == TeamState.Captain;
    }

    public bool IsMember()
    {
        return State == TeamState.Member;
    }

    public bool IsInTeam()
    {
        return State != TeamState.Default;
    }

    public bool IsFree()
    {
        if (IsMember())
        {
            //非跟随状态
            return AvatarTeamInfo.status != Define.TEAM_MEMBER_FOLLOWING;
        }
        return true;
    }

    public TeamMemberData AvatarTeamInfo
    {
        get { return _teamMembers.Find(x => x.dbId == _avatar.DatabaseID); } 
    }
}
