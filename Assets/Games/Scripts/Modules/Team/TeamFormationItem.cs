﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 组队阵法单元
/// </summary>
public class TeamFormationItem : WinItem
{
    private Transform _entityRoot;
    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            Transform child = _entityRoot.Find(resName);
            if (child != null)
            {
                Renderer r = child.GetComponent<Renderer>();
                r.sortingOrder = Const.EntitySortingOrder;
                AssetsManager.instance.Despawn(child.gameObject);
            }
            if (o)
            {
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 3;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }
    private UILabel _name;
    public string Name
    {
        set { _name.text = value; }
    }
    private UISprite _schoolIcon;
    public string SchoolIcon
    {
        set { _schoolIcon.spriteName = value; }
    }
    private UISprite _typeIcon;
    public string TypeIcon
    {
        set { _typeIcon.spriteName = value; }
    }
    private UILabel _level;
    public string Level
    {
        set { _level.text = value; }
    }
    private UISprite _state;
    public string State
    {
        set 
        {
            if (string.IsNullOrEmpty(value))
            {
                _state.enabled = false;
            }
            else
            {
                _state.enabled = true;
                _state.spriteName = value; 
            }
        }
    }
    protected override void OnInit()
    {
        _entityRoot = transform.Find("EntityRoot");
        _name = transform.Find("Name/Text").GetComponent<UILabel>();
        _schoolIcon = transform.Find("School/Icon").GetComponent<UISprite>();
        _typeIcon = transform.Find("Type/Icon").GetComponent<UISprite>();
        _level = transform.Find("Level/Text").GetComponent<UILabel>();
        _state = transform.Find("State").GetComponent<UISprite>();
    }
}
