﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 目标等级设置Page
/// </summary>
public class TeamTargetLimitPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        int num = GameMain.Player.teamModule.CurID;
        if (num == 0)
        {
            SetDefaultLevel();
        }
        else
        {
            SetLevel(num);
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    #region 初始化
    private UICenterOnChild _leftUICenter;
    private UICenterOnChild _rightUICenter;
    private UILabel _limit;
    private UILabel _time;
    private void InitItem()
    {
        _limit = transform.Find("Desc/Limit").GetComponent<UILabel>();
        _time = transform.Find("Desc/Time").GetComponent<UILabel>();

        Transform leftGrid = transform.Find("Left/ScrollView/UIGrid");
        _leftUICenter = leftGrid.GetComponent<UICenterOnChild>();
        _leftUICenter.onCenter = (go) => { _leftUICenter.onCenter = null; SetDefaultLevel(); };
        InitGrid(leftGrid);
        Transform rightGrid = transform.Find("Right/ScrollView/UIGrid");
        _rightUICenter = rightGrid.GetComponent<UICenterOnChild>();
        _rightUICenter.onCenter = (go) => { _rightUICenter.onCenter = null; SetDefaultLevel(); };
        InitGrid(rightGrid);

        GameObject btnSure = transform.Find("BtnSure").gameObject;
        InputManager.instance.AddClickListener(btnSure, (go) =>
        {
            UIManager.instance.CloseWindow(WinID.UITeamTarget);
            int left = 0;
            int.TryParse(_leftUICenter.centeredObject.name, out left);
            int right = 0;
            int.TryParse(_rightUICenter.centeredObject.name, out right);
            if(right > left)
            {
                GameMain.Player.teamModule.SetTargetLevel(left + 1, right + 1);
            }
            else
            {
                GameMain.Player.teamModule.SetTargetLevel(right + 1, left + 1);
            }
        });
    }
    private const int Num = 100;
    private void InitGrid(Transform grid)
    {
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform item = grid.GetChild(i);
            item.name = i.ToString();
            UILabel uiLabel = item.GetComponent<UILabel>();
            uiLabel.text = (i + 1).ToString();
        }
    }
    #endregion

    private void SetLevel(int num)
    {
        int level = GameMain.Player.Level;
        int start = Mathf.Max(0, level - 6);
        int end = Mathf.Min(99, level + 9);
        Transform s = _leftUICenter.transform.Find(start.ToString());
        _leftUICenter.CenterOn(s);
        Transform e = _rightUICenter.transform.Find(end.ToString());
        _rightUICenter.CenterOn(e);
    }
    private void SetDefaultLevel()
    {
        Vector2 limit = GameMain.Player.teamModule.TargetLimit;
        int start = (int)limit.x - 1;
        int end = (int)limit.y - 1;
        Transform s = _leftUICenter.transform.Find(start.ToString());
        _leftUICenter.CenterOn(s);
        Transform e = _rightUICenter.transform.Find(end.ToString());
        _rightUICenter.CenterOn(e);
    }
}
