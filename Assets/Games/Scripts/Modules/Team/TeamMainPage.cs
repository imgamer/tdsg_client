﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 组队界面首页（没有组队状态）
/// </summary>
public class TeamMainPage : WinPage
{
    private UILabel desc;
    protected override void OnInit()
    {
        desc = transform.Find("Desc").GetComponent<UILabel>();
        GameObject btnCreate = transform.Find("BtnCreate").gameObject;
        InputManager.instance.AddClickListener(btnCreate, (go) => 
        {
            GameMain.Player.teamModule.RequestCreateTeam();
        });
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
