﻿using UnityEngine;
using System.Collections;

public class ArenaShopItem : WinItem
{

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _price;
    public string Price
    {
        set
        {
            _price.text = value.ToString();
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _price = transform.Find("Price").GetComponent<UILabel>();
    }
}
