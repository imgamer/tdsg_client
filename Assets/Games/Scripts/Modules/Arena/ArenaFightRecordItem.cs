﻿using UnityEngine;
using System.Collections;

public class ArenaFightRecordItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value.ToString();
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value.ToString();
        }
    }
    private UILabel _factionName;
    public string FactionName
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _factionName.enabled = false;
            }
            else
            {
                _factionName.enabled = true;
                _factionName.text = value.ToString();
            }
        }
    }
    private UILabel _time;
    public string Time
    {
        set
        {
            _time.text = value.ToString();
        }
    }

    private GameObject _playBtn;
    public GameObject PlayBtn
    {
        get
        {
            return _playBtn;
        }
    }

    private UISprite _winOrLose;
    public int WinOrLose
    {
        set
        {
            if(value == 0)
            {
                _winOrLose.spriteName = "[11-15]UI-s2-jn-shibaixiao";
            }
            else if(value == 1)
            {
                _winOrLose.spriteName = "[11-15]UI-s2-jn-shenglixiao";
            }
        }
    }

    private UISprite _upDownArrow;
    private UILabel _upDownNum;
    public void SetUpDown(int state,string num)
    {
       if(state == 0)
       {
           _upDownArrow.gameObject.SetActive(true);
           _upDownNum.gameObject.SetActive(true);
           _upDownArrow.spriteName = "UI-s2-common-hongjian";
           _upDownNum.text = num;
       }
        else if(state == 1)
       {
           _upDownArrow.gameObject.SetActive(true);
           _upDownNum.gameObject.SetActive(true);
           _upDownArrow.spriteName = "UI-s2-common-lvjian";
           _upDownNum.text = num;
       }
       else if(state == 2)
       {
           _upDownArrow.gameObject.SetActive(false);
           _upDownNum.gameObject.SetActive(false);
       }
    }


    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _factionName = transform.Find("FactionName").GetComponent<UILabel>();
        _time = transform.Find("Time").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _playBtn = transform.Find("PlayBtn").gameObject;
        _playBtn.SetActive(false);
        _winOrLose = transform.Find("WinOrLose").GetComponent<UISprite>();
        _upDownArrow = transform.Find("UpDownArrow").GetComponent<UISprite>();
        _upDownNum = transform.Find("UpDownNum").GetComponent<UILabel>();
    }
}
