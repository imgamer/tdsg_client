﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UIArenaRankUp : UIWin
{
    protected override void OnInit()
    {
        InitPage();

        InputManager.instance.AddClickListener(gameObject, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            GameMain.Player.cellCall("confirmFightResult", new object[] { });
        });
    }

    private List<string> _result = new List<string>();
    protected override void OnOpen(params object[] args)
    {
        _result = (List<string>)args[0];
    }

    protected override void OnRefresh()
    {
        Int32 differ = Convert.ToInt32(_result[4]) - Convert.ToInt32(_result[1]);

        _upName.text = _result[0];
        _upRanking.text = _result[1];
        _upArrowRanking.text = Convert.ToString(differ);
        _upRoleLevel.text = _result[2];

        _downName.text = _result[3];    
        _downRanking.text = _result[4];
        _downArrowRanking.text = Convert.ToString(differ);
        _downRoleLevel.text = _result[5]; 
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _upRanking;
    private UILabel _upArrowRanking;
    private UILabel _upName;
    private UILabel _upRoleLevel;

    private UILabel _downRanking;
    private UILabel _downArrowRanking;
    private UILabel _downName;
    private UILabel _downRoleLevel;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _upRanking = center.Find("UpItem/Rank").GetComponent<UILabel>();
        _upName = center.Find("UpItem/Name").GetComponent<UILabel>();
        _upArrowRanking = center.Find("UpArrow/Rank").GetComponent<UILabel>();
        _upRoleLevel = center.Find("UpItem/Level").GetComponent<UILabel>();

        _downRanking = center.Find("DownItem/Rank").GetComponent<UILabel>();
        _downName = center.Find("DownItem/Name").GetComponent<UILabel>();
        _downArrowRanking = center.Find("DownArrow/Rank").GetComponent<UILabel>();
        _downRoleLevel = center.Find("DownItem/Level").GetComponent<UILabel>();
    }
}
