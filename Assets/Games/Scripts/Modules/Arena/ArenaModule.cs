﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using LitJson;

//        rivalDatas是一个字典列表，字典属性如下（对接完后删掉注释）
//        <Properties>
// 			<ranking>
// 				<Type>	UINT32	</Type> 
// 			</ranking>
// 			<name>
// 				<Type>	UNICODE </Type> 
// 			</name>
// 			<level>
// 				<Type>	LEVEL	</Type> 
// 			</level>
// 			<combatPoint>
// 				<Type>	UINT32	</Type> 
// 			</combatPoint>
// 		</Properties>
public class ArenaRivalData
{
    public UInt32 ranking;
    public string name;
    public UInt16 level;
    public UInt32 combatPoint;

    public ArenaRivalData(Dictionary<string, object> rivalData)
    {
        ranking = (UInt32)rivalData["ranking"];
        name = (string)rivalData["name"];
        level = (UInt16)rivalData["level"];
        combatPoint = (UInt32)rivalData["combatPoint"];
    }
}

//          fightResultRecords是一个字典列表，字典属性如下（对接完后删掉注释）
//         <Properties>
// 			<challengerName>
// 				<Type>	UNICODE	</Type> 
// 			</challengerName>
// 			<challengerLevel>
// 				<Type>	LEVEL	</Type> 
// 			</challengerLevel>
// 			<rivalName>
// 				<Type>	UNICODE	</Type> 
// 			</rivalName>
// 			<rivalLevel>
// 				<Type>	LEVEL	</Type> 
// 			</rivalLevel>
// 			<challengeResult>
// 				<Type>	INT8 </Type> 
// 			</challengeResult>
// 			<challengeBeforeRanking>
// 				<Type>	UINT32	</Type> 
// 			</challengeBeforeRanking>
// 			<challengeLaterRanking>
// 				<Type>	UINT32	</Type> 
// 			</challengeLaterRanking>
// 			<challengeTime>
// 				<Type>	INT64	</Type> 
// 			</challengeTime>
// 		</Properties>

public class ArenaFightRecordData
{
    public string challengerName;
    public UInt16 challengerLevel;
    public string rivalName;
    public UInt16 rivalLevel;
    public SByte challengeResult;
    public UInt32 challengeBeforeRanking;
    public UInt32 challengeLaterRanking;
    public Int64 challengeTime;

    public ArenaFightRecordData(Dictionary<string, object> fightResultRecord)
    {
        challengerName = (string)fightResultRecord["challengerName"];
        challengerLevel = (UInt16)fightResultRecord["challengerLevel"];
        rivalName = (string)fightResultRecord["rivalName"];
        rivalLevel = (UInt16)fightResultRecord["rivalLevel"];
        challengeResult = (SByte)fightResultRecord["challengeResult"];
        challengeBeforeRanking = (UInt32)fightResultRecord["challengeBeforeRanking"];
        challengeLaterRanking = (UInt32)fightResultRecord["challengeLaterRanking"];
        challengeTime = (Int64)fightResultRecord["challengeTime"];
    }
}


public class ArenaRangeData
{
    public int rankRangeId;
    public int firstRivalStart;
    public int secondRivalStart;
    public int thirdRivalStart;
    public int rankingReward_9PM;
    public int rankingReward_rising;
    public int rankingReward_realTime;
    public List<int> rankRange;   //范围区间
}

public class ArenaModule
{
    private KBEngine.Avatar _avatar;

    public UInt32 myRanking; //当前自己排名
    public string broadcast;
    public UInt16 remainChallengeCount; //剩余挑战次数

    public int rankingReward9PM { get; private set; } //晚9点奖励
    public int realTimeReward{ get; private set; } //当前排名每分钟增加的奖励
    public UInt32 currentReciveReward { get; private set; } //当前可以领取的奖励

    private List<ArenaRivalData> _arenaRivalData = new List<ArenaRivalData>();
    private List<ArenaFightRecordData> _arenaFightRecordData = new List<ArenaFightRecordData>();

    private Dictionary<string, ArenaRangeData> _arenaRangeData = new Dictionary<string, ArenaRangeData>();

    public ArenaModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;

        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_arena_range_datas");
        _arenaRangeData = JsonMapper.ToObject<Dictionary<string, ArenaRangeData>>(textdata.text);

        broadcast = "暂时没有广播数据";
    }

    public List<ArenaRivalData> RivalData
    {
        get
        {
            return _arenaRivalData;
        }
    }

    public List<ArenaFightRecordData> FightRecordData
    {
        get
        {
            return _arenaFightRecordData;
        }
    }

    public void ReceiveCurrentRankingAndRival(UInt32 currentRanking, List<object> rivalDatas)
    {
        myRanking = currentRanking;
        _arenaRivalData.Clear();
        foreach (object obj in rivalDatas)
        {
            Dictionary<string, object> data = (Dictionary<string, object>)obj;
            ArenaRivalData rivalData = new ArenaRivalData(data);
            _arenaRivalData.Add(rivalData);
        }

        foreach(ArenaRangeData data in _arenaRangeData.Values)
        {
            if(data.rankRange[1] == -1)
            {
                 if( myRanking >= data.rankRange[0])
                 {
                     rankingReward9PM = data.rankingReward_9PM;
                     realTimeReward = data.rankingReward_realTime;
                 }
            }
            else
            {
                if (myRanking >= data.rankRange[0] && myRanking <= data.rankRange[1])
                {
                    rankingReward9PM = data.rankingReward_9PM;
                    realTimeReward = data.rankingReward_realTime;
                }
            }
        }
        EventManager.Invoke<object>(EventID.EVNT_ARENA_RIVAL_UPDATE, null);
    }

    public void ReceiveFightResultRecords(List<object> fightResultRecords)
    {
        _arenaFightRecordData.Clear();
        foreach (object obj in fightResultRecords)
        {
            Dictionary<string, object> data = (Dictionary<string, object>)obj;
            ArenaFightRecordData recordData = new ArenaFightRecordData(data);
            _arenaFightRecordData.Add(recordData);
        }
        EventManager.Invoke<object>(EventID.EVNT_ARENA_RECORD_UPDATE, null);
    }

    public void ReceiveFightRecordDynamic(string challengerName, string rivalName, UInt32 challengerCurrentRanking)
    {
        broadcast = challengerName + "把" + rivalName + "打败，排名上升到第" + challengerCurrentRanking + "名。";
    }

    public void RemainChallengeCount(UInt16 challengeCount)
    {
        remainChallengeCount = challengeCount;

        EventManager.Invoke<object>(EventID.EVNT_ARENA_COUNT_UPDATE, null);
    }

    public void ChallengeCDOverTime(Int32 cdTime)
    {
        EventManager.Invoke<Int32>(EventID.EVNT_ARENA_TIME_UPDATE, cdTime);
    }

    public void BuyChallengeCountNeedIngot(Int32 needCostIngot)
    {
        EventManager.Invoke<Int32>(EventID.EVNT_ARENA_BUYCOUNT_UPDATE, needCostIngot);
    }

    public void CurrentMedalCanToReceive(UInt32 medals)
    {
        currentReciveReward = medals;
        EventManager.Invoke<object>(EventID.EVNT_ARENA_REWARD_UPDATE, null);
    }

    /// <summary>
    /// 请求对手数据
    /// </summary>
    public void QueryCurrentRankingAndRival()
    {
        _avatar.cellCall("queryCurrentRankingAndRival", new object[] { });
    }

    /// <summary>
    /// 请求战斗记录数据
    /// </summary>
    public void QueryFightResultRecords()
    {
        _avatar.cellCall("queryFightResultRecords", new object[] { });
    }

    /// <summary>
    /// 挑战对手
    /// </summary>
    /// <param name="data"></param>
    public void ChallengeRival(ArenaRivalData data)
    {
        _avatar.cellCall("challengeRival", new object[] { data.name, data.ranking });
    }

    /// <summary>
    /// 查询购买此次挑战次数需要花费的元宝数
    /// </summary>
    public void QueryBuyChallengeCountNeedIngot()
    {
        _avatar.cellCall("queryBuyChallengeCountNeedIngot", new object[] {});
    }

    /// <summary>
    /// 查询我当前剩余的挑战次数
    /// </summary>
    public void QueryRemainChallengeCount()
    {
        _avatar.cellCall("queryRemainChallengeCount", new object[] { });
    }

    /// <summary>
    /// 购买次数
    /// </summary>
    public void BuyChallengeCount()
    {
        _avatar.cellCall("buyChallengeCount", new object[] { });
    }

    /// <summary>
    /// 查询离挑战冷却结束时间还有几秒：得出的是秒数, 0代表冷却结束
    /// </summary>
    public void QueryChallengeCDOverTime()
    {
        _avatar.cellCall("queryChallengeCDOverTime", new object[] { });
    }

    /// <summary>
    /// 查询当前可领取的勋章
    /// </summary>
    public void QueryCurrentMedalCanToReceive()
    {
        _avatar.cellCall("queryCurrentMedalCanToReceive", new object[] { });
    }


    /// <summary>
    /// 结束挑战冷却
    /// </summary>
    public void OverChallengeCD()
    {
        _avatar.cellCall("overChallengeCD", new object[] { });
    }

    /// <summary>
    /// 领取当前可领取的勋章
    /// </summary>
    public void ReceiveCurrentMedal()
    {
        _avatar.cellCall("receiveCurrentMedal", new object[] { });
    }

    private List<UInt16> _shopItems = new List<UInt16>();
    public List<UInt16> GetShopItems()
    {
        return _shopItems;
    }

    /// <summary>
    /// 接受竞技场商店物品数据
    /// </summary>
    public void ReceiveArenaShopSellItems(List<object> items)
    {
//        _shopItems = (List<UInt16>)items;
        _shopItems.Clear();
        foreach(object obj in items)
        {
            _shopItems.Add((UInt16)obj);
        }

        EventManager.Invoke<object>(EventID.EVNT_ARENA_SHOP_UPDATE, null);
    }

    /// <summary>
    /// 请求竞技场商店数据
    /// </summary>
    public void RequestArenaShopSellItems()
    {
        _avatar.cellCall("requestArenaShopSellItems", new object[] { });
    }

    /// <summary>
    /// 刷新竞技场商店物品
    /// </summary>
    public void FlushArenaShopItem()
    {
        _avatar.cellCall("flushArenaShopItem", new object[] { });
    }

   /// <summary>
   /// 购买物品
   /// </summary>
   /// <param name="id"></param>
    public void BuyArenaShopSellItem(UInt16 id)
    {
        _avatar.cellCall("buyArenaShopSellItem", new object[] {id });
    }

    /// <summary>
    /// 离开竞技场处理
    /// </summary>
    public void LeaveArena()
    {
        EventManager.AddListener<Scene>(EventID.EVNT_SCENE_READY, OnLeaveArena);
    }
    private void OnLeaveArena(Scene scene)
    {
        EventManager.RemoveListener<Scene>(EventID.EVNT_SCENE_READY, OnLeaveArena);
        if (!scene.IsFightScene())
        {
            UIManager.instance.OpenWindow(null, WinID.UISlave);
        }    
    }
}
