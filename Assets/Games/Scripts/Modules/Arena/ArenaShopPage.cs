﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ArenaShopPage : WinPage
{
    private UInt16 CurrentSelectItemID;
    protected override void OnInit()
    {
        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_MEDAL_UPDATE, UpdateMedal);
        EventManager.AddListener<object>(EventID.EVNT_ARENA_SHOP_UPDATE, UpdateItem);
    }

    protected override void OnOpen(params object[] args)
    {
        GameMain.Player.arenaModule.RequestArenaShopSellItems();
        _haveCoin.text = GameMain.Player.ArenaMedal.ToString();
        ResetInfo();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_MEDAL_UPDATE, UpdateMedal);
        EventManager.RemoveListener<object>(EventID.EVNT_ARENA_SHOP_UPDATE, UpdateItem);
    }

    private UI.Grid _grid;
    private GameObject _tempItem;
    private GameObject _select;

    private UILabel _name;
    private UILabel _desc;
    private UILabel _haveCoin;
    private void InitPage()
    {
        _grid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = transform.Find("Scroll/Grid/Item").gameObject;
        _select = transform.Find("Select").gameObject;
        _select.SetActive(false);

        _name = transform.Find("Prop/Name").GetComponent<UILabel>();
        _desc = transform.Find("Prop/Desc").GetComponent<UILabel>();

        _haveCoin = transform.Find("Have/Value").GetComponent<UILabel>();

        GameObject refushBtn = transform.Find("Refresh/RefreshBtn").gameObject;
        InputManager.instance.AddClickListener(refushBtn, (go) =>
        {
            GameMain.Player.arenaModule.FlushArenaShopItem();
        });

        GameObject buyBtn = transform.Find("BuyBtn").gameObject;
        InputManager.instance.AddClickListener(buyBtn, (go) =>
        {
            if (CurrentSelectItemID == 0) return;
            GameMain.Player.arenaModule.BuyArenaShopSellItem(CurrentSelectItemID);
        });
    }

    private List<ArenaShopItem> _items = new List<ArenaShopItem>();
    private void RefreshItems()
    {
        SelectItem(null);
        List<UInt16> itemsData = GameMain.Player.arenaModule.GetShopItems();
        int dataCount = itemsData.Count;
        int itemCount = _items.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    _items[i].gameObject.SetActive(true);
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _tempItem.SetActive(true);
                    ArenaShopItem item = Instantiate(_tempItem).GetComponent<ArenaShopItem>();
                    item.Init(ParentUI);
                    _tempItem.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    SetItemData(item, itemsData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _items[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
    }

    private void SetItemData(ArenaShopItem item, UInt16  id)
    {
        ArenaShopData data = ArenaShopConfig.SharedInstance.GetArenaShopData(id);
        if (data == null) return;
        item.Name = data.itemName;
        item.Price = data.itemPrice.ToString();
        item.Icon = data.icon;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SelectItem(item.transform);
            SetPropInfo(data);
        });
    }

    private void SetPropInfo(ArenaShopData data)
    {
        _name.text = data.itemName;
        _desc.text = data.description;
        CurrentSelectItemID = Convert.ToUInt16(data.id);
    }

    private void SelectItem(Transform item)
    {
        if(item == null)
        {
            _select.SetActive(false);
            return;
        }
        _select.transform.parent = item;
        _select.transform.localPosition = Vector3.zero;
        _select.transform.localScale = Vector3.one;
        _select.SetActive(true);
    }

    private void ResetInfo()
    {
        _name.text = "";
        _desc.text = "";
        CurrentSelectItemID = 0;
    }

    private void UpdateItem(object o)
    {
        if (!Active) return;
        RefreshItems();
    }

    private void UpdateMedal(object o)
    {
        if (!Active) return;
        _haveCoin.text = GameMain.Player.ArenaMedal.ToString();
    }
}
