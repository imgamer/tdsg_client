﻿using UnityEngine;
using System.Collections;

public class UIArenaLose : UIWin
{
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        InputManager.instance.AddClickListener(gameObject, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            GameMain.Player.cellCall("confirmFightResult", new Object[] { });
        });
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
