﻿using UnityEngine;
using System.Collections;

public class ArenaRivalItem : WinItem
{
    private Transform _entityRoot;
    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            Transform child = _entityRoot.Find(resName);
            if (child != null)
            {
                Renderer r = child.GetComponent<Renderer>();
                r.sortingOrder = Const.EntitySortingOrder;
                AssetsManager.instance.Despawn(child.gameObject);
            }
            if (o)
            {
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private UILabel _rank;
    public string Rank
    {
        set
        {
            _rank.text = value.ToString();
        }
    }

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }
    private UILabel _faction;
    public string FactionName
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _faction.text = string.Empty;
            }
            else
            {
                _faction.text = string.Format("【{0}】", value);
            }
        }
    }

    private UILabel _fighting;
    public string Fighting
    {
        set
        {
            _fighting.text = value.ToString();
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value.ToString();
        }
    }

    private GameObject _infoBtn;
    public GameObject InfoBtn
    {
        get { return _infoBtn; }
    }

    private GameObject _info;
    public GameObject Info
    {
        get { return _info; }
    }

    private GameObject _challengeBtn;
    public GameObject ChallengeBtn
    {
        get { return _challengeBtn; }
    }
    protected override void OnInit()
    {
        _entityRoot = transform.Find("EntityRoot");
        _rank = transform.Find("Other/Rank/Value").GetComponent<UILabel>();
        _name = transform.Find("Name/Text").GetComponent<UILabel>();
        _faction = transform.Find("Other/Info/Faction").GetComponent<UILabel>();
        _fighting = transform.Find("Fighting/Value").GetComponent<UILabel>();
        _level = transform.Find("Other/Level/Value").GetComponent<UILabel>();
        _infoBtn = transform.Find("Other/InfoBtn").gameObject;
        _info = transform.Find("Other/Info").gameObject;
        _info.SetActive(false);
        _challengeBtn = transform.Find("ChallengeBtn").gameObject;
    }
}
