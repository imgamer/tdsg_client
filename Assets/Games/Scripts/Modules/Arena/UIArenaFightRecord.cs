﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UIArenaFightRecord : UIWin
{
    private KBEngine.Avatar _avatar;

    private List<ArenaFightRecordItem> _recordItem = new List<ArenaFightRecordItem>();

    private ArenaFightRecordData _currentSelectData;

    private GameObject _selectBg;
    private Transform _grid;
    private UI.Grid _uiGrid;
    private GameObject _tempItem;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _selectBg = center.Find("SelectBg").gameObject;
        _selectBg.SetActive(false);
        _grid = center.Find("Scroll/Grid");
        _uiGrid = _grid.GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject captureBtn = center.Find("CaptureBtn").gameObject;
        InputManager.instance.AddClickListener(captureBtn, (go) =>
        {
         
            
        });

        EventManager.AddListener<object>(EventID.EVNT_ARENA_RECORD_UPDATE, UpdateItem);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _selectBg.SetActive(false);
        
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ARENA_RECORD_UPDATE, UpdateItem);

    }

    private void RefreshItem()
    {
        List<ArenaFightRecordData> recordData = _avatar.arenaModule.FightRecordData;

        int dataCount = recordData.Count;
        int itemCount = _recordItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_recordItem[i], recordData[i]);
                    _recordItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    Transform item = UITools.AddChild(_grid, _tempItem);
                    _tempItem.SetActive(false);
                    item.name = i.ToString();
                    ArenaFightRecordItem recordItem = item.GetComponent<ArenaFightRecordItem>();
                    recordItem.Init(this);
                    _recordItem.Add(recordItem);
                    SetItemData(recordItem, recordData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_recordItem[i], recordData[i]);
                }
                else
                {
                    _recordItem[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(ArenaFightRecordItem item, ArenaFightRecordData data)
    {    
        item.FactionName = "";
        item.Time = TimeUtils.GetTimeSpanToNow(data.challengeTime, TimeUtils.TimeFormat4v) + "前";
        if(data.challengerName == _avatar.Name)
        {
            item.Name = data.rivalName;
            item.Level = Convert.ToString(data.rivalLevel);
            if(data.challengeResult == 0)
            {
                item.SetUpDown(2, "");
                item.WinOrLose = 0;
            }
            else
            {               
                item.SetUpDown(1, Convert.ToString(data.challengeBeforeRanking - data.challengeLaterRanking));
                item.WinOrLose = 1;
            }
        }
        else
        {
            item.Name = data.challengerName;
            item.Level = Convert.ToString(data.challengerLevel);
            if(data.challengeResult == 0)
            {
                item.SetUpDown(2, "");
                item.WinOrLose = 1;
            }
            else
            {
                item.SetUpDown(0, Convert.ToString(data.challengeBeforeRanking - data.challengeLaterRanking));
                item.WinOrLose = 0;
            }
        }
        
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _currentSelectData = data;
            _selectBg.transform.parent = item.transform;
            _selectBg.transform.localPosition = Vector3.zero;
            _selectBg.SetActive(true);
        });
    }

    private void UpdateItem(object obj)
    {
        if(!Active)
        {
            return;
        }
        RefreshItem();
    }
}
