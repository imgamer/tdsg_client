﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIArenaWin : UIWin
{
    protected override void OnInit()
    {
        InputManager.instance.AddClickListener(gameObject, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            UIManager.instance.OpenWindow(null, WinID.UIArenaRankUp, _result);
        });
    }

    private List<string> _result = new List<string>();
    protected override void OnOpen(params object[] args)
    {
        _result = (List<string>)args[0];
    }

    protected override void OnRefresh()
    {
        
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        
    }
}
