﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ArenaPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private bool _cdState = false; //是否挑战冷却中

    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitRivalItem();
        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_ARENA_RIVAL_UPDATE, UpdateRivalRanking);
        EventManager.AddListener<Int32>(EventID.EVNT_ARENA_TIME_UPDATE, UpdateChallengeTime);
        EventManager.AddListener<object>(EventID.EVNT_ARENA_COUNT_UPDATE, UpdateRemainCount);
        EventManager.AddListener<Int32>(EventID.EVNT_ARENA_BUYCOUNT_UPDATE, UpdateBuyCountNeedIngot);
        EventManager.AddListener<object>(EventID.EVNT_ARENA_REWARD_UPDATE, UpdateCanReceiveReward);
 //       EventManager.AddListener<object>(EventID.EVNT_MEDAL_UPDATE, UpdateArenaMedal);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        InitOwnInfo();
//        _mineMedal.text = Convert.ToString(_avatar.ArenaMedal);
        _avatar.arenaModule.QueryCurrentRankingAndRival();
        _avatar.arenaModule.QueryChallengeCDOverTime();
        _avatar.arenaModule.QueryCurrentMedalCanToReceive();
    }

    protected override void OnClose()
    {
        TimeUtils.RemoveTimer(_challengeTimerId);
        TimeUtils.RemoveTimer(_rewardTimerId);

        _challengeTimerId = TimeUtils.InvalidTimerID;
        _rewardTimerId = TimeUtils.InvalidTimerID;
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_ARENA_RIVAL_UPDATE, UpdateRivalRanking);
        EventManager.RemoveListener<Int32>(EventID.EVNT_ARENA_TIME_UPDATE, UpdateChallengeTime);
        EventManager.RemoveListener<object>(EventID.EVNT_ARENA_COUNT_UPDATE, UpdateRemainCount);
        EventManager.RemoveListener<Int32>(EventID.EVNT_ARENA_BUYCOUNT_UPDATE, UpdateBuyCountNeedIngot);
        EventManager.RemoveListener<object>(EventID.EVNT_ARENA_REWARD_UPDATE, UpdateCanReceiveReward);
 //       EventManager.RemoveListener<object>(EventID.EVNT_MEDAL_UPDATE, UpdateArenaMedal);
    }

    private const int Num = 3;
    private ArenaRivalItem[] _arenaRivalItems = new ArenaRivalItem[Num];

    private void InitRivalItem()
    {
        Transform rival = transform.Find("Rivals");
        for(int i = 0; i < Num; i++)
        {
            ArenaRivalItem item = rival.Find(i.ToString()).GetComponent<ArenaRivalItem>();
            item.Init(ParentUI);
            _arenaRivalItems[i] = item;
        }
    }

    private UILabel _ownName;
    private UILabel _ownLevel;
    private UILabel _ownRank;
    private UILabel _ownFaction;
    private UILabel _ownFighting;
    private UILabel _ownRank2;
    private Transform _entityRoot;

    private UILabel _remainCount;
    private GameObject _buyBtn;
    private GameObject _cdObj;
    private UILabel _cdTime;

    private UILabel _rankReward;
    private UILabel _canReceiveReward;

    private void InitPage()
    {
        Transform own = transform.Find("Own");
        _ownName = own.Find("Name/Text").GetComponent<UILabel>();
        _ownLevel = own.Find("Other/Level/Value").GetComponent<UILabel>();
        _ownRank = own.Find("Other/Rank/Value").GetComponent<UILabel>();
        _ownFaction = own.Find("Faction").GetComponent<UILabel>();
        _ownFighting = own.Find("Fighting/Value").GetComponent<UILabel>();
        _entityRoot = own.Find("EntityRoot");

        Transform challengeCount = transform.Find("ChallengeCount");
        _remainCount = challengeCount.Find("Count/Value").GetComponent<UILabel>();
        _buyBtn = challengeCount.Find("BuyBtn").gameObject;
        InputManager.instance.AddClickListener(_buyBtn, (go) =>
        {
            if (_avatar.arenaModule.remainChallengeCount == 0)
            {
                _avatar.arenaModule.QueryBuyChallengeCountNeedIngot();
            }
        });

        _cdObj = challengeCount.Find("CD").gameObject;
        _cdTime = challengeCount.Find("CD/Value").GetComponent<UILabel>();
        GameObject cleanCDBtn = challengeCount.Find("CD/CleanCDBtn").gameObject;
        InputManager.instance.AddClickListener(cleanCDBtn, (go) =>
        {
            TipManager.instance.ShowTwoButtonTip(Define.ARENA_OVER_CHALLENGE_CD, () =>
            {
                TimeUtils.RemoveTimer(_challengeTimerId);
                _avatar.arenaModule.OverChallengeCD();
                return true;
            }, null);
        });

        Transform other = transform.Find("Other");
        _ownRank2 = other.Find("Rank/Value").GetComponent<UILabel>();
        GameObject helpBtn = other.Find("HelpBtn").gameObject;
        InputManager.instance.AddClickListener(helpBtn, (go) =>
        {

        });

        GameObject refreshBtn = other.Find("RefreshBtn").gameObject;
        InputManager.instance.AddClickListener(refreshBtn, (go) =>
        {
            _avatar.arenaModule.QueryCurrentRankingAndRival();
        });

        GameObject recordBtn = other.Find("RecordBtn").gameObject;
        InputManager.instance.AddClickListener(recordBtn, (go) =>
        {
            _avatar.arenaModule.QueryFightResultRecords();
            UIManager.instance.OpenWindow(null, WinID.UIArenaFightRecord);
        });

        GameObject rankListBtn = other.Find("RankListBtn").gameObject;
        InputManager.instance.AddClickListener(rankListBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIRank, new object[] { 3, 301 });
        });

        Transform reward = transform.Find("Reward");
        _rankReward = reward.Find("Rank/Value").GetComponent<UILabel>();
        _canReceiveReward = reward.Find("CanReceive/Value").GetComponent<UILabel>();
        GameObject receiveBtn = reward.Find("ReceiveBtn").gameObject;
        InputManager.instance.AddClickListener(receiveBtn, (go) =>
        {
            TimeUtils.PauseTimer(_rewardTimerId);
            _avatar.arenaModule.ReceiveCurrentMedal();
        });
    }

    private void InitOwnInfo()
    {
        _ownName.text = Convert.ToString(_avatar.Name);
        _ownLevel.text = String.Format("Lv.{0}", _avatar.Level);
        SetEntity("diancangpai");
    }

    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            Transform child = _entityRoot.Find(resName);
            if (child != null)
            {
                Renderer r = child.GetComponent<Renderer>();
                r.sortingOrder = Const.EntitySortingOrder;
                AssetsManager.instance.Despawn(child.gameObject);
            }
            if (o)
            {
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private void RereshArenaRivalItem()
    {
        List<ArenaRivalData> rivalData = _avatar.arenaModule.RivalData;
        int dataCount = rivalData.Count;
        if(dataCount < Num)
        {
            for(int i = 0; i < Num; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_arenaRivalItems[i], rivalData[i]);
                    _arenaRivalItems[i].gameObject.SetActive(true);
                } 
                else
                {
                    _arenaRivalItems[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            for(int i = 0; i < Num; i++)
            {
                SetItemData(_arenaRivalItems[i], rivalData[i]);
                _arenaRivalItems[i].gameObject.SetActive(true);
            }
        }
    }

    private void SetItemData(ArenaRivalItem item, ArenaRivalData data)
    {
        item.Rank = Convert.ToString(data.ranking);
        item.Name = data.name;
        item.Level = string.Format("Lv.{0}", data.level);
        item.FactionName = "";
        item.Fighting = Convert.ToString(data.combatPoint);
        item.SetEntity("diancangpai");
        InputManager.instance.AddClickListener(item.InfoBtn, (go) =>
        {
            if(item.Info.activeSelf)
            {
                item.Info.SetActive(false);
            }
            else
            {
                item.Info.SetActive(true);
            }
        });
        InputManager.instance.AddClickListener(item.ChallengeBtn, (go) =>
        {
            if(_cdState)
            {
                TipManager.instance.ShowTextTip(Define.ARENA_CHALLENGE_CD);
                return;
            }
            else if(_avatar.arenaModule.remainChallengeCount == 0)
            {
                TipManager.instance.ShowTextTip(Define.ARENA_CHALLENGE_COUNT_NOT_ENOUGH);
                return ;
            }
            TimeUtils.RemoveTimer(_rewardTimerId);
            _avatar.arenaModule.ChallengeRival(data);
        });
    }

    private void UpdateRivalRanking(object value)
    {
        if (!Active)
        {
            return;
        }
        _ownRank.text = Convert.ToString(_avatar.arenaModule.myRanking);
        _ownRank2.text = Convert.ToString(_avatar.arenaModule.myRanking);
//        _nineReward.text = Convert.ToString(_avatar.arenaModule.rankingReward9PM);
        _rankReward.text = Convert.ToString(_avatar.arenaModule.realTimeReward);
        RereshArenaRivalItem();
    }

    private ulong _challengeTimerId;
    private void UpdateChallengeTime(Int32 time)
    {
        if (!Active)
        {
            return;
        }
        if(time == 0)
        {
            _cdObj.SetActive(false);
            _cdState = false;         
        }
        else
        {
            _cdObj.SetActive(true);
            _cdState = true;
            if (_challengeTimerId != TimeUtils.InvalidTimerID)
            {
                TimeUtils.RestartTimer(_challengeTimerId);
            }
            else
            {
                _challengeTimerId = TimeUtils.AddCountdownRealTimerCallStr(time, (str) => 
                {
                    _cdTime.text = str;
                }, () => 
                {
                    _cdObj.SetActive(false);
                    _cdState = false;
                    _challengeTimerId = TimeUtils.InvalidTimerID;
                    _avatar.arenaModule.QueryRemainChallengeCount();
                }, TimeUtils.TimeFormat3v);
                TimeUtils.StartTimer(_challengeTimerId);
            }         
        }

        _avatar.arenaModule.QueryRemainChallengeCount();
    }

    private void UpdateRemainCount(object obj)
    {
        if (!Active)
        {
            return;
        }
        if (_avatar.arenaModule.remainChallengeCount == 0 && !_cdState)
        {
            _buyBtn.SetActive(true);
        }
        else
        {
            _buyBtn.SetActive(false);
        }
        _remainCount.text = Convert.ToString(_avatar.arenaModule.remainChallengeCount);
    }

    private void UpdateBuyCountNeedIngot(Int32  ingotNum)
    {
        if (!Active)
        {
            return;
        }
        TipManager.instance.ShowTwoButtonTip(Define.ARENA_BUY_CHALLENGE_COUNT, Convert.ToString(ingotNum), () =>
            {
                if (ingotNum > _avatar.Fairyjade)
                {
                    TipManager.instance.ShowTextTip(Define.ARENA_INGOT_NOT_ENOUGH);
                }
                else
                {
                    _avatar.arenaModule.BuyChallengeCount();
                }
                return true;
            }, null);
    }

    private ulong _rewardTimerId;
    private void UpdateCanReceiveReward(object value)
    {
        if (!Active)
        {
            return;
        }
        _canReceiveReward.text = Convert.ToString(_avatar.arenaModule.currentReciveReward);
        if(_rewardTimerId !=0)
        {
            TimeUtils.RestartTimer(_rewardTimerId);
        }
        else
        {
            _rewardTimerId = TimeUtils.AddLoopRealTimer(60f, AddMedalByTime);
            TimeUtils.StartTimer(_rewardTimerId);
        }       
    }

   private void AddMedalByTime(int tick)
    {
        Int32 medal = Convert.ToInt32(_canReceiveReward.text) + _avatar.arenaModule.realTimeReward;
        _canReceiveReward.text = Convert.ToString(medal);
    }

//     private void UpdateArenaMedal(object value)
//    {
//        if (!Active)
//        {
//            return;
//        }
//       _mineMedal.text = Convert.ToString(_avatar.ArenaMedal);
//    }
}
 