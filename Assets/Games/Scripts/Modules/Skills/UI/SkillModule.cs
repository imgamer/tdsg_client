﻿using System.Collections;
using System.Collections.Generic;
using Skill;
/// <summary>
/// Avatar技能模块管理
/// </summary>
public class SkillModule 
{
    private KBEngine.Avatar _avatar;
    public SkillModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    public List<SkillInitiative> GetInitiativeSkills()
    {
        List<int> skillIDs = _avatar.skillBox.skills;
        List<SkillInitiative> list = new List<SkillInitiative>();
        for (int i = 0; i < skillIDs.Count; i++)
        {
            SkillBase data = SkillManager.instance.GetSkill(skillIDs[i]);
            SkillInitiative initiativeSkill = data as SkillInitiative;
            if (initiativeSkill == null) continue;
            list.Add(initiativeSkill);
        }
        return list;
    }

}
