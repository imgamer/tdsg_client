﻿using System;
using System.Collections.Generic;
using Skill;

// 战斗数据
public class FightData
{
    public int SkillId { get; protected set; }
    public int UserId { get; protected set; }
    public List<int> TargetsId { get; protected set; }
    public Dictionary<int, List<List<int>>> TargetEffects { get; protected set; }
    public Dictionary<int, List<int>> TargetBuffs { get; protected set; }

    public List<object> Effects { get; private set; }

    public List<int> GetTargetsId()
    {
        return TargetsId;
    }

    public List<List<int>> GetTargetEffect(int targetId)
    {
        List<List<Int32>> targetEffect;
        TargetEffects.TryGetValue(targetId, out targetEffect);
        return targetEffect;
    }

    public List<int> GetTargetBuffIds(int targetId)
    {
        List<Int32> targetBuffIds;
        TargetBuffs.TryGetValue(targetId, out targetBuffIds);
        return targetBuffIds;
    }


    public void SetSkillId(int value)
    {
        SkillId = value;
    }
    public void SetUserId(int value)
    {
        UserId = value;
    }

    public void SetEffects(List<object> value)
    {
        Effects = value;
        TargetsId = new List<int>();
        TargetEffects = new Dictionary<int, List<List<int>>>();
        TargetBuffs = new Dictionary<int, List<int>>();
        for (int i = 0; i < Effects.Count; i++)
        {
            Dictionary<string, object> effectDict = (Dictionary<string, object>)Effects[i];
            int targetId = Convert.ToInt32(effectDict["targetId"]);
            TargetsId.Add(targetId);

            List<object> targetEffectObject = (List<object>)effectDict["targetEffect"];
            SetTargetEffects(targetId, targetEffectObject);

            List<object> targetBuffIdsObject = (List<object>)effectDict["buffIds"];
            SetTargetBuffs(targetId, targetBuffIdsObject);
        }
    }
    private void SetTargetEffects(int targetId, List<object> targetEffectObject)
    {
        List<List<Int32>> targetEffect = new List<List<int>>();
        for (int i = 0; i < targetEffectObject.Count; i++)
        {
            List<object> list = (List<object>)targetEffectObject[i];
            List<Int32> damageList = new List<Int32>();
            for (int j = 0; j < list.Count; j++)
            {
                damageList.Add(Convert.ToInt32(list[j]));
            }
            targetEffect.Add(damageList);
        }
        TargetEffects[targetId] = targetEffect;
    }

    private void SetTargetBuffs(int targetId, List<object> targetBuffIdsObject)
    {
        List<Int32> targetBuffIds = new List<int>();
        for (int i = 0; i < targetBuffIdsObject.Count; i++)
        {
            int id = Convert.ToInt32(targetBuffIdsObject[i]);
            targetBuffIds.Add(id);
        }
        TargetBuffs[targetId] = targetBuffIds;
    }

}

public class FightShowData : FightData
{
    //用于记录实体剩余的受击表现次数
    public Dictionary<int, int> TargetEffectsCount = new Dictionary<int, int>();
    public FightShowData(FightData data)
    {
        SetUserId(data.UserId);
        SetSkillId(data.SkillId);
        CopyTargetIds(data.TargetsId);
        CopyTargetEffects(data.TargetEffects);
        CopyTargetBuffs(data.TargetBuffs);

        for (int i = 0; i < TargetsId.Count; i++)
		{
			int id = TargetsId[i];
            List<List<int>> list;
            if (TargetEffects.TryGetValue(id, out list))
            {
                TargetEffectsCount[id] = list.Count;
            }
            else
            {
                Printer.LogError("表现数据:Id:{0} 效果列表为空！", id);
                TargetEffectsCount[id] = 0;
            }
		}
    }


    private void CopyTargetIds(List<int> targetsIds)
    {
        TargetsId = new List<int>();
        for (int i = 0; i < targetsIds.Count; i++)
        {
            TargetsId.Add(targetsIds[i]);
        }
    }

    private void CopyTargetEffects(Dictionary<int, List<List<int>>> targetEffects)
    {
        TargetEffects = new Dictionary<int, List<List<int>>>();
        foreach (var item in targetEffects)
        {
            int id = item.Key;
            List<List<int>> list = item.Value;
            List<List<int>> newList = new List<List<int>>();
            for (int i = 0; i < list.Count; i++)
            {
                List<int> desc = list[i];
                List<int> newDesc = new List<int>();
                for (int j = 0; j < desc.Count; j++)
                {
                    newDesc.Add(desc[j]);
                }
                newList.Add(newDesc);
            }
            TargetEffects[id] = newList;
        }
    }

    private void CopyTargetBuffs(Dictionary<int, List<int>> targetBuffs)
    {
        TargetBuffs = new Dictionary<int, List<int>>();
        foreach (var item in targetBuffs)
        {
            int id = item.Key;
            List<int> list = item.Value;
            List<int> newList = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                newList.Add(list[i]);
            }
            TargetBuffs[id] = newList;
        }
    }

    public bool IsValid()
    {
        // Check if user exist
        var scene = SceneManager.instance.CurrentScene;
        var se = scene.GetEntityByID(UserId);
        return se != null;
    }
}
