﻿using UnityEngine; 
using System; 
using System.Collections; 
using System.Collections.Generic;
using KBEngine;
using Skill;

public class SkillBox
{
	public List<int> skills = new List<int> ();
    	
	public SkillBox ()
	{

	}

	public void pull ()
	{
		clear ();
			
//		KBEngine.Entity player = KBEngineApp.app.player ();
//		if (player != null)
//			player.cellCall ("requestPull", new object[]{});
	}
		
	public void clear ()
	{
		skills.Clear ();
	}
		
	public void add (int id)
	{
        if (skills.Contains(id))
        {
            Printer.LogWarning("SkillBox::add: " + id + " is exist!");
            return;
        }
		skills.Add (id);
	}
		
	public void remove (Int32 id)
	{
        skills.Remove(id);
	}
		
	public SkillBase get (Int32 id)
	{
        return SkillManager.instance.GetSkill(id);
	}
}
