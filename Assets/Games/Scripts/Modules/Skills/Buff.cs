using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;
using KBEngine;
namespace Skill
{
	public class Buff : SkillBase
	{
		public int buffID;
        public string seqName;
        public double showTime;
        public int attachPercent;
		public int triggerPercent;
		public int totaltime;
        public int looptime;

		
		public override void Init (JsonData data)
		{
			base.Init(data);
            seqName = (string)data["seqName"];
            showTime = (double)data["showTime"];
            attachPercent = (int)data["attachPercent"];
			triggerPercent = (int)data["triggerPercent"];
			totaltime = (int)data ["totaltime"];
            looptime = (int)data["looptime"];

		}
		
		public void SetSourceID( Int32 p_parentskillid, Int32 index )
		{
            buffID = id;
            id = p_parentskillid * 100 + index + 1; // like as:p_parentskillid为1001001，那么buff id应该是100100101
            SkillManager.instance.RegisterSkill(id, this);
		}
		
// 		public virtual void OnTick (SceneEntityObject seo)
// 		{
// 			
// 		}
		
		public virtual bool IsTimeOver (int tickRoundCount)
		{
			//是否到达可持续次数
			if ( tickRoundCount >= totaltime )
			{
				return true;
			}
			return false;
		}
		
		
// 		public virtual void OnAttach (SceneEntityObject seo)
// 		{
// 			//被绑定时
// 			
// 		}
		
		
// 		public virtual void OnDetach (SceneEntityObject seo)
// 		{
// 			//取消绑定时
// 			
// 		}

        public override void Cast(FightData data, Action onFinish = null)
        {
            base.Cast(data, onFinish);

            // play seq 
            FightShowData showData = new FightShowData(data);
            SeqManager.instance.PlaySeq(seqName, showData.UserId, showData,
                CameraManager.instance.CurCamera.transform, (seq) =>
                {
                    CalculateResult(data);
                    if (seq) AssetsManager.instance.Despawn(seq.gameObject);
                    if (onFinish != null) onFinish();
                });
        }

	}
}