﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;
using KBEngine;
using System.Collections.Generic;

namespace Skill
{
	
	public abstract class SkillBase
	{
		#region skill infos
		public Int32 id;
		public string name;
		public string icon;
		public string description;
        public int type;
		#endregion

        public virtual void Init(JsonData skillinfo)
        {
            id = (int)skillinfo ["id"];
            name = (string)skillinfo ["name"];
            icon = (string)skillinfo ["icon"];
            description = (string)skillinfo ["description"];
            type = (int)skillinfo["type"];

        }
        
        /// <summary>
        /// 技能表现开始
        /// </summary>
        public virtual void Cast(FightData data, Action onFinish = null)
        { 
            
        }

        public SkillBase New()
        {
            return null;
        }
        public virtual void CalculateResult(FightData data)
        {
            var scene = SceneManager.instance.CurrentScene;

            var targets = data.GetTargetsId();
            for (int i = 0; i < targets.Count; i++)
            {
                int id = targets[i];
                var target = scene.GetEntityByID(id);
                if (target == null) continue;
                var damageList = data.GetTargetEffect(id);
                if (damageList == null)
                {
                    Printer.LogError("Id:{0} 结算伤害时，找不到伤害列表！", id);
                    continue;
                }
                for (int j = 0; j < damageList.Count; j++)
                {
                    target.CalculateDamage(damageList[j]);
                }
            }
        }

	}	
}