﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.18444
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using KBEngine;
using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{		
	/// <summary>
	/// 主动技能
	/// </summary>
	public class SkillInitiative: SkillBase
	{
		public string clientScript;
        public string seqName;
        public double showTime;
		public int level;
		public int levelMax;
		public int target;
        public int targetContainHeavilyDamage;
		public int targetCount;
		public int cooldown;
		public int consumeMP;
        public int consumeActionValue;
        public int extraPercent;
        public int extraValue;


		private List<Buff> _buffs = new List<Buff>(); //技能附带的buff列表

		public SkillInitiative()
		{
		}
		
		public override void Init(JsonData skillinfo)
		{
			base.Init(skillinfo);
			
			clientScript = (string)skillinfo ["clientScript"];
            seqName = (string)skillinfo["seqName"];
            showTime = (double)skillinfo["showTime"];
			level = (int)skillinfo ["level"];
			levelMax = (int)skillinfo ["levelMax"];
			target = (int)skillinfo ["target"];
            targetContainHeavilyDamage = (int)skillinfo["targetContainHeavilyDamage"];
			targetCount = (int)skillinfo ["targetCount"];
			cooldown = (int)skillinfo ["cooldown"];
			consumeMP = (int)skillinfo ["consumeMP"];
            consumeActionValue = (int)skillinfo["consumeActionValue"];
            extraPercent = (int)skillinfo["extraPercent"];
            extraValue = (int)skillinfo["extraValue"];
			
			/// 初始化buff数据
			JsonData buffDatas = skillinfo ["buffs"];
			for (int i = 0; i < buffDatas.Count; i++)
			{
				int bid = (int)buffDatas [i] ["id"];
				Type typeClass = Type.GetType("Skill.Buff_" + Convert.ToString(bid));
				Buff buffInst = (Buff)Activator.CreateInstance(typeClass);
				buffInst.Init(buffDatas [i]);
				buffInst.SetSourceID(id, i);
				_buffs.Add(buffInst);
			}			

            // Init skill show
//            _show = SkillShow.GetSkillShow(this);
		}

        public virtual bool CanUse(KBEngine.GameObject caster, KBEngine.GameObject selectedTarget)
		{
            if (caster == null || selectedTarget == null)
			{
				return false;
			}

            if (caster.MP < consumeMP)
            {
                return false;
            }

//             List<SceneEntity> skillCanSelectTargets = TargetHandle.SharedInstance.GetCanSelectTargets(caster, id);
// 			if( skillCanSelectTargets == null || skillCanSelectTargets.Count < 1 )
// 			{
// 				return false;
// 			}

			return true;
		}

        public virtual void Use(KBEngine.GameObject caster, KBEngine.GameObject selectedTarget)
		{
			caster.cellCall("useTargetSkill", new object[]{id});
		}

        public override void Cast( FightData data, Action onFinish)
        {
            // get caster
            var caster = SceneManager.instance.CurrentScene.GetEntityByID(data.UserId);
            if(caster == null)
            {
                Printer.LogWarning("caster id " + data.UserId + " not found.");

                if (onFinish != null)
                    onFinish();
                return;
            }
            
            // show skill name 
            HUDManager.instance.SetContent(caster.TopRoot, HUDUnit.HUDType.Skill, name);
            // play seq 
            FightShowData showData = new FightShowData(data);
            SeqManager.instance.PlaySeq(seqName, showData.UserId, showData,
                CameraManager.instance.CurCamera.transform, (seq) =>
                {
                    caster.KBEntity.SetActionValue(caster.KBEntity.ActionValue - (uint)consumeActionValue);
                    CalculateResult(data);
                    if (seq) AssetsManager.instance.Despawn(seq.gameObject);
                    if (onFinish != null) onFinish();
                } );
        }

        public virtual void ReceiveBuff(SceneEntity caster, List<SceneEntity> skillTargets, int buffID)
        {
            throw new NotImplementedException();
        }
        
        public virtual void CalcSkillDamage(SceneEntity caster, List<SceneEntity> skillTargets, List<Int32> targetsEffect)
        {
            
        }

        public void OnSkillCastOver(SceneEntity caster, List<SceneEntity> skillTargets, List<int> targetsEffect)
        {
            throw new NotImplementedException();
        }
    }
}
