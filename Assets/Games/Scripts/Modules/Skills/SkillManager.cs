﻿using System.Reflection;
using UnityEngine;
using System;
using System.Collections.Generic;
using LitJson;
using Skill;

using SkillShowResult = System.Collections.Generic.Dictionary<int, System.Collections.Generic.List<FightData>>;
/// <summary>
/// 注意，技能流程的实现应该找时间重构
/// </summary>
public class SkillManager : MonoSingleton<SkillManager>
{
	private Dictionary<Int32, SkillBase> SkillDatas_ = new Dictionary<Int32, SkillBase>();
	public Dictionary<Int32, SkillBase> SkillDatas{ get { return SkillDatas_; } }
    Queue<SkillShowResult> _fightDataQueue = new Queue<SkillShowResult>();

    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {
    }

	private SkillBase CreateSkillInstance(JsonData skillJsonData)
	{
        SkillBase skillinstance = null;

        try
        {
            string scriptName = (string)skillJsonData["clientScript"];
            Type skillClass = Type.GetType("Skill." + scriptName);
            skillinstance = (SkillBase)Activator.CreateInstance(skillClass);
            skillinstance.Init(skillJsonData);
        }
        catch (Exception e)
        {
            Printer.LogError("SkillManager::CreateSkillInstance:skill({0}) error:{1}.", (int)skillJsonData["id"], e.ToString());
        }

		return skillinstance;
	}

	private void LoadSkillJson(Int32 id)
	{
		var skillinstance = InstantiateSkill(id);
        if (skillinstance == null) return;
		RegisterSkill(id, skillinstance);
	}

    private SkillBase InstantiateSkill(int id)
    {
        SkillBase skillinstance = null;
        JsonData skillJsonData = SkillConfig.SharedInstance.GetJsonData(id);
        if (skillJsonData != null)
        {
            skillinstance = CreateSkillInstance(skillJsonData);
        }
        else// 有可能是个buffid，再一次尝试加载
        {
            Int32 skillid = id / 100;
            skillJsonData = SkillConfig.SharedInstance.GetJsonData(skillid);
            if (skillJsonData != null)
            {
                skillinstance = CreateSkillInstance(skillJsonData);
            }
            else
            {
                Printer.LogError("SkillManger::LoadSkillJson:cant find skill:" + id);
                return skillinstance;
            }
        }
        return skillinstance;
    }

	public void RegisterSkill(Int32 skillid, SkillBase skillinst)
	{
		SkillDatas_.Add(skillid, skillinst);
	}

	public SkillBase GetSkill(Int32 skillID)
	{
		SkillBase skillData;
		SkillDatas_.TryGetValue(skillID, out skillData);
		if (skillData == null)
		{
			LoadSkillJson(skillID);
			SkillDatas_.TryGetValue(skillID, out skillData);
			if (skillData == null)
			{
				return null;
			}
		}
		return skillData;
	}

    private bool _onShow = false;
    SkillShowResult _currentFight;
    public void OnFightStart()
    {
        
        if (_fightDataQueue.Count <= 0)
            return;
        
        if (_onShow)
        {
            return;
        }
        _currentFight = _fightDataQueue.Dequeue();
        if (_currentFight == null)
            return;

        _onShow = true;
        fightShow(_currentFight, Define.SKILL_RESULT_BEFORE_USE, () =>
        {
            fightShow(_currentFight, Define.SKILL_RESULT_IN_USE, () =>
                {
                    _onShow = false;
                    OnFightStart();
                });
        });
    }

    public void PlayShow(int timing, Action onComplete)
    {
        if (_currentFight == null)
        {
            Printer.LogError("当前战斗表现数据为空！timing : {0}", timing);
            if (onComplete != null) onComplete();
            return;
        }

        fightShow(_currentFight, timing, onComplete);
    }

    public void OnLeaveSpace()
    {
        _onShow = false;
        _fightDataQueue.Clear();
    }

    void fightShow(SkillShowResult show, int timing, Action onFinish)
    {
        List<FightData> fightdatas;
        if (show.TryGetValue(timing, out fightdatas))
        {
            FightStart(fightdatas, onFinish);
            show.Remove(timing);
        }
        else
        {
            if (onFinish != null) onFinish();
        }
    }

    void FightStart(List<FightData> fightdatas, Action onFinish)
    {
        if (fightdatas == null)
        {
            Printer.LogError("战斗数据列表为空！");
            if(onFinish != null) onFinish();
            return;
        }       
        
        int count = 0;
        for(int i = 0; i < fightdatas.Count; i++)
        {
            var data = fightdatas[i];
            var skill = GetSkill(data.SkillId);
            if (skill == null)
            {
                Printer.LogError("找不到技能！ID:{0}", data.SkillId);
                count++;
                continue;
            }

            skill.Cast(data, ()=> {
                count++;
                if(count == fightdatas.Count)
                {
                    if(onFinish != null) onFinish();
                }
            });
        }
    }
    
    public void RecieveFightData(List<object> skillShowResultDataList)
    {
        var skillShowResult = new SkillShowResult();
        foreach (object skillShowResultDataDict in skillShowResultDataList)
        {
            Dictionary<string, object> skillTimingShowResult = (Dictionary<string, object>)skillShowResultDataDict;
            Int32 timing = (Int32)skillTimingShowResult["skillShowResultTiming"];
            List<object> skillTimingShowResultAttrList = (List<object>)skillTimingShowResult["skillShowResult"];
            List<FightData> fightDatas = new List<FightData>();
            FightData fightData = null;
            foreach (object skillTimingShowResultAttr in skillTimingShowResultAttrList)
            {
                Dictionary<string, object> skillTimingAttr = (Dictionary<string, object>)skillTimingShowResultAttr;
                fightData = new FightData();
                foreach (string key in skillTimingAttr.Keys)
                {
                    object value = skillTimingAttr[key];
                    
                    MethodInfo setMethod = fightData.GetType().GetMethod("Set" + key,
                                                                         BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.IgnoreCase,
                                                                         null,
                                                                         new Type[] { value.GetType() }, null);
                    if (setMethod != null)
                    {
                        setMethod.Invoke(fightData, new object[] { value });
                    }
                    else
                    {
                        Printer.LogError("Can't find property setter Set" + key);
                    }
                }
                
                fightDatas.Add(fightData);
            }
            skillShowResult[timing] = fightDatas;
        }
        _fightDataQueue.Enqueue(skillShowResult);
    }
}

