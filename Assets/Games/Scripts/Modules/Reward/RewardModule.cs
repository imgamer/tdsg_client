﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 奖励控制类
/// </summary>
public class RewardModule
{
    private KBEngine.Avatar _avatar;
    public RewardModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    private bool _signed = true;
    public bool Signed { get { return _signed; } private set { _signed = value; } }
    public uint SignedDays { get; private set; }
    public uint UnsignedDays { get; private set; }

    #region 签到补签数据更新
    public void UpdateSignState(bool signed)
    {
        Signed = signed;
    }
    public void UpdateSignTimes(uint signedDays)
    {
        SignedDays = signedDays;
        EventManager.Invoke<object>(EventID.EVNT_SIGN_DATA_UPDATE, null);
    }
    public void UpdateRetroactiveTimes(uint unsignedDays)
    {
        UnsignedDays = unsignedDays;
        EventManager.Invoke<object>(EventID.EVNT_SIGN_DATA_UPDATE, null);
    }
    #endregion

    #region 签到补签消息发送
    public void SendToSign()
    {
        if (Signed) return;
        _avatar.baseCall("playerSign");
    }
    public void SendToRetroactive()
    {
        if (UnsignedDays <= 0) return;
        _avatar.baseCall("playerRetroactive");
    }
    #endregion

    private bool _opened = false;
    public void TryOpenUISign()
    {
        if (!Signed && !_opened)
        {
            _opened = true;
            UIManager.instance.OpenWindow(null, WinID.UISign);
        }
    }

}
