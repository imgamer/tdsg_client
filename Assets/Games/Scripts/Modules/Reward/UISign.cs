﻿using UnityEngine;
using System.Collections;
using UI;
using System;
using System.Collections.Generic;
/// <summary>
/// 签到界面
/// </summary>
public class UISign : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_SIGN_DATA_UPDATE, UpdateData);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateData();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_SIGN_DATA_UPDATE, UpdateData);
    }

    private UILabel _signed;
    private UILabel _unsigned;
    private Grid _uiGrid;
    private const int MaxNum = 31;
    private SignItem[] _items = new SignItem[MaxNum];
    private Transform _select;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        _signed = center.Find("Signed/Text").GetComponent<UILabel>();
        _unsigned = center.Find("Unsigned/Num").GetComponent<UILabel>();

        _select = center.Find("ScrollView/Select");
        Transform grid = center.Find("ScrollView/Grid");
        _uiGrid = grid.GetComponent<Grid>();
        UITools.CopyFixNumChild(grid, MaxNum);
        for (int i = 0; i < MaxNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            SignItem item = child.GetComponent<SignItem>();
            item.Init(this);
            _items[i] = item;
        }
    }

    private void UpdateData()
    {
        int signedDays = (int)GameMain.Player.rewardModule.SignedDays;
        int unsignedDays = (int)GameMain.Player.rewardModule.UnsignedDays;
        DateTime dt = TimeUtils.NowDateTime();
        int totalDays = DateTime.DaysInMonth(dt.Year, dt.Month);
        _signed.text = string.Format("{0}月已签到天数：{1}", dt.Month, signedDays);
        int day = dt.Date.Day;
        _unsigned.text = string.Format("{0}", unsignedDays);
        SelectItem(null);
        UpdateItems(totalDays, signedDays, unsignedDays, day);
    }
    private static readonly Dictionary<int, string> MarkDesc = new Dictionary<int, string>() 
    {
        {6, "7天"},
        {8, "9天"},
        {13, "14天"},
        {15, "16天"},
    };
    private void UpdateItems(int totalDays, int signedDays, int unsignedDays, int day)
    {
        bool signToday = GameMain.Player.rewardModule.Signed;
        int maxSignNum = Mathf.Min(signedDays + unsignedDays, day);
        for (int i = 0; i < MaxNum; i++)
        {
            SignItem item = _items[i];
            if(i < totalDays)
            {
                SignReward data = SignRewardConfig.SharedInstance.GetSignReward(i);
                item.Icon = data.itemData.icon;
                item.Num = data.num;
                if (data == null)
                {
                    Printer.LogError("第{0}天奖励数据出错！", i + 1);
                    continue;
                }
                item.gameObject.SetActive(true);
                if (i < signedDays)
                {
                    //已签到
                    item.Done = true;
                    string str;
                    MarkDesc.TryGetValue(i, out str);
                    item.SetMark(0, str);
                    //查看详情
                    ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Adjust, SourcePage.Other, data.itemData, (go) =>
                    {
                        SelectItem(item.transform);
                    });
                }
                else
                {
                    //未签到
                    item.Done = false;
                    if(!signToday && i == signedDays)
                    {
                        //可签到
                        string str;
                        MarkDesc.TryGetValue(i, out str);
                        item.SetMark(0, str);
                        InputManager.instance.AddClickListener(item.gameObject, (go) =>
                        {
                            //发送签到消息
                            GameMain.Player.rewardModule.SendToSign();
                        });
                    }
                    else
                    {
                        if (signToday && i >= signedDays && i < maxSignNum)
                        {
                            //补签
                            item.SetMark(1, "补签");
                            InputManager.instance.AddClickListener(item.gameObject, (go) =>
                            {
                                //发送补签消息
                                GameMain.Player.rewardModule.SendToRetroactive();
                            });
                        }
                        else
                        {
                            string str;
                            MarkDesc.TryGetValue(i, out str);
                            item.SetMark(0, str);
                            //查看详情
                            ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Adjust, SourcePage.Other, data.itemData, (go) => 
                            {
                                SelectItem(item.transform);
                            });
                        }
                    }
                }
            }
            else
            {
                item.gameObject.SetActive(false);
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        _uiGrid.Reposition();
        _uiGrid.GoToItem(signedDays, totalDays);
    }
    private void SelectItem(Transform item)
    {
        if(item == null)
        {
            _select.gameObject.SetActive(false);
        }
        else
        {
            _select.gameObject.SetActive(true);
            _select.parent = item;
            _select.localPosition = Vector3.zero;
            _select.localEulerAngles = Vector3.zero;
            _select.localScale = Vector3.one;
        }
    }

    private void UpdateData(object o)
    {
        if (!Active) return;
        UpdateData();
    }
}
