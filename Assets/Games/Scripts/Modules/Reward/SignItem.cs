﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 签到界面Item
/// </summary>
public class SignItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            ParentUI.SetDynamicSpriteName(_icon, value);
        }
    }
    private const int MarkNum = 2;
    private GameObject _markRoot;
    private GameObject[] _marks = new GameObject[MarkNum];
    private UILabel _markDesc;
    public void SetMark(int index, string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            _markRoot.SetActive(false);
        }
        else
        {
            for (int i = 0; i < MarkNum; i++)
            {
                _marks[i].SetActive(i == index);
            }
            _markRoot.SetActive(true);
            _markDesc.text = value;
        }
    }
    private UILabel _num;
    public int Num
    {
        set
        {
            _num.text = value <= 1 ? string.Empty : string.Format("×{0}", value);
        }
    }
    private GameObject _done;
    public bool Done
    {
        set
        {
            _done.SetActive(value);
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _markRoot = transform.Find("Mark").gameObject;
        _marks[0] = transform.Find("Mark/0").gameObject;
        _marks[1] = transform.Find("Mark/1").gameObject;
        _markDesc = transform.Find("Mark/Text").GetComponent<UILabel>();

        _num = transform.Find("Num").GetComponent<UILabel>();
        _done = transform.Find("Done").gameObject;
        
    }
}
