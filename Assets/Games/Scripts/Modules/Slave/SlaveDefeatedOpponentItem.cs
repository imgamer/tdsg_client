﻿using UnityEngine;
using System.Collections;

public class SlaveDefeatedOpponentItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value.ToString();
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value.ToString();
        }
    }
    private UILabel _factionName;
    public string FactionName
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _factionName.enabled = false;
            }
            else
            {
                _factionName.enabled = true;
                _factionName.text = value.ToString();
            }
        }
    }
    private UILabel _sword;
    public string Sword
    {
        set
        {
            _sword.text = value.ToString();
        }
    }
//     private UISprite _schoolIcon;
//     public string SchoolIcon
//     {
//         set
//         {
//             _schoolIcon.spriteName = value.ToString();
//         }
//     }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _factionName = transform.Find("FactionName").GetComponent<UILabel>();
        _sword = transform.Find("Sword").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
      //  _schoolIcon = transform.Find("SchoolIcon").GetComponent<UISprite>();
    }
}
