﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SlaveInfo
{
    public UInt64 dbid;
    public string name;
    public UInt16 level;
    public UInt32 utype;
    public UInt32 combatPoint; 
}
public class SlaveModule 
{
    private KBEngine.Avatar _avatar;
    public SByte identity { get; private set; } //玩家身份

    private List<SlaveInfo> _slaveInfoList = new List<SlaveInfo>(); //奴隶列表数据（当前身份为领主或者奴隶时）

    public List<Int64> SlavesToGetRidOfRemainTime = new List<Int64>(); //奴隶距离摆脱剩余的时间

    private List<SlaveInfo> _handLosers = new List<SlaveInfo>();

    public List<string> LogRecord = new List<string>();// 日志

    public Int32 RemainSqueezeCount { get; private set; }  // 剩余压榨次数

    public UInt16 RemainCaptureCount { get; private set; } //剩余挑战次数

//    public Int64 CaptureCDOverTime { get; private set; } //俘虏冷却时间   

//    public Int64 SqueezeCDOverTime { get; private set; } //压榨冷却时间

//     public Int32 RemainPlacateCount { get; private set; } //剩余安抚次数

    public SlaveModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    public List<SlaveInfo> SlaveInfoList
    {
        get
        {
            return new List<SlaveInfo>(_slaveInfoList);
        }
    }

    public List<SlaveInfo> HandLosers
    {
        get
        {
            return new List<SlaveInfo>(_handLosers);
        }
    }

    // avatarIdentity 玩家身份
    // slavesInfo 奴隶信息：如果玩家是自由民，则为空； 如果是领主，则是所拥有的奴隶列表；如果是奴隶，则是同一个领主的奴隶伙伴
    // lordInfo 领主信息：如果玩家是自由民或领主，则为空；如果玩家是奴隶，则是所属领主信息，有数值的话取第一个就好了（因为当前领主只有一个）
    // slavesToGetRidOfRemainTime 奴隶距离摆脱剩余的时间（单位为s）
    public void OnIdentityAndRelationAvatar(sbyte avatarIdentity, List<object> slavesInfo, List<object> lordInfo, List<object> slavesToGetRidOfRemainTime)
    {
        identity = avatarIdentity;
        _slaveInfoList.Clear();
        SlavesToGetRidOfRemainTime.Clear();
        if(avatarIdentity == Define.LORD_IDENTITY_TYPE_SLAVE)
        {
            _slaveInfoList.AddRange(InitSlavesInfo(lordInfo));
            _slaveInfoList.AddRange(InitSlavesInfo(slavesInfo));
        }
        else if(avatarIdentity == Define.LORD_IDENTITY_TYPE_LORD)
        {
            _slaveInfoList = InitSlavesInfo(slavesInfo);
            foreach(object obj in slavesToGetRidOfRemainTime)
            {
                SlavesToGetRidOfRemainTime.Add((Convert.ToInt64(obj)));
            }
            QueryRemainSqueezeCount();
            QueryRemainPlacateCount();         
            
            QueryRemainCaptureCount();
            QuerySqueezeCDOverTime();
        }
        else if(avatarIdentity == Define.LORD_IDENTITY_TYPE_FREE)
        {
            QueryRemainCaptureCount();
        }
        EventManager.Invoke<object>(EventID.EVNT_SLAVE_LIST_UPDATE, null);
    }

    private List<SlaveInfo> InitSlavesInfo(List<object> info)
    {
        List<SlaveInfo> slave = new List<SlaveInfo>();
        foreach (object obj in info)
        {
            Dictionary<string, object> data = (Dictionary<string, object>)obj;
            SlaveInfo slaveInfo = new SlaveInfo();
            slaveInfo.dbid = Convert.ToUInt64(data["dbid"]);
            slaveInfo.name = Convert.ToString(data["name"]);
            slaveInfo.utype = Convert.ToUInt32(data["utype"]);
            slaveInfo.level = Convert.ToUInt16(data["level"]);
            slaveInfo.combatPoint = Convert.ToUInt32(data["combatPoint"]);
            slave.Add(slaveInfo);
        }
        return slave;
    }

    public void OnHandLosers(List<object> handLosers)
    {
        _handLosers = InitSlavesInfo(handLosers);
        EventManager.Invoke<object>(EventID.EVNT_SLAVE_HANDLOSERS_UPDATE, null);
    }

    public void OnRemainCaptureCount(UInt16 remainCaptureCount)
    {
        RemainCaptureCount = remainCaptureCount;
        EventManager.Invoke<UInt16>(EventID.EVNT_SLAVE_CAPTURE_COUNT_UPDATE, remainCaptureCount);
    }

    public void OnCaptureCDOverTime(Int32 cdTime)
    {
       // CaptureCDOverTime = cdTime;
        EventManager.Invoke<Int32>(EventID.EVNT_SLAVE_CAPTURE_TIME_UPDATE, cdTime);
    }

    public void OnSqueezeCDOverTime(Int32 cdTime)
    {
        //SqueezeCDOverTime = cdTime;
        EventManager.Invoke<Int32>(EventID.EVNT_SLAVE_SQUEEZE_TIME_UPDATE, cdTime);
    }

    public void OnRemainSqueezeCount(UInt16 remainSqueezeCount)
    {
       RemainSqueezeCount = remainSqueezeCount;
        EventManager.Invoke<UInt16>(EventID.EVNT_SLAVE_SQUEEZE_COUNT_UPDATE, remainSqueezeCount);
    }

    public void OnRemainPlacateCount(UInt16 remainPlacateCount)
    {
        EventManager.Invoke<UInt16>(EventID.EVNT_SLAVE_PLACATE_COUNT_UPDATE, remainPlacateCount);
    }

    /// <summary>
    /// 查询当前的身份和相关角色（领主或奴隶列表）
    /// </summary>
    public void QueryIdentityAndRelationAvatars()
    {
        _avatar.cellCall("queryIdentityAndRelationAvatars", new object[] { });
    }

    /// <summary>
    /// 查询俘获日志
    /// </summary>
    public void QueryLordLogRecords()
    {
        _avatar.cellCall("queryLordLogRecords", new object[] { });
    }

    /// <summary>
    /// 请求手下败将列表
    /// </summary>
    public void QueryHandLosers()
    {
        _avatar.cellCall("queryHandLosers", new object[] { });
    }

    /// <summary>
    /// 查询当前俘获次数
    /// </summary>
    public void QueryRemainCaptureCount()
    {
        _avatar.cellCall("queryRemainCaptureCount", new object[] { });
    }

    /// <summary>
    /// 查询离俘获冷却结束时间还有几秒：得出的是秒数, 0代表冷却结束
    /// </summary>
    public void QueryCaptureCDOverTime()
    {
        _avatar.cellCall("queryCaptureCDOverTime", new object[] { });
    }

    /// <summary>
    /// 查询剩余的安抚次数
    /// </summary>
    public void QueryRemainPlacateCount()
    {
        _avatar.cellCall("queryRemainPlacateCount", new object[] { });
    }

    /// <summary>
    /// 查询剩余的压榨次数
    /// </summary>
    public void QueryRemainSqueezeCount()
    {
        _avatar.cellCall("queryRemainSqueezeCount", new object[] { });
    }

    /// <summary>
    /// 查询离压榨冷却结束时间还有几秒
    /// </summary>
    public void QuerySqueezeCDOverTime()
    {
        _avatar.cellCall("querySqueezeCDOverTime", new object[] { });
    }

    /// <summary>
    /// 购买俘获次数
    /// </summary>
    public void BuyCaptureCount()
    {
        _avatar.cellCall("buyCaptureCount", new object[] { });
    }

    /// <summary>
    /// 安抚奴隶
    /// </summary>
    public void PlacateSlave(string name)
    {
        _avatar.cellCall("placateSlave", new object[] { name });
    }

    /// <summary>
    /// 讨好主人
    /// </summary>
    /// <param name="type"></param>
    public void PleaseLord(string captorName, SByte type)
    {
        _avatar.cellCall("pleaseLord", new object[] { captorName, type });
    }

    /// <summary>
    /// 奴隶反抗
    /// </summary>
    public void GainstLord(string name)
    {
        _avatar.cellCall("gainstLord", new object[] { name });
    }

    /// <summary>
    /// 购买压榨次数
    /// </summary>
    public void BuySqueezeCount()
    {
        _avatar.cellCall("buySqueezeCount", new object[] { });
    }

    /// <summary>
    /// 压榨奴隶
    /// </summary>
    public void SqueezeSlave(string name)
    {
        _avatar.cellCall("squeezeSlave", new object[] {name});
    }

    /// <summary>
    /// 俘获奴隶
    /// </summary>
    /// <param name="name"></param>
    public void CaptureSlave(string name)
    {
        _avatar.cellCall("captureSlave", new object[] {name});
    }

    /// <summary>
    /// 清除俘获时间CD
    /// </summary>
    public void OverCaptureCD()
    {
        _avatar.cellCall("overCaptureCD", new object[] { });
    }

    public void OnLordLogRecords(List<object> lordLogRecords)
    {
        LogRecord.Clear();
        foreach(object obj in lordLogRecords)
        {
            Dictionary<string, object> data = (Dictionary<string, object>)obj;
            Int32 logID = Convert.ToInt32(data["logId"]);
            string logContent = Convert.ToString(data["logContent"]);
            LogRecord.Add(AddLogRecord(logID, logContent));
        }
    }

    private string AddLogRecord(Int32 logType, string logContent)
    {
        string log = LordInteractLogConfig.SharedInstance.GetLogData(logType);
        string[] content = logContent.Split(',');
        string time = content[0];
        string logTime = TimeUtils.GetDateTime(Convert.ToDouble(time), TimeUtils.DateFormat4);
        string logStr = string.Empty;
        if(content.Length == 2)
        {
            string logName = "[ff0033]" + content[1] + "[-]";
            logStr = String.Format(log, logTime, logName);
        }
        else
        {
            string logName1 ="[ff0033]" + content[1] + "[-]";
            string logName2 = "[ff0033]" + content[2] + "[-]";
            logStr = String.Format(log, logTime, logName1, logName2);
        }
        return "[666666]" + logStr + "[-]";
    }

    /// <summary>
    /// 离开奴隶战斗处理
    /// </summary>
    public void LeaveSlave()
    {
        EventManager.AddListener<Scene>(EventID.EVNT_SCENE_READY, OnLeaveSlave);
    }
    private void OnLeaveSlave(Scene scene)
    {
        EventManager.RemoveListener<Scene>(EventID.EVNT_SCENE_READY, OnLeaveSlave);
        if (!scene.IsFightScene())
        {
            UIManager.instance.OpenWindow(null, WinID.UISlave);
        }       
    }
}
