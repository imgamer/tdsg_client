﻿using UnityEngine;
using System.Collections;

public class SlaveItem : WinItem
{
    private Transform _entityRoot;
    public void SetEntity(string resName)
    {
        if (string.IsNullOrEmpty(resName)) return;
        BaseSpawnData data = new InstantiateData(resName, resName, PoolName.Model, PoolType.Loop, (o) =>
        {
            Transform child = _entityRoot.Find(resName);
            if (child != null)
            {
                Renderer r = child.GetComponent<Renderer>();
                r.sortingOrder = Const.EntitySortingOrder;
                AssetsManager.instance.Despawn(child.gameObject);
            }
            if (o)
            {
                o.transform.parent = _entityRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 3;

                AnimationControl control = o.GetComponent<AnimationControl>();
                control.Init();
                control.SetIdle(-90);
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private GameObject _info;
    public GameObject Info
    {
        get
        {
            return _info;
        }
    }
    private UISprite _identity;
    public string IdentitySprite
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _identity.enabled = false;
            }
            else
            {
                _identity.enabled = true;
                _identity.spriteName = value;
            }
        }
    }

    private UILabel _sword;
    public  string Sword
    {
        set
        {
            if(string.IsNullOrEmpty(value))
            {
                _sword.gameObject.SetActive(false);
            }
            else
            {
                _sword.gameObject.SetActive(true);
                _sword.text = value.ToString();
            }
        }
    }

    private UILabel _factionName;
    public string FactionName
    {
        set
        {
            if(string.IsNullOrEmpty(value))
            {
                _factionName.enabled = false;
            }
            else
            {
                _factionName.enabled = true;
                _factionName.text = value.ToString();
            }
        }
    }
    private GameObject _rewardBtn;
    public GameObject RewardBtn
    {
        get
        {
            return _rewardBtn;
        }
    }
    private UILabel _time;
    public string TimeValue
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _time.gameObject.SetActive(false);
            }
            else
            {
                _time.gameObject.SetActive(true);
                _time.text = value.ToString();
            }
        }
    }

    private UISprite _factionBg;
    public string FactionBg
    {
        set
        {
            _factionBg.spriteName = value.ToString();
        }
    }

    private UISprite _schoolIcon;
    public string SchoolIcon
    {
        set
        {
            _schoolIcon.spriteName = value.ToString();
        }
    }

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }
    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value.ToString();
        }
    }

    private GameObject _reward;
    public GameObject Reward
    {
        get
        {
            return _reward;
        }
    }
    private GameObject _closeBtn;
    public GameObject CloseBtn
    {
        get
        {
            return _closeBtn;
        }
    }
    private GameObject _goldBtn;
    public GameObject GoldBtn
    {
        get
        {
            return _goldBtn;
        }
    }
    private GameObject _ingotBtn;
    public GameObject IngotBtn
    {
        get
        {
            return _ingotBtn;
        }
    }
    protected override void OnInit()
    {
        _entityRoot = transform.Find("EntityRoot");
        _info = transform.Find("Info").gameObject;
        _identity = transform.Find("Info/Identity").GetComponent<UISprite>();
        _sword = transform.Find("Info/Sword").GetComponent<UILabel>();
        _factionName = transform.Find("Info/FactionName").GetComponent<UILabel>();
        _rewardBtn = transform.Find("Info/RewardBtn").gameObject;
        _time = transform.Find("Info/Time").GetComponent<UILabel>();
        _factionBg = transform.Find("FactionBg").GetComponent<UISprite>();
        _schoolIcon = transform.Find("SchoolIcon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();

        _reward = transform.Find("Reward").gameObject;
        _closeBtn = transform.Find("Reward/CloseBtn").gameObject;
        _goldBtn = transform.Find("Reward/GoldBtn").gameObject;
        _ingotBtn = transform.Find("Reward/IngotBtn").gameObject;
    }
}
