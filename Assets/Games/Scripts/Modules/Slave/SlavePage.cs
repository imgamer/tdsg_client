﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SlavePage : WinPage
{
    private KBEngine.Avatar _avatar;

    private List<SlaveItem> _slaveItem = new List<SlaveItem>();

    private SByte _avatarIdentity;

    private SlaveInfo _currentSelectSlave;
    protected override void OnInit()
    {
        InitPage();

        _avatar = GameMain.Player;
        EventManager.AddListener<object>(EventID.EVNT_SLAVE_LIST_UPDATE, UpdateSlaveList);
        EventManager.AddListener<UInt16>(EventID.EVNT_SLAVE_CAPTURE_COUNT_UPDATE, UpdateCaptureCount);
        EventManager.AddListener<UInt16>(EventID.EVNT_SLAVE_SQUEEZE_COUNT_UPDATE, UpdateSqueezeCount);
        EventManager.AddListener<UInt16>(EventID.EVNT_SLAVE_PLACATE_COUNT_UPDATE, UpdatePlacateCount);
        EventManager.AddListener<Int32>(EventID.EVNT_SLAVE_SQUEEZE_TIME_UPDATE, UpdateSqueezeCDTime);
    }

    protected override void OnOpen(params object[] args)
    {
        
    }

    protected override void OnRefresh()
    {
        _avatar.slaveModule.QueryIdentityAndRelationAvatars();
        _avatar.slaveModule.QueryLordLogRecords();
        _currentSelectSlave = null;
        _selectBg.SetActive(false);
    }

    protected override void OnClose()
    {
        TimeUtils.RemoveTimer(_squeezeTimerId);
        _squeezeTimerId = 0;
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_SLAVE_LIST_UPDATE, UpdateSlaveList);
        EventManager.RemoveListener<UInt16>(EventID.EVNT_SLAVE_CAPTURE_COUNT_UPDATE, UpdateCaptureCount);
        EventManager.RemoveListener<UInt16>(EventID.EVNT_SLAVE_SQUEEZE_COUNT_UPDATE, UpdateSqueezeCount);
        EventManager.RemoveListener<UInt16>(EventID.EVNT_SLAVE_PLACATE_COUNT_UPDATE, UpdatePlacateCount);
        EventManager.RemoveListener<Int32>(EventID.EVNT_SLAVE_SQUEEZE_TIME_UPDATE, UpdateSqueezeCDTime);

    }

    private UILabel _identity;
    private Transform _grid;
    private UI.Grid _uiGrid;
    private GameObject _selectBg;
    private GameObject _tempSlaveItem;

    private GameObject _lord;
    private UILabel _lordCount;
    private UILabel _lordCaptureSurplus;
    private GameObject _placateBtn;
    private UILabel _placateBtnLabel;
    private UILabel _lordPlacateSurplus;
    private GameObject _squeezeBtn;
    private UILabel _squeezeBtnLabel;
    private UILabel _lordSqueezeSurplus;
    private GameObject _addCaptureBtn;
    private GameObject _addSqueezeBtn;


    private GameObject _slave;
    private UILabel _slaveCount;

    private GameObject _freedom;
    private UILabel _freedomSurplus;


    private void InitPage()
    {
        _identity = transform.Find("Identity").GetComponent<UILabel>();
        _grid = transform.Find("Scroll/Grid");
        _uiGrid = _grid.GetComponent<UI.Grid>();
        _tempSlaveItem = transform.Find("Scroll/Grid/Item").gameObject;
        _tempSlaveItem.SetActive(false);
        _selectBg = transform.Find("Select").gameObject;

        GameObject recordBtn = transform.Find("RecordBtn").gameObject;
        InputManager.instance.AddClickListener(recordBtn, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UISlaveRecord);
        });

        //Lord
        _lord = transform.Find("Lord").gameObject;
        _lordCount = transform.Find("Lord/Count").GetComponent<UILabel>();
        _lordCaptureSurplus = transform.Find("Lord/CaptureSurplus").GetComponent<UILabel>();

        GameObject catchBtn = transform.Find("Lord/CatchBtn").gameObject;
        InputManager.instance.AddClickListener(catchBtn, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UISlaveDefeatedOpponent);
        });

        _squeezeBtn = transform.Find("Lord/SqueezeBtn").gameObject;
        _squeezeBtnLabel = transform.Find("Lord/SqueezeBtn/Label").GetComponent<UILabel>();
        _lordSqueezeSurplus = transform.Find("Lord/SqueezeSurplus").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_squeezeBtn, (go) =>
        {
            if(_currentSelectSlave == null)
            {
                return;
            }
            _avatar.slaveModule.SqueezeSlave(_currentSelectSlave.name);
        });

        _placateBtn = transform.Find("Lord/PlacateBtn").gameObject;
        _placateBtnLabel = transform.Find("Lord/PlacateBtn/Label").GetComponent<UILabel>();
        _lordPlacateSurplus = transform.Find("Lord/PlacateSurplus").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_placateBtn, (go) =>
        {
            if (_currentSelectSlave == null)
            {
                return;
            }
            _avatar.slaveModule.PlacateSlave(_currentSelectSlave.name);
        });

        _addCaptureBtn = transform.Find("Lord/AddCaptureBtn").gameObject;
        InputManager.instance.AddClickListener(_addCaptureBtn, (go) =>
        {
            TipManager.instance.ShowTwoButtonTip(Define.STATUSID_LORD_BUY_CAPTURE_COUNT_NEED_INGOT,Convert.ToString(Define.LORD_BUY_CAPTURE_COUNT_NEED_INGOT), () =>
            {
                _avatar.slaveModule.BuySqueezeCount();
                return true;
            }, null);
        });

        _addSqueezeBtn = transform.Find("Lord/AddSqueezeBtn").gameObject;
        InputManager.instance.AddClickListener(_addSqueezeBtn, (go) =>
        {
            TipManager.instance.ShowTwoButtonTip(Define.STATUSID_LORD_BUY_SQUEEZE_COUNT_NEED_INGOT, Convert.ToString(Define.LORD_BUY_SQUEEZE_COUNT_NEED_INGOT), () =>
            {
                _avatar.slaveModule.BuySqueezeCount();
                return true;
            }, null);
        });


        //Slave
        _slave = transform.Find("Slave").gameObject;
        _slaveCount = transform.Find("Slave/Count").GetComponent<UILabel>();
        GameObject revoltBtn = transform.Find("Slave/RevoltBtn").gameObject;
        InputManager.instance.AddClickListener(revoltBtn, (go) =>
        {
            _avatar.slaveModule.GainstLord(_avatar.slaveModule.SlaveInfoList[0].name);
        });
        GameObject helpBtn = transform.Find("Slave/HelpBtn").gameObject;
        InputManager.instance.AddClickListener(helpBtn, (go) =>
        {

        });

        //Freedom
        _freedom = transform.Find("Freedom").gameObject;
        _freedomSurplus = transform.Find("Freedom/Surplus").GetComponent<UILabel>();
        GameObject fBleedBtn = transform.Find("Freedom/BleedBtn").gameObject;
        InputManager.instance.AddClickListener(fBleedBtn, (go) =>
        {
            UIManager.instance.OpenWindow(ParentUI, WinID.UISlaveDefeatedOpponent);
        });
    }

    private void RefreshItem()
    {
        List<SlaveInfo> slaveData = _avatar.slaveModule.SlaveInfoList;
        _avatarIdentity = _avatar.slaveModule.identity;
        if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_FREE)
        {
            _identity.text = "自由民";
            _freedom.SetActive(true);
            _lord.SetActive(false);
            _slave.SetActive(false);
        }
        else if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_LORD)
        {
            _identity.text = "领主";
            _freedom.SetActive(false);
            _lord.SetActive(true);
            _slave.SetActive(false);
            _lordCount.text = String.Format("{0}/4", Convert.ToString(slaveData.Count));
        }
        else if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_SLAVE)
        {
            _identity.text = "奴隶";
            _freedom.SetActive(false);
            _lord.SetActive(false);
            _slave.SetActive(true);
            _slaveCount.text = String.Format("{0}/4", Convert.ToString(slaveData.Count - 1));
        }

        int dataCount = slaveData.Count;
        int itemCount = _slaveItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_slaveItem[i], slaveData[i], i);
                    _slaveItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempSlaveItem.SetActive(true);
                    Transform item = UITools.AddChild(_grid, _tempSlaveItem);
                    _tempSlaveItem.SetActive(false);
                    item.name = i.ToString();
                    SlaveItem slaveItem = item.GetComponent<SlaveItem>();
                    slaveItem.Init(ParentUI);
                    _slaveItem.Add(slaveItem);
                    SetItemData(slaveItem, slaveData[i], i);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_slaveItem[i], slaveData[i], i);
                }
                else
                {
                    _slaveItem[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(SlaveItem item, SlaveInfo data, int index)
    {
        if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_LORD)
        {
            item.Sword = "";
            item.TimeValue = TimeUtils.GetTimeSpan(_avatar.slaveModule.SlavesToGetRidOfRemainTime[index], TimeUtils.TimeFormat4v);
            item.IdentitySprite = "";
            item.RewardBtn.SetActive(false);
        }
        else if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_SLAVE)
        {
            item.TimeValue = "";
            item.Sword = Convert.ToString(data.combatPoint);
            if (index == 0)
            {
                item.IdentitySprite = "[11-15]UI-s2-jn-linzhu";
                InputManager.instance.AddClickListener(item.RewardBtn, (go) =>
                {
                    item.Reward.SetActive(true);
                    item.Info.SetActive(false);
                });

                InputManager.instance.AddClickListener(item.CloseBtn, (go) =>
                {
                    item.Reward.SetActive(false);
                    item.Info.SetActive(true);
                });
                InputManager.instance.AddClickListener(item.GoldBtn, (go) =>
                {
                    //金币打赏
                    _avatar.slaveModule.PleaseLord(data.name, Define.LORD_PLEASE_LORD_TYPE_SEND_FLOWER);
                });
                InputManager.instance.AddClickListener(item.IngotBtn, (go) =>
                {
                    //元宝打赏
                    _avatar.slaveModule.PleaseLord(data.name, Define.LORD_PLEASE_LORD_TYPE_POUR_TEA);
                });
            }
            else
            {
                item.IdentitySprite = "";
                item.RewardBtn.SetActive(false);
            }
        }
        item.SetEntity("diancangpai");
        item.Reward.SetActive(false);
        item.Name = data.name;
        item.Level = Convert.ToString(data.level);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SetItemSelect(item);
            _currentSelectSlave = data;
        });
    }

    private void SetItemSelect(SlaveItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void UpdateSlaveList(object o)
    {
        if (!Active)
        {
            return;
        }
        RefreshItem();
    }

    private void UpdateCaptureCount(UInt16 value)
    {
        if (!Active)
        {
            return;
        }
        if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_LORD)
        {
            if (value == 0)
            {
                _addCaptureBtn.SetActive(true);
            }
            else
            {
                _addCaptureBtn.SetActive(false);
            }
            _lordCaptureSurplus.text = Convert.ToString(value);
        }
        else if (_avatarIdentity == Define.LORD_IDENTITY_TYPE_FREE)
        {
            _freedomSurplus.text = Convert.ToString(value);
        }
    }

    private ulong _squeezeTimerId = TimeUtils.InvalidTimerID;
    private bool _squeezeCDState = false;
    private void UpdateSqueezeCDTime(Int32 value)
    {
        if (!Active)
        {
            return;
        }

        if (value == 0)
        {
            _squeezeBtnLabel.text = "压 榨";
            if (_avatar.slaveModule.RemainSqueezeCount == 0)
            {
                UITools.SetButtonState(_squeezeBtn, false);
            }
            else
            {
                UITools.SetButtonState(_squeezeBtn, true);
            }
            _squeezeCDState = false;
        }
        else
        {
            if (_squeezeTimerId != TimeUtils.InvalidTimerID)
            {
                TimeUtils.RestartTimer(_squeezeTimerId);
            }
            else
            {
                UITools.SetButtonState(_squeezeBtn, false);
                _squeezeCDState = true;
                _squeezeTimerId = TimeUtils.AddCountdownRealTimerCallStr(value, (str) =>
                {                   
                    _squeezeBtnLabel.text = string.Format("冷却 {0}", str);                    
                }, () =>
                {                  
                    _squeezeBtnLabel.text = "压 榨";
                    _squeezeTimerId = TimeUtils.InvalidTimerID;
                    if(_avatar.slaveModule.RemainSqueezeCount == 0)
                    {
                        UITools.SetButtonState(_squeezeBtn, false);
                    }
                    else
                    {
                        UITools.SetButtonState(_squeezeBtn, true);
                    }
                    _squeezeCDState = false;
                }, TimeUtils.TimeFormat4v);
                TimeUtils.StartTimer(_squeezeTimerId);
            }
        }
    }

    private void UpdateSqueezeCount(UInt16 value)
    {
        if (!Active)
        {
            return;
        }
        if (value == 0)
        {
            _addSqueezeBtn.SetActive(true);
        }
        else
        {
            _addSqueezeBtn.SetActive(false);
            if(!_squeezeCDState)
            {
                UITools.SetButtonState(_squeezeBtn, true); //防止购买次数后无冷却时间时按钮没取消置灰
            }          
        }
        _lordSqueezeSurplus.text = Convert.ToString(value);
    }

    private void UpdatePlacateCount(UInt16 value)
    {
        if (!Active)
        {
            return;
        }
        if(value == 0)
        {
            UITools.SetButtonState(_placateBtn, false);
        }
        else
        {
            UITools.SetButtonState(_placateBtn, true);
        }
        _lordPlacateSurplus.text = Convert.ToString(value);
    }
}
