﻿using UnityEngine;
using System.Collections;

public class UISlave : UIWin
{
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1)
        {
            uiTab.SwitchTo(uiTab.CurTabIndex);
        }
        else
        {
            int index = (int)args[0];
            uiTab.SwitchTo(index);
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        if(curContent != null)
        {
            curContent.Close();
        }     
    }

    protected override void OnUnInit()
    {

    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(this);
        };
    }
}
