﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UISlaveDefeatedOpponent : UIWin
{
    private KBEngine.Avatar _avatar;

    private List<SlaveDefeatedOpponentItem> _dOpItem = new List<SlaveDefeatedOpponentItem>();

    private SlaveInfo _currentSelectSlave;


    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        InitPage();
        EventManager.AddListener<object>(EventID.EVNT_SLAVE_HANDLOSERS_UPDATE, UpdateHandLosers);
        EventManager.AddListener<Int32>(EventID.EVNT_SLAVE_CAPTURE_TIME_UPDATE, UpdateCaptureCDTime);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _currentSelectSlave = null;
        _selectBg.SetActive(false);
        _avatar.slaveModule.QueryHandLosers();       

        _avatar.slaveModule.QueryCaptureCDOverTime();
        
    }

    protected override void OnClose()
    {
        TimeUtils.RemoveTimer(_captureTimerId);
        _captureTimerId = 0; 
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_SLAVE_HANDLOSERS_UPDATE, UpdateHandLosers);
        EventManager.RemoveListener<Int32>(EventID.EVNT_SLAVE_CAPTURE_TIME_UPDATE, UpdateCaptureCDTime);

    }

    private GameObject _selectBg;
    private Transform _grid;
    private UI.Grid _uiGrid;
    private GameObject _tempItem;

    private GameObject _captureBtn;
    private UILabel _captureBtnLabel;

    GameObject _cdBtn; 
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _selectBg = center.Find("SelectBg").gameObject;
        _selectBg.SetActive(false);
        _grid = center.Find("Scroll/Grid");
        _uiGrid = _grid.GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _captureBtn = center.Find("CaptureBtn").gameObject;
        _captureBtnLabel = center.Find("CaptureBtn/Label").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_captureBtn, (go) =>
        {
            if (_currentSelectSlave == null)
            {
                return;
            }
            if(_avatar.slaveModule.SlaveInfoList.Count == Define.LORD_SLAVE_COUNT_LIMIT)
            {
                TipManager.instance.ShowTextTip(Define.STATUSID_LORD_SLAVE_COUNT_LIMIT);
                return;
            }
            _avatar.slaveModule.CaptureSlave(_currentSelectSlave.name);
        });

        _cdBtn = center.Find("CDBtn").gameObject;
        InputManager.instance.AddClickListener(_cdBtn, (go) =>
        {
            TipManager.instance.ShowTwoButtonTip(Define.STATUSID_LORD_OVER_CAPTURE_TIME_CD_NEED_INGOT, Convert.ToString(Define.LORD_OVER_CAPTURE_TIME_CD_NEED_INGOT), () =>
            {
                TimeUtils.RemoveTimer(_captureTimerId);
                _avatar.slaveModule.OverCaptureCD();
                return true;
            }, null);      
        });
    }

    private void RefreshItem()
    {
        List<SlaveInfo> handLosers = _avatar.slaveModule.HandLosers;

        int dataCount = handLosers.Count;
        int itemCount = _dOpItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_dOpItem[i], handLosers[i]);
                    _dOpItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    Transform item = UITools.AddChild(_grid, _tempItem);
                    _tempItem.SetActive(false);
                    item.name = i.ToString();
                    SlaveDefeatedOpponentItem dOpItem = item.GetComponent<SlaveDefeatedOpponentItem>();
                    dOpItem.Init(this);
                    _dOpItem.Add(dOpItem);
                    SetItemData(dOpItem, handLosers[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_dOpItem[i], handLosers[i]);
                }
                else
                {
                    _dOpItem[i].gameObject.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
    }

    private void SetItemData(SlaveDefeatedOpponentItem item, SlaveInfo info)
    {
        item.Name = info.name;
        item.Level = Convert.ToString(info.level);
        item.Sword = Convert.ToString(info.combatPoint);
        item.FactionName = "";
        //item.Icon = HerosConfig.SharedInstance.GetHerosData(info.utype).icon;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _currentSelectSlave = info;
            _selectBg.transform.parent = item.transform;
            _selectBg.transform.localPosition = Vector3.zero;
            _selectBg.SetActive(true);
        });
    }

    public void UpdateHandLosers(object o)
    {
        if (!Active)
        {
            return;
        }
        RefreshItem();
    }

    private ulong _captureTimerId;
    private void UpdateCaptureCDTime(Int32 value)
    {
        if (!Active)
        {
            return;
        }
        if (value == 0)
        {
            _cdBtn.SetActive(false);
            _captureBtnLabel.text = "俘 虏";
            if (_avatar.slaveModule.RemainCaptureCount == 0)
            {
                UITools.SetButtonState(_captureBtn, false);
            }
            else
            {
                UITools.SetButtonState(_captureBtn, true);
            }           
        }
        else
        {
            _cdBtn.SetActive(true);
            UITools.SetButtonState(_captureBtn, false);
            if (_captureTimerId != TimerManager.InvalidTimerID)
            {
                TimeUtils.RestartTimer(_captureTimerId);
            }
            else
            {
                _captureTimerId = TimeUtils.AddCountdownRealTimerCallStr(value, (str) =>
                {
                    _captureBtnLabel.text = string.Format("冷却 {0}", str);                   
                }, () =>
                {
                    _cdBtn.SetActive(false);
                    _captureBtnLabel.text = "俘 虏";
                    _captureTimerId = TimerManager.InvalidTimerID;
                    if (_avatar.slaveModule.RemainCaptureCount == 0)
                    {
                        UITools.SetButtonState(_captureBtn, false);                       
                    }
                    else
                    {
                        UITools.SetButtonState(_captureBtn, true);
                    }
                }, TimeUtils.TimeFormat4v);
                TimeUtils.StartTimer(_captureTimerId);               
            }
        }
    }
}


