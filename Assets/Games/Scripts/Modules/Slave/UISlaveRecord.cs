﻿using UnityEngine;
using System.Collections;

public class UISlaveRecord : UIWin
{
    private UITextList _logList;
    protected override void OnInit()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _logList = center.Find("Scroll/Log").GetComponent<UITextList>();
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject closeBtn2 = center.Find("CloseBtn2").gameObject;
        InputManager.instance.AddClickListener(closeBtn2, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _logList.Clear();
        KBEngine.Avatar avatar = GameMain.Player;
        foreach (string str in avatar.slaveModule.LogRecord)
        {
            _logList.Add(str);
        }
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
