﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 任务界面菜单Item
/// </summary>
public class RankMenuItem : WinItem
{
    private UILabel name;
    public string Name
    {
        set
        {
            if (name == null) return;
            name.text = value;
        }
    }
    private UISprite mark;
    private UISprite selectBG;
    private GameObject select;
    private bool openState = false;
    public bool OpenState
    {
        get { return openState; }
    }
    public void SwitchState()
    {
        openState = !openState;
        uiGrid.gameObject.SetActive(openState);
        select.SetActive(openState);
        if (openState)
        {
            mark.spriteName = "UI-s2-common-jiantouxiangshang";
            uiGrid.Reposition();
        }
        else
        {
            mark.spriteName = "UI-s2-common-jiantouxiangxia";
        }
    }
    public void SetState(bool state)
    {
        openState = state;
        uiGrid.gameObject.SetActive(openState);
        select.SetActive(openState);
        if (openState)
        {
            mark.spriteName = "UI-s2-common-jiantouxiangshang";
            uiGrid.Reposition();
        }
        else
        {
            mark.spriteName = "UI-s2-common-jiantouxiangxia";
        }
    }
    private UIGrid uiGrid;
    public UIGrid Grid
    {
        get { return uiGrid; }
    }
    private GameObject template;
    public GameObject Template
    {
        get { return template; }
    }
    private List<RankCategoryItem> items = new List<RankCategoryItem>();
    public List<RankCategoryItem> Items
    {
        get { return items; }
    }
    public int DataNum
    {
        set { selectBG.height = 85 + (int)uiGrid.cellHeight * value; }
    }
    public int category;

    protected override void OnInit()
    {
        name = transform.Find("Name").GetComponent<UILabel>();
        mark = transform.Find("Mark").GetComponent<UISprite>();
        select = transform.Find("Select").gameObject;
        select.SetActive(false);
        selectBG = select.transform.Find("BG").GetComponent<UISprite>();
        uiGrid = transform.Find("UIGrid").GetComponent<UIGrid>();
        template = uiGrid.transform.Find("Item").gameObject;
        template.SetActive(false);
    }

}
