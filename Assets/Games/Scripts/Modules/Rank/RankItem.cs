﻿using UnityEngine;
using System.Collections;

public class RankItem : WinItem
{
    private UILabel _rank;
    private UISprite _rankSprite;
    public string Rank
    {
        set
        {
            if(value == "1")
            {
                _rankSprite.gameObject.SetActive(true);
                _rank.gameObject.SetActive(false);
                _rankSprite.spriteName = "[11-15]UI-s2-ph-diyi";
            }
            else if (value == "2")
            {
                _rankSprite.gameObject.SetActive(true);
                _rank.gameObject.SetActive(false);
                _rankSprite.spriteName = "[11-15]UI-s2-ph-dier";
            }
            else if (value == "3")
            {
                _rankSprite.gameObject.SetActive(true);
                _rank.gameObject.SetActive(false);
                _rankSprite.spriteName = "[11-15]UI-s2-ph-disan";
            }
            else
            {
                _rankSprite.gameObject.SetActive(false);
                _rank.gameObject.SetActive(true);
                _rank.text = value.ToString();
            }
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }
    private UILabel _unknown;
    public string Unknown
    {
        set
        {
            _unknown.text = value.ToString();
        }
    }
    private UILabel _score;
    public string Score
    {
        set
        {
            _score.text = value.ToString();
        }
    }
    protected override void OnInit()
    {
        _rank = transform.Find("Rank").GetComponent<UILabel>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _unknown = transform.Find("Level").GetComponent<UILabel>();
        _score = transform.Find("Score").GetComponent<UILabel>();
        _rankSprite = transform.Find("RankSprite").GetComponent<UISprite>();
    }

}
