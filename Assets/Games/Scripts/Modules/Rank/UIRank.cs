﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UIRank : UIWin
{
    private KBEngine.Avatar _avatar;

    private bool beInited = false;
    private List<RankMenuItem> _rankMenuItems = new List<RankMenuItem>();
    private List<RankItem> _rankItems = new List<RankItem>();

    private int index = 0; //20条数据一页，当前所在页数
    private int maxIndex = 0; //目前该榜单最大页数
    private UInt16 currentType; //当前榜的Type
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        if (_avatar == null)
        {
            Printer.LogWarning("GameMgr.SharedInstance.m_player==null");
        }

        InitInterface();

        EventManager.AddListener<UInt16>(EventID.EVNT_RANK_UPDATE, RefreshRankItem);
    }

    protected override void OnOpen(params object[] args)
    {
        if (!beInited)
        {
            InitUIItem();
            beInited = true;
        }    

        if (args == null || args.Length == 0)
        {
            OpenAssignSelect(1, 101);
        }
        else
        {
            int category = (int)args[0];
            int type = (int)args[1];
            OpenAssignSelect(category, type);
        }
    }

    protected override void OnRefresh()
    {
        
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UI.Table _uiTable;
    private RankMenuItem _templateItem;
    private GameObject _selectItem;

    private UILabel _header1;
    private UILabel _header2;
    private UILabel _header3;
    private UILabel _header4;

    private UILabel _mineRank;
    private UISprite _mineRankSprite;
    private UILabel _mineName;
    private UILabel _mineUnKnow;
    private UILabel _mineScore;

    private UIScrollView _rankScrollView;
    private GameObject _tempRankItem;
   // private Transform _rankGrid;
    private UI.Grid _rankUiGrid;

    private void InitInterface()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        Transform table = center.Find("Menu/UITable");
        _uiTable = table.GetComponent<UI.Table>();
        _templateItem = _uiTable.transform.Find("Item").GetComponent<RankMenuItem>();
        _templateItem.Init(this);
        _templateItem.gameObject.SetActive(false);
        _selectItem = center.Find("Menu/SelectItem").gameObject;
        _selectItem.SetActive(false);

        _header1 = center.Find("Text/Header1").GetComponent<UILabel>();
        _header2 = center.Find("Text/Header2").GetComponent<UILabel>();
        _header3 = center.Find("Text/Header3").GetComponent<UILabel>();
        _header4 = center.Find("Text/Header4").GetComponent<UILabel>();

        _mineRank = center.Find("Mine/Rank").GetComponent<UILabel>();
        _mineRankSprite = center.Find("Mine/RankSprite").GetComponent<UISprite>();
        _mineName = center.Find("Mine/Name").GetComponent<UILabel>();
        _mineUnKnow = center.Find("Mine/Level").GetComponent<UILabel>();
        _mineScore = center.Find("Mine/Score").GetComponent<UILabel>();
        GameObject infoBtn = center.Find("Mine/InfoBtn").gameObject;
        InputManager.instance.AddClickListener(infoBtn, (go) =>
        {

        });

        _rankScrollView = center.Find("Scroll").GetComponent<UIScrollView>();
        _rankScrollView.onDragFinished = OnDragFinished;
        //_rankGrid = center.Find("Scroll/Grid");
        _rankUiGrid = center.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempRankItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempRankItem.SetActive(false);
    }

    private void InitUIItem()
    {
        List<RankUIData> UIdata = _avatar.rankModule.GetRankUIData;
        UIdata = SortRankUIList(UIdata);
        int dataNum = UIdata.Count;
        int itemNum = _rankMenuItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    RankMenuItem item = _rankMenuItems[i];

                    item.gameObject.SetActive(true);
                    SetUIItemData(item, UIdata[i]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    _templateItem.gameObject.SetActive(true);
                    RankMenuItem item = Instantiate(_templateItem) as RankMenuItem;
                    _templateItem.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiTable.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(this);
                    _rankMenuItems.Add(item);

                    SetUIItemData(item, UIdata[i]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                RankMenuItem item = _rankMenuItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetUIItemData(item, UIdata[i]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiTable.Reposition();
    }

    private void SetUIItemData(RankMenuItem menuItem, RankUIData data)
    {
        menuItem.category = data.category;
        menuItem.Name = data.categoryName;
        List<RankCategory> categoryData = new List<RankCategory>(data.subCategory.Values);
        categoryData = SortCategoryList(categoryData);
        UIGrid uiGrid = menuItem.Grid;
        RankCategoryItem template = menuItem.Template.GetComponent<RankCategoryItem>();
        List<RankCategoryItem> items = menuItem.Items;
        int dataNum = categoryData.Count;
        menuItem.DataNum = dataNum;
        int itemNum = items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    RankCategoryItem item = items[i];
                    item.gameObject.SetActive(true);
                    SetCategoryItemData(item, categoryData[i]);
                }
                else
                {
                    template.gameObject.SetActive(true);
                    RankCategoryItem item = Instantiate(template) as RankCategoryItem;
                    item.Init(this);
                    template.gameObject.SetActive(false);
                    item.transform.parent = uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    items.Add(item);
                    SetCategoryItemData(item, categoryData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                RankCategoryItem item = items[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetCategoryItemData(item, categoryData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        NGUITools.SetActive(uiGrid.gameObject, menuItem.OpenState);
        if (menuItem.OpenState) uiGrid.Reposition();
    }

    private void SetCategoryItemData(RankCategoryItem item, RankCategory data)
    {
        item.Name = data.categoryName;
        item.CategoryType = data.category;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            if(data.category == currentType)
            {
                return;
            }
            ResetRankItemPosition();
            CategoryItemSelect(item, data);
        });
    }

    private void CategoryItemSelect(RankCategoryItem item, RankCategory data)
    {
        _selectItem.SetActive(true);
        _selectItem.transform.parent = item.transform;
        _selectItem.transform.localPosition = Vector3.zero;
        _selectItem.transform.localEulerAngles = Vector3.zero;
        _selectItem.transform.localScale = Vector3.one;

        _header1.text = data.attribute[0];
        _header2.text = data.attribute[1];
        _header3.text = data.attribute[2];
        _header4.text = data.attribute[3];

        index = 0;
        currentType = Convert.ToUInt16(data.category);
        _avatar.rankModule.QueryRankDatas(currentType, Define.RANK_BEGIN_INDEX, Define.RANK_DATAS_COUNT);
    }

    private void SwitchState(RankMenuItem menuItem)
    {
        menuItem.SwitchState();
        _uiTable.Reposition();
    }

    /// <summary>
    /// 优先级排序
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    private List<RankUIData> SortRankUIList(List<RankUIData> list)
    {
        list.Sort(delegate(RankUIData x, RankUIData y)
        {
            return y.categoryPriority.CompareTo(x.categoryPriority);
        });
        return list;
    }

    private List<RankCategory> SortCategoryList(List<RankCategory> list)
    {
        list.Sort(delegate(RankCategory x, RankCategory y)
        {
            return y.categoryPriority.CompareTo(x.categoryPriority);
        });
        return list;
    }

   /// <summary>
   /// 打开指定的选项卡
   /// </summary>
    private void OpenAssignSelect(int category, int type)
    {
        RankCategory data = _avatar.rankModule.GetAssignData(category, type);
        foreach (var item in _rankMenuItems)
        {
            if (item.category == category)
            {
                item.SetState(true);
                foreach (RankCategoryItem typeItem in item.Items)
                {
                    if (typeItem.CategoryType == type)
                    {
                        CategoryItemSelect(typeItem, data);
                    }
                }             
            }
            else
            {
                item.SetState(false);
            }
        }
        _uiTable.Reposition();
    }


    private void RefreshRankItem(UInt16 rankType)
    {
        List<List<string>> rankData = _avatar.rankModule.GetRankData(rankType);
        InitMineInfo(rankType);        
        InitMaxIndex(rankData.Count);

        List<List<string>> rankIndexData = GetIndexRankData(rankData, index);
        int dataNum = rankIndexData.Count;
        if(dataNum == 0)
        {
            return;
        }
        int itemNum = _rankItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    RankItem item = _rankItems[i];
                    item.gameObject.SetActive(true);
                    SetRankItemData(rankType, item, rankIndexData[i]);
                }
                else
                {
                    _tempRankItem.gameObject.SetActive(true);
                    GameObject item = Instantiate(_tempRankItem) as GameObject;
                    RankItem rankItem = item.GetComponent<RankItem>();
                    rankItem.Init(this);
                    _tempRankItem.gameObject.SetActive(false);
                    item.transform.parent = _rankUiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _rankItems.Add(rankItem);
                    SetRankItemData(rankType, rankItem, rankIndexData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                RankItem item = _rankItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetRankItemData(rankType, item, rankIndexData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _rankUiGrid.Reposition();
    }

    private void SetRankItemData(UInt16 rankType, RankItem item, List<string> data)
    {
        if(rankType == 101 || rankType == 102 || rankType == 103)
        {
            item.Unknown= SchoolString(Convert.ToUInt32(data[2]));        
        }
        else
        {
            item.Unknown = data[2];
        }
        item.Rank = data[0];
        item.Name = data[1];              
        item.Score = data[3];
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            if(rankType ==104)
            {
                UIManager.instance.OpenWindow(null, WinID.UIPetDetail);
            }
            else
            {
                UIManager.instance.OpenWindow(null, WinID.UIRoleInfo);
            }
        });
    }

    /// <summary>
    /// 拖动到底消息回调
    /// </summary>
    private void OnDragFinished()
    {
        Bounds b = _rankScrollView.bounds;
        Vector3 constraint = _rankScrollView.panel.CalculateConstrainOffset(b.min, b.max);
        if (constraint.y < 0)
        {
            if (constraint.sqrMagnitude > 5f)
            {
                index++;
                if (index > maxIndex || currentType == 0)
                {
                    index = maxIndex;
                    return;
                }
                RefreshRankItem(currentType);
                ResetRankItemPosition();
            }
        }
        else
        {
            if (constraint.sqrMagnitude > 5f)
            {
                index--;
                if (index < 0 || currentType == 0)
                {
                    index = 0;
                    return;
                }
                RefreshRankItem(currentType);
                ResetRankItemPosition();
            }
        }
    }

    private void ResetRankItemPosition()
    {
        _rankUiGrid.GoToItem(0, _rankItems.Count);
    }

    /// <summary>
    /// 获取当前页的数据
    /// </summary>
    /// <param name="data"></param>
    /// <param name="index">0.1.2.3.4 共五页</param>
    /// <returns></returns>
    private List<List<string>> GetIndexRankData(List<List<string>> data, int index)
    {
        int count = data.Count;
        List<List<string>> rankList = new List<List<string>>();
        for (int i = index * 20; i < (index + 1) * 20; i++)
        {
            if (i >= count)
            {
                break;
            }
            rankList.Add(data[i]);
        }
        return rankList;
    }

    /// <summary>
    ///当前数据量最大页数 
    /// </summary>
    /// <param name="dataNum"></param>
    private void InitMaxIndex(int dataNum)
    {
        if(dataNum >= 0 && dataNum <= 20)
        {
            maxIndex = 0;
        }
        else if(dataNum > 20 && dataNum <= 40)
        {
            maxIndex = 1;
        }
        else if (dataNum > 40 && dataNum <= 60)
        {
            maxIndex = 2;
        }
        else if (dataNum > 60 && dataNum <= 80)
        {
            maxIndex = 3;
        }
        else if (dataNum > 80 && dataNum <= 100)
        {
            maxIndex = 4;
        }
    }

    private void InitMineInfo(UInt16 rankType)
    {
        switch(rankType)
        {
            case 101:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = SchoolString(Convert.ToUInt32(_avatar.School));
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 102:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = SchoolString(Convert.ToUInt32(_avatar.School)); 
                _mineScore.text = Convert.ToString(_avatar.Level);
                break;
            case 103:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = SchoolString(Convert.ToUInt32(_avatar.School)); 
                _mineScore.text = Convert.ToString(_avatar.Score);
                break;
            case 104:
                InitPetValue();
                SetMineRank(rankType, _petMaxName);
                _mineName.text = _petMaxName;
                _mineUnKnow.text = _avatar.Name;
                _mineScore.text = Convert.ToString(_petMaxScore);
                break;
            case 201:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 202:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 203:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 204:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 205:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 206:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = Convert.ToString(_avatar.Score + _petMaxTwoScore);
                break;
            case 301:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = "0";
                break;
            case 302:
                _mineName.text = _avatar.Name;
                SetMineRank(rankType, _avatar.Name);
                _mineUnKnow.text = "暂无";
                _mineScore.text = "0";
                break;
            default:
                break;
        }
    }

    private void SetMineRank(UInt16 rankType, string name)
    {
        string rankStr = _avatar.rankModule.GetRankByName(rankType, name);
        if(rankStr == "")
        {
            _mineRankSprite.gameObject.SetActive(false);
            _mineRank.gameObject.SetActive(true);
            _mineRank.text = "榜外";
        }
        else if (rankStr == "1")
        {
            _mineRankSprite.gameObject.SetActive(true);
            _mineRank.gameObject.SetActive(false);
            _mineRankSprite.spriteName = "[11-15]UI-s2-ph-diyi";
        }
        else if (rankStr == "2")
        {
            _mineRankSprite.gameObject.SetActive(true);
            _mineRank.gameObject.SetActive(false);
            _mineRankSprite.spriteName = "[11-15]UI-s2-ph-dier";
        }
        else if (rankStr == "3")
        {
            _mineRankSprite.gameObject.SetActive(true);
            _mineRank.gameObject.SetActive(false);
            _mineRankSprite.spriteName = "[11-15]UI-s2-ph-disan";
        }
        else
        {
            _mineRankSprite.gameObject.SetActive(false);
            _mineRank.gameObject.SetActive(true);
            _mineRank.text = rankStr;
        }
    }

    private string _petMaxName; //最厉害宠物的名字
    private UInt32 _petMaxTwoScore; //最厉害的二个宠物评分
    private UInt32 _petMaxScore; // 最厉害的宠物评分
    private void InitPetValue()
    {
        List<PetInfo> petInfo = _avatar.petModule.GetAllPets();
        if(petInfo.Count == 0)
        {
            _petMaxScore = 0;
            _petMaxName = "";
            return;
        }
        List<UInt32> scoreLst = new List<UInt32>();
        foreach(PetInfo info in petInfo)
        {
            scoreLst.Add(info.score);
        }
        scoreLst.Sort(delegate(UInt32 x, UInt32 y)
        {
            return y.CompareTo(x);
        });

        foreach (PetInfo info in petInfo)
        {
            if (info.score == scoreLst[0])
            {
                _petMaxName = info.name;
            }
        }

        _petMaxScore = scoreLst[0];
        if(scoreLst.Count ==1)
        {       
            _petMaxTwoScore = scoreLst[0];
        }
        else
        {
            _petMaxScore = scoreLst[0] + scoreLst[1];
        }       
    }

    private  string SchoolString(UInt32 type)
    {
        string schoolStr = "";
        switch(type)
        {
            case Define.ROLE_TYPE_DIANCANG:
                schoolStr =  "点苍派";
                break;
            case Define.ROLE_TYPE_JIUHUASHAN:
                schoolStr = "九华山";
                break;
            case Define.ROLE_TYPE_TIANXINGE:
                schoolStr = "天心阁";
                break;
            case Define.ROLE_TYPE_XIAOYAOGONG:
                schoolStr = "逍遥宫";
                break;
            case Define.ROLE_TYPE_XUANQINGGONG:
                schoolStr = "玄清宫";
                break;
            case Define.ROLE_TYPE_ZIDIANMEN:
                schoolStr = "紫电门";
                break;
            default:
                break;       
        }
        return schoolStr;
    }
}
