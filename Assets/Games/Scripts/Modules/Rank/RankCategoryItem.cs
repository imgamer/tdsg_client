﻿using UnityEngine;
using System.Collections;
using System;

public class RankCategoryItem : WinItem
{
    private int _categoryType;
    public int CategoryType
    {
        set
        {
            _categoryType = value;
        }
        get
        {
            return _categoryType;
        }
    }

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
    }
}
