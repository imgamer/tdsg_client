﻿using UnityEngine;
using System.Collections;

public class UIRoleInfo : UIWin
{

    protected override void OnInit()
    {
        Init();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _name;
    private GameObject _roleRoot;

    //role
    private GameObject _role;
    private UILabel _equipScore;
    private UILabel _levelScore;
    private UILabel _skillScore;
    private UILabel _pracScore;
    private UILabel _roleScore;

    //faction
    private GameObject _faction;
    private UILabel _fRoleScore;
    private UILabel _petScore;
    private UILabel _synScore;
    private void Init()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _role = center.Find("Role").gameObject;
        _equipScore = center.Find("Role/EquipScore").GetComponent<UILabel>();
        _levelScore = center.Find("Role/LevelScore").GetComponent<UILabel>();
        _skillScore = center.Find("Role/SkillScore").GetComponent<UILabel>();
        _pracScore = center.Find("Role/PracScore").GetComponent<UILabel>();
        _roleScore = center.Find("Role/RoleScore").GetComponent<UILabel>();

        _faction = center.Find("Faction").gameObject;
        _fRoleScore = center.Find("Faction/RoleScore").GetComponent<UILabel>();
        _petScore = center.Find("Faction/PetScore").GetComponent<UILabel>();
        _synScore = center.Find("Faction/SynScore").GetComponent<UILabel>();

        GameObject checkBtn = center.Find("CheckBtn").gameObject;
        InputManager.instance.AddClickListener(checkBtn, (go) =>
        {

        });
    }
}
