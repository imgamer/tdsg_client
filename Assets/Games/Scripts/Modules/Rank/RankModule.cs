﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public class RankUIData
{
    public int category;
    public string categoryName;
    public int categoryPriority;
    public Dictionary<string, RankCategory> subCategory = new Dictionary<string,RankCategory>();

}

public class RankCategory
{
    public int category;
    public string categoryName;
    public int categoryPriority;
    public List<string> attribute = new List<string>();
}

public class RankData
{
    public UInt64 lastCallTime;
    public List<List<string>> rankData = new List<List<string>>();

    public RankData(UInt64 callTime, List<object> rankDatas)
    {
        foreach (object obj in rankDatas)
        {
            List<object> rank = (List<object>)obj;
            List<string> temprRank = new List<string>();
            foreach (object a in rank)
            {
                temprRank.Add(Convert.ToString(a));
            }
            rankData.Add(temprRank);
        }
        lastCallTime = callTime;
    }
}

public class RankModule 
{
    KBEngine.Avatar _avatar;
    private Dictionary<string, RankUIData> _rankUIData = new Dictionary<string, RankUIData>(); //UI界面数据

    private Dictionary<UInt16, RankData> _rankData = new Dictionary<UInt16, RankData>();
    public RankModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;

        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_rank");
        _rankUIData = JsonMapper.ToObject<Dictionary<string, RankUIData>>(textdata.text);
    }

    public List<RankUIData> GetRankUIData
    {
        get
        {
            return new List<RankUIData>(_rankUIData.Values);
        }
    }

    public RankCategory GetAssignData(int category, int type)
    {
        RankCategory data;
        RankUIData uiData;
        _rankUIData.TryGetValue(Convert.ToString(category), out uiData);
        uiData.subCategory.TryGetValue(Convert.ToString(type), out data);

        return data;
    }

    public void QueryRankDatas(UInt16 type, UInt16 begin, UInt16 count)
    {
        if (TypeLastCallTime(type))
        {
            _avatar.baseCall("queryRankingDatas", new object[] { type, begin, count });
        }
        else
        {
            EventManager.Invoke<UInt16>(EventID.EVNT_RANK_UPDATE, type);
        }       
    }

    public void ReceiveRankDatas(UInt16 rankType, UInt16 beginIndex, List<object> rankDatas)
    {
        DateTime time = DateTime.Now;
        RankData  rankData = new RankData(ConvertDateTime(time), rankDatas);
        if(_rankData.ContainsKey(rankType))
        {
            _rankData[rankType] = rankData;          
        }
        else
        {
            _rankData.Add(rankType, rankData);
        }

        EventManager.Invoke<UInt16>(EventID.EVNT_RANK_UPDATE, rankType);
    }

    /// <summary>
    /// 获取当前时间戳
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public UInt64 ConvertDateTime(DateTime time)
    {
        DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
        return (UInt64)(time - startTime).TotalSeconds;
    }

    public List<List<string>>  GetRankData(UInt16 rankType)
    {
        RankData data;
        _rankData.TryGetValue(rankType, out data);
        return data.rankData;
    }

    public int RankDataCount
    {
        get
        {
            return _rankData.Count;
        }
    }

    private bool TypeLastCallTime(UInt16 type)
    {
        RankData data;
        _rankData.TryGetValue(type, out data);
        if(data == null)
        {
            return true;
        }
        UInt64 nowTime = ConvertDateTime(DateTime.Now);
        if(nowTime - data.lastCallTime > Define.RANK_DATAS_REFRESH)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// 判断自己人物是否在排行榜内
    /// </summary>
    /// <param name="type"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public string GetRankByName(UInt16 type, string name)
    {
        RankData data;
        _rankData.TryGetValue(type, out data);
        if(data == null)
        {
            return "";
        }
        foreach(List<string> lst in data.rankData)
        {
            if(type == 104)
            {
                if(lst[1] == name && lst [2] == _avatar.Name)
                {
                    return lst[0];
                }
            }
            else if(lst[1] == name)
            {
                return lst[0];
            }
        }
        return "";
    }
}
