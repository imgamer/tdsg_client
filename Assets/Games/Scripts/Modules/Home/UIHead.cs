﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 角色宠物头像界面
/// </summary>
public class UIHead : UIWin
{

    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdatePet);
        EventManager.AddListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        EventManager.AddListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdatePlayer);
        InitItem();
        InitProgress();
    }

    protected override void OnOpen(params object[] args)
    {
        UpdateAll();
    }

    protected override void OnRefresh()
    {
        progress.value = 0.08f;
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_PET_BATTLE_STATE, UpdatePet);
        EventManager.RemoveListener<PetInfo>(EventID.EVNT_PET_STATE_UPDATE, UpdatePet);
        EventManager.RemoveListener<object>(EventID.EVNT_ROLE_ATTRIBUTE_UPDATE, UpdatePlayer);
    }

    private UISprite _playerIcon;
    private UISprite _playerSchool;
    private UILabel _playerName;
    private UILabel _playerLevel;
    private UILabel _playerPower;

    private UISprite _petIcon;
    private UILabel _petLevel;
    private void InitItem()
    {
        Transform topLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopLeft);
        GameObject btnPlayer = topLeft.Find(Function.Player.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnPlayer);
        _playerIcon = btnPlayer.transform.Find("Icon/Icon").GetComponent<UISprite>();
        _playerSchool = btnPlayer.transform.Find("School").GetComponent<UISprite>();
        _playerName = btnPlayer.transform.Find("Name/Text").GetComponent<UILabel>();
        _playerLevel = btnPlayer.transform.Find("Level/Text").GetComponent<UILabel>();
        _playerPower = btnPlayer.transform.Find("Power/Text").GetComponent<UILabel>();

        InputManager.instance.AddClickListener(btnPlayer, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UICharacter);
        });
        GameObject btnPet = topLeft.Find(Function.Pet.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnPet);
        _petIcon = btnPet.transform.Find("Icon/Icon").GetComponent<UISprite>();
        _petLevel = btnPet.transform.Find("Level/Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnPet, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIPet);
        });
    }

    private UIProgressBar progress;
    private void InitProgress()
    {
        Transform bottom = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Bottom);
        progress = bottom.Find("Progress").GetComponent<UIProgressBar>();
    }

    private void UpdateAll()
    {
//         _playerIcon.spriteName = "";
//         _playerSchool.spriteName = "";
        _playerLevel.text = string.Format("{0}", GameMain.Player.Level);
        _playerName.text = GameMain.Player.Name;
        _playerLevel.text = string.Format("Lv.{0}", GameMain.Player.Level);
        _playerPower.text = string.Format("战力 {0}", 500000);
        UpdatePet();
    }

    private void UpdatePlayer(object o)
    {
        if (!Active) return;
        _playerLevel.text = string.Format("Lv.{0}", GameMain.Player.Level);
        _playerPower.text = string.Format("战力 {0}", 500000);
    }

    private void UpdatePet(PetInfo o)
    {
        if (!Active) return;
        UpdatePet();
    }
    private void UpdatePet(object o)
    {
        if (!Active) return;
        UpdatePet();
    }
    private void UpdatePet()
    {
        PetInfo pet = GameMain.Player.petModule.GetBattlePet();
        if (pet != null)
        {
            //_petIcon.spriteName = "Pet_002";
            _petIcon.enabled = true;
            _petLevel.text = string.Format("Lv.{0}", pet.level);
        }
        else
        {
            _petIcon.enabled = false;
            _petLevel.text = string.Empty;
        }
    }


}
