﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 主场景队伍界面Item
/// </summary>
public class MemberItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set { _icon.spriteName = value; }
    }
    private UILabel _name;
    public string Name
    {
        set { _name.text = value; }
    }
    private UISprite _schoolIcon;
    public string SchoolIcon
    {
        set { _schoolIcon.spriteName = value; }
    }
    private UILabel _level;
    public string Level
    {
        set { _level.text = value; }
    }
    private UIProgressBar _redBar;
    public float RedBar
    {
        set { _redBar.value = value; }
    }
    private UIProgressBar _blueBar;
    public float BlueBar
    {
        set { _blueBar.value = value; }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _schoolIcon = transform.Find("School/Icon").GetComponent<UISprite>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _redBar = transform.Find("RedBar").GetComponent<UIProgressBar>();
        _blueBar = transform.Find("BlueBar").GetComponent<UIProgressBar>();
    }
}
