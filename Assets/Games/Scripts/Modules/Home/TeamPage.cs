﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 主界面队伍Page
/// </summary>
public class TeamPage : WinPage
{
    protected override void OnInit()
    {
        EventManager.AddListener<TeamModule.TeamState>(EventID.EVNT_TEAM_STATE_UPDATE, ChangeState);
        EventManager.AddListener<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, RefreshMembers);
        InitItem();
        InitMembers();
    }

    protected override void OnOpen(params object[] args)
    {
        SwitchTo(GameMain.Player.teamModule.State);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<TeamModule.TeamState>(EventID.EVNT_TEAM_STATE_UPDATE, ChangeState);
        EventManager.RemoveListener<object>(EventID.EVNT_TEAM_MEMBER_UPDATE, RefreshMembers);
    }
    private GameObject _default;
    private GameObject _matching;
    private GameObject _members;
    private void InitItem()
    {
        _default = transform.Find("Default").gameObject;
        _matching = transform.Find("Matching").gameObject;
    }
    private void SwitchTo(TeamModule.TeamState state)
    {
        switch (state)
        {
            case TeamModule.TeamState.Default:
                _default.SetActive(true);
                _matching.SetActive(false);
                _members.SetActive(false);
                break;
            case TeamModule.TeamState.Matching:
                _default.SetActive(false);
                _matching.SetActive(true);
                _members.SetActive(false);
                break;
            case TeamModule.TeamState.Captain:
            case TeamModule.TeamState.Member:
                _default.SetActive(false);
                _matching.SetActive(false);
                _members.SetActive(true);
                _menu.SetActive(false);
                UpdateMembers();
                break;
            default:
                break;
        }
    }
    private MemberItem[] _items = new MemberItem[3];
    private GameObject _menu;
    private GameObject _btnOne;
    private UILabel _btnOneName;
    private GameObject _btnTwo;
    private UILabel _btnTwoName;
    private void InitMembers()
    {
        _members = transform.Find("Members").gameObject;
        _menu = _members.transform.Find("Menu").gameObject;
        _btnOne = _menu.transform.Find("UIGrid/0").gameObject;
        _btnOneName = _btnOne.transform.Find("Text").GetComponent<UILabel>();
        _btnTwo = _menu.transform.Find("UIGrid/1").gameObject;
        _btnTwoName = _btnTwo.transform.Find("Text").GetComponent<UILabel>();

        Transform grid = _members.transform.Find("Scroll/UIGrid");
        for (int i = 0; i < _items.Length; i++)
        {
            MemberItem item = grid.Find(i.ToString()).GetComponent<MemberItem>();
            _items[i] = item;
        }
    }
    private void UpdateMembers()
    {
        List<TeamModule.TeamMemberData> datas = GameMain.Player.teamModule.TeamMembers;
        for (int i = 0; i < _items.Length; i++)
        {
            MemberItem item = _items[i];
            if (i < datas.Count)
            {
                TeamModule.TeamMemberData data = datas[i];
                item.gameObject.SetActive(true);
                item.Init(this.ParentUI);
                var resName = "";//EntityPrefabMap.instance.getAssetName(Convert.ToInt32(data.getDefinedPropterty("modelID")), false);
                item.Name = data.name;
                item.Level = string.Format("{0}", data.level);
                InputManager.instance.AddClickListener(item.gameObject, (go) => 
                {
                    if (data.dbId == GameMain.Player.DatabaseID)
                    {
                        _menu.SetActive(!_menu.activeSelf);
                        _menu.transform.localPosition = new Vector3(0, item.transform.localPosition.y, 0);
                        if (!_menu.activeSelf) return;
                        if (data.status == Define.TEAM_MEMBER_SEPARATING)
                        {
                            _btnOneName.text = "回归队伍";
                            InputManager.instance.AddClickListener(_btnOne, (go1) =>
                            {
                                _menu.SetActive(false);
                                GameMain.Player.teamModule.BackForTeam();
                            });
                        }
                        else
                        {
                            _btnOneName.text = "暂离队伍";
                            InputManager.instance.AddClickListener(_btnOne, (go1) =>
                            {
                                _menu.SetActive(false);
                                GameMain.Player.teamModule.SeparateFromTeam();
                            });
                        }
                        _btnTwoName.text = "离开队伍";
                        InputManager.instance.AddClickListener(_btnTwo, (go2) =>
                        {
                            _menu.SetActive(false);
                            GameMain.Player.teamModule.RequestQuitTeam();
                        });
                    }
                    else if (GameMain.Player.teamModule.State == TeamModule.TeamState.Captain)
                    {
                        _menu.SetActive(!_menu.activeSelf);
                        _menu.transform.localPosition = new Vector3(0, item.transform.localPosition.y, 0);
                        if (!_menu.activeSelf) return;
                        _btnOneName.text = "升为队长";
                        _btnTwoName.text = "请离队伍";
                        InputManager.instance.AddClickListener(_btnOne, (go1) =>
                        {
                            _menu.SetActive(false);
                            GameMain.Player.teamModule.PromotedCaptain(data.dbId);
                        });
                        InputManager.instance.AddClickListener(_btnTwo, (go2) =>
                        {
                            _menu.SetActive(false);
                            GameMain.Player.teamModule.KickTeam(data.dbId);
                        });
                    }
                    else
                    {
                        _menu.SetActive(false);
                        InputManager.instance.RemoveClickListener(_btnOne);
                        InputManager.instance.RemoveClickListener(_btnTwo);
                    }
                });
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
    }
    private void ChangeState(TeamModule.TeamState state)
    {
        if (!Active) return;
        SwitchTo(state);
    }
    private void RefreshMembers(object o)
    {
        if (!Active) return;
        UpdateMembers();
    }
}
