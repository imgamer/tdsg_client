﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 任务队伍界面
/// </summary>
public class UITaskTeam : UIWin
{
    protected override void OnInit()
    {
        InitItem();
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        uiTab.SwitchTo(uiTab.CurTabIndex);
        ResetAnim();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private Animation _animtion;
    private Transform root;
    private Transform btnCloseIcon;
    private void InitItem()
    {
        Transform right = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Right);
        _animtion = right.GetComponent<Animation>();
        root = right.Find("Root");
        GameObject btnClose = root.Find("BtnClose").gameObject;
        btnCloseIcon = btnClose.transform.Find("Icon");
        InputManager.instance.AddClickListener(btnClose, (go) => 
        {
            PlayAnim();
        });
    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(this);
        };
        InputManager.instance.AddPressListener(uiTab.tabAndPage[0].tab.gameObject, (go, press) => 
        {
            if (uiTab.CurTabIndex == 0 && press) UIManager.instance.OpenWindow(null, WinID.UITask);
        });
        InputManager.instance.AddPressListener(uiTab.tabAndPage[1].tab.gameObject, (go, press) =>
        {
            if (uiTab.CurTabIndex == 1 && press) UIManager.instance.OpenWindow(null, WinID.UITeam);
        });
    }

    #region 动画
    private void ResetAnim()
    {
        _forward = true;
        root.localPosition = new Vector3(-330, 200, 0);
        btnCloseIcon.localEulerAngles = Vector3.zero;
    }
    private bool _forward = false;
    private void PlayAnim()
    {
        AnimationState animtionState = _animtion[_animtion.clip.name];
        UITools.PlayUIAnim(_animtion, animtionState, _forward);
        _forward = !_forward;
    }
    #endregion

}
