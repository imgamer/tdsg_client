﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// 主界面管理
/// </summary>
public class HomeManager : MonoSingleton<HomeManager>
{
    protected override void OnInit()
    {
        InitFunctionState();
        _functionsData = Convert.ToInt32("101111111111111111100010", 2);
    }

    protected override void OnUnInit()
    {
        _functionsData = 0;
        _functionsState.Clear();
        _functionsItem.Clear();
    }
    /// <summary>
    /// 初始化状态
    /// </summary>
    private void InitFunctionState()
    {
        int max = (int)Function.Max;
        for (int i = 0; i < max; i++)
        {
            Function function = (Function)i;
            _functionsState[function] = true;
        }
    }
    //本地功能状态数据
    private Dictionary<Function, bool> _functionsState = new Dictionary<Function, bool>();
    //服务器功能状态数据
    private Int32 _functionsData = 0;
    /// <summary>
    /// 获取功能按钮组件状态
    /// </summary>
    /// <param name="function"></param>
    /// <returns></returns>
    private bool GetFunctionState(Function function)
    {
        //ID表示二进制中的第几位
        int index = HomeFunctionsConfig.SharedInstance.GetID(function);
        int temp = 1 << index;
        int ret = temp & _functionsData;
        return ret != 0 && _functionsState[function];
    }

    public void OpenHome()
    {
        UIManager.instance.OpenWindow(null, WinID.UIMain);
        UIManager.instance.OpenWindow(null, WinID.UIHead);
        UIManager.instance.OpenWindow(null, WinID.UITaskTeam);
        UIManager.instance.OpenWindow(null, WinID.UIChat);
        UpdateFunctionState();
    }

    public void TryOpenUI()
    {
        GameMain.Player.rewardModule.TryOpenUISign();
    }

    private void UpdateFunctionState()
    {
        foreach (var item in _functionsItem)
        {
            bool state = GetFunctionState(item.Key);
            if (item.Value.activeSelf == state) continue;
            item.Value.SetActive(state);
        }
        EventManager.Invoke<object>(EventID.EVNT_FUNCTION_STATE_UPDATE, null);
    }

    //功能按钮控件
    private Dictionary<Function, GameObject> _functionsItem = new Dictionary<Function, GameObject>();
    /// <summary>
    /// 设置功能按钮控件
    /// </summary>
    /// <param name="item"></param>
    public void AddFunctionsItem(GameObject item)
    {
        if (item == null) return;
        Function function = (Function)Enum.Parse(typeof(Function), item.name);
        _functionsItem[function] = item;
    }
    /// <summary>
    /// 获取功能按钮控件的优先级
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public int GetPriority(Transform item)
    {
        if (item == null) return -1;
        Function function = (Function)Enum.Parse(typeof(Function), item.name);
        return HomeFunctionsConfig.SharedInstance.GetPriority(function);
    }
    /// <summary>
    /// 本地开启或者关闭功能
    /// </summary>
    /// <param name="functions"></param>
    /// <param name="state"></param>
    public void SetFunctionState(IEnumerable<Function> functions, bool state)
    {
        foreach (var item in functions)
        {
            _functionsState[item] = state;
        }
        UpdateFunctionState();
    }
    /// <summary>
    /// 本地开启所有功能
    /// </summary>
    public void OpenAllFunctions()
    {
        int max = (int)Function.Max;
        for (int i = 0; i < max; i++)
        {
            Function function = (Function)i;
            _functionsState[function] = true;
        }
        UpdateFunctionState();
    }
    /// <summary>
    /// 服务器通知开启新功能
    /// </summary>
    /// <param name="function"></param>
    public void UpdateFunctions(Int32 functionsData)
    {
        _functionsData = functionsData;
        //TODO 播放新功能开启动画等
        UpdateFunctionState();
    }


}
