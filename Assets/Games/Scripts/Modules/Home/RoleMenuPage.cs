﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 角色菜单Page
/// </summary>
public class RoleMenuPage : WinPage 
{
    public struct RoleData
    {
        public bool teaming;
        public string name;
        public Int32 entityID;
    }
    protected override void OnInit()
    {
        InitMenu();
    }
    private RoleData _data;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _data = (RoleData)args[0];
        _name.text = _data.name;
        if(_data.teaming)
        {
            btnTeamName.text = "申请入队";
        }
        else
        {
            btnTeamName.text = "邀请入队";
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private UILabel _name;
    private UILabel btnTeamName;
    private void InitMenu()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        GameObject btnTeam = transform.Find("UIGrid/2").gameObject;
        btnTeamName = btnTeam.transform.Find("Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnTeam, (go) =>
        {
            Close();
            if (_data.teaming)
            {
                GameMain.Player.teamModule.RequestJoinTeamNear(_data.entityID, _data.name);
            }
            else
            {
                GameMain.Player.teamModule.InviteTeammate(_data.entityID, _data.name);
            }
        });
    }
}
