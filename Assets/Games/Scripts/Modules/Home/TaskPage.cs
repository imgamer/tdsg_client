﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI;
/// <summary>
/// 任务界面
/// </summary>
public class TaskPage : WinPage
{
    private Table _uiTable;
    private TaskItem _template;
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_UPDATE_QUEST, UpdateTask);
        EventManager.AddListener<uint>(EventID.EVNT_REMOVE_QUEST, RemoveTask);
        Transform table = transform.Find("Scroll/Table");
        _uiTable = table.GetComponent<Table>();
        _template = table.Find("Item").GetComponent<TaskItem>();
        _template.Init(ParentUI);
        _template.gameObject.SetActive(false);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        RefreshTask();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_UPDATE_QUEST, UpdateTask);
        EventManager.RemoveListener<uint>(EventID.EVNT_REMOVE_QUEST, RemoveTask);
    }

    private void UpdateTask(object o)
    {
        if (!Active) return;
        RefreshTask();
    }

    private void RemoveTask(uint o)
    {
        if (!Active) return;
        RefreshTask();
    }

    private List<TaskItem> _taskItems = new List<TaskItem>();
    private void RefreshTask()
    {
        List<Quest.QuestBase> dataList = GameMain.Player.taskModule.GetAllQuests();
        int dataNum = dataList.Count;
        int itemNum = _taskItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                Quest.QuestBase questBase = dataList[i];
                if (i < itemNum)
                {
                    TaskItem item = _taskItems[i];
                    item.gameObject.SetActive(true);
                    SetData(item, questBase);
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    TaskItem item = Instantiate(_template) as TaskItem;
                    _template.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiTable.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(ParentUI);
                    _taskItems.Add(item);
                    SetData(item, questBase);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                TaskItem item = _taskItems[i];
                if (i < dataNum)
                {
                    Quest.QuestBase questBase = dataList[i];
                    item.gameObject.SetActive(true);
                    SetData(item, questBase);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiTable.Reposition();
    }
    private void SetData(TaskItem item, Quest.QuestBase questBase)
    {
        item.Name = "[fff000]" + questBase.GetCurTaskName();
        item.Desc = questBase.GetCurTaskDesc();
        InputManager.instance.AddClickListener(item.BoxObject, (go) =>
        {
            questBase.ExecuteCurTask();
        });
    }
}
