﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 主界面任务列表Item
/// </summary>
public class TaskItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                _icon.spriteName = value;
            }
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private UILabel _desc;
    public string Desc
    {
        set
        {
            _desc.text = value;
            float hight = _desc.printedSize.y;
            _background.height = (int)(hight - _desc.fontSize - 6) + _originalHight;
        }
    }
    private UISprite _background;
    private int _originalHight;
    public GameObject BoxObject
    {
        get { return _background.gameObject; }
    }
    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _desc = transform.Find("Desc").GetComponent<UILabel>();
        _background = transform.Find("Background").GetComponent<UISprite>();
        _originalHight = _background.height;
    }

}
