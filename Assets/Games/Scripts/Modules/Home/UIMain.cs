﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 主界面
/// </summary>
public class UIMain : UIWin
{

    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_FUNCTION_STATE_UPDATE, SortMenu);
        InitTop();
        InitBottom();
    }

    protected override void OnOpen(params object[] args)
    {
        ResetMenuAnim();
        _sceneName.text = SceneManager.instance.CurrentSceneDesc;
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_FUNCTION_STATE_UPDATE, SortMenu);
    }

    #region 初始化
    private UI.Grid _topGrid;
    private UILabel _sceneName;
    private void InitTop()
    {
        Transform topLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopLeft);
        GameObject btnShop = topLeft.Find(Function.Shop.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnShop);
        InputManager.instance.AddClickListener(btnShop, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIShop);
        });
        GameObject btnFriend = topLeft.Find(Function.Friend.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnFriend);
        InputManager.instance.AddClickListener(btnFriend, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIFriend);
        });


        Transform topRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopRight);
        GameObject btnMap = topRight.Find(Function.Map.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnMap);
        InputManager.instance.AddClickListener(btnMap, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIWorldMap);
        });
        GameObject btnScene = topRight.Find("Scene").gameObject;
        _sceneName = topRight.Find("Scene/Name").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnScene, (go) =>
        {

        });

        Transform btnRoot = topRight.Find("BtnRoot");
        _topGrid = btnRoot.GetComponent<UI.Grid>();

        GameObject btnAct = btnRoot.Find(Function.Act.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnAct);
        InputManager.instance.AddClickListener(btnAct, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIActivity);
        });
        GameObject btnAward = btnRoot.Find(Function.Award.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnAward);
        InputManager.instance.AddClickListener(btnAward, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UISign);
        });

        GameObject btnGuide = btnRoot.Find(Function.Guide.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnGuide);
        InputManager.instance.AddClickListener(btnGuide, (go) =>
        {

        });

        GameObject btnHang = btnRoot.Find(Function.Hang.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnHang);
        InputManager.instance.AddClickListener(btnHang, (go) =>
        {

        });
        GameObject btnPromote = btnRoot.Find(Function.Promote.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnPromote);
        InputManager.instance.AddClickListener(btnPromote, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UISlave);
        });
        GameObject btnVigour = btnRoot.Find(Function.Vigour.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnVigour);
        InputManager.instance.AddClickListener(btnVigour, (go) =>
        {

        });
    }
    private int _curState = 0;
    private UI.Grid[] _bottomGrids = new UI.Grid[2];
    private Transform _btnShowIcon;
    private Animation _menuAnimtion;
    private void InitBottom()
    {
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        _menuAnimtion = bottomRight.GetComponent<Animation>();
        GameObject btnShow = bottomRight.Find("BtnShow").gameObject;
        _btnShowIcon = bottomRight.Find("BtnShow/Icon");
        InputManager.instance.AddClickListener(btnShow, (go) =>
        {
            PlayMenuAnim();
        });

        Transform btnRoot = bottomRight.Find("BtnRoot");
        Transform state0 = btnRoot.Find("0");
        _bottomGrids[0] = state0.GetComponent<UI.Grid>();

        GameObject btnBag = state0.Find(Function.Bag.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnBag);
        InputManager.instance.AddClickListener(btnBag, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIKitbag);
        });
        GameObject btnStrengthen = state0.Find(Function.Strengthen.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnStrengthen);
        InputManager.instance.AddClickListener(btnStrengthen, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIEquipManufacture);
        });
        GameObject btnFaction = state0.Find(Function.Faction.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnFaction);
        InputManager.instance.AddClickListener(btnFaction, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIFaction);
        });
        GameObject btnSkill = state0.Find(Function.Skill.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnSkill);
        InputManager.instance.AddClickListener(btnSkill, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UISkill);
        });


        Transform state1 = btnRoot.Find("1");
        _bottomGrids[1] = state1.GetComponent<UI.Grid>();

        GameObject btnAssist = state1.Find(Function.Assist.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnAssist);
        InputManager.instance.AddClickListener(btnAssist, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIHero);
        });

        GameObject btnRank = state1.Find(Function.Rank.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnRank);
        InputManager.instance.AddClickListener(btnRank, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIRank);
        });

        GameObject btnSystem = state1.Find(Function.System.ToString()).gameObject;
        HomeManager.instance.AddFunctionsItem(btnSystem);
        InputManager.instance.AddClickListener(btnSystem, (go) =>
        {

        });

    }
    #endregion

    #region 菜单动画
    private void ResetMenuAnim()
    {
        _curState = 0;
        _btnShowIcon.localEulerAngles = Vector3.zero;
        _bottomGrids[0].transform.localPosition = Vector3.zero;
        _bottomGrids[0].gameObject.SetActive(true);
        _bottomGrids[1].gameObject.SetActive(false);
    }
    private void PlayMenuAnim()
    {
        _curState = _curState == 0 ? 1 : 0;
        AnimationState animtionState = _menuAnimtion[_menuAnimtion.clip.name];
        UITools.PlayUIAnim(_menuAnimtion, animtionState, _curState == 1);
    }
    #endregion

    #region 菜单排序
    private void SortMenu(object o)
    {
        if (!Active) return;
        _topGrid.onCustomSort = (a, b) => 
        {
            int i = HomeManager.instance.GetPriority(a);
            int j = HomeManager.instance.GetPriority(b);
            return i.CompareTo(j);
        };
        _topGrid.Reposition();

        _bottomGrids[_curState].onCustomSort = (a, b) =>
        {
            int i = HomeManager.instance.GetPriority(a);
            int j = HomeManager.instance.GetPriority(b);
            return j.CompareTo(i);
        };
        _bottomGrids[_curState].Reposition();
    }
    #endregion

}
