﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 角色菜单界面（点击场景角色显示头像菜单）
/// </summary>
public class UIRoleMenu : UIWin
{
    protected override void OnInit()
    {
        InitItem();
    }
    private RoleMenuPage.RoleData _roleData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        KBEngine.Avatar entity = (KBEngine.Avatar)args[0];
        _roleData = new RoleMenuPage.RoleData() 
        {
            teaming = (entity.TeamStatus == Define.TEAM_STATUS_CAPTAIN || entity.TeamStatus == Define.TEAM_STATUS_MEMBER),
            entityID = entity.id, 
            name = entity.Name 
        };
        _menuPage.Close();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private RoleMenuPage _menuPage;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _menuPage = center.Find("Menu").GetComponent<RoleMenuPage>();
        Transform right = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Right);
        GameObject _icon = right.Find("Icon").gameObject;
        InputManager.instance.AddClickListener(_icon, (go) => 
        {
            _menuPage.Open(this, _roleData);
        });
        GameObject box = transform.Find("Box").gameObject;
        InputManager.instance.AddPressListener(box, (go, press) =>
        {
            if (press) UIManager.instance.CloseWindow(winID);
        });
    }
}
