﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DBID_TYPE = System.UInt64;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;
/// <summary>
/// 帮会数据类
/// </summary>
public class TongModule
{
    public class TongMemberData
    {
        public DBID_TYPE dbId;
        public Int32 id;
        public string name;
        public UInt16 level;
        public UInt16 feature; //特征值，包含性别、职业等属性
        public Byte status;    //状态，暂离，跟随，领导，离线
        public Byte duty;      //职务
        public float joinTime; //入帮时间
        public float logoutTime; //下线时间
        public UInt32 contribution; //帮贡

        private KBEngine.GameObject entity;
        public void SetEntity(Int32 id)
        {
            this.id = id;
            entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
        }
        public KBEngine.GameObject GetEntity()
        {
            if (entity == null || entity.SceneEntityObj == null)
            {
                entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
            }
            return entity;
        }
    }

    public class TongApplicant
    {
        public DBID_TYPE dbId;
        public Int32 id;
        public string name;
        public UInt16 level;
        public UInt16 feature; //特征值，包含性别、职业等属性

        private KBEngine.GameObject entity;
        public void SetEntity(Int32 id)
        {
            this.id = id;
            entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
        }
        public KBEngine.GameObject GetEntity()
        {
            if (entity == null || entity.SceneEntityObj == null)
            {
                entity = SceneManager.instance.CurrentScene.GetKBEntityByID(this.id);
            }
            return entity;
        }
    }

    #region fields and properties

    private KBEngine.Avatar _avatar;
    //帮会成员字典
    private Dictionary<DBID_TYPE, TongMemberData> _tongMembers = new Dictionary<DBID_TYPE, TongMemberData>();
    //当前请求列表
    private List<TongApplicant> _applyMembers = new List<TongApplicant>();

    public string TongName { get; private set; }
    public UInt16 TongLevel { get; private set; }
    public DBID_TYPE CaptainDbid { get; private set; }
    public List<TongMemberData> TongMembers
    {
        get { return new List<TongMemberData>(_tongMembers.Values); }
    }
    public List<TongApplicant> ApplyMembers
    {
        get { return _applyMembers; }
    }
    #endregion

    public TongModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    #region 加入帮会
    //创建帮会
    public void RequestCreateTong(string name)
    {
        _avatar.cellCall("createTong", name);
    }

    public void JoinTong(JSON_DICT_TYPE tongInfo)
    {
        TongName = (string)tongInfo["name"];
        TongLevel = (UInt16)tongInfo["level"];
        CaptainDbid = (DBID_TYPE)tongInfo["captainDbid"];

        _tongMembers.Clear();
        List<object> members = (List<object>)tongInfo["members"];
        for (int i = 0; i < members.Count; i++)
        {
            JSON_DICT_TYPE member = (JSON_DICT_TYPE)members[i];
            TongMemberData data = new TongMemberData();
            data.dbId = (DBID_TYPE)member["dbid"];
            data.id = (Int32)member["id"];
            data.name = (string)member["name"];
            data.level = (UInt16)member["level"];
            data.feature = (UInt16)member["feature"]; //特征值，包含性别、职业等属性
            data.status = (Byte)member["status"];	  //状态，暂离，跟随，领导，离线
            data.duty = (Byte)member["duty"];         //职务
            data.joinTime = (float)member["joinTime"];
            data.logoutTime = (float)member["logoutTime"];
            data.contribution = (UInt32)member["contribution"];
            data.SetEntity(data.id);

            _tongMembers[data.dbId] = data;
        }
        EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
    }
    #endregion

    #region 退出帮会
    //退出帮会
    public void RequestQuitTong()
    {
        _avatar.cellCall("leaveTong");
    }
    public void QuitTong(Byte reason)
    {
        _tongMembers.Clear();
        switch (reason)
        {
            case Define.LEAVE_TONG_KICKOUT:
                break;
            case Define.LEAVE_TONG_INITIATIVE:
                break;
            case Define.LEAVE_TONG_DISBAND:
                break;
            default:
                break;
        }
        EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
    }
    #endregion

    #region 邀请入会
    //通过id和名字邀请一个附近的玩家入会
    public void InviteTongMember(Int32 entityId, string entityName)
    {
        _avatar.cellCall("inviteJoinTongNear", new object[] { entityId, entityName });
    }
    //通过名字邀请一个玩家入会
    public void InviteTongMember(string entityName)
    {
        _avatar.cellCall("inviteJoinTongRemote", new object[] { entityName });
    }
    //回复是否同意入会邀请
    private void ReplyJoinTongInvite(bool agree)
    {
        int arg = agree ? 1 : 0;
        _avatar.baseCall("replyJoinTongInvite", new object[] { arg });
    }
    //接收到的入会邀请
    public void ReceiveJoinTongInvite(string inviterName, string tongName)
    {
        TipManager.instance.ShowTwoButtonTip(4624, inviterName + "," + tongName, () => 
        {
            ReplyJoinTongInvite(true);
            return true;
        }, 
        () => 
        {
            ReplyJoinTongInvite(false);
            return true;
        });
    }
    #endregion

    #region 申请/接收加入玩家的帮会
    //申请加入玩家的帮会，当客户端能看得到玩家时调用
    public void RequestJoinTongNear(Int32 entityId, string entityName)
    {
        _avatar.cellCall("requestJoinTongNear", new object[] { entityId, entityName });
    }
    //申请加入玩家的帮会，当玩家不在视野内时调用
    public void RequestJoinTongRemote(string entityName)
    {
        _avatar.cellCall("requestJoinTongRemote", new object[] { entityName });
    }
    //接收到入会申请成员
    public void ReceiveJoinTongRequest(JSON_DICT_TYPE requestorInfo)
    {
        TongApplicant data = new TongApplicant();
        data.dbId = (DBID_TYPE)requestorInfo["dbid"];
        data.id = (Int32)requestorInfo["id"];
        data.name = (string)requestorInfo["name"];
        data.level = (UInt16)requestorInfo["level"];
        data.feature = (UInt16)requestorInfo["feature"]; //特征值，包含性别、职业等属性

        _applyMembers.Add(data);

		EventManager.Invoke<object>(EventID.EVNT_TONG_APPLICANT_UPDATE, null);
		_avatar.statusMessage((UInt16)StatusID.TONG_RECEIVE_JOIN_TONG_REQUEST, data.name);
    }
    //同意玩家加入帮会申请
    public void AcceptJoinTongRequest(DBID_TYPE dbid)
    {
        _avatar.cellCall("acceptJoinTongRequest", new object[] { dbid });
        for (int i = 0; i < _applyMembers.Count; i++)
        {
            TongApplicant item = _applyMembers[i];
            if(item.dbId == dbid)
            {
                _applyMembers.Remove(item);
                EventManager.Invoke<object>(EventID.EVNT_TONG_APPLICANT_UPDATE, null);
                break;
            }
        }
    }
    //清除帮会申请列表
    public void ClearTongRequest()
    {
        _applyMembers.Clear();
        EventManager.Invoke<object>(EventID.EVNT_TONG_APPLICANT_UPDATE, null);
    }
    #endregion

    #region 帮会成员增加/离开
    public void AddTongMember(JSON_DICT_TYPE tongMemberInfo)
    {
        TongMemberData data = new TongMemberData();
        data.dbId = (DBID_TYPE)tongMemberInfo["dbid"];
        data.id = (Int32)tongMemberInfo["id"];
        data.name = (string)tongMemberInfo["name"];
        data.level = (UInt16)tongMemberInfo["level"];
        data.feature = (UInt16)tongMemberInfo["feature"]; //特征值，包含性别、职业等属性
        data.status = (Byte)tongMemberInfo["status"];		//状态，暂离，跟随，领导，离线
        data.duty = (Byte)tongMemberInfo["duty"];
        data.joinTime = (float)tongMemberInfo["joinTime"];
        data.logoutTime = (float)tongMemberInfo["logoutTime"];
        data.contribution = (UInt32)tongMemberInfo["contribution"];
        data.SetEntity(data.id);
        _tongMembers[data.dbId] = data;

        EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
    }
    public void TongMemberQuit(DBID_TYPE dbId, Byte reason)
    {
        if (_tongMembers.ContainsKey(dbId))
        {
            _tongMembers.Remove(dbId);
        }

        switch (reason)
        {
            case Define.LEAVE_TONG_KICKOUT:
                break;
            case Define.LEAVE_TONG_INITIATIVE:
                break;
            case Define.LEAVE_TONG_DISBAND:
                break;
            default:
                break;
        }
        EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
    }
    #endregion

    //根据帮会ID申请加入帮会，当客户端可以得到帮会ID是可以用这个接口，例如在匹配系统中
    public void RequestJoinTong(Int32 tongId)
    {
        _avatar.cellCall("requestJoinTongId", new object[] { tongId });
    }

    //升为帮主
    public void PromotedCaptain(DBID_TYPE dbid)
    {
        _avatar.cellCall("transferTongCaptainTo", dbid);
    }

    public void CaptainChanged(DBID_TYPE captainDbid)
    {
        TongMemberData oldCaptain = null;
        TongMemberData newCaptain = null;

        _tongMembers.TryGetValue(CaptainDbid, out oldCaptain);
        _tongMembers.TryGetValue(captainDbid, out newCaptain);

        //设置新帮主
        CaptainDbid = captainDbid;

        if (oldCaptain != null)
        {
            oldCaptain.duty = Define.TONG_DUTY_MEMBER;
        }

        if (newCaptain != null)
        {
            newCaptain.duty = Define.TONG_DUTY_CAPTAIN;
            if (newCaptain.dbId == _avatar.DatabaseID)
            {
                TipManager.instance.ShowTextTip((int)StatusID.TONG_CAPTAIN_CHANGED, "你");
            }
            else
            {
                TipManager.instance.ShowTextTip((int)StatusID.TONG_CAPTAIN_CHANGED, newCaptain.name);
            }
        }
        EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
    }
    //请离帮会
    public void KickTong(DBID_TYPE tongMemberDbid)
    {
        _avatar.cellCall("kickoutTongMember", new object[] { tongMemberDbid });
    }

    #region 帮会成员数据更新
    //更新队员等级
    public void UpdateTongMemberLevel(DBID_TYPE dbId, UInt16 level)
    {
        TongMemberData member = null;
        _tongMembers.TryGetValue(dbId, out member);

        if (member != null)
        {
            member.level = level;
            EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
        }
        else
        {
            Printer.LogError("Member of dbid {0} is not exist.", dbId);
        }
    }
    //更新队员状态
    public void UpdateTongMemberState(DBID_TYPE dbId, Byte status)
    {
        TongMemberData member = null;
        _tongMembers.TryGetValue(dbId, out member);

        if (member != null)
        {
            member.status = status;
            EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
        }
        else
        {
            Printer.LogError("Member of dbid {0} is not exist.", dbId);
        }
    }

    //职务改变
    public void OnCommittedTongDuty(DBID_TYPE operatorDbid, DBID_TYPE targetDbid, Byte duty)
    {
        if (!_tongMembers.ContainsKey(operatorDbid))
        {
            Printer.LogError("Operator of dbid {0} dosen't exist.", operatorDbid);
            return;
        }

        if (_tongMembers.ContainsKey(targetDbid))
        {
            _tongMembers[targetDbid].duty = duty;
            EventManager.Invoke<object>(EventID.EVNT_TONG_MEMBER_UPDATE, null);
            _avatar.statusMessage((UInt16)StatusID.TONG_EVENT_COMMIT_DUTY, " ," + _tongMembers[operatorDbid].name +
                "," + _tongMembers[targetDbid].name + "," + duty.ToString());
        }
        else
        {
            Printer.LogError("Member of dbid {0} is not exist.", targetDbid);
        }
    }
    #endregion

    /// <summary>
    /// 根据DBID获得成员数据
    /// </summary>
    /// <param name="dbid"></param>
    /// <returns></returns>
    public TongMemberData GetMemberByDBID(DBID_TYPE dbid)
    {
        TongMemberData member = null;
        _tongMembers.TryGetValue(dbid, out member);
        return member;
    }

    public bool IsCaptain()
    {
        return CaptainDbid == _avatar.DatabaseID;
    }

    public bool IsMember()
    {
        return IsInTong() && !IsCaptain();
    }

    public bool IsInTong()
    {
        return !string.IsNullOrEmpty(TongName);
    }

    public TongMemberData AvatarTongInfo
    {
        get { return GetMemberByDBID(_avatar.DatabaseID); } 
    }

    #region duty
    public Byte Duty
    {
        get {
            return GetMemberByDBID(_avatar.DatabaseID).duty;
        }
    }

    /// <summary>
    /// 统计职务已有数量
    /// </summary>
    /// <param name="duty"></param>
    /// <returns></returns>
    public int CountDuty(Byte duty)
    {
        int count = 0;
        foreach (var m in _tongMembers.Values)
        {
            if (m.duty == duty)
                count++;
        }
        return count;
    }

    /// <summary>
    /// 根据自己的职务，获取可以对某个职务设置的所有职务
    /// </summary>
    /// <param name="duty"></param>
    /// <returns></returns>
    public List<Byte> OperationalDuties(Byte duty)
    {
        List<Byte> duties;
        //检查我是否可以设置这个职务
        if (Array.Exists<Byte>(Define.TONG_DUTY_PRIVILEGES[Duty], x => x == duty))
        {
            //如果可以就显示除了这个职务之外，我所有可以设置的职务
            duties = new List<Byte>(Define.TONG_DUTY_PRIVILEGES[Duty]);
            duties.Remove(duty);
        }
        else
        {
            duties = new List<Byte>();
        }
        return duties;
    }

    /// <summary>
    /// 任命帮会成员
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duty"></param>
    public void CommitDuty(DBID_TYPE target, Byte duty)
    {
        _avatar.baseCall("commitTongDuty", target, duty);
    }

    /// <summary>
    /// 逐出帮会
    /// </summary>
    /// <param name="target"></param>
    public void Kickout(DBID_TYPE target)
    {
        _avatar.cellCall("kickoutTongMember", target);
    }

    /// <summary>
    /// 离开帮会
    /// </summary>
    public void LeaveTong()
    {
        _avatar.cellCall("leaveTong");
    }
    #endregion //duty
}
