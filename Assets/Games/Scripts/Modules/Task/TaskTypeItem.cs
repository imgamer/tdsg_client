﻿using UnityEngine;
using System.Collections;

public class TaskTypeItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
    }
}
