﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Quest;

public class TaskModule
{
    private KBEngine.Avatar _avatar;
    //已经提交的任务ID记录
    private QuestLogger _questLogger = new QuestLogger();
    //已接任务实例（包括正在做的，已经完成但未提交的）
    private Dictionary<uint, QuestBase> _questDict = new Dictionary<uint, QuestBase>();
    //界面显示任务组优先级
    private readonly byte[] DefaultPrioritys = new byte[] { Define.QUEST_GROUP_GENERAL, Define.QUEST_GROUP_ACTIVITY, Define.QUEST_GROUP_MAINLINE, Define.QUEST_GROUP_BRANCH };
    //界面显示任务组(分组，分组列表)
    private Dictionary<byte, List<QuestBase>> _groupDict = new Dictionary<byte, List<QuestBase>>();
    public TaskModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
        InitGroupDict();
    }
    //初始化分组列表
    private void InitGroupDict()
    {
        _groupDict.Clear();
        for (int i = 0; i < DefaultPrioritys.Length; i++)
        {
            Byte groupId = DefaultPrioritys[i];
            _groupDict[groupId] = new List<QuestBase>();
        }
    }
    //获取界面所有的任务组(分组，分组列表)
    public Dictionary<byte, List<QuestBase>> GetAllQeusts()
    {
        return _groupDict;
    }
    //获取界面显示有效的任务组(分组，分组列表)
    public Dictionary<byte, List<QuestBase>> GetValidGroups()
    {
        Dictionary<byte, List<QuestBase>> groups = new Dictionary<byte, List<QuestBase>>();
        for (int i = 0; i < DefaultPrioritys.Length; i++)
        {
            Byte groupId = DefaultPrioritys[i];
            List<QuestBase> list;
            if (_groupDict.TryGetValue(groupId, out list))
            {
                if (list != null && list.Count > 0)
                {
                    groups[groupId] = list;
                }
            }
        }
        return groups;
    }
    //获取界面默认显示组的任务表
    public Byte GetDefaultGroup(out List<QuestBase> list)
    {
        for (int i = 0; i < DefaultPrioritys.Length; i++)
        {
            Byte groupId = DefaultPrioritys[i];
            if (_groupDict.TryGetValue(groupId, out list))
            {
                if (list != null && list.Count > 0) return groupId;
            }
        }
        list = null;
        return Define.QUEST_GROUP_MAINLINE;
    }
    //获取所有任务列表
    private List<QuestBase> _questList = new List<QuestBase>();
    public List<QuestBase> GetAllQuests()
    {
        _questList.Clear();
        for (int i = 0; i < DefaultPrioritys.Length; i++)
        {
            Byte groupId = DefaultPrioritys[i];
            List<QuestBase> list;
            if (_groupDict.TryGetValue(groupId, out list))
            {
                if (list != null && list.Count > 0)
                {
                    _questList.AddRange(list);
                }
            }
        }
        return _questList;
    }
    public QuestBase GetQuestByID(uint id)
    {
        QuestBase quest = null;
        _questDict.TryGetValue(id, out quest);
        return quest;
    }

    public bool HaveQuest(uint id)
    {
        return _questDict.ContainsKey(id);
    }

    public bool QuestIsDone(uint id)
    {
        QuestBase quest = GetQuestByID(id);
        if (quest == null)
        {
            return false;
        }
        else
        {
            return quest.IsDone();
        }
    }

    public bool QuestFinished(uint id)
    {
        return _questLogger.Query(id) != 0;
    }

    private bool AddQuest(QuestBase quest)
    {
        if (quest == null) return false;
        if (_questDict.ContainsKey(quest.ID)) return false;
        _questDict[quest.ID] = quest;

        Byte groupId = GetGroupId(quest.Type);
        List<QuestBase> list;
        if (!_groupDict.TryGetValue(groupId, out list))
        {
            list = new List<QuestBase>();
            _groupDict[groupId] = list;
        }
        list.Insert(0, quest);
        list.Sort(GetCollation(groupId));
        return true;
    }
    private Comparison<QuestBase> GetCollation(Byte groupId)
    {
        switch (groupId)
        {
            case Define.QUEST_GROUP_GENERAL:
                return (QuestBase a, QuestBase b) => { return 0; };
            case Define.QUEST_GROUP_ACTIVITY:
                return (QuestBase a, QuestBase b) => { return 0; };
            case Define.QUEST_GROUP_MAINLINE:
                return (QuestBase a, QuestBase b) => { return a.ID.CompareTo(b.ID); };
            case Define.QUEST_GROUP_BRANCH:
                return (QuestBase a, QuestBase b) => { return a.ID.CompareTo(b.ID); };
        }
        return (QuestBase a, QuestBase b) => { return 0; };
    }

    private bool RemoveQuest(uint id)
    {
        QuestBase quest;
        if (_questDict.TryGetValue(id, out quest))
        {
            Byte groupId = GetGroupId(quest.Type);
            List<QuestBase> list;
            if (_groupDict.TryGetValue(groupId, out list))
            {
                list.Remove(quest);
            }
        }
        return _questDict.Remove(id);
    }

    public void AcceptQuest(uint id)
    {
        QuestBase questBase = QuestLoader.NewQuest(id);
        AddQuest(questBase);
        questBase.AcceptedCurTask();
        EventManager.Invoke<object>(EventID.EVNT_UPDATE_QUEST, null);
    }

    public void AddQuest(Dictionary<string, object> questData)
    {
        QuestBase questBase = QuestLoader.NewQuest(questData);
        AddQuest(questBase);
        EventManager.Invoke<object>(EventID.EVNT_UPDATE_QUEST, null);
    }

    public void RecvQuestLog(Dictionary<string, object> questLogData)
    {
        _questLogger.InitFromStream(questLogData);
    }

    public void RequestToAbandonQuest(uint id)
    {
        _avatar.cellCall("requestToAbandonQuest", new object[] { id });
    }
    public void AbandonQuest(uint id)
    {
        QuestBase quest = GetQuestByID(id);
        if (quest != null)
        {
            quest.AbandonTasks();
        }
        if (RemoveQuest(id))
        {
            EventManager.Invoke<uint>(EventID.EVNT_REMOVE_QUEST, id);
        }
    }

    public void CommitQuest(uint id)
    {
        if (RemoveQuest(id))
        {
            _questLogger.Log(id);
            EventManager.Invoke<uint>(EventID.EVNT_REMOVE_QUEST, id);
        }
    }

    public void UpdateQuestTask(uint questID, ushort taskID, Int32 progress)
    {
        QuestBase quest = GetQuestByID(questID);
        if (quest != null)
        {
            quest.UpdateTask(taskID, progress);
            EventManager.Invoke<object>(EventID.EVNT_UPDATE_QUEST, null);
        }
    }

    public void RunToNpcTalk(Vector2 dest, UInt32 UType)
    {
        SceneEntity player = _avatar.SceneEntityObj;
        if (player != null)
        {
            player.Navigate.Start(BaseNavigate.NavigateType.Initiative, dest, 1f,
            () =>
            {
                SceneEntity npcEntity = SceneManager.instance.CurrentScene.GetEntityByUType(UType);
                _avatar.talkModule.TalkTo(npcEntity);
            });
        }
    }

    /// <summary>
    /// 提示猎魂任务将获得较少奖励
    /// </summary>
    public void WarnOfDecadentLiehunReward()
    {
        TipManager.instance.ShowTwoButtonTip(3805, () =>
        {
            _avatar.cellCall("requestToAcceptLiehunQuest");
            return true;
        }, null);
    }

    /// <summary>
    /// 提示师门任务将获得较少奖励
    /// </summary>
    public void WarnOfDecadentShimenReward()
    {
        TipManager.instance.ShowTwoButtonTip(3203, () =>           
            {
                _avatar.cellCall("requestToAcceptShimenQuest");
                return true;
            }, null);
    }

    private uint _ringIndex = 0;
    public void ShimenRingIndex(object old)
    {
        _ringIndex = (uint)_avatar.getDefinedPropterty("shimenRingIndex");
        EventManager.Invoke<object>(EventID.EVNT_UPDATE_QUEST, null);
    }

    public string GetShimenFullTaskName(string name)
    {
        if(_ringIndex == 0)
        {
            _ringIndex = (uint)_avatar.getDefinedPropterty("shimenRingIndex");
        }
        uint index = _ringIndex % Define.SHIMEN_ROUND_AMOUNT_PER_RING;
        if (index == 0)
        {
            index = Define.SHIMEN_ROUND_AMOUNT_PER_RING;
        }
        return string.Format("{0}({1}/{2})", name, index, Define.SHIMEN_ROUND_AMOUNT_PER_RING);
    }

    public void ShimenDailyCounter(object index)
    {
        //getDefinedPropterty("shimenDailyCounter");
    }

    public QuestBase ShimenQuest 
    { 
        get 
        {
            foreach (var k in _questDict.Values)
            {
                if (k.Type == Define.QUEST_TYPE_SHIMEN)
                    return k;
            }
            return null;
        } 
    }

    public static Byte GetGroupId(Byte type)
    {
        return QuestsGroupConfig.SharedInstance.QuestTypeToGroup(type);
    }
    public static string GetGroupName(Byte groupId)
    {
        switch (groupId)
        {
            case Define.QUEST_GROUP_GENERAL:
                return "常规任务";
            case Define.QUEST_GROUP_MAINLINE:
                return "主线剧情";
            case Define.QUEST_GROUP_BRANCH:
                return "支线剧情";
            case Define.QUEST_GROUP_ACTIVITY:
                return "活动任务";
            default:
                break;
        }
        return "未定义";
    }
    public static string GetTypeName(Byte type)
    {
        switch (type)
        {
            case Define.QUEST_TYPE_MAILLINE:
                return "主线任务";
            case Define.QUEST_TYPE_BRANCH:
                return "支线任务";
            case Define.QUEST_TYPE_DAILY:
                return "日常任务";
            case Define.QUEST_TYPE_SHIMEN:
                return "师门任务";
            case Define.QUEST_TYPE_CHAIN:
                return "任务链";
            case Define.QUEST_TYPE_TREASUREMAP:
                return "宝图任务";
            case Define.QUEST_TYPE_ACTIVITY_PATA:
                return "爬塔活动";
            case Define.QUEST_TYPE_ACTIVITY_MOJIN:
                return "摸金活动";
            case Define.QUEST_TYPE_ACTIVITY_LIEHUN:
                return "猎魂活动";
            default:
                break;
        }
        return "未定义";
    }
}
