﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 奖励物品
/// </summary>
public class AwardItem : WinItem
{
    private UISprite _quality;
    public string Quality
    {
        set
        {
            _quality.spriteName = value;
        }
    }

    private UISprite _goods;
    public string Goods
    {
        set
        {
            ParentUI.SetDynamicSpriteName(_goods, value);
        }
    }
    private UILabel _count;
    public int Count
    {
        set
        {
            _count.text = value.ToString();
        }
    }

    protected override void OnInit()
    {
        _quality = transform.Find("Quality").GetComponent<UISprite>();
        _goods = transform.Find("Goods").GetComponent<UISprite>();
        _count = transform.Find("Count").GetComponent<UILabel>();
    }
}
