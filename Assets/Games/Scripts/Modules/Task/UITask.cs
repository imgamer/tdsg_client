﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 任务界面
/// </summary>
public class UITask : UIWin
{
    protected override void OnInit()
    {
        EventManager.AddListener<object>(EventID.EVNT_UPDATE_QUEST, UpdateTask);
        EventManager.AddListener<uint>(EventID.EVNT_REMOVE_QUEST, RemoveTask);
        InitItem();
        InitTab();
    }

    protected override void OnOpen(params object[] args)
    {
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {
        RefreshTask();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_UPDATE_QUEST, UpdateTask);
        EventManager.RemoveListener<uint>(EventID.EVNT_REMOVE_QUEST, RemoveTask);
    }

    #region 初始化
    private UI.Table _uiTable;
    private TaskGroupItem _template;
    private UISprite _selectItem;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        Transform table = center.Find("Menu/UITable");
        _uiTable = table.GetComponent<UI.Table>();
        _template = _uiTable.transform.Find("Item").GetComponent<TaskGroupItem>();
        _template.Init(this);
        _template.gameObject.SetActive(false);
        _selectItem = center.Find("Menu/SelectItem").GetComponent<UISprite>();
        _selectItem.enabled = false;
    }
    private WinPage _curContent;
    private UI.Tab _uiTab;
    private void InitTab()
    {
        _uiTab = transform.GetComponent<UI.Tab>();
        _uiTab.Init();
        _uiTab.OnSelect = (page, index) =>
        {
            _curContent = page.GetComponent<WinPage>();
            RefreshTask();
            DefaultSelect();
        };
    }
    #endregion

    private void UpdateTask(object o)
    {
        if (!Active) return;
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
        RefreshTask();
    }

    private void RemoveTask(uint o)
    {
        if (!Active) return;
        _uiTab.SwitchTo(_uiTab.CurTabIndex);
        RefreshTask();
        DefaultSelect();
    }

    private List<TaskGroupItem> _taskItems = new List<TaskGroupItem>();
    private void RefreshTask()
    {
        Dictionary<Byte, List<Quest.QuestBase>> dataDic = GameMain.Player.taskModule.GetValidGroups();
        List<Byte> groupList = new List<Byte>(dataDic.Keys);
        int dataNum = dataDic.Count;
        int itemNum = _taskItems.Count;

        Transform root = _curContent.transform.Find("Root");
        root.gameObject.SetActive(dataNum != 0);

        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                Byte groupId = groupList[i];
                if (i < itemNum)
                {
                    TaskGroupItem item = _taskItems[i];

                    item.GroupID = groupId;
                    item.Name = GetGroupName(groupId);
                    item.gameObject.SetActive(true);
                    SetData(item, dataDic[groupId]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    TaskGroupItem item = Instantiate(_template) as TaskGroupItem;
                    _template.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiTable.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(this);
                    _taskItems.Add(item);

                    item.GroupID = groupId;
                    item.Name = GetGroupName(groupId);
                    SetData(item, dataDic[groupId]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                TaskGroupItem item = _taskItems[i];
                if (i < dataNum)
                {
                    Byte groupId = groupList[i];

                    item.GroupID = groupId;
                    item.Name = GetGroupName(groupId);
                    item.gameObject.SetActive(true);
                    SetData(item, dataDic[groupId]);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        SwitchState(item);
                    });
                }
                else
                {
                    item.GroupID = 0;
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiTable.Reposition();
    }
    private string GetGroupName(Byte groupId)
    {
        return TaskModule.GetGroupName(groupId);
    }
    private void SetData(TaskGroupItem groupItem, List<Quest.QuestBase> dataList)
    {
        int dataNum = dataList.Count;
        groupItem.DataNum = dataNum;
        UIGrid uiGrid = groupItem.Grid;
        TaskTypeItem template = groupItem.Template;
        List<TaskTypeItem> items = groupItem.Items;
        int itemNum = items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                Quest.QuestBase data = dataList[i];
                if (i < itemNum)
                {
                    TaskTypeItem item = items[i];
                    item.gameObject.SetActive(true);
                    item.Name = TaskModule.GetTypeName(data.Type);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        Select(item, data);
                    });
                    //TODO
                }
                else
                {
                    template.gameObject.SetActive(true);
                    TaskTypeItem item = Instantiate(template) as TaskTypeItem;
                    template.gameObject.SetActive(false);
                    item.transform.parent = uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(this);
                    items.Add(item);
                    item.Name = TaskModule.GetTypeName(data.Type);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        Select(item, data);
                    });
                    //TODO
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                TaskTypeItem item = items[i];
                if (i < dataNum)
                {
                    Quest.QuestBase data = dataList[i];
                    item.gameObject.SetActive(true);
                    item.Name = TaskModule.GetTypeName(data.Type);
                    InputManager.instance.AddClickListener(item.gameObject, (go) =>
                    {
                        Select(item, data);
                    });
                    //TODO
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        NGUITools.SetActive(uiGrid.gameObject, groupItem.OpenState);
        if (groupItem.OpenState) uiGrid.Reposition();
    }
    #region 选择状态相关
    private void SetState(bool openState)
    {
        foreach (var item in _taskItems)
        {
            item.SetState(openState);
        }
        _uiTable.Reposition();
    }
    private void SwitchState(TaskGroupItem groupItem)
    {
        groupItem.SwitchState();
        _uiTable.Reposition();
    }
    private void Select(TaskTypeItem item, Quest.QuestBase data)
    {
        _selectItem.enabled = true;
        _selectItem.transform.parent = item.transform;
        _selectItem.transform.localPosition = Vector3.zero;
        _selectItem.transform.localEulerAngles = Vector3.zero;
        _selectItem.transform.localScale = Vector3.one;
        _curContent.Open(this, data);
    }
    private void CancelSelect()
    {
        _selectItem.enabled = false;
    }
    private void DefaultSelect()
    {
        List<Quest.QuestBase> list;
        Byte groupId = GameMain.Player.taskModule.GetDefaultGroup(out list);
        for (int i = 0; i < _taskItems.Count; i++)
        {
            TaskGroupItem item = _taskItems[i];
            if (list != null && groupId == item.GroupID)
            {
                item.SetState(true);
                Select(item.Items[0], list[0]);
            }
            else
            {
                item.SetState(false);
            }
        }
        _uiTable.Reposition();
    }
    #endregion


}
