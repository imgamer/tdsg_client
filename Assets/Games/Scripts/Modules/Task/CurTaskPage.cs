﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 当前任务
/// </summary>
public class CurTaskPage : WinPage
{
    protected override void OnInit()
    {
        InitItem();
        InitContent();
    }
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        CurQuestBase = (Quest.QuestBase)args[0];
    }

    protected override void OnRefresh()
    {
        if (CurQuestBase == null) return;
        UpdateContent();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
    private void InitItem()
    {
        Transform root = transform.Find("Root");
        GameObject btnAbandon = root.Find("BtnAbandon").gameObject;
        GameObject btnTransmit = root.Find("BtnTransmit").gameObject;
        InputManager.instance.AddClickListener(btnAbandon, (go) =>
        {
            if (CurQuestBase == null) return;
            GameMain.Player.taskModule.RequestToAbandonQuest(CurQuestBase.ID);
        });
        InputManager.instance.AddClickListener(btnTransmit, (go) =>
        {
            if (CurQuestBase == null) return;
            UIManager.instance.CloseWindow(WinID.UITask);
            CurQuestBase.ExecuteCurTask();
        });
    }
    private UILabel _name;
    private UILabel _tips;
    private UILabel _desc;
    private UI.Grid _uiGrid;
    private GameObject _awardRoot;
    private void InitContent()
    {
        Transform content = transform.Find("Root/Content");
        _name = content.Find("Name").GetComponent<UILabel>();
        _tips = content.Find("Tips").GetComponent<UILabel>();
        _desc = content.Find("Desc/Desc").GetComponent<UILabel>();
        _awardRoot = content.Find("Award").gameObject;
        _uiGrid = content.Find("Award/UIGrid").GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(_uiGrid.transform, 6);
        for (int i = 0; i < 6; i++)
        {
            Transform item = _uiGrid.transform.GetChild(i);
            item.name = i.ToString();
            AwardItem awardItem = item.GetComponent<AwardItem>();
            awardItem.Init(ParentUI);
            _awardItems.Add(awardItem);
        }
    }
    public Quest.QuestBase CurQuestBase { get; private set; }
    private void UpdateContent()
    {
        _name.text = CurQuestBase.GetCurTaskName();
        _tips.text = CurQuestBase.GetCurTaskDesc();
        _desc.text = CurQuestBase.Description;
        if (CurQuestBase.Rewards == null || CurQuestBase.Rewards.Length <= 0)
        {
            _awardRoot.SetActive(false);
        }
        else
        {
            _awardRoot.SetActive(true);
            SetAward(CurQuestBase.Rewards);
        }
    }
    private List<AwardItem> _awardItems = new List<AwardItem>();
    private void SetAward(Quest.QuestReward[] datas)
    {
        int dataNum = datas.Length;
        int itemNum = _awardItems.Count;
        for (int i = 0; i < itemNum; i++)
        {
            AwardItem item = _awardItems[i];
            if (i < dataNum)
            {
                Quest.QuestReward data = datas[i];
                item.gameObject.SetActive(true);
                item.Goods = data.Icon;
                item.Count = data.Number;
                ItemDetailManager.instance.AddListenerItemDetail(item.gameObject, AnchorType.Adjust, SourcePage.Other, data);
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
        _uiGrid.Reposition();
    }

}
