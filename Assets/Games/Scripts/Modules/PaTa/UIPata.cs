﻿using UnityEngine;
using System.Collections;

public class UIPata : UIWin
{
    protected override void OnInit()
    {
        Initpage();
    }

    protected override void OnOpen(params object[] args)
    {

    }
    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void Initpage()
    {
        Transform topleft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopLeft);
        GameObject exchangBtn = topleft.Find("BtnExchange").gameObject;
        InputManager.instance.AddClickListener(exchangBtn, (go) =>
        {
            
        });

        GameObject helpBtn = topleft.Find("BtnHelp").gameObject;
        InputManager.instance.AddClickListener(helpBtn, (go) =>
        {

        });

        Transform bottomleft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomLeft);
        GameObject leaveBtn = bottomleft.Find("BtnLeave").gameObject;
        InputManager.instance.AddClickListener(leaveBtn, (go) =>
        {
            GameMain.Player.cellCall("requestLeavePata");
        });

        GameObject resetBtn = bottomleft.Find("BtnReset").gameObject;
        InputManager.instance.AddClickListener(resetBtn, (go) =>
        {
            GameMain.Player.cellCall("requestToResetPataQuest");
        });
    }
}
