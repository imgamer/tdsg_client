//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;

namespace Quest
{
	public class QuestShimen : QuestNormal
	{
        public QuestShimen()
            : base()
		{
		}

        //师门任务不显示奖励
        private QuestReward[] emptyReward = new QuestReward[0];
        public override QuestReward[] Rewards
        {
            get
            {
                return emptyReward;
            }
        }

        public override string GetCurTaskName()
        {
            return GameMain.Player.taskModule.GetShimenFullTaskName(base.GetCurTaskName());
        }
	}
}

