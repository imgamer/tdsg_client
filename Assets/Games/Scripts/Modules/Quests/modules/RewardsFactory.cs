//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace Quest
{
	/// <summary>
	/// 负责创建任务奖励实例
	/// </summary>
	public static class RewardsFactory
	{
		private static Dictionary<string, Type> _mapper;
		
		static RewardsFactory()
		{
			_mapper = new Dictionary<string, Type>();
			_mapper["QuestRewardGold"] = typeof(QuestRewardGold);	//奖励金币
			_mapper["QuestRewardExp"] = typeof(QuestRewardExp);		//奖励经验
            _mapper["QuestRewardExpPet"] = typeof(QuestRewardExpPet);   //奖励宠物经验
            _mapper["QuestRewardStamina"] = typeof(QuestRewardStamina);	//奖励体力值
			_mapper["QuestRewardItem"] = typeof(QuestRewardItem);	//奖励一个道具
			_mapper["QuestRewardIngot"] = typeof(QuestRewardIngot);	//奖励元宝
            _mapper["QuestRewardPataBox"] = typeof(QuestRewardPataBox);	//奖励一个爬塔宝箱
            _mapper["QuestRewardLiehun"] = typeof(QuestRewardLiehun);	//奖励一个猎魂宝箱
		}
		
		public static QuestReward NewReward(RewardData data)
		{
			Type RewardType;
			if(_mapper.TryGetValue(data.script, out RewardType))
			{
				QuestReward reward = (QuestReward)Activator.CreateInstance(RewardType);
				reward.InitFromConfig(data);
				return reward;
			}
			else
			{
                Printer.LogError("Reward " + data.script + " not found.");
				return null;
			}
		}
	}
}

