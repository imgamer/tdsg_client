﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 上交物品Item
/// </summary>
public class SubmitItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            ParentUI.SetDynamicSpriteName(_icon, value);
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
    }
}
