﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 上交宠物Item
/// </summary>
public class SubmitPetItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }
    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value;
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
    }
}
