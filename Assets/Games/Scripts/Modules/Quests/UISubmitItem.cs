﻿using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;
using Item;
using System;
/// <summary>
/// 师门任务——上交物品装备界面
/// </summary>
public class UISubmitItem : UIWin
{
    public Action<ItemBase> OnSubmit;
    protected override void OnInit()
    {
        InitItem();
    }

    private ITEM_ID _conditionID;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _conditionID = (ITEM_ID)(Int32)args[0];
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private Grid _uiGrid;
    private Transform _select;
    private SubmitItem _template;
    private List<SubmitItem> _items = new List<SubmitItem>();
    private void InitItem()
    {
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        Transform root = bottomRight.Find("Root");
        GameObject btnClose = root.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject btnSubmit = root.Find("BtnSubmit").gameObject;
        InputManager.instance.AddClickListener(btnSubmit, (go) =>
        {
            if (OnSubmit != null) OnSubmit(_selectData);
            UIManager.instance.CloseWindow(winID);
        });

        _select = root.Find("ScrollView/Select");
        Transform grid = root.Find("ScrollView/Grid");
        _template = grid.Find("Item").GetComponent<SubmitItem>();
        _template.gameObject.SetActive(false);
        _uiGrid = grid.GetComponent<Grid>();
    }

    private void UpdateItems()
    {
        List<ItemBase> dataList = GameMain.Player.kitbagModule.GetItemsByID(_conditionID);
        int dataNum = dataList.Count;
        if (dataNum <= 0) return;
        int itemNum = _items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                ItemBase data = dataList[i];
                if (i < itemNum)
                {
                    SubmitItem item = _items[i];
                    item.gameObject.SetActive(true);
                    SetData(item, data);
                }
                else
                {
                    _template.gameObject.SetActive(true);
                    SubmitItem item = Instantiate(_template) as SubmitItem;
                    _template.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(this);
                    _items.Add(item);
                    SetData(item, data);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                SubmitItem item = _items[i];
                if (i < dataNum)
                {
                    ItemBase data = dataList[i];
                    item.gameObject.SetActive(true);
                    SetData(item, data);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        SelectItem(_items[0], dataList[0]);
        _uiGrid.Reposition();
        _uiGrid.GoToItem(0, dataNum);
    }
    private void SetData(SubmitItem item, ItemBase data)
    {
        item.Icon = data.Icon;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SelectItem(item, data);
        });
    }
    private ItemBase _selectData = null;
    private void SelectItem(SubmitItem item, ItemBase data)
    {
        _selectData = data;
        if (item == null)
        {
            _select.gameObject.SetActive(false);
            return;
        }
        _select.gameObject.SetActive(true);
        _select.parent = item.transform;
        _select.localPosition = Vector3.zero;
        _select.localEulerAngles = Vector3.zero;
        _select.localScale = Vector3.one;
    }
}
