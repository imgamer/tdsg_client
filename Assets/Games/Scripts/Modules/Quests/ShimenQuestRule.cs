﻿using System;
using UnityEngine;

public static class ShimenQuestRule
{
    static ShimenQuestRule()
	{
	}

    /*****************************************************
	* 寻找物品任务
	*****************************************************/
    /// <summary>
    /// 根据道具和等级得出品质标准
    /// </summary>
    /// <param name="level"></param>
    /// <param name="itemID"></param>
    /// <returns></returns>
	public static int StandardItemQuality(int level, uint itemID)
    {
		// 具体规则候补
		return 30;
    }

    public static void GetShimenMaster(KBEngine.Avatar avatar, out short spaceUType, out int npcUType)
    {
        spaceUType = 1000;
        npcUType = 30009;
    }
}
