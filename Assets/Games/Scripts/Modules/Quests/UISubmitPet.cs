﻿using System;
using UnityEngine;
using System.Collections;
using UI;
using System.Collections.Generic;
/// <summary>
/// 师门任务——上交宠物界面
/// </summary>
public class UISubmitPet : UIWin
{
    public Action<PetInfo> OnSubmit;
    protected override void OnInit()
    {
        InitItem();
    }

    private uint _requirePetUType;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _requirePetUType = (uint)args[0];
        UpdateItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private SubmitPetItem _item;
    private const int Count = 7;
    private UILabel[] _values = new UILabel[Count];

    private Grid _uiGrid;
    private const int MaxNum = 8;
    private SubmitPetItem[] _items = new SubmitPetItem[MaxNum];
    private Transform _select;
    private void InitItem()
    {
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        Transform root = bottomRight.Find("Root");
        GameObject btnClose = root.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        GameObject btnSubmit = root.Find("BtnSubmit").gameObject;
        InputManager.instance.AddClickListener(btnSubmit, (go) =>
        {
            if (OnSubmit != null) OnSubmit(_selectData);
            UIManager.instance.CloseWindow(winID);
        });

        Transform detail = root.Find("Detail");
        _item = detail.Find("Item").GetComponent<SubmitPetItem>();
        _item.Init(this);
        for (int i = 0; i < Count; i++)
        {
            Transform child = detail.Find(string.Format("Grid/{0}", i));
            UILabel item = child.Find("Value").GetComponent<UILabel>();
            _values[i] = item;
        }

        _select = root.Find("ScrollView/Select");
        Transform grid = root.Find("ScrollView/Grid");
        _uiGrid = grid.GetComponent<Grid>();
        UITools.CopyFixNumChild(grid, MaxNum);
        for (int i = 0; i < MaxNum; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString();
            SubmitPetItem item = child.GetComponent<SubmitPetItem>();
            item.Init(this);
            _items[i] = item;
        }
        _uiGrid.Reposition();
    }
    
    private void UpdateItems()
    {
        List<PetInfo> pets = GameMain.Player.petModule.GetPetsOfUType(_requirePetUType);
        if (pets.Count <= 0) return;
        for (int i = 0; i < MaxNum; i++)
        {
            SubmitPetItem item = _items[i];
            if(i < pets.Count)
            {
                PetInfo data = pets[i];
                item.gameObject.SetActive(true);
                //item.Icon = data.
                item.Name = data.Name;
                item.Level = string.Format("{0}级", data.level);
                InputManager.instance.AddClickListener(item.gameObject, (go) => 
                {
                    SelectItem(item, data);
                });
            }
            else
            {
                item.gameObject.SetActive(false);
                InputManager.instance.RemoveClickListener(item.gameObject);
            }
        }
        SelectItem(_items[0], pets[0]);
        _uiGrid.GoToItem(0, pets.Count);
    }

    private PetInfo _selectData = null;
    private void SelectItem(SubmitPetItem item, PetInfo data)
    {
        if (item == null)
        {
            _select.gameObject.SetActive(false);
            return;
        }
        _selectData = data;
        _select.gameObject.SetActive(true);
        _select.parent = item.transform;
        _select.localPosition = Vector3.zero;
        _select.localEulerAngles = Vector3.zero;
        _select.localScale = Vector3.one;
        ShowDetail(data);
    }

    private void ShowDetail(PetInfo data)
    {
        if (data == null) return;
        //_item.Icon = data.
        _item.Name = data.Name;
        _item.Level = string.Format("{0}级", data.level);
        _values[0].text = string.Format("{0}", data.PhyAttack);
        _values[1].text = string.Format("{0}", data.PhyDefense);
        _values[2].text = string.Format("{0}", data.MagAttack);
        _values[3].text = string.Format("{0}", data.MagDefense);
        _values[4].text = string.Format("{0}", data.Hp);
        _values[5].text = string.Format("{0}", data.Speed);
        _values[6].text = string.Format("{0}", data.skills.Count);
    }
}
