﻿using System;
using System.Collections;
using ENTITY_UTYPE = System.UInt32;

public static class HeroRule {

	static HeroRule()
	{
    }

    private static double UpgradeByLevel(ushort level)
    {
        return (level - 1) * 0.05;
    }

    private static double CalcPropertyMax(int baseValue, ushort level)
    {
        return baseValue * (1 + level / 10);
    }

    public static int PropertyAdjustToLevel(ENTITY_UTYPE utype, string propertyName, ushort level)
    {
        return (int)(HerosConfig.SharedInstance.GetConfig<int>(utype, propertyName) * (1 + UpgradeByLevel(level)));
    }

    public static int PropertyMaxAtLevel(ENTITY_UTYPE utype, string propertyName, ushort level)
    {
        return (int)CalcPropertyMax(HerosConfig.SharedInstance.GetConfig<int>(utype, propertyName + "_Max"), level);
    }

    public static string HeroSchoolIcon(UInt32 school)
    {
        string str = string.Empty;
        switch(school)
        {
            case Define.ROLE_TYPE_DIANCANG:
                str = "UI-s2-common-menpaidiancangpai";
                break;
            case Define.ROLE_TYPE_JIUHUASHAN:
                str = "UI-s2-common-menpaijiuhuasan";
                break;
            case Define.ROLE_TYPE_TIANXINGE:
                str = "UI-s2-common-menpaitianxinge";
                break;
            case Define.ROLE_TYPE_XIAOYAOGONG:
                str = "UI-s2-common-menpaixiaoyaopai";
                break;
            case Define.ROLE_TYPE_ZIDIANMEN:
                str = "UI-s2-common-menpaizidianmen";
                break;
            case Define.ROLE_TYPE_XUANQINGGONG:
                str = "UI-s2-common-menpaixuanqinggong";
                break;
            default:
                break;
        }
        return str;
    }
}
