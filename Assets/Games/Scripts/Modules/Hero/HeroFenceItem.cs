﻿using UnityEngine;
using System.Collections;

public class HeroFenceItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = value;
        }
    }

    private UISprite _school;
    public string School
    {
        set
        {
            _school.spriteName = value;
        }
    }

    private const int Num = 3;
    private GameObject[] _states = new GameObject[Num];
    public void SetState(int index)
    {
        for (int i = 0; i < Num; i++)
        {
            _states[i].SetActive(i == index);
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _school = transform.Find("School").GetComponent<UISprite>();
        Transform state0 = transform.Find("State/0");
        _states[0] = state0.gameObject;
 
        Transform state1 = transform.Find("State/1");
        _states[1] = state1.gameObject;

        Transform state2 = transform.Find("State/2");
        _states[2] = state2.gameObject;
    }
}
