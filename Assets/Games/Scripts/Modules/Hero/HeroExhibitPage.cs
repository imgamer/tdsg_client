﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

using ENTITY_UTYPE = System.UInt32;

public class HeroExhibitPage : WinPage
{
    protected override void OnInit()
    {
        InitPage();
        InitAttributeItem();

        EventManager.AddListener<object>(EventID.EVNT_HERO_SELECT_UPDATE, UpdateHeroInfo);
        EventManager.AddListener<object>(EventID.EVNT_HERO_STATE_UPDATE, UpdateHeroInfo);
    }

    public ENTITY_UTYPE CurrentHero;
    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_HERO_SELECT_UPDATE, UpdateHeroInfo);
        EventManager.RemoveListener<object>(EventID.EVNT_HERO_STATE_UPDATE, UpdateHeroInfo);
    }

    private Transform  _modelRoot;
    private UILabel _name;
    private UILabel _level;
    private UILabel _friendShip;
    private UILabel _remainTrain;

    private UILabel _fightText;
    private GameObject _fightBtn;

    private UILabel _trainText;
    private GameObject _trainBtn;

    private GameObject _giftBtn;

    private const int SkillNum = 3;
    private UISprite[] _skills = new UISprite[SkillNum];
    private void InitPage()
    {
        _name = transform.Find("Name/Text").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _friendShip = transform.Find("FriendShip/Value").GetComponent<UILabel>();
        _remainTrain = transform.Find("RemainTrain/Value").GetComponent<UILabel>();

        _modelRoot = transform.Find("ModelRoot");

        for (int i = 0; i < SkillNum; i++)
        {
            UISprite sprite = transform.Find(string.Format("Skill/{0}/Icon", i)).GetComponent<UISprite>();
            _skills[i] = sprite;
        }
        _giftBtn = transform.Find("FriendShip/GiftBtn").gameObject;
        InputManager.instance.AddClickListener(_giftBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIHeroGift);
        });

        _fightBtn = transform.Find("FightBtn").gameObject;
        _fightText = transform.Find("FightBtn/Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_fightBtn, (go) =>
        {
            if(_fightText.text == "招    募")
            {
                GameMain.Player.heroModule.RequestToRecruitHero(CurrentHero);
            }
        });

        _trainBtn = transform.Find("TrainBtn").gameObject;
        _trainText = transform.Find("TrainBtn/Text").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(_trainBtn, (go) =>
        {
            GameMain.Player.heroModule.RequestEnhanceProperty(CurrentHero);
        });  
    }

    private const int AttributeNum = 5;
    private HeroAttributeItem[] _attributeItems = new HeroAttributeItem[AttributeNum];
    private void InitAttributeItem()
    {
        Transform grid = transform.Find("Attribute/Grid");
        for(int i =0; i < AttributeNum; i++)
        {
            HeroAttributeItem item = grid.Find(i.ToString()).GetComponent<HeroAttributeItem>();
            item.Init(ParentUI);
            _attributeItems[i] = item;
        }
    }

    private void RefreshHeroData(HerosData data)
    {
        _name.text = data.name;
        _level.text = string.Format("Lv.{0}", 1);
        _remainTrain.text = "0";
        _fightText.text = "招    募";
        _friendShip.text = "0";
         if (data.takeLevel > GameMain.Player.Level)
         {
             UITools.SetButtonState(_fightBtn,false);
         }
         else
         {
             if (data.convertCost == 0)
             {
                 UITools.SetButtonState(_fightBtn, true);
             }
             else
             {
                 if (data.convertCost < GameMain.Player.ArenaMedal)
                 {
                     UITools.SetButtonState(_fightBtn, true);
                 }
                 else
                 {
                     UITools.SetButtonState(_fightBtn, false);
                 }
             }           
         }
         UITools.SetButtonState(_trainBtn, false);
         _giftBtn.SetActive(false);
        List<int> skills = data.skills;
        for (int i = 0; i < SkillNum; i++)
        {
            _skills[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < skills.Count; i++)
        {
            JsonData skillData = SkillConfig.SharedInstance.GetJsonData(skills[i]);
            _skills[i].spriteName = (string)skillData["icon"];
            _skills[i].gameObject.SetActive(true);
        }
        
        SetEntity(data.modelID);
        _attributeItems[0].SetInit(data.physicsPower_Max, data.physicsPower, 0);
        _attributeItems[1].SetInit(data.magicPower_Max, data.magicPower, 0);
        _attributeItems[2].SetInit(data.physicsDefense_Max, data.physicsDefense, 0);
        _attributeItems[3].SetInit(data.magicDefense_Max, data.magicDefense, 0);
        _attributeItems[4].SetInit(data.HP_Max, data.HP, 0);
    }

    private void RefreshHeroInfo(HeroInfo info)
    {
        HerosData data = HerosConfig.SharedInstance.GetHerosData(info.Utype);
        if (data == null) return;
        _name.text = data.name;
        _level.text = string.Format("Lv.{0}", GameMain.Player.Level);
        _friendShip.text = info.Friendship.ToString();
        _giftBtn.SetActive(true);
        SetEntity(data.modelID);
        
        Int32[] skills = info.Skills;
        for (int i = 0; i < SkillNum; i++)
        {
            _skills[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < skills.Length; i++)
        {
            JsonData skillData = SkillConfig.SharedInstance.GetJsonData(skills[i]);
            _skills[i].spriteName = (string)skillData["Icon"];
        }     
        //判断当前是否参战 临时表现
        _fightText.text = "参    战";
        UITools.SetButtonState(_fightBtn, false);
        //修炼判断
        if(info.FreeToEnhance())
        {
            _trainText.text = "免费修炼";
            UITools.SetButtonState(_trainBtn, true);
        }
        else
        {
            _trainText.text = "修    炼";
            UITools.SetButtonState(_trainBtn, false);
            //还要根据当前点数对修炼按钮进行灰化处理
        }        
    }

     private void RefreshHeroAttribute(HeroInfo info)
    {
        HerosData data = HerosConfig.SharedInstance.GetHerosData(info.Utype);
        if (data == null) return;
        _attributeItems[0].SetInit(data.physicsPower_Max, data.physicsPower, info.PhysicsPower);
        _attributeItems[1].SetInit(data.magicPower_Max, data.magicPower, info.MagicPower);
        _attributeItems[2].SetInit(data.physicsDefense_Max, data.physicsDefense, info.PhysicsDefense);
        _attributeItems[3].SetInit(data.magicDefense_Max, data.magicDefense, info.MagicDefense);
        _attributeItems[4].SetInit(data.HP_Max, data.HP, info.Hp_Max);
    }

    private void SetEntity(string name)
    {
        BaseSpawnData data = new InstantiateData(name, name, PoolName.Model, PoolType.Loop, (o) =>
        {
            if (o)
            {
                ClearModel();
                o.transform.parent = _modelRoot;
                o.transform.localPosition = Vector3.zero;
                o.transform.localEulerAngles = Vector3.zero;
                o.transform.localScale = Vector3.one;
                Renderer r = o.GetComponent<Renderer>();
                r.sortingOrder = ParentUI.SortingOrder + 1;
            }
        });
        AssetsManager.instance.AsyncInstantiate(new BaseSpawnData[] { data });
    }

    private void ClearModel()
    {
        for (int i = 0; i < _modelRoot.childCount; i++)
        {
            Transform child = _modelRoot.GetChild(i);
            Renderer r = child.GetComponent<Renderer>();
            r.sortingOrder = Const.EntitySortingOrder;
            AssetsManager.instance.Despawn(child.gameObject);
        }
    }

    private void UpdateHeroInfo(object o)
    {
        if (!Active) return;
        if( CurrentHero != GameMain.Player.heroModule.SelectHero)
        {
            CurrentHero = GameMain.Player.heroModule.SelectHero;
            for (int i = 0; i < AttributeNum; i++)
            {
                _attributeItems[i].InitItemTempValue();
            }
        }
        if(GameMain.Player.heroModule.ContainHero(CurrentHero))
        {
            HeroInfo info = GameMain.Player.heroModule.GetHeroInfoByUType(CurrentHero);
            if (info == null) info = new HeroInfo(CurrentHero);
            RefreshHeroInfo(info);
            RefreshHeroAttribute(info);
        }
        else
        {
            HerosData data = HerosConfig.SharedInstance.GetHerosData(CurrentHero);
            if (data == null) return;
            RefreshHeroData(data);
        }
    }
}
