﻿using UnityEngine;
using System.Collections;

public class UIHero : UIWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {
        ExhibitPage.Open(this);
        FencePage.Open(this);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    public static WinPage FencePage { get; private set; }
    public static WinPage ExhibitPage { get; private set; }
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
        ExhibitPage = center.Find("Exhibit").GetComponent<WinPage>();
        FencePage = center.Find("Fence").GetComponent<WinPage>();        
    }
}
