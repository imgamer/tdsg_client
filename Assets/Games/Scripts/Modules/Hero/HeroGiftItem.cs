﻿using UnityEngine;
using System.Collections;

public class HeroGiftItem : WinItem
{
    private GameObject _prop;
    private UISprite _icon;
    private UILabel _count;

    private GameObject _empty;

    protected override void OnInit()
    {
        _prop = transform.Find("Prop").gameObject;
        _empty = transform.Find("Empty").gameObject;

        _icon = transform.Find("Prop/Icon").GetComponent<UISprite>();
        _count = transform.Find("Prop/Count").GetComponent<UILabel>();
    }
}
