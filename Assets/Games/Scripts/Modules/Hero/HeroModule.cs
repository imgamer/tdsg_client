﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using ENTITY_UTYPE = System.UInt32;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;
/// <summary>
/// 助战控制类
/// </summary>
public class HeroModule
{
    private KBEngine.Avatar _avatar;
    public HeroModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    #region 助战相关操作
    //拥有的助战列表
    private List<ENTITY_UTYPE> _heroUTypes = new List<ENTITY_UTYPE>();
    public List<ENTITY_UTYPE> GetAllHerosUType()
    {
        return _heroUTypes;
    }

    public bool ContainHero(ENTITY_UTYPE uType)
    {
        return _heroUTypes.Contains(uType);
    }

    //保存助战修炼数据
    private List<HeroInfo> _heros = new List<HeroInfo>();
    public List<HeroInfo> GetAllHeros()
    {
        return _heros;
    }

    public List<HeroInfo> GetHerosOfUType(uint utype)
    {
        return _heros.FindAll((p) =>
            {
                return p.Utype == utype;
            });
    }

    public HeroInfo GetHeroInfoByUType(ENTITY_UTYPE utype)
    {
        HeroInfo ret = _heros.Find((hero) => 
        {
            return hero.Utype == utype;
        });
        return ret;
    }

    public ENTITY_UTYPE SelectHero { get; private set; }
    public void SetSelectHero(ENTITY_UTYPE UType)
    {
        if (UType != SelectHero)
        {
            SelectHero = UType;
            EventManager.Invoke<object>(EventID.EVNT_HERO_SELECT_UPDATE, null);
        }
    }

    #endregion

    #region 助战网络请求
    //助战修炼
    public void RequestEnhanceProperty(ENTITY_UTYPE utype)
    {
        _avatar.cellCall("enhanceHeroProperty", utype);
    }

    /// <summary>
    /// 增加助战亲密度
    /// </summary>
    /// <param name="utype"></param>
    /// <param name="itemID"></param>
    public void RequestToAddHeroFriendship(ENTITY_UTYPE utype, int itemID)
    {
        _avatar.cellCall("addHeroFriendship", utype, ITEM_ID.Convert(itemID));
    }

    /// <summary>
    /// 招募助战
    /// </summary>
    /// <param name="utype">助战ID</param>
    /// <param name="groupIndex">所在组</param>
    public void RequestToRecruitHero(ENTITY_UTYPE utype)
    {
        _avatar.cellCall("recruitHero", utype);
    }
    #endregion

    #region 助战更新
    public void RecvRecruitHeroData(List<object> heroUType)
    {
        foreach (object obj in heroUType)
        {
            _heroUTypes.Add((ENTITY_UTYPE)obj);
        }
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

    public void RecvHeroData(Dictionary<string, object> heroData)
    {
        var hero = new HeroInfo(heroData);
        _heros.Add(hero);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

	public void UpdateHeroPropertyJson(ENTITY_UTYPE utype, string attrName, string json)
    {
        HeroInfo hero = GetHeroInfoByUType(utype);
        if (hero == null)
        {
            Printer.LogError("Can't find hero by utype {0}", utype);
            return; 
        }
        hero.updateFromJson(attrName, json);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

    public void UpdateHeroPropertyInt32(ENTITY_UTYPE utype, string attrName, Int32 value)
    {
        HeroInfo hero = GetHeroInfoByUType(utype);
        if (hero == null)
        {
            Printer.LogError("Can't find hero by utype {0}", utype);
            return;
        }
        hero.update(attrName, value);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

    public void UpdateHeroPropertyFloat(ENTITY_UTYPE utype, string attrName, float value)
    {
        HeroInfo hero = GetHeroInfoByUType(utype);
        if (hero == null)
        {
            Printer.LogError("Can't find hero by utype {0}", utype);
            return;
        }
        hero.update(attrName, value);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

    public void UpdateHeroPropertyString(ENTITY_UTYPE utype, string attrName, string value)
    {
        HeroInfo hero = GetHeroInfoByUType(utype);
        if (hero == null)
        {
            Printer.LogError("Can't find hero by utype {0}", utype);
            return;
        }
        hero.update(attrName, value);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }

    public void UpdateHeroPropertyListInt(ENTITY_UTYPE utype, string attrName, UInt16 index, Int32 newValue)
    {
        HeroInfo hero = GetHeroInfoByUType(utype);
        if (hero == null)
        {
            Printer.LogError("Can't find hero by utype {0}", utype);
            return;
        }
        hero.updateListInt(attrName, index, newValue);
        EventManager.Invoke<object>(EventID.EVNT_HERO_STATE_UPDATE, null);
    }
    #endregion

}
