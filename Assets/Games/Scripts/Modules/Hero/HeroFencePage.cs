﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using ENTITY_UTYPE = System.UInt32;

public class HeroFencePage : WinPage
{
    private bool _inited = false;
    protected override void OnInit()
    {
        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_HERO_STATE_UPDATE, UpdateItem);
    }

    protected override void OnOpen(params object[] args)
    {
        RefreshItems();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_HERO_STATE_UPDATE, UpdateItem);
    }

    
    private const int Num = 6; //临时只有6个
    private UI.Grid _uiGrid;
    private Transform _select;
    private HeroFenceItem[] items = new HeroFenceItem[Num];
    private void InitPage()
    {
        _select = transform.Find("Select");
        Transform grid = transform.Find("ScrollView/Grid");
        _uiGrid = grid.GetComponent<UI.Grid>();
        UITools.CopyFixNumChild(grid, Num);
        for (int i = 0; i < Num; i++)
        {
            Transform child = grid.GetChild(i);
            child.name = i.ToString("D2");
            HeroFenceItem item = child.GetComponent<HeroFenceItem>();
            item.Init(ParentUI);
            items[i] = item;
        }
        _uiGrid.Reposition();
    }

    private void RefreshItems()
    {
        List<HerosData> herolist = HerosConfig.SharedInstance.GetAllHerosData();
        for (int i = 0; i < Num; i++)
        {
            HeroFenceItem item = items[i];
            HerosData data = herolist[i];
            item.Name = data.name;
            item.Icon = "Assistant_004";
            item.School = HeroRule.HeroSchoolIcon(data.school);
            if (GameMain.Player.heroModule.ContainHero((ENTITY_UTYPE)data.utype))
            {
                item.Level = string.Format("{0}级", GameMain.Player.Level);
                //当前是否出战还无法判断
                item.SetState(0);               
            }
            else
            {
                if(data.takeLevel > GameMain.Player.Level)
                {
                    item.Level = string.Format("{0}级可招募", data.takeLevel);                   
                }
                else
                {
                    if (data.convertCost <= GameMain.Player.ArenaMedal)
                    {
                        item.Level = "可招募";
                    }
                    else
                    {
                        item.Level = "竞技币不足";
                    }
                }               
                item.SetState(2);
            }           
            InputManager.instance.AddClickListener(item.gameObject, (go) =>
            {
                SelectItem(go.transform, (ENTITY_UTYPE)data.utype);
            });

            if ((ENTITY_UTYPE)data.utype == GameMain.Player.heroModule.SelectHero)
            {
                SelectItem(item.transform, (ENTITY_UTYPE)data.utype);
            }

            //第一次打开
            if (i == 0 && !_inited)
            {
                SelectItem(item.transform, (ENTITY_UTYPE)data.utype);
                _inited = true;
            }
        }  

    }

    private void SelectItem(Transform item, ENTITY_UTYPE UType)
    {
        _select.parent = item;
        _select.localPosition = Vector3.zero;
        _select.localScale = Vector3.one;
        GameMain.Player.heroModule.SetSelectHero(UType);
    }

    private void UpdateItem(object o)
    {
        if (!Active) return;
        RefreshItems();
    }
}
