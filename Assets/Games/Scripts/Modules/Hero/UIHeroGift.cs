﻿using UnityEngine;
using System.Collections;

public class UIHeroGift : UIWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {
        
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UILabel _friendShip;
    private UILabel _addValue;
    private UIProgressBar _bar;

    private const int Num = 5;
    private HeroGiftItem[] _items = new HeroGiftItem[Num];

    private void InitPage() 
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject btnClose = center.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _icon = center.Find("Info/Icon").GetComponent<UISprite>();
        _friendShip = center.Find("Info/FriendShip/Value").GetComponent<UILabel>();
        _addValue = center.Find("Info/FriendShip/AddValue").GetComponent<UILabel>();
        _bar = center.Find("Info/Bar").GetComponent<UIProgressBar>();

        Transform grid = center.Find("Prop");
        for(int i= 0; i< Num; i++)
        {
            HeroGiftItem item = grid.Find(i.ToString()).GetComponent<HeroGiftItem>();
            item.Init(this);
            _items[i] = item;
        }
    }
}
