﻿using UnityEngine;
using System.Collections;
using System;

public class HeroAttributeItem : WinItem
{
    private UILabel _maxValue;
    private UILabel _baseValue;

    private UILabel _addValue;

    private GameObject _tempAdd;
    private UILabel _tempAddValue;

    private UI.ProgressBar _bar;
    private UISprite _original;
    private UISprite _foreground;
    private int _totalSize;

    private int _oldAddValue;
    private bool _train;
    public void InitItemTempValue()
    {
        _oldAddValue = 0;
        _train = false;
    }

    //total, base, add
    public void SetInit(int totalValue, int originalValue, int fixedValue)
    {
        int _relLength = totalValue - originalValue;
        if (_relLength <= 0) _relLength = 1;
        _original.width = (int)(_totalSize * originalValue / (float)totalValue);
        _foreground.width = _totalSize - _original.width;

        _bar.value = 1;//修复不刷新问题
        _bar.value = fixedValue / (float)(_relLength);

        _maxValue.text = totalValue.ToString();
        _baseValue.text = originalValue.ToString();
        if (fixedValue == 0)
        {
            _addValue.enabled = false;
            _tempAdd.SetActive(false);
            _oldAddValue = 0;
        }
        else
        {
            _addValue.enabled = true;
            //防止第一次打开会显示临时增加数值
            if(_train)
            {
                if (/*_oldAddValue > 0 &&*/ fixedValue - _oldAddValue > 0)
                {
                    _tempAdd.SetActive(true);
                    _tempAddValue.text = Convert.ToString(fixedValue - _oldAddValue);
                }
            }           
            _oldAddValue = fixedValue;
            _addValue.text = string.Format("+{0}", fixedValue);           
        }
        _train = true;
    }

    protected override void OnInit()
    {
        Transform bar = transform.Find("Bar");
        _bar = bar.GetComponent<UI.ProgressBar>();
        _original = bar.Find("Original").GetComponent<UISprite>();
        _foreground = bar.Find("Foreground").GetComponent<UISprite>();
        UISprite background = bar.Find("Background").GetComponent<UISprite>();
        _totalSize = background.width;

        _maxValue = transform.Find("MaxValue").GetComponent<UILabel>();
        _baseValue = transform.Find("BaseValue").GetComponent<UILabel>();
        _addValue = transform.Find("AddValue").GetComponent<UILabel>();
        _tempAdd = transform.Find("TempAdd").gameObject;
        _tempAddValue = transform.Find("TempAdd/Value").GetComponent<UILabel>();
        _tempAdd.SetActive(false);
    }
}
