﻿using UnityEngine;
using System.Collections;
using Item;
using Quest;
using System;
/// <summary>
/// 对话框提示界面管理类
/// </summary>
public enum SourcePage
{
    Other,      //非操作区
    kitbag,     //背包包裹栏
    Warehouse,  //背包仓库栏
    Avatar,     //主角身上
    Goods,      //背包物品栏
}

public class ItemDetailManager : MonoSingleton<ItemDetailManager>
{
    private int _halfScreenWidth;
    private int _halfScreenHight;
    protected override void OnInit()
    {
        _halfScreenWidth = Screen.width / 2;
        _halfScreenHight = Screen.height / 2;
    }

    protected override void OnUnInit()
    {

    }

    public void AddListenerItemDetail(GameObject clickObject, AnchorType anchorType, SourcePage sourcePage, ItemBase itemData, Action<GameObject> cb = null)
    {
        WinID winID = GetItemDetailWinID(itemData);
        if (winID == WinID.None) return;
        if (winID == WinID.UIItemDetail)
        {
            InputManager.instance.AddClickListener(clickObject, (go) =>
            {
                SetViewOffset(clickObject);
                BaseItemDetailWin dialog = (BaseItemDetailWin)UIManager.instance.OpenWindow(null, winID, ItemDisplayRule.GetItemDisplayByItem(itemData), sourcePage);
                dialog.SetAnchorType(anchorType);
                if (cb != null) cb(go);
            });
        }
        else if (winID == WinID.UIEquipItemDetail)
        {
            InputManager.instance.AddClickListener(clickObject, (go) =>
            {
                SetViewOffset(clickObject);
                BaseItemDetailWin dialog = (BaseItemDetailWin)UIManager.instance.OpenWindow(null, winID, itemData, sourcePage);
                dialog.SetAnchorType(anchorType);
                if (cb != null) cb(go);
            });
        }
    }
    public void AddListenerItemDetail(GameObject clickObject, AnchorType anchorType, SourcePage sourcePage, ItemsData itemData, Action<GameObject> cb = null)
    {
        InputManager.instance.AddClickListener(clickObject, (go) =>
        {
            SetViewOffset(clickObject);
            BaseItemDetailWin dialog = (BaseItemDetailWin)UIManager.instance.OpenWindow(null, WinID.UIItemDetail, ItemDisplayRule.GetItemDisplayByItem(itemData), sourcePage);
            dialog.SetAnchorType(anchorType);
            if (cb != null) cb(go);
        });
    }
    public void AddListenerItemDetail(GameObject clickObject, AnchorType anchorType, SourcePage sourcePage, QuestReward itemData, Action<GameObject> cb = null)
    {
        InputManager.instance.AddClickListener(clickObject, (go) =>
        {
            SetViewOffset(clickObject);
            BaseItemDetailWin dialog = (BaseItemDetailWin)UIManager.instance.OpenWindow(null, WinID.UIItemDetail, ItemDisplayRule.GetItemDisplayByItem(itemData), sourcePage);
            dialog.SetAnchorType(anchorType);
            if (cb != null) cb(go);
        });
    }
    public void RemoveListenerItemDetail(GameObject clickObject)
    {
        InputManager.instance.RemoveClickListener(clickObject);
    }

    public void HideItemDetail(ItemBase itemData)
    {
        WinID winID = GetItemDetailWinID(itemData);
        if (winID == WinID.None) return;
        UIManager.instance.CloseWindow(winID);
    }

    private WinID GetItemDetailWinID(ItemBase itemData)
    {
        if (itemData == null) return WinID.None;
        WinID winID = WinID.None;
        switch (itemData.Type)
        {
            case ItemTypeDefine.ITEM_EQUIPMENT:
                winID = WinID.UIEquipItemDetail;
                break;
            case ItemTypeDefine.ITEM_FUNCTION:
            case ItemTypeDefine.ITEM_CONSUMABLE:
                winID = WinID.UIItemDetail;
                break;
            default:
                break;
        }
        return winID;
    }

    private Vector2 _adjustView = Vector2.zero;
    public Vector2 GetAdjustView()
    {
        return _adjustView;
    }
    public void SetViewOffset(GameObject target)
    {
        if (target == null) return;
        Vector2 screenView = UICamera.mainCamera.WorldToScreenPoint(target.transform.position);
        int x = ((int)screenView.x - _halfScreenWidth) / 2;
        _adjustView = new Vector2(x, 0);
    }

}
