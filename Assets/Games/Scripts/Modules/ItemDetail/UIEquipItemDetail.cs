﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
/// <summary>
/// 装备提示界面
/// </summary>
public class UIEquipItemDetail : BaseItemDetailWin
{
    protected override void OnInit()
    {
        InitPage();
    }

    protected override void OnOpen(params object[] args)
    {
        SetPage(args);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnSetAnchorType(AnchorType type)
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        UIAnchor anchor = center.GetComponent<UIAnchor>();
        switch (type)
        {
            case AnchorType.Adjust:
                anchor.pixelOffset = ItemDetailManager.instance.GetAdjustView();
                break;
            case AnchorType.Left:
                anchor.pixelOffset = new Vector2(-200, 0);
                break;
            case AnchorType.Center:
                anchor.pixelOffset = Vector2.zero;
                break;
            case AnchorType.Right:
                anchor.pixelOffset = new Vector2(200, 0);
                break;
            default:
                break;
        }
        for (int i = 0; i < _pages.Length; i++)
        {
            _pages[i].AdjustSize();
        }
        KeepHorizontal();
        _uiGrid.gameObject.SetActive(false);
        _uiGrid.gameObject.SetActive(true);
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private BaseItemDetailWinPage[] _pages = new BaseItemDetailWinPage[2];
    private Transform _pageOneTop;
    private Transform _pageOneBottom;
    private Transform _pageTwoTop;
    private UIGrid _uiGrid;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        Transform grid = center.Find("UIGrid");
        _uiGrid = grid.GetComponent<UIGrid>();
        for (int i = 0; i < _pages.Length; i++)
        {
            _pages[i] = grid.Find(i.ToString()).GetComponent<BaseItemDetailWinPage>();
        }
        Transform pageOneRoot = grid.Find("0/Root");
        _pageOneTop = pageOneRoot.Find("Top");
        _pageOneBottom = pageOneRoot.Find("Bottom");

        _pageTwoTop = grid.Find("1/Root/Top");
    }
    //比较挫的做法
    private void KeepHorizontal()
    {
        Vector3 offset = _pageTwoTop.localPosition - _pageOneTop.localPosition;
        _pageOneTop.localPosition = _pageTwoTop.localPosition;
        _pageOneBottom.localPosition += offset;
    }

    private void SetPage(params object[] args)
    {
        if (args == null || args.Length != 2) return;
        ItemEquipment equip = (ItemEquipment)args[0];
        SourcePage sourcePage = (SourcePage)args[1];
        if (equip == null) return;

        switch (sourcePage)
        {
            case SourcePage.Other:
                _pages[0].Close();
                _pages[1].Open(this, equip, sourcePage);
                break;
            case SourcePage.kitbag:
                _pages[0].Close();
                _pages[1].Open(this, equip, sourcePage);
                break;
            case SourcePage.Warehouse:
                _pages[0].Close();
                _pages[1].Open(this, equip, sourcePage);
                break;
            case SourcePage.Avatar:
                _pages[0].Close();
                _pages[1].Open(this, equip, sourcePage);
                break;
            case SourcePage.Goods:
                EQUIPMENT_LOCATION location;
                EquipmentRule.GetEquipmentLocation(equip, out location);
                ItemEquipment avatarEquip;
                if (GameMain.Player.equipModule.GetEquipmentInAvatar(location, out avatarEquip))
                {
                    _pages[0].Open(this, avatarEquip, SourcePage.Other);
                }
                else
                {
                    _pages[0].Close();
                }
                _pages[1].Open(this, equip, sourcePage, avatarEquip);
                break;
            default:
                _pages[0].Close();
                _pages[1].Close();
                break;
        }
        _uiGrid.Reposition();
    }
}
