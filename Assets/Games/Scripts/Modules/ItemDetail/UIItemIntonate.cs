﻿using UnityEngine;
using System.Collections;
using Item;
/// <summary>
/// 物品吟唱界面
/// </summary>
public class UIItemIntonate : UIWin
{
    protected override void OnInit()
    {
        InitItem();
    }

    private ItemUsable _data;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _data = (ItemUsable)args[0];
        SetItem(_data);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        CancelUpdate();
    }

    protected override void OnUnInit()
    {

    }

    private UIProgressBar _bar;
    private UILabel _name;
    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _bar = center.Find("Bar").GetComponent<UIProgressBar>();
        _name = center.Find("Name").GetComponent<UILabel>();
        _bar.value = 0;
    }

    private void SetItem(ItemUsable data)
    {
        if (data == null)
        {
            UIManager.instance.CloseWindow(winID);
            return;
        }
        _name.text = data.OperationDesc;
        _bar.value = 0;
        CancelUpdate();
        InvokeRepeating("UpdateBar", 0, 0.01f * data.IntonateTime);
    }
    private void UpdateBar()
    {
        if (GameMain.Player.SceneEntityObj == null || GameMain.Player.SceneEntityObj.Curstatus != Status.Idle)
        {
            UIManager.instance.CloseWindow(winID);
            return;
        }
        if(_bar.value >= 1)
        {
            UIManager.instance.CloseWindow(winID);
            if (_data == null) return;
            _data.OnIntonateOver(GameMain.Player);
        }
        else
        {
            _bar.value += 0.01f;
        }
    }
    private void CancelUpdate()
    {
        CancelInvoke("UpdateBar");
    }
}
