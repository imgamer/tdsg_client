﻿using UnityEngine;
using System.Collections;

public class EquipDetailBaseItem : WinItem
{
    private UILabel _property;
    public string Property
    {
        set{ _property.text = value; }
    }

    private const int Num = 3;
    private GameObject[] _upDownState = new GameObject[Num];

    //0: None 1:UP 2: Down
    public void SetState(int state)
    {
        for(int i = 0; i < Num; i++)
        {
            _upDownState[i].SetActive(i == state);
        }
    }

    protected override void OnInit()
    {
        _property = transform.Find("Text").GetComponent<UILabel>();
        Transform state = transform.Find("State");
        for(int i = 0; i < Num; i++)
        {
            GameObject item = state.Find(i.ToString()).gameObject;
            _upDownState[i] = item;
        }
    }
}
