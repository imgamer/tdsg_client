﻿using UnityEngine;
using System.Collections;
using Item;
/// <summary>
/// 物品使用界面
/// </summary>
public class UIItemUse : UIWin
{
    protected override void OnInit()
    {
        InitItem();
    }

    private ItemUsable _data;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _data = (ItemUsable)args[0];
        SetItem(_data);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UILabel _name;
    private void InitItem()
    {
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        Transform root = bottomRight.Find("Root");
        GameObject btnUse = root.Find("BtnUse").gameObject;
        InputManager.instance.AddClickListener(btnUse, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
            if (_data == null) return;
            //对自己使用道具
            StatusID status = _data.Use(GameMain.Player, GameMain.Player);
            if (status != StatusID.SKILL_OK)
            {
                TipManager.instance.ShowTextTip((int)status);
            }
        });
        GameObject btnClose = root.Find("BtnClose").gameObject;
        InputManager.instance.AddClickListener(btnClose, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        Transform item = root.Find("Item");
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _name = item.Find("Name").GetComponent<UILabel>();
    }

    private void SetItem(ItemUsable data)
    {
        if (data == null) return;
        _icon.spriteName = data.Icon;
        _name.text = ItemDisplayRule.GetItemName(data);
    }
}
