﻿using UnityEngine;
using System.Collections;

public class EquipDetailGemItem : WinItem
{
    private int _level;
    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }

    private UILabel _desc;
    public string Desc
    {
        set
        {
            _desc.text = value;
        }
    }

    private GameObject _upMark;
    private UILabel _upValue;
    public string UpValue
    {
        set
        {
            if(string.IsNullOrEmpty(value))
            {
                _upMark.SetActive(false);
            }
            else
            {
                _upMark.SetActive(true);
                _upValue.text = value.ToString();
            }         
        }
    }

    private const int Num = 3;
    private GameObject[] _stateObject = new GameObject[Num];

    //0: 有宝石 1：未镶嵌 2：未开启
    public void SetItemState(int state)
    {
        for(int i =0; i < Num; i++)
        {
            _stateObject[i].SetActive(i == state);
        }
    }

    protected override void OnInit()
    {
        for(int i =0; i < Num; i++)
        {
            GameObject item = transform.Find(i.ToString()).gameObject;
            _stateObject[i] = item;
        }
        _icon = transform.Find("0/Icon").GetComponent<UISprite>();
        _desc = transform.Find("0/Text").GetComponent<UILabel>();
        _upMark = transform.Find("0/Up").gameObject;
        _upValue = transform.Find("0/Up/Value").GetComponent<UILabel>();
    }
}
