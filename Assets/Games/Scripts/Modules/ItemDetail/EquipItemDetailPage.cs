﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
/// <summary>
/// 装备提示Page
/// </summary>
public class EquipItemDetailPage : WinPage
{
    protected override void OnInit()
    {
        InitTop();
        InitBottom();
    }

    private ItemEquipment _itemData;
    private SourcePage _sourcePage;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 2) return;
        _itemData = (ItemEquipment)args[0];
        _sourcePage = (SourcePage)args[1];
        if (_itemData == null) return;
        UpdateData();
        AdjustSize();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UILabel _name;
    private UILabel _level;
    private UILabel _score;

    #region 滑动区组件
    private float _initHight = 0;
    private UIScrollView _srollView;
    private Transform _top;
    private UI.Table _uiTable;

    private GameObject _base;
    private UIGrid _baseUIGrid;
    private GameObject _baseTemplate;
    private List<UILabel> _baseTexts = new List<UILabel>();

    private GameObject _attach;
    private UIGrid _attachUIGrid;
    private GameObject _attachTemplate;
    private List<UILabel> _attachTexts = new List<UILabel>();

    private GameObject _gem;
    private UIGrid _gemUIGrid;
    private GameObject _gemTemplate;
    private List<UILabel> _gemTexts = new List<UILabel>();

    private GameObject _other;
    private UILabel _otherText;
    private GameObject _desc;
    private UILabel _descText;
    #endregion
    private void InitTop()
    {
        _top = transform.Find("Root/Top");
        Transform item = _top.Find("Item");
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _name = item.Find("Name").GetComponent<UILabel>();
        _level = item.Find("Level").GetComponent<UILabel>();
        _score = item.Find("Score/Value").GetComponent<UILabel>();

        _srollView = _top.Find("ScrollView").GetComponent<UIScrollView>();
        _initHight = _srollView.panel.GetViewSize().y;
        Transform table = _top.Find("ScrollView/UITable");
        _uiTable = table.GetComponent<UI.Table>();

        _base = table.Find("Base").gameObject;
        Transform baseGrid = table.Find("Base/UIGrid");
        _baseUIGrid = baseGrid.GetComponent<UIGrid>();
        _baseTemplate = baseGrid.Find("Item").gameObject;
        _baseTemplate.SetActive(false);

        _attach = table.Find("Attach").gameObject;
        Transform attachGrid = table.Find("Attach/UIGrid");
        _attachUIGrid = attachGrid.GetComponent<UIGrid>();
        _attachTemplate = attachGrid.Find("Item").gameObject;
        _attachTemplate.SetActive(false);

        _gem = table.Find("Gem").gameObject;
        Transform gemGrid = table.Find("Gem/UIGrid");
        _gemUIGrid = gemGrid.GetComponent<UIGrid>();
        _gemTemplate = gemGrid.Find("Item").gameObject;
        _gemTemplate.SetActive(false);

        _other = table.Find("Other").gameObject;
        _otherText = table.Find("Other/Text").GetComponent<UILabel>();
        _desc = table.Find("Desc").gameObject;
        _descText = table.Find("Desc/Text").GetComponent<UILabel>();
    }

    private UIWidget _background;
    private Transform _bottom;
    private GameObject[] _states = new GameObject[5];
    private void InitBottom()
    {
        _background = transform.Find("Root/BG").GetComponent<UIWidget>();
        _bottom = transform.Find("Root/Bottom");
        Transform state0 = _bottom.Find(string.Format("State{0}", 0));
        state0.gameObject.SetActive(false);
        _states[0] = state0.gameObject;

        Transform state1 = _bottom.Find(string.Format("State{0}", 1));
        state1.gameObject.SetActive(false);
        _states[1] = state1.gameObject;
        GameObject btnCenter1 = state1.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter1, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToWarehouse(_itemData.Order);
            UIManager.instance.CloseWindow(ParentUI.winID);
        });

        Transform state2 = _bottom.Find(string.Format("State{0}", 2));
        state2.gameObject.SetActive(false);
        _states[2] = state2.gameObject;
        GameObject btnCenter2 = state2.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter2, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToKitbag(_itemData.Order);
            UIManager.instance.CloseWindow(ParentUI.winID);
        });

        Transform state3 = _bottom.Find(string.Format("State{0}", 3));
        state3.gameObject.SetActive(false);
        _states[3] = state3.gameObject;
        GameObject btnLeft3 = state3.Find("BtnLeft").gameObject;
        InputManager.instance.AddClickListener(btnLeft3, (go) =>
        {
            Printer.Log("镶嵌");
            UIManager.instance.CloseWindow(ParentUI.winID);
        });
        GameObject btnRight3 = state3.Find("BtnRight").gameObject;
        InputManager.instance.AddClickListener(btnRight3, (go) =>
        {
            Printer.Log("卸下");
            UIManager.instance.CloseWindow(ParentUI.winID);
            GameMain.Player.equipModule.SendTakeOffEquipment(_itemData);
        });

        Transform state4 = _bottom.Find(string.Format("State{0}", 4));
        state4.gameObject.SetActive(false);
        _states[4] = state4.gameObject;
        GameObject btnLeft4 = state4.Find("BtnLeft").gameObject;
        InputManager.instance.AddClickListener(btnLeft4, (go) =>
        {
            Printer.Log("更多");
            UIManager.instance.CloseWindow(ParentUI.winID);
        });
        GameObject btnRight4 = state4.Find("BtnRight").gameObject;
        InputManager.instance.AddClickListener(btnRight4, (go) =>
        {
            Printer.Log("装备");
            UIManager.instance.CloseWindow(ParentUI.winID);
            GameMain.Player.equipModule.SendPutOnEquipment(_itemData);
        });
    }

    private void UpdateData()
    {
        ShowItem();
        ShowBase();
        ShowAttach();
        ShowGem();
        ShowOther();
        ShowDesc();
        _uiTable.Reposition();
        _uiTable.GoToStart();
    }
    private void ShowItem()
    {
        _icon.spriteName = _itemData.Icon;
        _name.text = ItemDisplayRule.GetItemName(_itemData);
        _level.text = string.Format("Lv.{0}", _itemData.Level);
        _score.text = string.Format("{0}", 12345);
    }
    private void ShowBase()
    {
        List<PropertyBase> properties = _itemData.BaseProperties;
        if (properties == null || properties.Count <= 0)
        {
            _base.SetActive(false);
            return;
        }
        _base.SetActive(true);
        int count = _baseTexts.Count;
        for (int i = 0; i < count; i++)
        {
            UILabel item = _baseTexts[i];
            item.enabled = false;
        }
        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                UILabel item = _baseTexts[i];
                item.enabled = true;
                item.text = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
            else
            {
                GameObject temp = Instantiate(_baseTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _baseTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                UILabel item = trans.Find("Text").GetComponent<UILabel>();
                _baseTexts.Add(item);
                item.text = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
        }
        _baseUIGrid.Reposition();
    }
    private void ShowAttach()
    {
        List<PropertyBase> properties = _itemData.ExtraProperties;
        if (properties == null || properties.Count <= 0)
        {
            _attach.SetActive(false);
            return;
        }
        _attach.SetActive(true);
        int count = _attachTexts.Count;
        for (int i = 0; i < count; i++)
        {
            UILabel item = _attachTexts[i];
            item.enabled = false;
        }
        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                UILabel item = _attachTexts[i];
                item.enabled = true;
                item.text = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
            else
            {
                GameObject temp = Instantiate(_attachTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _attachTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                UILabel item = trans.Find("Text").GetComponent<UILabel>();
                _attachTexts.Add(item);
                item.text = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
        }
        _attachUIGrid.Reposition();
    }
    private void ShowGem()
    {
        _gem.SetActive(false);
    }
    private void ShowOther()
    {
        _other.SetActive(true);
        _otherText.text = string.Format("打造:{0} 强化打造\n耐久度:{1}\n出售价格:{2}", GameMain.Player.Name, 100, _itemData.Price);
    }
    private void ShowDesc()
    {
        _descText.text = ItemDisplayRule.GetItemDesc(_itemData);
    }
    private IEnumerator itr = null;
    private void AdjustSize()
    {
        if (itr != null) StopCoroutine(itr);
        itr = AdjustView();
        StartCoroutine(itr);
    }
    private const float Max = 150;
    private IEnumerator AdjustView()
    {
        yield return new WaitForEndOfFrame();
        float value = (_srollView.bounds.size.y - _initHight) * 0.5f;
        float y = 0;
        if (value > 0)
        {
            y = value + 2;
            y = Mathf.Min(y, Max);
        }
        else
        {
            y = 0;
        }
        _top.localPosition = new Vector3(0, y, 0);
        _bottom.localPosition = new Vector3(0, -y, 0);
        SetState();
    }

    private void SetState()
    {
        switch (_sourcePage)
        {
            case SourcePage.Other:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 0);
                }
                _background.bottomAnchor.Set(0, -30);
                break;
            case SourcePage.kitbag:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 1);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Warehouse:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 2);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Avatar:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 3);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Goods:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 4);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            default:
                break;
        }
    }
}
