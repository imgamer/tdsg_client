﻿using UnityEngine;
using System.Collections;

public abstract class BaseItemDetailWinPage : WinPage 
{
    public void AdjustSize()
    {
        if (!Active) return;
        OnAdjustSize();
    }
    protected abstract void OnAdjustSize();
}
