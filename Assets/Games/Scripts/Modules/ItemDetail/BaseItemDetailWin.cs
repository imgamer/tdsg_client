﻿using UnityEngine;
using System.Collections;
using Item;
/// <summary>
/// 提示对话框基类
/// </summary>
public enum AnchorType
{
    Adjust,
    Left,
    Center,
    Right,
}
public abstract class BaseItemDetailWin : UIWin
{
    protected AnchorType anchorType = AnchorType.Center;
    private IEnumerator itr = null;
    public void SetAnchorType(AnchorType type)
    {
        anchorType = type;
        if (itr != null) StopCoroutine(itr);
        itr = SetAnchor(anchorType);
        StartCoroutine(itr);
    }
    private IEnumerator SetAnchor(AnchorType type)
    {
        yield return new WaitForEndOfFrame();
        OnSetAnchorType(type);
    }
    protected abstract void OnSetAnchorType(AnchorType type);



}
