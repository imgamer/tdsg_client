﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
/// <summary>
/// 道具提示界面
/// </summary>
public class UIItemDetail : BaseItemDetailWin
{
    protected override void OnInit()
    {
        InitTop();
        InitBottom();
    }

    private ItemDisplay _itemDisplay;
    private SourcePage _sourcePage;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 2) return;
        _itemDisplay = (ItemDisplay)args[0];
        _sourcePage = (SourcePage)args[1];
        if (_itemDisplay == null) return;
        UpdateData();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnSetAnchorType(AnchorType type)
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        UIAnchor anchor = center.GetComponent<UIAnchor>();
        switch (type)
        {
            case AnchorType.Adjust:
                anchor.pixelOffset = ItemDetailManager.instance.GetAdjustView();
                break;
            case AnchorType.Left:
                anchor.pixelOffset = new Vector2(-200, 0);
                break;
            case AnchorType.Center:
                anchor.pixelOffset = Vector2.zero;
                break;
            case AnchorType.Right:
                anchor.pixelOffset = new Vector2(200, 0);
                break;
            default:
                break;
        }
        AdjustSize();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UILabel _name;
    private UILabel[] _text = new UILabel[2];

    #region 滑动区组件
    private float _initHight = 0;
    private UIScrollView _srollView;
    private Transform _top;
    private UI.Table _uiTable;

    private GameObject _base;
    private UILabel _baseText;

    private GameObject _desc;
    private UILabel _descText;
    #endregion
    private void InitTop()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _top = center.Find("Root/Top");
        Transform item = _top.Find("Item");
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _name = item.Find("Name").GetComponent<UILabel>();
        _text[0] = item.Find("Text0").GetComponent<UILabel>();
        _text[1] = item.Find("Text1").GetComponent<UILabel>();

        _srollView = _top.Find("ScrollView").GetComponent<UIScrollView>();
        _initHight = _srollView.panel.GetViewSize().y;
        Transform table = _top.Find("ScrollView/UITable");
        _uiTable = table.GetComponent<UI.Table>();

        Transform baseGrid = table.Find("Base/UIGrid");
        _baseText = table.Find("Base/Text").GetComponent<UILabel>();

        _desc = table.Find("Desc").gameObject;
        _descText = table.Find("Desc/Text").GetComponent<UILabel>();
    }

    private UIWidget _background;
    private Transform _bottom;
    private GameObject[] _states = new GameObject[4];
    private const int MaxNum = 7;
    private GameObject[] _operationBtns = new GameObject[MaxNum];
    private UILabel[] _operationBtnNames = new UILabel[MaxNum];
    private void InitBottom()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _background = center.Find("Root/BG").GetComponent<UIWidget>();
        _bottom = center.Find("Root/Bottom");
        Transform state0 = _bottom.Find(string.Format("State{0}", 0));
        state0.gameObject.SetActive(false);
        _states[0] = state0.gameObject;

        Transform state1 = _bottom.Find(string.Format("State{0}", 1));
        state1.gameObject.SetActive(false);
        _states[1] = state1.gameObject;
        GameObject btnCenter1 = state1.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter1, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToWarehouse(_itemDisplay.data.Order);
            UIManager.instance.CloseWindow(winID);
        });

        Transform state2 = _bottom.Find(string.Format("State{0}", 2));
        state2.gameObject.SetActive(false);
        _states[2] = state2.gameObject;
        GameObject btnCenter2 = state2.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter2, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToKitbag(_itemDisplay.data.Order);
            UIManager.instance.CloseWindow(winID);
        });

        Transform state3 = _bottom.Find(string.Format("State{0}", 3));
        state3.gameObject.SetActive(false);
        _states[3] = state3.gameObject;
        Transform btn0 = state3.Find("0");
        _operationBtns[0] = btn0.gameObject;
        _operationBtnNames[0] = btn0.Find("Text").GetComponent<UILabel>();
        Transform grid = state3.Find("Grid");
        for (int i = 1; i < MaxNum; i++)
        {
            Transform child = grid.Find(i.ToString());
            _operationBtns[i] = child.gameObject;
            _operationBtnNames[i] = child.Find("Text").GetComponent<UILabel>();
        }
    }

    private void UpdateData()
    {
        ShowItem();
        ShowBase();
        ShowDesc();
        _uiTable.Reposition();
        _uiTable.GoToStart();
    }
    private void ShowItem()
    {
        SetDynamicSpriteName(_icon, _itemDisplay.icon);
        _name.text = _itemDisplay.name;
        _text[0].text = _itemDisplay.text[0];
        _text[1].text = _itemDisplay.text[1];
    }
    private void ShowBase()
    {
        _baseText.text = _itemDisplay.desc[0];
    }
    private void ShowDesc()
    {
        _descText.text = _itemDisplay.desc[1];
    }

    private const float Max = 150;
    private void AdjustSize()
    {
        float value = (_srollView.bounds.size.y - _initHight) * 0.5f;
        float y = 0;
        if (value > 0)
        {
            y = value + 2;
            y = Mathf.Min(y, Max);
        }
        else
        {
            y = 0;
        }
        _top.localPosition = new Vector3(0, y, 0);
        _bottom.localPosition = new Vector3(0, -y, 0);
        SetState();
    }

    private void SetState()
    {
        switch (_sourcePage)
        {
            case SourcePage.Other:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 0);
                }
                _background.bottomAnchor.Set(0, -30);
                break;
            case SourcePage.kitbag:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 1);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Warehouse:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 2);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Avatar:
                break;
            case SourcePage.Goods:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 3);
                }
                _background.bottomAnchor.Set(0, -90);
                SetOperationMenu();
                break;
            default:
                break;
        }
    }
    private void SetOperationMenu()
    {
        HideOperationMenu();
        if (_itemDisplay == null || _itemDisplay.data == null) return;
        List<byte> _operationList = _itemDisplay.data.OperationList;
        if(_operationList.Count == 2)
        {
            for (int i = 0; i < _operationList.Count; i++)
            {
                GameObject btn = _operationBtns[i];
                byte key = _operationList[i];
                Operation operation;
                ItemOperationRule.TryGetOperationByKey(key, out operation);
                if (operation == null) continue;
                _operationBtnNames[i].text = operation.operationName;
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if (operation.operationAction != null)
                    {
                        operation.operationAction(_itemDisplay.data);
                    }
                    UIManager.instance.CloseWindow(winID);
                });
            }
            return;
        }

        //超过两种操作
        int index = 0;
        for (int i = 0; i < MaxNum; i++)
        {
            //插入“更多”按钮
            GameObject btn = _operationBtns[i];
            if(i == 1)
            {
                _operationBtnNames[i].text = "更多";
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if(_show)
                    {
                        HideOperationMenu();
                    }
                    else
                    {
                        ShowOperationMenu(_operationList.Count);
                    }
                });
                continue;
            }

            if (index < _operationList.Count)
            {
                byte key = _operationList[index];
                Operation operation;
                ItemOperationRule.TryGetOperationByKey(key, out operation);
                if (operation == null) continue;
                _operationBtnNames[i].text = operation.operationName;
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if (operation.operationAction != null)
                    {
                        operation.operationAction(_itemDisplay.data);
                    }
                    UIManager.instance.CloseWindow(winID);
                });
            }
            index++;
        }
    }
    private bool _show = false;
    private void ShowOperationMenu(int endAt = 1)
    {
        _show = true;
        for (int i = MaxNum - 1; i >= 0; i--)
        {
            if (i <= endAt)
            {
                _operationBtns[i].SetActive(true);
            }
            else
            {
                _operationBtns[i].SetActive(false);
            }
        }
    }
    private void HideOperationMenu()
    {
        _show = false;
        for (int i = MaxNum - 1; i >= 0; i--)
        {
            if (i <= 1)
            {
                _operationBtns[i].SetActive(true);
            }
            else
            {
                _operationBtns[i].SetActive(false);
            }
        }
    }
}
