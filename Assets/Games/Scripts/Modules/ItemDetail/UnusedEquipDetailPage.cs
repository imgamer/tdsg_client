﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
/// <summary>
/// 非使用中的装备提示Page
/// </summary>
public class UnusedEquipDetailPage : BaseItemDetailWinPage
{
    protected override void OnInit()
    {
        InitTop();
        InitBottom();
    }

    private ItemEquipment _itemData;
    private ItemEquipment _avatarItemData;
    private SourcePage _sourcePage;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length == 1) return;
        _itemData = (ItemEquipment)args[0];      
        _sourcePage = (SourcePage)args[1];
        if(args.Length == 3)     _avatarItemData = (ItemEquipment)args[2];
        if (_itemData == null) return;
        UpdateData();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UILabel _name;
    private UILabel _level;
    private UILabel _score;

    #region 滑动区组件
    private float _initHight = 0;
    private UIScrollView _srollView;
    private Transform _top;
    private UI.Table _uiTable;

    private GameObject _base;
    private UIGrid _baseUIGrid;
    private GameObject _baseTemplate;
    private List<EquipDetailBaseItem> _baseItems = new List<EquipDetailBaseItem>();

    private GameObject _attach;
    private UIGrid _attachUIGrid;
    private GameObject _attachTemplate;
    private List<UILabel> _attachTexts = new List<UILabel>();

    private const int GemNum = 3;
    private GameObject _gem;
    private UIGrid _gemUIGrid;
    private EquipDetailGemItem[] _gemItems = new EquipDetailGemItem[GemNum];

    private GameObject _other;
    private UILabel _otherText;
    private GameObject _desc;
    private UILabel _descText;
    #endregion
    private void InitTop()
    {
        _top = transform.Find("Root/Top");
        Transform item = _top.Find("Item");
        _icon = item.Find("Icon").GetComponent<UISprite>();
        _name = item.Find("Name").GetComponent<UILabel>();
        _level = item.Find("Level").GetComponent<UILabel>();
        _score = item.Find("Score/Value").GetComponent<UILabel>();

        _srollView = _top.Find("ScrollView").GetComponent<UIScrollView>();
        _initHight = _srollView.panel.GetViewSize().y;
        Transform table = _top.Find("ScrollView/UITable");
        _uiTable = table.GetComponent<UI.Table>();

        _base = table.Find("Base").gameObject;
        Transform baseGrid = table.Find("Base/UIGrid");
        _baseUIGrid = baseGrid.GetComponent<UIGrid>();
        _baseTemplate = baseGrid.Find("Item").gameObject;
        _baseTemplate.SetActive(false);

        _attach = table.Find("Attach").gameObject;
        Transform attachGrid = table.Find("Attach/UIGrid");
        _attachUIGrid = attachGrid.GetComponent<UIGrid>();
        _attachTemplate = attachGrid.Find("Item").gameObject;
        _attachTemplate.SetActive(false);

        _gem = table.Find("Gem").gameObject;
        Transform gemGrid = table.Find("Gem/UIGrid");
        _gemUIGrid = gemGrid.GetComponent<UIGrid>();
        for (int i = 0; i < GemNum; i++)
        {
            EquipDetailGemItem gemItem = gemGrid.Find(i.ToString()).GetComponent<EquipDetailGemItem>();
            gemItem.Init(ParentUI);
            _gemItems[i] = gemItem;
        }

        _other = table.Find("Other").gameObject;
        _otherText = table.Find("Other/Text").GetComponent<UILabel>();
        _desc = table.Find("Desc").gameObject;
        _descText = table.Find("Desc/Text").GetComponent<UILabel>();
    }

    private UIWidget _background;
    private Transform _bottom;
    private GameObject[] _states = new GameObject[5];
    private const int MaxNum = 7;
    private GameObject[] _operationBtns = new GameObject[MaxNum];
    private UILabel[] _operationBtnNames = new UILabel[MaxNum];
    private void InitBottom()
    {
        _background = transform.Find("Root/BG").GetComponent<UIWidget>();
        _bottom = transform.Find("Root/Bottom");
        Transform state0 = _bottom.Find(string.Format("State{0}", 0));
        state0.gameObject.SetActive(false);
        _states[0] = state0.gameObject;

        Transform state1 = _bottom.Find(string.Format("State{0}", 1));
        state1.gameObject.SetActive(false);
        _states[1] = state1.gameObject;
        GameObject btnCenter1 = state1.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter1, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToWarehouse(_itemData.Order);
            UIManager.instance.CloseWindow(ParentUI.winID);
        });

        Transform state2 = _bottom.Find(string.Format("State{0}", 2));
        state2.gameObject.SetActive(false);
        _states[2] = state2.gameObject;
        GameObject btnCenter2 = state2.Find("BtnCenter").gameObject;
        InputManager.instance.AddClickListener(btnCenter2, (go) =>
        {
            GameMain.Player.kitbagModule.MoveItemToKitbag(_itemData.Order);
            UIManager.instance.CloseWindow(ParentUI.winID);
        });

        Transform state3 = _bottom.Find(string.Format("State{0}", 3));
        state3.gameObject.SetActive(false);
        _states[3] = state3.gameObject;
        GameObject btnLeft3 = state3.Find("BtnLeft").gameObject;
        InputManager.instance.AddClickListener(btnLeft3, (go) =>
        {
            UIManager.instance.CloseWindow(ParentUI.winID);
        });
        GameObject btnRight3 = state3.Find("BtnRight").gameObject;
        InputManager.instance.AddClickListener(btnRight3, (go) =>
        {
            UIManager.instance.CloseWindow(ParentUI.winID);
            GameMain.Player.equipModule.SendTakeOffEquipment(_itemData);
        });

        Transform state4 = _bottom.Find(string.Format("State{0}", 4));
        state4.gameObject.SetActive(false);
        _states[4] = state4.gameObject;
        Transform btn0 = state4.Find("0");
        _operationBtns[0] = btn0.gameObject;
        _operationBtnNames[0] = btn0.Find("Text").GetComponent<UILabel>();
        Transform grid = state4.Find("Grid");
        for (int i = 1; i < MaxNum; i++)
        {
            Transform child = grid.Find(i.ToString());
            _operationBtns[i] = child.gameObject;
            _operationBtnNames[i] = child.Find("Text").GetComponent<UILabel>();
        }
    }

    private void UpdateData()
    {
        ShowItem();
        ShowBase();
        ShowAttach();
        ShowGem();
        ShowOther();
        ShowDesc();
        _uiTable.Reposition();
        _uiTable.GoToStart();
    }
    private void ShowItem()
    {
        _icon.spriteName = _itemData.Icon;
        _name.text = ItemDisplayRule.GetItemName(_itemData);
        _level.text = string.Format("Lv.{0}", _itemData.Level);
        _score.text = string.Format("{0}", 12345);
    }
    private void ShowBase()
    {
        List<PropertyBase> properties = _itemData.BaseProperties;
        if (properties == null || properties.Count <= 0)
        {
            _base.SetActive(false);
            return;
        }
        _base.SetActive(true);

        int count = _baseItems.Count;
        for (int i = 0; i < count; i++)
        {
            EquipDetailBaseItem item = _baseItems[i];
            item.gameObject.SetActive(false);
        }
        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                EquipDetailBaseItem item = _baseItems[i];
                item.gameObject.SetActive(true);
                item.Property = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
            else
            {
                GameObject temp = Instantiate(_baseTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _baseTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                EquipDetailBaseItem item = trans.GetComponent<EquipDetailBaseItem>();
                item.Init(ParentUI);
                _baseItems.Add(item);
                item.Property = string.Format("{0} {1}", property.PropertyName, property.Value);
            }
        }
        _baseUIGrid.Reposition();

        if (_avatarItemData == null)
        {
            for (int i = 0; i < _baseItems.Count; i++)
            {
                _baseItems[i].SetState(0);
            }
        }
        else
        {
            List<PropertyBase> avatarProperties = _avatarItemData.BaseProperties;
            for (int i = 0; i < avatarProperties.Count; i++)
            {
                _baseItems[i].SetState(EquipRule.ComparePorperty(avatarProperties[i], properties[i]));
            }
        }
    }

    private void ShowAttach()
    {
        List<PropertyBase> properties = _itemData.ExtraProperties;
        if (properties == null || properties.Count <= 0)
        {
            _attach.SetActive(false);
            return;
        }
        _attach.SetActive(true);
        int count = _attachTexts.Count;
        for (int i = 0; i < count; i++)
        {
            UILabel item = _attachTexts[i];
            item.enabled = false;
        }
        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                UILabel item = _attachTexts[i];
                item.enabled = true;
                item.text = string.Format("{0} {1}   {2}星", property.PropertyName, property.Value, property.ValueStar);
            }
            else
            {
                GameObject temp = Instantiate(_attachTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _attachTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                UILabel item = trans.Find("Text").GetComponent<UILabel>();
                _attachTexts.Add(item);
                item.text = string.Format("{0} {1}   {2}星", property.PropertyName, property.Value, property.ValueStar);
            }
        }
        _attachUIGrid.Reposition();
    }

    private void ShowGem()
    {
        _gemItems[0].UpValue = "";
        _gemItems[1].UpValue = "";
        _gemItems[2].UpValue = "";
        //宝石数据索引是 1 2 3
        Dictionary<int, int> gemData = _itemData.EquipGem;
        foreach (int key in gemData.Keys)
        {
            SetGemData(_gemItems[key - 1], gemData[key]);          
        }

         if(_itemData.Quality == ItemTypeDefine.ITEM_QUALITY_ONE)
        {
            for (int i = 0; i < GemNum; i++)
            {
                _gemItems[i].SetItemState(2);
            }
        }
         else if (_itemData.Quality == ItemTypeDefine.ITEM_QUALITY_TWO)
        {
             //只能镶嵌一颗宝石
            if(gemData.Count == 0)
            {
                _gemItems[0].SetItemState(1);
            }
            _gemItems[1].SetItemState(2);
            _gemItems[2].SetItemState(2);
        }
         else if (_itemData.Quality == ItemTypeDefine.ITEM_QUALITY_THREE || _itemData.Quality == ItemTypeDefine.ITEM_QUALITY_FOUR)
         {
             //有二个孔  宝石可能一号位 也可能二号位 也可能都有宝石
             if (gemData.Count == 0)
             {
                 _gemItems[0].SetItemState(1);
                 _gemItems[1].SetItemState(1);
             }
             else if (gemData.Count == 1)
             {
                 if (gemData.ContainsKey(1))
                 {
                     _gemItems[1].SetItemState(1);
                 }
                 else
                 {
                     _gemItems[0].SetItemState(1);
                 }
             }
             else if(gemData.Count == 2)
             {
                 if(_gemItems[0].Level == _gemItems[1].Level)
                 {
                     _gemItems[0].UpValue = "5%";
                     _gemItems[1].UpValue = "5%";
                 }
             }
             _gemItems[2].SetItemState(2);
         }
         else if (_itemData.Quality == ItemTypeDefine.ITEM_QUALITY_FIVE)
         {
             if (gemData.Count == 0)
             {
                 _gemItems[0].SetItemState(1);
                 _gemItems[1].SetItemState(1);
                 _gemItems[2].SetItemState(1);
             }
             else if (gemData.Count == 1)
             {
                 if (gemData.ContainsKey(1))
                 {
                     _gemItems[1].SetItemState(1);
                     _gemItems[2].SetItemState(1);
                 }
                 else if (gemData.ContainsKey(2))
                 {
                     _gemItems[0].SetItemState(1);
                     _gemItems[2].SetItemState(1);
                 }
                 else if (gemData.ContainsKey(3))
                 {
                     _gemItems[0].SetItemState(1);
                     _gemItems[1].SetItemState(1);
                 }
             }
             else if (gemData.Count == 2)
             {
                 if (!gemData.ContainsKey(1))
                 {
                     _gemItems[0].SetItemState(1);
                     if (_gemItems[1].Level == _gemItems[2].Level)
                     {
                         _gemItems[1].UpValue = "5%";
                         _gemItems[2].UpValue = "5%";
                     }
                 }
                 else if (!gemData.ContainsKey(2))
                 {
                     _gemItems[1].SetItemState(1);
                     if (_gemItems[0].Level == _gemItems[2].Level)
                     {
                         _gemItems[0].UpValue = "5%";
                         _gemItems[2].UpValue = "5%";
                     }
                 }
                 else if (!gemData.ContainsKey(3))
                 {
                     _gemItems[2].SetItemState(1);
                     if (_gemItems[0].Level == _gemItems[1].Level)
                     {
                         _gemItems[0].UpValue = "5%";
                         _gemItems[1].UpValue = "5%";                        
                     }
                 }
             }
             else if(gemData.Count == 3)
             {
                 if(_gemItems[0].Level == _gemItems[1].Level && _gemItems[1].Level == _gemItems[2].Level)
                 {
                     _gemItems[0].UpValue = "10%";
                     _gemItems[1].UpValue = "10%";
                     _gemItems[2].UpValue = "10%";
                 }
                 else if (_gemItems[0].Level == _gemItems[1].Level && _gemItems[1].Level != _gemItems[2].Level)
                 {
                     _gemItems[0].UpValue = "5%";
                     _gemItems[1].UpValue = "5%";
                 }
                 else if (_gemItems[0].Level == _gemItems[2].Level && _gemItems[0].Level != _gemItems[1].Level)
                 {
                     _gemItems[0].UpValue = "5%";
                     _gemItems[2].UpValue = "5%";
                 }
                 else if (_gemItems[1].Level == _gemItems[2].Level && _gemItems[0].Level != _gemItems[1].Level)
                 {
                     _gemItems[2].UpValue = "5%";
                     _gemItems[1].UpValue = "5%";
                 }
             }
         }
    }

    private void SetGemData(EquipDetailGemItem item, int gemID)
    {
        item.SetItemState(0);
        ItemsData itemdata = ItemsConfig.SharedInstance.GetItemData(gemID);
        if (itemdata == null) return;
        item.Icon = itemdata.icon;
        item.Desc = itemdata.description;
        item.Level = itemdata.level;
    }

    private void ShowOther()
    {
        _other.SetActive(true);
        _otherText.text = string.Format("耐久度:{0}/{1}\n出售价格:{2}", _itemData.Durability, _itemData.DurabilityMax, _itemData.Price);
    }
    private void ShowDesc()
    {
        _descText.text = ItemDisplayRule.GetItemDesc(_itemData);
    }

    private const float Max = 150;
    protected override void OnAdjustSize()
    {
        float value = (_srollView.bounds.size.y - _initHight) * 0.5f;
        float y = 0;
        if (value > 0)
        {
            y = value + 2;
            y = Mathf.Min(y, Max);
        }
        else
        {
            y = 0;
        }
        _top.localPosition = new Vector3(0, y, 0);
        _bottom.localPosition = new Vector3(0, -y, 0);
        SetState();
    }

    private void SetState()
    {
        switch (_sourcePage)
        {
            case SourcePage.Other:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 0);
                }
                _background.bottomAnchor.Set(0, -30);
                break;
            case SourcePage.kitbag:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 1);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Warehouse:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 2);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Avatar:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 3);
                }
                _background.bottomAnchor.Set(0, -90);
                break;
            case SourcePage.Goods:
                for (int i = 0; i < _states.Length; i++)
                {
                    _states[i].SetActive(i == 4);
                }
                _background.bottomAnchor.Set(0, -90);
                SetOperationMenu();
                break;
            default:
                break;
        }
    }
    private void SetOperationMenu()
    {
        HideOperationMenu();
        if (_itemData == null) return;
        List<byte> _operationList = _itemData.OperationList;
        if (_operationList.Count == 2)
        {
            for (int i = 0; i < _operationList.Count; i++)
            {
                GameObject btn = _operationBtns[i];
                byte key = _operationList[i];
                Operation operation;
                ItemOperationRule.TryGetOperationByKey(key, out operation);
                if (operation == null) continue;
                _operationBtnNames[i].text = operation.operationName;
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if (operation.operationAction != null)
                    {
                        operation.operationAction(_itemData);
                    }
                    UIManager.instance.CloseWindow(ParentUI.winID);
                });
            }
            return;
        }

        //超过两种操作
        int index = 0;
        for (int i = 0; i < MaxNum; i++)
        {
            //插入“更多”按钮
            GameObject btn = _operationBtns[i];
            if (i == 1)
            {
                _operationBtnNames[i].text = "更多";
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if (_show)
                    {
                        HideOperationMenu();
                    }
                    else
                    {
                        ShowOperationMenu(_operationList.Count);
                    }
                });
                continue;
            }

            if (index < _operationList.Count)
            {
                byte key = _operationList[index];
                Operation operation;
                ItemOperationRule.TryGetOperationByKey(key, out operation);
                if (operation == null) continue;
                _operationBtnNames[i].text = operation.operationName;
                InputManager.instance.AddClickListener(btn, (go) =>
                {
                    if (operation.operationAction != null)
                    {
                        operation.operationAction(_itemData);
                    }
                    UIManager.instance.CloseWindow(ParentUI.winID);
                });
            }
            index++;
        }
    }
    private bool _show = false;
    private void ShowOperationMenu(int endAt = 1)
    {
        _show = true;
        for (int i = MaxNum - 1; i >= 0; i--)
        {
            if (i <= endAt)
            {
                _operationBtns[i].SetActive(true);
            }
            else
            {
                _operationBtns[i].SetActive(false);
            }
        }
    }
    private void HideOperationMenu()
    {
        _show = false;
        for (int i = MaxNum - 1; i >= 0; i--)
        {
            if (i <= 1)
            {
                _operationBtns[i].SetActive(true);
            }
            else
            {
                _operationBtns[i].SetActive(false);
            }
        }
    }
}
