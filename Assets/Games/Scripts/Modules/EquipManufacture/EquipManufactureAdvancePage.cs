﻿using UnityEngine;
using System.Collections;
using Item;
using System;
using System.Collections.Generic;

public class EquipManufactureAdvancePage : WinPage
{
    private KBEngine.Avatar _avatar;
    private int _tipID;
    private bool _canAdvance;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitPage();

        EventManager.AddListener<Int32>(EventID.EVNT_SILVER_UPDATE, UpdateSilver);
    }

    private ItemEquipment _equipData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1)
        {
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }

        if (args[0] == null)
        {
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }
        _equipData = (ItemEquipment)args[0];
        RefreshConsumeItem();
        RefreshConsumeSilver();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private GameObject _advanceConsumeItem;
    private const int Count = 3;
    private EquipManufactureAdvanceConsumeItem[] _consumeItems = new EquipManufactureAdvanceConsumeItem[Count];

    private GameObject _successConsumeItem;
    private UISprite _successConsumeIcon;
    private UILabel _successConsumeName;

    private GameObject _succeedItem;
    private UISprite _succeedIcon;
    private UILabel _suceedName;

    private UILabel _costSilver;
    private UILabel _haveSilver;
    private UILabel _successTip;
    private void InitPage()
    {
        Transform consumeGrid = transform.Find("ConsumeGrid");
        _advanceConsumeItem = consumeGrid.gameObject;
        for (int i = 0; i < Count; i++)
        {
            EquipManufactureAdvanceConsumeItem item = consumeGrid.Find(i.ToString()).GetComponent<EquipManufactureAdvanceConsumeItem>();
            item.Init(ParentUI);
            _consumeItems[i] = item;
        }

        _successConsumeItem = transform.Find("SuccessConsumeItem").gameObject;
        _successConsumeIcon = transform.Find("SuccessConsumeItem/Icon").GetComponent<UISprite>();
        _successConsumeName = transform.Find("SuccessConsumeItem/Name").GetComponent<UILabel>();

        _succeedItem = transform.Find("SucceedItem").gameObject;
        _succeedIcon = transform.Find("SucceedItem/Icon").GetComponent<UISprite>();
        _suceedName = transform.Find("SucceedItem/Name").GetComponent<UILabel>();

        _costSilver = transform.Find("Cost/Value").GetComponent<UILabel>();
        _haveSilver = transform.Find("Have/Value").GetComponent<UILabel>();

        _successTip = transform.Find("Tip").GetComponent<UILabel>();

        GameObject previewBtn = transform.Find("PreviewBtn").gameObject;
        InputManager.instance.AddClickListener(previewBtn, (go) =>
        {
          
        });

        GameObject advancedBtn = transform.Find("AdvancedBtn").gameObject;
        InputManager.instance.AddClickListener(advancedBtn, (go) =>
        {
            if(!_canAdvance)
            {
                if (_tipID == Define.CONSUME_MATERIAL_NOT_ENOUGH)
                {
                    TipManager.instance.ShowTextTip(_tipID, materialName);
                }
                else
                {
                    TipManager.instance.ShowTextTip(_tipID);
                }
                return;
            }
            if (_avatar.equipModule.Equipped(_equipData.Order))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.BuildEquipmentInEquipmentBag(location, SuccessConsumeItemAdd);
            }
            else
            {
                _avatar.equipModule.BuildEquipmentInKitbag((Int16)_equipData.Order, SuccessConsumeItemAdd);
            }
        });
    }

    private void SetGameObjectActiveFalse()
    {
        _advanceConsumeItem.SetActive(false);
        _succeedItem.SetActive(false);
        _successConsumeItem.SetActive(false);
    }

    private string materialName; //材料不足的材料名
    EquipAdvanceData advanceData;

    private const int SuccessConsumeItemID = 30400003; //增加成功率物品
    private int SuccessConsumeItemAdd;
    private void RefreshConsumeItem()
    {
        _canAdvance = true;
        if(_equipData.Quality == 5)
        {
            _canAdvance = false;
            _tipID = Define.EQUIPMENT_QUALITY_MAX;
            SetGameObjectActiveFalse();
            return;
        }     
        advanceData = EquipmentAdvanceConfig.SharedInstance.GetEquipmentAdvanceData((Int32)_equipData.ID, _equipData.Quality);        
        if(advanceData == null)
        {
            return;
        }
        _advanceConsumeItem.SetActive(true);
        for(int i = 0; i < Count; i++)
        {
            List<int> material = new List<int>();
            if(i == 0)
            {
                material = advanceData.material1;
            }
            else if(i == 1)
            {
                material = advanceData.material2;
            }
            else if(i == 2)
            {
                material = advanceData.material3;
            }
            ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(material[0]);
            if(itemData == null)
            {
                Printer.LogWarning("ItemsConfig  item ID:" + material[0] + "not found");
                return;
            }
            List<ItemBase> bagItems = _avatar.kitbagModule.GetItemsByID(material[0]);
            _consumeItems[i].Name = ItemsConfig.SharedInstance.GetItemName(itemData);
            ParentUI.SetDynamicSpriteName(_consumeItems[i].Icon, itemData.icon);
            _consumeItems[i].Level = Convert.ToString(itemData.level);
            if(bagItems.Count == 0)
            {
                _consumeItems[i].Count = string.Format("{0}/{1}", 0, material[1]);
            }
            else
            {
                _consumeItems[i].Count = string.Format("{0}/{1}", bagItems[0].Amount, material[1]);
            }
            
            if (bagItems.Count == 0 || bagItems[0].Amount < material[1])
            {
                _tipID = Define.CONSUME_MATERIAL_NOT_ENOUGH;
                _canAdvance = false;
                materialName = itemData.name;
            }          
        }

        _succeedItem.SetActive(true);
        ParentUI.SetDynamicSpriteName(_succeedIcon, _equipData.Icon);
        _suceedName.text = ItemDisplayRule.GetItemName(_equipData, _equipData.Quality + 1);
        ItemDetailManager.instance.AddListenerItemDetail(_succeedItem, AnchorType.Center, SourcePage.Other, _equipData);

        List<ItemBase> bagSuccessItem = _avatar.kitbagModule.GetItemsByID(SuccessConsumeItemID);
        if (bagSuccessItem.Count == 0 || bagSuccessItem[0].Amount == 0)
        {
            _successConsumeItem.SetActive(false);
            SuccessConsumeItemAdd = 0;
        }
        else
        {
            _successConsumeItem.SetActive(true);
            SuccessConsumeItemAdd = 1;
            ItemsData SuccessItemData = ItemsConfig.SharedInstance.GetItemData(SuccessConsumeItemID);
            if (SuccessItemData == null) return;
            _successConsumeName.text = ItemsConfig.SharedInstance.GetItemName(SuccessItemData);
            ParentUI.SetDynamicSpriteName(_successConsumeIcon, SuccessItemData.icon);
        }
    }
    
    private void RefreshConsumeSilver()
    {
        if(advanceData != null)
        {
            _costSilver.text = Convert.ToString(advanceData.consumesilver);
            if (advanceData.consumesilver > _avatar.Silver)
            {
                if (_canAdvance)
                {
                    _tipID = Define.SILVER_NOT_ENOUGH;
                    _canAdvance = false;
                }
            }
        }
        else
        {
            _costSilver.text = "0";
        }
        _haveSilver.text = Convert.ToString(_avatar.Silver);
       
    }

    private void UpdateSilver(Int32 value)
    {
        if (!Active) return;
        RefreshConsumeSilver();
    }
}
