﻿using UnityEngine;
using System.Collections;

public class EquipInlayGemItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
        get
        {
            return _name.text;
        }
    }

    private UILabel _count;
    public string Count
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _count.gameObject.SetActive(false);
            }
            else
            {
                _count.text = value.ToString();
                _count.gameObject.SetActive(true);
            }
        }
    }

    private UILabel _desc;
    public string Desc
    {
        set
        {
            _desc.text = value.ToString();
        }
    }
    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }
    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _count = transform.Find("Count").GetComponent<UILabel>();
        _desc = transform.Find("Desc").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
    }
}
