﻿using UnityEngine;
using System.Collections;
using Item;
using System;

public class EquipInlayedItem : WinItem
{
    private Byte _index; //宝石孔索引，第几个宝石孔
    public Byte Index
    {
        set
        {
            _index = value;
        }
        get
        {
            return _index;
        }
    }

    private UISprite _haveIcon;
    public string Icon
    {
        set
        {
            _haveIcon.spriteName = value.ToString();
        }
    }
    private UILabel _haveName;
    public string Name
    {
        set
        {
            _haveName.text = value.ToString();
        }
    }

    private UILabel _haveDesc;
    public string Desc
    {
        set
        {
            _haveDesc.text = value.ToString();
        }
    }


    private UILabel _lockDesc;
    public string LockDesc
    {
        set
        {
            _lockDesc.text = value.ToString();
        }
    }

    private bool _canSelect = false; //是否可以选中
    public bool CanSelect
    {
        get
        {
            return _canSelect;
        }
    } 

    private GameObject _takeOffBtn;
    public GameObject TakeOffBtn
    {
        get
        {
            return _takeOffBtn;
        }
    }

    private GameObject _upAttribute;
    private UILabel _upValue;
    public string UpValue
    {
        set
        {
            if(string.IsNullOrEmpty(value))
            {
                _upAttribute.SetActive(false);
            }
            else
            {
                _upAttribute.SetActive(true);
                _upValue.text = value;
            }
        }
    }

    private int _level;
    public  int Level
    {
        get { return _level; }
        set {_level = value;}
    }

    private const int Num = 3;
    private GameObject[] _stateItem = new GameObject[Num];

    /// <summary>
    /// 0:have 1：empty 2:Lock
    /// </summary>
    /// <param name="state"></param>
    public  void SetItemState(int state)
    {
        for (int i = 0; i < Num; i++)
        {
            _stateItem[i].SetActive(i == state); 
        }
        if(state ==2)
        {
            _canSelect = false;
        }
        else
        {
            _canSelect = true;
        }
    }

    protected override void OnInit()
    {
        for (int i = 0; i < Num; i++)
        {
            GameObject item = transform.Find(i.ToString()).gameObject;
            _stateItem[i] = item;
        }      

        _haveIcon = transform.Find("0/Icon").GetComponent<UISprite>();
        _haveName = transform.Find("0/Name").GetComponent<UILabel>();
        _haveDesc = transform.Find("0/Desc").GetComponent<UILabel>();
        _takeOffBtn = transform.Find("0/TakeOffBtn").gameObject;
        _upAttribute = transform.Find("0/Up").gameObject;
        _upValue = transform.Find("0/Up/Value").GetComponent<UILabel>();
        _upAttribute.SetActive(false);

        _lockDesc = transform.Find("2/Desc").GetComponent<UILabel>();       
    }
}
