﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;
using LitJson;
using EQUIPMENT_BAG_TYPE = System.Collections.Generic.Dictionary<EQUIPMENT_LOCATION, Item.ItemEquipment>;
using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;

public class EquipGemMenuData
{
    public int gemType;
    public string name;
}
/// <summary>
/// 装备管理类
/// </summary>
public class EquipModule
{
    private KBEngine.Avatar _avatar;

    private Dictionary<string, EquipGemMenuData> _equipGemMenuData = new Dictionary<string, EquipGemMenuData>();  //宝石菜单列表数据
    public EquipModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;

        TextAsset textdata = AssetsManager.instance.SyncSpawnConfig("d_gem_type");
        _equipGemMenuData = JsonMapper.ToObject<Dictionary<string, EquipGemMenuData>>(textdata.text);
    }

    private EQUIPMENT_BAG_TYPE _equipments = new EQUIPMENT_BAG_TYPE();
    public EQUIPMENT_BAG_TYPE GetEquipments()
    { 
        return _equipments; 
    }

    public List<EquipGemMenuData> GemMenuData
    {
        get
        {
            return new List<EquipGemMenuData>(_equipGemMenuData.Values);
        }
    }

    /// <summary>
    /// 获取所有的装备
    /// </summary>
    /// <returns></returns>
    public List<ItemEquipment> AllEquipment()
    {
        List<ItemEquipment> equipment = new List<ItemEquipment>();
        List<ItemEquipment> bagData = _avatar.kitbagModule.EquipItems();
        equipment.AddRange(_equipments.Values);
        equipment.AddRange(bagData);
        return equipment;
    }

    /// <summary>
    /// 该装备是否已经装备
    /// </summary>
    /// <param name="order"></param>
    /// <returns></returns>
    public bool Equipped(ITEM_ORDER order)
    {
        foreach(ItemEquipment equip in _equipments.Values)
        {
            if(equip.Order == order)
            {
                return true;
            }
        }
        return false;
    }
  

    public ItemEquipment GetLocationItemEquipment(Byte location)
    {
        ItemEquipment equipment;
        _equipments.TryGetValue(location, out equipment);
        return equipment;
    }
    
    /// <summary>
    /// 查询装备的location
    /// </summary>
    /// <param name="equipment"></param>
    /// <returns></returns>
    public Byte GetEquipLocation(ItemEquipment equipment)
    {
        foreach(Byte key in _equipments.Keys)
        {
            if(_equipments[key] == equipment)
            {
                return key;
            }
        }
        return 255;
    }

    #region Avatar穿戴的装备数据更新
    public void recvEquipmentBag(JSON_DICT_TYPE data)
    {
        foreach (var eitem in (List<object>)data["equipments"])
        {
            Byte location = (Byte)(((JSON_DICT_TYPE)eitem)["location"]);
            object o = (((JSON_DICT_TYPE)eitem)["equipment"]);
            ItemEquipment equipment = ItemFactory.CreateItem((Dictionary<string, object>)o) as ItemEquipment;
            _equipments[location] = equipment;
        }
    }

    public void putOnEquipment(ITEM_ORDER order)
    {
        if (!_avatar.kitbagModule.OrderHasItem(order))
        {
            Printer.LogError("Avatar put on equipment of order {0}, but kitbag has no item in this order.", order);
        }
        else
        {
            ItemEquipment equipment = _avatar.kitbagModule.GetItemByOrder(order) as ItemEquipment;
            EQUIPMENT_LOCATION location;
            if (!EquipmentRule.GetEquipmentLocation(equipment, out location))
            {
                Printer.LogError("Equipment, no adjust location of equipment {0}", equipment.ID);
            }
            else
            {
                _equipments[location] = equipment;
                EventManager.Invoke<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_PUTON, location);
            }
        }
    }

    public void takeOffEquipment(EQUIPMENT_LOCATION location)
    {
        if (_equipments.ContainsKey(location))
        {
            _equipments.Remove(location);
            EventManager.Invoke<EQUIPMENT_LOCATION>(EventID.EVNT_EQUIP_TAKEOFF, location);
        }
        else
        {
            Printer.LogError("Equipment, no equipment in location {0}", location);
        }
    }

    public void updateEquipmentAttrInt32(EQUIPMENT_LOCATION location, Int32 attrIndex, Int32 value)
    {
        if (_equipments.ContainsKey(location))
        {
            UpdateItemAttribute(_equipments[location], attrIndex, value);
        }
        else
        {
            Printer.LogError("EquipmentAttrInt32, no equipment in location {0}", location);
        }
    }

    public void updateEquipmentAttrString(EQUIPMENT_LOCATION location, Int32 attrIndex, string value)
    {
        if (_equipments.ContainsKey(location))
        {
            UpdateItemAttribute(_equipments[location], attrIndex, value);
        }
        else
        {
            Printer.LogError("EquipmentAttrString, no equipment in location {0}", location);
        }
    }

    private bool UpdateItemAttribute(Item.ItemBase item, Int32 attrIndex, object value)
    {
        if (item == null) return false;
        string attribute = Item.ItemAttributes.Attribute(attrIndex);
        if (attribute == null) return false;
        if (item.UpdateAttribute(attribute, value))
        {
            EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, item.Order);
            return true;
        }
        return false;
    }
    #endregion

    #region 发送穿戴或者卸下装备请求
    public void SendPutOnEquipment(ItemEquipment equip)
    {
        if (equip == null) return;
        ITEM_ORDER order = _avatar.kitbagModule.GetOrderByItemBase(equip);
        _avatar.cellCall("putOnEquipment", (Int16)order);
    }

    public void SendTakeOffEquipment(ItemEquipment equip)
    {
        if (equip == null) return;
        Byte location;
        if(EquipmentRule.GetEquipmentLocation(equip, out location))
        {
            _avatar.cellCall("takeOffEquipment", location);
        }
    }
    #endregion

    #region 获取Avatar身上指定位置的装备
    public bool GetEquipmentInAvatar(EQUIPMENT_LOCATION location, out ItemEquipment equip)
    {
        if(_equipments.TryGetValue(location, out equip))
        {
            return true;
        }
        return false;
    }
    #endregion

    #region 特技洗练
    /// <summary>
    /// 洗练特技
    /// </summary>
    /// <param name="roder"></param>
    public void TryRenewingEquipmentSpecialSkill(ITEM_ORDER order)
    {
        Int16 orderId = (Int16)order;
        _avatar.cellCall("tryRenewingEquipmentSpecialSkill", new object[] { orderId });
    }

    /// <summary>
    /// 替换特技
    /// </summary>
    /// <param name="roder"></param>
    public void ReplaceEquipmentSpecialSkill(ITEM_ORDER order)
    {
        Int16 orderId = (Int16)order;
        _avatar.cellCall("replaceEquipmentSpecialSkill", new object[] { orderId });
    }
    #endregion

    #region 宝石合成
    /// <summary>
    /// 合成宝石
    /// </summary>
    /// <param name="order"></param>
    /// <param name="type">1: 合成一次，0：合成十次</param>
    public void GemCombine(ITEM_ORDER order, int type)
    {
        Int16 orderId = (Int16)order;
        _avatar.cellCall("GemCombine", new object[] { orderId, type });
    } 
    #endregion

    #region 装备修理
    /// <summary>
    /// 装备栏装备修理
    /// </summary>
    /// <param name="location"></param>
    public void RepairEquipmentInEquipmentBag(Byte location)
    {
        _avatar.cellCall("repairEquipmentInEquipmentBag", new object[] { location });
    }   

    /// <summary>
    /// 背包里面的物品修理
    /// </summary>
    /// <param name="order"></param>
    public void RepairEquipmentInKitbag(ITEM_ORDER order)
    {
        _avatar.cellCall("repairEquipmentInKitbag", new object[] { (Int16)order });
    }   
    #endregion

    /// <summary>
    /// 背包取下装备宝石
    /// </summary>
    /// <param name="order"></param>
    /// <param name="index"></param>
    public void TakeOffGemFromEquipmentInKitbag(ITEM_ORDER order, byte index)
    {
        Int16 orderId = (Int16)order;
        _avatar.cellCall("takeOffGemFromEquipmentInKitbag", new object[] { orderId, index});
    }

    /// <summary>
    /// 背包物品镶嵌宝石,替换宝石
    /// </summary>
    /// <param name="equipOrder"></param>
    /// <param name="gemOrder"></param>
    /// <param name="index"></param>
    public void InlayGemToEquipmentInKitbag(ITEM_ORDER equipOrder, ITEM_ORDER gemOrder, Byte index)
    {
        _avatar.cellCall("inlayGemToEquipmentInKitbag", new object[] { (Int16)equipOrder, (Int16)gemOrder, index });
    }

    /// <summary>
    /// 装备栏的宝石镶嵌
    /// </summary>
    /// <param name="location"></param>
    /// <param name="gemOrder"></param>
    /// <param name="index"></param>
    public void InlayGemToEquipmentInEquipmentBag(Byte location, ITEM_ORDER gemOrder, Byte index)
    {
        _avatar.cellCall("inlayGemToEquipmentInEquipmentBag", new object[] { location, (Int16)gemOrder, index });
    }

    public void TakeOffGemFromEquipmentInEquipmentBag(Byte location, Byte index)
    {
        _avatar.cellCall("takeOffGemFromEquipmentInEquipmentBag", new object[] {location, index});
    }

    /// <summary>
    /// 更新背包装备宝石属性
    /// </summary>
    /// <param name="order"></param>
    /// <param name="itemAttrsIndex"></param>
    /// <param name="value"></param>
    public void onItemAttrUpdatedDict(ITEM_ORDER order, Int32 itemAttrsIndex , string value)
    {
        ItemEquipment equipItem = _avatar.kitbagModule.GetItemByOrder(order) as ItemEquipment;
        string attribute = ItemAttributes.Attribute(itemAttrsIndex);
        if (attribute == null)
        {
            Printer.LogError("onItemAttrUpdatedDict: attribute index " + itemAttrsIndex.ToString() + " out of range.");
            return;
        }
        if (equipItem.UpdateAttribute(attribute, value))
        {
            EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, equipItem.Order);
        }
    }

    /// <summary>
    /// 更新装备栏装备的宝石属性
    /// </summary>
    /// <param name="location"></param>
    /// <param name="itemAttrsIndex"></param>
    /// <param name="value"></param>
    public void updateEquipmentAttrDict(EQUIPMENT_LOCATION location, Int32 itemAttrsIndex, string value)
    {
        ItemEquipment equipItem = _equipments[location];
        string attribute = ItemAttributes.Attribute(itemAttrsIndex);
        if (attribute == null)
        {
            Printer.LogError("onItemAttrUpdatedDict: attribute index " + itemAttrsIndex.ToString() + " out of range.");
            return;
        }
        if (equipItem.UpdateAttribute(attribute, value))
        {
            EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, equipItem.Order);
        }
    }

    /// <summary>
    /// 背包装备升星培养
    /// </summary>
    /// <param name="order"></param>
    /// <param name="type">true:培养一次，flase：培养十次</param>
    public void AscendStarOfEquipmentInKitbag(ITEM_ORDER order, int type)
    {
        _avatar.cellCall("ascendStarOfEquipmentInKitbag", new object[] { (Int16)order, type});
    }

    /// <summary>
    /// 装备栏装备升星培养
    /// </summary>
    /// <param name="location"></param>
    /// <param name="type"></param>
    public void AscendStarOfEquipmentInEquipmentBag(Byte location, int type)
    {
        _avatar.cellCall("ascendStarOfEquipmentInEquipmentBag", new object[] { location, type });
    }

    /// <summary>
    /// 背包装备属性洗练
    /// </summary>
    /// <param name="order"></param>
    /// <param name="locklist"></param>
    public void WashsEquipmentInKitbag(ITEM_ORDER order, List<object> locklist)
    {

        _avatar.cellCall("washsEquipmentInKitbag", new object[] { (Int16)order, locklist});
    }

    /// <summary>
    /// 背包装备替换洗练属性
    /// </summary>
    /// <param name="order"></param>
    public void ReplaceEquipmentExtraPropertiesInKitbag(ITEM_ORDER order)
    {
        _avatar.cellCall("replaceEquipmentExtraPropertiesInKitbag", new object[] { (Int16)order});
    }

    /// <summary>
    /// 装备栏装备属性洗练
    /// </summary>
    /// <param name="location"></param>
    /// <param name="locklist"></param>
    public void WashsEquipmentInEquipmentBag(Byte location, List<object> locklist)
    {
        _avatar.cellCall("washsEquipmentInEquipmentBag", new object[] { location, locklist});
    }

    /// <summary>
    /// 装备栏装备替换属性
    /// </summary>
    /// <param name="location"></param>
    public void ReplaceEquipmentExtraPropertiesInEquipmentBag(Byte location)
    {
        _avatar.cellCall("replaceEquipmentExtraPropertiesInEquipmentBag", new object[] {location});
    }

    /// <summary>
    /// 进阶背包里装备
    /// </summary>
    /// <param name="order"></param>
    /// <param name="luckyStone">1:有成功率物品 0:没有</param>
    public void BuildEquipmentInKitbag(ITEM_ORDER order, int luckyStone)
    {
        _avatar.cellCall("buildEquipmentInKitbag", new object[] { (Int16)order, luckyStone });
    }


    /// <summary>
    /// 进阶装备栏里装备
    /// </summary>
    /// <param name="location"></param>
    /// <param name="luckyStone"></param>
    public void BuildEquipmentInEquipmentBag(Byte location, int luckyStone)
    {
        _avatar.cellCall("buildEquipmentInEquipmentBag", new object[] { location, luckyStone });
    }


    /// <summary>
    /// 装备分解
    /// </summary>
    /// <param name="order"></param>  
    public void ResolveEquipment(ItemEquipment equip)
    {
        _avatar.cellCall("resolveEquipment", new object[] { (Int16)equip.Order });
    }
}
