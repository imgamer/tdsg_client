﻿using UnityEngine;
using System.Collections;

public class EquipLevelMenuItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
        get
        {
            return _name.text;
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
    }
}
