﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;

public class EquipWashsPage : WinPage
{
    protected override void OnInit()
    {
        InitTab();
    }

    private ItemEquipment _equipData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        if (args[0] == null) return;

        _equipData = (ItemEquipment)args[0];
        uiTab.SwitchTo(uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        
    }

    protected override void OnUnInit()
    {
        
    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(ParentUI, _equipData);
        };
    }
}
