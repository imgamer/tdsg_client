﻿using UnityEngine;
using System.Collections;

public class EquipManufactureStarValueItem : WinItem
{
    private UILabel _value;
    public string Value
    {
        set
        {
            _value.text = value.ToString();
        }
        get
        {
            return _value.text;
        }
    }
    private UILabel _up;
    public double Up
    {
        set
        {
            if (value == 0f)
            {
                _up.enabled = false;
            }
            else
            {
                _up.enabled = true;
                _up.text = "+" + (value * 100).ToString() + "%";
            } 
        }
    }

    protected override void OnInit()
    {
        _value = transform.Find("Value").GetComponent<UILabel>();
        _up = transform.Find("Up").GetComponent<UILabel>();
    }
}
