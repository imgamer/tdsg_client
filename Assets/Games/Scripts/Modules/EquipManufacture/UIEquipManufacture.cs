﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;
using LitJson;

using EQUIPMENT_BAG_TYPE = System.Collections.Generic.Dictionary<EQUIPMENT_LOCATION, Item.ItemEquipment>;

public class UIEquipManufacture : UIWin
{
    private KBEngine.Avatar _avatar;

    private GameObject _equipSelect;
    private List<string> levelMenu = new List<string>(new string[] { "10级", "20级", "30级", "40级", "50级", "60级", "70级", "80级", "90级", "100级" });
    private int _currentSelectLevel = 0; //当前等级菜单选择的等级项
    private bool _equipped = false; //是否加载装备栏中的装备

    private bool _inited = false; //是否初始化过装备列表

    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });
       
        InitTab();
        InitEquipSelect();

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, UpdateItem);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItem);
    }

    protected override void OnOpen(params object[] args)
    {
        uiTab.SwitchTo(0);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, UpdateItem);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, UpdateItem);
    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();           
            if(index == 2)
            {
                _equipSelect.SetActive(false);
                curContent.Open(this);
            }
            else if(index == 1|| index ==0)
            {
                _equipSelect.SetActive(true);
                _equipped = true;
                //_inited = false;
                RefreshEquipItem(_currentSelectLevel, _equipped);
                curContent.Open(this, _selectEquipData);
            }
            else
            {
                _equipSelect.SetActive(true);
                _equipped = false;
                //_inited = false;
                RefreshEquipItem(_currentSelectLevel, _equipped);
                curContent.Open(this, _selectEquipData);
            }
        };
    }

    private UI.Grid _grid;
    private GameObject _tempItem;
    private GameObject _selectBg;

    private UILabel _menuLabel;
    private GameObject _menuObj;

    private UI.Grid _levelGrid;
    private GameObject _tempLevelItem;
    private void InitEquipSelect()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _equipSelect = center.Find("EquipSelect").gameObject;
        _equipSelect.SetActive(false);

        _grid = center.Find("EquipSelect/Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("EquipSelect/Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        _selectBg = center.Find("EquipSelect/SelectBg").gameObject;
        _selectBg.SetActive(false);

        GameObject levelMenuBtn = center.Find("EquipSelect/LevelMenuBtn").gameObject;
        _menuLabel = center.Find("EquipSelect/LevelMenuBtn/Label").GetComponent<UILabel>();
        _menuObj = center.Find("EquipSelect/LevelMenu").gameObject;
        _levelGrid = _menuObj.transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempLevelItem = _menuObj.transform.Find("Scroll/Grid/Item").gameObject;
        _tempLevelItem.SetActive(false);

        InitLevelMeau();
        _menuObj.SetActive(false);
        InputManager.instance.AddClickListener(levelMenuBtn, (go) =>
        {
            if (_menuObj.activeSelf)
            {
                _menuObj.SetActive(false);
            }
            else
            {
                _menuObj.SetActive(true);
            }
        });
    }

    private List<EquipLevelMenuItem> _levelMenuItems = new List<EquipLevelMenuItem>();
    private void InitLevelMeau()
    {
        int dataCount = levelMenu.Count;
        int itemCount = _levelMenuItems.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetLevelItemData(_levelMenuItems[i], levelMenu[i]);
                    _levelMenuItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempLevelItem.SetActive(true);
                    GameObject item = Instantiate(_tempLevelItem) as GameObject;
                    EquipLevelMenuItem levelItem = item.GetComponent<EquipLevelMenuItem>();
                    levelItem.Init(this);
                    _tempLevelItem.SetActive(false);
                    item.transform.parent = _levelGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _levelMenuItems.Add(levelItem);
                    SetLevelItemData(levelItem, levelMenu[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetLevelItemData(_levelMenuItems[i], levelMenu[i]);
                    _levelMenuItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _levelMenuItems[i].gameObject.SetActive(false);
                }
            }
        }
        _levelGrid.Reposition();
    }

    private void SetLevelItemData(EquipLevelMenuItem item, string menuName)
    {
        item.Name = menuName;
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _menuLabel.text = menuName;
            _currentSelectLevel = Convert.ToInt32(menuName.Replace("级", ""));
            RefreshEquipItem(_currentSelectLevel, _equipped);
            _inited = false; //重新选择等级能默认选择第一个装备
            _menuObj.SetActive(false);
        });
    }

    private ItemEquipment _selectEquipData;
    private List<EquipManufactureItem> _equipItems = new List<EquipManufactureItem>();
    /// <summary>
    /// 加载装备列表
    /// </summary>
    /// <param name="level">当前等级选项 0代表全部</param>
    /// <param name="equipped">是否加载已装备的装备（true加载）</param>
    private void RefreshEquipItem(int level, bool equipped)
    {
        List<ItemEquipment> data = new List<ItemEquipment>();
        if(equipped)
        {
            List<ItemEquipment> itemData = _avatar.equipModule.AllEquipment();
            if(level== 0)
            {
                data = itemData;
            }
            else
            {
                foreach(ItemEquipment equip in itemData)
                {
                    if(equip.Level == level)
                    {
                        data.Add(equip);
                    }
                }
            }
        }
        else
        {
            if (level == 0)
            {
                data =_avatar.kitbagModule.EquipItems();
            }
            else
            {
                data = _avatar.kitbagModule.EquipLevelItems(level);
            }     
        }        
     
        int dataCount = data.Count;
        int itemCount = _equipItems.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_equipItems[i], data[i]);
                    _equipItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    EquipManufactureItem equipItem = item.GetComponent<EquipManufactureItem>();
                    equipItem.Init(this);
                    _tempItem.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _equipItems.Add(equipItem);
                    SetItemData(equipItem, data[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_equipItems[i], data[i]);
                    _equipItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _equipItems[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();

        //默认选中第一个的数据
        if(!_inited)
        {
            if (data.Count > 0)
            {
                OpenSelectItemData(_equipItems[0], data[0]);
            }
            _inited = true;
        }
       
    }

    private void SetItemData(EquipManufactureItem item, ItemEquipment data)
    {
        if (_avatar.equipModule.Equipped(data.Order))
        {
            item.Equipped = true;
            item.Location = _avatar.equipModule.GetEquipLocation(data);
            item.Order = 0;
        }
        else
        {
            item.Equipped = false;
            item.Order = (Int16)data.Order;
            item.Location = 255; //防止查询出错
        }
        item.Name = ItemDisplayRule.GetItemName(data);
        SetDynamicSpriteName(item.Icon, data.Icon);
        item.Icon.gameObject.SetActive(true);
        item.Level = Convert.ToString(data.Level);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            OpenSelectItemData(item, data);
        });
    }

    private void SelectItem(EquipManufactureItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void OpenSelectItemData(EquipManufactureItem item, ItemEquipment data)
    {
        SelectItem(item);
        _selectEquipData = data;
        curContent.Open(this, _selectEquipData);           
    }

    private void UpdateItem(object o)
    {
        if (!Active) return;
        if (!_equipSelect.activeSelf ) return;

        RefreshEquipItem(_currentSelectLevel, _equipped);

        if (_selectEquipData == null) return;

        ItemEquipment data = new ItemEquipment();
        if (_avatar.equipModule.Equipped(_selectEquipData.Order))
        {
            Byte location =_avatar.equipModule.GetEquipLocation(_selectEquipData);
            if (location == 255) return;
            foreach (EquipManufactureItem item in _equipItems)
            {
                if (item.Location == location && item.gameObject.activeSelf)
                {
                    SelectItem(item);
                }
            }
            data =_avatar.equipModule.GetLocationItemEquipment(location);
        }
        else
        {
            foreach (EquipManufactureItem item in _equipItems)
            {
                if (item.Order == (Int16)_selectEquipData.Order && item.gameObject.activeSelf)
                {
                    SelectItem(item);
                }
            }
            data =_avatar.kitbagModule.GetItemByOrder(_selectEquipData.Order) as ItemEquipment;
        }

        curContent.Open(this, data);
    }
}
