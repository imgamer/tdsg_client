﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;
using LitJson;

public class EquipWashsSkillPage : WinPage
{
    private KBEngine.Avatar _avatar;
    private bool _canWashs = false; //是否可以进行洗练

    private int _tipID;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitPage();
    }

    private ItemEquipment _equipData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1)
        {           
            _canWashs = false;
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }

        if (args[0] == null)
        {          
            _canWashs = false;
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }

        _equipData = (ItemEquipment)args[0];
        if (_equipData.Quality < 2)
        {
            SetGameObjectActiveFalse();
            _canWashs = false;
            _tipID = Define.EQUIPMENT_WASHSKILL_NOT_ALLOW;
            return;
        }

        RefreshConsumeItem();
        RefreshOldSkillItem();
        RefreshNewSkillItem();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        
    }

    private GameObject _consumeItem;
    private UISprite _consumeIcon;
    private UILabel _consumeName;
    private UILabel _consumeCount;

    private GameObject _oldSkillItem;
    private UISprite _oldSkillIcon;
    private UILabel _oldSkillName;

    private GameObject _newSkillItem;
    private UISprite _newSkillIcon;
    private UILabel _newSkillName;
    private void InitPage()
    {
        _consumeItem = transform.Find("ConsumeItem").gameObject;
        _consumeIcon = transform.Find("ConsumeItem/Icon").GetComponent<UISprite>();
        _consumeName = transform.Find("ConsumeItem/Name").GetComponent<UILabel>();
        _consumeCount = transform.Find("ConsumeItem/Count").GetComponent<UILabel>();
        _consumeItem.SetActive(false);

        _oldSkillItem = transform.Find("Old/SkillItem").gameObject;
        _oldSkillIcon = transform.Find("Old/SkillItem/Icon").GetComponent<UISprite>();
        _oldSkillName = transform.Find("Old/SkillItem/Name").GetComponent<UILabel>();
        _oldSkillItem.SetActive(false);

        _newSkillItem = transform.Find("New/SkillItem").gameObject;
        _newSkillIcon = transform.Find("New/SkillItem/Icon").GetComponent<UISprite>();
        _newSkillName = transform.Find("New/SkillItem/Name").GetComponent<UILabel>();
        _newSkillItem.SetActive(false);

        GameObject washBtn = transform.Find("WashBtn").gameObject;
        InputManager.instance.AddClickListener(washBtn, (go) =>
        {
            if (!_canWashs)
            {
                if (_tipID == Define.CONSUME_MATERIAL_NOT_ENOUGH)
                {
                    TipManager.instance.ShowTextTip(_tipID, _consumeName.text);
                }
                else
                {
                    TipManager.instance.ShowTextTip(_tipID);
                }
            }
            else
            {
                _avatar.equipModule.TryRenewingEquipmentSpecialSkill(_equipData.Order);
            }
        });

        GameObject replaceBtn = transform.Find("ReplaceBtn").gameObject;
        InputManager.instance.AddClickListener(replaceBtn, (go) =>
        {
            if (_equipData == null || _equipData.NewSpecialSkill == 0)
            {
                return;
            }
            _avatar.equipModule.ReplaceEquipmentSpecialSkill(_equipData.Order);
        });
    }

    private void SetGameObjectActiveFalse()
    {
        _consumeItem.SetActive(false);
        _oldSkillItem.SetActive(false);
        _newSkillItem.SetActive(false);
    }

    private void RefreshConsumeItem()
    {
        EquipmentSpecialSkillConsumeData consumeData = EquipmentRenewSpecialSkillConsumeConfig.SharedInstance.GetSpecialSkillConsumeData(_equipData.Quality);
        if (consumeData == null) return;
        _consumeCount.text = "x" + Convert.ToString(consumeData.amount);
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(consumeData.itemID);
        if (itemData == null) return;
        _consumeName.text = Convert.ToString(itemData.name);
        ParentUI.SetDynamicSpriteName(_consumeIcon, itemData.icon);
        _consumeItem.SetActive(true);

        List<ItemBase> bagItems = _avatar.kitbagModule.GetItemsByID(consumeData.itemID);
        if (bagItems.Count == 0 || bagItems[0].Amount < consumeData.amount)
        {
            _canWashs = false;
            _tipID = Define.CONSUME_MATERIAL_NOT_ENOUGH;
            return;
        }
        _canWashs = true;
    }

    private void RefreshOldSkillItem()
    {
        if (_equipData.SpecialSkill != 0)
        {
            JsonData skillData = SkillConfig.SharedInstance.GetJsonData(_equipData.SpecialSkill);
            _oldSkillIcon.spriteName = (string)skillData["icon"];
            _oldSkillName.text = (string)skillData["name"];
            _oldSkillItem.SetActive(true);
        }
        else
        {
            _oldSkillItem.SetActive(false);
        }
    }

    private void RefreshNewSkillItem()
    {
        if (_equipData.NewSpecialSkill != 0)
        {
            JsonData skillData = SkillConfig.SharedInstance.GetJsonData(_equipData.NewSpecialSkill);
            _newSkillIcon.spriteName = (string)skillData["icon"];
            _newSkillName.text = (string)skillData["name"];
            _newSkillItem.SetActive(true);
        }
        else
        {
            _newSkillItem.SetActive(false);
        }
    }
}

