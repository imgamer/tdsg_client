﻿using UnityEngine;
using System.Collections;

public class EquipManufactureAdvanceConsumeItem : WinItem
{

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = "LV." + value.ToString();
        }
    }
    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }

    private UILabel _count;
    public string Count
    {
        set
        {
            _count.text = value;
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _count = transform.Find("Count").GetComponent<UILabel>();
    }
}
