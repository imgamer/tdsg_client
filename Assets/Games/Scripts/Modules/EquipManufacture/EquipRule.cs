﻿using System;
using Item;

public static class EquipRule 
{
    static EquipRule()
    {

    }

    /// <summary>
    /// 是否可以镶嵌
    /// </summary>
    /// <param name="equip"></param>
    /// <returns></returns>
    public static bool Inlay(ItemEquipment equip)
    {
        if(equip.Quality == ItemTypeDefine.ITEM_QUALITY_ONE || equip.Level == 10)
        {
            return false;
        }
        return true;
    }

    private const int EQUAL_THAN = 0;
    private const int GREATER_THAN = 1;
    private const int LESS_THAN = 2;
    /// <summary>
    /// 比较当前装备和装备中装备的属性
    /// </summary>
    /// <param name="avatarEquip"></param>
    /// <param name="bagEuip"></param>
    /// <returns></returns>
    public static int ComparePorperty(PropertyBase avatarEquip, PropertyBase bagEuip)
    {
        int avatarProperty = Convert.ToInt32(avatarEquip.Value);
        int bagProperty = Convert.ToInt32(bagEuip.Value);
        if (avatarProperty < bagProperty) return GREATER_THAN;
        else if (avatarProperty > bagProperty) return LESS_THAN;
        else return EQUAL_THAN;
    }
   }
