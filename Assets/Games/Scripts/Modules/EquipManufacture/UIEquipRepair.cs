﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;

public class UIEquipRepair : UIWin
{
    private KBEngine.Avatar _avatar;
    private List<EquipRepairItem> _repairItems = new List<EquipRepairItem>();

    private bool _canRepair = true;
    private int _tipID;

    private ItemEquipment _selectData; //打开选择的默认数据
    private const int _repairItemID = 30300001; //修理需要消耗的道具ID，临时
    
    protected override void OnInit()
    {
        InitPage();
        _avatar = GameMain.Player;

        EventManager.AddListener<Int32>(EventID.EVNT_SILVER_UPDATE, UpdateSilver);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, RefreshPage);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, RefreshPage);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        _selectBg.SetActive(false);
        _selectData = null;
        _canRepair = true;
        RefreshItem();
        _have.text = Convert.ToString(_avatar.Silver);
    }

    protected override void OnClose()
    {
        
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<Int32>(EventID.EVNT_SILVER_UPDATE, UpdateSilver);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, RefreshPage);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, RefreshPage);
    }

    private UI.Grid _grid;
    private GameObject _tempItem;
    private GameObject _selectBg;

    private GameObject _repairInfoItem;
    private UISprite _repairIcon;
    private UILabel _repairName;
    private UILabel _durability;

    private UILabel _cost;
    private UILabel _have;

    private GameObject _consumeItem;
    private UISprite _consumeIcon;
    private UILabel _consumeLevel;
    private UILabel _consumeName;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        GameObject closeBtn = center.Find("CloseBtn").gameObject;
        InputManager.instance.AddClickListener(closeBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        _grid = center.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        _selectBg = center.Find("SelectBg").gameObject;
        _selectBg.SetActive(false);

        _repairInfoItem = center.Find("Repair/Item").gameObject;
        _repairIcon = center.Find("Repair/Item/Icon").GetComponent<UISprite>();
        _repairName = center.Find("Repair/Item/Name").GetComponent<UILabel>();
        _durability = center.Find("Repair/Item/Durability").GetComponent<UILabel>();

        

        _repairInfoItem.SetActive(false);

        _cost = center.Find("Repair/Cost/Cost").GetComponent<UILabel>();
        _have = center.Find("Repair/Have/Have").GetComponent<UILabel>();

        GameObject repairBtn = center.Find("Repair/RepairBtn").gameObject;
        InputManager.instance.AddClickListener(repairBtn, (go) =>
        {
            if(!_canRepair)
            {
                if (_tipID == Define.EQUIPMENT_DURABILITY_MAX || _tipID == Define.SILVER_NOT_ENOUGH || _tipID == Define.EQUIPMENT_NOT_SELECT)
                {
                    TipManager.instance.ShowTextTip(_tipID);
                }
                else if (_tipID == Define.CONSUME_MATERIAL_NOT_ENOUGH)
                {
                    TipManager.instance.ShowTextTip(_tipID, _consumeName.text);
                }
                return;
            }

            if (_avatar.equipModule.Equipped(_selectData.Order))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_selectData);
                if (location == 255) return;
                _avatar.equipModule.RepairEquipmentInEquipmentBag(location);
            }
            else
            {
                _avatar.equipModule.RepairEquipmentInKitbag(_selectData.Order);
            } 
        });

        _consumeItem = center.Find("Consume/Item").gameObject;
        _consumeIcon = center.Find("Consume/Item/Icon").GetComponent<UISprite>();
        _consumeLevel = center.Find("Consume/Item/Level").GetComponent<UILabel>();
        _consumeName = center.Find("Consume/Item/Name").GetComponent<UILabel>();
        _consumeItem.SetActive(false);
    }

    private void RefreshItem()
    {
        List<ItemEquipment> data = _avatar.equipModule.AllEquipment();       
        int dataCount = data.Count;
        if(dataCount == 0)
        {
            ShowEmpty();
        }

        int itemCount = _repairItems.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_repairItems[i], data[i]);
                    _repairItems[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    EquipRepairItem repairItem = item.GetComponent<EquipRepairItem>();
                    repairItem.Init(this);
                    _tempItem.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _repairItems.Add(repairItem);
                    SetItemData(repairItem, data[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_repairItems[i], data[i]);
                }
                else
                {
                    _repairItems[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
        
        if(dataCount > 0 && _selectData ==null)
        {
            OpenSelectItemData(_repairItems[0], data[0]);
            _grid.GoToItem(0, dataCount);
        }        
    }

    private void SetItemData(EquipRepairItem item, ItemEquipment data)
    {
        if (_avatar.equipModule.Equipped(data.Order))
        {
            item.Equipped = true;
            item.Location = _avatar.equipModule.GetEquipLocation(data);
            item.Order = 0;
        }
        else
        {
            item.Equipped = false;
            item.Order = (Int16)data.Order;
            item.Location = 255; //防止查询出错
        }
        item.Name = data.Name;
        SetDynamicSpriteName(item.Icon, data.Icon);
        item.Icon.gameObject.SetActive(true);
        item.Level = Convert.ToString(data.Level);
        item.Durability = string.Format("{0}/{1}", data.Durability, data.DurabilityMax);
        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {           
            OpenSelectItemData(item, data);
        });
    }

    private void OpenSelectItemData(EquipRepairItem item, ItemEquipment data)
    {
        _selectData = data;
        SelectItem(item);
        UpdateRepairInfo(data);
        UpdateConsumeInfo(data);   
    }

    private void UpdateRepairInfo(ItemEquipment data)
    {
        ItemDetailManager.instance.AddListenerItemDetail(_repairInfoItem, AnchorType.Center, SourcePage.Other, data);
        SetDynamicSpriteName(_repairIcon, data.Icon);
        _repairName.text = data.Name;
        _durability.text = string.Format("{0}/{1}", data.Durability, data.DurabilityMax);
        _repairInfoItem.SetActive(true);

        int costSilver = RepairCostSilver(data.Level, data.Quality, data.DurabilityMax - data.Durability);
        _cost.text = Convert.ToString(costSilver);

        if (_avatar.Silver < costSilver)
        {
            _tipID = Define.SILVER_NOT_ENOUGH;
            _canRepair = false;
            return;
        }

        if (data.Durability >= data.DurabilityMax)
        {
            _tipID = Define.EQUIPMENT_DURABILITY_MAX;
            _canRepair = false;
        }
        else
        {
            _canRepair = true;
        }
        
    }

    private void UpdateConsumeInfo(ItemEquipment data)
    {
        if (data.Quality >= 4)
        {
            _consumeItem.SetActive(true);
            //临时
            ItemsData itemsData = ItemsConfig.SharedInstance.GetItemData(_repairItemID);
            _consumeName.text = itemsData.name;
            SetDynamicSpriteName(_consumeIcon, itemsData.icon);
            _consumeLevel.text = "LV." + Convert.ToString(itemsData.level);

            if (!_avatar.kitbagModule.HasItem(_repairItemID))
            {
                _tipID = Define.CONSUME_MATERIAL_NOT_ENOUGH;
                _canRepair = false;
            }
        }
        else
        {
            _consumeItem.SetActive(false);
        }
        
    }

    //修理费用=装备等级*品质系数*耐久度恢复值。
    private int RepairCostSilver(int level, int quality, int value)
    {
        return level * quality * value;
    }

    private void SelectItem(EquipRepairItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void ShowEmpty()
    {
        _canRepair = false;
        _tipID = Define.EQUIPMENT_NOT_SELECT;
        _cost.text = "0";
        _selectBg.SetActive(false);
        _repairInfoItem.SetActive(false);
        _consumeItem.SetActive(false);
    }

    private void UpdateSilver(Int32 o)
    {
        if (!Active) return;
        _have.text = Convert.ToString(o);
    }

    private void RefreshPage(object o)
    {
        if (!Active) return;
        RefreshItem();

        ItemEquipment data;
        if (_avatar.equipModule.Equipped(_selectData.Order))
        {
            Byte location = _avatar.equipModule.GetEquipLocation(_selectData);
            if (location == 255) return;
            foreach (EquipRepairItem item in _repairItems)
            {
                if (item.Location == location && item.gameObject.activeSelf)
                {
                    SelectItem(item);
                }
            }
            data = _avatar.equipModule.GetLocationItemEquipment(location);
        }
        else
        {
            foreach (EquipRepairItem item in _repairItems)
            {
                if (item.Order == (Int16)_selectData.Order && item.gameObject.activeSelf)
                {
                    SelectItem(item);
                }
            }
            data = _avatar.kitbagModule.GetItemByOrder(_selectData.Order) as ItemEquipment;
        }

        _canRepair = true;
        UpdateRepairInfo(data);
        UpdateConsumeInfo(data); 
    }
}
