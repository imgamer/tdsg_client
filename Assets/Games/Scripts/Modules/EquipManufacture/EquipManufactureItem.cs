﻿using UnityEngine;
using System.Collections;
using System;

public class EquipManufactureItem : WinItem
{
    private Int16 _order;
    public Int16 Order
    {
        get
        {
            return _order;
        }
        set
        {
            _order = value;
        }
    }

    private Byte _location;
    public Byte Location
    {
        get
        {
            return _location;
        }
        set
        {
            _location = value;
        }
    }

    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value.ToString();
        }
    }

    private UILabel _level;
    public string Level
    {
        set
        {
            _level.text = "LV." + value.ToString();
        }
    }
    private UISprite _icon;
    public UISprite Icon
    {
        get
        {
            return _icon;
        }
    }

    private GameObject _equipped;
    public bool Equipped
    {
        set
        {
            _equipped.SetActive(value);
        }
    }
    
    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _level = transform.Find("Level").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _equipped = transform.Find("Equipped").gameObject;
        _equipped.SetActive(false);
    }
}
