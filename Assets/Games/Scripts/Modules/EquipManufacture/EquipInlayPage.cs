﻿using UnityEngine;
using System.Collections;
using Item;
using System.Collections.Generic;
using System;
using LitJson;
public class EquipInlayPage : WinPage
{
    private KBEngine.Avatar _avatar;

    private int _gemLimitLevel; //当前装备镶嵌宝石等级的限制
    protected override void OnInit()
    {
        _avatar = GameMain.Player;

        InitPage();
    }

    private ItemEquipment _equipData;
    private ITEM_ORDER _equipDataOrder = 0;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        if (args[0] == null) return;

        _equipData = (ItemEquipment)args[0];
        if ((Int16)_equipDataOrder != (Int16)_equipData.Order)
        {
            _inlaySelectIndex = 0;
            _equipDataOrder = _equipData.Order;
        }     

        _gemLimitLevel = (int)_equipData.Level / 10;
        RefreshEquipInfo();
        RefreshInlayedItem();
        RefreshGemMenu();
    }

    protected override void OnRefresh()
    {
        
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UILabel _equipName;
    private UISprite _EquipIcon;

    private UI.Table _uiTable;
    private EquipInlayMenuItem _templateItem;
    private GameObject _selectItem;
    private GameObject _selectMenu;
    private void InitPage()
    {
        _equipName = transform.Find("EquipItem/Name").GetComponent<UILabel>();
        _EquipIcon = transform.Find("EquipItem/Icon").GetComponent<UISprite>();

        InitInlayedGemItem();

        Transform table = transform.Find("GemMenu/UITable");
        _uiTable = table.GetComponent<UI.Table>();
        _templateItem = _uiTable.transform.Find("Item").GetComponent<EquipInlayMenuItem>();
        _templateItem.Init(ParentUI);
        _templateItem.gameObject.SetActive(false);
        _selectItem = transform.Find("GemMenu/SelectItem").gameObject;
        _selectItem.SetActive(false);
        _selectMenu = transform.Find("GemMenu/SelectMenu").gameObject;
        _selectMenu.SetActive(false);
    }

    private const int Count = 3;
    private EquipInlayedItem[] _inlayedItems = new EquipInlayedItem[Count];
    private GameObject _inlaySelect;
    private int _inlaySelectIndex = 0; //当前选择的索引号

    private EquipInlayedItem _selectInlayedItem; //当前选择的宝石孔
    private void InitInlayedGemItem()
    {
        _inlaySelect = transform.Find("InlaySelect").gameObject;
        _inlaySelect.SetActive(false);
        Transform inlayed = transform.Find("Inlayed");
        for(int i = 0; i < Count; i++)
        {
            EquipInlayedItem item = inlayed.Find(i.ToString()).GetComponent<EquipInlayedItem>();
            item.Init(ParentUI);
            _inlayedItems[i] = item;
            item.Index = (byte)(i + 1);
            InputManager.instance.AddClickListener(item.gameObject, (go) =>
            {
                if (!item.CanSelect)
                {
                    return;
                }
                SetSelectInlayedItem(item);
            });
        }
    }

    private void RefreshInlayedItem()
    {
        _selectInlayedItem = null;
        _inlaySelect.SetActive(false);
        _inlayedItems[0].UpValue = "";
        _inlayedItems[1].UpValue = "";
        _inlayedItems[2].UpValue = "";

        Dictionary<int, int> gemData = _equipData.EquipGem;
        foreach (int key in gemData.Keys)
        {
            SetInlayedItemData(_inlayedItems[ key - 1], gemData[key]);
        }   
        //SetItemState状态 0：有宝石 ；1：解锁但没有宝石； 2：锁住。
        if(_equipData.Quality == ItemTypeDefine.ITEM_QUALITY_ONE)
        {
            _inlayedItems[0].SetItemState(2);
            _inlayedItems[1].SetItemState(2);
            _inlayedItems[2].SetItemState(2);
            _inlayedItems[0].LockDesc = "绿色装备解锁";
            _inlayedItems[1].LockDesc = "蓝色装备解锁";
            _inlayedItems[2].LockDesc = "橙色装备解锁";
            return;
        }
        else if (_equipData.Quality == ItemTypeDefine.ITEM_QUALITY_TWO)
        {
            if(gemData.Count == 0)
            {
                _inlayedItems[0].SetItemState(1);
            }
            _inlayedItems[1].SetItemState(2);
            _inlayedItems[2].SetItemState(2);
            _inlayedItems[1].LockDesc = "蓝色装备解锁";
            _inlayedItems[2].LockDesc = "橙色装备解锁";
            
        }
        else if (_equipData.Quality == ItemTypeDefine.ITEM_QUALITY_THREE || _equipData.Quality == ItemTypeDefine.ITEM_QUALITY_FOUR)
        {            
            if(gemData.Count == 0)
            {
                _inlayedItems[0].SetItemState(1);
                _inlayedItems[1].SetItemState(1);
            }
            else if (gemData.Count == 1)
            {
                //有二个孔  宝石可能一号位 也可能二号位 也可能都有宝石
                if (gemData.ContainsKey(1))
                {
                    _inlayedItems[1].SetItemState(1);
                }
                else
                {
                    _inlayedItems[0].SetItemState(1);
                }                                                                                  
            }
            else if(gemData.Count == 2)
            {
                if(_inlayedItems[0].Level == _inlayedItems[1].Level)
                {
                    _inlayedItems[0].UpValue = "5%";
                    _inlayedItems[1].UpValue = "5%";
                }
            }
            _inlayedItems[2].SetItemState(2);
            _inlayedItems[2].LockDesc = "橙色装备解锁";
        }
        else if (_equipData.Quality == ItemTypeDefine.ITEM_QUALITY_FIVE)
        {
            if (gemData.Count == 0)
            {
                _inlayedItems[0].SetItemState(1);
                _inlayedItems[1].SetItemState(1);
                _inlayedItems[2].SetItemState(1);
            }
            else if (gemData.Count == 1)
            {
                if (gemData.ContainsKey(1))
                {
                    _inlayedItems[1].SetItemState(1);
                    _inlayedItems[2].SetItemState(1);
                }
                else if (gemData.ContainsKey(2))
                {
                    _inlayedItems[0].SetItemState(1);
                    _inlayedItems[2].SetItemState(1);
                }
                else if (gemData.ContainsKey(3))
                {
                    _inlayedItems[0].SetItemState(1);
                    _inlayedItems[1].SetItemState(1);
                }
            }
            else if (gemData.Count == 2)
            {
                if (!gemData.ContainsKey(1))
                {
                    _inlayedItems[0].SetItemState(1);
                    if (_inlayedItems[1].Level == _inlayedItems[2].Level)
                    {
                        _inlayedItems[1].UpValue = "5%";
                        _inlayedItems[2].UpValue = "5%";
                    }
                }
                else if (!gemData.ContainsKey(2))
                {
                    _inlayedItems[1].SetItemState(1);
                    if (_inlayedItems[0].Level == _inlayedItems[2].Level)
                    {
                        _inlayedItems[0].UpValue = "5%";
                        _inlayedItems[2].UpValue = "5%";
                    }
                }
                else if (!gemData.ContainsKey(3))
                {
                    _inlayedItems[2].SetItemState(1);
                    if (_inlayedItems[0].Level == _inlayedItems[1].Level)
                    {
                        _inlayedItems[0].UpValue = "5%";
                        _inlayedItems[1].UpValue = "5%";
                    }
                }
            }
            else if(gemData.Count == 3)
            {
                if(_inlayedItems[0].Level == _inlayedItems[1].Level && _inlayedItems[1].Level == _inlayedItems[2].Level)
                {
                    _inlayedItems[0].UpValue = "10%";
                    _inlayedItems[1].UpValue = "10%";
                    _inlayedItems[2].UpValue = "10%";
                }
                else if(_inlayedItems[0].Level == _inlayedItems[1].Level && _inlayedItems[1].Level != _inlayedItems[2].Level)
                {
                    _inlayedItems[0].UpValue = "5%";
                    _inlayedItems[1].UpValue = "5%";
                }
                else if (_inlayedItems[0].Level == _inlayedItems[2].Level && _inlayedItems[0].Level != _inlayedItems[1].Level)
                {
                    _inlayedItems[0].UpValue = "5%";
                    _inlayedItems[2].UpValue = "5%";
                }
                else if (_inlayedItems[1].Level == _inlayedItems[2].Level && _inlayedItems[0].Level != _inlayedItems[1].Level)
                {
                    _inlayedItems[2].UpValue = "5%";
                    _inlayedItems[1].UpValue = "5%";
                }
            }
        }
        SetSelectInlayedItem(_inlayedItems[_inlaySelectIndex]);
    }

    public void SetInlayedItemData(EquipInlayedItem item, int gemID)
    {
        item.SetItemState(0);
        ItemsData itemdata = ItemsConfig.SharedInstance.GetItemData(gemID);
        if (itemdata == null) return;
        item.Name = itemdata.name;
        item.Icon = itemdata.icon;
        item.Desc = itemdata.description;

        item.Level = itemdata.level;
        InputManager.instance.AddClickListener(item.TakeOffBtn, (go) =>
        {
            SetSelectInlayedItem(item);
            if (_avatar.equipModule.Equipped(_equipDataOrder))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.TakeOffGemFromEquipmentInEquipmentBag(location, item.Index);
            }
            else
            {
                _avatar.equipModule.TakeOffGemFromEquipmentInKitbag(_equipDataOrder, item.Index);
            }          
        });
    }

    private void SetSelectInlayedItem(EquipInlayedItem item)
    {
        _inlaySelectIndex = item.Index - 1;
        _selectInlayedItem = item;
        _inlaySelect.transform.parent = item.transform;
        _inlaySelect.transform.localPosition = Vector3.zero;
        _inlaySelect.SetActive(true);
    }

    private void RefreshEquipInfo()
    {
        _equipName.text = _equipData.Name;
        _EquipIcon.spriteName = _equipData.Icon;
    }

    private List<EquipInlayMenuItem> _inlayMenuItems = new List<EquipInlayMenuItem>(); 
    private void RefreshGemMenu()
    {
        List<EquipGemMenuData> menuData = _avatar.equipModule.GemMenuData;
        int dataNum = menuData.Count;
        int itemNum = _inlayMenuItems.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    EquipInlayMenuItem item = _inlayMenuItems[i];

                    item.gameObject.SetActive(true);
                    SetMenuItemData(item, menuData[i]);
                }
                else
                {
                    _templateItem.gameObject.SetActive(true);
                    EquipInlayMenuItem item = Instantiate(_templateItem) as EquipInlayMenuItem;
                    _templateItem.gameObject.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiTable.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    item.Init(ParentUI);
                    _inlayMenuItems.Add(item);

                    SetMenuItemData(item, menuData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                EquipInlayMenuItem item = _inlayMenuItems[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetMenuItemData(item, menuData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        _uiTable.Reposition();
    }

    private void SwitchState(EquipInlayMenuItem menuItem)
    {
        _selectMenu.transform.parent = menuItem.transform;
        _selectMenu.transform.localPosition = Vector3.zero;
        _selectMenu.SetActive(true);

        menuItem.SwitchState();
        _uiTable.Reposition();
    }

    private void SetMenuItemData(EquipInlayMenuItem menuItem, EquipGemMenuData data)
    {
        menuItem.Name = data.name;
        InputManager.instance.AddClickListener(menuItem.gameObject, (go) =>
        {
            for(int i = 0; i < _inlayMenuItems.Count; i++)
            {
                if(_inlayMenuItems[i] != menuItem)
                {
                    _inlayMenuItems[i].SetState(false);
                   
                }                
            }
            _selectItem.SetActive(false);
            SwitchState(menuItem);           
        });
        List<ItemGem> gemData = new List<ItemGem>();
        if (EquipRule.Inlay(_equipData))
        {
            List<ItemGem> typeData = _avatar.kitbagModule.GemTypeItems(data.gemType);
            foreach (ItemGem gem in typeData)
            {
                if (gem.Level <= _gemLimitLevel)
                {
                    gemData.Add(gem);
                }
            }
        }
       
        UIGrid uiGrid = menuItem.Grid;
        List<EquipInlayGemItem> items = menuItem.Items;
        int dataNum = gemData.Count;
        int itemNum = items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                if (i < itemNum)
                {
                    EquipInlayGemItem item = items[i];
                    item.gameObject.SetActive(true);
                    SetGemItemData(item, gemData[i]);
                }
                else
                {
                    EquipInlayGemItem template = menuItem.Template.GetComponent<EquipInlayGemItem>();
                    menuItem.Template.SetActive(true);
                    EquipInlayGemItem item = Instantiate(template) as EquipInlayGemItem;
                    item.Init(ParentUI);
                    menuItem.Template.SetActive(false);
                    item.transform.parent = uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    items.Add(item);
                    SetGemItemData(item, gemData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                EquipInlayGemItem item = items[i];
                if (i < dataNum)
                {
                    item.gameObject.SetActive(true);
                    SetGemItemData(item, gemData[i]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        uiGrid.gameObject.SetActive(menuItem.OpenState);
        if (menuItem.OpenState) uiGrid.Reposition();
    }

    private void SetGemItemData(EquipInlayGemItem item, ItemGem data)
    {
        item.Name = data.Name;
        item.Desc = data.Description;
        item.Count = Convert.ToString(data.Amount);
        ParentUI.SetDynamicSpriteName(item.Icon, data.Icon);

        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            _selectItem.transform.parent = item.transform;
            _selectItem.transform.localPosition = Vector3.zero;
            _selectItem.SetActive(true);

            if(_selectInlayedItem == null)
            {
                return;
            }

            if(data.Level > _gemLimitLevel)
            {
                TipManager.instance.ShowTextTip(Define.GEM_INLAY_BEYONDLEVEL);
            }

            if (_avatar.equipModule.Equipped(_equipDataOrder))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.InlayGemToEquipmentInEquipmentBag(location, data.Order, _selectInlayedItem.Index);
            }
            else
            {
                _avatar.equipModule.InlayGemToEquipmentInKitbag(_equipDataOrder, data.Order, _selectInlayedItem.Index);
            }           
        });      
    }
}
