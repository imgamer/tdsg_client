﻿using UnityEngine;
using System.Collections;
using System;
using Item;
using System.Collections.Generic;

public class EquipManufactureAscendStarPage : WinPage
{
    private KBEngine.Avatar _avatar;
    private bool _canTrain = false; //是否可以培养
    private int _tipID;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitPage();
    }

     private ItemEquipment _equipData;
     protected override void OnOpen(params object[] args)
     {
         if (args == null || args.Length != 1)
         {
             _equip.SetActive(false);
             _tipID =  Define.EQUIPMENT_NOT_SELECT;
             _canTrain = false;
             return;
         }
         if (args[0] == null)
         {
             _equip.SetActive(false);
             _tipID = Define.EQUIPMENT_NOT_SELECT;
             _canTrain = false;
             return;
         }

         _equipData = (ItemEquipment)args[0];
         RefreshConsumeItem();
         RefreshEquipAttribute();
     }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private const int Count = 5;
    private EquipManufactureStarItem[] _starItem = new EquipManufactureStarItem[Count];

    private GameObject _consumeItem;
    private UISprite _consumeIcon;
    private UILabel _consumeName;

    private GameObject _equip;
    private GameObject _equipItem;
    private UISprite _equipIcon;
    private UIProgressBar _proBar;
    private UILabel _exp;

    private UIGrid _currentUIGrid;
    private GameObject _currentTemplate;
    private List<EquipManufactureStarValueItem> _currentAttributes = new List<EquipManufactureStarValueItem>();

    private UIGrid _upUIGrid;
    private GameObject _upTemplate;
    private List<EquipManufactureStarValueItem> _upAttributes = new List<EquipManufactureStarValueItem>();

    private void InitPage()
    {
        _consumeItem = transform.Find("ConsumeItem").gameObject;
        _consumeIcon = transform.Find("ConsumeItem/Icon").GetComponent<UISprite>();
        _consumeName = transform.Find("ConsumeItem/Name").GetComponent<UILabel>();
        _consumeItem.SetActive(false);

        Transform equip = transform.Find("Equip");
        _equip = equip.gameObject;
        _equipItem = equip.Find("Item").gameObject;
        _equipIcon = equip.Find("Item/Icon").GetComponent<UISprite>();
        _proBar = equip.Find("ProBar").GetComponent<UIProgressBar>();
        _exp = equip.Find("Exp").GetComponent<UILabel>();
        _equip.SetActive(false);

        Transform starGrid = equip.Find("Star");
        for (int i = 0; i < Count; i++)
        {
            EquipManufactureStarItem item = starGrid.Find(i.ToString()).GetComponent<EquipManufactureStarItem>();
            item.Init(ParentUI);
            _starItem[i] = item;
            _starItem[i].Star.SetActive(false);
        }

        Transform attribute = equip.Find("Attribute");
        _currentUIGrid = attribute.Find("Current").GetComponent<UIGrid>();
        _currentTemplate = attribute.Find("Current/Item").gameObject;
        _currentTemplate.SetActive(false);

        _upUIGrid = attribute.Find("Up").GetComponent<UIGrid>();
        _upTemplate = attribute.Find("Up/Item").gameObject;
        _upTemplate.SetActive(false);

        GameObject oneBtn = transform.Find("OneBtn").gameObject;
        InputManager.instance.AddClickListener(oneBtn, (go) =>
        {
            if (!_canTrain)
            {
                if (_tipID == Define.CONSUME_MATERIAL_NOT_ENOUGH)
                {
                    TipManager.instance.ShowTextTip(_tipID, _consumeName.text);
                }
                else
                {
                    TipManager.instance.ShowTextTip(_tipID);
                }
                return;
            }

            if (_avatar.equipModule.Equipped(_equipData.Order))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.AscendStarOfEquipmentInEquipmentBag(location, 1);
            }
            else
            {
                _avatar.equipModule.AscendStarOfEquipmentInKitbag(_equipData.Order, 1);
            }       
        });

        GameObject tenBtn = transform.Find("TenBtn").gameObject;
        InputManager.instance.AddClickListener(tenBtn, (go) =>
        {
            if (!_canTrain)
            {
                TipManager.instance.ShowTextTip(_tipID);
                return;
            }
            if(_consumeItemCount < 10)
            {
                TipManager.instance.ShowTextTip(Define.CONSUME_MATERIAL_NOT_ENOUGH, _consumeName.text);
                return;
            }

            if(_avatar.equipModule.Equipped(_equipData.Order))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.AscendStarOfEquipmentInEquipmentBag(location, 0);
            }
            else
            {
                _avatar.equipModule.AscendStarOfEquipmentInKitbag(_equipData.Order, 0);
            }           
        });

    }

    private const int ConsumeItemID = 30400001; //培养需要的道具ID
    private int _consumeItemCount = 0; //当前拥有材料的数量
    private void RefreshConsumeItem()
    {
        ItemsData consumeData = ItemsConfig.SharedInstance.GetItemData(ConsumeItemID);
        if (consumeData == null) return;
        _consumeName.text = Convert.ToString(consumeData.name);
        ParentUI.SetDynamicSpriteName(_consumeIcon, consumeData.icon);
        _consumeItem.SetActive(true);

        List<ItemBase> bagItems = _avatar.kitbagModule.GetItemsByID(ConsumeItemID);
        if (bagItems.Count == 0 || bagItems[0].Amount == 0)
        {
            _canTrain = false;
            _tipID = Define.CONSUME_MATERIAL_NOT_ENOUGH;
            return;
        }

        _consumeItemCount = bagItems[0].Amount;
        _canTrain = true;
    }

    private const int STAR_MAX_LEVEL = 5;
    private void RefreshEquipAttribute()
    {
        _equip.SetActive(true);
        ItemDetailManager.instance.AddListenerItemDetail(_equipItem, AnchorType.Center, SourcePage.Other, _equipData);
        _equipIcon.spriteName = _equipData.Icon;
        List<PropertyBase> properties = _equipData.BaseProperties;

        int star = _equipData.StarLevel;
        int exp = _equipData.StarExp;

        for (int i = 0; i < Count; i++)
        {
            _starItem[i].Star.SetActive(i <= (star - 1));
        }

        EquipmentAscendStarData starData = EquipmentAscendStarInfoConfig.SharedInstance.GetAscendStarData(star);
        RereshEquipCurrentAttribute(properties, starData);
  
        if (star == STAR_MAX_LEVEL)
        {
            _tipID = Define.EQUIPMENT_STAR_LEVEL_MAX;
            _canTrain = false;
            _exp.gameObject.SetActive(false);
            _proBar.gameObject.SetActive(false);
            for (int i = 0; i < _upAttributes.Count; i++)
            {
                _upAttributes[i].gameObject.SetActive(false);
            }
            return;
        }
        EquipmentAscendStarData nextStarData = EquipmentAscendStarInfoConfig.SharedInstance.GetAscendStarData(star + 1);
        if (nextStarData == null) return;
        _exp.gameObject.SetActive(true);
        _proBar.gameObject.SetActive(true);
        _exp.text = string.Format("{0}/{1}", exp, nextStarData.exp);
        var percent = (float)exp / (float)nextStarData.exp;
        _proBar.value = Mathf.Max(0, percent);
        RereshEquipUpAttribute(properties, nextStarData);        
    }

    private void RereshEquipCurrentAttribute(List<PropertyBase> properties, EquipmentAscendStarData starData)
    {
        int count = _currentAttributes.Count;
        for (int i = 0; i < count; i++)
        {
            _currentAttributes[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                EquipManufactureStarValueItem item = _currentAttributes[i];
                item.gameObject.SetActive(true);
                item.Value = string.Format("{0} {1}", property.PropertyName, property.Value);
                if (starData == null)
                {
                    item.Up = 0f;
                }
                else
                {
                    item.Up = starData.markup;
                }               
            }
            else
            {
                GameObject temp = Instantiate(_currentTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _currentTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                EquipManufactureStarValueItem item = temp.GetComponent<EquipManufactureStarValueItem>();
                item.Init(ParentUI);
                _currentAttributes.Add(item);
                item.Value = string.Format("{0} {1}", property.PropertyName, property.Value);
                if (starData == null)
                {
                    item.Up = 0f;
                }
                else
                {
                    item.Up = starData.markup;
                }      
            }
        }
        _currentUIGrid.Reposition();
    }

    private void RereshEquipUpAttribute(List<PropertyBase> properties, EquipmentAscendStarData starData)
    {
        int count = _upAttributes.Count;
        for (int i = 0; i < count; i++)
        {
            _upAttributes[i].gameObject.SetActive(false);
        }
        if(starData == null)
        {
            return;
        }
        for (int i = 0; i < properties.Count; i++)
        {
            PropertyBase property = properties[i];
            if (i < count)
            {
                EquipManufactureStarValueItem item = _upAttributes[i];
                item.gameObject.SetActive(true);
                item.Value = string.Format("{0} {1}", property.PropertyName, property.Value);
                item.Up = starData.markup;
            }
            else
            {
                GameObject temp = Instantiate(_upTemplate) as GameObject;
                temp.SetActive(true);
                temp.name = i.ToString();
                Transform trans = temp.transform;
                trans.parent = _upTemplate.transform.parent;
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                EquipManufactureStarValueItem item = temp.GetComponent<EquipManufactureStarValueItem>();
                item.Init(ParentUI);
                _upAttributes.Add(item);
                item.Value = string.Format("{0} {1}", property.PropertyName, property.Value);
                item.Up = starData.markup;
            }
        }
        _upUIGrid.Reposition();
    }
}
