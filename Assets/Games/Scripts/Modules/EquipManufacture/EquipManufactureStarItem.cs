﻿using UnityEngine;
using System.Collections;

public class EquipManufactureStarItem : WinItem
{

    private GameObject _star;
    public GameObject Star
    {
        get { return _star; }
    }
  
    protected override void OnInit()
    {
        _star = transform.Find("Star").gameObject;
    }
}
