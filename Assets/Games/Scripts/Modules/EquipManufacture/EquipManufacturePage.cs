﻿using UnityEngine;
using System.Collections;
using Item;

public class EquipManufacturePage : WinPage
{
    protected override void OnInit()
    {
        InitTab();
        InitPage();
    }

    private ItemEquipment _equipData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        if (args[0] == null) return;

        _equipData = (ItemEquipment)args[0];
        uiTab.SwitchTo(uiTab.CurTabIndex);
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void InitPage()
    {
        GameObject repairBtn = transform.Find("RepairBtn").gameObject;
        InputManager.instance.AddClickListener(repairBtn, (go) =>
        {
            UIManager.instance.OpenWindow(null, WinID.UIEquipRepair);
        });
    }

    private WinPage curContent;
    private UI.Tab uiTab;
    private void InitTab()
    {
        uiTab = transform.GetComponent<UI.Tab>();
        uiTab.Init();
        uiTab.OnSelect = (page, index) =>
        {
            curContent = page.GetComponent<WinPage>();
            curContent.Open(ParentUI, _equipData);
        };
    }
}
