﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;

public class EquipWashsAttributePage : WinPage
{
    private KBEngine.Avatar _avatar;
    private bool _canWashs = false;

    private int _tipID;
    protected override void OnInit()
    {
        _avatar = GameMain.Player;
        InitPage();
    }

    private ItemEquipment _equipData;
    private ITEM_ORDER _equipDataOrder = 0;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1)
        {
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }

        if (args[0] == null)
        {
            _tipID = Define.EQUIPMENT_NOT_SELECT;
            SetGameObjectActiveFalse();
            return;
        }
        _equipData = (ItemEquipment)args[0];
        if ((Int16)_equipDataOrder != (Int16)_equipData.Order)
        {
            LockIndex.Clear();
            _equipDataOrder = _equipData.Order;
        }
        
        RefreshConsumeItem();
        RefreshAttributeItem();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void SetGameObjectActiveFalse()
    {
        _grid.gameObject.SetActive(false);
        _consumeItem.SetActive(false);
    }

    private GameObject _consumeItem;
    private UISprite _consumeIcon;
    private UILabel _consumeName;
    private UILabel _consumeCount;

    private UIGrid _grid;
    private const int Count = 4;
    private EquipWashsAttributeItem[] _attributeItem = new EquipWashsAttributeItem[Count];
    private void InitPage()
    {
        _consumeItem = transform.Find("ConsumeItem").gameObject;
        _consumeIcon = transform.Find("ConsumeItem/Icon").GetComponent<UISprite>();
        _consumeName = transform.Find("ConsumeItem/Name").GetComponent<UILabel>();
        _consumeCount = transform.Find("ConsumeItem/Count").GetComponent<UILabel>();
        _consumeItem.SetActive(false);

        Transform attributeGrid = transform.Find("Attribute");
        _grid = attributeGrid.GetComponent<UIGrid>();
        for (int i = 0; i < Count; i++)
        {
            EquipWashsAttributeItem item = attributeGrid.Find(i.ToString()).GetComponent<EquipWashsAttributeItem>();
            item.Init(ParentUI);
            item.Index = Convert.ToByte(i);
            _attributeItem[i] = item;
            InputManager.instance.AddClickListener(item.LockBtn, (go) =>
            {
                if(item.LockState)
                {
                    item.SetLockBtnSprite = false;
                    if (LockIndex.Contains(item.Index))
                    {
                        LockIndex.Remove(item.Index);
                    }                 
                }
                else
                {
                    item.SetLockBtnSprite = true;
                    if (!LockIndex.Contains(item.Index))
                    {
                        LockIndex.Add(item.Index);
                    }
                }
            });
        }

        GameObject replaceBtn = transform.Find("ReplaceBtn").gameObject;
        InputManager.instance.AddClickListener(replaceBtn, (go) =>
        {
            if(_equipData.NewExtraProperties.Count == 0)
            {
                return;
            }
             if(_avatar.equipModule.Equipped(_equipDataOrder))
             {
                 Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.ReplaceEquipmentExtraPropertiesInEquipmentBag(location);
             }
             else
             {
                 _avatar.equipModule.ReplaceEquipmentExtraPropertiesInKitbag(_equipDataOrder);
             }
        });

        GameObject washBtn = transform.Find("WashBtn").gameObject;
        InputManager.instance.AddClickListener(washBtn, (go) =>
        {
            if(!_canWashs)
            {
                if(_tipID == Define.CONSUME_MATERIAL_NOT_ENOUGH)
                {
                    TipManager.instance.ShowTextTip(_tipID, _consumeName.text);
                }
                else
                {
                    TipManager.instance.ShowTextTip(_tipID);
                }
                return;
            }

            if(_avatar.equipModule.Equipped(_equipDataOrder))
            {
                Byte location = _avatar.equipModule.GetEquipLocation(_equipData);
                if (location == 255) return;
                _avatar.equipModule.WashsEquipmentInEquipmentBag(location, LockIndex);               
            }
            else
            {
                _avatar.equipModule.WashsEquipmentInKitbag(_equipDataOrder, LockIndex);
            }
        });
    }

    private void RefreshConsumeItem()
    {
        EquipmentAttributeConsumeData consumeData = EquipmentWashsAttributeConsumeConfig.SharedInstance.GetAttributeConsumeData(_equipData.Level);
        if (consumeData == null) return;
        _consumeCount.text = "x" + Convert.ToString(consumeData.amount);
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(consumeData.itemID);
        if (itemData == null) return;
        _consumeName.text = Convert.ToString(itemData.name);
        ParentUI.SetDynamicSpriteName(_consumeIcon, itemData.icon);
        _consumeItem.SetActive(true);

        List<ItemBase> bagItems = _avatar.kitbagModule.GetItemsByID(consumeData.itemID);
        if (bagItems.Count == 0 || bagItems[0].Amount < consumeData.amount)
        {
            _canWashs = false;
            _tipID = Define.CONSUME_MATERIAL_NOT_ENOUGH;
            return;
        }

        _canWashs = true;
    }

    private const int LIMIT_LEVEL = 30;
    private const int LIMIT_QIALITY = 2;

    private List<object> LockIndex = new List<object>();
    private void RefreshAttributeItem()
    {
        _grid.gameObject.SetActive(true);
        if (_equipData.Quality < LIMIT_QIALITY)
        {
            _canWashs = false;
            _tipID = Define.EQUIPMENT_WASHS_NOT;
            for (int i = 0; i < Count; i++)
            {
                if(i == 0)
                {
                    _attributeItem[i].SetItemState(1);
                    _attributeItem[i].EmpteDesc = "绿色品质装备解锁";
                }
                else
                {
                    _attributeItem[i].gameObject.SetActive(false);
                }
            }
            return;
        }
        if (_equipData.Level < LIMIT_LEVEL)
        {
            _canWashs = false;
            _tipID = Define.EQUIPMENT_WASHS_NOT;
            for (int i = 0; i < Count; i++)
            {
                if (i == 0)
                {
                    _attributeItem[0].SetItemState(1);
                    _attributeItem[0].EmpteDesc = "等级大于30才能洗练";
                }
                else
                {
                    _attributeItem[i].gameObject.SetActive(false);
                }
            }
            return;
        }

        for (int i = 0; i < Count; i++)
        {
            _attributeItem[i].gameObject.SetActive(true);
            _grid.Reposition();
        }

        switch(_equipData.Quality)
        {
            case 2:
                _attributeItem[1].SetItemState(1);
                _attributeItem[1].EmpteDesc = "蓝色装备解锁";
                _attributeItem[2].SetItemState(1);
                _attributeItem[2].EmpteDesc = "紫色装备解锁";
                _attributeItem[3].SetItemState(1);
                _attributeItem[3].EmpteDesc = "橙色装备解锁";
                break;
            case 3:
                _attributeItem[2].SetItemState(1);
                _attributeItem[2].EmpteDesc = "紫色装备解锁";
                _attributeItem[3].SetItemState(1);
                _attributeItem[3].EmpteDesc = "橙色装备解锁";
                break;
            case 4:
                _attributeItem[3].SetItemState(1);
                _attributeItem[3].EmpteDesc = "橙色装备解锁";
                break;
        }

        List<PropertyBase> extraProperties = _equipData.ExtraProperties;
        List<PropertyBase> newExtraPorperties = _equipData.NewExtraProperties;

        for(int i = 0; i< extraProperties.Count; i++)
        {
            _attributeItem[i].SetItemState(2);
            _attributeItem[i].SetLockBtnSprite = false;
            _attributeItem[i].SetCurrentAttribute(extraProperties[i], _equipData.Level);
        }

        if (newExtraPorperties.Count == 0)
        {
            for (int i = 0; i < extraProperties.Count; i++)
            {
                _attributeItem[i].SetItemState(2);
            }
        }
        else
        {
            for (int i = 0; i < newExtraPorperties.Count; i++)
            {
                _attributeItem[i].SetItemState(3);
                _attributeItem[i].SetNewAttribute(newExtraPorperties[i], _equipData.Level);
            }
        }
       
        //已锁定表现
        for(int i = 0; i < LockIndex.Count; i++)
        {
            int index = Convert.ToInt32(LockIndex[i]);
            _attributeItem[index].SetItemState(2);
            _attributeItem[index].SetLockBtnSprite = true;
        }
    }
}
