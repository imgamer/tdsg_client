﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Item;
using System;

public class EquipCompoundPage : WinPage
{
    private List<EquipCompoundItem> _equipCompoundItem = new List<EquipCompoundItem>();

    private bool _canCompound = false; //是否可以合成
    private ITEM_ORDER _order; //准备合成的宝石
    private int _compundCoin; //合成需要消耗的银币
    private int _tipID; //提示ID
    protected override void OnInit()
    {
        InitPage();

        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, RefreshPage);
        EventManager.AddListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, RefreshPage); 
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {
        InitPrivateData();

        RefreshItem();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, RefreshPage);
        EventManager.RemoveListener<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, RefreshPage); 
    }

    private UILabel _compoundCount;
    private UILabel _successRate;

    private GameObject _itemGem;
    private UISprite _itemIcon;
    private UILabel _itemName;
    private UILabel _itemCount;

    private GameObject _toItemGem;
    private UISprite _toItemIcon;
    private UILabel _toItemName;
    private UILabel _toItemCount;

    private UI.Grid _grid;
    private GameObject _tempItem;
    private GameObject _selectBg;
    private void InitPage()
    {
        _compoundCount = transform.Find("CompoundCount").GetComponent<UILabel>();
        _successRate = transform.Find("SuccessRate/Rate").GetComponent<UILabel>();

        GameObject oneBtn = transform.Find("OneBtn").gameObject;
        InputManager.instance.AddClickListener(oneBtn, (go) =>
        {
            if (!_canCompound)
            {
                TipManager.instance.ShowTextTip(_tipID);
                return;
            }
            GameMain.Player.equipModule.GemCombine(_order, 1);
        });

        GameObject allBtn = transform.Find("AllBtn").gameObject;
        InputManager.instance.AddClickListener(allBtn, (go) =>
        {
            if (!_canCompound)
            {
                TipManager.instance.ShowTextTip(_tipID);
                return;
            }
            GameMain.Player.equipModule.GemCombine(_order, 0);
        });

        _itemGem = transform.Find("Item/Gem").gameObject;
        _itemIcon = transform.Find("Item/Gem/Icon").GetComponent<UISprite>();
        _itemName = transform.Find("Item/Gem/Name").GetComponent<UILabel>();
        _itemCount = transform.Find("Item/Gem/Count").GetComponent<UILabel>();
        _itemGem.SetActive(false);

        _toItemGem = transform.Find("ToItem/Gem").gameObject;
        _toItemIcon = transform.Find("ToItem/Gem/Icon").GetComponent<UISprite>();
        _toItemName = transform.Find("ToItem/Gem/Name").GetComponent<UILabel>();
        _toItemCount = transform.Find("ToItem/Gem/Count").GetComponent<UILabel>();
        _toItemGem.SetActive(false);

        _grid = transform.Find("Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = transform.Find("Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        _selectBg = transform.Find("SelectBg").gameObject;
        _selectBg.SetActive(false);
    }

    private void RefreshItem()
    {
        List<ItemBase> data = GameMain.Player.kitbagModule.ChildTypeItems(ItemTypeDefine.ITEM_GEM);

        int dataCount = data.Count;
        int itemCount = _equipCompoundItem.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    SetItemData(_equipCompoundItem[i], data[i]);
                    _equipCompoundItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _tempItem.SetActive(true);
                    GameObject item = Instantiate(_tempItem) as GameObject;
                    EquipCompoundItem compoundItem = item.GetComponent<EquipCompoundItem>();
                    compoundItem.Init(ParentUI);
                    _tempItem.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _equipCompoundItem.Add(compoundItem);
                    SetItemData(compoundItem, data[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_equipCompoundItem[i], data[i]);
                    _equipCompoundItem[i].gameObject.SetActive(true);
                }
                else
                {
                    _equipCompoundItem[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
    }

    private void SetItemData(EquipCompoundItem item, ItemBase baseData)
    {
        ItemGem data = baseData as ItemGem;
        item.Order = data.Order;
        item.Name = data.Name;
        ParentUI.SetDynamicSpriteName(item.Icon, data.Icon);
        item.Icon.gameObject.SetActive(true);
        item.Desc = data.Description;
        item.Count = Convert.ToString(data.Amount);

        InputManager.instance.AddClickListener(item.gameObject, (go) =>
        {
            SelectItem(item);

            _order = data.Order;

            UpdateItem(data);
            UpdateToItem(data);       
        });
    }

    private void SelectItem(EquipCompoundItem item)
    {
        _selectBg.transform.parent = item.transform;
        _selectBg.transform.localPosition = Vector3.zero;
        _selectBg.SetActive(true);
    }

    private void UpdateItem(ItemGem data)
    {
        _itemGem.SetActive(true);
        ParentUI.SetDynamicSpriteName(_itemIcon, data.Icon);
        _itemName.text = data.Name;
        _itemCount.text = Convert.ToString(data.Amount);
    }

    private void UpdateToItem(ItemGem data)
    {
        if (data == null || data.Amount < 4)
        {
            //没有选择宝石或者宝石数量小于4个不能合成
            _canCompound = false;
            _toItemGem.SetActive(false);
            _tipID = Define.GEM_COUNT_NOT_ENOUGH;
            return;
        }
        GemCombinData gemData = GemCombinConfig.SharedInstance.GetGemCombinData((int)data.ID);
        if (gemData == null)
        {
            return;
        }
        ItemsData nextGemData = ItemsConfig.SharedInstance.GetItemData(gemData.nextgemid);
        if (nextGemData == null)
        {
            _toItemGem.SetActive(false);
            //没有下一个等级宝石数据，或已经达到最高等级，不能合成
            _tipID = Define.GEM_LEVEL_MAX;
            _canCompound = false;
            return;
        }

        _toItemGem.SetActive(true);
        ParentUI.SetDynamicSpriteName(_toItemIcon, nextGemData.icon);
        _toItemName.text = gemData.nextgemname;
        _toItemCount.text = Convert.ToString((int)(data.Amount / 4));
        _compundCoin = gemData.nextgemlevel * Define.GEM_COMBIN_NUM;
        if (GameMain.Player.Silver < _compundCoin)
        {
            //消耗的银币达不到要求  无法合成
            _canCompound = false;
            _tipID = Define.SILVER_NOT_ENOUGH;
            return;
        }
        _canCompound = true;
    }

    private void InitPrivateData()
    {
        _itemGem.SetActive(false);
        _toItemGem.SetActive(false);
        _selectBg.SetActive(false);
        _order = 0;
        _tipID = Define.GEM_COUNT_NOT_ENOUGH;
        _compundCoin = 0;
        _canCompound = false;
    }

    private void RefreshPage(object o)
    {
        if (!Active) return;

        RefreshItem();

        if (_order == 0) return;

        foreach (EquipCompoundItem item in _equipCompoundItem)
        {
            if ((Int16)item.Order == (Int16)_order && item.gameObject.activeSelf)
            {
                SelectItem(item);
            }
        }
        ItemGem data = GameMain.Player.kitbagModule.GetItemByOrder(_order) as ItemGem;
        UpdateItem(data);
        UpdateToItem(data);
    }
}
