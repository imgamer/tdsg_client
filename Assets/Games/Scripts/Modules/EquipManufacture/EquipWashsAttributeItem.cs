﻿using UnityEngine;
using System.Collections;
using Item;
using System.Collections.Generic;
using System;

public class EquipWashsAttributeItem : WinItem
{
    private object _index;
    public object Index
    {
        set
        {
            _index = value;
        }
        get
        {
            return _index;
        }
    }

    private UILabel _emptyDesc;
    public string EmpteDesc
    {
        set
        { _emptyDesc.text = value.ToString(); }
    }

    private GameObject _have;
    private GameObject _lockBtn;
    private UISprite _lockBtnSprite;

    private bool _lockState = false;
    public bool LockState
    {
        get { return _lockState; }
    }
    /// <summary>
    /// true:锁定 false：不锁定
    /// </summary>
    public bool SetLockBtnSprite
    {
        set
        {
            if(value)
            {
                _lockBtnSprite.spriteName = "[11-15]UI-s2-zb-dasuo";
                _lockState = true;
            }
            else
            {
                _lockBtnSprite.spriteName = "[11-15]UI-s2-zb-xiaosuokai";
                _lockState = false;
            }
        }
    
    }

    public GameObject LockBtn
    {
        get { return _lockBtn; }
    }

    private GameObject _current;
    private UILabel _currentName;
    private UILabel _currentValue;
    private UILabel _currentStar;

    private GameObject _new;
    private UILabel _newName;
    private UILabel _newValue;
    private UILabel _newStar;

    private int CurrentState = 0;
    /// <summary>
    /// 1:显示提示；2：显示属性； 3：显示属性和新属性；
    /// </summary>
    /// <param name="state"></param>
    public void SetItemState(int state)
    {
        if (state == CurrentState)
        {
            return;
        }
        switch(state)
        {
            case 1:
                _current.SetActive(false);
                _new.SetActive(false);
                _emptyDesc.gameObject.SetActive(true);
                CurrentState = 1;
                break;
            case 2:
                _current.SetActive(true);
                _new.SetActive(false);
                _emptyDesc.gameObject.SetActive(false);
                CurrentState = 2;
                break;
            case 3:
                _current.SetActive(true);
                _new.SetActive(true);
                _emptyDesc.gameObject.SetActive(false);
                CurrentState = 3;
                break;
        }
    }

    private Int32 CurrentStar = 0;
    public void SetCurrentAttribute(PropertyBase data, int level)
    {
        _currentName.text = data.PropertyName;
        _currentValue.text = "+" + data.Value.ToString();
        _currentStar.text = string.Format("({0}星)", data.ValueStar);
        CurrentStar = Convert.ToInt32(data.ValueStar);
    }
    
    public void SetNewAttribute(PropertyBase data, int level)
    {
        if (CurrentStar > Convert.ToInt32(data.ValueStar))
        {
            _newValue.color = GameUtils.RedColor;
            _newName.color = GameUtils.RedColor;
            _newStar.color = GameUtils.RedColor;
        }
        else
        {
            _newValue.color = GameUtils.GreenColor;
            _newName.color = GameUtils.GreenColor;
            _newStar.color = GameUtils.GreenColor;
        }
        _newName.text = data.PropertyName;       
        _newValue.text = "+" + data.Value.ToString();
        _newStar.text = string.Format("({0}星)", data.ValueStar);
    }

    protected override void OnInit()
    {
        _emptyDesc = transform.Find("EmptyDesc").GetComponent<UILabel>();
        _current = transform.Find("Current").gameObject;
        _lockBtn = transform.Find("Current/LockBtn").gameObject;
        _lockBtnSprite = transform.Find("Current/LockBtn/BackGround").GetComponent<UISprite>();

        _currentName = transform.Find("Current/Name").GetComponent<UILabel>();
        _currentValue = transform.Find("Current/Value").GetComponent<UILabel>();
        _currentStar = transform.Find("Current/Star").GetComponent<UILabel>();

        _new = transform.Find("New").gameObject;
        _newName = transform.Find("New/Name").GetComponent<UILabel>();
        _newValue = transform.Find("New/Value").GetComponent<UILabel>();
        _newStar = transform.Find("New/Star").GetComponent<UILabel>();
    }
}
