﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// Loading进度条管理
/// </summary>
public class LoadingManager : MonoSingleton<LoadingManager>
{
    private Queue<Action<Action<bool>>> _queue = new Queue<Action<Action<bool>>>();
    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {

    }

    private float _step;
    public void AddLoadQueue(Action<Action<bool>> loadAction)
    {
        if (loadAction == null) return;
        if (_loading) StopLoading();
        _queue.Enqueue(loadAction);
    }

    private const float LastStep = 0.05f;
    private bool _loading = false;
    private Action _onDone;
    public void StartLoading(Action onDone)
    {
        if (_loading) return;
        if (_queue.Count <= 0)
        {
            EndLoading();
            return;
        }
        _step = (1.0f - LastStep) / _queue.Count;
        _onDone = onDone;
        DoNext();
    }

    private void DoNext()
    {
        if (_queue.Count <= 0)
        {
            EndLoading();
            return;
        }
        EventManager.Invoke<float>(EventID.EVNT_SCENE_LOADING, _step);
        Action<Action<bool>> action = _queue.Dequeue();
        if(action == null)
        {
            DoNext();
            return;
        }
        _loading = true;
        action((ok) => 
        {
            if (!_loading) return;
            _loading = false;
            DoNext();
        });
    }

    public void OpenUILoading()
    {
        UIManager.instance.OpenTopWindow(null, WinID.UILoading);
    }

    private void EndLoading()
    {
        _loading = false;
        EventManager.Invoke<float>(EventID.EVNT_SCENE_LOADING, LastStep);
        if (_onDone != null) _onDone();
    }

    private void StopLoading()
    {
        _loading = false;
        _queue.Clear();
        UIManager.instance.CloseWindow(WinID.UILoading);
    }

}
