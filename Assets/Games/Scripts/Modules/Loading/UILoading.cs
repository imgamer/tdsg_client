﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 场景跳转界面
/// </summary>
public class UILoading : UIWin
{
    private float _targetValue = 0;
    private UIProgressBar _progress;
    private UILabel _text;
    protected override void OnInit()
    {
        EventManager.AddListener<float>(EventID.EVNT_SCENE_LOADING, UpdateBar);
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        Reset();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<float>(EventID.EVNT_SCENE_LOADING, UpdateBar);
    }

    private void InitItem()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _progress = center.Find("Progress").GetComponent<UIProgressBar>();
        _text = center.Find("Text").GetComponent<UILabel>();
    }

    private void Reset()
    {
        _targetValue = 0;
        _progress.value = 0;
        CancelLoad();
        CancleClose();
        InvokeRepeating("DoLoad", 0.2f, 0.01f);
    }

    private void DoLoad()
    {
        if(_progress.value >= 1)
        {
            CancelLoad();
            Invoke("CloseMe", 0.5f);
            return;
        }
        if (_progress.value >= _targetValue) return;
        _progress.value += 0.01f;
    }

    private void CancelLoad()
    {
        CancelInvoke("DoLoad");
    }

    private void CloseMe()
    {
        UIManager.instance.CloseWindow(winID);
    }

    private void CancleClose()
    {
        CancelInvoke("CloseMe");
    }

    private void UpdateBar(float value)
    {
        if (!Active) return;
        _targetValue += value;
    }
}
