﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TeamCopyAgree
{
    public string name;
    public UInt64 dbid;
    public Int32 agree; //0:不同意
}
 
public class TeamCopyModule
{
    private KBEngine.Avatar _avatar;

    private List<TeamCopyAgree> _agreeData = new List<TeamCopyAgree>();

    public TeamCopyModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    public List<TeamCopyAgree> GetAgreeData()
    {
        return _agreeData;
    }

    public void ConfirmJoinMultipleFightCopy(List<object> agreeDatas)
    {
        _agreeData.Clear();
        if(agreeDatas.Count > 0)
        {
            UIManager.instance.OpenWindow(null, WinID.UITeamCopyConfirm);
            foreach (object obj in agreeDatas)
            {
                Dictionary<string, object> data = (Dictionary<string, object>)obj;
                TeamCopyAgree copyAgree = new TeamCopyAgree();
                copyAgree.name = (string)data["name"];
                copyAgree.dbid = (UInt64)data["dbid"];
                copyAgree.agree = Convert.ToInt32(data["agree"]);
                _agreeData.Add(copyAgree);
            }
            EventManager.Invoke<object>(EventID.EVNT_TEAMCOPY_AGREE_UPDATE, null);
        }
        else
        {
            UIManager.instance.CloseWindow(WinID.UITeamCopyConfirm);
        }      
    }

    public Int16 Type{get; private set;}
    public Int32 Chivalrous { get; private set; }
    public Int32 Gold { get; private set; }
    public Int32 Silver { get; private set; }
    public Int32 EXP { get; private set; }
    public List<object> Reward = new List<object>();
    public void ConfirmLeaveMultipleSpace(Int16 leaveType, Int32 statsChivalrous, Int32 statsGolds, Int32 statsSilvers, Int32 statsAvatarEXPs, Dictionary<string, object> statsItems)
    {
        Type = leaveType;
        Chivalrous = statsChivalrous;
        Gold = statsGolds;
        Silver = statsSilvers;
        EXP = statsAvatarEXPs;
        Reward = (List<object>)statsItems["statsItems"];

        EventManager.Invoke<object>(EventID.EVNT_TEAMCOPY_REWARD_UPDATE, null);
    }

    /// <summary>
    /// 准备进入副本：准备或者取消
    /// </summary>
    /// <param name="dbid"></param>
    /// <param name="agree"></param>
    public void JoinMultipleFightCopyConfirm(UInt64 dbid, int agree)
    {
        _avatar.teamModule.getCaptainEntity().cellCall("joinMultipleFightCopyConfirm", new object[] { dbid, agree });
    }

    /// <summary>
    /// 请求离开多人副本场景，并且服务器回发结果数据
    /// </summary>
    public void RequestLeaveMultipleSpace()
    {
        _avatar.cellCall("requestLeaveMultipleSpace", new object[] { });
    }

    /// <summary>
    /// 确认离开多人副本
    /// </summary>
    public void ConfirmLeaveMultipleSpace()
    {
        _avatar.cellCall("confirmLeaveMultipleSpace", new object[] {Define.LEAVE_TEAM_LEAVE_MULTSPACE });
    }


}
