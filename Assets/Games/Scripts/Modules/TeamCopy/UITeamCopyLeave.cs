﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UITeamCopyLeave : UIWin
{
	 protected override void OnInit()
    {
        InitPage();
        EventManager.AddListener<object>(EventID.EVNT_TEAMCOPY_REWARD_UPDATE, UpdatePage);
    }

    protected override void OnOpen(params object[] args)
    {
        
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_TEAMCOPY_REWARD_UPDATE, UpdatePage);
    }

    private UILabel _gold;
    private UILabel _exp;
    private UILabel _chivalrous;

    private UI.Grid _grid;
    private GameObject _tempItem;

    private GameObject _state1;
    private GameObject _state2;
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);

        _gold = center.Find("Reward/Gold/Value").GetComponent<UILabel>();
        _exp = center.Find("Reward/Exp/Value").GetComponent<UILabel>();
        _chivalrous = center.Find("Reward/Chivalrous/Value").GetComponent<UILabel>();

        _grid = center.Find("Reward/Scroll/Grid").GetComponent<UI.Grid>();
        _tempItem = center.Find("Reward/Scroll/Grid/Item").gameObject;
        _tempItem.SetActive(false);

        _state1 = center.Find("State1").gameObject;
        GameObject cancelBtn = center.Find("State1/CancelBtn").gameObject;
        InputManager.instance.AddClickListener(cancelBtn, (go) =>
        {
            UIManager.instance.CloseWindow(winID);
        });

        GameObject confirmBtn = center.Find("State1/ConfirmBtn").gameObject;
        InputManager.instance.AddClickListener(confirmBtn, (go) =>
        {
            GameMain.Player.teamCopyModule.ConfirmLeaveMultipleSpace();
        });

        _state2 = center.Find("State2").gameObject;
        GameObject confirmBtn2 = center.Find("State2/ConfirmBtn").gameObject;
        InputManager.instance.AddClickListener(confirmBtn2, (go) =>
        {
            GameMain.Player.teamCopyModule.ConfirmLeaveMultipleSpace();
        });
        _state2.SetActive(false);
    }

    private void RefreshReward()
    {
        _chivalrous.text = GameMain.Player.teamCopyModule.Chivalrous.ToString();
        _gold.text = GameMain.Player.teamCopyModule.Gold.ToString();
        _exp.text = GameMain.Player.teamCopyModule.EXP.ToString();

        if(GameMain.Player.teamCopyModule.Type == Define.MULTIPLE_FIGHT_COPY_LEAVE_TYPE_INITIATIVE)
        {
            _state1.SetActive(true);
            _state2.SetActive(false);
        }
        else
        {
            _state1.SetActive(false);
            _state2.SetActive(true);
        }
    }

    private List<TeamCopyRewardItem> _items = new List<TeamCopyRewardItem>();
    private void RefreshItem()
    {
        List<object> itemsData = GameMain.Player.teamCopyModule.Reward;
        int dataCount = itemsData.Count;
        int itemCount = _items.Count;
        if (dataCount >= itemCount)
        {
            for (int i = 0; i < dataCount; i++)
            {
                if (i < itemCount)
                {
                    _items[i].gameObject.SetActive(true);
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _tempItem.SetActive(true);
                    TeamCopyRewardItem item = Instantiate(_tempItem).GetComponent<TeamCopyRewardItem>();
                    item.Init(this);
                    _tempItem.SetActive(false);
                    item.transform.parent = _grid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    SetItemData(_items[i], itemsData[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < itemCount; i++)
            {
                if (i < dataCount)
                {
                    SetItemData(_items[i], itemsData[i]);
                }
                else
                {
                    _items[i].gameObject.SetActive(false);
                }
            }
        }
        _grid.Reposition();
    }

    private void SetItemData(TeamCopyRewardItem item, object data)
    {
        Dictionary<string, object> dic = (Dictionary<string, object>)data;
        Int32 id = (Int32)dic["itemId"];
        Int32 count = (Int32)dic["itemCount"];
        ItemsData itemData = ItemsConfig.SharedInstance.GetItemData(id);
        if (itemData == null) return;
        item.Icon = itemData.icon;
        item.Count = count;
    }

    private void UpdatePage(object o)
    {
        if (!Active) return;
        RefreshReward();
        RefreshItem();
    }
}
