﻿using UnityEngine;
using System.Collections;

public class TeamCopyRewardItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }

    private UILabel _count;
    public int Count
    {
        set
        {
            if(value == 0)
            {
                _count.gameObject.SetActive(false);
            }
            else
            {
                _count.text = value.ToString();
                _count.gameObject.SetActive(true);
            }
        }
    }

    protected override void OnInit()
    {
        _count = transform.Find("Count").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
    }
}
