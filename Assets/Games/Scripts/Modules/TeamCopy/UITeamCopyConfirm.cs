﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UITeamCopyConfirm : UIWin
{
    protected override void OnInit()
    {
        InitPage();

        EventManager.AddListener<object>(EventID.EVNT_TEAMCOPY_AGREE_UPDATE, UpdateItem);
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {
        TimeUtils.RemoveTimer(TimerId);
        TimerId = TimeUtils.InvalidTimerID;
    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<object>(EventID.EVNT_TEAMCOPY_AGREE_UPDATE, UpdateItem);
    }

    private GameObject _confirm;
    private UIProgressBar _proBar;
    private UILabel _time;
    private GameObject confirmBtn;
    private UILabel _confirmText;

    private const int Num = 3;
    private TeamCopyMemberItem[] _member = new TeamCopyMemberItem[Num];
    private void InitPage()
    {
        Transform center = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Center);
        _confirm = center.Find("Confirm").gameObject;
        _time = center.Find("Confirm/Time").GetComponent<UILabel>();
        _proBar = center.Find("Confirm/ProBar").GetComponent<UIProgressBar>();

        Transform member = center.Find("Member");
        for(int i = 0; i < Num; i++)
        {
            TeamCopyMemberItem item = member.Find(i.ToString()).GetComponent<TeamCopyMemberItem>();
            item.Init(this);
            _member[i] = item;
        }

        GameObject cancelBtn = center.Find("CancelBtn").gameObject;
        InputManager.instance.AddClickListener(cancelBtn, (go) =>
        {
            GameMain.Player.teamCopyModule.JoinMultipleFightCopyConfirm(GameMain.Player.DatabaseID, 0);
            UIManager.instance.CloseWindow(winID);
        });

        confirmBtn = center.Find("ConfirmBtn").gameObject;
        _confirmText = center.Find("ConfirmBtn/Label").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(confirmBtn, (go) =>
        {
            GameMain.Player.teamCopyModule.JoinMultipleFightCopyConfirm(GameMain.Player.DatabaseID, 1);
        });
    }

    private void RefreshItem()
    {
        List<TeamCopyAgree> agreeData = GameMain.Player.teamCopyModule.GetAgreeData();
        if (agreeData.Count != 3) return;
        UInt64 CaptainDbid = GameMain.Player.teamModule.CaptainDbid;
        for(int i= 0; i < Num; i++)
        {
            _member[i].Name = agreeData[i].name;
            _member[i].SetState(agreeData[i].agree);
            if(CaptainDbid == agreeData[i].dbid)
            {
                _member[i].SetCaptain(true);               
            }
            else
            {
                _member[i].SetCaptain(false);
            }
           if(GameMain.Player.DatabaseID == agreeData[i].dbid)
           {
               if(agreeData[i].agree == 1)
               {
                   _confirmText.text = "已准备";
                   UITools.SetButtonState(confirmBtn, false);
               }
           }
        }     
    }

    private ulong TimerId = TimeUtils.InvalidTimerID;
    private void RefreshReadyTime()
    {
        Int32 time = Define.TEAMCOPY_READY_TIME;        
        if (TimerId != TimeUtils.InvalidTimerID)
        {
            TimeUtils.RestartTimer(TimerId);
        }
        else
        {
            _proBar.value = 1.0f;
            TimerId = TimeUtils.AddCountdownScaleTimerCallInt(time, (ints) =>
            {
                _time.text = string.Format("{0}S", ints);
                _proBar.value -= 1/40f;
            }, () =>
            {
                TimerId = TimeUtils.InvalidTimerID;
                UIManager.instance.CloseWindow(winID);
            });
            TimeUtils.StartTimer(TimerId);
        }
    }

    private void UpdateItem(object o)
    {
        if (!Active) return;
        _confirmText.text = "准    备";
        UITools.SetButtonState(confirmBtn, true);
        RefreshItem();
        RefreshReadyTime();
    }
}
