﻿using UnityEngine;
using System.Collections;

public class UITeamCopy : UIWin
{
    protected override void OnInit()
    {
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        GameObject leaveBtn = bottomRight.Find("BtnLeave").gameObject;
        InputManager.instance.AddClickListener(leaveBtn, (go) =>
        {
            GameMain.Player.teamCopyModule.RequestLeaveMultipleSpace();
            UIManager.instance.OpenWindow(null, WinID.UITeamCopyLeave);
        });
    }

    protected override void OnOpen(params object[] args)
    {
        
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
