﻿using UnityEngine;
using System.Collections;

public class TeamCopyMemberItem : WinItem
{
    private UILabel _name;
    public string Name
    {
        set
        {
            _name.text = value;
        }
    }

    private UISprite _icon;
    public string Icon
    {
        set
        {
            _icon.spriteName = value;
        }
    }

    private GameObject _sign;
    public void  SetCaptain(bool value)
    {
        if (value)
        {
            _sign.gameObject.SetActive(true);
        }
        else
        {
            _sign.gameObject.SetActive(false);
        }
    }

    private GameObject _state1; //未准备
    private GameObject _state2;//已准备
    public void SetState(int value)
    {
        if (value == 0)
        {
            _state1.gameObject.SetActive(true);
            _state2.gameObject.SetActive(false);
        }
        else
        {
            _state1.gameObject.SetActive(false);
            _state2.gameObject.SetActive(true);
        }
    }

    protected override void OnInit()
    {
        _name = transform.Find("Name").GetComponent<UILabel>();
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _sign = transform.Find("Sign").gameObject;
        _state1 = transform.Find("State1").gameObject;
        _state2 = transform.Find("State2").gameObject;
    }
}
