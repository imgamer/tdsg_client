﻿using UnityEngine;
using System.Collections;

/// <summary>
/// PVE战斗管理
/// </summary>
public class PVEManager : MonoSingleton<PVEManager>
{

    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {

    }

    private int _selectSkillId = 0;
    public void ClearCurSelectSkill()
    {
        _selectSkillId = 0;
    }
    public int GetCurSelectSkill()
    {
        return _selectSkillId;
    }
    public void SetCurSelectSkill(KBEngine.GameObject entity, int skillId)
    {
        if (entity == null || skillId == 0) return;
        _selectSkillId = skillId;
        sbyte state = entity.FightState;
        if (state == Define.ENTITY_STATE_FIGHT_DEAD || state == Define.ENTITY_STATE_FIGHT_WOUNDED) return;
        entity.cellCall("selectSkill", new object[] { skillId });
    }
    public void SetCurSelectTarget(KBEngine.GameObject entity, SceneEntity targetEntity)
    {
        if (entity == null) return;
        if (!TargetHandle.SharedInstance.IsLegalTarget(entity.SceneEntityObj, GetCurSelectSkill(), targetEntity)) return;
        sbyte state = entity.FightState;
        if (state == Define.ENTITY_STATE_FIGHT_DEAD || state == Define.ENTITY_STATE_FIGHT_WOUNDED) return;
        entity.cellCall("selectTargetId", new object[] { targetEntity.KBEntity.id });
    }


}
