﻿using UnityEngine;
using System.Collections;
/// <summary>
/// PVE战斗界面Boss显示面板
/// </summary>
public class BossMenu : WinPage
{
    protected override void OnInit()
    {
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {

    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private UISprite _icon;
    private UISprite _foreground;
    private UIProgressBar _bar;
    private void InitItem()
    {
        _icon = transform.Find("Icon/Icon").GetComponent<UISprite>();
        _foreground = transform.Find("Bar/Foreground").GetComponent<UISprite>();
        _bar = transform.Find("Bar").GetComponent<UIProgressBar>();
    }
}
