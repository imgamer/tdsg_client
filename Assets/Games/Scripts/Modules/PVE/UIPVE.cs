﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Skill;
using System;

/// <summary>
/// PVE战斗界面
/// </summary>
public class UIPVE : UIWin
{
	protected override void OnInit()
    {
        EventManager.AddListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChat);
        EventManager.AddListener<bool>(EventID.EVNT_PLAYER_MODE_UPDATE, UpdateMode);
        InitTop();
        InitItem();
    }

    protected override void OnOpen(params object[] args)
    {
        _pveMenu.Open(this, PVEMenu.Type.Skill);
        SetMode();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<int>(EventID.EVNT_CHAT_DATA_UPDATE, UpdateChat);
        EventManager.RemoveListener<bool>(EventID.EVNT_PLAYER_MODE_UPDATE, UpdateMode);
    }
    private GameObject _chatMark;
    private UIWin _UIChannel;
    private void InitTop()
    {
        Transform topLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.TopLeft);
        GameObject btnChat = topLeft.Find("BtnChat").gameObject;
        _chatMark = topLeft.Find("BtnChat/Mark").gameObject;
        _chatMark.SetActive(false);
        InputManager.instance.AddClickListener(btnChat, (go) =>
        {
            _chatMark.SetActive(false);
            _UIChannel = UIManager.instance.OpenWindow(null, WinID.UIChannel);
        });
    }
    private UILabel _modeText;
    private PVEMenu _pveMenu;
    private bool _escape = false;
    private void InitItem()
    {
        Transform bottom = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Bottom);
        _pveMenu = bottom.Find("PVEMenu").GetComponent<PVEMenu>();
        Transform bottomLeft = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomLeft);
        GameObject btnMode = bottomLeft.Find("BtnMode").gameObject;
        _modeText = btnMode.transform.Find("Label").GetComponent<UILabel>();
        InputManager.instance.AddClickListener(btnMode, (go) => 
        {
            SceneManager.instance.CurrentScene.ReqModeSwitch();
        });
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        GameObject btnEscape = bottomRight.Find("BtnEscape").gameObject;
        InputManager.instance.AddClickListener(btnEscape, (go) =>
        {
            if (_escape)
            {
                SceneManager.instance.CurrentScene.ReqReturnCityRoom();
            }
        });
//         GameObject btnSkill = bottomRight.Find("BtnSkill").gameObject;
//         InputManager.instance.AddClickListener(btnSkill, (go) =>
//         {
//             _pveMenu.Open(this, PveMenu.Type.Skill);
//         });
//         GameObject btnBag = bottomRight.Find("BtnBag").gameObject;
//         InputManager.instance.AddClickListener(btnBag, (go) =>
//         {
//             _pveMenu.Open(this, PveMenu.Type.Goods);
//         });
    }

    private void SetMode()
    {
        if (GameMain.Player.AutoMode)
        {
            _modeText.text = "手动";
        }
        else
        {
            _modeText.text = "自动"; 
        }
    }

    private void UpdateMode(bool autoMode)
    {
        if (!Active) return;
        SetMode();
    }

    private void UpdateChat(int channel)
    {
        if (!Active) return;
        if (_UIChannel != null && _UIChannel.Active) return;
        _chatMark.SetActive(true);
    }

}
