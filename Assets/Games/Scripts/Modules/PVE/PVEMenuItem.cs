﻿using UnityEngine;
using System.Collections;
/// <summary>
/// PVEMenu的Item
/// </summary>
public class PVEMenuItem : WinItem
{
    private UISprite _icon;
    public string Icon
    {
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                _icon.enabled = false;
            }
            else
            {
                _icon.enabled = true;
                ParentUI.SetDynamicSpriteName(_icon, value);
            }
        }
    }
    private Transform _effectRoot;
    public Transform EffectRoot
    {
        get { return _effectRoot; }
    }
    private GameObject _state;
    private BoxCollider _box;
    public bool Active
    {
        set 
        {
            if(value)
            {
                _state.SetActive(false);
                _box.enabled = true;
            }
            else
            {
                _state.SetActive(true);
                _box.enabled = false;
            }
        }
    }

    protected override void OnInit()
    {
        _icon = transform.Find("Icon").GetComponent<UISprite>();
        _effectRoot = transform.Find("EffectRoot");
        _state = transform.Find("State").gameObject;
        _box = transform.GetComponent<BoxCollider>();
    }
}
