using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Skill;
/// <summary>
/// PVE战斗技能/物品选择界面
/// </summary>
public class PVEMenu : WinPage
{
    public enum Type
    {
        Skill,
        Goods,
    }
    protected override void OnInit()
    {
        EventManager.AddListener<Int32>(EventID.EVNT_PLAYER_ANGER_UPDATE, OnUpdateAnger);
        EventManager.AddListener<bool>(EventID.EVNT_PLAYER_MODE_UPDATE, OnUpdateMode);
        EventManager.AddListener<sbyte>(EventID.EVNT_PLAYER_FIGHTSTATE_UPDATE, OnUpdateState);
        EventManager.AddListener<object>(EventID.EVNT_FIGHT_ENTITY_NUM_UPDATE, OnUpdateFightEntityNum);
        EventManager.AddListener<Int32>(EventID.EVNT_ROUND_START, OnUpdateRound);
        InitItem();
    }

    private Type _type = Type.Skill;
    private Int32 _angerMax;
    private List<SkillInitiative> skills = new List<SkillInitiative>();
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length != 1) return;
        _type = (Type)args[0];
        _angerMax = GameMain.Player.MP_Max;
        skills = GameMain.Player.skillModule.GetInitiativeSkills();
        UpdateAnger(GameMain.Player.MP);
        //清空已选择的技能
        ClearSelect();
        UpdateMenu();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {
        EventManager.RemoveListener<Int32>(EventID.EVNT_PLAYER_ANGER_UPDATE, OnUpdateAnger);
        EventManager.RemoveListener<bool>(EventID.EVNT_PLAYER_MODE_UPDATE, OnUpdateMode);
        EventManager.RemoveListener<sbyte>(EventID.EVNT_PLAYER_FIGHTSTATE_UPDATE, OnUpdateState);
        EventManager.RemoveListener<object>(EventID.EVNT_FIGHT_ENTITY_NUM_UPDATE, OnUpdateFightEntityNum);
        EventManager.RemoveListener<Int32>(EventID.EVNT_ROUND_START, OnUpdateRound);
    }

    private const int Num = 5;
    private PVEMenuItem[] _menuItems = new PVEMenuItem[Num];
    private UIProgressBar _bar;
    private UILabel _value;
    private BaseEffect _effect = null;
    private void InitItem()
    {
        Transform grid = transform.Find("UIGrid");
        for (int i = 0; i < Num; i++)
        {
            Transform item = grid.Find(i.ToString());
            PVEMenuItem menuItem = item.GetComponent<PVEMenuItem>();
            menuItem.Init(ParentUI);
            _menuItems[i] = menuItem;
        }
        _bar = transform.Find("AngerBar").GetComponent<UIProgressBar>();
        _value = transform.Find("AngerBar/Value").GetComponent<UILabel>();
        EffectManager.instance.GetEffect("fx_ui_zhandou_jinengtishi", transform, (e) => 
        {
            if(e)
            {
                _effect = e;
                _effect.Hide();
            }
        });
    }

    private void UpdateMenu()
    {
        switch (_type)
        {
            case Type.Skill:
                UpdateSkill();
                break;
            case Type.Goods:
                UpdateGoods();
                break;
            default:
                break;
        }
    }

    private void ClearSelect()
    {
        PVEManager.instance.ClearCurSelectSkill();
        if (_effect == null) return;
        _effect.Hide();
    }
    private void SelectMenu(PVEMenuItem skillItem)
    {
        if (_effect == null) return;
        //---------------------------------------------//
        //后期有特效分层后，不需要外部设置sortingOrder
        Renderer[] rs = _effect.GetComponentsInChildren<Renderer>(true);
        for (int i = 0; i < rs.Length; i++)
        {
            rs[i].sortingOrder = ParentUI.SortingOrder + 1;
        }
        //---------------------------------------------//
        _effect.transform.parent = skillItem.EffectRoot;
        _effect.transform.localPosition = Vector3.zero;
        _effect.transform.localEulerAngles = Vector3.zero;
        _effect.Play(false);

        for (int i = 0; i < _menuItems.Length; i++)
        {
            PVEMenuItem item = _menuItems[i];
            item.Active = skillItem == item;
        }
    }

    #region 更新技能状态
    private void UpdateSkill()
    {
        for (int i = 0; i < _menuItems.Length; i++)
        {
            PVEMenuItem skillItem = _menuItems[i];
            if (i < skills.Count)
            {
                SkillInitiative data = skills[i];
                skillItem.Icon = data.icon;
                bool active = CanUseSkill(data);
                if (active)
                {
                    skillItem.Active = true;
                }
                else
                {
                    //技能不合法，清空已选择的技能
                    if (data.id == PVEManager.instance.GetCurSelectSkill())
                    {
                        ClearSelect();
                    }
                    skillItem.Active = false;
                }
                
                InputManager.instance.AddClickListener(skillItem.gameObject, (go) =>
                {
                    //已经选择，不能替换
                    if (PVEManager.instance.GetCurSelectSkill() != 0) return;

                    SelectMenu(skillItem);
                    if(GameMain.Player.AutoMode)
                    {
                        SceneManager.instance.CurrentScene.ReqModeSwitch();
                    }
                    PVEManager.instance.SetCurSelectSkill(GameMain.Player, data.id);
                });
            }
            else
            {
                skillItem.Icon = string.Empty;
                skillItem.Active = false;
                InputManager.instance.RemoveClickListener(skillItem.gameObject);
            }
        }
    }
    private bool CanUseSkill(SkillInitiative data)
    {
        sbyte state = GameMain.Player.FightState;
        if(state == Define.ENTITY_STATE_FIGHT_DEAD || state == Define.ENTITY_STATE_FIGHT_WOUNDED)
        {
            return false;
        }
        return TargetHandle.SharedInstance.IsLegalSkill(GameMain.Player.SceneEntityObj, data.id);
    }
    #endregion

    #region 更新物品状态
    private void UpdateGoods()
    {
        for (int i = 0; i < _menuItems.Length; i++)
        {
            PVEMenuItem propItem = _menuItems[i];
            if (i < 0)
            {
                propItem.Icon = string.Empty;
                propItem.Active = CanUseGoods();
                InputManager.instance.AddClickListener(propItem.gameObject, (go) =>
                {

                });
            }
            else
            {
                propItem.Icon = string.Empty;
                propItem.Active = true;
                InputManager.instance.RemoveClickListener(propItem.gameObject);
            }
        }
    }
    private bool CanUseGoods()
    {
        return true;
    }
    #endregion

    #region 更新怒气
    private void UpdateAnger(Int32 value)
    {
        _bar.value = (float)value / _angerMax;
        _value.text = string.Format("{0}/{1}", value, _angerMax);
    }
    private void OnUpdateAnger(Int32 value)
    {
        if (!Active) return;
        UpdateAnger(value);
    }
    #endregion

    #region 玩家战斗模式更新
    private void OnUpdateMode(bool autoMode)
    {
        if (!Active) return;
        if (!autoMode) return;
        //清空已选择的技能
        ClearSelect();
    }
    #endregion

    #region 玩家战斗状态更新
    private void OnUpdateState(sbyte state)
    {
        if (!Active) return;
        //复活，死亡，重伤状态切换时，需要更新
        if (state != Define.ENTITY_STATE_FIGHT_FREE && state != Define.ENTITY_STATE_FIGHT_DEAD && state != Define.ENTITY_STATE_FIGHT_WOUNDED) return;
        //清空已选择的技能
        ClearSelect();
        UpdateMenu();
    }
    #endregion

    #region 战斗实体数量变化
    private void OnUpdateFightEntityNum(object o)
    {
        if (!Active) return;
        UpdateMenu();
    }
    #endregion

    #region 玩家战斗回合开始
    private void OnUpdateRound(Int32 value)
    {
        if (!Active) return;
        //玩家表现完，需要更新
        if (value != GameMain.Player.id) return;
        //清空已选择的技能
        ClearSelect();
        UpdateMenu();
    }
    #endregion

}
