﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 剧情对话界面
/// </summary>
public class UIPlot : UIWin
{
    private UILabel title;
    private UILabel left;
    private UISprite roleLeft;
    private UILabel right;
    private UISprite roleRight;
    protected override void OnInit()
    {
        Transform bottom = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Bottom);
        Transform root = bottom.Find("Dialog");
        title = root.Find("Title").GetComponent<UILabel>();
        left = root.Find("Left").GetComponent<UILabel>();
        roleLeft = root.Find("RoleLeft").GetComponent<UISprite>();
        right = root.Find("Right").GetComponent<UILabel>();
        roleRight = root.Find("RoleRight").GetComponent<UISprite>();

        InputManager.instance.AddClickListener(root.gameObject, (go)=>
        {
            PlotManager.instance.MoveNext();
        }); 
    }

    private RoleDialogData plotData;
    protected override void OnOpen(params object[] args)
    {
        if (args == null || args.Length <= 0) return;
        plotData = (RoleDialogData)args[0];
        SetData();
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private void SetData()
    {
        switch (plotData.type)
        {
            case "Own":
                title.text = plotData.name;

                left.text = "";
                roleLeft.enabled = true;
                roleLeft.spriteName = plotData.icon;

                right.text = plotData.content;
                roleRight.enabled = false;
                break;
            case "Enemy":
                title.text = plotData.name;

                left.text = plotData.content;
                roleLeft.enabled = false;
                
                right.text = "";
                roleRight.enabled = true;
                roleRight.spriteName = plotData.icon;
                break;
            default:
                title.text = plotData.name;

                left.text = plotData.content;
                roleLeft.enabled = false;
                
                right.text = "";
                roleRight.enabled = false;
                break;
        }
    }
}
