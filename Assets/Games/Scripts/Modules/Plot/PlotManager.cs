﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// 剧情对话管理
/// </summary>
public class PlotManager : MonoSingleton<PlotManager>
{
    protected override void OnInit()
    {

    }

    protected override void OnUnInit()
    {
        _roleDialogList.Clear();
        _callBack = null;
        RemoveTimer();
    }
    private List<RoleDialogData> _roleDialogList = new List<RoleDialogData>();
    private Action _callBack;
    public void Play(uint index, Action cb)
    {
        _callBack = cb;
        _roleDialogList = RoleDialogConfig.SharedInstance.GetRoleDialogDataList(index);
        if (_roleDialogList == null || _roleDialogList.Count <= 0)
        {
            Printer.LogError(string.Format("{0}没有找不到剧情内容！", index));
        }
        else
        {
            MoveNext();
        }
    }
    private ulong _downTimerId = TimeUtils.InvalidTimerID;
    public void MoveNext()
    {
        RemoveTimer();
        if (_roleDialogList.Count == 0)
        {
            UIManager.instance.CloseWindow(WinID.UIPlot);
            if (_callBack != null) _callBack();
        }
        else
        {
            StartTimer();
            RoleDialogData roleDialogData = _roleDialogList[0];
            _roleDialogList.RemoveAt(0);
            UIManager.instance.OpenWindow(null, WinID.UIPlot, roleDialogData);
        }
    }

    private void StartTimer()
    {
        _downTimerId = TimeUtils.AddRealTimer(3.0f, Timedown, null);
        TimeUtils.StartTimer(_downTimerId);
    }
    private void RemoveTimer()
    {
        TimeUtils.RemoveTimer(_downTimerId);
        _downTimerId = TimeUtils.InvalidTimerID;
    }
    private void Timedown(int tick)
    {
        MoveNext();
    }
}
