﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 对话内容框
/// </summary>
public class DialogPage : WinPage
{
    private UISprite _icon;
    private UILabel _name;
    private UILabel _content;
    protected override void OnInit()
    {
        Transform root = transform.Find("Root");
        _icon = root.Find("Icon").GetComponent<UISprite>();
        _name = root.Find("Name").GetComponent<UILabel>();
        _content = root.Find("View/Content").GetComponent<UILabel>();
        GameObject box = root.Find("Box").gameObject;
        InputManager.instance.AddClickListener(box, (go) => 
        {
            GameMain.Player.talkModule.DoNextTalk();
        });
    }

    protected override void OnOpen(params object[] args)
    {
        TalkOptionData option = GameMain.Player.talkModule.Option;
        _content.text = option.message;

        KBEngine.GameObject kbentity = GameMain.Player.talkModule.Entity;
        if (kbentity == null) return;
        _name.text = kbentity.Name;
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }
}
