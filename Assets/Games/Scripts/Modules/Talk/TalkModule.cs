﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TalkModule
{
    private KBEngine.Avatar _avatar;
    private SensitivewordFilter _sensitivewordFilter;
    public TalkModule(KBEngine.Avatar avatar)
    {
        _avatar = avatar;
    }

    public KBEngine.GameObject Entity { get; private set; }
    public TalkOptionData Option { get; private set; }
    private Int32 _nextTalk;
    public Int32 NextTalk { get { return _nextTalk; } }
    private List<Int32> talkOptions = new List<Int32>();
    public List<Int32> TalkOptions { get { return talkOptions; } }
    public void TalkToNPC(Int32 npcId, Int32 key, Int32 nextTalk, List<object> options)
    {
        UIManager.instance.CloseWindow(WinID.UITalkDialog);
        Entity = SceneManager.instance.CurrentScene.GetKBEntityByID(npcId);
        Option = NPCTalksConfig.GetTalkOption(key);
        _nextTalk = nextTalk;
        talkOptions.Clear();
        foreach (var item in options)
        {
            Int32 id = (Int32)item;
            talkOptions.Add(id);
        }
        TalkOptionFactory.SharedInstance.CreateOption(key).DoTalk();
        if (string.IsNullOrEmpty(Option.message) && talkOptions.Count <= 0) return;
        UIManager.instance.OpenWindow(null, WinID.UITalkDialog);
    }
    public void DoNextTalk()
    {
        UIManager.instance.CloseWindow(WinID.UITalkDialog);
        if (_nextTalk != 0)
        {
            TalkOptionFactory.SharedInstance.CreateOption(_nextTalk).DoTalk(() =>
                {
                    ReceiveTalk(_nextTalk);
                });
        }
    }

    public void DoOptionTalk(Int32 key)
    {
        UIManager.instance.CloseWindow(WinID.UITalkDialog);
        TalkOptionFactory.SharedInstance.CreateOption(key).DoTalk(() =>
        {
            ReceiveTalk(key);
        });
    }

    public void ReceiveTalk(Int32 key)
    {
        if (Entity == null) return;
        Entity.cellCall("receiveTalk", new object[] { key });
    }

    public void ReceiveParamTalk(Int32 key, string jsonParam)
    {
        if (Entity == null) return;
        Entity.cellCall("receiveParamTalk", new object[] { key, jsonParam });
    }

    public void TalkTo(SceneEntity entity)
    {
        SceneEntity playerEntity = _avatar.SceneEntityObj;
        playerEntity.AnimControl.LookAt(entity.transform.position);
        entity.RotateTo(playerEntity);
        entity.KBEntity.cellCall("receiveHello");
    }
}
