﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 对话事务框
/// </summary>
public class AffairPage : WinPage
{
    private UISprite _bg;
    private Transform _text;
    private UI.Grid _uiGrid;
    private GameObject _template;
    protected override void OnInit()
    {
        Transform root = transform.Find("Root");
        _bg = root.Find("BG").GetComponent<UISprite>();
        _text = root.Find("Text");
        _uiGrid = root.Find("UIGrid").GetComponent<UI.Grid>();
        _template = root.Find("UIGrid/Item").gameObject;
        _template.SetActive(false);
    }

    protected override void OnOpen(params object[] args)
    {
        _talkOptions.Clear();
        List<Int32> options = GameMain.Player.talkModule.TalkOptions;
        foreach (var key in options)
        {
            TalkOptionData item = NPCTalksConfig.GetTalkOption(key);
            _talkOptions.Add(item);
        }
    }

    protected override void OnRefresh()
    {
        RefreshOptions();
    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

    private List<TalkOptionData> _talkOptions = new List<TalkOptionData>();
    private List<GameObject> _items = new List<GameObject>();
    private void RefreshOptions()
    {
        int dataNum = _talkOptions.Count;
        int itemNum = _items.Count;
        if (dataNum > itemNum)
        {
            for (int i = 0; i < dataNum; i++)
            {
                TalkOptionData data = _talkOptions[i];
                if (i < itemNum)
                {
                    GameObject item = _items[i];
                    gameObject.SetActive(true);
                    UILabel label = item.transform.Find("Text").GetComponent<UILabel>();
                    label.text = data.title;
                    InputManager.instance.AddClickListener(item, (go) => 
                    {
                        GameMain.Player.talkModule.DoOptionTalk(data.key);
                    });
                }
                else
                {
                    _template.SetActive(true);
                    GameObject item = Instantiate(_template) as GameObject;
                    _template.SetActive(false);
                    item.name = i.ToString();
                    item.transform.parent = _uiGrid.transform;
                    item.transform.localPosition = Vector3.zero;
                    item.transform.localEulerAngles = Vector3.zero;
                    item.transform.localScale = Vector3.one;
                    _items.Add(item);
                    UILabel label = item.transform.Find("Text").GetComponent<UILabel>();
                    label.text = data.title;
                    InputManager.instance.AddClickListener(item, (go) =>
                    {
                        GameMain.Player.talkModule.DoOptionTalk(data.key);
                    });
                }
            }
        }
        else
        {
            for (int i = 0; i < itemNum; i++)
            {
                GameObject item = _items[i];
                if (i < dataNum)
                {
                    TalkOptionData data = _talkOptions[i];
                    item.SetActive(true);
                    UILabel label = item.transform.Find("Text").GetComponent<UILabel>();
                    label.text = data.title;
                    InputManager.instance.AddClickListener(item, (go) =>
                    {
                        GameMain.Player.talkModule.DoOptionTalk(data.key);
                    });
                }
                else
                {
                    item.SetActive(false);
                }
            }
        }
        _uiGrid.Reposition();
        Align(dataNum);
    }

    private void Align(int num)
    {
        int n = num - 1;
        _bg.height = 185 + (int)(_uiGrid.cellHeight * n);
        _text.localPosition = new Vector3(_text.localPosition.x, _uiGrid.cellHeight * n, _text.localPosition.z);
    }
}
