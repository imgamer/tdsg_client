﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 对话界面
/// </summary>
public class UITalkDialog : UIWin
{
    private DialogPage _dialogPage;
    private AffairPage _affairPage;
    protected override void OnInit()
    {
        Transform bottom = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.Bottom);
        _dialogPage = bottom.Find("Dialog").GetComponent<DialogPage>();
        Transform bottomRight = UITools.GetUIAnchorTrans(transform, UIAnchor.Side.BottomRight);
        _affairPage = bottomRight.Find("Affair").GetComponent<AffairPage>();
        InputManager.instance.AddClickListener(gameObject, (go) => 
        {
            if (GameMain.Player.talkModule.TalkOptions.Count > 0)
                UIManager.instance.CloseWindow(winID);
            else
                GameMain.Player.talkModule.DoNextTalk();
        });
    }
    protected override void OnOpen(params object[] args)
    {
        if (GameMain.Player.talkModule.Entity != null)
        {
            TalkOptionData option = GameMain.Player.talkModule.Option;
            if(!string.IsNullOrEmpty(option.message)) 
                _dialogPage.Open(this);
            else
                _dialogPage.Close();
        }
        else
        {
            _dialogPage.Close();
        }

        if (GameMain.Player.talkModule.TalkOptions.Count > 0)
        {
            _affairPage.Open(this);
        }
        else
        {
            _affairPage.Close();
        }
    }

    protected override void OnRefresh()
    {

    }

    protected override void OnClose()
    {

    }

    protected override void OnUnInit()
    {

    }

}
