﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SensitivewordFilterFactory
{
    /**
     * 敏感词过滤器工厂-类职责：
     * 创建一个敏感词过滤器，让其初始化敏感词库，将敏感词加入到字典中，构建DFA算法模型，
     * 判断文本中是否存在敏感词，获取敏感词和替换敏感词等
     * 享元设计: 避免大量拥有相同内容的敏感词过滤小类重复创建，使其共享一个敏感词过滤类。
     */


    private static Dictionary<int, SensitivewordFilter> _sensitiveFilterType = new Dictionary<int, SensitivewordFilter>();

    public static SensitivewordFilter GetSensitiveFilter(int sensitiveFilterType)
    {
        if (_sensitiveFilterType.ContainsKey(sensitiveFilterType))
        {
            return _sensitiveFilterType[sensitiveFilterType];
        }
        else
        {
            List<string> sensitiveWordList = SensitivewordsConfig.SharedInstance.SensitiveWordList;
            SensitivewordFilter sensitivewordFilter = new SensitivewordFilter(sensitiveWordList);
            _sensitiveFilterType[sensitiveFilterType] = sensitivewordFilter;
            return sensitivewordFilter;
        }

    }

}


public class SensitivewordFilter
{
    private Dictionary<string, object> _sensitiveWordMap = null;

    public SensitivewordFilter(List<string> keyWords)
    {
        _sensitiveWordMap = new Dictionary<string, object>();
        InitKeyWord(keyWords);

    }

    /**
     *  初始化敏感词库
     */
    private void InitKeyWord(List<string> keyWords)
    {
        if (keyWords == null || keyWords.Count == 0)
        {
            return;
        }
        // 将敏感词库加入到sensitiveWordMap中
        AddSensitiveWordToHashMap(keyWords);     
    }

    /**
     *  构建一个DFA算法模型：
     *  格式如下：
     *      "操","毛泽东","他妈的","他妈" 
     *          ==>> 
     *      {操={isEnd=1}, 他={isEnd=0, 妈={isEnd=1, 的={isEnd=1}}}, 毛={isEnd=0, 泽={isEnd=0, 东={isEnd=1}}}}
     */
    private void AddSensitiveWordToHashMap(List<string> keyWords)
    {
        Dictionary<string, object> nowMap = null;
        Dictionary<string, object> newWorMap = null;
        foreach(string key in keyWords)
        {
            nowMap = _sensitiveWordMap;
            for(int i = 0 ; i < key.Length ; i++)
            {
                string keyChar = Convert.ToString(key[i]);    //获取单个字符    
                if (nowMap.ContainsKey(keyChar)) // 如果存在该key，直接赋值
                {
                    object wordMap = nowMap[keyChar];
                    nowMap = (Dictionary<string, object>)wordMap;
                }
                else
                {
                    newWorMap = new Dictionary<string, object>();
                    newWorMap["isEnd"] = 0;       //不是最后一个
                    nowMap[keyChar] = newWorMap;
                    nowMap = newWorMap;
                }

                if (i == key.Length - 1)
                {
                    nowMap["isEnd"] = 1;   //最后一个
                }
            }
        }
    }

    /**
     *  判断文字是否包含敏感字符, 包含返回True
     *  @param txt: 检验的文本
     *  @type  txt: string 
     *  @param matchType: 匹配类型
     *  @type  matchType: INT8
     */
    public bool IsContaintSensitiveWord(string txt, sbyte matchType = Define.SENSITIVE_MATCH_TYPE_MIN)
    {
        bool flag = false;
        for (int i = 0; i < txt.Length; i++)
        {
            int matchFlag = CheckSensitiveWord(txt, i, matchType); //判断是否包含敏感字符
            if (matchFlag > 0)
            {    //大于0存在，返回true
                flag = true;
            }
        }
        return flag;
    }

    private int CheckSensitiveWord( string txt, int beginIndex, int matchType )
    {
        bool flag = false;    //敏感词结束标识位：用于敏感词只有1位的情况
        int matchFlag = 0;     //匹配标识数默认为0
        string word = "";
        Dictionary<string, object> nowMap = _sensitiveWordMap;
        for (int i = beginIndex; i < txt.Length; i++)
        {
            word = Convert.ToString(txt[i]);

            if (nowMap.ContainsKey(word))
            {
                //存在，则判断是否为最后一个
                nowMap = (Dictionary<string, object>)nowMap[word];     //获取指定key
                matchFlag++;     //找到相应key，匹配标识+1
                if (1 == Convert.ToInt32(nowMap["isEnd"])) //如果为最后一个匹配规则,结束循环，返回匹配标识数
                {
                    flag = true;       //结束标志位为true
                    if (Define.SENSITIVE_MATCH_TYPE_MIN == matchType)  //最小规则，直接返回,最大规则还需继续查找
                    {   
                        break;
                    }

                }
            }
            else  //不存在，直接返回
            {
                break;
            } 
        }

        //		if(matchFlag < 2 || !flag){        //长度必须大于等于1，为词 
        //			matchFlag = 0;
        //		}
        if (!flag)
        {
            matchFlag = 0;
        }
        return matchFlag;
    }

    /**
     *  获取文本中的敏感词列表
     *  @param txt: 检验的文本
     *  @type  txt: string 
     *  @param matchType: 匹配类型
     *  @type  matchType: INT8
     */
    public HashSet<string> GetSensitiveWord(string txt, int matchType = Define.SENSITIVE_MATCH_TYPE_MIN)
    {
        HashSet<string> sensitiveWordList = new HashSet<string>();
        for(int i = 0 ; i < txt.Length; i++)
        {
			int length = CheckSensitiveWord(txt, i, matchType);    //判断是否包含敏感字符
			if(length > 0){    //存在,加入list中
				sensitiveWordList.Add(txt.Substring(i, length));
				i = i + length - 1;    //减1的原因，是因为for会自增
			}
		}
		return sensitiveWordList;
    }

    /**
     *  替换敏感字字符串并放回
     *  @param txt: 检验的文本
     *  @type  txt: string 
     *  @param replaceChar: 代替的字符，比如"*"
     *  @type  replaceChar: string
     *  @param matchType: 匹配类型
     *  @type  matchType: INT8
     */
    public string ReplaceSensitiveWord(string txt, string replaceChar = "*", int matchType = Define.SENSITIVE_MATCH_TYPE_MIN)
    {
        string resultTxt = txt;
        HashSet<string> wordSet = GetSensitiveWord(txt, matchType);     //获取所有的敏感词
        string replaceString = null;
        foreach(string word in wordSet)
        {
            replaceString = GetReplaceChars(replaceChar, word.Length);
            resultTxt = resultTxt.Replace(word, replaceString);
        }
        return resultTxt;
    }

    private string GetReplaceChars(string replaceChar,int length)
    {
		string resultReplace = replaceChar;
		for(int i = 1 ; i < length ; i++){
			resultReplace += replaceChar;
		}
		return resultReplace;
	}
}

