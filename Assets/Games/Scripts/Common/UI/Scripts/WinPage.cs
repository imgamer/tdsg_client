﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// UI嵌套窗口基类
/// </summary>
public abstract class WinPage : BasePage 
{
    public UIWin ParentUI { get; private set; }
    private PageAnim pageAnim;

    public override bool Active
    {
        get 
        {
            if (ParentUI == null) return false;
            return ParentUI.Active && active; 
        }
    }

    public void Open(UIWin parent, params object[] args)
    {
        this.ParentUI = parent;
        if (!inited)
        {
            OnInit();
            inited = true;
        }
        //解决在OnOpen调用激活状态条件下使用的函数方法
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        active = true;
        OnOpen(args);
        Refresh();
    }

    public override void PlayOpenAnim(Action callBack)
    {
        if (ParentUI && ParentUI.IsPlaying) return;

        if (pageAnim == null) pageAnim = transform.GetComponent<PageAnim>();
        if (pageAnim != null && pageAnim.isActiveAndEnabled && pageAnim.ActiveClipLength > 0)
        {
            pageAnim.Init();
            if (pageAnim.isPlaying) return;
            pageAnim.PlayOpenAnim();
            StartCoroutine(DoDelay(pageAnim.ActiveClipLength, callBack));
        }
        else
        {
            if (callBack != null) callBack();
        }
    }

}
