﻿/// <summary>
/// 窗口ID
/// </summary>
public enum WinID
{
    None = -1,

    UIConnect,
    UILogin,
    UIDownloadInfo,
    UIDownload,
    UIServer,
    UISelectServer,
    UISelectRole,
    UILoading,
    UIBridge,

    UIMain,
    UIHead,
    UITaskTeam,
    UIChat,
    UIChatSettings,
    UIChannel,
    UIChatExpand,
    UIChatMenu,

    UITask,
    UITalkDialog,

    UITeam,
    UITeamTarget,
    UIRoleMenu,

    UIPet,
    UIPetPunctuate,
    UIPetEvolution,
    UISelectPet,
    UICombinePet,
    UICombinePetResult,

    UIWorldMap,
    UIWorldDetailMap,
    UIPVE,
    UIPlot,
	UIFriend,
	UIAddFriend,
	UIKitbag,
	UICharacter,
    UICharacterPlusPointHelp,
    UIShop,
    UIShopPage,
    UIPitchPage,
    UICommercePage,
    UIRechargePage,
    UIPropSearch,
    UITradingRecord,
    UIPropOperate,
    UIActivity,
    UISkill,

    UISlave,
    UISlaveRecord,
    UISlaveDefeatedOpponent,
    UIArenaFightRecord,
    UIArenaWin,
    UIArenaLose,
    UIArenaRankUp,

    UIRank,
    UIPetDetail,
    UIRoleInfo,

    UISign,


#region 装备，道具等提示详情界面
    UIEquipItemDetail,
    UIItemDetail,
#endregion

    UIEquipManufacture,
    UIEquipRepair,

    UIItemUse,
    UIItemIntonate,
    UISubmitPet,
    UISubmitItem,

    UIPata,

    UIFaction,
    UIFactionCreate,
    UIFactionDonate,
    UIFactionRecruit,
    UIFactionMemberMenu,
    UIFactionMemberAdvancedMenu,
    UIFactionShop,

    UIHero,
    UIHeroGift,

    UITeamCopy,
    UITeamCopyConfirm,
    UITeamCopyLeave,

    Max
}

