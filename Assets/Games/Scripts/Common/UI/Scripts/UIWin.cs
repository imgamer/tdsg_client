﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum WinPriority
{
    Bottom,  //底部优先级
    Top,     //置顶优先级
}
/// <summary>
/// UI窗口基类
/// </summary>
[RequireComponent(typeof(UIPanel))]
public abstract class UIWin : BasePage
{
    [HideInInspector]
    public WinID winID = WinID.None;
    [HideInInspector]
    public WinPriority winPriority = WinPriority.Bottom;
    /// <summary>
    /// 来源的UI界面（例如从A界面打开B界面，B关闭需要返回A时的需求，才需要设置）
    /// </summary>
    public UIWin SourceUI { get; private set; }

    private PageAnim uiAnim;
    private LinkedList<PageAnim> anims = new LinkedList<PageAnim>();

    public override bool Active
    {
        get { return active; }
    }

    public int SortingOrder { get; private set; }
    private Dictionary<UIPanel, int> childPanels = new Dictionary<UIPanel, int>();
    public void Init()
    {
        if (!inited)
        {
            InitAtlas();
            OnInit();
            UIPanel[] panels = transform.GetComponentsInChildren<UIPanel>(true);
            for (int i = 0; i < panels.Length; i++)
            {
                UIPanel p = panels[i];
                childPanels[p] = p.depth;
            }
            InitAnims();
            inited = true;
        }
    }
    public void Open(UIWin sourceUI, params object[] args)
    {
        this.SourceUI = sourceUI;
        //解决在OnOpen调用激活状态条件下使用的函数方法
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        active = true;
        OnOpen(args);
        Refresh();
    }

    private DynamicAtlas dynamicAtlas;
    private void InitAtlas()
    {
        dynamicAtlas = transform.GetComponent<DynamicAtlas>();
        if (dynamicAtlas)
        {
            if (dynamicAtlas.m_atlas == null || dynamicAtlas.m_atlas.Length == 0)
            {
                Printer.LogError("{0} 界面的动态图集个数为0", winID);
            }
        }
    }

    public void SetDepth(int depth)
    {
        transform.localPosition = new Vector3(0, 0, -depth);
        foreach (UIPanel p in childPanels.Keys)
        {
            p.depth = childPanels[p] + depth;
            p.sortingOrder = p.depth;
        }
        SortingOrder = depth;
    }

    #region 设置使用动态图集的精灵名字
    public void SetDynamicSpriteName(UISprite sprite, string spriteName)
    {
        if (dynamicAtlas) dynamicAtlas.SetDynamicSpriteName(sprite, spriteName);
    }
    #endregion

    #region UI动画播放
    private void InitAnims()
    {
        uiAnim = transform.GetComponent<PageAnim>();
        if (uiAnim != null)
        {
            uiAnim.Init();
            if (uiAnim.ActiveClipLength > 0) anims.AddFirst(uiAnim);
        }
        PageAnim[] pageAnims = transform.GetComponentsInChildren<PageAnim>(true);
        foreach (var item in pageAnims)
        {
            if (item == uiAnim) continue;
            if (item != null)
            {
                item.Init();
                if (item.ActiveClipLength > 0) anims.AddLast(item);
            }
        }
    }
    private IEnumerator<PageAnim> animItr;
    private bool isPlaying = false;
    public bool IsPlaying { get { return isPlaying; } }
    public override void PlayOpenAnim(Action callBack)
    {
        try
        {
            if (isPlaying) return;
            animItr = anims.GetEnumerator();
            animItr.Reset();
            isPlaying = true;
            MoveNext(callBack);
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            isPlaying = false;
            if (callBack != null) callBack();
        }
    }
    private void MoveNext(Action callBack)
    {
        if (animItr.MoveNext())
        {
            if (animItr.Current.isActiveAndEnabled)
            {
                animItr.Current.PlayOpenAnim();
                StartCoroutine(DoDelay(animItr.Current.ActiveClipLength, () =>
                {
                    MoveNext(callBack);
                }));
            }
            else
            {
                MoveNext(callBack);
            }
        }
        else
        {
            isPlaying = false;
            if (OnPlayCallBack != null) OnPlayCallBack();
            if (callBack != null) callBack();
        }
    }
    public void PlayCloseAnim(Action callBack)
    {
        try
        {
            if (uiAnim != null && uiAnim.isActiveAndEnabled && uiAnim.InactiveClipLength > 0)
            {
                uiAnim.Init();
                if (uiAnim.isPlaying) return;
                uiAnim.PlayCloseAnim();
                StartCoroutine(DoDelay(uiAnim.InactiveClipLength, callBack));
            }
            else
            {
                if (callBack != null) callBack();
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            if (callBack != null) callBack();
        }
    }
    public void StopAllPlay()
    {
        if (uiAnim) uiAnim.StopPlay();
        foreach (var item in anims)
        {
            if (item) item.StopPlay();
        }
        isPlaying = false;
    }
    void OnDisable()
    {
        StopAllPlay();
    }
    #endregion

}
