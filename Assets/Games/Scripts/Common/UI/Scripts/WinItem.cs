﻿using UnityEngine;
using System.Collections;
/// <summary>
/// UI内部对象基类
/// </summary>
public abstract class WinItem : MonoBehaviour 
{
    public UIWin ParentUI { get; private set; }
    protected abstract void OnInit();

    private bool inited = false;
    public void Init(UIWin parent)
    {
        if (!inited)
        {
            this.ParentUI = parent;
            OnInit();
            inited = true;
        }
    }
}
