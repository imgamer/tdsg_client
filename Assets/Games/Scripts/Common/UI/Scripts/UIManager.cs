﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// UI管理类
/// </summary>
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// 缓存的UI对象
    /// </summary>
    private UIWin[] cacheWins = new UIWin[(int)WinID.Max];
    /// <summary>
    /// 处于激活状态的UI对象
    /// </summary>
    private Dictionary<WinPriority, LinkedList<UIWin>> activeWinDict = new Dictionary<WinPriority, LinkedList<UIWin>>(2) 
    {
        {WinPriority.Bottom, new LinkedList<UIWin>()},
        {WinPriority.Top, new LinkedList<UIWin>()}
    };
    /// <summary>
    /// UI间的深度间距
    /// </summary>
    private const int offsetDepth = 50;

    public static UIManager instance { get; private set; }
    void Awake()
    {
        UIRoot uiRoot = transform.parent.GetComponent<UIRoot>();
#if UNITY_ANDROID || UNITY_IPHONE
        uiRoot.scalingStyle = UIRoot.Scaling.ConstrainedOnMobiles;
#elif UNITY_WEBPLAYER || UNITY_STANDALONE_WIN
        uiRoot.scalingStyle = UIRoot.Scaling.Constrained;
#endif
        instance = this;
    }

    #region 激活UI列表的操作
    private UIWin[] GetAllActiveWins()
    {
        LinkedList<UIWin> bottomList = GetActiveWins(WinPriority.Bottom);
        LinkedList<UIWin> topList = GetActiveWins(WinPriority.Top);
        UIWin[] winArray = new UIWin[bottomList.Count + topList.Count];
        bottomList.CopyTo(winArray, 0);
        topList.CopyTo(winArray, bottomList.Count);
        return winArray;
    }
    private LinkedList<UIWin> GetActiveWins(WinPriority winPriority)
    {
        LinkedList<UIWin> list;
        activeWinDict.TryGetValue(winPriority, out list);
        return list;
    }
    private void RemoveWin(UIWin ui)
    {
        LinkedList<UIWin> list;
        activeWinDict.TryGetValue(ui.winPriority, out list);
        list.Remove(ui);
    }
    private void AddWinAtLast(WinPriority winPriority, UIWin ui)
    {
        ui.winPriority = winPriority;
        LinkedList<UIWin> list;
        activeWinDict.TryGetValue(ui.winPriority, out list);
        list.AddLast(ui);
    }
    private bool IsActiveUI(UIWin ui)
    {
        LinkedList<UIWin> list;
        activeWinDict.TryGetValue(ui.winPriority, out list);
        return list.Contains(ui);
    }
    #endregion

    #region 获取UI相关
    /// <summary>
    /// 加载UI
    /// </summary>
    /// <param name="winID"></param>
    /// <returns></returns>
    private UIWin LoadUI(WinID winID)
    {
        UIWin UI = GetCacheUI(winID);
        try
        {
            if (UI == null)
            {
                GameObject prefab = Resources.Load<GameObject>(string.Format("UI/{0}", winID));
                if (prefab == null)
                {
                    Printer.LogWarning("UI/{0} is not exist!", winID);
                    return null;
                }
                else
                {
                    GameObject go = (GameObject)Instantiate(prefab);
                    go.name = prefab.name;
                    UI = go.GetComponent<UIWin>();
                    if (!UI)
                    {
                        Printer.LogError("UI:{0} has not UIWin component!", winID);
                        return null;
                    }
                    Transform trans = go.transform;
                    trans.parent = transform;
                    trans.localScale = Vector3.one;
                    trans.localPosition = Vector3.one;
                    cacheWins[(int)winID] = UI;
                    UI.winID = winID;
                    go.SetActive(false);
                }
            }
            return UI;
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            return null;
        }
    }
    /// <summary>
    /// 设置UI的深度
    /// </summary>
    private void SetWindowsDepth()
    {
        UIWin[] winArray = GetAllActiveWins();
        for (int i = 0; i < winArray.Length; i++)
        {
            UIWin ui = winArray[i];
            if (ui != null)
            {
                ui.SetDepth((i + 1) * offsetDepth);
            }
        }
    }
    /// <summary>
    /// 获取缓存UI
    /// </summary>
    /// <param name="winID"></param>
    /// <returns></returns>
    public UIWin GetCacheUI(WinID winID)
    {
        return cacheWins[(int)winID];
    }
    /// <summary>
    /// 获取激活状态的UI
    /// </summary>
    /// <param name="winID"></param>
    /// <returns></returns>
    public bool IsActiveUI(WinID winID)
    {
        UIWin ui = GetCacheUI(winID);
        if (ui == null) return false;
        return IsActiveUI(ui);
    }
    #endregion

    #region 打开UI相关
    /// <summary>
    /// 打开普通UI，改变UI的父窗口
    /// </summary>
    /// <param name="fromUI"></param>
    /// <param name="toUI"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public UIWin OpenWindow(UIWin fromUI, WinID toUI, params object[] args)
    {
        return OpenPriorityWindow(fromUI, toUI, WinPriority.Bottom, args);
    }
    /// <summary>
    /// 打开置顶UI，改变UI的父窗口（慎用，置顶UI打开普通UI会表现异常：原因置顶UI总是显示在普通UI上方）
    /// </summary>
    /// <param name="fromUI"></param>
    /// <param name="toUI"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public UIWin OpenTopWindow(UIWin fromUI, WinID toUI, params object[] args)
    {
        return OpenPriorityWindow(fromUI, toUI, WinPriority.Top, args);
    }
    private UIWin OpenPriorityWindow(UIWin fromUI, WinID toUI, WinPriority winPriority, params object[] args)
    {
        try
        {
            if (fromUI && fromUI.winID == toUI)
            {
                Printer.LogError("UI:{0} Can not open myself!", fromUI.winID);
                return fromUI;
            }
            UIWin ui = LoadUI(toUI);
            if (ui == null) return null;

            //当前UI激活列表中移至最顶层，并打开
            RemoveWin(ui);
            AddWinAtLast(winPriority, ui);

            ui.StopAllPlay();
            ui.Init();
            SetWindowsDepth();
            ui.Open(fromUI, args);
            ui.PlayOpenAnim(null);
            return ui;
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            return null;
        }
    }
    #endregion

    #region 返回UI相关
    /// <summary>
    /// 返回上一层UI
    /// </summary>
    /// <param name="fromUI"></param>
    /// <returns></returns>
    public UIWin BackToWindow(UIWin fromUI)
    {
        try
        {
            if (fromUI == null) return null;
            //当前UI激活列表中移除，并关闭
            if (IsActiveUI(fromUI))
            {
                RemoveWin(fromUI);
                fromUI.StopAllPlay();
                fromUI.PlayCloseAnim(() =>
                {
                    if (fromUI && !IsActiveUI(fromUI)) fromUI.Close();
                });
            }

            //来源UI激活列表中移至最顶层，并刷新
            UIWin ui = fromUI.SourceUI;
            if (ui == null) return null;

            RemoveWin(ui);
            AddWinAtLast(ui.winPriority, ui);
            ui.StopAllPlay();
            SetWindowsDepth();
            ui.Refresh();
            return ui;
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
            return null;
        }
    }
    #endregion

    #region 关闭UI相关
    /// <summary>
    /// 关闭UI
    /// </summary>
    /// <param name="winID"></param>
    public void CloseWindow(WinID winID)
    {
        try
        {
            UIWin ui = GetCacheUI(winID);
            if (ui == null) return;

            //当前UI激活列表中移除，并关闭
            if (IsActiveUI(ui))
            {
                RemoveWin(ui);
                ui.PlayCloseAnim(() =>
                {
                    if (ui && !IsActiveUI(ui)) ui.Close();
                });
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 关闭所有UI，除了winID
    /// </summary>
    /// <param name="winID"></param>
    public void CloseAllExcept(WinID winID)
    {
        try
        {
            UIWin[] winArray = GetAllActiveWins();
            for (int i = 0; i < winArray.Length; i++)
            {
                UIWin ui = winArray[i];
                if (ui != null)
                {
                    if (ui.winID == winID) continue;
                    //当前UI激活列表中移除，并关闭
                    RemoveWin(ui);
                    ui.Close();
                }
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 关闭所有UI，除了winIDs
    /// </summary>
    /// <param name="winIDs"></param>
    public void CloseAllExcept(List<WinID> winIDs)
    {
        try
        {
            if (winIDs != null)
            {
                Printer.LogError("winIDs不能为空！");
                return;
            }
            UIWin[] winArray = GetAllActiveWins();
            for (int i = 0; i < winArray.Length; i++)
            {
                UIWin ui = winArray[i];
                if (ui != null)
                {
                    if (winIDs.Contains(ui.winID)) continue;
                    //当前UI激活列表中移除，并关闭
                    RemoveWin(ui);
                    ui.Close();
                }
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 关闭所有UI
    /// </summary>
    public void CloseAll()
    {
        try
        {
            UIWin[] winArray = GetAllActiveWins();
            for (int i = 0; i < winArray.Length; i++)
            {
                UIWin ui = winArray[i];
                if (ui != null)
                {
                    //当前UI激活列表中移除，并关闭
                    RemoveWin(ui);
                    ui.Close();
                }
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    #endregion

    #region 销毁UI相关
    /// <summary>
    /// 销毁UI
    /// </summary>
    /// <param name="winID"></param>
    public void DestroyWindow(WinID winID)
    {
        try
        {
            //当前UI激活列表中移除，并销毁
            UIWin ui = GetCacheUI(winID);
            if (ui == null) return;
            if (IsActiveUI(ui))
            {
                RemoveWin(ui);
                ui.UnInit();
            }
            Destroy(ui.gameObject);
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 销毁所有UI，除了winID
    /// </summary>
    /// <param name="winID"></param>
    public void DestroyAllExcept(WinID winID)
    {
        try
        {
            for (int i = 0; i < cacheWins.Length; i++)
            {
                //当前UI激活列表中移除，并销毁
                UIWin ui = cacheWins[i];
                if (ui == null || ui.winID == winID) continue;
                if (IsActiveUI(ui))
                {
                    RemoveWin(ui);
                    ui.UnInit();
                }
                Destroy(ui.gameObject);
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 销毁所有UI
    /// </summary>
    public void DestroyAll()
    {
        try
        {
            for (int i = 0; i < cacheWins.Length; i++)
            {
                //当前UI激活列表中移除，并销毁
                UIWin ui = cacheWins[i];
                if (ui == null) continue;
                if (IsActiveUI(ui))
                {
                    RemoveWin(ui);
                    ui.UnInit();
                }
                Destroy(ui.gameObject);
            }
        }
        catch (System.Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    #endregion

    #region 隐藏/显示激活的UI
    public void HideAll()
    {
        UIWin[] winArray = GetAllActiveWins();
        for (int i = 0; i < winArray.Length; i++)
        {
            UIWin ui = winArray[i];
            if (ui != null)
            {
                ui.gameObject.SetActive(false);
            }
        }
    }
    public void ShowAll()
    {
        UIWin[] winArray = GetAllActiveWins();
        for (int i = 0; i < winArray.Length; i++)
        {
            UIWin ui = winArray[i];
            if (ui != null)
            {
                ui.gameObject.SetActive(true);
            }
        }
    }
    #endregion

}
