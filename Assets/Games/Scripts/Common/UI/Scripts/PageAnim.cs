﻿using UnityEngine;
/// <summary>
/// 界面动画组件
/// </summary>
[AddComponentMenu("NGUI/Custom/PageAnim")]
[RequireComponent(typeof(BasePage))]
public class PageAnim : MonoBehaviour 
{
    private Animation anim;
    public AnimationClip activeAnim;
    public AnimationClip inactiveAnim;

    public bool isPlaying
    {
        get { return anim && anim.isPlaying; }
    }

    public void Init()
    {
        if (anim) return;
        anim = gameObject.GetComponent<Animation>();
        if (!anim)
        {
            anim = gameObject.AddComponent<Animation>();
        }
        else
        {
            anim.playAutomatically = false;
        }
        if (activeAnim) anim.AddClip(activeAnim, activeAnim.name);
        if (inactiveAnim) anim.AddClip(inactiveAnim, inactiveAnim.name);
    }

    public void PlayOpenAnim()
    {
        if (activeAnim == null) return;
        if (anim && !anim.IsPlaying(activeAnim.name))
        {
            anim.Stop();
            anim.Play(activeAnim.name);
        }
    }

    public void PlayCloseAnim()
    {
        if (inactiveAnim == null) return;
        if (anim && !anim.IsPlaying(inactiveAnim.name))
        {
            anim.Stop();
            anim.Play(inactiveAnim.name);
        }
    }

    public void StopPlay()
    {
        if (isPlaying) anim.Stop();
    }

    public float ActiveClipLength
    {
        get 
        {
            if (activeAnim)
            {
                return activeAnim.length;
            }
            else
            {
                return 0;
            }
        }
    }

    public float InactiveClipLength
    {
        get
        {
            if (inactiveAnim)
            {
                return inactiveAnim.length;
            }
            else
            {
                return 0;
            }
        }
    }

}
