﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// UIRoot下对象的缓存类（UIWin对象除外，只存储Layer：UI的对象）
/// </summary>
public class CacheManager : MonoBehaviour 
{
    #region 初始化
    public static CacheManager instance { get; private set; }
    void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    #endregion

    private Dictionary<string, LinkedList<GameObject>> prefabDict = new Dictionary<string, LinkedList<GameObject>>();
    public GameObject Spawn(string name, string path)
    {
        LinkedList<GameObject> list;
        if (!prefabDict.TryGetValue(name, out list))
        {
            list = new LinkedList<GameObject>();
            GameObject prefab = Resources.Load<GameObject>(path);
            GameObject t = Instantiate<GameObject>(prefab);
            t.name = name;
            t.SetActive(false);
            t.transform.parent = transform;
            t.transform.localPosition = Vector3.zero;
            t.transform.localEulerAngles = Vector3.zero;
            t.transform.localScale = Vector3.one;
            list.AddLast(t);
            prefabDict[name] = list;
        }
        GameObject ret = null;
        if (list.Count == 1)
        {
            ret = Instantiate(list.First.Value) as GameObject;
            ret.transform.parent = transform;
        }
        else if (list.Count > 1)
        {
            ret = list.Last.Value;
            if (ret == null)
            {
                Printer.LogError(string.Format("UI的缓存:{0}被意外删除!", name));
                return null;
            }
            list.RemoveLast();
        }
        ret.name = name;
        ret.SetActive(true);
        return ret;
    }

    public void Despawn(string name, GameObject o)
    {
        if (o == null) return;
        LinkedList<GameObject> list;
        if (prefabDict.TryGetValue(name, out list))
        {
            o.transform.parent = transform;
            list.AddLast(o);
        }
        else
        {
            Printer.LogError("找不到缓存列表：{0}", name);
            Destroy(o);
        }
    }

}
