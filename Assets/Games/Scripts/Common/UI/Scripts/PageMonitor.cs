﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 页面焦点监视器
/// </summary>
[AddComponentMenu("NGUI/Custom/PageMonitor")]
[RequireComponent(typeof(BasePage))]
public class PageMonitor : MonoBehaviour 
{
    public enum Type
    {
        ActiveWhenClickMe,
        InactiveWhenClickMe,
    }
    public Type type = Type.InactiveWhenClickMe;
    void Start()
    {
        BasePage thisPage = transform.GetComponent<BasePage>();
        if (thisPage == null)
        {
            Printer.LogError("找到BasePage组件：{0}", gameObject.name);
            return;
        }
        if (thisPage is UIWin)
        {
            UIWin thisWin = (UIWin)thisPage;
            InputManager.instance.AddHitListener(gameObject, (go) =>
            {
                UIWin ui = UITools.FindInParents<UIWin>(go);
                if (ui != thisPage || ShouldClose(go)) UIManager.instance.CloseWindow(thisWin.winID);
            });
        }
        else if (thisPage is WinPage)
        {
            InputManager.instance.AddHitListener(gameObject, (go) =>
            {
                WinPage page = UITools.FindInParents<WinPage>(go);
                if (page != thisPage || ShouldClose(go)) thisPage.Close();
            });
        }
    }
    private bool ShouldClose(GameObject go)
    {
        switch (type)
        {
            case Type.ActiveWhenClickMe:
                return false;
                break;
            case Type.InactiveWhenClickMe:
                return go == gameObject;
                break;
            default:
                return false;
                break;
        }
    }
}
