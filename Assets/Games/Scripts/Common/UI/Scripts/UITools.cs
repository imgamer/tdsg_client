﻿using UnityEngine;
using System.Collections;
using UI;
/// <summary>
/// UI工具类
/// </summary>
public class UITools : MonoBehaviour 
{
    /// <summary>
    /// 复制固定数量的子对象
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="targetCount"></param>
    public static void CopyFixNumChild(Transform parent, int targetCount)
    {
        if (parent == null || targetCount < 0) return;
        Transform firstChild = parent.GetChild(0);
        int toAddCount = targetCount - parent.childCount;
        if (toAddCount > 0)
        {
            for (int i = 0; i < toAddCount; i++)
            {
                Transform child = Instantiate(firstChild, firstChild.position, firstChild.rotation) as Transform;
                child.parent = parent;
                child.localScale = firstChild.localScale;
            }
        }
        else
        {
            for (int i = parent.childCount - 1; i >= targetCount; i--)
            {
                Transform child = parent.GetChild(i);
                DestroyImmediate(child.gameObject);
            }
        }
    }
    /// <summary>
    /// 获取UI根节点
    /// </summary>
    /// <param name="ui"></param>
    /// <param name="side"></param>
    /// <returns></returns>
    public static Transform GetUIAnchorTrans(Transform ui, UIAnchor.Side side)
    {
        if (ui == null)
        {
            Printer.LogError("UI对象不能为空");
            return null;
        }
        Transform trans = ui.Find(string.Format("Root/{0}", side));
        return trans;
    }
    /// <summary>
    /// 添加子对象
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="temp"></param>
    /// <returns></returns>
    public static Transform AddChild(Transform parent, GameObject temp)
    {
        if (parent == null || temp == null) return null;
        GameObject child = Instantiate(temp, temp.transform.position, temp.transform.rotation) as GameObject;
        child.transform.parent = parent;
        child.transform.localScale = temp.transform.localScale;
        return child.transform;
    }
    /// <summary>
    /// 播放UI动画
    /// </summary>
    /// <param name="animation"></param>
    /// <param name="animtionState"></param>
    /// <param name="forward"></param>
    public static void PlayUIAnim(Animation animation, AnimationState animtionState, bool forward)
    {
        if (forward)
        {
            if (!animation.isPlaying)
            {
                animation.Play(animtionState.clip.name);
                animtionState.time = 0;
            }
            animtionState.speed = 1f;
        }
        else
        {
            if (!animation.isPlaying)
            {
                animation.Play(animtionState.clip.name);
                animtionState.time = animtionState.clip.length;
            }
            animtionState.speed = -1f;
        }
    }
    /// <summary>
    /// 获取父对象的BasePage
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="go"></param>
    /// <returns></returns>
    public static T FindInParents<T>(GameObject go) where T : BasePage
    {
        if (go == null) return null;
        T comp = go.GetComponent<T>();
        if (comp == null)
        {
            Transform t = go.transform.parent;

            while (t != null && comp == null)
            {
                comp = t.gameObject.GetComponent<T>();
                t = t.parent;
            }
        }
        return comp;
    }
    /// <summary>
    /// 设置按钮状态
    /// </summary>
    /// <param name="go"></param>
    /// <param name="active"></param>
    public static void SetButtonState(GameObject go, bool active)
    {
        if (go == null) return;
        BoxCollider box = go.GetComponent<BoxCollider>();
        if (box == null)
        {
            Printer.LogError("{0}:按钮状态设置失败！缺少BoxCollider组件", go.name);
            return;
        }
        WidgetGray widgetGray = go.GetComponent<WidgetGray>();
        if (widgetGray == null) widgetGray = go.AddComponent<WidgetGray>();
        widgetGray.Gray = !active;
        box.enabled = active;
    }
}
