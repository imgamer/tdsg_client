﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 界面的基础类
/// </summary>
public abstract class BasePage : MonoBehaviour 
{
    protected bool inited = false;
    protected bool active = false;
    public abstract bool Active { get; }
    /// <summary>
    /// 打开动画播放结束回调
    /// </summary>
    protected Action OnPlayCallBack;
    /// <summary>
    /// 初始化
    /// </summary>
    protected abstract void OnInit();
    /// <summary>
    /// 打开
    /// </summary>
    /// <param name="args"></param>
    protected abstract void OnOpen(params object[] args);
    /// <summary>
    /// 刷新
    /// </summary>
    protected abstract void OnRefresh();
    /// <summary>
    /// 关闭
    /// </summary>
    protected abstract void OnClose();
    /// <summary>
    /// 注销
    /// </summary>
    protected abstract void OnUnInit();
    /// <summary>
    /// 动画
    /// </summary>
    public abstract void PlayOpenAnim(Action callBack);
    /// <summary>
    /// 延时
    /// </summary>
    /// <param name="time"></param>
    /// <param name="cb"></param>
    /// <returns></returns>
    protected IEnumerator DoDelay(float time, Action cb)
    {
        yield return new WaitForSeconds(time);
        if (cb != null) cb();
    }

    public void Refresh()
    {
        if (!inited)
        {
            Printer.LogError(string.Format("{0}: No initialization, can't call the Refresh!", gameObject.name));
            return;
        }
        transform.localScale = Vector3.one;
        if (!gameObject.activeSelf) gameObject.SetActive(true);
        active = true;
        OnRefresh();
    }

    public void Close()
    {
        if (active)
        {
            OnClose();
            active = false;
        }
        if (gameObject.activeSelf) gameObject.SetActive(false);
        transform.localScale = Vector3.one;
    }

    public void UnInit()
    {
        Close();
        if (inited)
        {
            OnUnInit();
            inited = false;
        }
    }

    void OnDestroy()
    {
        UnInit();
    }
}
