﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 跨图集设置UISprite名称
/// </summary>
[RequireComponent(typeof(UIWin))]
public class DynamicAtlas : MonoBehaviour 
{
    public UIAtlas[] m_atlas;
    public void SetDynamicSpriteName(UISprite sprite, string spriteName)
    {
        if (sprite == null) return;
        if (sprite.atlas == null || sprite.atlas.GetSprite(spriteName) == null)
        {
            UIAtlas atlas = null;
            if (m_atlas != null)
            {
                foreach (var item in m_atlas)
                {
                    if (item.GetSprite(spriteName) != null)
                    {
                        atlas = item;
                        break;
                    }
                }
            }
            if (atlas)
            {
                sprite.atlas = atlas;
                sprite.spriteName = spriteName;
            }
            else
            {
                Printer.LogError(string.Format("{0}：对应的精灵名：{1}找不到图集", sprite.name, spriteName));
            }
        }
        else
        {
            sprite.spriteName = spriteName;
        }
    }

}
