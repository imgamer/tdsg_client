﻿using UnityEngine;
using System.Collections;

namespace UI
{
    /// <summary>
    /// NGUI Widget置灰组件
    /// Sprite（Unlit - Transparent Colored，Unlit - Transparent Colored 1，Unlit - Transparent Colored 2， Unlit - Transparent Colored 3等4个shader进行修改）
    /// Label(Unlit - Text, Unlit - Text 1, Unlit - Text 2, Unlit - Text 3等4个shader进行修改）
    /// 插件shader代码片段
    ///		fixed gray = 0.22 * col.r + 0.70 * col.g + 0.08 * col.b;
    ///		col = lerp(col, fixed4(gray, gray, gray, col.a), IN.color.r == 0);
    /// 禁止使用UIButton和UIButtonColor等外部改变颜色的组件
    /// </summary>
    [AddComponentMenu("NGUI/Custom/WidgetGray")]
    public class WidgetGray : MonoBehaviour
    {
        //是否是置灰状态
        private bool gray = false;
        public bool Gray
        {
            get { return gray; }
            set
            {
                if (gray == value) return;
                gray = value;
                SetColor(gray);
            }
        }

        void Awake()
        {
            Init();
        }

        private UIWidget[] widgets;
        private Color[] widgetColors;
        private bool inited = false;
        private void Init()
        {
            if (inited) return;
            inited = true;
            widgets = transform.GetComponentsInChildren<UIWidget>(true);
            widgetColors = new Color[widgets.Length];
            for (int i = 0; i < widgets.Length; i++)
            {
                UIWidget widget = widgets[i];
                widgetColors[i] = widget.color;
            }
        }
        private void SetColor(bool gray)
        {
            if (widgets == null) return;
            for (int i = 0; i < widgets.Length; i++)
            {
                UIWidget widget = widgets[i];
                Color color = widgetColors[i];
                //shader在处理r = 0时，启动置灰机制
                if (gray) color.r = 0;
                widget.color = color;
            }
        }
    }
}

