﻿using UnityEngine;
using System.Collections;

namespace UI
{
    /// <summary>
    /// 不规则Item的排序组件（Item的Pivot为左上角）
    /// </summary>
    [AddComponentMenu("NGUI/Custom/UITable")]
    public class Table : UITable
    {

        public void GoToStart()
        {
            try
            {
                UIScrollView uiScrollView = NGUITools.FindInParents<UIScrollView>(gameObject);
                if (uiScrollView != null)
                {
                    uiScrollView.SetDragAmount(0, 0, false);
                }
            }
            catch (System.Exception ex)
            {
                Printer.LogException(ex);
            }
        }
    }
}

