﻿using UnityEngine;
using System.Collections;
using System;

namespace UI
{
    /// <summary>
    /// 标签页切换组件
    /// </summary>
    [RequireComponent(typeof(BasePage))]
    [AddComponentMenu("NGUI/Custom/UITab")]
    public class Tab : MonoBehaviour
    {
        public string normalSprite = "Smooth";
        public string pressedSprite = "Glow";

        [System.Serializable]
        public struct TabPage
        {
            public BoxCollider tab;
            public UISprite icon;
            public GameObject page;
        }
        public TabPage[] tabAndPage;

        public int CurTabIndex
        {
            private set;
            get;
        }
        public GameObject CurPage
        {
            get
            {
                return tabAndPage[CurTabIndex].page;
            }
        }
        public Action<GameObject, int> OnSelect;

        private bool inited = false;
        public void Init()
        {
            if (inited) return;

            if (tabAndPage == null || tabAndPage.Length <= 0)
            {
                Printer.LogError("tabAndPage is Null!");
                return;
            }
            for (int i = 0; i < tabAndPage.Length; i++)
            {
                TabPage tabPage = tabAndPage[i];
                InputManager.instance.AddClickListener(tabPage.tab.gameObject, (go) =>
                {
                    SwitchTo(tabPage);
                });
                Select(tabAndPage[i], i == 0);
            }
            CurTabIndex = 0;

            inited = true;
        }

        public void SwitchTo(int index)
        {
            if (index < 0 || index >= tabAndPage.Length) return;
            TabPage tabPage = tabAndPage[index];

            if (CurTabIndex == index)
            {
                if (OnSelect != null) OnSelect(tabPage.page, CurTabIndex);
                return;
            }

            for (int i = 0; i < tabAndPage.Length; i++)
            {
                TabPage item = tabAndPage[i];
                if (tabPage.Equals(item))
                {
                    CurTabIndex = i;
                    Select(item, true);
                }
                else
                {
                    Select(item, false);
                }
            }
        }

        private void SwitchTo(TabPage tabPage)
        {
            if (tabPage.Equals(tabAndPage[CurTabIndex])) return;
            for (int i = 0; i < tabAndPage.Length; i++)
            {
                TabPage item = tabAndPage[i];
                WinPage winContent = item.page.GetComponent<WinPage>();
                if (tabPage.Equals(item))
                {
                    CurTabIndex = i;
                    Select(item, true);
                    if (winContent != null) winContent.PlayOpenAnim(null);
                }
                else
                {
                    Select(item, false);
                    if (winContent != null) winContent.Close();
                }
            }
        }
        private void Select(TabPage tabPage, bool selected)
        {
            if (selected)
            {
                tabPage.icon.spriteName = pressedSprite;
                if (OnSelect != null) OnSelect(tabPage.page, CurTabIndex);
                tabPage.page.SetActive(true);
            }
            else
            {
                tabPage.icon.spriteName = normalSprite;
                tabPage.page.SetActive(false);
            }
        }

    }
}

