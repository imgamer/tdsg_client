﻿using UnityEngine;
using System.Collections;

namespace UI
{
    /// <summary>
    /// 排序组件
    /// </summary>
    [AddComponentMenu("NGUI/Custom/UIGrid")]
    public class Grid : UIGrid
    {
        /// <summary>
        /// 滚动到具体的Item
        /// </summary>
        /// <param name="index"></param> 从0开始计算
        /// <param name="totalNum"></param> 当前激活状态的数量
        public void GoToItem(int index, int totalNum)
        {
            try
            {
                UIScrollView uiScrollView = NGUITools.FindInParents<UIScrollView>(gameObject);
                if (uiScrollView != null)
                {
                    int lineNum = (maxPerLine == 0) ? 1 : maxPerLine;
                    totalNum = Mathf.CeilToInt((1.0f * totalNum) / lineNum);
                    index = (index < 0) ? 0 : index;
                    int n = index / lineNum;

                    Vector4 cr = uiScrollView.panel.finalClipRegion;
                    float count = 0;
                    if (uiScrollView.movement == UIScrollView.Movement.Horizontal)
                    {
                        count = cr.z / cellWidth;
                        float x = n / (totalNum - count);
                        x = Mathf.Clamp01(x);
                        uiScrollView.SetDragAmount(x, 0, false);
                    }
                    else if (uiScrollView.movement == UIScrollView.Movement.Vertical)
                    {
                        count = cr.w / cellHeight;
                        float y = n / (totalNum - count);
                        y = Mathf.Clamp01(y);
                        uiScrollView.SetDragAmount(0, y, false);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Printer.LogException(ex);
            }
        }
    }
}

