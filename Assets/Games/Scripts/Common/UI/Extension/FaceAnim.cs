﻿using UnityEngine;
using System.Collections;

namespace UI
{
    /// <summary>
    /// 聊天动态表情
    /// </summary>
    public class FaceAnim : MonoBehaviour
    {
        private int index = 0;
        private float delta = 0f;
        private float rate = 0;
        public int fps = 30;
        public bool loop = true;
        public bool snap = true;
        public string[] spriteNames;

        private UISprite sprite;
        void Awake()
        {
            sprite = transform.GetComponent<UISprite>();
            rate = 1f / fps;
        }

        public void Reset()
        {
            delta = 0;
            index = 0;
        }

        public void OnUpdate()
        {
            if (spriteNames == null || spriteNames.Length <= 0) return;
            delta += RealTime.deltaTime;
            if (rate < delta)
            {
                delta = (rate > 0f) ? delta - rate : 0f;

                if (++index >= spriteNames.Length)
                {
                    index = 0;
                }

                if (loop)
                {
                    sprite.spriteName = spriteNames[index];
                    if (snap) sprite.MakePixelPerfect();
                }
            }
        }
    }
}

