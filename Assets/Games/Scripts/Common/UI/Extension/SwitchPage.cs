﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace UI
{
    ///左右滑动页结构
    /// + ScrollView (UIPanel, UIScrollView)
    /// |- Pages (IUIPage, UICenterOnChild)
    /// |-- Page (IUIDragScrollView)
    /// |--- Item 1
    /// |--- Item 2
    /// |--- Item 3
    [RequireComponent(typeof(UICenterOnChild))]
    [AddComponentMenu("NGUI/Custom/IUIPage")]
    public class SwitchPage : UIWrapContent
    {
        protected override void Start()
        {
            //覆盖基类
        }

        private const int Num = 3;
        private int offset;
        private List<Transform> pages = new List<Transform>(Num);
        private UIScrollView scrollView;
        private bool inited = false;
        public virtual void Init()
        {
            if (inited) return;

            scrollView = transform.parent.GetComponent<UIScrollView>();
            onInitializeItem = UpdatePage;
            itemCountPerPage = transform.GetChild(0).childCount;
            offset = (Num - 1) / 2;

            UITools.CopyFixNumChild(transform, Num);
            for (int i = 0; i < Num; i++)
            {
                Transform child = transform.GetChild(i);
                child.name = i.ToString();
                pages.Add(child);
            }
            base.Start();

            inited = true;
        }

        private IList datas;
        private int dataCount = 0;
        public virtual void SetData(IList data)
        {
            if (data == null)
            {
                gameObject.SetActive(false);
                return;
            }
            datas = data;
            dataCount = datas.Count;
            PageCount = Mathf.CeilToInt((float)dataCount / itemCountPerPage);
            scrollView.enabled = PageCount > 1;

            GoToPage(curPageIndex);
        }

        //每一页的Item数量
        private int itemCountPerPage;
        //当前的页码
        public int curPageIndex { get; private set; }
        //下一页面的页码
        public int nextPageIndex { get; private set; }
        //滑动数据总数
        public int PageCount { get; private set; }

        public virtual void GoToPage(int index)
        {
            if (dataCount <= 0) return;
            if (scrollView.movement == UIScrollView.Movement.Horizontal)
            {
                pages.Sort(delegate(Transform a, Transform b)
                {
                    return a.localPosition.x.CompareTo(b.localPosition.x);
                });
            }
            else if (scrollView.movement == UIScrollView.Movement.Vertical)
            {
                pages.Sort(delegate(Transform a, Transform b)
                {
                    return b.localPosition.x.CompareTo(a.localPosition.x);
                });
            }
            curPageIndex = GetIndex(index);
            for (int i = 0; i < pages.Count; i++)
            {
                int pageIndex = index - 1 + i;
                RefreshPage(pages[i], GetIndex(pageIndex));
            }
        }

        private int GetIndex(int i)
        {
            if (PageCount <= 0) return 0;
            int index;
            if (i >= 0)
            {
                index = i % PageCount;
            }
            else
            {
                index = i % PageCount;
                index = PageCount + index;
            }
            return index;
        }

        private int LoopClamp(int realIndex)
        {
            int pageIndex = 0;
            if (realIndex < 0)
            {
                pageIndex = (-realIndex + offset) % PageCount;
            }
            else
            {
                pageIndex = (realIndex - offset) % PageCount;
                pageIndex = (PageCount - pageIndex) % PageCount;
            }
            return pageIndex;
        }

        private int lastRealIndex = 0;
        private void SetPageIndex(int realIndex)
        {
            if (lastRealIndex == realIndex) return;
            if (scrollView.movement == UIScrollView.Movement.Horizontal)
            {
                int temp = -realIndex + 1;
                if (realIndex < lastRealIndex)
                {
                    //左滑
                    curPageIndex = LoopClamp(temp - 1);
                }
                else
                {
                    //右滑
                    curPageIndex = LoopClamp(temp + 1);
                }
                nextPageIndex = LoopClamp(temp);
            }
            else if (scrollView.movement == UIScrollView.Movement.Vertical)
            {
                if (realIndex < lastRealIndex)
                {
                    //上滑
                    curPageIndex = LoopClamp(realIndex + 1);
                }
                else
                {
                    //下滑
                    curPageIndex = LoopClamp(realIndex - 1);
                }
                nextPageIndex = LoopClamp(realIndex);
            }
            lastRealIndex = realIndex;
        }

        public Action<Transform, object> OnItemLoad;
        private void UpdatePage(GameObject go, int wrapIndex, int realIndex)
        {
            if (dataCount <= 0) return;
            SetPageIndex(realIndex);
            RefreshPage(go.transform, nextPageIndex);
        }

        private void RefreshPage(Transform page, int pageIndex)
        {
            int beginIndex = pageIndex * itemCountPerPage;
            for (int i = 0; i < itemCountPerPage; ++i)
            {
                Transform item = page.GetChild(i);
                if (item == null)
                {
                    Printer.LogError(string.Format("page:{0} can not get child : {1}", page.name, i));
                    continue;
                }
                int index = beginIndex + i;
                if (index < dataCount)
                {
                    item.gameObject.SetActive(true);
                    item.name = index.ToString();
                    if (OnItemLoad != null) OnItemLoad(item, datas[index]);
                }
                else
                {
                    item.gameObject.SetActive(false);
                    item.name = string.Format("empty:{0}", index);
                }
            }
            UIGrid uiGrid = page.GetComponent<UIGrid>();
            if (uiGrid) uiGrid.Reposition();
        }
    }
}

