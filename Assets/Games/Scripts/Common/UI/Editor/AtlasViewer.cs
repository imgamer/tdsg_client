﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace UI
{
    /// <summary>
    /// UI图集监视器
    /// </summary>
    public class AtlasViewer : EditorWindow
    {
        Vector2 mScroll = Vector2.zero;
        UIRoot uiRoot = null;

        UIAtlas mLastAtlas;

        [MenuItem("UITools/UI 图集监视器", false)]
        static public void OpenTool()
        {
            DeleteSettings();
            EditorWindow.GetWindow<AtlasViewer>(false, "UI图集监视器", true).Show();
        }

        void OnSelectionChange() { Repaint(); }

        void OnGUI()
        {
            if (uiRoot == null)
            {
                uiRoot = Transform.FindObjectOfType<UIRoot>();
                if (uiRoot)
                {
                    Selection.activeGameObject = uiRoot.gameObject;
                }
                else
                {
                    EditorGUILayout.HelpBox("当前场景没有 UIRoot", MessageType.Info);
                    return;
                }
            }
            
            Dictionary<Material, List<UIWidget>> dic = new Dictionary<Material, List<UIWidget>>();
            UIWidget[] widgets = uiRoot.GetComponentsInChildren<UIWidget>(true);
            if (widgets == null || widgets.Length < 1)
            {
                EditorGUILayout.HelpBox("当前场景没有 UI图集", MessageType.Info);
                return;
            }
            List<string> enumList = new List<string>();
            enumList.Add("All");
            List<Material> mList = new List<Material>();
            
            foreach (var w in widgets)
            {
                if (w.material == null) continue;
                List<UIWidget> list = null;
                if (!dic.TryGetValue(w.material, out list))
                {
                    enumList.Add(w.material.name);
                    mList.Add(w.material);
                    list = new List<UIWidget>();
                }
                list.Add(w);
                dic[w.material] = list;
            }

            GUILayout.Space(6f);

            Material key = null;
            if (enumList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                int selectType = EditorGUILayout.Popup("Materials", GetType(), enumList.ToArray());
                if (GetType() != selectType)
                {
                    SetType(selectType);
                }
                key = (selectType == 0 || selectType - 1 >= mList.Count)? null : mList[selectType - 1];
                NGUIEditorTools.DrawPadding();
                GUILayout.EndHorizontal();

                
                if (key != null)
                {
                    GUILayout.BeginHorizontal();
                    UIAtlas atlas = null;
                    List<UIWidget> ws = dic[key];
                    foreach (var item in ws)
                    {
                        atlas = FindAtlas(item);
                        if (atlas) break;
                    }
                    EditorGUILayout.ObjectField("Atlas", atlas, typeof(UIAtlas), false);
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    if (mLastAtlas != NGUISettings.atlas)
                        mLastAtlas = NGUISettings.atlas;
                    ComponentSelector.Draw<UIAtlas>("替换成", NGUISettings.atlas, OnSelectAtlas, true);
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("替换"))
                    {
                        if (mLastAtlas == null)
                        {
                            this.ShowNotification(new GUIContent("替换图集为空"));
                            return;
                        }
                        if (mLastAtlas == atlas)
                        {
                            this.ShowNotification(new GUIContent("相同图集不能替换"));
                            return;
                        }
                        foreach (var item in ws)
                        {
                            UISprite uiSprite = item.GetComponent<UISprite>();
                            if (uiSprite)
                            {
                                if (uiSprite.atlas == null) continue;
                                uiSprite.atlas = mLastAtlas;
                                EditorUtility.SetDirty(uiSprite);
                            }
                            else
                            {
                                UILabel uiLabel = item.GetComponent<UILabel>();
                                if (uiLabel != null)
                                {
                                    this.ShowNotification(new GUIContent(string.Format("不允许修改{0}:字体图集", uiLabel.name)));
                                    continue;
                                }
                            }
                        }
                    }
                    GUILayout.EndHorizontal();
                }
            }

            GUILayout.Space(6f);
            GUI.color = Color.white;
            NGUIEditorTools.SetLabelWidth(200);
            mScroll = GUILayout.BeginScrollView(mScroll);

            if (key == null)
            {
                foreach (var item in dic)
                {
                    ShowList(item.Key, item.Value);
                }
            }
            else
            {
                ShowList(key, dic[key]);
            }
            GUILayout.EndScrollView();
        }

        private void OnSelectAtlas(Object obj)
        {
            if (NGUISettings.atlas != obj)
            {
                NGUISettings.atlas = obj as UIAtlas;
                Repaint();
            }
        }

        private void ShowList(Material mat, List<UIWidget> widgets)
        {
            GUILayout.Space(6f);
            NGUIEditorTools.BeginContents();
            EditorGUILayout.ObjectField("Material", mat, typeof(Material), false);
            foreach (var w in widgets)
            {
                GUI.color = w.isActiveAndEnabled ? Color.white : Color.gray;
                UIPanel panel = w.panel;
                if (panel == null) panel = NGUITools.FindInParents<UIPanel>(w.gameObject);
                EditorGUILayout.ObjectField(string.Format("[{0}]-{1}", panel.name, w.name), w, typeof(UIWidget), false);
            }
            NGUIEditorTools.EndContents();
        }

        private UIAtlas FindAtlas(UIWidget widget)
        {
            if (widget == null) return null;
            UISprite uiSprite = widget.GetComponent<UISprite>();
            if (uiSprite) return uiSprite.atlas;

            UILabel uiLabel = widget.GetComponent<UILabel>();
            if (uiLabel && uiLabel.bitmapFont)
            {
                return uiLabel.bitmapFont.atlas;
            }

            return null;
        }

        private int GetType()
        {
            return EditorPrefs.GetInt("Materials", 0);
        }
        private int SetType(int value)
        {
            EditorPrefs.SetInt("Materials", value);
            return value;
        }
        private static void DeleteSettings()
        {
            EditorPrefs.DeleteKey("Materials");
        }
    }
}
