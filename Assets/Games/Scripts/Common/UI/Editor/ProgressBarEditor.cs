﻿using UnityEngine;
using UnityEditor;
namespace UI
{
    [CanEditMultipleObjects]
#if UNITY_3_5
[CustomEditor(typeof(UIProgressBar))]
#else
    [CustomEditor(typeof(UI.ProgressBar), true)]
#endif
    public class ProgressBarEditor : UIWidgetContainerEditor
    {
        public override void OnInspectorGUI()
        {
            NGUIEditorTools.SetLabelWidth(80f);

            serializedObject.Update();

            GUILayout.Space(3f);

            DrawLegacyFields();

            GUILayout.BeginHorizontal();
            SerializedProperty sp = NGUIEditorTools.DrawProperty("Steps", serializedObject, "numberOfSteps", GUILayout.Width(110f));
            if (sp.intValue == 0) GUILayout.Label("= unlimited");
            GUILayout.EndHorizontal();

            OnDrawExtraFields();

            if (NGUIEditorTools.DrawHeader("Appearance", "Appearance", false, true))
            {
                NGUIEditorTools.BeginContents(true);
                NGUIEditorTools.DrawProperty("Foreground", serializedObject, "mFG");
                NGUIEditorTools.DrawProperty("Background", serializedObject, "mBG");
                NGUIEditorTools.DrawProperty("Thumb", serializedObject, "thumb");

                GUILayout.BeginHorizontal();
                NGUIEditorTools.DrawProperty("Direction", serializedObject, "mFill");
                NGUIEditorTools.DrawPadding();
                GUILayout.EndHorizontal();

                OnDrawAppearance();
                NGUIEditorTools.EndContents();
            }

            UI.ProgressBar sb = target as UI.ProgressBar;
            NGUIEditorTools.DrawEvents("On Value Change", sb, sb.onChange);
            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void DrawLegacyFields()
        {
            UI.ProgressBar sb = target as UI.ProgressBar;
            float val = EditorGUILayout.Slider("Value", sb.value, 0f, 1f);
            float alpha = EditorGUILayout.Slider("Alpha", sb.alpha, 0f, 1f);

            if (sb.value != val ||
                sb.alpha != alpha)
            {
                NGUIEditorTools.RegisterUndo("Progress Bar Change", sb);
                sb.value = val;
                sb.alpha = alpha;
                NGUITools.SetDirty(sb);

                for (int i = 0; i < UIScrollView.list.size; ++i)
                {
                    UIScrollView sv = UIScrollView.list[i];

                    if (sv.horizontalScrollBar == sb || sv.verticalScrollBar == sb)
                    {
                        NGUIEditorTools.RegisterUndo("Progress Bar Change", sv);
                        sv.UpdatePosition();
                    }
                }
            }
        }

        protected virtual void OnDrawExtraFields() { }
        protected virtual void OnDrawAppearance() { }
    }
}
