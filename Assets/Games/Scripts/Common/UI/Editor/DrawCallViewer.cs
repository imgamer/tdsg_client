﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace UI
{
    /// <summary>
    /// UI Draw Calls 监视器
    /// </summary>
    public class DrawCallViewer : EditorWindow
    {
        enum Visibility
        {
            Visible,
            Hidden,
        }

        enum ShowFilter
        {
            AllPanels,
            SelectedUI,
        }

        Vector2 mScroll = Vector2.zero;

        private int warningNum = 5;
        private int warningCount = 2;
        void OnSelectionChange() { Repaint(); }

        [MenuItem("UITools/UIDrawCall 监视器", false)]
        static public void OpenTool()
        {
            EditorWindow.GetWindow<DrawCallViewer>(false, "DrawCall监视器", true).Show();
        }

        private bool IsHighlight(UIPanel[] panels, UIPanel panel)
        {
            if (panels == null || panels.Length < 1) return false;
            foreach (var item in panels)
            {
                if (item == panel) return true;
            }
            return false;
        }

        void OnGUI()
        {
            BetterList<UIDrawCall> activeDcs = UIDrawCall.activeList;

            activeDcs.Sort(delegate(UIDrawCall a, UIDrawCall b)
            {
                return a.finalRenderQueue.CompareTo(b.finalRenderQueue);
            });

            if (activeDcs.size == 0)
            {
                EditorGUILayout.HelpBox("当前场景UI没有 DrawCall", MessageType.Info);
                return;
            }

            UIPanel[] selectedPanels = null;
            if (Selection.activeGameObject)
            {
                UIWin uiWin = NGUITools.FindInParents<UIWin>(Selection.activeGameObject);
                if (uiWin)
                {
                    selectedPanels = uiWin.GetComponentsInChildren<UIPanel>();
                }
                else
                {
                    selectedPanels = Selection.activeGameObject.GetComponentsInChildren<UIPanel>();
                }
            }

            GUILayout.Space(12f);

            NGUIEditorTools.SetLabelWidth(240);
            ShowFilter show = (NGUISettings.showAllDCs ? ShowFilter.AllPanels : ShowFilter.SelectedUI);

            warningNum = EditorGUILayout.IntField("每一个界面的DrawCall数量最大值 Max:", warningNum);
            warningCount = EditorGUILayout.IntField("每一个DrawCall的组件数量最小值 Min:", warningCount);

            if ((ShowFilter)EditorGUILayout.EnumPopup("选择监视目标", show) != show)
                NGUISettings.showAllDCs = !NGUISettings.showAllDCs;

            GUILayout.Space(6f);

            if (show == ShowFilter.AllPanels)
                EditorGUILayout.LabelField("当前场景UI的DrawCall:", activeDcs.size.ToString());

            if (GUILayout.Button("重新监视"))
            {
                NGUISettings.showAllDCs = show == ShowFilter.AllPanels;
            }
            GUILayout.Space(6f);

            if (!NGUISettings.showAllDCs)
            {
                if (selectedPanels == null || selectedPanels.Length < 1)
                {
                    EditorGUILayout.HelpBox("未选择含有UIPanel的对象", MessageType.Info);
                    return;
                }
            }

            NGUIEditorTools.SetLabelWidth(80f);
            mScroll = GUILayout.BeginScrollView(mScroll);

            int dcCount = 0;
            Dictionary<UIWin, int> dic = new Dictionary<UIWin, int>();
            for (int i = 0; i < activeDcs.size; ++i)
            {
                UIDrawCall dc = activeDcs[i];
                string key = "Draw Call " + (i + 1);
                bool highlight = IsHighlight(selectedPanels, dc.manager);

                if (!highlight)
                {
                    //非全选的状态下，过滤掉非选中对象
                    if (!NGUISettings.showAllDCs) continue;

                    if (UnityEditor.EditorPrefs.GetBool(key, true))
                    {
                        GUI.color = Color.gray;
                    }
                    else
                    {
                        GUI.contentColor = Color.gray;
                    }
                }
                else GUI.contentColor = Color.white;

                ++dcCount;
                string name = key + " / " + activeDcs.size;
                if (!dc.isActive) name = name + " (HIDDEN)";

                bool warning = false;
                UIWin ui = NGUITools.FindInParents<UIWin>(dc.manager.gameObject);
                if (ui != null)
                {
                    if (dic.ContainsKey(ui))
                    {
                        dic[ui]++;
                    }
                    else
                    {
                        dic[ui] = 1;
                    }
                    if (dic[ui] > warningNum) warning = true;
                    name = string.Format("{0}  [{1}]-{2}", name, ui.gameObject.name, dc.manager.name);
                }  
                else
                {
                    name = string.Format("{0}  [{1}]", name, dc.manager.name);
                }

                if (NGUIEditorTools.DrawHeader(name, key))
                {
                    if (warning)
                    {
                        GUI.color = Color.yellow;
                    }
                    else
                    {
                        GUI.color = highlight ? Color.white : new Color(0.8f, 0.8f, 0.8f);
                    }

                    NGUIEditorTools.BeginContents();
                    EditorGUILayout.ObjectField("Material", dc.dynamicMaterial, typeof(Material), false);

                    int count = 0;

                    for (int a = 0; a < UIPanel.list.Count; ++a)
                    {
                        UIPanel p = UIPanel.list[a];

                        for (int b = 0; b < p.widgets.Count; ++b)
                        {
                            UIWidget w = p.widgets[b];
                            if (w.drawCall == dc) ++count;
                        }
                    }

                    string myPath = NGUITools.GetHierarchy(dc.manager.cachedGameObject);
                    string remove = myPath + "\\";
                    string[] list = new string[count + 1];
                    list[0] = count.ToString();
                    count = 0;

                    for (int a = 0; a < UIPanel.list.Count; ++a)
                    {
                        UIPanel p = UIPanel.list[a];

                        for (int b = 0; b < p.widgets.Count; ++b)
                        {
                            UIWidget w = p.widgets[b];

                            if (w.drawCall == dc)
                            {
                                string path = NGUITools.GetHierarchy(w.cachedGameObject);
                                list[++count] = count + ". " + (string.Equals(path, myPath) ? w.name : path.Replace(remove, ""));
                            }
                        }
                    }

                    GUILayout.BeginHorizontal();
                    GUI.color = list.Length - 1 < warningCount ? Color.yellow : Color.white;
                    int sel = EditorGUILayout.Popup("Widgets", 0, list);
                    NGUIEditorTools.DrawPadding();
                    GUILayout.EndHorizontal();

                    if (sel != 0)
                    {
                        count = 0;

                        for (int a = 0; a < UIPanel.list.Count; ++a)
                        {
                            UIPanel p = UIPanel.list[a];

                            for (int b = 0; b < p.widgets.Count; ++b)
                            {
                                UIWidget w = p.widgets[b];

                                if (w.drawCall == dc && ++count == sel)
                                {
                                    Selection.activeGameObject = w.gameObject;
                                    break;
                                }
                            }
                        }
                    }

                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Render Q", dc.finalRenderQueue.ToString(), GUILayout.Width(120f));
                    bool draw = (Visibility)EditorGUILayout.EnumPopup(dc.isActive ? Visibility.Visible : Visibility.Hidden) == Visibility.Visible;
                    NGUIEditorTools.DrawPadding();
                    GUILayout.EndHorizontal();

                    if (dc.isActive != draw)
                    {
                        dc.isActive = draw;
                        NGUITools.SetDirty(dc.manager);
                    }

                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Triangles", dc.triangles.ToString(), GUILayout.Width(120f));

                    UIPanel selectedPanel = null;
                    if (Selection.activeGameObject) selectedPanel = Selection.activeGameObject.GetComponent<UIPanel>();
                    if (selectedPanel != dc.manager)
                    {
                        if (GUILayout.Button("选中Panel"))
                        {
                            Selection.activeGameObject = dc.manager.gameObject;
                        }
                        NGUIEditorTools.DrawPadding();
                    }
                    GUILayout.EndHorizontal();

                    if (dc.manager.clipping != UIDrawCall.Clipping.None && !dc.isClipped)
                    {
                        EditorGUILayout.HelpBox("You must switch this material's shader to Unlit/Transparent Colored or Unlit/Premultiplied Colored in order for clipping to work.",
                            MessageType.Warning);
                    }

                    NGUIEditorTools.EndContents();
                    GUI.color = Color.white;
                }
            }

            if (dcCount == 0)
            {
                EditorGUILayout.HelpBox("当前场景没有 DrawCall", MessageType.Info);
            }
            GUILayout.EndScrollView();
        }
    }
}

