﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace UI
{
    /// <summary>
    /// 创建UI模板
    /// </summary>
    public class PrefabCreater : EditorWindow
    {
        [MenuItem("UITools/UI 模板创建", false)]
        static public void OpenTool()
        {
            EditorWindow.GetWindow<PrefabCreater>(false, "创建UI模板", true).Show();
        }

        private string uiName;
        void OnGUI()
        {
            if (Application.isPlaying)
            {
                EditorGUILayout.HelpBox("运行状态不能创建UI", MessageType.Info);
                return;
            }

            uiName = EditorGUILayout.TextField("输入名称:", uiName);
            if (GUILayout.Button("确定创建"))
            {
                if (string.IsNullOrEmpty(uiName))
                {
                    this.ShowNotification(new GUIContent("UI名字非法"));
                    return;
                }
                CreateUI();
                this.Close();
            }
            if (GUILayout.Button("放弃创建"))
            {
                this.Close();
            }
        }
        void OnHierarchyChange()
        {
            this.RemoveNotification();
        }
        private readonly Dictionary<string, UIAnchor.Side> dic = new Dictionary<string, UIAnchor.Side> 
        {
            {"Top", UIAnchor.Side.Top},
            {"Bottom", UIAnchor.Side.Bottom},
            {"Left", UIAnchor.Side.Left},
            {"Right", UIAnchor.Side.Right},
            {"Center", UIAnchor.Side.Center},
            {"TopLeft", UIAnchor.Side.TopLeft},
            {"TopRight", UIAnchor.Side.TopRight},
            {"BottomLeft", UIAnchor.Side.BottomLeft},
            {"BottomRight", UIAnchor.Side.BottomRight}
        };
        private void CreateUI()
        {
            UIManager uiMgr = Transform.FindObjectOfType<UIManager>();
            if (uiMgr)
            {
                GameObject go = new GameObject(uiName);
                go.layer = uiMgr.gameObject.layer;
                UIPanel panel = go.AddComponent<UIPanel>();
                panel.depth = 0;
                Rigidbody rb = go.AddComponent<Rigidbody>();
                rb.useGravity = false;
                rb.isKinematic = true;
                BoxCollider box = go.AddComponent<BoxCollider>();
                box.isTrigger = true;
                box.center = new Vector3(0, 0, 0);
                box.size = new Vector3(3000, 3000, 0);

                go.transform.parent = uiMgr.transform;
                go.transform.localPosition = Vector3.zero;
                go.transform.localEulerAngles = Vector3.zero;
                go.transform.localScale = Vector3.one;

                GameObject root = new GameObject("Root");
                root.layer = uiMgr.gameObject.layer;
                root.transform.parent = go.transform;
                root.transform.localPosition = Vector3.zero;
                root.transform.localEulerAngles = Vector3.zero;
                root.transform.localScale = Vector3.one;


                foreach (var item in dic)
                {
                    GameObject o = new GameObject(item.Key);
                    o.layer = uiMgr.gameObject.layer;
                    o.transform.parent = root.transform;
                    o.transform.localPosition = new Vector3(0, 0, -5);
                    o.transform.localEulerAngles = Vector3.zero;
                    o.transform.localScale = Vector3.one;

                    UIAnchor Ac = o.AddComponent<UIAnchor>();
                    Ac.side = item.Value;
                    Ac.runOnlyOnce = false;
                }
            }
        }
    }
}

