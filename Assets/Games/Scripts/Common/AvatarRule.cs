﻿using System;
using System.Collections.Generic;

public static class AvatarRule
{
    static AvatarRule()
	{
	}

    /// <summary>
    /// 计算升级所需经验值
    /// </summary>
    /// <returns>The level need exp.</returns>
    /// <param name="start">Start.</param>
    /// <param name="end">End.</param>
    public static Int32 UpgradeLevelNeedExp(UInt16 start, UInt16 end)
    {
        Int32 result = 0;
        for (UInt16 i = start; i < end; i++)
        {
            result += AvatarLevelUpResumeConfig.SharedInstance.GetAvatarLevelUpResumeData(i).EXP;
        }
        return result;
    }

    /// <summary>
    /// 分析出吸收指定经验值后，从指定等级升到指定上限等级后剩余的经验值以及实际可以提升的等级
    /// </summary>
    /// <param name="startLevel">Start level.</param>
    /// <param name="upperLevel">Upper level.</param>
    /// <param name="value">Value.</param>
    /// <param name="level">Level.</param>
    /// <param name="remain">Remain.</param>
    public static void AbsorbExpAnalysis(UInt16 startLevel, UInt16 upperLevel, Int32 value, out UInt16 level, out Int32 remain)
    {
        level = startLevel;
        remain = value;

        while (level < upperLevel)
        {
            Int32 need = UpgradeLevelNeedExp(level, (UInt16)(level + 1));
            if (remain < need)
                break;

            remain -= need;
            level += 1;
        }
    }

    /// <summary>
    /// 计算生命上限
    /// </summary>
    /// <param name="school"></param>
    /// <param name="vitality"></param>
    /// <returns></returns>
    public static Int32 CalcHPMax(UInt16 school, UInt32 vitality)
    {
        return (Int32)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).HP_Max + vitality * ConfigConst.AVATAR_VITALITY_TRANS_HP_RATIO);
    }

    /// <summary>
    /// 计算物理攻击
    /// </summary>
    /// <param name="school"></param>
    /// <param name="force"></param>
    /// <returns></returns>
    public static Int32 CalcPhysicsPower(UInt16 school, UInt32 force)
    {
        return (Int32)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).PhysicsPower + force * ConfigConst.AVATAR_FORCE_TRANS_PHYSICS_POWER_RATIO);
    }

    /// <summary>
    /// 计算物理防御
    /// </summary>
    /// <param name="school"></param>
    /// <param name="gengu"></param>
    /// <returns></returns>
    public static Int32 CalcPhysicsDefense(UInt16 school, UInt32 force, UInt32 gengu)
    {
        return (Int32)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).PhysicsDefense +
            force * ConfigConst.AVATAR_FORCE_TRANS_PHYSICS_DEFENSE_RATIO + 
            gengu * ConfigConst.AVATAR_GENGU_TRANS_PHYSICS_DEFENSE_RATIO);
    }

    /// <summary>
    /// 计算暴击率
    /// </summary>
    /// <param name="school"></param>
    /// <param name="crit"></param>
    /// <param name="toughness"></param>
    /// <returns></returns>
    public static float CalcCritRate(UInt16 school, Int32 crit, Int32 toughness)
    {
        return (float)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).CritRate +
            (crit - toughness) / ((crit - toughness) * ConfigConst.FIGHT_SETTLEMENT_CONST_CRIT_PARAM1 + ConfigConst.FIGHT_SETTLEMENT_CONST_CRIT_PARAM2));
    }

    /// <summary>
    /// 计算命中率
    /// </summary>
    /// <param name="school"></param>
    /// <param name="hit"></param>
    /// <param name="dodge"></param>
    /// <returns></returns>
    public static float CalcHitRate(UInt16 school, Int32 hit, Int32 dodge)
    {
        return (float)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).HitRate +
            (hit - dodge) / ((hit - dodge) * ConfigConst.FIGHT_SETTLEMENT_CONST_HIT_PARAM1 + ConfigConst.FIGHT_SETTLEMENT_CONST_HIT_PARAM2));
    }

    /// <summary>
    /// 法术攻击力
    /// </summary>
    /// <param name="school"></param>
    /// <param name="spirituality"></param>
    /// <returns></returns>
    public static Int32 CalcMagicPower(UInt16 school, UInt32 spirituality)
    {
        return (Int32)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).MagicPower + spirituality * ConfigConst.AVATAR_SPIRITUALITY_TRANS_MAGIC_POWER_RATIO);
    }

    /// <summary>
    /// 计算法术防御力
    /// </summary>
    /// <param name="school"></param>
    /// <param name="vitality"></param>
    /// <param name="spirituality"></param>
    /// <param name="force"></param>
    /// <param name="gengu"></param>
    /// <returns></returns>
    public static Int32 CalcMagicDefense(UInt16 school, UInt32 vitality, UInt32 spirituality, UInt32 gengu)
    {
        return (Int32)(AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).MagicDefense + 
            vitality * ConfigConst.AVATAR_VITALITY_TRANS_MAGIC_DEFENSE_RATIO + 
            spirituality * ConfigConst.AVATAR_SPIRITUALITY_TRANS_MAGIC_DEFENSE_RATIO + 
            gengu * ConfigConst.AVATAR_GENGU_TRANS_MAGIC_DEFENSE_RATIO);
    }

    /// <summary>
    /// 计算攻击速度
    /// </summary>
    /// <param name="school"></param>
    /// <returns></returns>
    public static Int32 CalcAttackSpeed(UInt16 school)
    {
        return (Int32)AvatarInitialConfig.SharedInstance.GetAvatarInitialData(school).AttackSpeed;
    }

    /// <summary>
    /// 计算法术命中率
    /// </summary>
    /// <param name="hit"></param>
    /// <param name="dodge"></param>
    /// <returns></returns>
    public static float CalcMagicHit(Int32 hit, Int32 dodge)
    {
        return 1.0f;
    }

    /// <summary>
    /// 计算物理命中率
    /// </summary>
    /// <param name="school"></param>
    /// <param name="hit"></param>
    /// <param name="dodge"></param>
    /// <returns></returns>
    public static float CalcPhysicsHit(Int32 hit, Int32 dodge)
    {
        return 1.0f;
    }

    private const int FreePointStep = 4;
    //获取等级改变时，自由点的变化
    public static int GetFreePointGrowth(int startLevel, int targetLevel)
    {
        return (targetLevel - startLevel) * FreePointStep;
    }

    //获取宠物最大可获得的自由点数
    public static int GetMaxFreePoint(int level)
    {
        return (level - 1) * FreePointStep;
    }
}
