﻿using UnityEngine;
using System;
using KBEngine;
/// <summary>
/// 客户端日志输出类
/// </summary>
public static class Printer 
{
	private static bool isDebugBuild = Debug.isDebugBuild;
    public static DEBUGLEVEL debugLevel = DEBUGLEVEL.ERROR;

    public static void Log(string message, params object[] args)
    {
#if UNITY_EDITOR
        Debug.Log(string.Format(message, args));
#else
        if (isDebugBuild || DEBUGLEVEL.DEBUG >= debugLevel)
            Debug.Log(string.Format(message, args));
#endif
    }

    public static void LogWarning(string message, params object[] args)
    {
#if UNITY_EDITOR
        Debug.LogWarning(string.Format(message, args));
#else
        if (isDebugBuild || DEBUGLEVEL.WARNING >= debugLevel)
            Debug.LogWarning(string.Format(message, args));
#endif
    }

    public static void LogError(string message, params object[] args)
    {
#if UNITY_EDITOR
        Debug.LogError(string.Format(message, args));
#else
        if (isDebugBuild || DEBUGLEVEL.ERROR >= debugLevel)
            Debug.LogError(string.Format(message, args));
#endif
    }

    public static void LogException(Exception ex)
    {
#if UNITY_EDITOR
        Debug.LogException(ex);
#else
        if (isDebugBuild || DEBUGLEVEL.ERROR >= debugLevel)
            Debug.LogException(ex);
#endif
    }

}
