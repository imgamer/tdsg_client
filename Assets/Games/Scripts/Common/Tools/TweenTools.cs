﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

/// <summary>
/// 对DoTween插件的封装
/// </summary>
public static class TweenTools
{
    /// <summary>
    /// 销毁Tweener
    /// </summary>
    /// <param name="tw"></param>
    public static void DOKill(ref Tweener tw)
    {
        if (tw != null)
        {
            tw.Kill();
            tw = null;
        }
    }
    /// <summary>
    /// 世界坐标移动
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="position"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _worldMoves = new Dictionary<int, Tweener>();
    public static Tweener DOWorldMove(Transform transform, Vector3 endPosition, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (transform == null) return null;
        int id = transform.GetInstanceID();
        Tweener tweener;
        if (_worldMoves.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _worldMoves.Remove(id);
        }
        tweener = transform.DOMove(endPosition, time).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() => 
        {
            _worldMoves.Remove(id);
            if (onComplete != null) onComplete(); 
        });
        _worldMoves.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 本地坐标移动
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="localposition"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _localMoves = new Dictionary<int, Tweener>();
    public static Tweener DOLocalMove(Transform transform, Vector3 endLocalposition, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (transform == null) return null;
        int id = transform.GetInstanceID();
        Tweener tweener;
        if (_localMoves.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _localMoves.Remove(id);
        }
        tweener = transform.DOLocalMove(endLocalposition, time).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _localMoves.Remove(id);
            if (onComplete != null) onComplete();
        });
        _localMoves.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 世界坐标旋转
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="rotation"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _worldRotates = new Dictionary<int, Tweener>();
    public static Tweener DOWorldRotate(Transform transform, Vector3 endRotation, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (transform == null) return null;
        int id = transform.GetInstanceID();
        Tweener tweener;
        if (_worldRotates.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _worldRotates.Remove(id);
        }
        tweener = transform.DORotate(endRotation, time).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _worldRotates.Remove(id);
            if (onComplete != null) onComplete();
        });
        _worldRotates.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 本地坐标旋转
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="rotation"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _localRotates = new Dictionary<int, Tweener>();
    public static Tweener DOLocalRotate(Transform transform, Vector3 endLocalRotation, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (transform == null) return null;
        int id = transform.GetInstanceID();
        Tweener tweener;
        if (_localRotates.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _localRotates.Remove(id);
        }
        tweener = transform.DOLocalRotate(endLocalRotation, time).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _localRotates.Remove(id);
            if (onComplete != null) onComplete();
        });
        _localRotates.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 相机位置抖动
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="strength"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _shakePositions = new Dictionary<int, Tweener>();
    public static Tweener DOShakePosition(Camera camera, Vector3 strength, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (camera == null) return null;
        int id = camera.GetInstanceID();
        Tweener tweener;
        if (_shakePositions.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _shakePositions.Remove(id);
        }
        tweener = camera.DOShakePosition(time, strength).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _shakePositions.Remove(id);
            if (onComplete != null) onComplete();
        });
        _shakePositions.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 相机旋转抖动
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="strength"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _shakeRotates = new Dictionary<int, Tweener>();
    public static Tweener DOShakeRotation(Camera camera, Vector3 strength, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (camera == null) return null;
        int id = camera.GetInstanceID();
        Tweener tweener;
        if (_shakeRotates.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _shakeRotates.Remove(id);
        }
        tweener = camera.DOShakeRotation(time, strength).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _shakeRotates.Remove(id);
            if (onComplete != null) onComplete();
        });
        _shakeRotates.Add(id, tweener);
        return tweener;
    }
    /// <summary>
    /// 材质渐变
    /// </summary>
    /// <param name="material"></param>
    /// <param name="endValue"></param>
    /// <param name="time"></param>
    /// <param name="easeType"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    private static Dictionary<int, Tweener> _materials = new Dictionary<int, Tweener>();
    public static Tweener DOFade(Material material, float endValue, float time, TweenCallback onUpdate, TweenCallback onComplete)
    {
        if (material == null) return null;
        int id = material.GetInstanceID();
        Tweener tweener;
        if (_materials.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _materials.Remove(id);
        }
        tweener = material.DOFade(endValue, time).SetEase(Ease.Linear).OnUpdate(onUpdate).OnComplete(() =>
        {
            _materials.Remove(id);
            if (onComplete != null) onComplete();
        });
        _materials.Add(id, tweener);
        return tweener;
    }

	/// <summary>
	/// 路径寻路
	/// </summary>
	/// <param name="tf"></param>
	/// <param name="path"></param>
	/// <param name="duration"></param>
	/// <param name="onWayPointChanged"></param>
	/// <param name="onComplete"></param>
	/// <param name="onUpdate"></param>
	/// <returns></returns>
    //此处DoPath有Bug，onWayPointChanged回调的Index有时并非从0开始。
    private static Dictionary<int, Tweener> _paths = new Dictionary<int, Tweener>();
    public static Tweener DOPath(Transform transform, Vector3[] path, float duration, TweenCallback<int> onWayPointChanged, TweenCallback onUpdate, TweenCallback onComplete)
	{
        if (transform == null) return null;
        int id = transform.GetInstanceID();
        Tweener tweener;
        if (_paths.TryGetValue(id, out tweener))
        {
            if(tweener != null) tweener.Kill();
            _paths.Remove(id);
        }
        tweener = transform.DOPath(path, duration).SetEase(Ease.Linear).OnWaypointChange(onWayPointChanged).OnUpdate(onUpdate).OnComplete(() =>
        {
            _paths.Remove(id);
            if (onComplete != null) onComplete();
        });
        _paths.Add(id, tweener);
        return tweener;
	}
}
