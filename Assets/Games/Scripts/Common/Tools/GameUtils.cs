﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
/// <summary>
/// 工具类
/// </summary>
public class GameUtils
{
    #region Layer和Tag
    public static int LayerDefault { get; private set; }
    public static int LayerUI { get; private set; }
    public static int LayerFade { get; private set; }
    public static int LayerHUD { get; private set; }
    public static int LayerUIEffect { get; private set; }
    public static int LayerEntity { get; private set; }
    public static int LayerSceneEffect { get; private set; }
    public static int LayerHighlight { get; private set; }
    public static int LayerSkillEffect { get; private set; }
    public static void InitLayers()
    {
        LayerDefault = LayerMask.NameToLayer("Default");
        LayerUI = LayerMask.NameToLayer("UI");
        LayerFade = LayerMask.NameToLayer("Fade");
        LayerHUD = LayerMask.NameToLayer("HUD");
        LayerUIEffect = LayerMask.NameToLayer("UIEffect");
        LayerEntity = LayerMask.NameToLayer("Entity");
        LayerSceneEffect = LayerMask.NameToLayer("SceneEffect");
        LayerHighlight = LayerMask.NameToLayer("HighLight");
        LayerSkillEffect = LayerMask.NameToLayer("SkillEffect");
    }
    /// <summary>
    /// Tag常量
    /// </summary>
    public const string TagMainCamera = "MainCamera";
    public const string TagFadeCamera = "FadeCamera";
    public const string TagSingleton = "Singleton";
    public const string TagShadowArea = "ShadowArea";
    #endregion

    public static readonly Color32 GreenColor = new Color32 (1, 255, 0, 255);
    public static readonly Color32 RedColor = new Color32(255, 0, 0, 255);

    #region 获取材质
    public static Material GetMaterial(Renderer render)
    {
        if (render == null) return null;
#if UNITY_EDITOR
        return render.material;//每次更换属性的时候Unity就会自动new一份新的material，影响性能，可能会造成内存泄漏
#else  
        return render.sharedMaterial;//在svn管理中，Material会变成红叹号，表示文件已经被修改，所以Editor下不建议使用
#endif
    } 
    #endregion

	#region obj copy
	/// <summary>
	/// 深复制一个对象。这个方法目前只是对大部分类型生效，如果遇到没有实
	/// 现克隆接口，也不提供无参构造函数的引用类型，调用这个方法会出错。
	/// </summary>
	/// <returns>The copy.</returns>
	/// <param name="obj">Object.</param>
	public static object DeepCopy(object obj)
	{
		//KBEngine.Dbg.DEBUG_MSG("obj is " + obj.ToString());
		if(obj == null)
			return null;

		if(obj is ICloneable)
			return (obj as ICloneable).Clone();

		object targetDeepCopyObj;  
		Type targetType = obj.GetType();  
		//值类型  
		if (targetType.IsValueType)  
		{  
			targetDeepCopyObj = obj;  
		}  
		//引用类型   
		else  
		{  
			//创建引用对象，这里要求此类型带有无参构造函数
			targetDeepCopyObj = System.Activator.CreateInstance(targetType);
			foreach(System.Reflection.FieldInfo field in obj.GetType().GetFields(Define.PUBLIC_AND_NONPUBLIC))
			{
				field.SetValue(targetDeepCopyObj, DeepCopy(field.GetValue(obj)));
			}
		}
		return targetDeepCopyObj;
	}
	#endregion

    public static void ChangeLayer(Transform[] objs, LayerMask layer)
    {
        for (int i = 0; i < objs.Length; i++)
        {
            ChangeLayer(objs[i], layer);
        }
    }

    public static void ChangeLayer(Transform tf, LayerMask layer)
    {
        var childs = tf.GetComponentsInChildren<MeshRenderer>();
        for (int j = 0; j < childs.Length; j++)
        {
            childs[j].gameObject.layer = layer;
        }
    }

}

