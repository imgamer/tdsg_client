﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;

namespace Tools
{
    /// <summary>
    /// 组件删除工具
    /// </summary>
    public class RemoveComponent : EditorWindow
    {
        /// <summary>
        /// 临时存储int[]
        /// </summary>
        private int[] intArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

        private int typeInt = 0;
        private static readonly Dictionary<string, Type> TypeDict = new Dictionary<string, Type>() 
        {
            {"UIButton", typeof(UIButton)},
            {"UIButtonColor", typeof(UIButtonColor)},
            {"UIButtonOffset", typeof(UIButtonOffset)},
            {"UIButtonScale", typeof(UIButtonScale)},
        };
        private static readonly string[] TypeString = new List<string>(TypeDict.Keys).ToArray();

        [MenuItem("GameTools/组件删除", false)]
        static public void OpenTool()
        {
            EditorWindow.GetWindow<RemoveComponent>(false, "组件删除", true).Show();
        }

        void OnGUI()
        {
            if (Application.isPlaying)
            {
                EditorGUILayout.HelpBox("运行状态不能进行组件删除", MessageType.Info);
                return;
            }
            EditorGUILayout.HelpBox("请在资源文件夹选中需要修改的Prefab", MessageType.Info);

            typeInt = EditorGUILayout.IntPopup("Component", typeInt, TypeString, intArray);
            if (GUILayout.Button("一键删除"))
            {
                UnityEngine.Object[] objs = GetSelecteds(typeInt);
                if (objs == null || objs.Length <= 0) 
                {
                    EditorUtility.DisplayDialog("抱歉", "找不到要删除的组件", "OK");
                    return;
                }
                foreach (var item in objs)
                {
                    DestroyImmediate(item, true);
                }
                AssetDatabase.Refresh();
                AssetDatabase.SaveAssets();
                EditorUtility.DisplayDialog("恭喜", "恭喜完成一键删除", "OK");
            }
        }

        private UnityEngine.Object[] GetSelecteds(int index)
        {
            Type type;
            if (TypeDict.TryGetValue(TypeString[typeInt], out type))
            {
                return GetSelecteds(type);
            }
            return null;
        }
        private UnityEngine.Object[] GetSelecteds(Type type)
        {
            return Selection.GetFiltered(type, SelectionMode.Deep);
        }
    }
}

