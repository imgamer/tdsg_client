﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 异步加载对象类
/// </summary>
public class AsyncInstantiateSpawner : AsyncBaseSpawner
{
    protected override bool GetChache(BaseSpawnData data)
    {
        InstantiateData objectData = data as InstantiateData;
        GameObject go;
        if (GamePoolManager.instance.TrySpawn(objectData.assetName, objectData.objectName, objectData.poolName, objectData.poolType, out go))
        {
            Succeed = go;
            if (objectData.cb != null) objectData.cb(go);
            return true;
        }
        return false;
    }

    protected override void OnSpawn()
    {
        if (loading) return;
        if (loadQueue.Count <= 0)
        {
            Remove();
            return;
        }

        AssetsDetail assetsDetail = loadQueue.Dequeue();
        string path;
        switch (assetsDetail.type)
        {
            case AssetsType.ResourceAssets:
                path = assetsDetail.path;
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadResource(path, assetsDetail);
                break;
            case AssetsType.LocalAssets:
                path = AssetsUtils.GetUrlFromLocal(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            case AssetsType.ServerAssets:
                path = AssetsUtils.GetUrlFromServer(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            default:
                break;
        }
    }

    protected override void OnRemove()
    {
        if (string.IsNullOrEmpty(loadingAsset)) return;
        Printer.LogWarning(string.Format("未加载结束，被意外清除:{0}", loadingAsset));
        string[] dependentNames = loader.GetAllDependencies(loadingAsset);
        foreach (var dependentName in dependentNames)
        {
            loader.CutDependentBundleRef(dependentName);
        }
    }

    #region Resource加载
    private void LoadResource(string path, AssetsDetail assetsDetail)
    {
        try
        {
            BeginSpawn(assetsDetail);
            StartCoroutine(SpawnResource(path, () =>
            {
                NextSpawn();
            }));
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
            Succeed = false;
            NextSpawn();
        }
    }
    private IEnumerator SpawnResource(string path, Action cb)
    {
        ResourceRequest request = Resources.LoadAsync<UnityEngine.Object>(path);
        yield return request;

        UnityEngine.Object o = request.asset;
        List<BaseSpawnData> list;
        if (spawnDict.TryGetValue(loadingAsset, out list))
        {
            for (int i = 0; i < list.Count; i++)
            {
                InstantiateData objectData = list[i] as InstantiateData;
                if (GetChache(objectData)) continue;

                yield return null;
                yield return null;

                if (o)
                {
                    GamePoolManager.instance.Add(objectData.assetName, objectData.objectName, o, objectData.poolName, objectData.poolType);
                    GameObject go = GamePoolManager.instance.Spawn(objectData.assetName, objectData.objectName, objectData.poolName, objectData.poolType);
                    Succeed = go;
                    if (objectData.cb != null) objectData.cb(go);
                }
                else
                {
                    Succeed = false;
                    if (objectData.cb != null) objectData.cb(null);
                }
            }
            spawnDict.Remove(loadingAsset);
        }
        if (cb != null) cb();
    }
    #endregion

    #region Bundle加载
    private void LoadBundle(string path, AssetsDetail assetsDetail)
    {
        BeginSpawn(assetsDetail);
        AssetBundle bundle;
        //判断Bundle是否为依赖包
        if (loader.TryGetBundle(loadingAsset, out bundle))
        {
            if (bundle)
            {
                //已经加载的依赖包
                StartCoroutine(SpawnBundle(bundle, NextSpawn, false));
            }
            else
            {
                //首次加载的依赖包
                StartCoroutine(LoadBundle(path, (b) =>
                {
                    bundle = b;
                    if (bundle)
                    {
                        bundle.LoadAllAssets();
                        loader.SetDependentBundle(loadingAsset, bundle);
                    }
                    StartCoroutine(SpawnBundle(bundle, NextSpawn, false));
                }));
            }
        }
        else
        {
            //对象Bundle
            string[] dependentNames = loader.GetAllDependencies(loadingAsset);
            foreach (var dependentName in dependentNames)
            {
                loader.ADDDependentBundleRef(dependentName);
            }
            StartCoroutine(LoadBundle(path, (b) =>
            {
                bundle = b;
                StartCoroutine(SpawnBundle(bundle, () =>
                {
                    foreach (var dependentName in dependentNames)
                    {
                        loader.CutDependentBundleRef(dependentName);
                    }
                    NextSpawn();
                }, true));
            }));
        }
    }
    private IEnumerator LoadBundle(string path, Action<AssetBundle> cb)
    {
        WWW www = WWW.LoadFromCacheOrDownload(path, 0, loader.GetAssetBundleCRC(loadingAsset));
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Succeed = false;
            Printer.LogError(string.Format("Bundle is null! URL:{0}", www.url));
        }
        www.Dispose();
        if (cb != null) cb(bundle);
    }
    private IEnumerator SpawnBundle(AssetBundle bundle, Action cb, bool unLoad)
    {
        List<BaseSpawnData> list;
        if (spawnDict.TryGetValue(loadingAsset, out list))
        {
            if (bundle)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    InstantiateData objectData = list[i] as InstantiateData;
                    if (GetChache(objectData)) continue;
                    yield return null;

                    UnityEngine.Object o = bundle.LoadAsset<UnityEngine.Object>(objectData.objectName);
                    if (o)
                    {
                        GamePoolManager.instance.Add(objectData.assetName, objectData.objectName, o, objectData.poolName, objectData.poolType);
                        GameObject go = GamePoolManager.instance.Spawn(objectData.assetName, objectData.objectName, objectData.poolName, objectData.poolType);
                        Succeed = go;
                        if (objectData.cb != null) objectData.cb(go);
                    }
                    else
                    {
                        Succeed = false;
                        if (objectData.cb != null) objectData.cb(null);
                    }
                }
                spawnDict.Remove(loadingAsset);
                if (unLoad) bundle.Unload(false);
            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    InstantiateData objectData = list[i] as InstantiateData;
                    if (GetChache(objectData)) continue;
                    if (objectData.cb != null) objectData.cb(null);
                }
                Succeed = false;
                spawnDict.Remove(loadingAsset);
            }
        }
        if (cb != null) cb();
    }
    #endregion

}
