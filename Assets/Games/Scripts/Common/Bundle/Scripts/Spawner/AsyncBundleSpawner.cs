﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 异步加载指定Bundle类
/// </summary>
public class AsyncBundleSpawner : AsyncBaseSpawner
{
    protected override bool GetChache(BaseSpawnData data)
    {
        return false;
    }

    protected override void OnSpawn()
    {
        if (loading) return;
        if (loadQueue.Count <= 0)
        {
            Remove();
            return;
        }

        AssetsDetail assetsDetail = loadQueue.Dequeue();
        string path;
        switch (assetsDetail.type)
        {
            case AssetsType.ResourceAssets:
                NextSpawn();
                break;
            case AssetsType.LocalAssets:
                path = AssetsUtils.GetUrlFromLocal(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            case AssetsType.ServerAssets:
                path = AssetsUtils.GetUrlFromServer(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            default:
                break;
        }
    }

    protected override void OnRemove()
    {
        if (string.IsNullOrEmpty(loadingAsset)) return;
        string[] dependentNames = loader.GetAllDependencies(loadingAsset);
        foreach (var dependentName in dependentNames)
        {
            loader.CutDependentBundleRef(dependentName);
        }
    }

    #region Bundle加载
    private void LoadBundle(string path, AssetsDetail assetsDetail)
    {
        BeginSpawn(assetsDetail);
        AssetBundle bundle;
        //判断Bundle是否为依赖包
        if (loader.TryGetBundle(loadingAsset, out bundle))
        {
            if (bundle)
            {
                //已经加载的依赖包
                SpawnBundle(bundle);
                NextSpawn();
            }
            else
            {
                //首次加载的依赖包
                StartCoroutine(LoadBundle(path, (b) =>
                {
                    bundle = b;
                    if (bundle)
                    {
                        bundle.LoadAllAssets();
                        loader.SetDependentBundle(loadingAsset, bundle);
                    }
                    SpawnBundle(bundle);
                    NextSpawn();
                }));
            }
        }
        else
        {
            string[] dependentNames = loader.GetAllDependencies(loadingAsset);
            foreach (var dependentName in dependentNames)
            {
                loader.ADDDependentBundleRef(dependentName);
            }
            StartCoroutine(LoadBundle(path, (b) =>
            {
                bundle = b;
                foreach (var dependentName in dependentNames)
                {
                    loader.CutDependentBundleRef(dependentName);
                }
                SpawnBundle(bundle);
                NextSpawn();
            }));
        }
    }

    private IEnumerator LoadBundle(string path, Action<AssetBundle> cb)
    {
        WWW www = WWW.LoadFromCacheOrDownload(path, 0, loader.GetAssetBundleCRC(loadingAsset));
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Succeed = false;
            Printer.LogError(string.Format("Bundle is null! URL:{0}", www.url));
        }
        www.Dispose();
        if (cb != null) cb(bundle);
    }
    private void SpawnBundle(AssetBundle bundle)
    {
        List<BaseSpawnData> list;
        if (spawnDict.TryGetValue(loadingAsset, out list))
        {
            for (int i = 0; i < list.Count; i++)
            {
                BundleData bundleData = list[i] as BundleData;
                if (GetChache(bundleData)) continue;
                if (bundleData.cb != null) bundleData.cb(bundle);
            }
            Succeed = bundle;
            spawnDict.Remove(loadingAsset);
        }
    }
    #endregion
}
