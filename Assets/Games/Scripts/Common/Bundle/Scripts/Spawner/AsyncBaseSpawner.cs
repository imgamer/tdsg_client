﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 异步加载资源基类
/// </summary>
public abstract class AsyncBaseSpawner : MonoBehaviour 
{
    #region 参数
    protected float timeout;
    private bool succeed = true;
    protected bool Succeed 
    {
        set 
        {
            if (value) return;
            Printer.LogError("{0} 资源加载出现失败情况！", loadingAsset);
            succeed = false;
        }
        get
        {
            return succeed;
        }
    }
    protected Action<bool> OnDone;
    protected BaseModeLoader loader;
    protected Queue<AssetsDetail> loadQueue = new Queue<AssetsDetail>();
    protected Dictionary<string, List<BaseSpawnData>> spawnDict = new Dictionary<string, List<BaseSpawnData>>();
    #endregion

    #region 初始化
    public void Init(BaseModeLoader loader, IEnumerable<BaseSpawnData> spawnDatas, PriorityMode mode, Action<bool> OnDone, float timeout)
    {
        this.OnDone = OnDone;
        this.loader = loader;
        this.timeout = timeout;
        if (spawnDatas == null)
        {
            Succeed = false;
            Remove();
            return;
        }
        switch (mode)
        {
            case PriorityMode.First:
                loader.AddLoadingFirst(this);
                break;
            case PriorityMode.Last:
                loader.AddLoadingLast(this);
                break;
            default:
                loader.AddLoadingLast(this);
                break;
        }
        OnInit(spawnDatas);
    }
    protected virtual void OnInit(IEnumerable<BaseSpawnData> spawnDatas)
    {
        foreach (var data in spawnDatas)
        {
            if (data == null)
            {
                Succeed = false;
                continue;
            }
            if (GetChache(data)) continue;
            string assetName = data.assetName;
            AssetsDetail assetsDetail = loader.GetAssetsDetail(assetName);
            if (string.IsNullOrEmpty(assetsDetail.path))
            {
                Printer.LogError("{0} 不存在资源列表中", assetName);
                Succeed = false;
                continue;
            }

            switch (assetsDetail.type)
            {
                case AssetsType.ResourceAssets:

                    break;
                case AssetsType.LocalAssets:
                case AssetsType.ServerAssets:
                    SetDependentBundle(assetName);
                    break;
                default:
                    break;
            }
            List<BaseSpawnData> list;
            if (!spawnDict.TryGetValue(assetName, out list))
            {
                list = new List<BaseSpawnData>();
                loadQueue.Enqueue(assetsDetail);
            }
            list.Add(data);
            spawnDict[assetName] = list;
        }
    }
    private void SetDependentBundle(string assetName)
    {
        string[] dependentNames = loader.GetAllDependencies(assetName);
        for (int i = 0; i < dependentNames.Length; i++)
        {
            var dependentName = dependentNames[i];
            loader.TryNewDependentBundle(dependentName);
            if (!spawnDict.ContainsKey(dependentName))
            {
                spawnDict[dependentName] = new List<BaseSpawnData>();
                AssetsDetail dependentDetail = loader.GetAssetsDetail(dependentName);
                loadQueue.Enqueue(dependentDetail);
            }
        }
    }
    #endregion

    #region 缓存读取
    protected abstract bool GetChache(BaseSpawnData spawnData);
    #endregion

    #region 设置在切换场景时是否自动撤销
    public void SetUndoOnLoad(bool undoOnLoad)
    {
        if (undoOnLoad) return;
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    #region 执行加载
    protected bool loading = false;
    protected string loadingAsset = string.Empty;
    protected void BeginSpawn(AssetsDetail assetsDetail)
    {
        loading = true;
        loadingAsset = assetsDetail.name;
    }
    protected void EndSpawn()
    {
        loading = false;
        loadingAsset = string.Empty;
    }
    protected void NextSpawn()
    {
        EndSpawn();
        OnSpawn();
    }
    public void Spawn()
    {
        OnSpawn();
    }
    protected abstract void OnSpawn();
    #endregion

    #region 主动移除
    private bool initiativeRemove = false;
    public void Remove()
    {
        initiativeRemove = true;
        loader.RemoveLoadingList(this);
        OnRemove();
        Destroy(gameObject);
        if (OnDone != null) OnDone(Succeed);
    }
    protected abstract void OnRemove();
    #endregion

    #region 意外移除
    void OnDestroy()
    {
        if (initiativeRemove) return;
        loader.RemoveLoadingList(this);
        OnRemove();
    }
    
    #endregion

}
