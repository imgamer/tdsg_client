﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 异步更新下载类
/// </summary>
public class AsyncDownloadSpawner : AsyncBaseSpawner
{
    protected override void OnInit(IEnumerable<BaseSpawnData> spawnDatas)
    {
        foreach (var data in spawnDatas)
        {
            if (data == null)
            {
                Succeed = false;
                continue;
            }
            string assetName = data.assetName;
            AssetsDetail assetsDetail = loader.GetAssetsDetail(assetName);
            if (string.IsNullOrEmpty(assetsDetail.path))
            {
                Succeed = false;
                continue;
            }

            List<BaseSpawnData> list;
            if (!spawnDict.TryGetValue(assetName, out list))
            {
                list = new List<BaseSpawnData>();
                loadQueue.Enqueue(assetsDetail);
            }
            list.Add(data);
            spawnDict[assetName] = list;
        }
    }

    protected override bool GetChache(BaseSpawnData data)
    {
        return false;
    }

    protected override void OnSpawn()
    {
        if (loading) return;
        if (loadQueue.Count <= 0)
        {
            Remove();
            return;
        }

        AssetsDetail assetsDetail = loadQueue.Dequeue();
        string path;
        switch (assetsDetail.type)
        {
            case AssetsType.ResourceAssets:
                Printer.LogError("不合法资源:{0} 更新缓存", assetsDetail.name);
                NextSpawn();
                break;
            case AssetsType.LocalAssets:
                Printer.LogError("不合法资源:{0} 更新缓存", assetsDetail.name);
                NextSpawn();
                break;
            case AssetsType.ServerAssets:
                path = AssetsUtils.GetUrlFromServer(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            default:
                break;
        }
    }

    protected override void OnRemove()
    {
        if (string.IsNullOrEmpty(loadingAsset)) return;
        Printer.LogWarning(string.Format("未加载结束，被意外清除:{0}", loadingAsset));
    }

    #region Bundle加载
    private void LoadBundle(string path, AssetsDetail assetsDetail)
    {
        BeginSpawn(assetsDetail);
        StartCoroutine(LoadBundle(path, (bundle) =>
        {
            if (bundle)
            {
                bundle.Unload(true);
            }
            NextSpawn();
        }));
    }
    private IEnumerator LoadBundle(string path, Action<AssetBundle> cb)
    {
        WWW www = WWW.LoadFromCacheOrDownload(path, 0, loader.GetAssetBundleCRC(loadingAsset));
        StartLoad(www);
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Succeed = false;
            Printer.LogError(string.Format("Bundle is null! URL:{0}", www.url));
        }
        EndLoad(www);
        if (cb != null) cb(bundle);
    }
    #endregion

    #region 超时检测
    private WWW www;
    private float startTime = -1;
    private float lastProgress = 0;
    protected void StartLoad(WWW www)
    {
        startTime = Time.realtimeSinceStartup;
        lastProgress = 0;
        this.www = www;
    }
    protected void EndLoad(WWW www)
    {
        startTime = -1;
        lastProgress = 0;
        this.www.Dispose();
    }

    void Update()
    {
        if (startTime == -1) return;
        if (lastProgress != www.progress)
        {
            startTime = Time.realtimeSinceStartup;
            lastProgress = www.progress;
        }
        else if (timeout > 0 && Time.realtimeSinceStartup - startTime > timeout)
        {
            Printer.LogError(string.Format("超时:{0}", www.url));
            Succeed = false;
            EndLoad(www);
            StopAllCoroutines();
            NextSpawn();
        }
    }
    #endregion

}
