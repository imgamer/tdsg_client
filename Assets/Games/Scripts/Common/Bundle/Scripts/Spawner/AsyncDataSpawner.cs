﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 异步加载非Prefab资源类
/// </summary>
public class AsyncDataSpawner : AsyncBaseSpawner
{
    protected override bool GetChache(BaseSpawnData data)
    {
        return false;
    }

    protected override void OnSpawn()
    {
        if (loading) return;
        if (loadQueue.Count <= 0)
        {
            Remove();
            return;
        }

        AssetsDetail assetsDetail = loadQueue.Dequeue();
        string path;
        switch (assetsDetail.type)
        {
            case AssetsType.ResourceAssets:
                path = assetsDetail.path;
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadResource(path, assetsDetail);
                break;
            case AssetsType.LocalAssets:
                path = AssetsUtils.GetUrlFromLocal(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            case AssetsType.ServerAssets:
                path = AssetsUtils.GetUrlFromServer(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            default:
                break;
        }
    }

    protected override void OnRemove()
    {
        if (string.IsNullOrEmpty(loadingAsset)) return;
        string[] dependentNames = loader.GetAllDependencies(loadingAsset);
        foreach (var dependentName in dependentNames)
        {
            loader.CutDependentBundleRef(dependentName);
        }
    }

    #region Resource加载
    private void LoadResource(string path, AssetsDetail assetsDetail)
    {
        try
        {
            BeginSpawn(assetsDetail);
            StartCoroutine(SpawnResource(path, () =>
            {
                NextSpawn();
            }));
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
            Succeed = false;
            NextSpawn();
        }
    }
    private IEnumerator SpawnResource(string path, Action cb)
    {
        ResourceRequest request = Resources.LoadAsync<UnityEngine.Object>(path);
        yield return request;

        UnityEngine.Object o = request.asset;
        List<BaseSpawnData> list;
        if (spawnDict.TryGetValue(loadingAsset, out list))
        {
            for (int i = 0; i < list.Count; i++)
            {
                AssetData assetData = list[i] as AssetData;
                if (GetChache(assetData)) continue;
                if (assetData.cb != null) assetData.cb(o);
            }
            spawnDict.Remove(loadingAsset);
        }
        Succeed = o;
        if (cb != null) cb();
    }
    #endregion

    #region Bundle加载
    private void LoadBundle(string path, AssetsDetail assetsDetail)
    {
        BeginSpawn(assetsDetail);
        AssetBundle bundle;
        //判断Bundle是否为依赖包
        if (loader.TryGetBundle(loadingAsset, out bundle))
        {
            if (bundle)
            {
                //已经加载的依赖包
                SpawnBundle(bundle, false);
                NextSpawn();
            }
            else
            {
                //首次加载的依赖包
                StartCoroutine(LoadBundle(path, (b) =>
                {
                    bundle = b;
                    if (bundle)
                    {
                        bundle.LoadAllAssets();
                        loader.SetDependentBundle(loadingAsset, bundle);
                    }
                    SpawnBundle(bundle, false);
                    NextSpawn();
                }));
            }
        }
        else
        {
            string[] dependentNames = loader.GetAllDependencies(loadingAsset);
            foreach (var dependentName in dependentNames)
            {
                loader.ADDDependentBundleRef(dependentName);
            }
            StartCoroutine(LoadBundle(path, (b) =>
            {
                bundle = b;
                foreach (var dependentName in dependentNames)
                {
                    loader.CutDependentBundleRef(dependentName);
                }
                SpawnBundle(bundle, true);
                NextSpawn();
            }));
        }
    }

    private IEnumerator LoadBundle(string path, Action<AssetBundle> cb)
    {
        WWW www = WWW.LoadFromCacheOrDownload(path, 0, loader.GetAssetBundleCRC(loadingAsset));
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Succeed = false;
            Printer.LogError("Bundle is null! URL:{0}", www.url);
        }
        www.Dispose();
        if (cb != null) cb(bundle);
    }
    private void SpawnBundle(AssetBundle bundle, bool unLoad)
    {
        List<BaseSpawnData> list;
        if (spawnDict.TryGetValue(loadingAsset, out list))
        {
            if (bundle)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    AssetData assetData = list[i] as AssetData;
                    if (GetChache(assetData)) continue;
                    UnityEngine.Object o = bundle.LoadAsset(assetData.objectName);
                    Succeed = o;
                    if (assetData.cb != null) assetData.cb(o);
                }
                spawnDict.Remove(loadingAsset);
                if (unLoad) bundle.Unload(false);
            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    AssetData assetData = list[i] as AssetData;
                    if (GetChache(assetData)) continue;
                    if (assetData.cb != null) assetData.cb(null);
                }
                Succeed = false;
                spawnDict.Remove(loadingAsset);
            }
        }     
    }
    #endregion
}