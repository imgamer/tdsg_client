﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 异步加载场景类
/// </summary>
public class AsyncSceneSpawner : AsyncBaseSpawner
{
    protected override bool GetChache(BaseSpawnData data)
    {
        return false;
    }

    protected override void OnSpawn()
    {
        if (loading) return;
        if (loadQueue.Count <= 0)
        {
            Remove();
            return;
        }

        AssetsDetail assetsDetail = loadQueue.Dequeue();
        string path;
        switch (assetsDetail.type)
        {
            case AssetsType.ResourceAssets:
                path = assetsDetail.name;
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadResource(path, assetsDetail);
                break;
            case AssetsType.LocalAssets:
                path = AssetsUtils.GetUrlFromLocal(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            case AssetsType.ServerAssets:
                path = AssetsUtils.GetUrlFromServer(assetsDetail.path);
                if (string.IsNullOrEmpty(path))
                {
                    Succeed = false;
                    NextSpawn();
                    return;
                }
                LoadBundle(path, assetsDetail);
                break;
            default:
                break;
        }
    }

    protected override void OnRemove()
    {
        if (string.IsNullOrEmpty(loadingAsset)) return;
        string[] dependentNames = loader.GetAllDependencies(loadingAsset);
        foreach (var dependentName in dependentNames)
        {
            loader.CutDependentBundleRef(dependentName);
        }
    }

    #region Resource加载
    private void LoadResource(string path, AssetsDetail assetsDetail)
    {
        try
        {
            BeginSpawn(assetsDetail);
            StartCoroutine(LoadLevel(path, (done) =>
            {
                NextSpawn();
            }));
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
            Succeed = false;
            NextSpawn();
        }
    }

    private IEnumerator LoadLevel(string levelName, Action<bool> cb)
    {
        AsyncOperation operation = Application.LoadLevelAsync(levelName);
        yield return operation;
        Succeed = operation.isDone;
        if (cb != null) cb(operation.isDone);
    }
    #endregion

    #region Bundle加载
    private void LoadBundle(string path, AssetsDetail assetsDetail)
    {
        BeginSpawn(assetsDetail);
        AssetBundle bundle;
        //判断Bundle是否为依赖包
        if (loader.TryGetBundle(loadingAsset, out bundle))
        {
            if(bundle)
            {
                //已经加载的依赖包
                NextSpawn();
            }
            else
            {
                //首次加载的依赖包
                StartCoroutine(LoadBundle(path, (b) =>
                {
                    bundle = b;
                    if (bundle)
                    {
                        bundle.LoadAllAssets();
                        loader.SetDependentBundle(loadingAsset, bundle);
                    }
                    NextSpawn();
                }));
            }
        }
        else
        {
            string[] dependentNames = loader.GetAllDependencies(loadingAsset);
            foreach (var dependentName in dependentNames)
            {
                loader.ADDDependentBundleRef(dependentName);
            }
            //场景Bundle
            StartCoroutine(LoadBundle(path, (b) =>
            {
                bundle = b;
                if (bundle)
                {
                    bundle.LoadAllAssets();
                    StartCoroutine(LoadLevel(loadingAsset, (done) =>
                    {
                        foreach (var dependentName in dependentNames)
                        {
                            loader.CutDependentBundleRef(dependentName);
                        }
                        bundle.Unload(false);
                        NextSpawn();
                    }));
                }
                else
                {
                    foreach (var dependentName in dependentNames)
                    {
                        loader.CutDependentBundleRef(dependentName);
                    }
                    NextSpawn();
                }
            }));
        }
    }

    private IEnumerator LoadBundle(string path, Action<AssetBundle> cb)
    {
        WWW www = WWW.LoadFromCacheOrDownload(path, 0, loader.GetAssetBundleCRC(loadingAsset));
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Succeed = false;
            Printer.LogError(string.Format("Bundle is null! URL:{0}", www.url));
        }
        www.Dispose();
        if (cb != null) cb(bundle);
    }
    #endregion
}
