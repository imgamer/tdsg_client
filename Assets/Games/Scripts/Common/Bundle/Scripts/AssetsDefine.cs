﻿using System;
using UnityEngine;
/// <summary>
/// 打包模式
/// </summary>
public enum BuildMode
{
    ResourceBuild,  //游戏资源生成路径：Resources文件
    LocalBuild,     //游戏资源生成路径：Resources，StreamingAssets文件
    ServerBuild,    //游戏资源生成路径：Resources，StreamingAssets, ServerAssets文件
}
/// <summary>
/// 加载模式
/// </summary>
public enum LoadMode
{
    LocalMode,    //本地模式
    ServerMode,   //服务器模式
}
/// <summary>
/// 资源构造类型
/// </summary>
public enum AssetsType
{
    ResourceAssets,  //Prefab文件，需要从Resources拉取
    LocalAssets,     //Bundle文件，需要从StreamingAssets拉取
    ServerAssets,    //Bundle文件，需要从ServerAssets拉取
}
/// <summary>
/// 异步加载优先模式
/// </summary>
public enum PriorityMode
{
    First,
    Last,
}

/// <summary>
/// 资源详情
/// </summary>
public struct AssetsDetail
{
    public string name;
    public AssetsType type;
    public string path;
    public uint crc;
    public int size;

    public AssetsDetail(string name, AssetsType type, string path, uint crc, int size)
    {
        this.name = name;
        this.type = type;
        this.path = path;
        this.crc = crc;
        this.size = size;
    }
}

/// <summary>
/// 依赖Bundle数据
/// </summary>
public struct DependentBundle
{
    public string assetName;
    public AssetBundle bundle;
    private int count;

    public void SetBundle(AssetBundle bundle)
    {
        this.bundle = bundle;
    }

    public void Init(string assetName)
    {
        this.assetName = assetName;
        this.bundle = null;
        this.count = 0;
    }

    public void ChangeCount(int change)
    {
        this.count += change;
        Printer.Log("{0}; {1}", this.assetName, this.count);
    }

    public bool Unreferenced()
    {
        return this.count <= 0;
    }

    public void TryUnload()
    {
        if (this.bundle)
        {
            this.bundle.Unload(false);
        }
    }
}

/// <summary>
/// 加载数据基类
/// </summary>
public abstract class BaseSpawnData
{
    public string assetName;
}

/// <summary>
/// 资源更新缓存或者下载数据
/// </summary>
public class CacheOrDownloadData : BaseSpawnData
{
    public CacheOrDownloadData(string assetName)
    {
        this.assetName = assetName;
    }
}

/// <summary>
/// 预加载对象数据
/// </summary>
public class PreloadData : BaseSpawnData
{
    public string objectName;
    public PoolName poolName;
    public PoolType poolType;

    public PreloadData(string assetName, string objectName, PoolName poolName, PoolType poolType)
    {
        this.assetName = assetName;
        this.objectName = objectName;
        this.poolName = poolName;
        this.poolType = poolType;
    }
}

/// <summary>
/// 实例化对象数据
/// </summary>
public class InstantiateData : BaseSpawnData
{
    public string objectName;
    public PoolName poolName;
    public PoolType poolType;
    public Action<GameObject> cb;

    public InstantiateData(string assetName, string objectName, PoolName poolName, PoolType poolType, Action<GameObject> cb)
    {
        this.assetName = assetName;
        this.objectName = objectName;
        this.poolName = poolName;
        this.poolType = poolType;
        this.cb = cb;
    }
}

/// <summary>
/// 加载场景数据
/// </summary>
public class LevelData : BaseSpawnData
{
    public string sceneName;
    public LevelData(string assetName, string sceneName)
    {
        this.assetName = assetName;
        this.sceneName = sceneName;
    }
}

/// <summary>
/// 加载指定Bundle数据
/// </summary>
public class BundleData : BaseSpawnData
{
    public Action<AssetBundle> cb;
    public BundleData(string assetName, Action<AssetBundle> cb)
    {
        this.assetName = assetName;
        this.cb = cb;
    }
}

/// <summary>
/// 加载指定非Prefab数据(例如音效，图片等等)
/// </summary>
public class AssetData : BaseSpawnData
{
    public string objectName;
    public Action<UnityEngine.Object> cb;
    public AssetData(string assetName, string objectName, Action<UnityEngine.Object> cb)
    {
        this.assetName = assetName;
        this.objectName = objectName;
        this.cb = cb;
    }
}

