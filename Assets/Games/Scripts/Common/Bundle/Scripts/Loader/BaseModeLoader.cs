﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 加载模式基类
/// </summary>
public abstract class BaseModeLoader : MonoBehaviour 
{
    protected Action<bool> OnReady;
    protected const string ManifestName = "BundleAssets";
    protected abstract string ManifestUrl { get; }
    protected const string DetailConfigName = "detail_config";
    protected abstract string DetailConfigUrl { get; }

    #region 初始化
    public void Init(Action<bool> cb, float timeout) 
    {
        OnReady = cb;
        try
        {
            InitManifest(ManifestUrl, (ok) => 
            {
                if(ok)
                {
                    InitAssetsDetailText(DetailConfigUrl, OnInit, timeout);
                }
                else
                {
                    if (OnReady != null) OnReady(false);
                }
            }, timeout);
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
            if (OnReady != null) OnReady(false);
        }
    }
    protected abstract void OnInit(bool ready);
    #endregion

    #region 初始化AssetBundleManifest
    protected AssetBundleManifest manifest = null;
    private void InitManifest(string url, Action<bool> cb, float timeout)
    {
        StartCoroutine(LoadBundle(url, (bundle) => 
        {
            if (bundle)
            {
                manifest = bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                if (cb != null) cb(manifest);
            }
            else
            {
                if (cb != null) cb(false);
            }
        }));
    }
    private IEnumerator LoadBundle(string url, Action<AssetBundle> cb)
    {
        WWW www = new WWW(url);
        yield return www;
        AssetBundle bundle = AssetsUtils.GetData<AssetBundle>(www);
        if (bundle == null)
        {
            Printer.LogError("Bundle is null! URL:{0}", www.url);
        }
        www.Dispose();
        if (cb != null) cb(bundle);
    }
    #endregion

    #region 初始化资源信息详情数据
    private const string DetailTextName = "detail_text";
    protected Dictionary<string, AssetsDetail> assetsDetailDict = new Dictionary<string, AssetsDetail>();
    private void InitAssetsDetailText(string url, Action<bool> cb, float timeout)
    {
        StartCoroutine(LoadBundle(url, (bundle) =>
        {
            if (bundle)
            {
                TextAsset table = bundle.LoadAsset<TextAsset>(DetailTextName);
                if (table != null)
                {
                    LoadAssetsDetailText(table.text);
                }
                bundle.Unload(true);
                if (cb != null) cb(assetsDetailDict.Count > 0);
            }
            else
            {
                if (cb != null) cb(false);
            }
        }));
    }
    private const char SplitChar = '\t';
    private void LoadAssetsDetailText(string content)
    {
        if (string.IsNullOrEmpty(content)) return;
        string[] lines = content.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var item in lines)
        {
            if (string.IsNullOrEmpty(item)) continue;
            string[] columns = item.Split(new char[] { SplitChar });
            if (columns.Length != 5) continue;
            AssetsType type = (AssetsType)Enum.Parse(typeof(AssetsType), columns[1]);
            assetsDetailDict[columns[0]] = new AssetsDetail(columns[0], type, columns[2], uint.Parse(columns[3]), int.Parse(columns[4]));
        }
    }
    #endregion

    #region 初始化配置数据
    private const string ConfigName = "configs";
    private AssetBundle configBundle;
    protected void InitConfig(Action<bool> cb)
    {
        AssetsDetail detail = GetAssetsDetail(ConfigName);
        switch (detail.type)
        {
            case AssetsType.ResourceAssets:
                if (cb != null) cb(true);
                break;
            case AssetsType.LocalAssets:
            case AssetsType.ServerAssets:
                BundleData data = new BundleData(ConfigName, (bundle) =>
                {
                    configBundle = bundle;
                    if (cb != null) cb(configBundle);
                });
                AsyncSpawnBundle(data, null);
                break;
        }
    }
    private void AsyncSpawnBundle(BundleData bundleData, Action<bool> cb)
    {
        BaseSpawnData[] data = new BaseSpawnData[] { bundleData };
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncBundleSpawner>();
        spawner.Init(this, data, PriorityMode.Last, cb, 0);
        spawner.SetUndoOnLoad(false);
    }
    #endregion

    #region 获取资源详情数据
    public AssetsDetail GetAssetsDetail(string name)
    {
        AssetsDetail detail;
        if (assetsDetailDict.TryGetValue(name, out detail))
        {
            return detail;
        }
        return default(AssetsDetail);
    }
    #endregion

    #region 获取Bundle资源依赖
    public string[] GetAllDependencies(string name)
    {
        string assetName = AssetsUtils.ToAssetName(name);
        string[] names = manifest.GetAllDependencies(assetName);
        int count = names.Length;
        if (count <= 0) return names;

        string[] ret = new string[count];
        for (int i = 0; i < count; i++)
        {
            ret[count - 1 - i] = AssetsUtils.RemoveBundleVariant(names[i]);
        }
        return ret;
    }
    #endregion

    #region 依赖Bundle管理
    private Dictionary<string, DependentBundle> dependentBundleDict = new Dictionary<string, DependentBundle>();
    //释放所有依赖包(存在可能误释放即将被使用的依赖包问题)
    public void UnloadDependentBundle()
    {
        List<DependentBundle> list = new List<DependentBundle>(dependentBundleDict.Values);
        List<string> removeList = new List<string>();
        foreach (var dependentBundle in list)
        {
            if(dependentBundle.Unreferenced())
            {
                removeList.Add(dependentBundle.assetName);
            }
        }
        foreach (var assetName in removeList)
        {
            DependentBundle dependentBundle;
            if(dependentBundleDict.TryGetValue(assetName, out dependentBundle))
            {
                dependentBundleDict.Remove(assetName);
                dependentBundle.TryUnload();
            }
        }
    }
    public void SetDependentBundle(string assetName, AssetBundle bundle)
    {
        DependentBundle dependentBundle;
        if (dependentBundleDict.TryGetValue(assetName, out dependentBundle))
        {
            dependentBundle.SetBundle(bundle);
            dependentBundleDict[assetName] = dependentBundle;
        }
        else
        {
            Printer.LogError("设置依赖包出错：{0}", bundle.name);
        }
    }
    public bool TryGetBundle(string assetName, out AssetBundle bundle)
    {
        DependentBundle dependentBundle;
        if (dependentBundleDict.TryGetValue(assetName, out dependentBundle))
        {
            bundle = dependentBundle.bundle;
            return true;
        }
        else
        {
            bundle = null;
            return false;
        }
    }
    public void TryNewDependentBundle(string assetName)
    {
        DependentBundle dependentBundle;
        if (!dependentBundleDict.TryGetValue(assetName, out dependentBundle))
        {
            dependentBundle = new DependentBundle();
            dependentBundle.Init(assetName);
            dependentBundleDict[assetName] = dependentBundle;
        }
    }
    public void ADDDependentBundleRef(string assetName)
    {
        DependentBundle dependentBundle;
        if (dependentBundleDict.TryGetValue(assetName, out dependentBundle))
        {
            dependentBundle.ChangeCount(1);
            dependentBundleDict[assetName] = dependentBundle;
        }
        else
        {
            Printer.LogError("设置依赖包引用次数出错：{0}", assetName);
        }
    }
    public void CutDependentBundleRef(string assetName)
    {
        DependentBundle dependentBundle;
        if (dependentBundleDict.TryGetValue(assetName, out dependentBundle))
        {
            dependentBundle.ChangeCount(-1);
            dependentBundleDict[assetName] = dependentBundle;
        }
        else
        {
            Printer.LogError("设置依赖包引用次数出错：{0}", assetName);
        }
    }
    #endregion

    #region 获取Bundle资源的CRC值
    public uint GetAssetBundleCRC(string name)
    {
        AssetsDetail detail;
        if (assetsDetailDict.TryGetValue(name, out detail))
        {
            return detail.crc;
        }
        else
        {
            Printer.LogError("无法找到对应的CRC值：{0}", name);
            return 0;
        }
    }
    #endregion

    #region 加载对象列表
    private LinkedList<AsyncBaseSpawner> loadingList= new LinkedList<AsyncBaseSpawner>();
    public void AddLoadingLast(AsyncBaseSpawner spawner)
    {
        if (loadingList.Contains(spawner)) return;
        loadingList.AddLast(spawner);
        spawner.name = string.Format(string.Format("BundleSpawner-{0}", GetIndex()));
    }
    public void AddLoadingFirst(AsyncBaseSpawner spawner)
    {
        if (loadingList.Contains(spawner)) return;
        loadingList.AddFirst(spawner);
        spawner.name = string.Format(string.Format("BundleSpawner+{0}", GetIndex()));
    }
    public void RemoveLoadingList(AsyncBaseSpawner spawner)
    {
        loadingList.Remove(spawner);
    }
    //外部强制撤销资源申请
    public void DestroySpawner(int id)
    {
        foreach (var item in loadingList)
        {
            if(item.GetInstanceID() == id)
            {
                Destroy(item.gameObject);
                break;
            }
        }
    }
    #endregion

    #region 尝试加载
    private int index = 0;
    private int GetIndex()
    {
        return index++;
    }
    void Update()
    {
        int count = loadingList.Count;
        if (count <= 0)
        {
            index = 0;
            return;
        }
        AsyncBaseSpawner spawner = loadingList.First.Value;
        if (spawner)
        {
            spawner.Spawn();
        }
    }
    #endregion

    #region 异步更新缓存资源
    private int cacheSpawnerID;
    protected void AsyncCache(IEnumerable<BaseSpawnData> objectData, Action<bool> cb)
    {
        Printer.Log("cache");
        if (objectData == null)
        {
            if (cb != null) cb(false);
            return;
        }
        DestroySpawner(cacheSpawnerID);
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncCacheSpawner>();
        spawner.Init(this, objectData, PriorityMode.Last, cb, -1);
        spawner.SetUndoOnLoad(false);
        cacheSpawnerID = spawner.GetInstanceID();
    }
    #endregion

    #region 异步更新下载资源
    private int downLoadSpawnerID;
    protected void AsyncDownLoad(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, float timeout)
    {
        Printer.Log("download");
        if (objectData == null)
        {
            if (cb != null) cb(false);
            return;
        }
        DestroySpawner(downLoadSpawnerID);
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncDownloadSpawner>();
        spawner.Init(this, objectData, PriorityMode.Last, cb, timeout);
        spawner.SetUndoOnLoad(false);
        downLoadSpawnerID = spawner.GetInstanceID();
    }
    #endregion

    #region 异步预加载对象
    public int AsyncPreload(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, bool undoOnLoad)
    {
        if (objectData == null)
        {
            if (cb != null) cb(false);
            return - 1;
        }
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncPreloadSpawner>();
        spawner.Init(this, objectData, PriorityMode.Last, cb, -1);
        spawner.SetUndoOnLoad(undoOnLoad);
        return spawner.GetInstanceID();
    }
    #endregion

    #region 异步实例化对象
    public int AsyncInstantiate(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, bool undoOnLoad)
    {
        if (objectData == null)
        {
            if (cb != null) cb(false);
            return -1;
        }
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncInstantiateSpawner>();
        spawner.Init(this, objectData, PriorityMode.Last, cb, -1);
        spawner.SetUndoOnLoad(undoOnLoad);
        return spawner.GetInstanceID();
    }
    #endregion

    #region 异步加载非Prefab资源
    public int AsyncSpawnData(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, PriorityMode mode, bool undoOnLoad)
    {
        if (objectData == null)
        {
            if (cb != null) cb(false);
            return -1;
        }
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncDataSpawner>();
        spawner.Init(this, objectData, mode, cb, 0);
        spawner.SetUndoOnLoad(undoOnLoad);
        return spawner.GetInstanceID();
    }
    #endregion

    #region 异步加载场景
    private int sceneSpawnerID;
    public void AsyncSpawnScene(LevelData sceneData, Action<bool> cb)
    {
        DestroySpawner(sceneSpawnerID);
        BaseSpawnData[] data = new BaseSpawnData[] { sceneData };
        GameObject go = new GameObject();
        AsyncBaseSpawner spawner = go.AddComponent<AsyncSceneSpawner>();
        spawner.Init(this, data, PriorityMode.First, cb, -1);
        spawner.SetUndoOnLoad(false);
        sceneSpawnerID = spawner.GetInstanceID();
    }
    #endregion

    #region 读取指定配置表
    public TextAsset SyncSpawnConfig(string configName)
    {
        TextAsset text;
        if(configBundle == null)
        {
            AssetsDetail detail = GetAssetsDetail(configName);
            text = Resources.Load<TextAsset>(detail.path);
        }
        else
        {
            text = configBundle.LoadAsset<TextAsset>(configName);
        }
        return text;
    }
    #endregion
}
