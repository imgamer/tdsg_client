﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
/// <summary>
/// 资源服务器加载模式
/// </summary>
public class ServerModeLoader : BaseModeLoader
{
    protected override string ManifestUrl
    {
        get { return AssetsUtils.GetUrlFromServer(ManifestName); }
    }
    protected override string DetailConfigUrl
    {
        get { return AssetsUtils.GetUrlFromServer(DetailConfigName); }
    }

    private Dictionary<string, uint> newCrcDict = new Dictionary<string, uint>();
    protected override void OnInit(bool ready)
    {
        if (ready)
        {
            LoadCacheText();
            List<BaseSpawnData> downList = new List<BaseSpawnData>();
            List<BaseSpawnData> cacheList = new List<BaseSpawnData>();
            foreach (var item in assetsDetailDict)
            {
                AssetsDetail detail = item.Value;
                if (detail.crc == 0) continue;
                newCrcDict[item.Key] = detail.crc;

                uint crc;
                if (preCrcDict.TryGetValue(item.Key, out crc))
                {
                    if (crc == detail.crc) continue;
                }
                CacheOrDownloadData data = new CacheOrDownloadData(item.Key);
                switch (item.Value.type)
                {
                    case AssetsType.ResourceAssets:
                        break;
                    case AssetsType.LocalAssets:
                        cacheList.Add(data);
                        break;
                    case AssetsType.ServerAssets:
                        downList.Add(data);
                        break;
                    default:
                        break;
                }
            }
            AsyncDownLoad(downList, (ok) => 
            {
                if (ok)
                {
                    AsyncCache(cacheList, WriteCacheText);
                }
                else
                {
                    WriteCacheText(false);
                }
            }, 2);
        }
        else
        {
            if (OnReady != null) OnReady(false);
        }
    }

    #region 读写缓存信息
    private const string CacheAssetsText = "detail_text.txt";
    private const char SplitChar = '\t';
    private Dictionary<string, uint> preCrcDict = new Dictionary<string, uint>();
    private void LoadCacheText()
    {
        string path = string.Format("{0}//{1}", Application.persistentDataPath, CacheAssetsText);
        if (!File.Exists(path)) return;
        StreamReader sr = File.OpenText(path);
        preCrcDict.Clear();

        string line;
        while ((line = sr.ReadLine()) != null)
        {
            if (string.IsNullOrEmpty(line)) continue;
            string[] columns = line.Split(new char[] { SplitChar });
            if (columns.Length != 2) continue;
            preCrcDict[columns[0]] = uint.Parse(columns[1]);
        }
        sr.Close();
        sr.Dispose();
    }

    private void WriteCacheText(bool ready)
    {
        if (ready)
        {
            try
            {
                string path = string.Format("{0}//{1}", Application.persistentDataPath, CacheAssetsText);
                FileStream fs = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Seek(0, SeekOrigin.Begin);
                fs.SetLength(0);
                fs.Close();

                StreamWriter sw = new StreamWriter(path, true, Encoding.UTF8);
                foreach (var item in newCrcDict)
                {
                    sw.WriteLine(item.Key + "\t" + item.Value + "\n");
                }
                newCrcDict.Clear();
                sw.Flush();
                sw.Close();

                InitConfig(OnReady);
            }
            catch (Exception ex)
            {
                Printer.LogException(ex);
                if (OnReady != null) OnReady(false);
            }
        }
        else
        {
            if (OnReady != null) OnReady(false);
        }
    }
    #endregion
}
