﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
/// <summary>
/// 资源工具类
/// </summary>
public class AssetsUtils
{
    #region 获取URL
    private static string GetServerPath()
    {
        return string.Format("http://{0}/{1}/{2}", AssetsManager.instance.assetServerIp, AssetsManager.instance.assetServerPort, AssetsManager.instance.assetServerFilePath);
    }
    public static string GetUrlFromServer(string bundleName)
    {
        string url = string.Empty;
        if (string.IsNullOrEmpty(bundleName)) return url;

        url = string.Format("{0}/{1}", GetServerPath(), bundleName);
        return url;
    }
    public static string GetUrlFromLocal(string bundleName)
    {
        string url = string.Empty;
        if (string.IsNullOrEmpty(bundleName)) return url;

        string urlName = string.Empty;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
        urlName = ToLocalBundlePath(bundleName);
        url = string.Format("file://{0}", urlName);
#else
        urlName = bundleName;
#if UNITY_ANDROID
        url = string.Format("jar:file://{0}!/assets/{1}", Application.dataPath, urlName);
#elif UNITY_IPHONE
        url = string.Format("file://{0}/{1}", Application.streamingAssetsPath, urlName);     
#elif UNITY_FLASH
        //TODO
#endif

#endif
        return url;
    }
    /// <summary>
    /// 转换本地Bundle路径
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private static string ToLocalBundlePath(string fileName)
    {
        return string.Format("{0}/StreamingAssets/{1}", Application.dataPath, fileName);
    }
    #endregion

    #region 同步方式加载WWW
    public static T LoadWWWSync<T>(string url, float timeout) where T : class
    {
        if (string.IsNullOrEmpty(url))
        {
            Printer.LogError(string.Format("Url is Error"));
            return null;
        }

        WWW www = new WWW(url);
        float startTime = Time.realtimeSinceStartup;
        int preBytesDownloaded = 0;
        while (!www.isDone)
        {
            if (preBytesDownloaded != www.bytesDownloaded)
            {
                startTime = Time.realtimeSinceStartup;
                preBytesDownloaded = www.bytesDownloaded;
            }
            else if (timeout > 0 && Time.realtimeSinceStartup - startTime > timeout)
            {
                Printer.LogError(string.Format("超时:{0}", www.url));
                break;
            }
            if (!string.IsNullOrEmpty(www.error))
            {
                break;
            }
        }
        T t = GetData<T>(www);
        www.Dispose();
        return t;
    }
    #endregion

    #region 获取WWW数据
    public static T GetData<T>(WWW www) where T : class
    {
        if(!www.isDone)
        {
            Printer.LogError(string.Format("下载未完成：{0}", www.url));
            return null;
        }
        if (!string.IsNullOrEmpty(www.error))
        {
            Printer.LogError(string.Format("下载出错：{0}; 错误：{1}", www.url, www.error));
            return null;
        }
        object obj = null;
        Type type = typeof(T);
        if (type.Name == typeof(string).Name)
        {
            obj = www.text;
        }
        else if (type.Name == typeof(byte[]).Name)
        {
            obj = www.bytes;
        }
        else if (type.Name == typeof(AudioClip).Name)
        {
            obj = www.audioClip;
        }
        else if (type.Name == typeof(AssetBundle).Name)
        {
            obj = www.assetBundle;
        }
        else
        {
            Printer.LogError(string.Format("不支持的类型：{0}; URL：{1}", type.Name, www.url));
        }
        return obj as T;
    }
    #endregion

    #region 平台路径相关
    /// <summary>
    /// 根据目录地址获取资源地址
    /// </summary>
    /// <param name="path">资源地址</param>
    /// <returns></returns>
    public static List<string> GetPaths(string path)
    {
        List<string> ret = new List<string>();

        if (string.IsNullOrEmpty(path)) return ret;
        string[] paths = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
        foreach (var p in paths)
        {
            string str = p.Replace('\\', '/');
            ret.Add(str);
        }
        return ret;
    }
    /// <summary>
    /// 根据平台设定尾缀符号
    /// </summary>
    /// <returns></returns>
    public static string ToBundleVariant()
    {
        string variant = string.Empty;
#if UNITY_ANDROID
        variant = string.Format("a");
#elif UNITY_IPHONE
        variant = string.Format("i");
#elif UNITY_WEBPLAYER
        variant = string.Format("w");
#elif UNITY_STANDALONE_WIN
        variant = string.Format("s");
#elif UNITY_FLASH
        variant = string.Format("f");
#endif
        return variant;
    }
    /// <summary>
    /// 本地初始Bundle文件目录
    /// </summary>
    /// <returns></returns>
    public static string ToLocalFilePath(string path)
    {
        return string.Format("{0}/{1}", Application.dataPath, path);
    }
    #endregion

    #region Bundle名称的转换
    /// <summary>
    /// 获取完整的Bundle名称
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static string ToAssetName(string name)
    {
        return string.Format("{0}.{1}", name, ToBundleVariant());
    }
    public static string RemoveBundleVariant(string assetName)
    {
        string variant = string.Empty;
#if UNITY_ANDROID
        variant = string.Format(".a");
#elif UNITY_IPHONE
        variant = string.Format(".i");
#elif UNITY_WEBPLAYER
        variant = string.Format(".w");
#elif UNITY_STANDALONE_WIN
        variant = string.Format(".s");
#elif UNITY_FLASH
        variant = string.Format(".f");
#endif
        return assetName.Replace(variant, "");
    }
    #endregion

}
