﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 资源管理类
/// </summary>
public class AssetsManager : MonoBehaviour 
{
    #region 
    public static AssetsManager instance { get; private set; }
	void Awake () 
    {
        instance = this;
	}
    #endregion

    public LoadMode type = LoadMode.LocalMode;
    public string assetServerIp = "";
    public string assetServerPort = "";
    public string assetServerFilePath = "";

    private BaseModeLoader loader;
    private bool inited = false;
	public void Init() 
    {
        if (inited) return;
        inited = true;
        switch (type)
        {
            case LoadMode.LocalMode:
                loader = gameObject.AddComponent<LocalModeLoader>();
                loader.Init(Ready, -1);
                break;
            case LoadMode.ServerMode:
                loader = gameObject.AddComponent<ServerModeLoader>();
                loader.Init(Ready, 2);
                break;
            default:
                break;
        }
	}

    public void UnInit()
    {
        inited = false;
        if (loader) Destroy(loader);
    }

    private void Ready(bool ready)
    {
        if(ready)
        {
            Printer.Log("加载成功");
            GameMain.instance.Ready();
        }
        else
        {
            Printer.LogError("加载失败");
        }
    }

    #region 同步读取配置
    public TextAsset SyncSpawnConfig(string configName)
    {
        return loader.SyncSpawnConfig(configName);
    }
    #endregion

    #region 异步预加载对象
    public int AsyncPreload(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, bool undoOnLoad = true)
    {
        return loader.AsyncPreload(objectData, cb, undoOnLoad);
    }
    #endregion

    #region 异步实例化对象
    public int AsyncInstantiate(IEnumerable<BaseSpawnData> objectData, Action<bool> cb = null, bool undoOnLoad = true)
    {
        return loader.AsyncInstantiate(objectData, cb, undoOnLoad);
    }
    #endregion

    #region 异步加载非Prefab资源
    public int AsyncSpawnData(IEnumerable<BaseSpawnData> objectData, Action<bool> cb, PriorityMode mode, bool undoOnLoad = true)
    {
        return loader.AsyncSpawnData(objectData, cb, mode, undoOnLoad);
    }
    #endregion

    #region 异步加载场景
    public void AsyncSpawnScene(LevelData sceneData, Action<bool> cb = null)
    {
        loader.AsyncSpawnScene(sceneData, cb);
    }
    #endregion

    #region 查找资源是否存在指定的游戏池中
    public bool Contains(string assetName, string objectName, PoolName poolName, PoolType type)
    {
        return GamePoolManager.instance.Contains(assetName, objectName, poolName, type);
    }
    #endregion

    #region 销毁对象
    public void Despawn(GameObject go, float time = 0)
    {
        if(time <= 0)
        {
            GamePoolManager.instance.Despawn(go);
        }
        else
        {
            ulong timerID = TimeUtils.AddRealTimer(time, null, () =>
            {
                GamePoolManager.instance.Despawn(go);
            });
            TimeUtils.StartTimer(timerID);
        }
    }
    #endregion

    #region 清除游戏池
    public void ClearPool(PoolName poolName)
    {
        GamePoolManager.instance.Clear(poolName);
    }
    #endregion

    #region 垃圾回收释放
    public void Collect()
    {
        Resources.UnloadUnusedAssets();
        GC.Collect();
    }
    #endregion

}
