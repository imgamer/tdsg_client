﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Text;
namespace Assets
{
    /// <summary>
    /// 打包工具
    /// </summary>
    public class BundleUtils
    {
        #region 自定义参数
        private static readonly string FixedAssetsPath = "Assets/Games/GameAssets/FixedAssets";         //固定资源文件路径
        private static readonly string UnfixedAssetsPath = "Assets/Games/GameAssets/UnfixedAssets";     //非固定资源文件路径
        private static readonly string BundleFilePath = "Assets/Games/GameAssets/BundleAssets";       //所有资源Bundle文件路径
        private static readonly string LocalBundleFilePath = "Games/GameAssets/BundleAssets";
        private static readonly string StreamingFilePath = "Assets/StreamingAssets";                 //本地资源Bundle文件路径
        private static readonly string ServerFilePath = "Assets/Games/GameAssets/ServerAssets";          //网络资源Bundle文件路径
        private static readonly string ScenesFilePath = "Assets/Games/GameAssets/UnfixedAssets/Scenes";

        private static readonly string DetailTextPath = string.Format("{0}/{1}", UnfixedAssetsPath, "DetailConfig/detail_text.txt");
        private static readonly string DetailConfigName = "detail_config";
        private static readonly string ResourcesFileName = "Resources";                  //Resources文件夹名
        private static readonly string PackagesFileName = "Packages";                    //Packages文件夹名
        private static readonly string BundleFileName = "BundleAssets";

        protected static Dictionary<string, AssetsDetail> assetsDetailDict = new Dictionary<string, AssetsDetail>();
        protected static Dictionary<AssetsType, List<AssetBundleBuild>> assetBundleDict = new Dictionary<AssetsType, List<AssetBundleBuild>>();
        #endregion

        [MenuItem("BundleTools/Resources打包方式(默认)")]
        public static void PackResources()
        {
            if (!EditorApplication.currentScene.Contains("GameStart"))
            {
                EditorUtility.DisplayDialog("提示", "请切换到GameStart场景打包！", "OK");
                return;
            }
            if (!EditorUtility.DisplayDialog("Resources打包", "是否开始打包", "Yes", "No"))
            {
                return;
            }
            SceneSettings(AssetsType.ResourceAssets);
            ResetAssets();
            PackResourceAssets();
            DistributeAssets();
            EditorUtility.DisplayDialog("完成", "资源打包完成，请检查Console窗口是否有错误提示", "OK");
        }
        [MenuItem("BundleTools/本地Bundle打包方式")]
        public static void PackLocalBundles()
        {
            if (!EditorApplication.currentScene.Contains("GameStart"))
            {
                EditorUtility.DisplayDialog("提示", "请切换到GameStart场景打包！", "OK");
                return;
            }
            if (!EditorUtility.DisplayDialog("本地Bundle打包", "是否开始打包", "Yes", "No"))
            {
                return;
            }
            SceneSettings(AssetsType.LocalAssets);
            ResetAssets();
            PackBundles(AssetsType.LocalAssets);
            DistributeAssets();
            EditorUtility.DisplayDialog("完成", "资源打包完成，请检查Console窗口是否有错误提示", "OK");
            EditorUtility.DisplayDialog("温馨提示", "记得切换回Resources打包方式，再进行SVN更新", "OK");
        }
        [MenuItem("BundleTools/网络Bundle打包方式")]
        public static void PackServerBundles()
        {
            if (!EditorApplication.currentScene.Contains("GameStart"))
            {
                EditorUtility.DisplayDialog("提示", "请切换到GameStart场景打包！", "OK");
                return;
            }
            if (!EditorUtility.DisplayDialog("网络Bundle打包", "是否开始打包", "Yes", "No"))
            {
                return;
            }
            SceneSettings(AssetsType.ServerAssets);
            ResetAssets();
            PackBundles(AssetsType.ServerAssets);
            DistributeAssets();
            EditorUtility.DisplayDialog("完成", "资源打包完成，请检查Console窗口是否有错误提示", "OK");
            EditorUtility.DisplayDialog("温馨提示", "记得切换回Resources打包方式，再进行SVN更新", "OK");
        }

        [MenuItem("BundleTools/清空缓存")]
        public static void ClearCache()
        {
            if (!EditorUtility.DisplayDialog("清空缓存", "是否清空缓存", "Yes", "No"))
            {
                return;
            }
            Caching.CleanCache();
        }

        #region 设置
        private static void SceneSettings(AssetsType type)
        {
            bool active = false;
            switch (type)
            {
                case AssetsType.ResourceAssets:
                    active = true;
                    break;
                case AssetsType.LocalAssets:
                case AssetsType.ServerAssets:
                    active = false;
                    break;
                default:
                    break;
            }
            EditorBuildSettingsScene[] scenesSettings = new EditorBuildSettingsScene[EditorBuildSettings.scenes.Length];
            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                EditorBuildSettingsScene item = EditorBuildSettings.scenes[i];
                item.enabled = active || i == 0;
                scenesSettings[i] = item;
            }
            EditorBuildSettings.scenes = scenesSettings;
        }
        #endregion

        #region 打包
        //Resources方式打包
        private static void PackResourceAssets()
        {
            UnfixedChildFileRename(ResourcesFileName);
            assetsDetailDict.Clear();
            assetBundleDict.Clear();

            PackAssetsDetailText(AssetsType.ResourceAssets);
            BuildAssetBundles();
        }
        //Bundle方式打包
        private static void PackBundles(AssetsType type)
        {
            UnfixedChildFileRename(PackagesFileName);
            assetsDetailDict.Clear();
            assetBundleDict.Clear();

            PackGroup("Configs", type);
            PackSingle("SkillEffects", type);
            PackSingle("SceneEffects", type);
            PackSingle("UIEffects", type);
            PackSingle("Seq", type);
            PackSingle("SelectRole", type);
            PackSingle("Models/Template", type);
            PackSingle("Models/Monsters", type);
            PackSingle("Models/NPCs", type);
            PackSingle("Models/Roles", type);

            PackScene(type);

            BuildAssetBundles();
            PackAssetsDetailText(type);
            BuildAssetBundles();
        }
        #endregion

        #region 设置UnfixedChildFile文件夹的名称
        private static void UnfixedChildFileRename(string newName)
        {
            string oldName = newName.Equals(ResourcesFileName) ? PackagesFileName : ResourcesFileName;
            if (oldName.Equals(newName)) return;
            AssetDatabase.RenameAsset(string.Format("{0}/{1}", UnfixedAssetsPath, oldName), newName);
            AssetDatabase.Refresh();
        }
        #endregion

        #region 重置回收分配出去的Bundle资源到BundleAssets文件夹
        private static void ResetAssets()
        {
            DirectoryInfo dirInfo1 = new DirectoryInfo(StreamingFilePath);
            FileInfo[] files1 = dirInfo1.GetFiles();
            foreach (FileInfo file in files1)
            {
                file.MoveTo(Path.Combine(BundleFilePath, file.Name));
            }
            DirectoryInfo dirInfo2 = new DirectoryInfo(ServerFilePath);
            FileInfo[] files2 = dirInfo2.GetFiles();
            foreach (FileInfo file in files2)
            {
                file.MoveTo(Path.Combine(BundleFilePath, file.Name));
            }
            AssetDatabase.Refresh();
        }
        #endregion

        #region 分配BundleAssets文件夹下的Bundle资源到相应文件夹
        private static void DistributeAssets()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(BundleFilePath);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (FileInfo file in files)
            {
                string name = Path.GetFileNameWithoutExtension(file.Name);
                string[] strs = name.Split(new char[] { '.' });
                AssetsDetail assetsDetail;
                if (assetsDetailDict.TryGetValue(strs[0], out assetsDetail))
                {
                    switch (assetsDetail.type)
                    {
                        case AssetsType.ResourceAssets:
                            if (string.IsNullOrEmpty(assetsDetail.path)) 
								file.MoveTo(Path.Combine(StreamingFilePath, file.Name));
                            break;
                        case AssetsType.LocalAssets:
                            file.MoveTo(Path.Combine(StreamingFilePath, file.Name));
                            break;
                        case AssetsType.ServerAssets:
                            file.MoveTo(Path.Combine(ServerFilePath, file.Name));
                            break;
                    }
                }
            }
            AssetDatabase.Refresh();
        }
        #endregion

        #region 设置资源详细信息
        private static void SetAssetsDetailText(AssetsType type)
        {
            AssetsDetail assetsDetailText = new AssetsDetail(DetailConfigName, type, string.Empty, 0, 0);
            assetsDetailDict[DetailConfigName] = assetsDetailText;
            AssetsDetail assetBundleManifest = new AssetsDetail(BundleFileName, type, string.Empty, 0, 0);
            assetsDetailDict[BundleFileName] = assetBundleManifest;

            SetAssetsDetailDict(type);

            switch (type)
            {
                case AssetsType.ResourceAssets:
                    SetAssetsDetailDict(string.Format("{0}", ScenesFilePath));
                    SetAssetsDetailDict(string.Format("{0}/{1}", FixedAssetsPath, ResourcesFileName));
                    SetAssetsDetailDict(string.Format("{0}/{1}", UnfixedAssetsPath, ResourcesFileName));
                    break;
                case AssetsType.LocalAssets:
                    SetAssetsDetailDict(string.Format("{0}/{1}", FixedAssetsPath, ResourcesFileName));
                    break;
                case AssetsType.ServerAssets:
                    SetAssetsDetailDict(string.Format("{0}/{1}", FixedAssetsPath, ResourcesFileName));
                    break;
            }
        }
        //设置Resource文件夹的资源信息
        private static void SetAssetsDetailDict(string path)
        {
            List<string> paths = GetPaths(path);
            foreach (var assetName in paths)
            {
                UnityEngine.Object o = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetName);
                if (o)
                {
                    string[] strs = assetName.Split(new char[] { '.' });
                    string asssetPath = strs[0].Replace(path + "/", "");
                    AssetsDetail assetsDetail = GetAssetsDetail(o.name, AssetsType.ResourceAssets, asssetPath, 0, 0);
                    if (string.IsNullOrEmpty(assetsDetail.name)) continue;
                    if (assetsDetailDict.ContainsKey(assetsDetail.name))
                    {
                        Printer.LogError("包含相同的资源名称:{0}", assetsDetail.name);
                        continue;
                    }
                    assetsDetailDict[assetsDetail.name] = assetsDetail;
                }
            }
        }
        private static void SetAssetsDetailDict(AssetsType type)
        {
            foreach (var item in assetBundleDict)
            {
                foreach (var assetBundle in item.Value)
                {
                    string path = string.Format(string.Format("{0}/{1}.{2}", BundleFilePath, assetBundle.assetBundleName, assetBundle.assetBundleVariant));
                    uint crc;
                    BuildPipeline.GetCRCForAssetBundle(path, out crc);
                    FileStream fild = new FileStream(path, FileMode.Open, FileAccess.Read);
                    AssetsDetail assetsDetail = GetAssetsDetail(assetBundle.assetBundleName, item.Key, string.Format("{0}.{1}", assetBundle.assetBundleName, assetBundle.assetBundleVariant), crc, (int)fild.Length);
                    fild.Close();
                    if (string.IsNullOrEmpty(assetsDetail.name)) continue;
                    if (assetsDetailDict.ContainsKey(assetsDetail.name))
                    {
                        Printer.LogError("包含相同的资源名称:{0}", assetsDetail.name);
                        continue;
                    }
                    assetsDetailDict[assetsDetail.name] = assetsDetail;
                }
            }
        }
        private static AssetsDetail GetAssetsDetail(string name, AssetsType type, string path, uint crc, int size)
        {
            if (string.IsNullOrEmpty(name))
            {
                Debug.LogError("资源包名字为空!");
                return default(AssetsDetail);
            }
            if (!name.Equals(name.ToLower()))
            {
                Debug.LogError(string.Format("资源包：{0} 命名不规范，必须全部字母小写！！！路径：{1}", name, path));
                return default(AssetsDetail);
            }
            AssetsDetail detail = new AssetsDetail(name, type, path, crc, size);
            return detail;
        }
        private static void SetAssetBundleDict(AssetsType type, List<AssetBundleBuild> list)
        {
            List<AssetBundleBuild> builds;
            if (!assetBundleDict.TryGetValue(type, out builds))
            {
                builds = new List<AssetBundleBuild>();
            }
            builds.AddRange(list);
            assetBundleDict[type] = builds;
        }
        #endregion

        #region 资源信息表打包
        private static void PackAssetsDetailText(AssetsType type)
        {
            SetAssetsDetailText(type);

            List<AssetBundleBuild> list = PackAssetsDetailText();
            SetAssetBundleDict(type, list);
        }
        private static List<AssetBundleBuild> PackAssetsDetailText()
        {
            FileStream fs1 = File.Open(DetailTextPath, FileMode.OpenOrCreate, FileAccess.Write);
            fs1.Seek(0, SeekOrigin.Begin);
            fs1.SetLength(0);
            fs1.Close();

            StreamWriter sw1 = new StreamWriter(DetailTextPath, true, Encoding.UTF8);
            foreach (var item in assetsDetailDict.Values)
            {
                sw1.WriteLine(item.name + "\t" + item.type + "\t" + item.path + "\t" + item.crc + "\t" + item.size + "\n");
            }
            sw1.Flush();
            sw1.Close();
            AssetDatabase.Refresh();

            List<AssetBundleBuild> builds = new List<AssetBundleBuild>();

            AssetBundleBuild build = new AssetBundleBuild();
            build.assetBundleName = DetailConfigName;
            build.assetNames = new string[] { DetailTextPath };
            builds.Add(build);

            return builds;
        }
        #endregion

        #region 单独打包
        public static void PackSingle(string filePath, AssetsType type)
        {
            string path = string.Format("{0}/{1}/{2}", UnfixedAssetsPath, PackagesFileName, filePath);
            List<AssetBundleBuild> list = PackSingle(path);
            SetAssetBundleDict(type, list);
        }
        /// <summary>
        /// 分别单独打包
        /// </summary>
        /// <param name="path">目录地址</param>
        private static List<AssetBundleBuild> PackSingle(string path)
        {
            List<AssetBundleBuild> builds = new List<AssetBundleBuild>();

            List<string> paths = GetPaths(path);
            if (paths.Count <= 0)
            {
                Debug.LogError(string.Format("{0}：没有可打包对象", path));
                return builds;
            }

            string variant = ToBundleVariant();
            foreach (var assetName in paths)
            {
                UnityEngine.Object o = AssetDatabase.LoadAssetAtPath(assetName, typeof(UnityEngine.Object));
                if (o)
                {
                    AssetBundleBuild build = new AssetBundleBuild();
                    build.assetBundleName = o.name;
                    build.assetBundleVariant = variant;
                    build.assetNames = new string[] { assetName };
                    builds.Add(build);
                }
            }

            return builds;
        }
        #endregion

        #region 成组打包
        public static void PackGroup(string fileName, AssetsType type)
        {
            string path = string.Format("{0}/{1}/{2}", UnfixedAssetsPath, PackagesFileName, fileName);
            List<AssetBundleBuild> list = PackGroup(fileName.ToLower(), path);
            SetAssetBundleDict(type, list);
        }
        /// <summary>
        /// 将所有资源打包生成同一个包
        /// </summary>
        /// <param name="pkgID">包名</param>
        /// <param name="path">目录地址</param>
        /// <returns></returns>
        private static List<AssetBundleBuild> PackGroup(string pkgID, string path)
        {
            List<AssetBundleBuild> builds = new List<AssetBundleBuild>();

            List<string> paths = GetPaths(path);
            if (paths.Count <= 0)
            {
                Debug.LogError(string.Format("{0}：没有可打包对象", pkgID));
                return builds;
            }

            List<string> assetNames = new List<string>();
            foreach (var assetName in paths)
            {
                UnityEngine.Object o = AssetDatabase.LoadAssetAtPath(assetName, typeof(UnityEngine.Object));
                if (o)
                {
                    assetNames.Add(assetName);
                }
            }
            if (assetNames.Count <= 0)
            {
                Debug.LogError(string.Format("{0}：没有可打包对象", pkgID));
                return builds;
            }
            string variant = ToBundleVariant();
            AssetBundleBuild build = new AssetBundleBuild();
            build.assetBundleName = pkgID;
            build.assetBundleVariant = variant;
            build.assetNames = assetNames.ToArray();
            builds.Add(build);

            return builds;
        }
        #endregion

        #region 共享打包
        public static void PackShare(string fileName, AssetsType type)
        {
            string path = string.Format("{0}/{1}/{2}", UnfixedAssetsPath, PackagesFileName, fileName);
            List<AssetBundleBuild> list = PackShare(fileName.ToLower(), path);
            SetAssetBundleDict(type, list);
        }
        private static List<AssetBundleBuild> PackShare(string pkgID, string path)
        {
            return PackShare(pkgID, GetPaths(path));
        }
        /// <summary>
        /// 生成共享包，并分别打包目录里的其他资源
        /// </summary>
        /// <param name="pkgID">共享包名</param>
        /// <param name="path">目录地址</param>
        private static List<AssetBundleBuild> PackShare(string pkgID, List<string> paths)
        {
            List<AssetBundleBuild> builds = new List<AssetBundleBuild>();
            if (paths.Count <= 0)
            {
                Debug.LogError(string.Format("{0}：没有可打包对象", pkgID));
                return builds;
            }

            string variant = ToBundleVariant();
            List<UnityEngine.Object> objects = new List<UnityEngine.Object>();
            bool init = false;
            foreach (var assetName in paths)
            {
                UnityEngine.Object o = AssetDatabase.LoadAssetAtPath(assetName, typeof(UnityEngine.Object));
                if (o)
                {
                    AssetBundleBuild build = new AssetBundleBuild();
                    build.assetBundleName = o.name;
                    build.assetBundleVariant = variant;
                    build.assetNames = new string[] { assetName };
                    builds.Add(build);
                    if (!init)
                    {
                        string[] strs = AssetDatabase.GetDependencies(new string[] { assetName });
                        foreach (var item in strs)
                        {
                            UnityEngine.Object t = AssetDatabase.LoadAssetAtPath(item, typeof(UnityEngine.Object));
                            objects.Add(t);
                        }
                        init = true;
                    }
                    else
                    {
                        string[] strs = AssetDatabase.GetDependencies(new string[] { assetName });
                        for (int i = objects.Count - 1; i >= 0; i--)
                        {
                            UnityEngine.Object s = objects[i];
                            bool haved = false;
                            foreach (var item in strs)
                            {
                                UnityEngine.Object t = AssetDatabase.LoadAssetAtPath(item, typeof(UnityEngine.Object));
                                if (s == t)
                                {
                                    haved = true;
                                    break;
                                }
                            }
                            if (!haved) objects.Remove(s);
                        }
                    }
                }
            }

            List<string> shareAssetNames = new List<string>();
            foreach (var item in objects)
            {
                string p = AssetDatabase.GetAssetPath(item);
                shareAssetNames.Add(p);
                Debug.Log(string.Format("{0} : {1}", pkgID, p));
            }

            if (objects.Count > 0)
            {
                //依赖打包，多了一个共享包，所有+1
                AssetBundleBuild shareBuild = new AssetBundleBuild();
                shareBuild.assetBundleName = pkgID;
                shareBuild.assetBundleVariant = variant;
                shareBuild.assetNames = shareAssetNames.ToArray();
                builds.Add(shareBuild);
            }

            return builds;
        }
        #endregion

        #region 场景打包
        public static void PackScene(AssetsType type)
        {
            List<AssetBundleBuild> list = PackScene(ScenesFilePath);
            SetAssetBundleDict(type, list);
        }
        private static List<AssetBundleBuild> PackScene(string path)
        {
            List<AssetBundleBuild> builds = new List<AssetBundleBuild>();

            List<string> paths = GetPaths(path);
            if (paths.Count <= 0)
            {
                Debug.LogError(string.Format("{0}：没有可打包对象", path));
                return builds;
            }

            string variant = ToBundleVariant();
            foreach (string assetName in paths)
            {
                if (assetName.IndexOf(".unity") != -1)
                {
                    UnityEngine.Object scene = AssetDatabase.LoadAssetAtPath(assetName, typeof(UnityEngine.Object));
                    if (scene)
                    {
                        AssetBundleBuild build = new AssetBundleBuild();
                        build.assetBundleName = scene.name;
                        build.assetBundleVariant = variant;
                        build.assetNames = new string[] { assetName };
                        builds.Add(build);
                    }
                }
            }

            return builds;
        }
        #endregion

        #region 生成包文件
        private static void BuildAssetBundles()
        {
            List<AssetBundleBuild> list = new List<AssetBundleBuild>();
            foreach (var item in assetBundleDict)
            {
                foreach (var asset in item.Value)
                {
                    list.Add(asset);
                }
            }
            string bundlePath = ToLocalFilePath(LocalBundleFilePath);
            BuildTarget target = ToBuildTarget();
            BuildPipeline.BuildAssetBundles(bundlePath, list.ToArray(), BuildAssetBundleOptions.None, target);
            AssetDatabase.Refresh();
        }
        #endregion

        #region 工具方法
        /// <summary>
        /// 根据目录地址获取资源地址
        /// </summary>
        /// <param name="path">资源地址</param>
        /// <returns></returns>
        private static List<string> GetPaths(string path)
        {
            List<string> ret = new List<string>();

            if (string.IsNullOrEmpty(path)) return ret;
            string[] paths = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            foreach (var p in paths)
            {
                string str = p.Replace('\\', '/');
                ret.Add(str);
            }
            return ret;
        }
        /// <summary>
        /// 获取平台
        /// </summary>
        /// <returns></returns>
        private static BuildTarget ToBuildTarget()
        {
            BuildTarget target = BuildTarget.StandaloneWindows;
#if UNITY_ANDROID
            target = BuildTarget.Android;
#elif UNITY_IPHONE
        target = BuildTarget.iPhone;
#elif UNITY_WEBPLAYER
        target = BuildTarget.WebPlayer;
#elif UNITY_STANDALONE_WIN
        target = BuildTarget.StandaloneWindows;
#elif UNITY_FLASH
        target = BuildTarget.FlashPlayer;
#endif
            return target;
        }
        /// <summary>
        /// 根据平台设定尾缀符号
        /// </summary>
        /// <returns></returns>
        private static string ToBundleVariant()
        {
            string variant = string.Format("s");
#if UNITY_ANDROID
            variant = string.Format("a");
#elif UNITY_IPHONE
        variant = string.Format("i");
#elif UNITY_WEBPLAYER
        variant = string.Format("w");
#elif UNITY_STANDALONE_WIN
        variant = string.Format("s");
#elif UNITY_FLASH
        variant = string.Format("f");
#endif
            return variant;
        }
        /// <summary>
        /// 本地目录
        /// </summary>
        /// <returns></returns>
        private static string ToLocalFilePath(string path)
        {
            return string.Format("{0}/{1}", Application.dataPath, path);
        }
        #endregion

    }
}

