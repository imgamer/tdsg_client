﻿public enum PoolName
{
    Permanent,
    Scene,
    HUD,
    Model,
    Effect,
    Seq,
    Max
}
public enum PoolType
{
    Default, // 从不进资源池，不创建不回收
    Once, // 一次性资源池，只创建不回收
    Loop  // 循环性资源池，既创建也回收
}
