﻿using System.Collections.Generic;
using UnityEngine;
public class PoolObject : MonoBehaviour
{
    public string objectName { get; private set; }
    public string key{get; private set;}
    public PoolName poolName{get; private set;}
    public PoolType type{get; private set;}
    public void Init(string key, string objectName, PoolName poolName, PoolType type)
    {
        this.key = key;
        this.objectName = objectName;
        this.poolName = poolName;
        this.type = type;
    }

    public void Reset()
    {
        gameObject.name = objectName;
    }
}
