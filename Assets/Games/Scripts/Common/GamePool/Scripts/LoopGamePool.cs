﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 循环性资源池，即创建也回收
/// </summary>
public class LoopGamePool : BasePool
{
    protected override int MaxNum { get { return 10; } }
    protected override GameObject OnSpawn(string key)
    {
        Stack<GameObject> stack;
        if (poolObjectDic.TryGetValue(key, out stack))
        {
            if (stack.Count == 1)
            {
                GameObject ret = Instantiate(stack.Peek());
                ret.SetActive(true);
                ret.transform.parent = transform;
                return ret;
            }
            else if (stack.Count > 1)
            {
                GameObject ret = stack.Pop();
                if (ret == null)
                {
                    Printer.LogError("循环池的资源:{0}被意外删除!", key);
                    return null;
                }
                ret.SetActive(true);
                return ret;
            }
        }
        return null;
    }
    protected override void OnDespawn(string key, GameObject o)
    {
        Stack<GameObject> stack;
        if (!poolObjectDic.TryGetValue(key, out stack))
        {
            stack = new Stack<GameObject>(MaxNum);
            poolObjectDic[key] = stack;
        }
        if (stack.Contains(o))
        {
            Printer.LogError("资源被重复回收:{0}", o.name);
            return;
        }
        if (stack.Count >= MaxNum)
        {
            Destroy(o);
            Printer.Log("游戏池达到最大数量: {0}", key);
            return;
        }
        o.SetActive(false);
        Transform item = o.transform;
        item.parent = transform;
        item.localPosition = Vector3.zero;
        item.localEulerAngles = Vector3.zero;
        item.localScale = Vector3.one;

        stack.Push(o);
    }
}
