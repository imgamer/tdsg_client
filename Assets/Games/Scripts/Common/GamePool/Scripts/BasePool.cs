﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 资源池基类
/// </summary>
public abstract class BasePool : MonoBehaviour
{
    protected Dictionary<string, Stack<GameObject>> poolObjectDic = new Dictionary<string, Stack<GameObject>>();
    protected abstract int MaxNum { get; }
    /// <summary>
    /// 是否存在池对象
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public bool Contains(string key)
    {
        Stack<GameObject> stack;
        if (poolObjectDic.TryGetValue(key, out stack))
        {
            return stack.Count > 0;
        }
        return false;
    }
    /// <summary>
    /// 获取池对象
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public GameObject Spawn(string key)
    {
        return OnSpawn(key);
    }
    /// <summary>
    /// 添加池对象
    /// </summary>
    /// <param name="key"></param>
    /// <param name="o"></param>
    public void Add(string key, GameObject o)
    {
        Stack<GameObject> stack;
        if (!poolObjectDic.TryGetValue(key, out stack))
        {
            stack = new Stack<GameObject>(MaxNum);
            poolObjectDic[key] = stack;
        }
        if (stack.Count >= MaxNum)
        {
            Destroy(o);
            Printer.LogError("游戏池达到最大数量: {0}", key);
            return;
        }
        o.SetActive(false);
        Transform item = o.transform;
        item.parent = transform;
        item.localPosition = Vector3.zero;
        item.localEulerAngles = Vector3.zero;
        item.localScale = Vector3.one;
        stack.Push(o);
    }
    /// <summary>
    /// 销毁池对象
    /// </summary>
    /// <param name="key"></param>
    /// <param name="item"></param>
    public void Despawn(string key, GameObject item)
    {
        if (item == null) return;
        OnDespawn(key, item);
    }
    /// <summary>
    /// 清除池
    /// </summary>
    public void Clear()
    {
        foreach (var item in poolObjectDic)
        {
            foreach (var o in item.Value)
            {
                Destroy(o);
            }
        }
        poolObjectDic.Clear();
    }
    protected abstract GameObject OnSpawn(string key);
    protected abstract void OnDespawn(string key, GameObject item);

}
