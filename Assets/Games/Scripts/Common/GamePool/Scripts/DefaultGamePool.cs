﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 从不进资源池，不创建不回收
/// </summary>
public class DefaultGamePool : BasePool
{
    protected override int MaxNum { get { return 1; } }
    protected override GameObject OnSpawn(string key)
    {
        Stack<GameObject> stack;
        if (poolObjectDic.TryGetValue(key, out stack))
        {
            if (stack.Count == 1)
            {
                GameObject ret = stack.Pop();
                ret.SetActive(true);
                return ret;
            }
        }
        return null;
    }
    protected override void OnDespawn(string key, GameObject o)
    {
        Destroy(o);
    }
}
