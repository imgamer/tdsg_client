﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 一次性资源池，只创建不回收
/// </summary>
public class OnceGamePool : BasePool
{
    protected override int MaxNum { get { return 1; } }
    protected override GameObject OnSpawn(string key)
    {
        Stack<GameObject> stack;
        if (poolObjectDic.TryGetValue(key, out stack))
        {
            if (stack.Count == 1)
            {
                GameObject ret = Instantiate(stack.Peek());
                ret.SetActive(true);
                ret.transform.parent = transform;
                return ret;
            }
        }
        return null;
    }
    protected override void OnDespawn(string key, GameObject o)
    {
        Destroy(o);
    }
}
