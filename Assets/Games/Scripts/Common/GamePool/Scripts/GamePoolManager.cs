﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
/// <summary>
/// 资源池管理类
/// </summary>
public class GamePoolManager : MonoBehaviour 
{
#region 初始化
    public static GamePoolManager instance { get; private set; }
    void Awake()
    {
        instance = this;
    }
#endregion

#region 获取游戏池
    private Dictionary<PoolName, Dictionary<PoolType, BasePool>> gamePools = new Dictionary<PoolName, Dictionary<PoolType, BasePool>>();
    private BasePool GetPool(PoolName poolName, PoolType type)
    {
        Dictionary<PoolType, BasePool> pools;
        BasePool pool;
        if (!gamePools.TryGetValue(poolName, out pools))
        {
            GameObject root = new GameObject(poolName.ToString());
            root.SetActive(false);
            Transform trans = root.transform;
            trans.parent = transform;
            trans.localPosition = Vector3.zero;
            trans.localEulerAngles = Vector3.zero;
            trans.localScale = Vector3.one;
            pools = new Dictionary<PoolType, BasePool>();
            gamePools[poolName] = pools;
            pool = Create(trans, type);
            pools[type] = pool;
        }
        else
        {
            if (!pools.TryGetValue(type, out pool))
            {
                Transform trans = transform.FindChild(poolName.ToString());
                pool = Create(trans, type);
                pools[type] = pool;
            }
        }
        return pool;
    }
    private BasePool Create(Transform root, PoolType type)
    {
        GameObject child = new GameObject(type.ToString());
        Transform trans = child.transform;
        trans.parent = root;
        trans.localPosition = Vector3.zero;
        trans.localEulerAngles = Vector3.zero;
        trans.localScale = Vector3.one;

        BasePool pool;
        switch(type)
        {
            case PoolType.Default:
                pool = child.AddComponent<DefaultGamePool>();
                break;
            case PoolType.Once:
                pool = child.AddComponent<OnceGamePool>();
                break;
            case PoolType.Loop:
                pool = child.AddComponent<LoopGamePool>();
                break;
            default:
                pool = child.AddComponent<DefaultGamePool>();
                break;
        }
        return pool;
    }
#endregion

#region 获取游戏池对象的Key
    private string GetKey(string assetName, string objectName)
    {
        return string.Format("{0}_{1}", assetName, objectName);
    }
#endregion

#region 对象操作方法
    public bool Contains(string assetName, string objectName, PoolName poolName, PoolType type)
    {
        BasePool pool = GetPool(poolName, type);
        string key = GetKey(assetName, objectName);
        return pool.Contains(key);
    }
    public void Add(string assetName, string objectName, UnityEngine.Object item, PoolName poolName, PoolType type)
    {
        if (item == null) return;
        BasePool pool = GetPool(poolName, type);
        string key = GetKey(assetName, objectName);

        GameObject temp = Instantiate(item) as GameObject;
        temp.name = objectName;
        if (temp.GetComponent<PoolObject>() == null)
        {
            temp.AddComponent<PoolObject>();
        }
        pool.Add(key, temp);
        Printer.Log("加入游戏池");
    }
    public bool TrySpawn(string assetName, string objectName, PoolName poolName, PoolType type, out GameObject ret)
    {
        BasePool pool = GetPool(poolName, type);
        string key = GetKey(assetName, objectName);
        if (pool.Contains(key))
        {
            ret = pool.Spawn(key);
            if (ret)
            {
                ret.name = objectName;
                PoolObject poolObject = ret.GetComponent<PoolObject>();
                poolObject.Init(key, objectName, poolName, type);
            }
            Printer.Log("游戏池取出");
            return true;
        }
        ret = null;
        return false;
    }
    public GameObject Spawn(string packName, string objectName, PoolName poolName, PoolType type)
    {
        BasePool pool = GetPool(poolName, type);
        string key = GetKey(packName, objectName);
        GameObject ret = pool.Spawn(key);
        if (ret)
        {
            ret.name = objectName;
            PoolObject poolObject = ret.GetComponent<PoolObject>();
            poolObject.Init(key, objectName, poolName, type);
        }
        Printer.Log("游戏池取出");
        return ret;
    }
    public void Despawn(GameObject item)
    {
        if (item == null) return;
        PoolObject[] poolObjects = item.GetComponentsInChildren<PoolObject>(true);
        if (poolObjects.Length <= 0)
        {
            Printer.LogError("对象：{0} 不是由池管理的资源对象", item.name);
            Destroy(item);
            return;
        }
        for (int i = 0; i < poolObjects.Length; i++)
        {
            PoolObject poolObject = poolObjects[i];
            poolObject.Reset();
            BasePool pool = GetPool(poolObject.poolName, poolObject.type);
            pool.Despawn(poolObject.key, poolObject.gameObject);
        }
    }
    public void Clear(PoolName poolName)
    {
        Dictionary<PoolType, BasePool> pools;
        if (gamePools.TryGetValue(poolName, out pools))
        {
            if (pools == null) return;
            foreach (var pool in pools.Values)
            {
                if (pool != null) pool.Clear();
            }
        }
    }
    public void ClearAll()
    {
        foreach (var pools in gamePools.Values)
        {
            if (pools == null) continue;
            foreach (var pool in pools.Values)
            {
                if (pool != null) pool.Clear();
            }
        }
    }
#endregion
}
