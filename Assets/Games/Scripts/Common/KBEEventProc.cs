using System;
public class KBEEventProc
{
	public static KBEEventProc inst = null;
	public KBEEventProc()
	{
		KBEEventProc.inst = this;
		KBEngine.Event.registerOut("onDisableConnect", KBEEventProc.inst, "onDisableConnect");
		KBEngine.Event.registerOut("onLoginFailed", KBEEventProc.inst, "onLoginFailed");
	}

	public void onDisableConnect()
	{
		Printer.Log("KBEEventProc :: onDisableConnect");
        TipManager.instance.ShowOneButtonTip(400, null);
	}

	public void onLoginFailed(UInt16 failedcode)
	{
        TipManager.instance.ShowServerErrTip(failedcode);
	}
	
}