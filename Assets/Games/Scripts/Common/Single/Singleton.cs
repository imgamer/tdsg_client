﻿

public abstract class Singleton<T> where T: Singleton<T>, new(){

    private static T _instance;

    public static T SharedInstance
    {
        get
        {
            if( _instance == null )
            {
                Create();
            }
            return _instance;
        }
    }

    public static void Create()
    {
        if (_instance == null)
        {
            _instance = new T();
            _instance.Init();
        }
    }

    private void Init()
    {
        OnInit();
    }

    protected abstract void OnInit();

}
