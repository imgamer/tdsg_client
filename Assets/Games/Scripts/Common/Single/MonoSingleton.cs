using UnityEngine;

/// <summary>
/// 单例基类
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    void Awake()
    {
        if (!gameObject.tag.Equals(GameUtils.TagSingleton))
        {
            Printer.LogError(string.Format("单例：{0} 不需要手动挂在对象上！", typeof(T).Name));
            Destroy(this);
        }
    }

    private static T _instance;
    public static T instance
    {
        get 
        {
            if (_instance == null)
            {
                Create();
            }
            return _instance;
        }
    }

    /// <summary>
    /// 初始化动作，在创建时会调用
    /// </summary>
    protected abstract void OnInit();

    /// <summary>
    /// 销毁动作，在销毁时会调用
    /// </summary>
    protected abstract void OnUnInit();

    /// <summary>
    /// 创建单例
    /// </summary>
    /// <returns></returns>
    public static T Create()
    {
        Transform root = Main.instance.transform.Find(GameUtils.TagSingleton);
        if (root == null)
        {
            root = new GameObject(GameUtils.TagSingleton).transform;
            root.tag = GameUtils.TagSingleton;
            root.parent = Main.instance.transform;
        }

        _instance = root.gameObject.GetComponent<T>();
        if (_instance == null)
        {
            _instance = root.gameObject.AddComponent<T>();
        }
       
        _instance.Init();
        return _instance;
    }

    /// <summary>
    /// 注销单例
    /// </summary>
    public static void Destroy()
    {
        if (_instance == null) return;
        _instance.UnInit();
    }

    private bool inited = false;
    /// <summary>
    /// 初始化单例
    /// </summary>
    private void Init()
    {
        if (inited) return;
        inited = true;
        OnInit();
    }
    /// <summary>
    /// 注销单例
    /// </summary>
    private void UnInit()
    {
        if (!inited) return;
        inited = false;
        OnUnInit();
        _instance = null;
    }

}
