﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public class Define
{
	//由于经常用到，这里做个定义
	public const BindingFlags PUBLIC_AND_NONPUBLIC = BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public;

    // entity状态，必须要与服务端定义同步
    public const SByte ENTITY_STATE_UNKNOW = -1;	// 未知
    public const SByte ENTITY_STATE_FREE = 0;		// 自由
    public const SByte ENTITY_STATE_DEAD = 1;		// 死亡
    public const SByte ENTITY_STATE_REST = 2;		// 休息
    public const SByte ENTITY_STATE_FIGHT = 3;		// 战斗

    // 装备栏部位定义（EquipmentBag）
    public const byte EB_ERROR_LOCATION = 0;		// 无效部位
    public const byte EB_WEAPON = 1;		// 武器
    public const byte EB_VICE_HAND = 2;		// 副手
    public const byte EB_HELMET = 3;		// 头盔
    public const byte EB_ORNAMENT = 4;		// 饰品
    public const byte EB_ARMOR = 5;		    // 衣甲
    public const byte EB_RING = 6;		    // 戒指
    public const byte EB_BOOT = 7;		    // 靴子
    public const byte EB_NECKLACE = 8;      // 项链
    public const byte EB_TALISMAN = 9;		// 法宝
    public const byte EB_BADGE = 10;		// 徽章

    //任务分组
    public const byte QUEST_GROUP_GENERAL = 1;   // 常规任务
    public const byte QUEST_GROUP_MAINLINE = 2;  // 主线剧情
    public const byte QUEST_GROUP_BRANCH = 3;    // 支线剧情
    public const byte QUEST_GROUP_ACTIVITY = 4;  // 活动任务

    //任务类型定义
    public const byte QUEST_TYPE_MAILLINE = 1;	// 主线任务
    public const byte QUEST_TYPE_BRANCH = 2;	// 支线任务
    public const byte QUEST_TYPE_DAILY = 3;	    // 日常任务
    public const byte QUEST_TYPE_SHIMEN = 4;	// 师门任务
    public const byte QUEST_TYPE_CHAIN = 5;	    // 任务链
    public const byte QUEST_TYPE_TREASUREMAP = 6;   // 宝图任务
    public const byte QUEST_TYPE_ACTIVITY_PATA = 9;   // 爬塔活动任务
    public const byte QUEST_TYPE_ACTIVITY_MOJIN = 10;   // 摸金活动任务
    public const byte QUEST_TYPE_ACTIVITY_LIEHUN = 11;   // 猎魂活动任务
    


    //任务目标类型定义
    public const byte TASK_TYPE_COLLECTION = 1;  // 收集物品
    public const byte TASK_TYPE_SEARCHING = 2;   // 寻找物品
    public const byte TASK_TYPE_SHIMEN_COLLECTION = 3;   // 师门专属任务-采集
    public const byte TASK_TYPE_CATCH_PET = 4;   // 捕捉宠物
    public const byte TASK_TYPE_DELIVER = 5;     // 送信
    public const byte TASK_TYPE_SUBDUING = 6;    // 收服
    public const byte TASK_TYPE_DEFEAT = 7;      // 战斗
    public const byte TASK_TYPE_PATROL = 8;      // 巡逻
    public const byte TASK_TYPE_SCHOOL_DEMONSTRATION = 9;   // 门派示威
    public const byte TASK_TYPE_SUPPORT_SCHOOL = 10;   // 支援同门
    public const byte TASK_TYPE_BOGY_ESCAPE = 11;   // 妖魔逃跑

    // 师门任务定义
    public const int SHIMEN_ROUND_AMOUNT_PER_RING = 10;	    // 师门每一环的次数
    public const int SHIMEN_HUGE_REWARD_RING_LIMIT = 20;	// 每日师门高额奖励次数
    public const int SHIMEN_DAILY_MAX_ROUND = 100;		    // 每日师门最多可做次数
    public const int SHIMEN_REQUIRE_LEVEL = 20;			    // 师门任务要求等级

    // entity 战斗场景中的状态，必须要与服务端定义相同，但不一定同步
    public const SByte ENTITY_STATE_FIGHT_FREE = 30;	// 空闲状态
    public const SByte ENTITY_STATE_FIGHT_READYING = 31;		// 准备中
    public const SByte ENTITY_STATE_FIGHT_READYED = 32;		// 准备好
    public const SByte ENTITY_STATE_FIGHT_CAST = 33;		// 出手
    public const SByte ENTITY_STATE_FIGHT_DEAD = 34;		// 战斗中死亡
    public const SByte ENTITY_STATE_FIGHT_WOUNDED = 35;		// 战斗中重伤（即血量为0但不会销毁能被加血和复活的）

    //entity 行动值
    public const uint DEFAULT_ACTION_VALUE = 500;
    public const uint MAX_ACTION_VALUE = 1000; 


    // npc talk
    public const Int32 INVALID_TALK_KEY = 0;		// 无效对话key

    // 队伍相关状态
    public const Byte TEAM_MEMBER_PENDING = 1;				// 队员未决状态
    public const Byte TEAM_MEMBER_OFFLINE = 2;				// 队员离线状态
    public const Byte TEAM_MEMBER_SEPARATING = 3;			// 队员暂离状态
    public const Byte TEAM_MEMBER_FOLLOWING = 4;			// 队员跟随状态
    public const Byte TEAM_MEMBER_LEADING = 5;				// 队员领导状态

    // 组队状态
    public const Byte TEAM_STATUS_NONE = 1;					// 没有组队
    public const Byte TEAM_STATUS_CAPTAIN = 2;				// 队长
    public const Byte TEAM_STATUS_MEMBER = 3;				// 普通队员

    // 队伍人数
    public const Byte TEAM_MAX_MEMBER = 3;					// 最多队伍成员数量

    // 离队原因定义
    public const Byte LEAVE_TEAM_KICKOUT = 1;				// 被剔除
    public const Byte LEAVE_TEAM_INITIATIVE = 2;			// 主动离开
    public const Byte LEAVE_TEAM_DISBAND = 3;				// 队伍解散
    public const Byte LEAVE_TEAM_LEAVE_MULTSPACE = 4;		// 离开多人副本场景


    // 帮会相关状态
    public const Byte TONG_MEMBER_PENDING = 1;				// 会员未决状态
    public const Byte TONG_MEMBER_ONLINE = 2;				// 会员在线状态
    public const Byte TONG_MEMBER_OFFLINE = 3;			    // 会员离线状态

    // 帮会人数
    public const Byte TONG_TOLERANCE_INIT = 50;				// 帮会初始容量
    public const Byte TONG_TOLERANCE_STEP = 5;				// 帮会每级容量
    
    public const int TONG_CREATE_NEED_MONEY = 100000;		// 创建帮会需要的游戏币

    // 离会原因定义
    public const Byte LEAVE_TONG_KICKOUT = 1;				// 被剔除
    public const Byte LEAVE_TONG_INITIATIVE = 2;			// 主动离开
    public const Byte LEAVE_TONG_DISBAND = 3;				// 帮会解散

    // 帮会职务定义
    public const Byte TONG_DUTY_CAPTAIN		    	= 1;     // 帮主
    public const Byte TONG_DUTY_CO_CAPTAIN	    	= 2;     // 副帮主
    public const Byte TONG_DUTY_OFFICER_QINGLONG	= 3;     // 青龙堂主
    public const Byte TONG_DUTY_OFFICER_BAIHU		= 4;     // 白虎堂主
    public const Byte TONG_DUTY_OFFICER_ZHUQUE	    = 5;     // 朱雀堂主
    public const Byte TONG_DUTY_OFFICER_XUANWU	    = 6;     // 玄武堂主
    public const Byte TONG_DUTY_MAINSTAY_QINGLONG	= 7;     // 青龙堂精英
    public const Byte TONG_DUTY_MAINSTAY_BAIHU  	= 8;     // 白虎堂精英
    public const Byte TONG_DUTY_MAINSTAY_ZHUQUE	    = 9;     // 朱雀堂精英
    public const Byte TONG_DUTY_MAINSTAY_XUANWU	    = 10;    // 玄武堂精英
    public const Byte TONG_DUTY_MAINSTAY			= 11;    // 精英
    public const Byte TONG_DUTY_MEMBER			    = 12;    // 帮众

    //职务数量定义
    public static readonly Dictionary<Byte, int> TONG_DUTY_AMOUNT = new Dictionary<Byte, int> { 
        {TONG_DUTY_CAPTAIN, 1},
	    {TONG_DUTY_CO_CAPTAIN, 1},
	    {TONG_DUTY_OFFICER_QINGLONG, 1},
	    {TONG_DUTY_OFFICER_BAIHU, 1},
	    {TONG_DUTY_OFFICER_ZHUQUE, 1},
	    {TONG_DUTY_OFFICER_XUANWU, 1},
	    {TONG_DUTY_MAINSTAY_QINGLONG, 10},
	    {TONG_DUTY_MAINSTAY_BAIHU, 10},
	    {TONG_DUTY_MAINSTAY_ZHUQUE, 10},
	    {TONG_DUTY_MAINSTAY_XUANWU, 10},
	    {TONG_DUTY_MAINSTAY, 20},
	    {TONG_DUTY_MEMBER, -1},
    };

    //任命权限
    public static readonly Dictionary<Byte, Byte[]> TONG_DUTY_PRIVILEGES = new Dictionary<Byte, Byte[]> { 
        {TONG_DUTY_CAPTAIN, new Byte[]{
            TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN, TONG_DUTY_OFFICER_QINGLONG, 
		    TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, TONG_DUTY_OFFICER_XUANWU, 
		    TONG_DUTY_MAINSTAY_QINGLONG, TONG_DUTY_MAINSTAY_BAIHU, TONG_DUTY_MAINSTAY_ZHUQUE,
		    TONG_DUTY_MAINSTAY_XUANWU, TONG_DUTY_MAINSTAY, TONG_DUTY_MEMBER}
            },
        {TONG_DUTY_CO_CAPTAIN, new Byte[]{
            TONG_DUTY_OFFICER_QINGLONG, TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, 
		    TONG_DUTY_OFFICER_XUANWU, TONG_DUTY_MAINSTAY_QINGLONG, TONG_DUTY_MAINSTAY_BAIHU, 
		    TONG_DUTY_MAINSTAY_ZHUQUE, TONG_DUTY_MAINSTAY_XUANWU, TONG_DUTY_MAINSTAY, TONG_DUTY_MEMBER,}
            },
        {TONG_DUTY_OFFICER_QINGLONG, new Byte[]{TONG_DUTY_MAINSTAY_QINGLONG, TONG_DUTY_MEMBER}},
        {TONG_DUTY_OFFICER_BAIHU, new Byte[]{TONG_DUTY_MAINSTAY_BAIHU, TONG_DUTY_MEMBER}},
        {TONG_DUTY_OFFICER_ZHUQUE, new Byte[]{TONG_DUTY_MAINSTAY_ZHUQUE, TONG_DUTY_MEMBER}},
        {TONG_DUTY_OFFICER_XUANWU, new Byte[]{TONG_DUTY_MAINSTAY_XUANWU, TONG_DUTY_MEMBER}},
        {TONG_DUTY_MAINSTAY_QINGLONG, new Byte[]{}},
        {TONG_DUTY_MAINSTAY_BAIHU, new Byte[]{}},
        {TONG_DUTY_MAINSTAY_ZHUQUE, new Byte[]{}},
        {TONG_DUTY_MAINSTAY_XUANWU, new Byte[]{}},
        {TONG_DUTY_MAINSTAY, new Byte[]{}},
        {TONG_DUTY_MEMBER, new Byte[]{}},
    };

    // ----------------------------------------------
    // 帮会功能权限定义
    // ----------------------------------------------
    // 帮会升级
    public static readonly Byte[] TONG_PRIVILEGE_UPGRADE = new Byte[] {TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN};
    // 逐出帮众
    public static readonly Byte[] TONG_PRIVILEGE_KICKOUT_MEMBER = new Byte[] {
	    TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN, TONG_DUTY_OFFICER_QINGLONG, 
	    TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, TONG_DUTY_OFFICER_XUANWU,
	    };
    // 编辑公告
    public static readonly Byte[] TONG_PRIVILEGE_EDIT_NOTICE = new Byte[] {TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN};
    // 帮会福利
    public static readonly Byte[] TONG_PRIVILEGE_GAIN_REWARD = new Byte[] {
	    TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN, TONG_DUTY_OFFICER_QINGLONG, 
	    TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, TONG_DUTY_OFFICER_XUANWU, 
	    TONG_DUTY_MAINSTAY_QINGLONG, TONG_DUTY_MAINSTAY_BAIHU, TONG_DUTY_MAINSTAY_ZHUQUE,
	    TONG_DUTY_MAINSTAY_XUANWU, TONG_DUTY_MAINSTAY, TONG_DUTY_MEMBER,
	    };
    // 群发信息
    public static readonly Byte[] TONG_PRIVILEGE_BROADCAST_MESSAGE = new Byte[] {TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN};
    // 快捷招人
    public static readonly Byte[] TONG_PRIVILEGE_BROADCAST_INVITE = new Byte[] {TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN};
    // 帮会求助
    public static readonly Byte[] TONG_PRIVILEGE_TONG_HELP = new Byte[] {
	    TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN, TONG_DUTY_OFFICER_QINGLONG, 
	    TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, TONG_DUTY_OFFICER_XUANWU, 
	    TONG_DUTY_MAINSTAY_QINGLONG, TONG_DUTY_MAINSTAY_BAIHU, TONG_DUTY_MAINSTAY_ZHUQUE,
	    TONG_DUTY_MAINSTAY_XUANWU, TONG_DUTY_MAINSTAY, TONG_DUTY_MEMBER,
	    };
    // 批准入会
    public static readonly Byte[] TONG_PRIVILEGE_ALLOW_JOIN_REQUEST = new Byte[] {
	    TONG_DUTY_CAPTAIN, TONG_DUTY_CO_CAPTAIN, TONG_DUTY_OFFICER_QINGLONG, 
	    TONG_DUTY_OFFICER_BAIHU, TONG_DUTY_OFFICER_ZHUQUE, TONG_DUTY_OFFICER_XUANWU,
	    };

    //宠物类型
    public const Byte PET_TYPE_WILD = 1;		            // 野生
    public const Byte PET_TYPE_FRIENDLY = 2;		        // 宝宝
    public const Byte PET_TYPE_SUPER = 3;		            // 变异
    public const Byte PET_TYPE_MYTHICAL = 4;		        // 神兽

    public const Byte SERVER_STATE_FREE = 1;		        // 空闲
    public const Byte SERVER_STATE_BUSY = 2;		        // 忙碌
    public const Byte SERVER_STATE_MAINTAIN = 3;		    // 维护

    public const UInt32 ROLE_TYPE_DIANCANG = 1;				//  典苍派
    public const UInt32 ROLE_TYPE_JIUHUASHAN = 2;			//  九华山
    public const UInt32 ROLE_TYPE_XUANQINGGONG = 3;			//  玄清宫
    public const UInt32 ROLE_TYPE_XIAOYAOGONG = 4;			//  逍遥宫
    public const UInt32 ROLE_TYPE_ZIDIANMEN = 5;			//  紫电门
    public const UInt32 ROLE_TYPE_TIANXINGE = 6;			//  天心阁

    //技能类型
    public const int SKILL_TYPE_INITIATIVE = 1;		        // 主动技能
    public const int SKILL_TYPE_PASSIVE = 2;		        // 被动技能
    public const int SKILL_TYPE_BUFF = 3;		            // buff技能

    //技能目标类型定义
    public const int SKILL_TARGET_ONESELF = 100;		                // 自己
    public const int SKILL_TARGET_FRIEND = 101;		            // 友方
    public const int SKILL_TARGET_EXCEPTONESELFFRIEND = 102;		// 除自己外的友方
    public const int SKILL_TARGET_ENEMY = 201;		                // 敌方
    public const int SKILL_TARGET_STRAIGHTATTACKENEMY = 202;		// 敌方单位直向攻击
    public const int SKILL_TARGET_TRANSVERSEATTACKENEMY = 203;		// 敌方单位横向攻击
    public const int SKILL_TARGET_FRONTBEHINDATTACKENEMY = 204;		// 敌方单位身前身后攻击

    //技能目标是否包含重伤对象
    public const int SKILL_TARGET_NOT_CONTAIN_HEAVILY_DAMAGE = 0;		// 不包含
    public const int SKILL_TARGET_CONTAIN_HEAVILY_DAMAGE = 1;		// 包含


    // 一次技能使用流程所产生的所有技能结果时机定义
    public const int SKILL_RESULT_BEFORE_USE = 0;				// 使用前触发的技能结果
    public const int SKILL_RESULT_IN_USE = 1;					// 使用中的技能结果
    public const int SKILL_RESULT_TARGET_DAMAGE = 2;					// 目标受到伤害时的技能效果
    public const int SKILL_RESULT_TARGET_DEAD = 3;					// 目标死亡时的技能效果

    //技能伤害类型
    public const int SKILL_DAMAGE_TYPE_ORDINARY = 1;				// 常规命中
    public const int SKILL_DAMAGE_TYPE_DODGE = 2;					// 闪避
    public const int SKILL_DAMAGE_TYPE_CRIT = 3;					// 暴击


    //战斗方
    public const int FIGHT_SIDE_OUR = 1;		// 我方
    public const int FIGHT_SIDE_ENEMY = 2;		// 敌方

    public const int COMMON_CURRENCY_TYPE_CHANGE = 1;
    public const int COMMON_LEVEL_UP = 2;

    //领主
    //玩家身份
    public const SByte LORD_IDENTITY_TYPE_FREE = 1;			//  自由民
    public const SByte LORD_IDENTITY_TYPE_LORD = 2;			//  领主
    public const SByte LORD_IDENTITY_TYPE_SLAVE = 3;		//  奴隶

    //奴隶讨好领主方式
    public const SByte LORD_PLEASE_LORD_TYPE_SEND_FLOWER = 1;		//  送花
    public const SByte LORD_PLEASE_LORD_TYPE_POUR_TEA = 2;			//  倒茶

    //俘获结果 1 成功  0 失败
    public const SByte LORD_CAPTURE_RESULT_SUCCESS = 1;
    public const SByte LORD_CAPTURE_RESULT_FAILURE = 0;

    //玩家能同时拥有的奴隶数目
    public const Int32 LORD_SLAVE_COUNT_LIMIT = 4;
    public const Int32 STATUSID_LORD_SLAVE_COUNT_LIMIT = 2107; //拥有奴隶数量上限

    //购买俘获次数需要花费的元宝数
    public const Int32 LORD_BUY_CAPTURE_COUNT_NEED_INGOT = 50;
    public const Int32 STATUSID_LORD_BUY_CAPTURE_COUNT_NEED_INGOT = 2118;

    //结束俘获冷却时间需要花费的元宝数
    public const Int32 LORD_OVER_CAPTURE_TIME_CD_NEED_INGOT = 50;
    public const Int32 STATUSID_LORD_OVER_CAPTURE_TIME_CD_NEED_INGOT = 2119;

    //购买压榨次数需要花费的元宝数
    public const Int32 LORD_BUY_SQUEEZE_COUNT_NEED_INGOT = 50;
    public const Int32 STATUSID_LORD_BUY_SQUEEZE_COUNT_NEED_INGOT = 2120;


    //聊天系统: 频道定义
    public const int CHAT_CHANNEL_NONE 		= 0; //  None
    public const int CHAT_CHANNEL_SYSTEM  	= 1; //  系统
    public const int CHAT_CHANNEL_WORLD  	= 2; //  世界
    public const int CHAT_CHANNEL_CURRENT  	= 3; //  当前
    public const int CHAT_CHANNEL_SCHOOL  	= 4; //  门派
    public const int CHAT_CHANNEL_FACTION  	= 5; //  帮派
    public const int CHAT_CHANNEL_TEAM  	= 6; //  队伍
    public const int CHAT_CHANNEL_PARTY     = 7; //  组队
    public const int CHAT_CHANNEL_NEAR      = 8; //  附近
    public const int CHAT_CHANNEL_WHISPER   = 9; //  私聊

    //敏感词过滤器

    /**
     * 匹配规则类型: 最小匹配规则, 最大匹配规则
     * 假设：
     * 检验文本有：他妈的连在一起的这几个字
     * 敏感词有： 他妈 他妈的
     * 最小匹配规则匹配出来的是 ： 他妈
     * 最大匹配规则匹配出来的是 ： 他妈的
     */ 
    public const SByte SENSITIVE_MATCH_TYPE_MIN = 1; //最小匹配规则
    public const SByte SENSITIVE_MATCH_TYPE_MAX = 2; //最大匹配规则

    //过滤器类型
    public const int SENSITIVE_FILTER_TYPE_DEFAULT  	= 0; // 默认的过滤器
    public const int SENSITIVE_FILTER_TYPE_CHAT  	= 1; // 聊天信息过滤器
    public const int SENSITIVE_FILTER_TYPE_AVATAR_NAME  	= 2; // 角色名过滤器

   /**
    * 排行榜
    */ 
    // 排行榜个人信息类型
    public const UInt16 RANK_TYPE_BASEINFO_COMPREHENSIVE_STRENGTH  = 101; // 综合实力
    public const UInt16 RANK_TYPE_BASEINFO_LEVEL  = 102; // 等级
    public const UInt16 RANK_TYPE_BASEINFO_AVATAR  = 103; // 人物
    public const UInt16 RANK_TYPE_BASEINFO_PET  = 104; // 宠物
   
    // 排行榜门派类型
    public const UInt16 RANK_TYPE_SCHOOL_DIANCANG = 201; // 典苍派
    public const UInt16 RANK_TYPE_SCHOOL_JIUHUASHAN = 202; // 九华山
    public const UInt16 RANK_TYPE_SCHOOL_XUANQINGGONG = 203; // 玄清宫
    public const UInt16 RANK_TYPE_SCHOOL_XIAOYAOGONG = 204; // 逍遥宫
    public const UInt16 RANK_TYPE_SCHOOL_ZIDIANMEN = 205; // 紫电门
    public const UInt16 RANK_TYPE_SCHOOL_TIANXINGE = 206; // 天心阁
    
    public const UInt16 RANK_BEGIN_INDEX = 0;   // 排行榜默认请求开始的索引
    public const UInt16 RANK_DATAS_COUNT = 100; // 排行榜默认数据每次请求的数量
    public const UInt16 RANK_DATAS_REFRESH= 1; // 排行榜刷新时间

	//定义宠物资质成长值列表，顺序与服务器的定义保持一致，以便通过索引获取正确的成长资质属性
	public static readonly List<string> PET_FACTOR_COLLECTION = new List<string>{
		"growthFactor", "physicsPowerFactor", "physicsDefenseFactor",
		"magicPowerFactor", "magicDefenseFactor", "hpFactor",
	};

    public static readonly List<int> PET_CACTOR_TO_CONSUME = new List<int> { 0, 31500001, 31500002, 31500003, 31500004, 31500005 };
    //玩家
    public const int AVATAR_UPPER_LEVEL = 80; //玩家最高等级

    //宠物
    public const int PET_MAX_STAGE = 4; // 最高阶次
    public const int PET_UPPER_LEVEL_OVERFLOW = 5; // 可超出角色的等级
    public const int PET_FIGHT_NOT_COMBINE = 4203; //出战宠物不可合成
    public const int PET_FREE = 4204; //是否放生宠物
    public const int PET_FIGHT_NOT_FREE = 4205; //出战宠物不能放生

    public const int ARENA_INGOT_NOT_ENOUGH = 2503; //元宝不够，挑战次数不能购买
    public const int ARENA_CHALLENGE_COUNT_NOT_ENOUGH=2504; //挑战次数已经用完，不能挑战
    public const int ARENA_CHALLENGE_CD=2505; //挑战冷却中，不能挑战
    public const int ARENA_OVER_CHALLENGE_CD=2506; //消除冷却需要花费50元宝
    public const int ARENA_BUY_CHALLENGE_COUNT = 2507;  //购买挑战次数

    //宝石合成
    public const int GEM_COMBIN_NUM = 10000; //宝石合成银币消耗基本单位
    public const int GEM_COUNT_NOT_ENOUGH = 2601; //宝石个数不足
    public const int SILVER_NOT_ENOUGH = 2603;//银币不足
    public const int GEM_LEVEL_MAX = 2604; //宝石已经最高等级

    //装备修理
    public const int EQUIPMENT_DURABILITY_MAX = 2801; //装备耐久度上限

    //装备洗练
    public const int EQUIPMENT_WASHSKILL_NOT_ALLOW = 2806; //该装备不允许洗练
    public const int EQUIPMENT_WASHS_NOT = 3301; // 不允许属性洗练

    public const int EQUIPMENT_NOT_SELECT = 2805; //未选择装备
    public const int CONSUME_MATERIAL_NOT_ENOUGH = 2804; //{0}材料不足

    //宝石镶嵌
//    public const int GEM_INLAY_SUCCESS = 2901;  //宝石镶嵌成功
//    public const int GEM_REPLACE_SUCCESS = 2902;  //宝石替换成功
       public const int GEM_INLAY_BEYONDLEVEL = 2903; //宝石等级太高

    //升星
    public const int EQUIPMENT_STAR_LEVEL_MAX = 3101; //装备星级已最高 

    public const int EQUIPMENT_QUALITY_MAX = 3401; //该装备品质已达上限
    public const int EQUIPMENT_BUILD_SUCCESS=3402; //装备打造成功
    public const int EQUIPMENT_BUILD_FAILURE=3403; //装备打造失败


    //爬塔
    public const string PATA_BOX_ICON = "Consumables_1105"; //爬塔宝箱图标


    //奖励类型
    public const int REWARD_TYPE_INGOT = 1;// 元宝
    public const int REWARD_TYPE_MEDAL = 2;// 勋章
    public const int REWARD_TYPE_GOLD = 3;// 金币
    public const int REWARD_TYPE_ITEM = 4;// 道具
    public const int REWARD_TYPE_AVATAREXP = 5;// 玩家经验

    //邮件是否已领取
    public const sbyte EMAIL_NOT_RECEIVE = 0; // 未领取
    public const sbyte EMAIL_HAS_RECEIVE = 1; // 已领取

    //多人副本离开类型
     public const Int16 MULTIPLE_FIGHT_COPY_LEAVE_TYPE_INITIATIVE = 1; // 主动离开
     public const Int16 MULTIPLE_FIGHT_COPY_LEAVE_TYPE_PASSIVE = 2; // 被动离开
     public const Int32 TEAMCOPY_READY_TIME = 40; //准备时间

}
