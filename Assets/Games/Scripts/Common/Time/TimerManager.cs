﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 时间管理类
/// </summary>
public class TimerManager : MonoSingleton<TimerManager>
{
    
    protected override void OnInit()
    {
        _timerDict.Clear();
        _timerid = 0;
    }

    protected override void OnUnInit()
    {
        _timerDict.Clear();
        _timerid = 0;
    }

    private ulong CreatTimerID()
    {
        if (_timerid > 1.8e+19)
        {
            _timerid = 0;
        }
        _timerid++;
        return _timerid;
    }

    void Update()
    {
        List<ulong> timerList = new List<ulong>(_timerDict.Keys);
        for (int i = 0; i < timerList.Count; i++)
        {
            ulong id = timerList[i];
            BaseTimer timer;
            if (_timerDict.TryGetValue(id, out timer))
            {
                if (timer.Verify())
                {
                    RemoveTimer(id);
                }
            }
        }
    }

    public const ulong InvalidTimerID = 0;
    private Dictionary<ulong, BaseTimer> _timerDict = new Dictionary<ulong, BaseTimer>();
    private ulong _timerid = InvalidTimerID;
    /// <summary>
    /// 添加有限循环时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="maxTick"></param>
    /// <returns></returns>
    public ulong AddScaleTimer(float time, Action<int> onUpdate, Action onComplete, int maxTick = 1)
    {
        if (maxTick < 0)
        {
            Printer.LogError("有限循环定时器，最大定时次数必须大于等于0！");
            return InvalidTimerID;
        }
        BaseTimer t = new ScaleTimer(time, maxTick, onUpdate, onComplete);
        _timerDict.Add(CreatTimerID(), t);
        return _timerid;
    }
    /// <summary>
    /// 添加无限循环时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public ulong AddLoopScaleTimer(float time, Action<int> onUpdate)
    {
        if (time <= 0)
        {
            Printer.LogError("无限循环定时器，时间周期必须大于0！");
            return InvalidTimerID;
        }
        BaseTimer t = new ScaleTimer(time, -1, onUpdate, null);
        _timerDict.Add(CreatTimerID(), t);
        return _timerid;
    }
    /// <summary>
    /// 添加有限循环真实时间定时器（不受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="maxTick"></param>
    /// <returns></returns>
    public ulong AddRealTimer(float time, Action<int> onUpdate, Action onComplete, int maxTick = 1)
    {
        if (maxTick < 0)
        {
            Printer.LogError("有限循环定时器，最大定时次数必须大于等于0！");
            return InvalidTimerID;
        }
        BaseTimer t = new RealTimer(time, maxTick, onUpdate, onComplete);
        _timerDict.Add(CreatTimerID(), t);
        return _timerid;
    }
    /// <summary>
    /// 添加无限循环真实时间定时器（不受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public ulong AddLoopRealTimer(float time, Action<int> onUpdate)
    {
        if(time <= 0)
        {
            Printer.LogError("无限循环定时器，时间周期必须大于0！");
            return InvalidTimerID;
        }
        BaseTimer t = new RealTimer(time, -1, onUpdate, null);
        _timerDict.Add(CreatTimerID(), t);
        return _timerid;
    }
    /// <summary>
    /// 开始定时
    /// </summary>
    /// <param name="timerID"></param>
    public void StartTimer(ulong timerID)
    {
        BaseTimer timer = null;
        if(_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.Start();
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 暂停定时
    /// </summary>
    /// <param name="timerID"></param>
    public void PauseTimer(ulong timerID)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.Pause();
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 继续定时
    /// </summary>
    /// <param name="timerID"></param>
    public void ContinueTimer(ulong timerID)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.Continue();
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 重新定时
    /// </summary>
    /// <param name="timerID"></param>
    public void RestartTimer(ulong timerID)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.Restart();
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 重置到达时间
    /// </summary>
    /// <param name="timerID"></param>
    /// <param name="triggerTime"></param>
    public void ResetTriggerTime(ulong timerID, float triggerTime)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.ResetTriggerTime(triggerTime);
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 重置当前定时次数
    /// </summary>
    /// <param name="timerID"></param>
    public void ResetCurTick(ulong timerID)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.ResetCurTick();
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 重置最大定时次数
    /// </summary>
    /// <param name="timerID"></param>
    /// <param name="maxTick"></param>
    public void ResetMaxTick(ulong timerID, int maxTick)
    {
        BaseTimer timer = null;
        if (_timerDict.TryGetValue(timerID, out timer))
        {
            if (timer != null)
            {
                timer.ResetMaxTick(maxTick);
            }
        }
        else
        {
            Printer.LogError("不存在 TimeID:{0}", timerID);
        }
    }
    /// <summary>
    /// 移除定时
    /// </summary>
    /// <param name="timerID"></param>
    public void RemoveTimer(ulong timerID)
    {
        _timerDict.Remove(timerID);
    }

}
