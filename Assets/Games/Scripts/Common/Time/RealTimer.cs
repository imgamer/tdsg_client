﻿using System;
using UnityEngine;
/// <summary>
/// 不受TimeScale影响的时间类
/// </summary>
public class RealTimer : BaseTimer
{
    public RealTimer(float second, int maxTick, Action<int> onUpdate, Action onComplete) : base(second, maxTick, onUpdate, onComplete) { }

    protected override float NowTime
    {
        get { return Time.realtimeSinceStartup; }
    }
}
