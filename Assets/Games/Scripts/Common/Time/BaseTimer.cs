﻿using System;
/// <summary>
/// 定时器基类
/// </summary>
public abstract class BaseTimer
{
    //是否在计时
    private bool _tricking;
    //时间跨度（定时时长）
    private float _timespan;
    //当前定时次数
    private int _curTick = 0;
    //最大定时次数(小于0无限循环，等于0立即执行，大于0有限循环)
    private int _maxTick = 0;
    //Tick回调
    private Action<int> _onUpdate;
    //完成回调
    private Action _onComplete;

    //当前累计时间
    private float _deltaTime;
    //过去记录的时间
    private float _pastTime;

    public BaseTimer(float second, int maxTick, Action<int> onUpdate, Action onComplete)
    {
        _tricking = false;
        _timespan = second;
        _maxTick = maxTick;
        _onUpdate = onUpdate;
        _onComplete = onComplete;
    }

    /// <summary>
    /// 现在的时间
    /// </summary>
    protected abstract float NowTime { get; }

    /// <summary>
    /// 校验是否到达
    /// </summary>
    public bool Verify()
    {
        //暂停状态
        if (!_tricking) return false;
        
        if (_maxTick == 0)
        {
            //最大定时次数小于等于0，直接触发
            _tricking = false;
            if (_onUpdate != null) _onUpdate(0);
            if (_onComplete != null) _onComplete();
            return true;
        }
        else
        {
            _deltaTime = NowTime - _pastTime;
            if (_deltaTime > _timespan)
            {
                _pastTime = NowTime;
                _curTick++;
                if (_maxTick >= 1 && _curTick >= _maxTick)
                {
                    _tricking = false;
                    if (_onUpdate != null) _onUpdate(_curTick);
                    if (_onComplete != null) _onComplete();
                    return true;
                }
                else
                {
                    if (_onUpdate != null) _onUpdate(_curTick);
                }
            }
            return false;
        }
    }

    /// <summary>
    /// 开始
    /// </summary>
    public void Start()
    {
        if (_tricking) return;
        _tricking = true;
        _deltaTime = 0;
        _curTick = 0;
        _pastTime = NowTime;
    }

    /// <summary>
    /// 暂停
    /// </summary>
    public void Pause()
    {
        _tricking = false;
    }

    /// <summary>
    /// 继续
    /// </summary>
    public void Continue()
    {
        if (_tricking) return;
        _tricking = true;
        _pastTime = NowTime;
        _pastTime -= _deltaTime; 
    }

    /// <summary>
    /// 重新开始
    /// </summary>
    public void Restart()
    {
        _tricking = true;
        _deltaTime = 0;
        _curTick = 0;
        _pastTime = NowTime;
    }

    /// <summary>
    /// 重置到达时间
    /// </summary>
    /// <param name="second">Trigger Time</param>
    public void ResetTriggerTime(float second)
    {
        _timespan = second;
    }

    /// <summary>
    /// 重置当前定时次数
    /// </summary>
    public void ResetCurTick()
    {
        _curTick = 0;
    }

    /// <summary>
    /// 重置最大定时次数
    /// </summary>
    /// <param name="maxTick"></param>
    public void ResetMaxTick(int maxTick)
    {
        _maxTick = maxTick;
    }

}
