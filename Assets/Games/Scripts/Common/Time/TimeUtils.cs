﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 时间工具
/// </summary>
public class TimeUtils
{
    #region 格式化日期字符串
    public static readonly string DateFormat1 = "yyyy-MM-dd";
    public static readonly string DateFormat2 = "yyyy-MM-dd hh:mm:ss";
    public static readonly string DateFormat3 = "yyyy年MM月dd日";
    public static readonly string DateFormat4 = "yyyy年MM月dd日 hh时mm分ss秒";
    #endregion

    #region 格式化时间字符串
    public static readonly string[] TimeFormat3v = new string[] 
    {
        "00:{0:D2}", "{0:D2}:{1:D2}", "{0:D2}:{1:D2}"
    };
    public static readonly string[] TimeFormat4v = new string[] 
    {
        "{0}秒", "{0}分{1}秒", "{0}时{1}分", "{0}天{1}时"
    };
    #endregion

    private static DateTime _initTimeLocal = new DateTime(1970, 1, 1, 8, 0, 0, 0);
    private static double _syncTimeServer = 0;
    private static float _syncTimeLocal;

    #region 无效时间ID
    public static ulong InvalidTimerID
    {
        get { return TimerManager.InvalidTimerID; }
    }
    #endregion

    #region 同步时间相关
    public static float ElapsedSinceSync
    {
        get
        {
            return Time.realtimeSinceStartup - _syncTimeLocal;
        }
    }

    public static bool IsSynchronous()
    {
        return Math.Abs(_syncTimeServer - 0f) > 0.001f;
    }

    /// <summary>
    /// 与服务器时间同步
    /// </summary>
    /// <param name="time"></param>
    public static void SyncFromServer(double time)
    {
        _syncTimeServer = time + 0.1;			//粗略估计其他延时是0.1秒
        _syncTimeLocal = Time.realtimeSinceStartup;
    }
    /// <summary>
    /// 当前时间
    /// </summary>
    /// <returns></returns>
    public static double Now()
    {
        return _syncTimeServer + ElapsedSinceSync;
    }
    /// <summary>
    /// 当前DateTime
    /// </summary>
    /// <returns></returns>
    public static DateTime NowDateTime()
    {
        return _initTimeLocal.AddSeconds(Now());
    }
    /// <summary>
    /// 服务器时间对应的DateTime
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static DateTime ServerDateTime(double time)
    {
        return _initTimeLocal.AddSeconds(time);
    }
    #endregion

    #region 获取时间显示
    /// <summary>
    /// 当前日期时间显示
    /// </summary>
    /// <returns></returns>
    public static string GetNowTime(string dateFormat)
    {
        return GetDateTime(Now(), dateFormat);
    }
    /// <summary>
    /// 日期时间显示
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="dateFormat"></param>
    /// <returns></returns>
    public static string GetDateTime(DateTime dt, string dateFormat)
    {
        return dt.ToString(dateFormat);
    }
    /// <summary>
    /// 日期时间显示
    /// </summary>
    /// <param name="seconds">距系统初始时间的时间间隔</param>
    /// <returns></returns>
    public static string GetDateTime(double seconds, string dateFormat)
    {
        DateTime dt = _initTimeLocal.AddSeconds(seconds);
        return GetDateTime(dt, dateFormat);
    }
    /// <summary>
    /// 获取时间显示
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <param name="TimeFormat"></param>
    /// <returns></returns>
    public static string GetTimeSpan(TimeSpan timeSpan, string[] TimeFormat)
    {
        if (timeSpan.Days > 0 && TimeFormat.Length >= 4)
        {
            return string.Format(TimeFormat[3], timeSpan.Days, timeSpan.Hours);
        }
        else if (timeSpan.Hours > 0 && TimeFormat.Length >= 3)
        {
            return string.Format(TimeFormat[2], timeSpan.Hours, timeSpan.Minutes);
        }
        else if (timeSpan.Minutes > 0 && TimeFormat.Length >= 2)
        {
            return string.Format(TimeFormat[1], timeSpan.Minutes, timeSpan.Seconds);
        }
        else if (timeSpan.Seconds >= 0 && TimeFormat.Length >= 1)
        {
            return string.Format(TimeFormat[0], timeSpan.Seconds);
        }
        Printer.LogError("不支持的时间格式化方式!");
        return string.Empty;
    }
    /// <summary>
    /// 时间跨度的时间显示
    /// </summary>
    /// <param name="seconds">时间跨度</param>
    /// <returns></returns>
    public static string GetTimeSpan(double seconds, string[] TimeFormat)
    {
        if (seconds < 0)
        {
            Printer.LogError("非法参数：时间小于0！");
            return string.Empty;
        }
        TimeSpan ts = TimeSpan.FromSeconds(seconds);
        return GetTimeSpan(ts, TimeFormat);
    }
    /// <summary>
    /// 两个时刻间隔的时间显示
    /// </summary>
    /// <param name="startTime">开始时刻</param>
    /// <param name="endTime">结束时刻</param>
    /// <returns></returns>
    public static string GetTimeSpan(double startTime, double endTime, string[] TimeFormat)
    {
        double seconds = endTime - startTime;
        if (seconds < 0)
        {
            Printer.LogError("非法参数：初始时间大于结束时间！");
            return string.Empty;
        }
        TimeSpan ts = TimeSpan.FromSeconds(seconds);
        return GetTimeSpan(ts, TimeFormat); ;
    }
    /// <summary>
    /// 当前距离过去某个时刻间隔的时间显示
    /// </summary>
    /// <param name="pastTime">过去的某个时刻</param>
    /// <returns></returns>
    public static string GetTimeSpanToNow(double pastTime, string[] TimeFormat)
    {
        double seconds = Now() - pastTime;
        if (seconds < 0)
        {
            Printer.LogError("非法参数：初始时间大于结束时间！");
            return string.Empty;
        }
        TimeSpan ts = TimeSpan.FromSeconds(seconds);
        return GetTimeSpan(ts, TimeFormat);
    }
    #endregion

    #region 有限循环定时器
    /// <summary>
    /// 添加时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="maxTick"></param>
    /// <returns></returns>
    public static ulong AddScaleTimer(float time, Action<int> onUpdate, Action onComplete, int maxTick = 1)
    {
        return TimerManager.instance.AddScaleTimer(time, onUpdate, onComplete, maxTick);
    }
    /// <summary>
    /// 添加时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="maxTick"></param>
    /// <returns></returns>
    public static ulong AddRealTimer(float time, Action<int> onUpdate, Action onComplete, int maxTick = 1)
    {
        return TimerManager.instance.AddRealTimer(time, onUpdate, onComplete, maxTick);
    }
    #endregion

    #region 无限循环定时器(无效时，使用者注意移除)
    /// <summary>
    /// 添加时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <returns></returns>
    public static ulong AddLoopScaleTimer(float time, Action<int> onUpdate)
    {
        return TimerManager.instance.AddLoopScaleTimer(time, onUpdate);
    }
    /// <summary>
    /// 添加时间缩放定时器（受TimeScale影响）
    /// </summary>
    /// <param name="time"></param>
    /// <param name="onUpdate"></param>
    /// <returns></returns>
    public static ulong AddLoopRealTimer(float time, Action<int> onUpdate)
    {
        return TimerManager.instance.AddLoopRealTimer(time, onUpdate);
    }
    #endregion

    #region 倒计时
    /// <summary>
    /// 添加缩放时间倒计时（受TimeScale影响）
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="period"></param>
    /// <returns></returns>
    public static ulong AddCountdownScaleTimerCallStr(int timeSpan, Action<string> onUpdate, Action onComplete, string[] TimeFormat, int period = 1)
    {
        if (period <= 0)
        {
            Printer.LogError("时间周期必须大于0！");
            return 0;
        }
        int maxTick = timeSpan / period;
        int reste = timeSpan % period;
        if (maxTick <= 0 || reste != 0)
        {
            Printer.LogError("时间跨度必须是时间周期正整数倍！");
            return 0;
        }
        if (onUpdate != null) onUpdate(GetTimeSpan(timeSpan, TimeFormat));
        return AddScaleTimer(period, (tick) =>
        {
            if (onUpdate != null) onUpdate(GetTimeSpan(tick * period, timeSpan, TimeFormat));
        }, onComplete, maxTick);
    }
    /// <summary>
    /// 添加缩放时间倒计时（受TimeScale影响）
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="period"></param>
    /// <returns></returns>
    public static ulong AddCountdownScaleTimerCallInt(int timeSpan, Action<int> onUpdate, Action onComplete, int period = 1)
    {
        if (period <= 0)
        {
            Printer.LogError("时间周期必须大于0！");
            return 0;
        }
        int maxTick = timeSpan / period;
        int reste = timeSpan % period;
        if (maxTick <= 0 || reste != 0)
        {
            Printer.LogError("时间跨度必须是时间周期正整数倍！");
            return 0;
        }
        if (onUpdate != null) onUpdate(timeSpan);
        return AddScaleTimer(period, (tick) =>
        {
            if (onUpdate != null) onUpdate(timeSpan - tick * period);
        }, onComplete, maxTick);
    }
    /// <summary>
    /// 添加真实时间倒计时（不受TimeScale影响）
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="period"></param>
    /// <returns></returns>
    public static ulong AddCountdownRealTimerCallStr(int timeSpan, Action<string> onUpdate, Action onComplete, string[] TimeFormat, int period = 1)
    {
        if (period <= 0)
        {
            Printer.LogError("时间周期必须大于0！");
            return 0;
        }
        int maxTick = timeSpan / period;
        int reste = timeSpan % period;
        if (maxTick <= 0 || reste != 0)
        {
            Printer.LogError("时间跨度必须是时间周期正整数倍！");
            return 0;
        }
        if (onUpdate != null) onUpdate(GetTimeSpan(timeSpan, TimeFormat));
        return AddRealTimer(period, (tick) =>
        {
            if (onUpdate != null) onUpdate(GetTimeSpan(tick * period, timeSpan, TimeFormat));
        }, onComplete, maxTick);
    }
    /// <summary>
    /// 添加真实时间倒计时（不受TimeScale影响）
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <param name="onUpdate"></param>
    /// <param name="onComplete"></param>
    /// <param name="period"></param>
    /// <returns></returns>
    public static ulong AddCountdownRealTimerCallInt(int timeSpan, Action<int> onUpdate, Action onComplete, int period = 1)
    {
        if (period <= 0)
        {
            Printer.LogError("时间周期必须大于0！");
            return 0;
        }
        int maxTick = timeSpan / period;
        int reste = timeSpan % period;
        if (maxTick <= 0 || reste != 0)
        {
            Printer.LogError("时间跨度必须是时间周期正整数倍！");
            return 0;
        }
        if (onUpdate != null) onUpdate(timeSpan);
        return AddRealTimer(period, (tick) =>
        {
            if (onUpdate != null) onUpdate(timeSpan - tick * period);
        }, onComplete, maxTick);
    }
    #endregion

    #region 开始，暂停，继续，移除，设置等操作
    /// <summary>
    /// 开始计时
    /// </summary>
    /// <param name="timerID"></param>
    public static void StartTimer(ulong timerID)
    {
        TimerManager.instance.StartTimer(timerID);
    }
    /// <summary>
    /// 暂停计时
    /// </summary>
    /// <param name="timerID"></param>
    public static void PauseTimer(ulong timerID)
    {
        TimerManager.instance.PauseTimer(timerID);
    }
    /// <summary>
    /// 继续计时
    /// </summary>
    /// <param name="timerID"></param>
    public static void ContinueTimer(ulong timerID)
    {
        TimerManager.instance.ContinueTimer(timerID);
    }
    /// <summary>
    /// 重新计时
    /// </summary>
    /// <param name="timerID"></param>
    public static void RestartTimer(ulong timerID)
    {
        TimerManager.instance.RestartTimer(timerID);
    }
    /// <summary>
    /// 重置当前计时次数
    /// </summary>
    /// <param name="timerID"></param>
    public static void ResetCurTick(ulong timerID)
    {
        TimerManager.instance.ResetCurTick(timerID);
    }
    /// <summary>
    /// 移除计时
    /// </summary>
    /// <param name="timerID"></param>
    public static void RemoveTimer(ulong timerID)
    {
        TimerManager.instance.RemoveTimer(timerID);
    }
    #endregion

}
