﻿using System;
using UnityEngine;
/// <summary>
/// 受TimeScale影响的时间类
/// </summary>
public class ScaleTimer : BaseTimer
{
    public ScaleTimer(float second, int maxTick, Action<int> onUpdate, Action onComplete) : base(second, maxTick, onUpdate, onComplete) { }

    protected override float NowTime
    {
        get { return Time.time; }
    }
}
