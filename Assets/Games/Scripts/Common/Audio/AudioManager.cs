﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 声音管理模块
/// </summary>
public class AudioManager : MonoSingleton<AudioManager>
{
    private const string audioPackName = "audios";
    private AudioSource _audioScene;
    private AudioSource _audioUI;
    protected override void OnInit()
    {
        Transform cameraMgr = CameraManager.instance.transform;
        _audioScene = cameraMgr.Find("AudioScene").GetComponent<AudioSource>();
        _audioUI = cameraMgr.Find("AudioUI").GetComponent<AudioSource>();
        if(_audioScene == null || _audioUI == null)
        {
            Printer.LogError("声音源初始化失败！");
        }
    }

    protected override void OnUnInit()
    {

    }

    #region 播放音效
    public void PlayAudio(string resName, float volume = -1)
    {
        if (_isMute) return;
        AssetData data = new AssetData(resName, resName, (o) =>
        {
            if (o)
            {
                AudioClip clip = o as AudioClip;
                if (clip)
                {
                    GameObject go = new GameObject(string.Format("Audio:{0}", resName));
                    go.transform.parent = CameraManager.instance.transform;
                    go.transform.localPosition = Vector3.zero;
                    AudioSource audioSource = go.AddComponent<AudioSource>();
                    audioSource.PlayOneShot(clip, volume <= 0 ? _volume : volume);
                    Destroy(go, clip.length);
                }
                else
                {
                    Debug.LogError(string.Format("播放音效：{0} 失败！", resName));
                }
            }
        });
        AssetsManager.instance.AsyncSpawnData(new BaseSpawnData[] { data }, null, PriorityMode.First);
    }
    #endregion

    #region 背景音乐
    public void PlayBGAudio(string resName, float volume = -1, bool loop = false)
    {
        if (_isMute) return;
        AssetData data = new AssetData(resName, resName, (o) => 
        {
            if(o)
            {
                AudioClip clip = o as AudioClip;
                if (clip)
                {
                    StopBGAudio();
                    _audioScene.clip = clip;
                    _audioScene.volume = volume <= 0 ? _volume : volume;
                    _audioScene.loop = loop;
                    PlayBGAudio();
                }
                else
                {
                    Debug.LogError(string.Format("播放背景音乐：{0} 失败！", resName));
                }
            }
        });
        AssetsManager.instance.AsyncSpawnData(new BaseSpawnData[] { data }, null, PriorityMode.First);
    }
    public void PlayBGAudio()
    {
        _audioScene.Play();
    }
    public void StopBGAudio()
    {
        _audioScene.Stop();
    }
    private float _volume = 1;
    public void SetVolume(float volume)
    {
        _volume = volume;
        _audioScene.volume = _volume;
    }
    private bool _isMute = false;
    public void SetMute(bool mute)
    {
        _isMute = mute;
        _audioScene.mute = mute;
    }
    #endregion

    #region 指定对象播放音效
    public void PlaySound(GameObject go, AudioClip clip, float volume)
    {
        if (go == null || clip == null) return;
        _audioUI.PlayOneShot(clip, volume);
    }
    #endregion

}
