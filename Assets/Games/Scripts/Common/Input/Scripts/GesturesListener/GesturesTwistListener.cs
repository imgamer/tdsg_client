﻿using UnityEngine;
using System.Collections;

public class GesturesTwistListener : GestureRecognizerDelegate
{
    void Start()
    {
        TwistRecognizer recognizer = transform.GetComponent<TwistRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnTwist(TwistGesture gesture)
    {
        InputManager.instance.OnTwist(gesture);
    }
}