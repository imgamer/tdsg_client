﻿using UnityEngine;
using System.Collections;

public class GesturesLongPressListener : GestureRecognizerDelegate
{
    void Start()
    {
        LongPressRecognizer recognizer = transform.GetComponent<LongPressRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnLongPress(LongPressGesture gesture)
    {
        InputManager.instance.OnLongPress(gesture);
    }
}
