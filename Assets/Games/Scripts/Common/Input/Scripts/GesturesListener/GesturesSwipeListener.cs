﻿using UnityEngine;
using System.Collections;

public class GesturesSwipeListener : GestureRecognizerDelegate
{
    void Start()
    {
        SwipeRecognizer recognizer = transform.GetComponent<SwipeRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnSwipe(SwipeGesture gesture)
    {
        InputManager.instance.OnSwipe(gesture);
    }
}
