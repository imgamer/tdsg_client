﻿using UnityEngine;
using System.Collections;

public class GesturesDragListener : GestureRecognizerDelegate
{
    void Start()
    {
        DragRecognizer recognizer = transform.GetComponent<DragRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnDrag(DragGesture gesture)
    {
        InputManager.instance.OnDrag(gesture);
    }
}
