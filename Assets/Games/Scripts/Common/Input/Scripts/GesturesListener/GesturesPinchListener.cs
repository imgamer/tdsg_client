﻿using UnityEngine;
using System.Collections;

public class GesturesPinchListener : GestureRecognizerDelegate
{
    void Start()
    {
        PinchRecognizer recognizer = transform.GetComponent<PinchRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnPinch(PinchGesture gesture)
    {
        InputManager.instance.OnPinch(gesture);
    }
}
