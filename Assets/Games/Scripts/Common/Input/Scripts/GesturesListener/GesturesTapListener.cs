﻿using UnityEngine;
using System.Collections;

public class GesturesTapListener : GestureRecognizerDelegate
{
    void Start()
    {
        TapRecognizer recognizer = transform.GetComponent<TapRecognizer>();
        recognizer.Delegate = this;
    }

    public override bool CanBegin(Gesture gesture, FingerGestures.IFingerList touches)
    {
        return !UICamera.isOverUI;
    }

    void OnTap(TapGesture gesture)
    {
        InputManager.instance.OnTap(gesture); 
    }
}
