﻿using UnityEngine;
using System.Collections;

public class NGUIPressListener : MonoBehaviour 
{
    void Start()
    {
        UICamera.onPress += InputManager.instance.OnPress;
    }
}
