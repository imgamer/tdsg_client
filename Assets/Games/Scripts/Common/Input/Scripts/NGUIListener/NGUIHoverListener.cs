﻿using UnityEngine;
using System.Collections;

public class NGUIHoverListener : MonoBehaviour 
{
    void Start()
    {
        UICamera.onHover += InputManager.instance.OnHover;
    }
}
