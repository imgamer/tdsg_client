﻿using UnityEngine;
using System.Collections;

public class NGUIDragListener : MonoBehaviour 
{
    void Start()
    {
        UICamera.onDragStart += InputManager.instance.OnDragStart;
        UICamera.onDrag += InputManager.instance.OnDrag;
        UICamera.onDragOver += InputManager.instance.OnDragOver;
        UICamera.onDragEnd += InputManager.instance.OnDragEnd;
        UICamera.onDragOut += InputManager.instance.OnDragOut;
    }
}
