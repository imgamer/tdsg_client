﻿using UnityEngine;
using System.Collections;

public class NGUIClickListener : MonoBehaviour 
{
	void Start () 
    {
        UICamera.onClick += InputManager.instance.OnClick;
        UICamera.onDoubleClick += InputManager.instance.OnDoubleClick;
	}
}
