﻿using UnityEngine;
using System.Collections;

public class NGUIKeyListener : MonoBehaviour
{
    void Start()
    {
        UICamera.onKey += InputManager.instance.OnKey;
    }
}
