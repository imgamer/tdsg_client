﻿using UnityEngine;
using System.Collections;

public class NGUIScrollListener : MonoBehaviour 
{
    void Start()
    {
        UICamera.onScroll += InputManager.instance.OnScroll;
    }
}
