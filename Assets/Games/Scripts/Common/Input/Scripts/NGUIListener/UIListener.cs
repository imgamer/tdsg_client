﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 界面事件监听类
/// </summary>
public class UIListener : MonoBehaviour
{
    public delegate void VoidDelegate(GameObject go);
    private List<VoidDelegate> delegateList = new List<VoidDelegate>();
    public void Click(GameObject go)                  
    {
        if (trigger == Trigger.OnClick) AudioManager.instance.PlaySound(gameObject, audioClip, volume);
    }
    public void Hover(GameObject go, bool isOver)       
    {
        if ((isOver && trigger == Trigger.OnHover)) AudioManager.instance.PlaySound(gameObject, audioClip, volume);
    }
    public void Press(GameObject go, bool isPressed)    
    {
        if ((isPressed && trigger == Trigger.OnPress)) AudioManager.instance.PlaySound(gameObject, audioClip, volume);
    }

    public enum Trigger
    {
        OnClick,
        OnHover,
        OnPress,
    }

    public AudioClip audioClip;
    public Trigger trigger = Trigger.OnClick;

    [Range(0f, 1f)]
    public float volume = 1f;


    public static UIListener SetDelegate(GameObject go, VoidDelegate onDestroy)
    {
        UIListener listener = go.GetComponent<UIListener>();
        if (listener == null) listener = go.AddComponent<UIListener>();
        if (listener.delegateList.Contains(onDestroy)) return listener;
        listener.delegateList.Add(onDestroy);
        return listener;
    }

    void OnDestroy()
    {
        for (int i = delegateList.Count - 1; i >= 0 ; i--)
        {
            VoidDelegate cb = delegateList[i];
            if (cb != null) cb(gameObject);
        }
        delegateList.Clear();
    }
}
