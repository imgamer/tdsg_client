﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 输入控制模块
/// </summary>
public class InputManager : MonoBehaviour
{
    #region Init和UnInit
    public static InputManager instance { get; private set; }
    void Awake()
    {
        instance = this;
    }

    public void Init()
    {

    }

    public void UnInit()
    {
        //场景事件移除
        gesturesTapDelegates.Clear();
        gesturesPickDelegates.Clear();
        gesturesLongPressDelegates.Clear();
        gesturesPinchDelegates.Clear();
        gesturesDragDelegates.Clear();
        gesturesSwipeDelegates.Clear();
        gesturesTwistDelegates.Clear();

        //UI事件移除
        nguiHitDelegates.Clear();
        nguiClickDelegates.Clear();
        nguiDoubleClickDelegates.Clear();
        nguiHoverDelegates.Clear();
        nguiPressDelegates.Clear();
        nguiScrollDelegates.Clear();
        nguiKeyDelegates.Clear();
        nguiDragStartDelegates.Clear();
        nguiDragDelegates.Clear();
        nguiDragOverDelegates.Clear();
        nguiDragOutDelegates.Clear();
        nguiDragEndDelegates.Clear();
    }
    #endregion

    #region 输入状态切换
    public enum State
    {
        All,
        OnlyUI,
        OnlyScene,
        None
    }
    public State state = State.All;
    /// <summary>
    /// 改变输入状态
    /// </summary>
    /// <param name="state"></param>
    public void SetState(State state)
    {
        this.state = state;
    }
    #endregion

    #region 场景事件委托定义
    public delegate void GestureDelegate(Gesture gesture);
    private Dictionary<GameObject, GestureDelegate> gesturesPickDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesTapDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesLongPressDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesPinchDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesDragDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesSwipeDelegates = new Dictionary<GameObject, GestureDelegate>();
    private Dictionary<GameObject, GestureDelegate> gesturesTwistDelegates = new Dictionary<GameObject, GestureDelegate>();
    #endregion

    #region 场景事件响应
    //OnTap
    public void OnTap(TapGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesturesPickDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(gesturesPickDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if (key == null)
                    {
                        gesturesPickDelegates.Remove(key);
                        continue;
                    }
                    if (key.activeInHierarchy)
                    {
                        gesturesPickDelegates[key](gesture);
                    }
                }
            }
            if (gesture.Selection)
            {
                GestureDelegate tap;
                if (gesturesTapDelegates.TryGetValue(gesture.Selection, out tap))
                {
                    if (tap != null) tap(gesture);
                }
            }
        }
    }

    //OnLongPress
    public void OnLongPress(LongPressGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesture.Selection == null) return;
            GestureDelegate longPress;
            if (gesturesLongPressDelegates.TryGetValue(gesture.Selection, out longPress))
            {
                if (longPress != null) longPress(gesture);
            }
        }
    }

    //OnPinch
    public void OnPinch(PinchGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesturesPinchDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(gesturesPinchDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if (key == null)
                    {
                        gesturesPinchDelegates.Remove(key);
                        continue;
                    }
                    if (key.activeInHierarchy)
                    {
                        gesturesPinchDelegates[key](gesture);
                    }
                }
            }
            if (gesture.Selection)
            {
                GestureDelegate pinch;
                if (gesturesPinchDelegates.TryGetValue(gesture.Selection, out pinch))
                {
                    if (pinch != null) pinch(gesture);
                }
            }
        }
    }

    //OnDrag
    public void OnDrag(DragGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesturesDragDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(gesturesDragDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if (key == null)
                    {
                        gesturesDragDelegates.Remove(key);
                        continue;
                    }
                    if (key.activeInHierarchy)
                    {
                        gesturesDragDelegates[key](gesture);
                    }
                }
            }
            if (gesture.Selection)
            {
                GestureDelegate drag;
                if (gesturesDragDelegates.TryGetValue(gesture.Selection, out drag))
                {
                    if (drag != null) drag(gesture);
                }
            }
        }
    }

    //OnSwipe
    public void OnSwipe(SwipeGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesturesSwipeDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(gesturesSwipeDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if (key == null)
                    {
                        gesturesSwipeDelegates.Remove(key);
                        continue;
                    }
                    if (key.activeInHierarchy)
                    {
                        gesturesSwipeDelegates[key](gesture);
                    }
                }
            }
            if (gesture.Selection)
            {
                GestureDelegate swipe;
                if (gesturesSwipeDelegates.TryGetValue(gesture.Selection, out swipe))
                {
                    if (swipe != null) swipe(gesture);
                }
            }
        }
    }

    //OnTwist
    public void OnTwist(TwistGesture gesture)
    {
        if (state == State.All || state == State.OnlyScene)
        {
            if (gesturesTwistDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(gesturesTwistDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if (key == null)
                    {
                        gesturesTwistDelegates.Remove(key);
                        continue;
                    }
                    if (key.activeInHierarchy)
                    {
                        gesturesTwistDelegates[key](gesture);
                    }
                }
            }
            if (gesture.Selection)
            {
                GestureDelegate twist;
                if (gesturesTwistDelegates.TryGetValue(gesture.Selection, out twist))
                {
                    if (twist != null) twist(gesture);
                }
            }
        }
    }
    #endregion

    #region 场景事件的监听和移除
    //Pick
    public void AddPickListener(GameObject go, GestureDelegate pick)
    {
        if (go == null) return;
        gesturesPickDelegates[go] = pick;
    }
    public void RemovePickListener(GameObject go)
    {
        if (go == null) return;
        gesturesPickDelegates.Remove(go);
    }
    //Tap
    public void AddTapListener(GameObject go, GestureDelegate tap)
    {
        if (go == null) return;
        gesturesTapDelegates[go] = tap;
    }
    public void RemoveTapListener(GameObject go)
    {
        if (go == null) return;
        gesturesTapDelegates.Remove(go);
    }
    //LongPress
    public void AddLongPressListener(GameObject go, GestureDelegate longPress)
    {
        if (go == null) return;
        gesturesLongPressDelegates[go] = longPress;
    }
    public void RemoveLongPressListener(GameObject go)
    {
        if (go == null) return;
        gesturesLongPressDelegates.Remove(go);
    }
    //Pinch
    public void AddPinchListener(GameObject go, GestureDelegate pinch)
    {
        if (go == null) return;
        gesturesPinchDelegates[go] = pinch;
    }
    public void RemovePinchListener(GameObject go)
    {
        if (go == null) return;
        gesturesPinchDelegates.Remove(go);
    }
    //Drag
    public void AddDragListener(GameObject go, GestureDelegate drag)
    {
        if (go == null) return;
        gesturesDragDelegates[go] = drag;
    }
    public void RemoveDragListener(GameObject go)
    {
        if (go == null) return;
        gesturesDragDelegates.Remove(go);
    }
    //Swipe
    public void AddSwipeListener(GameObject go, GestureDelegate swipe)
    {
        if (go == null) return;
        gesturesSwipeDelegates[go] = swipe;
    }
    public void RemoveSwipeListener(GameObject go)
    {
        if (go == null) return;
        gesturesSwipeDelegates.Remove(go);
    }
    //Twist
    public void AddTwistListener(GameObject go, GestureDelegate twist)
    {
        if (go == null) return;
        gesturesTwistDelegates[go] = twist;
    }
    public void RemoveTwistListener(GameObject go)
    {
        if (go == null) return;
        gesturesTwistDelegates.Remove(go);
    }
    #endregion

    #region UI事件委托定义
    public delegate void VoidDelegate(GameObject go);
    public delegate void BoolDelegate(GameObject go, bool state);
    public delegate void FloatDelegate(GameObject go, float delta);
    public delegate void VectorDelegate(GameObject go, Vector2 delta);
    public delegate void ObjectDelegate(GameObject go, GameObject obj);
    public delegate void KeyCodeDelegate(GameObject go, KeyCode key);

    private Dictionary<GameObject, VoidDelegate> nguiHitDelegates = new Dictionary<GameObject, VoidDelegate>();
    private Dictionary<GameObject, VoidDelegate> nguiClickDelegates = new Dictionary<GameObject, VoidDelegate>();
    private Dictionary<GameObject, VoidDelegate> nguiDoubleClickDelegates = new Dictionary<GameObject, VoidDelegate>();
    private Dictionary<GameObject, BoolDelegate> nguiHoverDelegates = new Dictionary<GameObject, BoolDelegate>();
    private Dictionary<GameObject, BoolDelegate> nguiPressDelegates = new Dictionary<GameObject, BoolDelegate>();
    private Dictionary<GameObject, FloatDelegate> nguiScrollDelegates = new Dictionary<GameObject, FloatDelegate>();
    private Dictionary<GameObject, KeyCodeDelegate> nguiKeyDelegates = new Dictionary<GameObject, KeyCodeDelegate>();

    private Dictionary<GameObject, VoidDelegate> nguiDragStartDelegates = new Dictionary<GameObject, VoidDelegate>();
    private Dictionary<GameObject, VectorDelegate> nguiDragDelegates = new Dictionary<GameObject, VectorDelegate>();
    private Dictionary<GameObject, ObjectDelegate> nguiDragOverDelegates = new Dictionary<GameObject, ObjectDelegate>();
    private Dictionary<GameObject, ObjectDelegate> nguiDragOutDelegates = new Dictionary<GameObject, ObjectDelegate>();
    private Dictionary<GameObject, VoidDelegate> nguiDragEndDelegates = new Dictionary<GameObject, VoidDelegate>();

    #endregion

    #region UI事件响应
    public void OnClick(GameObject go)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            if (nguiHitDelegates.Count > 0)
            {
                List<GameObject> list = new List<GameObject>(nguiHitDelegates.Keys);
                for (int i = 0; i < list.Count; i++)
                {
                    GameObject key = list[i];
                    if(key == null)
                    {
                        nguiHitDelegates.Remove(key);
                        continue;
                    }
                    if(key.activeInHierarchy)
                    {
                        nguiHitDelegates[key](go);
                    }
                }
            }
            VoidDelegate onClick;
            if (nguiClickDelegates.TryGetValue(go, out onClick))
            {
                onClick(go);
            }
        }
    }
    public void OnHover(GameObject go, bool isOver)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            BoolDelegate onHover;
            if (nguiHoverDelegates.TryGetValue(go, out onHover))
            {
                onHover(go, isOver);
            }
        }
    }
    public void OnPress(GameObject go, bool isPressed)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            BoolDelegate onPress;
            if (nguiPressDelegates.TryGetValue(go, out onPress))
            {
                onPress(go, isPressed);
            }
        }
    }
    public void OnDoubleClick(GameObject go)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            VoidDelegate onDoubleClick;
            if (nguiDoubleClickDelegates.TryGetValue(go, out onDoubleClick))
            {
                onDoubleClick(go);
            }
        }
    }
    public void OnScroll(GameObject go, float delta)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            FloatDelegate onScroll;
            if (nguiScrollDelegates.TryGetValue(go, out onScroll))
            {
                onScroll(go, delta);
            }
        }
    }
    public void OnDragStart(GameObject go)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            VoidDelegate onDragStart;
            if (nguiDragStartDelegates.TryGetValue(go, out onDragStart))
            {
                onDragStart(go);
            }
        }
    }
    public void OnDrag(GameObject go, Vector2 delta)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            VectorDelegate onDrag;
            if (nguiDragDelegates.TryGetValue(go, out onDrag))
            {
                onDrag(go, delta);
            }
        }
    }
    public void OnDragOver(GameObject go, GameObject obj) 
    {
        if (state == State.All || state == State.OnlyUI)
        {
            ObjectDelegate onDragOver;
            if (nguiDragOverDelegates.TryGetValue(go, out onDragOver))
            {
                onDragOver(go, obj);
            }
        }
    }
    public void OnDragOut(GameObject go, GameObject obj)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            ObjectDelegate onDragOut;
            if (nguiDragOutDelegates.TryGetValue(go, out onDragOut))
            {
                onDragOut(go, obj);
            }
        }
    }
    public void OnDragEnd(GameObject go)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            VoidDelegate onDragEnd;
            if (nguiDragEndDelegates.TryGetValue(go, out onDragEnd))
            {
                onDragEnd(go);
            }
        }
    }
    public void OnKey(GameObject go, KeyCode key)
    {
        if (state == State.All || state == State.OnlyUI)
        {
            KeyCodeDelegate onKey;
            if (nguiKeyDelegates.TryGetValue(go, out onKey))
            {
                onKey(go, key);
            }
        }
    }
    #endregion

    #region UI事件监听和移除
    //Hit
    public void AddHitListener(GameObject go, VoidDelegate onHit)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveHitListener);
        nguiHitDelegates[go] = onHit;
    }
    public void RemoveHitListener(GameObject go)
    {
        if (go == null) return;
        nguiHitDelegates.Remove(go);
    }
    //Click
    public void AddClickListener(GameObject go, VoidDelegate onClick)
    {
        if (go == null) return;
        UIListener listener = UIListener.SetDelegate(go, RemoveClickListener);
        nguiClickDelegates[go] = onClick + listener.Click;
    }
    public void RemoveClickListener(GameObject go)
    {
        if (go == null) return;
        nguiClickDelegates.Remove(go);
    }
    //Hover
    public void AddHoverListener(GameObject go, BoolDelegate onHover)
    {
        if (go == null) return;
        UIListener listener = UIListener.SetDelegate(go, RemoveHoverListener);
        nguiHoverDelegates[go] = onHover + listener.Hover;
    }
    public void RemoveHoverListener(GameObject go)
    {
        if (go == null) return;
        nguiHoverDelegates.Remove(go);
    }
    //Press
    public void AddPressListener(GameObject go, BoolDelegate onPress)
    {
        if (go == null) return;
        UIListener listener = UIListener.SetDelegate(go, RemovePressListener);
        nguiPressDelegates[go] = onPress + listener.Press;
    }
    public void RemovePressListener(GameObject go)
    {
        if (go == null) return;
        nguiPressDelegates.Remove(go);
    }
    //DoubleClick
    public void AddDoubleClickListener(GameObject go, VoidDelegate onDoubleClick)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDoubleClickListener);
        nguiDoubleClickDelegates[go] = onDoubleClick;
    }
    public void RemoveDoubleClickListener(GameObject go)
    {
        if (go == null) return;
        nguiDoubleClickDelegates.Remove(go);
    }
    //Scroll
    public void AddScrollListener(GameObject go, FloatDelegate onScroll)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveScrollListener);
        nguiScrollDelegates[go] = onScroll;
    }
    public void RemoveScrollListener(GameObject go)
    {
        if (go == null) return;
        nguiScrollDelegates.Remove(go);
    }
    //DragStart
    public void AddDragStartListener(GameObject go, VoidDelegate onDragStart)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDragStartListener);
        nguiDragStartDelegates[go] = onDragStart;
    }
    public void RemoveDragStartListener(GameObject go)
    {
        if (go == null) return;
        nguiDragStartDelegates.Remove(go);
    }
    //Draging
    public void AddDragingListener(GameObject go, VectorDelegate onDrag)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDragingListener);
        nguiDragDelegates[go] = onDrag;
    }
    public void RemoveDragingListener(GameObject go)
    {
        if (go == null) return;
        nguiDragDelegates.Remove(go);
    }
    //DragOver
    public void AddDragOverListener(GameObject go, ObjectDelegate onDragOver)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDragOverListener);
        nguiDragOverDelegates[go] = onDragOver;
    }
    public void RemoveDragOverListener(GameObject go)
    {
        if (go == null) return;
        nguiDragOverDelegates.Remove(go);
    }
    //DragOut
    public void AddDragOutListener(GameObject go, ObjectDelegate onDragOut)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDragOutListener);
        nguiDragOutDelegates[go] = onDragOut;
    }
    public void RemoveDragOutListener(GameObject go)
    {
        if (go == null) return;
        nguiDragOutDelegates.Remove(go);
    }
    //DragEnd
    public void AddDragEndListener(GameObject go, VoidDelegate onDragEnd)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, RemoveDragEndListener);
        nguiDragEndDelegates[go] = onDragEnd;
    }
    public void RemoveDragEndListener(GameObject go)
    {
        if (go == null) return;
        nguiDragEndDelegates.Remove(go);
    }
    //Key
    public void AddKeyListener(GameObject go, KeyCodeDelegate onKey)
    {
        if (go == null) return;
        UIListener.SetDelegate(go, AddKeyListener);
        nguiKeyDelegates[go] = onKey;
    }
    public void AddKeyListener(GameObject go)
    {
        if (go == null) return;
        nguiKeyDelegates.Remove(go);
    }
    #endregion

}
