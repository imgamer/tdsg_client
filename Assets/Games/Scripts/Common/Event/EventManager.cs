﻿using System;
using System.Collections;
using System.Collections.Generic;

public delegate void Callback<T>(T arg1);
/// <summary>
/// 事件管理中心
/// </summary>
public static class EventManager
{
    private static Dictionary<EventID, Delegate> _eventTable = new Dictionary<EventID, Delegate>();

    /// <summary>
    /// 清除所有事件监听
    /// </summary>
    public static void ClearEvent()
    {
        _eventTable.Clear();
    }

    /// <summary>
    /// 添加事件
    /// </summary>
    /// <param name="eventType"></param>
    /// <param name="handler"></param>
    public static void AddListener<T>(EventID eventType, Callback<T> handler)
    {
        lock (_eventTable)
        {
            if (!_eventTable.ContainsKey(eventType))
            {
                _eventTable.Add(eventType, handler);
            }
            else
            {
                _eventTable[eventType] = (Callback<T>)_eventTable[eventType] + handler;
            }
        }
    }
    /// <summary>
    /// 移除事件
    /// </summary>
    /// <param name="eventType"></param>
    /// <param name="handler"></param>
    public static void RemoveListener<T>(EventID eventType, Callback<T> handler)
    {
        lock (_eventTable)
        {
            if (_eventTable.ContainsKey(eventType))
            {
                _eventTable[eventType] = (Callback<T>)_eventTable[eventType] - handler;
                if (_eventTable[eventType] == null)
                {
                    _eventTable.Remove(eventType);
                }
            }
        }
    }
    /// <summary>
    /// 激活事件
    /// </summary>
    /// <param name="eventType"></param>
    /// <param name="arg1"></param>
    public static void Invoke<T>(EventID eventType, T arg1)
    {
        Delegate kDelegate;
        if (_eventTable.TryGetValue(eventType, out kDelegate))
        {
            Callback<T> handler = (Callback<T>)kDelegate;
            if (handler != null)
            {
                handler(arg1);
            }
        }
    }
}
