﻿//------------------------------------------------------------------------------
// Author: yp.
// Date created:2014-10-20.
//------------------------------------------------------------------------------

public enum EventID
{
	EVNT_NONE,
    EVNT_UPDATE_DOWNLOAD,  //更新下载进度
    EVNT_SCENE_LOADING,//更新场景加载进度
    EVNT_ROOM_ENTER,//完全进入场景         
	EVNT_ROOM_STAGE_START, //关卡阶段消息提示
	EVNT_ON_NPCCREATE_OVER,//NPC创建结束
	EVNT_ON_LOGIN_SUCCESS,//登陆成功
	EVNT_ON_LOGIN_FAILED,//登陆失败
    EVNT_ROLE_NUM_UPDATE,//角色数量变化
	EVNT_ROOM_STAGE_OVER, // 关卡结束

    EVNT_SCENE_READY,//客户端场景，实体等完全准备完毕
    EVNT_PLAYERENTITY_STATUS,//场景实体状态变化

	EVNT_BAG_ADD_ITEM,//UI背包加入物品
	EVNT_BAG_REMOVE_ITEM,//UI背包移除物品
    EVNT_BAG_UPDATE_ITEM_ATTRIBUTE,//UI背包刷新物品属性
    EVNT_WAREHOUSE_ADD_ITEM,//UI仓库加入物品
    EVNT_WAREHOUSE_REMOVE_ITEM,//UI仓库移除物品

    EVNT_EQUIP_PUTON,//穿戴装备
    EVNT_EQUIP_TAKEOFF,//卸下装备

	EVNT_UPDATE_QUEST,//更新任务
    EVNT_REMOVE_QUEST,//移除任务

    EVNT_FIGHT_ENTITY_NUM_UPDATE,//战斗实体数量变化
    EVNT_FIGHT_START,//阶段战斗开始
    EVNT_FIGHT_END,//阶段战斗结束
    EVNT_ROUND_START,//战斗回合开始
    EVNT_ROUND_END,//战斗回合结束
    EVNT_PLAYER_MODE_UPDATE,//玩家战斗模式修改
    EVNT_PLAYER_ANGER_UPDATE,//玩家怒气值更新
    EVNT_PLAYER_FIGHTSTATE_UPDATE,//玩家战斗状态更新

	EVNT_EMAIL_UPDATE, //邮件数据更新
	EVNT_FRIEND_LIST_UPDATE, //好友列表数据更新
	EVNT_FRIEND_REQUEST_UPDATE, //好友申请列表数据更新
	EVNT_FRIEND_RECOMMEND_UPDATE, //好友推荐数据更新

    EVNT_FUNCTION_STATE_UPDATE,//主界面功能开放状态更新
    EVNT_CHAT_DATA_ADD, //聊天输入
    EVNT_CHAT_DATA_UPDATE, //聊天数据更新

    EVNT_TEAM_STATE_UPDATE,//组队状态更新
    EVNT_TEAM_MEMBER_UPDATE,//组队队员更新
    EVNT_TEAM_TARGET_UPDATE,//组队目标更新
    EVNT_TEAM_APPLICANT_UPDATE,//组队申请者更新

    EVNT_TONG_STATE_UPDATE,//帮会状态更新
    EVNT_TONG_MEMBER_UPDATE,//帮会成员更新
    EVNT_TONG_TARGET_UPDATE,//帮会目标更新
    EVNT_TONG_APPLICANT_UPDATE,//帮会申请者更新

    EVNT_PET_BATTLE_STATE,//宠物出战状态
    EVNT_PET_SELECT_UPDATE,//宠物选择对象更新
    EVNT_PET_STATE_UPDATE,//宠物状态更新
    EVNT_PET_NUM_UPDATE,//宠物数量更新
    EVNT_PET_COMBINEPET_UPDATE,//宠物合宠对象更新
    EVNT_PET_COMBINEPET_SUCCESS,//宠物合成成功提示

    EVNT_GOLD_UPDATE, //界面金币刷新
    EVNT_INGOT_UPDATE, //界面仙玉刷新
    EVNT_SILVER_UPDATE, //界面银币更新
    EVNT_MEDAL_UPDATE,//竞技场勋章更新

    EVNT_PITCH_TRADE_UPDATE, //摆摊物品数据更新
    EVNT_PITCH_PUBLIC_UPDATE, //摆摊公示物品数据刷新

    EVNT_ACTIVITY_EVERYDAY_UPDATE,//日常活动数据刷新
    EVNT_ACTIVENESS_UPDATE, //活跃度值更新
    EVNT_ACTIVENESS_REWARD_STATE_UPDATE, //活跃度奖励状态更新
    EVNT_RANK_UPDATE, //排行榜数据刷新

    EVNT_ARENA_RIVAL_UPDATE, //竞技场数据更新
    EVNT_ARENA_RECORD_UPDATE,//竞技场战斗记录数据更新
    EVNT_ARENA_TIME_UPDATE, //竞技场时间冷却相关
    EVNT_ARENA_COUNT_UPDATE, //竞技场挑战次数更新
    EVNT_ARENA_BUYCOUNT_UPDATE, //竞技场购买次数
    EVNT_ARENA_REWARD_UPDATE, //竞技场可领取奖励更新
    EVNT_ARENA_SHOP_UPDATE,//竞技场商店更新

    EVNT_SLAVE_LIST_UPDATE,//奴隶列表更新   
    EVNT_SLAVE_HANDLOSERS_UPDATE, //手下败将列表更新
    EVNT_SLAVE_CAPTURE_COUNT_UPDATE, //俘虏次数更新
    EVNT_SLAVE_CAPTURE_TIME_UPDATE, //将奴俘获冷却时间相关
    EVNT_SLAVE_SQUEEZE_TIME_UPDATE, //将奴压榨冷却时间相关
    EVNT_SLAVE_SQUEEZE_COUNT_UPDATE, //将奴压榨次数相关
    EVNT_SLAVE_PLACATE_COUNT_UPDATE, //将奴安抚次数相关

    EVNT_ROLE_ATTRIBUTE_UPDATE, //角色属性更新

    EVNT_HERO_SELECT_UPDATE, //助战选择对象更新
    EVNT_HERO_STATE_UPDATE,

    EVNT_SIGN_DATA_UPDATE,//签到数据更新

    EVNT_TEAMCOPY_AGREE_UPDATE, //组队副本同意状态更新
    EVNT_TEAMCOPY_REWARD_UPDATE, //组队副本奖励更新
    EVNT_MAX,
}