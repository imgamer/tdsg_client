﻿using UnityEngine;
using System.Collections;
using System;

public class Const
{
    //登录场景类型
    public const UInt16 LoginSceneType = 1007;

    //场景地图默认SortingOrder
    public const int MapSortingOrder = -1000;
    //场景后景特效默认SortingOrder
    public const int BottomSceneEffectSortingOrder = -600;
    //场景前景特效默认SortingOrder
    public const int TopSceneEffectSortingOrder = -100;
    //模型实体默认SortingOrder
    public const int EntitySortingOrder = -300;
    //模型实体后HUD默认SortingOrder
    public const int BottomHUDSortingOrder = -400;
    //模型实体前HUD默认SortingOrder
    public const int TopHUDSortingOrder = 2;//特效设置分层后，改为-200 
    //相机遮罩蒙版默认SortingOrder
    public const int CameraMaskSortingOrder = 0;

	//buff触发时机（0是释放技能时触发，1是释放技能后触发，2是下回合出手前触发，3是下回合出手后触发）
	//	1、行动时
	//	2、受伤时
	//	3、造成伤害时
	//	4、造成目标死亡时
	//	5、立即生效时
	//	6、加血时

	public const int BUFF_TRIGGER_USE_SKILL = 0;
	public const int BUFF_TRIGGER_USE_SKILL_OVER = 1;
	public const int BUFF_TRIGGER_NEXT_ROUND_SPELL_BEFORE = 2;
	public const int BUFF_TRIGGER_NEXT_ROUND_SPELL_LATER = 3;

	public const int BUFF_TRIGGER_XDS = 1;
	public const int BUFF_TRIGGER_SSS = 2;
	public const int BUFF_TRIGGER_ZCSHS = 3;
	public const int BUFF_TRIGGER_ZCMBSMS = 4;
	public const int BUFF_TRIGGER_LJSXS = 5;
	public const int BUFF_TRIGGER_JXS = 6;


	


	//buff类型(1是强化，2是弱化)
	public const int BUFF_TYPE_ENHANCEBUFF = 1;
	public const int BUFF_TYPE_DEBUFF = 2;

	//属性种类id(攻击力，防御力....)
	public const int ATTACK_VALUE = 3;//攻击
	public const int DEFENSE_VALUE = 4;//防御
	public const int CRITICAL_VALUE = 5;//暴击值
	public const int TOUGHNESS_VALUE = 6;//韧性值
	public const int CRITICAL_DAMAGE_VALUE = 7;//必杀值
	public const int PARRY_VALUE = 8;//格挡值
	public const int SUNDER_ARMORVALUE = 9;//破甲值
	public const int EFFECT_RESIST_VALUE = 10;//效果抵抗值
	public const int EFFECT_ENHANCE_VALUE = 11;//效果增强值

	// 邮件附件类型
	public const int EMAIL_ATTACHMENT_TYPE_ITME = 1; // 道具
	public const int EMAIL_ATTACHMENT_TYPE_GOLD = 2; // 金币
	public const int EMAIL_ATTACHMENT_TYPE_INGOT = 3; // 元宝
	public const int EMAIL_ATTACHMENT_TYPE_AVATAREXP = 4; // 玩家经验
			
	// 邮件收取状态
	public const int EMAIL_NOT_RECEIVE = 0; // 未收取
	public const int EMAIL_HAS_RECEIVE = 1; // 已收取

	//技能类型 0攻击 1回复 2消除增益buff 3触发buff的空技能规则 4被动 5控制 6复活 7消除减益的debuff
	public const int SKILL_TYPE_ATTACK = 0; 
	public const int SKILL_TYPE_RESTORE = 1;
	public const int SKILL_TYPE_ELIMINATE_BUFF = 2;
	public const int SKILL_TYPE_TRIGGER_BUFF = 3;
	public const int SKILL_TYPE_PASSIVE = 4;
	public const int SKILL_TYPE_CONTROL = 5;
	public const int SKILL_TYPE_RESURRECT = 6;
	public const int SKILL_TYPE_ELIMINATE_DEBUFF = 7;




	//战斗阶段
	public const int SELECT_OPERATION_STAGE = 1; //选择操作阶段
	public const int CAST_OPERATION_STAGE = 2; //释放操作阶段


    public const float THUNDER_FIGHT_JUDGE_TIME = 3.0f; //踩雷遇怪判定时间
    public const float THUNDER_FIGHT_JUDGE_KEEP_MOVE_TIME = 0.1f; //连续走动时间有效时长判定
		
}
