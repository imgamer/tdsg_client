﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;

/// <summary>
/// 相机工具
/// </summary>
public static class CameraUtils
{
    #region 将某个相机所看到的某些层或者某些对象隔离出来，用一个专有相机来渲染。多用在FADE和静态屏幕效果时间，筛选出不参与的对象
    private static LayerMask excludeLayer;
    private static Camera excludeTarget;
    private static void Exclude(this Camera targetCamera, LayerMask value)
    {
        if (excludeLayer != value)
        {
            Camera camera = ExcludeCamera;
            camera.enabled = true;
            camera.CopyFrom(targetCamera);

            camera.clearFlags = CameraClearFlags.Depth;
            camera.cullingMask = value;
            camera.transform.parent = targetCamera.transform;
            camera.transform.localPosition = Vector3.zero;
            camera.transform.localRotation = Quaternion.identity;
            camera.depth = targetCamera.depth + 1;

            excludeLayer = value;
            excludeTarget = targetCamera;
            excludeTarget.cullingMask &= ~excludeLayer;
        }
    }

    private static void UnExclude()
    {
        if (excludeCamera)
        {
            excludeCamera.enabled = false;
        }
        if (excludeTarget) excludeTarget.cullingMask |= excludeLayer;
        excludeLayer = 0;
    }

    private static Camera excludeCamera;
    private static Camera ExcludeCamera
    {
        get
        {
            if (!excludeCamera)
            {
                GameObject go = GameObject.FindGameObjectWithTag(GameUtils.TagFadeCamera);
                if (!go || go.GetComponent<Camera>() == null)
                {
                    go = new GameObject("ExcludeCamera");
                    go.tag = GameUtils.TagFadeCamera;
                    excludeCamera = go.AddComponent<Camera>();
                }
                else
                {
                    excludeCamera = go.GetComponent<Camera>();
                }
            }
            return excludeCamera;
        }
    }
    #endregion

    #region 在指定的相机前增加一个蒙版，和基于GUITexture的FADE技术不同的是，这种FADE还可以支持EXCLUDE机制，即我们可以和他结合只FADE部分对象
    private static Renderer fadeMask;
    private static Renderer FadeMask(this Camera targetCamera)
    {
        if (!fadeMask)
        {
            Transform trans = targetCamera.transform.FindChild("_Mask");
            if (!trans)
            {
                trans = GameObject.CreatePrimitive(PrimitiveType.Quad).transform;
                trans.gameObject.layer = GameUtils.LayerFade;
                trans.name = "_Mask";
                trans.transform.localScale = new Vector3(1000, 1000, 1);
            }
            fadeMask = trans.GetComponent<Renderer>();

            if (!fadeMask.sharedMaterial)
            {
                fadeMask.sharedMaterial = new Material(Shader.Find("Transparent/Diffuse"));
            }
            else
            {
                fadeMask.sharedMaterial.shader = Shader.Find("Transparent/Diffuse");
            }
            fadeMask.sharedMaterial.mainTexture = CameraUtils.GetColorTexture(Color.black);
        }
        //设置目标相机的蒙版
        fadeMask.gameObject.SetActive(true);
        fadeMask.transform.parent = targetCamera.transform;
        fadeMask.transform.localPosition = new Vector3(0, 0, 1);
        fadeMask.transform.localRotation = Quaternion.identity;

        //目标相机添加蒙版层
        targetCamera.cullingMask |= (1 << GameUtils.LayerFade);
        return fadeMask;
    }
    /// <summary>
    /// 相机移除蒙版层
    /// </summary>
    /// <param name="targetCamera"></param>
    private static void UnFadeMask(this Camera targetCamera)
    {
        if (fadeMask)
        {
            fadeMask.gameObject.SetActive(false);
        }
        targetCamera.cullingMask &= ~(1 << GameUtils.LayerFade);
    }
    #endregion

    #region 淡入淡出
    private static Tweener fadeTween;
    private static Camera targetCamera;
    public static void Fade(this Camera camera, float time, bool In, LayerMask excludeLayer, Action cb)
    {
        try
        {
            if (camera == null) return;
            StopFade();
            
            if (time < 0) time = 0;
            targetCamera = camera;
            targetCamera.Exclude(excludeLayer);
            Renderer render = targetCamera.FadeMask();
            Material mat = render.sharedMaterial;
            Color color = fadeTargetColor;
            if (In)
            {
                color.a = 1;
                mat.color = color;
                fadeTween = TweenTools.DOFade(mat, 0, time, null, () =>
                {
                    StopFade();
                    if (cb != null) cb();
                });
            }
            else
            {
                color.a = 0;
                mat.color = color;
                fadeTween = TweenTools.DOFade(mat, 1, time, null, () =>
                {
                    StopFade();
                    if (cb != null) cb();
                });
            }
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 淡入淡出
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="time"></param>
    /// <param name="In"></param>
    /// <param name="cb"></param>
    public static void Fade(this Camera camera, float time, bool In, Action cb)
    {
        Fade(camera, time, In, 0, cb);
    }
    /// <summary>
    /// 高亮
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="time"></param>
    /// <param name="In"></param>
    /// <param name="excludeLayer"></param>
    public static void HightLight(this Camera camera, float time, bool In, LayerMask excludeLayer)
    {
        try
        {
            if (camera == null) return;

            if (time < 0) time = 0;
            Material mat = camera.HightLight(excludeLayer);
            Color color = fadeTargetColor;
            if (In)
            {
                color.a = 1;
                mat.color = color;
                fadeTween = TweenTools.DOFade(mat, 0, time, null, () =>
                {
                    StopFade();
                });
            }
            else
            {
                color.a = 0;
                mat.color = color;
                fadeTween = TweenTools.DOFade(mat, 1, time, null, () =>
                {
                    TweenTools.DOKill(ref fadeTween);
                });
            }
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    /// <summary>
    /// 高亮
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="excludeLayer"></param>
    /// <param name="alpha"></param>
    public static void HightLight(this Camera camera, LayerMask excludeLayer, float alpha)
    {
        try
        {
            StopFade();
            Material mat = camera.HightLight(excludeLayer);
            Color color = fadeTargetColor;
            color.a = alpha;
            mat.color = color;
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
        }
    }
    private static Material HightLight(this Camera camera, LayerMask excludeLayer)
    {
        try
        {
            targetCamera = camera;
            targetCamera.Exclude(excludeLayer);
            Renderer render = targetCamera.FadeMask();
            return render.sharedMaterial;
        }
        catch (Exception ex)
        {
            Printer.LogException(ex);
            return null;
        }
    }
    /// <summary>
    /// 设置目标相机颜色
    /// </summary>
    private static Color fadeTargetColor = new Color(0, 0, 0, 1);
    public static void SetTargetColor(this Camera camera, Color color)
    {
        fadeTargetColor = color;
    }
    /// <summary>
    /// 停止渐变/高亮
    /// </summary>
    public static void StopFade()
    {
        UnExclude();
        if (targetCamera) targetCamera.UnFadeMask();
        TweenTools.DOKill(ref fadeTween);
    }
    #endregion

    #region 相机移动对齐
    private static Tweener moveTweener;
    public static Tweener Move(this Transform transform, Vector3 position, float time, Action cb)
    {
        TweenTools.DOKill(ref moveTweener);
        if (transform == null) return moveTweener;
        if (time <= 0)
        {
            transform.position = position;
            if (cb != null) cb();
        }
        else
        {
            moveTweener = TweenTools.DOWorldMove(transform, position, time, null, () =>
            {
                TweenTools.DOKill(ref moveTweener);
                if (cb != null) cb();
            });
        }
        return moveTweener;
    }
    #endregion

    #region 纯颜色图片，只有四个像素，可以用于各种纯颜色图片的场合
    private static Dictionary<Color, Texture2D> colorTextures = new Dictionary<Color, Texture2D>();
    private static Texture2D CreateColorTexture(Color color)
    {
        Texture2D texRet = new Texture2D(2, 2);
        Color[] colors = new Color[4];
        for (int i = 0; i < 4; i++)
        {
            colors[i] = color;
        }
        texRet.SetPixels(colors);
        texRet.Apply();
        return texRet;
    }
    public static Texture2D GetColorTexture(Color color)
    {
        Texture2D ret = null;
        if (!colorTextures.TryGetValue(color, out ret))
        {
            ret = CreateColorTexture(color);
            colorTextures[color] = ret;
        }
        return ret;
    }
    #endregion

} 
