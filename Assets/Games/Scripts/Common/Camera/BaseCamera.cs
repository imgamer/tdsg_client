﻿/// <summary>
/// 场景相机附加功能基类
/// </summary>
public abstract class BaseCamera 
{
    protected SceneCamera sceneCamera;
    public void Init(SceneCamera sceneCamera)
    {
        this.sceneCamera = sceneCamera;
        OnInit();
    }
    protected abstract void OnInit();

    public void UnInit()
    {
        OnUnInit();
    }
    protected abstract void OnUnInit();
}
