﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 场景相机对象类
/// </summary>
[RequireComponent(typeof(Camera))]
public class SceneCamera : MonoBehaviour 
{
    #region 初始化
    private Camera camera;
    public void Init()
    {
        camera = transform.GetComponent<Camera>();
        UpdateViewSize();
    }
    #endregion

    #region 获取相机
    public Camera GetCamera()
    {
        return camera;
    }
    #endregion

    #region 显示隐藏
    public void Show(bool show)
    {
        camera.enabled = show;
    }
    #endregion

    #region 参数复制
    public void CopyFrom(EditorCamera target)
    {
        if (target == null) return;
        camera.CopyFrom(target.MainCamera);
        camera.transform.localPosition = Vector3.zero;
        camera.transform.localEulerAngles = Vector3.zero;
        camera.transform.localScale = Vector3.one;

        UpdateViewSize();
        SetState(target.state);
        Destroy(target.gameObject);
    }
    #endregion

    #region 状态设置
    private BaseCamera cameraType = new StaticCamera();
    /// <summary>
    /// 恢复之前状态
    /// </summary>
    private CameraState lastState = CameraState.Static;
    private void ResumeState()
    {
        SetState(lastState);
    }
    /// <summary>
    /// 设置当前状态
    /// </summary>
    public CameraState curstate = CameraState.Static;
    private void SetCurState()
    {
        SetState(curstate);
    }
    /// <summary>
    /// 设置状态
    /// </summary>
    public void SetState(CameraState state)
    {
        if (curstate == state) return;
        lastState = curstate;
        curstate = state;

        cameraType.UnInit();
        switch (state)
        {
            case CameraState.Static:
                cameraType = new StaticCamera();
                cameraType.Init(this);
                break;
            case CameraState.Follow:
                cameraType = new FollowCamera();
                cameraType.Init(this);
                break;
        }
    }
    #endregion

    #region 视野大小
    private Vector2 viewSize = Vector2.zero;
    public Vector2 ViewSize
    {
        get { return viewSize; }
    }
    public float OrthographicSize
    {
        get { return camera.orthographicSize; }
        set 
        {
            if (Mathf.Approximately(camera.orthographicSize, value)) return;
            camera.orthographicSize = value;
        }
    }
    private void UpdateViewSize()
    {
        float orthographicSize = camera.orthographicSize;
        viewSize.x = ((float)Screen.width / Screen.height) * orthographicSize;
        viewSize.y = orthographicSize;
    }
    #endregion

    #region 获取合法位置
    public void GetValidPosition(ref Vector3 validPosition)
    {
        MapDetail mapRoot = MapDetail.instance;
        if (mapRoot != null)
        {
            float left = mapRoot.LeftX + ViewSize.x;
            float right = mapRoot.RightX - ViewSize.x;
            if (validPosition.x < left)
            {
                validPosition.x = left;
            }
            else if (validPosition.x > right)
            {
                validPosition.x = right;
            }

            float top = mapRoot.TopY - ViewSize.y;
            float bottom = mapRoot.BottomY + ViewSize.y;
            if (validPosition.y > top)
            {
                validPosition.y = top;
            }
            else if (validPosition.y < bottom)
            {
                validPosition.y = bottom;
            }
        }
    }
    #endregion
}
