﻿using UnityEngine;
/// <summary>
/// 编辑相机脚本
/// </summary>
public enum CameraState
{
    Static,
    Follow,
}
[RequireComponent(typeof(Camera))]
public class EditorCamera : MonoBehaviour 
{
    public static EditorCamera instance { get; private set; }
    public CameraState state = CameraState.Static;
    public Camera MainCamera { get; private set; }
    void Awake()
    {
        instance = this;
        MainCamera = transform.GetComponent<Camera>();
        gameObject.SetActive(false);
    }
}
