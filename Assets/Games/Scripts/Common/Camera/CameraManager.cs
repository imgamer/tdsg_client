﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
/// <summary>
/// 场景相机管理
/// </summary>
public class CameraManager : MonoBehaviour
{
    #region instance
    public static CameraManager instance { get; private set; }
    void Awake()
    {
        instance = this;
        Init();
    }
    #endregion

    public Camera CurCamera { get { return sceneCamera.GetCamera(); } } 
    private SceneCamera sceneCamera;
    private void Init()
    {
        sceneCamera = transform.Find("MainCamera").GetComponent<SceneCamera>();
        if(sceneCamera != null)
        {
            sceneCamera.Init();
        }
        else
        {
            Printer.LogError("相机初始化失败!");
        }
    }

    private void UnInit()
    {

    }

    #region 相机显示隐藏
    public void Show(bool show)
    {
        sceneCamera.Show(show);
    }
    #endregion

    #region 相机切换对齐方式
    private bool CopyFrom(EditorCamera target)
    {
        if (target == null) return false;
        transform.position = target.transform.position;
        transform.rotation = target.transform.rotation;
        transform.localScale = Vector3.one;

        sceneCamera.CopyFrom(target);
        return true;
    }
    public void AlignTo(Action cb = null)
    {
        TweenTools.DOKill(ref _moveTweener);
        CopyFrom(EditorCamera.instance);
        if (cb != null) cb();
    }
    public void AlignFadeTo(Action cb = null)
    {
        TweenTools.DOKill(ref _moveTweener);
        if (CopyFrom(EditorCamera.instance))
        {
            CameraUtils.Fade(sceneCamera.GetCamera(), 2, true, () =>
            {
                if (cb != null) cb();
            });
        }
        else
        {
            if (cb != null) cb();
        }
    }
    #endregion

    private bool LegalMove()
    {
        return sceneCamera.curstate != CameraState.Static;
    }

    #region 相机跟随目标
    //只在Update函数中调用
    public void FollowTarget(Vector3 targetPos)
    {
        TweenTools.DOKill(ref _moveTweener);
        Vector3 camPos = transform.position;
        targetPos.z = camPos.z;

        sceneCamera.GetValidPosition(ref targetPos);
        Vector3 offset = (targetPos - camPos) * 0.8f;
        transform.position = targetPos - offset;
    }
    //平滑跟随目标
    private Tweener _moveTweener;
    public void AlignTarget(Vector3 targetPos, float time, Action cb = null)
    {
        if (!LegalMove()) return;
        Vector3 camPos = transform.position;
        if (Vector2.Distance(targetPos, camPos) < Mathf.Epsilon) return;

        targetPos.z = camPos.z;
        sceneCamera.GetValidPosition(ref targetPos);
        _moveTweener = CameraUtils.Move(transform, targetPos, time, cb);
    }
    #endregion

    #region 高亮指定层次的物体
    //高亮指定层次的物体
    public void HightLight(Color color, float time, bool In, LayerMask excludeLayer)
    {
        CurCamera.SetTargetColor(color);
        CurCamera.HightLight(time, In, excludeLayer);
    }
    #endregion

}