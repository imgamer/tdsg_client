﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Item
{
    public class Operation
    {
        public string operationName;
        public Predicate<ItemBase> operationAction;
        public Operation(string operationName, Predicate<ItemBase> operationAction)
        {
            this.operationName = operationName;
            this.operationAction = operationAction;
        }
    }
    public static class ItemOperationRule
    {
        public const byte ITEM_NONE = 0;//无定义，什么也不做
        public const byte ITEM_USE = 1;//使用
        public const byte ITEM_BATCHUSE = 2;//批量使用
        public const byte ITEM_ABANDON = 3;//丢弃
        public const byte ITEM_SALE = 4;//摆摊出售
        public const byte ITEM_DONATE = 5;//赠送
        public const byte ITEM_RESOLVE = 6;//分解
        public const byte ITEM_COMPOUND = 7;//合成


        public const byte EQUIP_DRESS = 20;//装备
        public const byte EQUIP_UNFIX = 21;//卸下
        public const byte EQUIP_INLAY = 22;//镶嵌
        public const byte EQUIP_REPAIR = 23;//修理
        public const byte EQUIP_MELT = 24;//溶炼
        public const byte EQUIP_RESOLVE = 25;//分解


        private static readonly Dictionary<byte, Operation> OperationMap = new Dictionary<byte, Operation>() 
        {
            {ITEM_NONE, new Operation("无效", null)},
            {ITEM_USE, new Operation("使用", (data)=>
            {
                ItemUsable itemUsable = data as ItemUsable;
                if (itemUsable != null)
                {
                    itemUsable.Use(GameMain.Player, GameMain.Player);
                }
                return true;
            })},
            {ITEM_BATCHUSE, new Operation("批量使用", (data)=>{return true;})},
            {ITEM_ABANDON, new Operation("丢弃", (data)=>{return true;})},
            {ITEM_SALE, new Operation("摆摊出售", (data)=>{return true;})},
            {ITEM_DONATE, new Operation("赠送", (data)=>{return true;})},
            {ITEM_RESOLVE, new Operation("分解", (data)=>{return true;})},
            {ITEM_COMPOUND, new Operation("合成", (data)=>{return true;})},
            {EQUIP_DRESS, new Operation("装备", (data)=>
            {
                ItemEquipment itemEquip = data as ItemEquipment;
                if (itemEquip != null)
                {
                    GameMain.Player.equipModule.SendPutOnEquipment(itemEquip);
                }
                return true;
            })},
            {EQUIP_UNFIX, new Operation("卸下", (data)=>
            {
                ItemEquipment itemEquip = data as ItemEquipment;
                if (itemEquip != null)
                {
                    GameMain.Player.equipModule.SendTakeOffEquipment(itemEquip);
                }
                return true;
            })},
            {EQUIP_INLAY, new Operation("镶嵌", (data)=>{return true;})},
            {EQUIP_REPAIR, new Operation("修理", (data)=>{return true;})},
            {EQUIP_MELT, new Operation("溶炼", (data)=>{return true;})},
            {EQUIP_RESOLVE, new Operation("分解", (data)=>
            {
                ItemEquipment itemEquip = data as ItemEquipment;
                if (itemEquip != null)
                {
                    GameMain.Player.equipModule.ResolveEquipment(itemEquip);
                }
                return true;
            })},
        };

        public static bool TryGetOperationByKey(byte key, out Operation operation)
        {
            return OperationMap.TryGetValue(key, out operation);
        }

    }
}
