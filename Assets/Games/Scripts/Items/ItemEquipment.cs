//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using LitJson;

using JSON_DICT_TYPE = System.Collections.Generic.Dictionary<string, object>;

namespace Item
{
	public class ItemEquipment: ItemBase
	{
        private int _durabilityMax;
        private List<PropertyBase> _baseProperties = new List<PropertyBase>();
        private List<PropertyBase> _extraProperties = new List<PropertyBase>();

        private List<byte> operationList = new List<byte>() { ItemOperationRule.EQUIP_DRESS, ItemOperationRule.EQUIP_RESOLVE };
        public override List<byte> OperationList
        {
            get { return operationList; }
        }

		#region properties
        public int Durability
        {
            get
            {
                JsonData durability = QueryExtra("durability");
                if (durability != null)
                {
                    return (int)durability;
                }
                else
                {
                    return _durabilityMax;
                }
            }
        }

        public int DurabilityMax
        {
            get
            {
                return _durabilityMax;
            }
        }

        public override byte Quality
        {
            get {
                JsonData quality = QueryExtra("quality");
                if (quality != null)
                {
                    return (byte)quality;
                }
                else
                {
                    return base.Quality;
                }
            }
        }

        //装备正在生效的特技
        public Int32 SpecialSkill
        {
            get 
            {
                JsonData data = QueryExtra("specialSkill");
                if(data !=null)
                {
                    return (Int32)data;
                }
                else
                {
                    return 0;
                }
            }         
        }

        //可以替换的新特技，一般洗练后会出现，替换后才会生效
        public Int32 NewSpecialSkill
        {
            get 
            {
                JsonData data = QueryExtra("newSpecialSkill");
                if(data !=null)
                {
                    return (Int32)data;
                }
                else
                {
                    return 0;
                }
            }         
        }

        public Int32 StarLevel
        {
            get
            {
                JsonData data = QueryExtra("starLevel");
                if (data != null)
                {
                    return (Int32)data;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Int32 StarExp
        {
            get
            {
                JsonData data = QueryExtra("starExp");
                if (data != null)
                {
                    return (Int32)data;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Dictionary<int, int> EquipGem
        {
            get
            {
                JsonData data = QueryExtra("gemBag");              
                Dictionary<int, int> gem = new Dictionary<int, int>();
                if (data == null)
                {
                    return gem;
                }
                foreach(string index in data.Keys)
                {
                    gem[Convert.ToInt32(index)] = (int)data[index];
                }
                return gem;
            }
        }

        public List<PropertyBase> BaseProperties
		{
			get{return _baseProperties;}
		}

        public List<PropertyBase> ExtraProperties
		{
            get { return _extraProperties; }
		}

        public List<PropertyBase> NewExtraProperties
        {
            get
            {
                List<PropertyBase> newProperties = new List<PropertyBase>();
                JsonData data = QueryExtra("newExtraProperties"); 
                if(data != null)
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        newProperties.Add(EquipmentProperties.BuildPropertyFromStream(data[i]));
                    }
                }             
                return newProperties;
            }
        }
		#endregion
		
		public override void InitFromConfig(ItemsData itemData)
		{
			base.InitFromConfig(itemData);
            _durabilityMax = itemData.durability;
			InitProperties();
		}

		public override void InitFromServer(CLIENT_ITEM itemInfo)
		{
			base.InitFromServer(itemInfo);

            //加载基础属性
            JsonData basePropertiesData = QueryExtra("baseProperties");
            for (int i = 0; i < basePropertiesData.Count; i++)
            {
                _baseProperties.Add(EquipmentProperties.BuildPropertyFromStream(basePropertiesData[i]));
            }

            //加载附加属性
            JsonData extraPropertiesData = QueryExtra("extraProperties");
            for (int i = 0; i < extraPropertiesData.Count; i++)
            {
                _extraProperties.Add(EquipmentProperties.BuildPropertyFromStream(extraPropertiesData[i]));
            }

            RefreshProperties();
		}
			
		protected virtual void InitProperties()
		{
		}

		protected virtual void RefreshProperties()
		{
		}

        public virtual void PutOnTo(KBEngine.GameObject target)
		{
            foreach (var property in _baseProperties)
			{
				property.ApplyTo(target);
			}

            foreach (var property in _extraProperties)
            {
                property.ApplyTo(target);
            }
		}

        public virtual void TakeOffFrom(KBEngine.GameObject target)
		{
            foreach (var property in _baseProperties)
			{
				property.RemoveFrom(target);
			}

            foreach (var property in _extraProperties)
            {
                property.RemoveFrom(target);
            }
		}
		
		#region setter
        protected virtual void set_quality(Int32 value)
        {
            SetExtra("quality", new JsonData(value));
        }

        protected virtual void set_durability(Int32 value)
        {
            SetExtra("durability", new JsonData(value));
        }
       
        //implement field setter here:
        /// <summary>
        /// 特技更新
        /// </summary>
        /// <param name="skillID"></param>
        protected virtual void set_specialSkill(Int32 skillID)
        {
            SetExtra("specialSkill", new JsonData(skillID));
            if (skillID == NewSpecialSkill)
            {
                SetExtra("newSpecialSkill", 0);
            }
            // to do: 通知更新
            Printer.Log("equipment {0} update special skill to {1}", this.ID, skillID);
        }

        /// <summary>
        /// 更新新特技，一般是特技洗练的时候，通知客户端洗练后获得的新特技，
        /// 由客户端决定是否替换。
        /// </summary>
        /// <param name="skillID"></param>
        protected virtual void set_newSpecialSkill(Int32 skillID)
        {
            SetExtra("newSpecialSkill", new JsonData(skillID));
            // to do: 通知更新
            Printer.Log("equipment {0} update new special skill to {1}", this.ID, skillID);
        }
		#endregion
        
        #region 宝石镶嵌
        protected void set_gemBag(string value)
        {
            JsonData data = JsonMapper.ToObject(value);
            SetExtra("gemBag", data);
        }

        protected virtual void set_gemType(int value)
        {
            SetExtra("gemType", new JsonData(value));
        }

        #endregion

        #region 装备升星
        protected virtual void set_starLevel(Int32 value)
        {
            SetExtra("starLevel", new JsonData(value));
        }

        protected virtual void set_starExp(Int32 value)
        {
            SetExtra("starExp", new JsonData(value));
        }
        #endregion

        #region 属性洗练
        protected virtual void set_rExtraProperties(string value)
        {
            JsonData extraPropertiesData = JsonMapper.ToObject(value);           
            _extraProperties.Clear();
            for (int i = 0; i < extraPropertiesData.Count; i++)
            {
                _extraProperties.Add(EquipmentProperties.BuildPropertyFromStream(extraPropertiesData[i]));
            }
            SetExtra("newExtraProperties", null);
        }

        protected virtual void set_newExtraProperties(string value)
        {
            JsonData extraPropertiesData = JsonMapper.ToObject(value);
            SetExtra("newExtraProperties", extraPropertiesData);
        }
        #endregion

        #region 升阶
        protected virtual void set_buildBaseProperties(string value)
        {
            JsonData basePropertiesData = JsonMapper.ToObject(value);
            _baseProperties.Clear();
            for (int i = 0; i < basePropertiesData.Count; i++)
            {
                _baseProperties.Add(EquipmentProperties.BuildPropertyFromStream(basePropertiesData[i]));
            }
        }

        protected virtual void set_buildExtraProperties(string value)
        {
            JsonData extraPropertiesData = JsonMapper.ToObject(value);
            _extraProperties.Clear();
            for (int i = 0; i < extraPropertiesData.Count; i++)
            {
                _extraProperties.Add(EquipmentProperties.BuildPropertyFromStream(extraPropertiesData[i]));
            }
        }
        #endregion

        #region notifier
        public virtual void onSet_level(int old)
        {
            RefreshProperties();
        }
        #endregion
    }
}

