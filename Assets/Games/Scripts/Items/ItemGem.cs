﻿using UnityEngine;
using System.Collections;

namespace Item
{
    public class ItemGem : ItemBase
    {
        private int _gemType;

        public int GemType
        {
            get
            {
                return _gemType;
            }
        }
        /// <summary>
        /// 从配置初始化
        /// </summary>
        /// <param name="itemData">配置数据</param>
        public override void InitFromConfig(ItemsData itemData)
        {
            base.InitFromConfig(itemData);
            _gemType = itemData.gemType;
        }
    }
}
