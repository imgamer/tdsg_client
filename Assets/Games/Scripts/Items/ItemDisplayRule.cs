﻿using UnityEngine;
using System.Collections;
using Quest;
using System.Collections.Generic;

namespace Item
{
    public class ItemDisplay
    {
        public string icon;
        public string name;
        public string[] text = new string[2];
        public string[] desc = new string[2];
        public ItemBase data;

        public ItemDisplay(string icon, string name, string[] text, string[] desc, ItemBase data = null)
        {
            this.icon = icon;
            this.name = name;
            this.text = text;
            this.desc = desc;
            this.data = data;
        }
    }
    /// <summary>
    /// Item类的界面显示规则
    /// </summary>
    public static class ItemDisplayRule
    {
        /// <summary>
        /// 获取颜色值
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static string GetItemColor(int quality)
        {
            switch (quality)
            {
                case ItemTypeDefine.ITEM_QUALITY_ONE:
                    return "[FFFFFF]{0}[-]";
                case ItemTypeDefine.ITEM_QUALITY_TWO:
                    return "[7CFC00]{0}[-]";
                case ItemTypeDefine.ITEM_QUALITY_THREE:
                    return "[0000FF]{0}[-]";
                case ItemTypeDefine.ITEM_QUALITY_FOUR:
                    return "[912CEE]{0}[-]";
                case ItemTypeDefine.ITEM_QUALITY_FIVE:
                    return "[EE7600]{0}[-]";
            }
            return string.Empty;
        }
        /// <summary>
        /// 获取物品名称
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetItemName(ItemBase item)
        {
            if (item == null) return string.Empty;
            return string.Format(GetItemColor(item.Quality), item.Name);
        }
        /// <summary>
        /// 获取物品名称
        /// </summary>
        /// <param name="item"></param>
        /// <param name="quality"></param>
        /// <returns></returns>
        public static string GetItemName(ItemBase item, int quality)
        {
            if (item == null) return string.Empty;
            return string.Format(GetItemColor(quality), item.Name);
        }
        /// <summary>
        /// 子类型的名称
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetItemChildTypeName(ItemBase item)
        {
            if (item == null) return string.Empty;
            return GetItemChildTypeName(item.ChildType);
        }
        public static string GetItemChildTypeName(int childType)
        {
            switch (childType)
            {
                case ItemTypeDefine.ITEM_BROADSSWORD:
                    return "刀";
                case ItemTypeDefine.ITEM_SPEAR:
                    return "枪";
                case ItemTypeDefine.ITEM_SWORD:
                    return "剑";
                case ItemTypeDefine.ITEM_CROSSBOW:
                    return "弩";
                case ItemTypeDefine.ITEM_FAN:
                    return "扇";
                case ItemTypeDefine.ITEM_CANE:
                    return "杖";
                case ItemTypeDefine.ITEM_HELMET:
                    return "盔甲";
                case ItemTypeDefine.ITEM_ARMOR:
                    return "衣甲";
                case ItemTypeDefine.ITEM_BOOT:
                    return "鞋子";
                case ItemTypeDefine.ITEM_BELT:
                    return "腰带";
                case ItemTypeDefine.ITEM_NECKLACE:
                    return "项链";
                case ItemTypeDefine.ITEM_RING:
                    return "戒子";
                case ItemTypeDefine.ITEM_ORNAMENT:
                    return "饰品";
                case ItemTypeDefine.ITEM_BADGE:
                    return "徽章";

                case ItemTypeDefine.ITEM_BILL:
                    return "银票";
                case ItemTypeDefine.ITEM_BILL_UNBOUND:
                    return "非绑定银票";
                case ItemTypeDefine.ITEM_HP_STORE:
                    return "生命储备符";
                case ItemTypeDefine.ITEM_MP_STORE:
                    return "法力储备符";
                case ItemTypeDefine.ITEM_DOUBLE_EXP:
                    return "双倍经验丹";
                case ItemTypeDefine.ITEM_EXP_PILL:
                    return "经验丹";
                case ItemTypeDefine.ITEM_TAOISM_PILL:
                    return "道行丹";
                case ItemTypeDefine.ITEM_PRACTICE_CARD:
                    return "历练卡";
                case ItemTypeDefine.ITEM_COMBAT_GAIN_CARD:
                    return "战绩卡";
                case ItemTypeDefine.ITEM_FLOWER:
                    return "鲜花";
                case ItemTypeDefine.ITEM_ACTIVITY_GIFTS_BAG:
                    return "活跃礼包";
                case ItemTypeDefine.ITEM_LEVEL_GIFTS_BAG:
                    return "等级礼包";
                case ItemTypeDefine.ITEM_VIP_GIFTS_BAG:
                    return "VIP礼包";
                case ItemTypeDefine.ITEM_KEJU_GIFTS_BAG:
                    return "科举礼包";
                case ItemTypeDefine.ITEM_HIDDEN_DRAGON_BOX:
                    return "藏龙匣";
                case ItemTypeDefine.ITEM_PET_SKILL_BOX:
                    return "宠物技能书匣";
                case ItemTypeDefine.ITEM_TREASURE_MAP_BOX:
                    return "藏宝图宝箱";
                case ItemTypeDefine.ITEM_SUPER_LUCKY_BOX:
                    return "大吉大利宝箱";
                case ItemTypeDefine.ITEM_HP_DRUG:
                    return "生命药";
                case ItemTypeDefine.ITEM_MP_DRUG:
                    return "法力药";
                case ItemTypeDefine.ITEM_STAMINA_DRUG:
                    return "体力药";
                case ItemTypeDefine.ITEM_DOUQI_WINE:
                    return "斗气酒";
                case ItemTypeDefine.ITEM_RESET_PROPERTY:
                    return "洗点券";
                case ItemTypeDefine.ITEM_TREASURE_MAP:
                    return "藏宝图";
                case ItemTypeDefine.ITEM_QUEST_TICKET:
                    return "任务门票";

                case ItemTypeDefine.ITEM_QUEST:
                    return "任务道具";
                case ItemTypeDefine.ITEM_GEM:
                    return "宝石";
                case ItemTypeDefine.ITEM_PRODUCE_MATERIAL:
                    return "生产材料";
                case ItemTypeDefine.ITEM_ENHANCE_MATERIAL:
                    return "强化材料";
                case ItemTypeDefine.ITEM_ACTIVITY:
                    return "活动道具";
                case ItemTypeDefine.ITEM_PET_SKILL_BOOK:
                    return "宠物技能书";
                case ItemTypeDefine.ITEM_PET_EXP_PILL:
                    return "宠物经验丹";
                case ItemTypeDefine.ITEM_PET_EXTEND_LIFE_PILL:
                    return "宠物延寿丹";
                case ItemTypeDefine.ITEM_PET_JUVEN_PILL:
                    return "宠物返童丹";
                case ItemTypeDefine.ITEM_PET_ENHANCE_PILL:
                    return "宠物强化丹";
                case ItemTypeDefine.ITEM_PET_REBUILD_INSTINCT:
                    return "宠物资质重塑丹";
                case ItemTypeDefine.ITEM_PET_UPGRADE_STAGE:
                    return "宠物进阶道具";
            }
            return string.Empty;
        }
        /// <summary>
        /// 获取物品描述
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetItemDesc(ItemBase item)
        {
            if (item == null) return string.Empty;
            return string.Format(GetItemColor(item.Quality), item.Description);
        }

        public static ItemDisplay GetItemDisplayByItem(ItemBase item)
        {
            if (item == null) return null;
            string[] text = new string[2] { string.Format("类型：{0}", GetItemChildTypeName(item)), string.Format("等级：{0}", item.Level) };
            string[] desc = new string[2] { GetItemDesc(item), GetItemDesc(item) };
            ItemDisplay ret = new ItemDisplay(item.Icon, GetItemName(item), text, desc, item);
            return ret;
        }

        public static ItemDisplay GetItemDisplayByItem(ItemsData item)
        {
            if (item == null) return null;
            string[] text = new string[2] { string.Format("类型：{0}", GetItemChildTypeName(item.childType)), string.Format("等级：{0}", 1) };
            string[] desc = new string[2] { item.description, string.Empty };
            ItemDisplay ret = new ItemDisplay(item.icon, item.name, text, desc);
            return ret;
        }

        public static ItemDisplay GetItemDisplayByItem(QuestReward item)
        {
            if (item == null) return null;
            string[] text = new string[2] { string.Format("类型：{0}", "未定义"), string.Format("等级：{0}", 1) };
            string[] desc = new string[2] { item.Name, string.Empty };
            ItemDisplay ret = new ItemDisplay(item.Icon, item.Name, text, desc);
            return ret;
        }
    }
}

