using System;
using System.Diagnostics;
using System.Collections.Generic;
using KBEngine;

//Item types define（定义物品相关的数据类型）
using ITEM_ORDER_TYPE = System.Int16;

namespace Item
{
	public class KitbagModule
	{
        private Avatar _avatar;

		private Dictionary<ITEM_ORDER, ItemBase> _items = new Dictionary<ITEM_ORDER, ItemBase>();
		private Dictionary<ITEM_UID, ITEM_ORDER> _uid2Order = new Dictionary<ITEM_UID, ITEM_ORDER>();

        private Dictionary<ITEM_ORDER, ItemBase> _warehouse = new Dictionary<ITEM_ORDER, ItemBase>();

        private int _currentWarehouseIndex; 
		public KitbagModule(Avatar avatar)
		{
            _avatar = avatar;
		}

		public int Count
		{
			get{ return _items.Count;}
		}

		/// <summary>
		/// 获取背包中所有物品实例
		/// </summary>
		/// <value>The items.</value>
		public List<ItemBase> Items
		{
			get{ return new List<ItemBase>(_items.Values);}
		}

		/// <summary>
		/// 获取背包中所有物品ITEM_ORDER
		/// </summary>
		/// <value>The ITEM_ORDER list of _items.</value>
		public List<ITEM_ORDER> ItemOrders
		{
			get{ return new List<ITEM_ORDER>(_items.Keys);}
		}

        /// <summary>
        /// 获取对应子类型道具
        /// </summary>
        /// <param name="childType"></param>
        /// <returns></returns>
        public List<ItemBase> ChildTypeItems(UInt16 childType)
        {
            List<ItemBase> Items = new List<ItemBase>();
            foreach (ItemBase item in _items.Values)
            {
                if (item.ChildType == childType)
                {
                    Items.Add(item);
                }
            }
            return Items;
        }

        /// <summary>
        /// 获取对应类型的宝石
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<ItemGem> GemTypeItems(int type)
        {
            List<ItemGem> gemTypeItems = new List<ItemGem>();
            List<ItemBase> gemItems = ChildTypeItems(ItemTypeDefine.ITEM_GEM);
            foreach (ItemBase item in gemItems)
            {
                ItemGem gem = item as ItemGem;
                if (gem.GemType == type)
                {
                    gemTypeItems.Add(gem);
                }
            }
            return gemTypeItems;   
        }

        /// <summary>
        /// 获取背包要显示的道具（除去任务道具）
        /// </summary>
        /// <returns></returns>
        public List<ItemBase> BagItems()
        {
            List<ItemBase> Items = new List<ItemBase>();
            foreach (ItemBase item in _items.Values)
            {
                if (item.ChildType != ItemTypeDefine.ITEM_QUEST)
                {
                    Items.Add(item);
                }
            }
            return Items;
        }

        /// <summary>
        /// 获取装备道具
        /// </summary>
        /// <returns></returns>
        public List<ItemEquipment> EquipItems()
        {
            List<ItemEquipment> equipItems = new List<ItemEquipment>();
            foreach (ItemBase item in _items.Values)
            {
                if (item.Type == ItemTypeDefine.ITEM_EQUIPMENT)
                {
                    equipItems.Add(item as ItemEquipment);
                }
            }
            return equipItems;
        }

        /// <summary>
        /// 获取指定等级的装备
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public List<ItemEquipment> EquipLevelItems(int level)
        {
            List<ItemEquipment> equipItems = new List<ItemEquipment>();
            foreach (ItemBase item in _items.Values)
            {
                if (item.Type == ItemTypeDefine.ITEM_EQUIPMENT && item.Level == level)
                {
                    equipItems.Add(item as ItemEquipment);
                }
            }
            return equipItems;
        }

		/// <summary>
		/// 清空所有物品数据
		/// </summary>
		public void Clear()
		{
			_items.Clear();
			_uid2Order.Clear();
		}

        /// <summary>
        /// 获取指定仓库的数据
        /// </summary>
        /// <param name="index"> 几号仓库</param>
        /// <returns></returns>
        public List<ItemBase> GetWarehouseData(int index)
        {
            List<ItemBase> temp = new List<ItemBase>();
            foreach (ITEM_ORDER order in _warehouse.Keys)
            {
                if(index == 1)
                {
                    if((Int32)order <= 50)
                    {
                        temp.Add(_warehouse[order]);
                    }
                }
                else if(index == 2)
                {
                    if ((Int32)order >50 && (Int32)order <= 100 )
                    {
                        temp.Add(_warehouse[order]);
                    }
                }        
            }
            return temp;
        }
		/// <summary>
		/// 增加一个物品
		/// </summary>
		/// <returns><c>true</c>, if item was added, <c>false</c> otherwise.</returns>
		/// <param name="item">物品实例</param>
		public bool AddItem(ItemBase item)
		{
			if (OrderHasItem(item.Order))
			{
                Printer.LogError("order " + item.Order.ToString() + " already has item.");
				return false;
			}

			if (HasUID(item.UID))
			{
                Printer.LogError("Item with uid " + item.UID.ToString() + " already exists.");
				return false;
			}

			_items [item.Order] = item;
			_uid2Order [item.UID] = item.Order;
            EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_ADD_ITEM, item.Order);

			return true;
		}

		/// <summary>
		/// Removes the item by order.
		/// </summary>
		/// <returns><c>true</c>, if item by order was removed, <c>false</c> otherwise.</returns>
		/// <param name="order">物品在背包的索引</param>
		public bool RemoveItemByOrder(ITEM_ORDER order)
		{
			if (OrderHasItem(order))
			{
				ItemBase item = _items [order];
				if (_uid2Order.Remove(item.UID))
				{
					//byte type = GetItemByOrder(order).Type;
					_items.Remove(order);
                    EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, order);
					return true;
				}
				else
				{
                    Printer.LogError("Item with uid " + item.UID.ToString() + " is not exist.");
					return false;
				}
			}
			else
			{
                Printer.LogError("order " + order.ToString() + " dosen't have item.");
				return false;
			}
		}

		/// <summary>
		/// Removes the item by UID
		/// </summary>
		/// <returns><c>true</c>, if item by user interface was removed, <c>false</c> otherwise.</returns>
		/// <param name="uid">物品的UID</param>
		public bool RemoveItemByUID(ITEM_UID uid)
		{
			if (HasUID(uid))
			{
				ITEM_ORDER order = _uid2Order [uid];
				Byte type = GetItemByOrder(order).Type;
				if (_items.Remove(order))
				{
					_uid2Order.Remove(uid);
                    EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_REMOVE_ITEM, order);
					return true;
				}
				else
				{
                    Printer.LogError("order " + order.ToString() + " dosen't have item.");
					return false;
				}
			}
			else
			{
                Printer.LogError("uid " + uid.ToString() + " is not exist.");
				return false;
			}
		}

		/// <summary>
		/// 检查背包索引是否有物品
		/// </summary>
		/// <returns><c>true</c>, if has item was ordered, <c>false</c> otherwise.</returns>
		/// <param name="order">物品在背包的索引.</param>
		public bool OrderHasItem(ITEM_ORDER order)
		{
			return _items.ContainsKey(order);
		}

		/// <summary>
		/// 检查背包是否存在指定uid的物品
		/// </summary>
		/// <returns><c>true</c> if this instance has user interface the specified uid; otherwise, <c>false</c>.</returns>
		/// <param name="uid">物品UID</param>
		public bool HasUID(ITEM_UID uid)
		{
			return _uid2Order.ContainsKey(uid);
		}

		/// <summary>
		/// 检查背包是否存在指定ID的物品
		/// </summary>
		/// <returns><c>true</c> if this instance has user interface the specified uid; otherwise, <c>false</c>.</returns>
		/// <param name="uid">物品UID</param>
		public bool HasItem(ITEM_ID id)
		{
			foreach (ItemBase item in _items.Values)
			{
				if (item.ID == id)
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// 获取相应索引的物品
		/// </summary>
		/// <returns>The item by order.</returns>
		/// <param name="order">Order.</param>
		public ItemBase GetItemByOrder(ITEM_ORDER order)
		{
			ItemBase item;
			_items.TryGetValue(order, out item);
			return item;
		}

		/// <summary>
		/// 获取指定UID的物品
		/// </summary>
		/// <returns>The item by user interface.</returns>
		/// <param name="uid">Uid.</param>
		public ItemBase GetItemByUID(ITEM_UID uid)
		{
			if (_uid2Order.ContainsKey(uid))
			{
				return GetItemByOrder(_uid2Order [uid]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获取指定物的Order
		/// </summary>
		/// <returns>The item by user interface.</returns>
		/// <param name="itemBase">ItemBase.</param>
		public ITEM_ORDER GetOrderByItemBase(ItemBase itemBase)
		{
			if (!_items.ContainsValue(itemBase))
			{
				Printer.LogError("KitbagModule :: GetOrderByItemBase : (!_items.ContainsValue(itemBase))");
				return -1;
			}

			foreach (KeyValuePair<ITEM_ORDER, ItemBase> kvp in _items)
			{
				if (kvp.Value == itemBase)
				{
					return kvp.Key;
				}
			}
			return -1;
		}
		
		/// <summary>
		/// 获取指定ID的物品
		/// </summary>
		/// <returns>The item by user interface.</returns>
		/// <param name="uid">Uid.</param>
		public List<ItemBase> GetItemsByID(ITEM_ID id)
		{
			List<ItemBase> items = new List<ItemBase>();

			foreach (ItemBase item in _items.Values)
			{
				if (item.ID == id)
				{
					items.Add(item);
				}
			}

			return items;
		}
		
		/// <summary>
		/// 交换背包中两个物品格之间的内容，不需要两个物品格都有物品
		/// </summary>
		/// <returns><c>true</c>, if order was swaped, <c>false</c> otherwise.</returns>
		/// <param name="orderA">Order a.</param>
		/// <param name="orderB">Order b.</param>
		public bool SwapOrder(ITEM_ORDER orderA, ITEM_ORDER orderB)
		{
			if (!OrderHasItem(orderA))
			{
				return ReorderItem(orderB, orderA);
			}
			else if (!OrderHasItem(orderB))
			{
				return ReorderItem(orderA, orderB);
			}
			else
			{
				return SwapItem(orderA, orderB);
			}
		}

		/// <summary>
		/// 交换两个物品在背包中的位置，必须两个物品格都有物品
		/// </summary>
		/// <returns><c>true</c>, if order was swaped, <c>false</c> otherwise.</returns>
		/// <param name="orderA">Order a.</param>
		/// <param name="orderB">Order b.</param>
		public bool SwapItem(ITEM_ORDER orderA, ITEM_ORDER orderB)
		{
			if (!OrderHasItem(orderA))
			{
                Printer.LogError("swapItem: order " + orderA.ToString() + " dosen't have item.");
				return false;
			}

			if (!OrderHasItem(orderB))
			{
                Printer.LogError("swapItem: order " + orderB.ToString() + " dosen't have item.");
				return false;
			}

			ItemBase itemA = _items [orderA];
			ItemBase itemB = _items [orderB];

			//设置物品的位置索引
            itemA.SetOrder((ITEM_ORDER_TYPE)orderB);
            itemB.SetOrder((ITEM_ORDER_TYPE)orderA);

			//交换在背包中的位置
			_items [orderA] = itemB;
			_items [orderB] = itemA;

			//更新uid字典中的映射
			_uid2Order [itemA.UID] = itemA.Order;
			_uid2Order [itemB.UID] = itemB.Order;

			return true;
		}

		/// <summary>
		/// 移动一个物品到新的位置
		/// </summary>
		/// <returns><c>true</c>, if item was reordered, <c>false</c> otherwise.</returns>
		/// <param name="src">Source.</param>
		/// <param name="dst">Dst.</param>
		public bool ReorderItem(ITEM_ORDER src, ITEM_ORDER dst)
		{
			if (!OrderHasItem(src))
			{
                Printer.LogError("ReorderItem: src order " + src.ToString() + " dosen't have item.");
				return false;
			}
			
			if (OrderHasItem(dst))
			{
                Printer.LogError("ReorderItem: dst order " + dst.ToString() + " already has item.");
				return false;
			}

			ItemBase item = _items [src];

			//设置物品的位置索引
            item.SetOrder((ITEM_ORDER_TYPE)dst);

			//更新物品在背包中的位置
			_items.Remove(src);
			_items [dst] = item;

			//更新物品在uid字典中的映射
			_uid2Order [item.UID] = dst;

			return true;
		}

		public bool UpdateItemAttribute(ITEM_ORDER order, Int32 attrIndex, object value)
		{
			ItemBase item = GetItemByOrder(order);

			if (item == null)
			{
                Printer.LogError("UpdateItemProperty: order " + order.ToString() + " dosen't have item.");
				return false;
			}

			string attribute = ItemAttributes.Attribute(attrIndex);

			if (attribute == null)
			{
                Printer.LogError("UpdateItemProperty: attribute index " + attrIndex.ToString() + " out of range.");
				return false;
			}

			if (item.UpdateAttribute(attribute, value))
			{
                EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_BAG_UPDATE_ITEM_ATTRIBUTE, order);
				return true;
			}
			return false;
		}

        /// <summary>
        /// 仓库增加一个物品
        /// </summary>
        public bool AddWarehouseItem(ItemBase item)
        {
            if (OrderHasWarehouseItem(item.Order))
            {
                Printer.LogError("order " + item.Order.ToString() + " already has item.");
                return false;
            }

//             if (HasUID(item.UID))
//             {
//                 Dbg.ERROR_MSG("Item with uid " + item.UID.ToString() + " already exists.");
//                 return false;
//             }

            _warehouse[item.Order] = item;
            //_uid2Order[item.UID] = item.Order;
            EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_ADD_ITEM, item.Order);

            return true;
        }

        /// <summary>
        ///检查仓库是否存在该物品 
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool OrderHasWarehouseItem(ITEM_ORDER order)
        {
            return _warehouse.ContainsKey(order);
        }

        /// <summary>
        /// 删除仓库里的物品
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool RemoveWarehouseItemByOrder(ITEM_ORDER order)
        {
            if (OrderHasWarehouseItem(order))
            {
                _warehouse.Remove(order);
                EventManager.Invoke<ITEM_ORDER>(EventID.EVNT_WAREHOUSE_REMOVE_ITEM, order);
                return true;
            }
            else
            {
                Printer.LogError("warehouse order " + order.ToString() + " dosen't have item.");
                return false;
            }
        }

        /// <summary>
        /// 获取仓库相应索引的物品
        /// </summary>
        /// <returns>The item by order.</returns>
        /// <param name="order">Order.</param>
        public ItemBase GetWarehouseItemByOrder(ITEM_ORDER order)
        {
            ItemBase item;
            _warehouse.TryGetValue(order, out item);
            return item;
        }

        /// <summary>
        /// 背包物品放入到仓库
        /// </summary>
        /// <param name="order"></param>
        public void MoveItemToWarehouse(ITEM_ORDER order)
        {
            Int16 orderId = (Int16)order;
            _avatar.cellCall("moveItemsToWareHouse", new object[] { orderId, _currentWarehouseIndex });
        }

        /// <summary>
        /// 仓库物品放入到背包
        /// </summary>
        /// <param name="order"></param>
        public void MoveItemToKitbag(ITEM_ORDER order)
        {
            Int16 orderId = (Int16)order;
            _avatar.baseCall("moveItemsToKitbag", new object[] {orderId});
        }

        /// <summary>
        /// 设置当前所处的仓库号
        /// </summary>
        /// <param name="index"></param>
        public int CurrentWarehouseIndex
        {
            set
            {
                _currentWarehouseIndex = value;
            }
        }

        #region use item
        /// <summary>
        /// 使用道具时，需要吟唱就调用这个方法
        /// </summary>
        /// <param name="item"></param>
        public void IntonateItem(ItemUsable item)
        {
            UIManager.instance.OpenWindow(null, WinID.UIItemIntonate, item);
        }
        #endregion
    }
}

