﻿using UnityEngine;
using System;
/// <summary>
/// 游戏入口类
/// </summary>
public class GameMain
{
	#region 单例
	private static GameMain _instance = new GameMain();
	public static GameMain instance
	{
		get
		{
			return _instance;
		}
	}
	#endregion

    private static KBEngine.Avatar _player;
    public static KBEngine.Avatar Player
    {
        get { return _player; }
    }
    public void SetSceneAvatar(KBEngine.Avatar player)
    {
        _player = player;
    }
    /// <summary>
    /// 游戏开始
    /// </summary>
    public void Start()
    {
        _player = null;
        UIManager.instance.DestroyAll();
        UIManager.instance.OpenWindow(null, WinID.UIConnect);
        EventManager.ClearEvent();
        InitSingleton();
        InitUtils();
        InitKBE();
    }
    /// <summary>
    /// 游戏重新开始
    /// </summary>
    public void ReStart()
    {
        _player = null;
        UIManager.instance.DestroyAll();
        UIManager.instance.OpenWindow(null, WinID.UIConnect);
        EventManager.ClearEvent();
        UnInitSingleton();
        InitSingleton();
    }
    /// <summary>
    /// 资源总表，详细信息表和版本检验完成（未进行增量更新）
    /// </summary>
    public void Ready()
    {
        SceneManager.instance.EnterScene(Const.LoginSceneType);
    }

    /// <summary>
    /// 游戏结束
    /// </summary>
    public void Destroy()
    {

    }

    #region 初始化
    private void InitSingleton()
    {
        AssetsManager.instance.Init();
        InputManager.instance.Init();
        AudioManager.Create();
        TimerManager.Create();
        SceneManager.Create();
        LoginManager.Create();
        HomeManager.Create();
    }
    private void InitUtils()
    {
        GameUtils.InitLayers();
    }
    private KBEEventProc m_eventProc;
    private void InitKBE()
    {
        m_eventProc = new KBEEventProc();
    }
    #endregion

    private void UnInitSingleton()
    {
        AssetsManager.instance.UnInit();
        InputManager.instance.UnInit();
        Transform root = Main.instance.transform.Find(GameUtils.TagSingleton);
        if (root)
        {
            root.gameObject.SendMessage("UnInit", SendMessageOptions.DontRequireReceiver);
        }
    }
}
