﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 特效延迟激活脚本
/// </summary>
[DisallowMultipleComponent]
public class EffectDelayActive : NcEffectBehaviour
{
    // Attribute ------------------------------------------------------------------------
    public float delayTime = 0;
    private float startTime = -1;

    // Property -------------------------------------------------------------------------
#if UNITY_EDITOR
    public override string CheckProperty()
    {
        if (1 < gameObject.GetComponents(GetType()).Length)
            return "SCRIPT_WARRING_DUPLICATE";

        return "";	// no error
    }
#endif

    public override int GetAnimationState()
    {
        if (enabled)
        {
            if (gameObject.activeSelf)
                return 1;
        }
        return 0;
    }

    // Loop Function --------------------------------------------------------------------
    //之所以在Awake执行，主要预防其他NcEffectBehaviour，在Start进行获取时间，或者复制等操作，导致操作时序不对。
    void Awake()
    {
#if UNITY_EDITOR
        if (IsCreatingEditObject() == false)
            InitDelayActive();
#else
 		InitDelayActive();
#endif
    }

    private void InitDelayActive()
    {
        if (delayTime <= 0)
        {
            enabled = false;
            return;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = transform.GetChild(i).gameObject;
            child.SetActive(false);
        }
    }

    void Update()
    {
        if (startTime == -1) startTime = GetEngineTime();
        if (startTime + delayTime <= GetEngineTime())
        {
            StartActive();
        }
    }


    // Event Function -------------------------------------------------------------------
    public override void OnUpdateEffectSpeed(float fSpeedRate, bool bRuntime)
    {
        delayTime /= fSpeedRate;
    }

    private void StartActive()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = transform.GetChild(i).gameObject;
            child.SetActive(true);
        }  
        enabled = false;
    }
}
