﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 特效UV脚本
/// </summary>
[DisallowMultipleComponent]
public class EffectUVScroller : NcEffectBehaviour
{
    // Attribute ------------------------------------------------------------------------
    public int targetMaterialSlot = 0;
    public float speedY = 0.5f;
    public float speedX = 0.0f;
    public float tilingX = 1f;
    public float tilingY = 1f;
    public float offsetX = 0f;
    public float offsetY = 0f;

    private float timeWentX = 0;
    private float timeWentY = 0;

    private Material material;

    // Property -------------------------------------------------------------------------
#if UNITY_EDITOR
    public override string CheckProperty()
    {
        if (1 < gameObject.GetComponents(GetType()).Length)
            return "SCRIPT_WARRING_DUPLICATE";

        return "";	// no error
    }
#endif

    void Start()
    {
        material = GetComponent<Renderer>().materials[targetMaterialSlot];
        material.mainTextureScale = new Vector2(tilingX, tilingY);
    }

    void Update()
    {
        timeWentY += Time.deltaTime * speedY;
        timeWentX += Time.deltaTime * speedX;
        material.SetTextureOffset("_MainTex", new Vector2(timeWentX + offsetX, timeWentY + offsetY));
    }

}
