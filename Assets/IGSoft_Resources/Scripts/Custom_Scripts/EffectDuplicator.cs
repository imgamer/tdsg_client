﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 特效克隆脚本
/// </summary>
[DisallowMultipleComponent]
public class EffectDuplicator : NcEffectBehaviour
{
    // Attribute ------------------------------------------------------------------------
    public float intervalTime = 0;//复制间隔时间
    public float lifeTime = 0;//复制对象生命时间（等于0不销毁，大于0销毁）
    public int cloneCount = 3;//复制对象个数
    public Vector3 positionRange = Vector3.zero;
    public Vector3 rotationAccum = Vector3.zero;
    public Vector2 scaleRange = Vector2.one;

    private Transform _parent;
    private float _startTime = 0;
    private int _cloneCount = 0;
    private GameObject _clonObject;

    // Property -------------------------------------------------------------------------
#if UNITY_EDITOR
    public override string CheckProperty()
    {
        if (1 < gameObject.GetComponents(GetType()).Length)
            return "SCRIPT_WARRING_DUPLICATE";

        // err check
        if (transform.parent != null && transform.parent.gameObject == FindRootEditorEffect())
            return "SCRIPT_ERROR_ROOT";
        return "";	// no error
    }
#endif

    public override int GetAnimationState()
    {
        if ((enabled && IsActive(gameObject)) && (cloneCount == 0 || cloneCount != 0 && _cloneCount < cloneCount))
            return 1;
        return 0;
    }

    protected override void OnDestroy()
    {
        if (_clonObject != null)
            Destroy(_clonObject);
        base.OnDestroy();
    }

    //只能用Awake，Start的话，会导致编辑状态下挂载部分脚本的时候，第二次播放会掉Material
    void Awake()
    {
        _parent = transform.parent;
        if (_parent == null) return;
        _cloneCount = 0;
        _startTime = -intervalTime;
        _clonObject = null;

#if UNITY_EDITOR
        if (IsCreatingEditObject() == false)
#endif
            if (IsActive(gameObject) && GetComponent<NcDontActive>() == null)
            {
                InitCloneObject();
            }  
    }
    private void InitCloneObject()
    {
        if (_clonObject == null)
        {
            // clone ----------------
            _clonObject = CreateGameObject(gameObject);
            _clonObject.name = gameObject.name;

            _clonObject.transform.parent = _parent;
            _clonObject.transform.localPosition = transform.localPosition;
            _clonObject.transform.localRotation = transform.localRotation;
            _clonObject.transform.localScale = transform.localScale;
            
            // Cancel ActiveControl
            HideNcDelayActive(_clonObject);

            // Remove Dup
            EffectDuplicator durCom = _clonObject.GetComponent<EffectDuplicator>();
            if (durCom != null)
                Destroy(durCom);

            // Remove NcDelayActive
            NcDelayActive delCom = _clonObject.GetComponent<NcDelayActive>();
            if (delCom != null)
                Destroy(delCom);

            // this ----------------
            // remove OtherComponent
            Component[] coms = transform.GetComponents<Component>();
            for (int n = 0; n < coms.Length; n++)
            {
                Component com = coms[n];
                if ((com is Transform) || (com is EffectDuplicator)) continue;
                Destroy(com);
            }
            // removeChild
#if (!UNITY_3_5)
            RemoveAllChildObject(gameObject, false);
#else
			RemoveAllChildObject(gameObject, false);
#endif
        }
    }

    void Update()
    {
        if (cloneCount == 0 || _cloneCount < cloneCount)
        {
            if (_startTime + intervalTime <= GetEngineTime())
            {
                _startTime = GetEngineTime();
                CreateCloneObject();
            }
        }
        else
        {
            enabled = false;
        }
    }

    private void CreateCloneObject()
    {
        if (_clonObject == null)  return;

        GameObject createObj = CreateGameObject(_parent.gameObject, _clonObject);
        if (createObj == null)
        {
            Debug.LogError(string.Format("克隆失败:{0}", _clonObject.name));
            return;
        }
        createObj.name += string.Format(" {0}", _cloneCount);
#if (!UNITY_3_5)
        SetActiveRecursively(createObj, true);
#endif

        // duplicateLifeTime
        if (0 < lifeTime)
        {
            NcAutoDestruct ncAd = createObj.GetComponent<NcAutoDestruct>();
            if (ncAd == null) ncAd = createObj.AddComponent<NcAutoDestruct>();
            ncAd.m_fLifeTime = lifeTime;
        }

        // Random pos
        Vector3 newPos = createObj.transform.position;
        createObj.transform.position = new Vector3(Random.Range(-positionRange.x, positionRange.x) + newPos.x, Random.Range(-positionRange.y, positionRange.y) + newPos.y, Random.Range(-positionRange.z, positionRange.z) + newPos.z);

        // accumStartRot
        createObj.transform.localRotation *= Quaternion.Euler(rotationAccum.x * _cloneCount, rotationAccum.y * _cloneCount, rotationAccum.z * _cloneCount);

        float scale = Random.Range(scaleRange.x, scaleRange.y);
        createObj.transform.localScale *= scale;
        _cloneCount++;
    }

    // Event Function -------------------------------------------------------------------
    public override void OnUpdateEffectSpeed(float fSpeedRate, bool bRuntime)
    {
        intervalTime /= fSpeedRate;
        lifeTime /= fSpeedRate;

        if (bRuntime && _clonObject != null) NsEffectManager.AdjustSpeedRuntime(_clonObject, fSpeedRate);
    }
}
